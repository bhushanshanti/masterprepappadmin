$(document).ready(function(){	
	if( $('.has-datetimepicker').length ) 
	{
		$('.has-datetimepicker').datetimepicker();
	}	
	if( $('.has-datepicker').length )
	{
		$('.has-datepicker').datetimepicker({format: 'DD-MM-YYYY'});
    //$('.has-datepicker').datetimepicker({format: 'YYYY-MM-DD'});
	}
});

//common search
$(document).ready(function(){
    $("#myInput").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $("#myTable tr").filter(function() {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
       });
    });
});

	 function show_acd_dashboard(){

      window.location.href = "dashboard_academic";
    }

    function show_gt_dashboard(){

      window.location.href = "dashboard_gt";
    }

    function show_ee_dashboard(){

      window.location.href = "dashboard_ee";
    }