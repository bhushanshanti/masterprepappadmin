<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';    
     
class Student_login extends REST_Controller {
    
    public function __construct() {
       parent::__construct();
       $this->load->database();
       $this->load->model('Student_model');
    }
    
    //student login if token exist
    public function index_post(){
        
        $token  = trim($this->input->get_request_header('token'));       
        $mno    = trim($this->post('mno', TRUE));
        $countryCode = trim($this->post('countryCode', TRUE));
       
           if($countryCode){

                $countryCodeExist = $this->db->query("SELECT country_id FROM country where phonecode = '".$countryCode."' and active = 1 ");

                if($countryCodeExist->num_rows() > 0){
                    $mno2 = $countryCode.$mno;
                }else{
                    $data2['error_message'] = ["success" => 0, "message" => "Invalid Country code!"];            
                    return $this->set_response($data2, REST_Controller::HTTP_CREATED);
                }
                
            }else{
                $mno2='';
                $data2['error_message'] = ["success" => 0, "message" => "Please select Country code!"];            
                return $this->set_response($data2, REST_Controller::HTTP_CREATED);
            }

        if (!$this->Authenticate_login($token,$mno,$countryCode)) {
            $this->set_response(['status' => 0, 'message' => UNAUTHORIZED_LOGIN], REST_Controller::HTTP_NOT_FOUND); 
        }else{
           
        $query = $this->db->query("SELECT id FROM students where mobile='" . $mno . "' and active = 1 and token = '".$token."' ");
            if ($query->num_rows() > 0) {
                $userdata = $query->result();
                $token_new = $this->getTokens($mno);

                //update new token after success login
                $new_token_data = array('token' => $token_new);
                $this->db->update('students', $new_token_data, array('id' => $userdata[0]->id));

                $query2 = $this->db->query("SELECT 
                    s.id,s.fname,s.lname,s.dob,s.email,s.country_code,s.mobile,s.programe_id,s.test_module_id,s.token,s.profile_pic,pgm.programe_name
                    FROM students s 
                    LEFT JOIN programe_masters pgm ON pgm.programe_id = s.programe_id
                    where s.country_code = '". $countryCode."' and s.mobile = '". $mno."' and s.active = 1 and s.token = '".$token_new."' 
                ");
                if ($query2->num_rows() > 0) {
                    $userdata2 = $query2->result();
                }

                $data['error_message'] = ['success' => 1, "message" => VALID_LOGIN, 'userdetails' => $userdata2[0]];
            } else {
                $data['error_message'] = ['success' => 0, "message" => INVALID_LOGIN ];
            }

        $this->set_response($data, REST_Controller::HTTP_CREATED);
        }
        
    } 
    	
}