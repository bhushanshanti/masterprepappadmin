<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/

    use Restserver\Libraries\REST_Controller;
    defined('BASEPATH') OR exit('No direct script access allowed');
    require APPPATH . 'libraries/REST_Controller.php';    
     
class Student_otp_login extends REST_Controller {
    
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }
    /**
     * Get login with OTP from this method.(Reset new token)
     *
     * @return Response
    */
    public function index_post()
    {
        
        $mno     = trim($this->post('mno', TRUE));
        $hashkey = trim($this->post('hashkey', TRUE));
        $countryCode = trim($this->post('countryCode', TRUE));
            if($countryCode){

                $countryCodeExist = $this->db->query("SELECT country_id FROM country where phonecode = '".$countryCode."' and active = 1 ");

                if($countryCodeExist->num_rows() > 0){
                    $mno2 = $countryCode.$mno;
                }else{
                    $data2['error_message'] = ["success" => 0, "message" => "Invalid Country code!"];            
                    return $this->set_response($data2, REST_Controller::HTTP_CREATED);
                }
                
            }else{
                $mno2='';
                $data2['error_message'] = ["success" => 0, "message" => "Please select Country code!"];            
                return $this->set_response($data2, REST_Controller::HTTP_CREATED);
            }

        if (!$this->Authenticate_login2($mno,$countryCode)) {
            $this->set_response(['status' => 0, 'message' => UNAUTHORIZED_LOGIN], REST_Controller::HTTP_NOT_FOUND); 
        }else{
            //DO NOTHING
        }

        if($mno=='' || $hashkey==''){
            $data['error_message'] = ["success" => 0, "message" => "Something went wrong!Please Try again."];

        }else if($mno <> ""){
            $query = $this->db->query("SELECT id FROM students where mobile ='" . $mno . "' and active = 1");
        }else{}
        
        if ($query->num_rows() > 0) {

            $userdata = $query->result();
            $otp = rand(1000, 9000);
            $open  = '(';
            $close = ')';

            $update_otp_data = array('OTP' => $otp);
            $this->db->update('students', $update_otp_data, array('id' => $userdata[0]->id));

            define('OTP_MESSAGE', ' Your Masterprep Login OTP is: '.$open.$hashkey.$close.' ');
            $message = OTP_MESSAGE.$otp;
            $data1="username=".API_USERNAME."&password=".API_PASSWORD."&to=".$mno2."&from=".API_FROM."&text=".$message;
            $response = $this->curlpostdata(API_URL, $data1);

            $data['error_message'] = [ "success" => 1, "message" => "OTP has been sent.", 'hashkey' => $hashkey ];
            
        }else{
            $data['error_message'] = ["success" => 0, "message" => INVALID_LOGIN];
        }
        $this->set_response($data, REST_Controller::HTTP_CREATED);
    }
      
}