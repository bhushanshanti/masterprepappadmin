<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/

    use Restserver\Libraries\REST_Controller;
    defined('BASEPATH') OR exit('No direct script access allowed');
    require APPPATH . 'libraries/REST_Controller.php';
    
     
class Static_page extends REST_Controller {
    
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    
    public function __construct() {
       parent::__construct();
       $this->load->database();
       $this->load->model('Static_page_model');
    }

    public function index_get($category_id = 0, $pid = 0)
    {
        if (!$this->Authenticate($this->input->get_request_header('token'))) {
            $this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND); 
        }else{ 
            $category_id = $this->uri->segment(3);
            $pid = $this->uri->segment(4);
            $data2 = $this->Static_page_model->getStaticPageContents($pid,$category_id);
            
            if(!empty($data2)){
              $data['error_message'] = [ "success" => 1, "message" => "success","data" => $data2];
            }else{
              $data['error_message'] = [ "success" => 0, "message" => "Data not found","data" => $data2];
            }
            $this->set_response($data, REST_Controller::HTTP_CREATED);
        }
    } 
        
}