<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/
    use Restserver\Libraries\REST_Controller;
    defined('BASEPATH') OR exit('No direct script access allowed');
    require APPPATH . 'libraries/REST_Controller.php';    
     
class Instruction extends REST_Controller {    
    
    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model('Programe_master_model');
        $this->load->model('Category_master_model');
        $this->load->model('Instruction_master_model');      
    }       
    /**
     * Get active instruction for a category from this method.
     *
     * @return Response
    */
    public function index_get()
    {        
        if (!$this->Authenticate($this->input->get_request_header('token'))) {
            $this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND); 
        }else{             
            
            $category_id = $this->input->get_request_header('categoryId');
            $data2 = $this->Instruction_master_model->get_instruction($category_id);
            if(!empty($data2))
            $data['error_message'] = [ "success" => 1, "message" => "success", "data"=> $data2];
            else
            $data['error_message'] = [ "success" => 0, "message" => "No Instruction found!", "data"=> $data2];
            $this->response($data, REST_Controller::HTTP_OK);
        }
    }
        
}