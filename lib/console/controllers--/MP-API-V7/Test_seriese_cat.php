<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
    
class Test_seriese_cat extends REST_Controller {   
     
    public function __construct() {
        
       parent::__construct();
       $this->load->database();
       $this->load->model('Test_seriese_model');      
    }
       
    /**
     * Get All test category from this method.
     *
     * @return Response
    */
    public function index_get()
    {
        $token = $this->input->get_request_header('token');
        if (!$this->Authenticate($token)) {
            $this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND);    
        }else{ 

        $spData = $this->tokenRoutineCall($token);       
        $programe_id    = $spData['programe_id'];
        $test_seriese_id = $this->input->get_request_header('tsId');

        $tm = $this->Test_seriese_model->get_test_module_id($test_seriese_id);
        $test_module_id = $tm['test_module_id'];
        $is_MtEt = $tm['is_MtEt'];
        
        if($is_MtEt == 2){
            $this->db->select('
                ts_cat.`ts_cat_assoc_id`,
                ts_cat.`category_id`, 
                cat.`category_name`,
                ts_cat.`icon`,
                CONCAT(`totalquest`, " Q.") as totalQuest,
                CONCAT(`paper_duration`, " Min.") as paperDuration,
            ');
        }else{
            $this->db->select('
                ts_cat.`ts_cat_assoc_id`,
                ts_cat.`category_id`, 
                cat.`category_name`,
                ts_cat.`icon`
            ');
        }
        
        $this->db->from('`ts_cat_assoc` ts_cat');        
        $this->db->join('`category_masters` cat', 'ts_cat.`category_id`= cat.`category_id`', 'left');
        if($test_module_id == EE_ID){
            //for keeping common for both GT and ACD
            $this->db->where(array('ts_cat.test_seriese_id'=> $test_seriese_id));
            $this->db->order_by('cat.category_name','ASC');
        }else{
            $this->db->where(array('ts_cat.test_seriese_id'=> $test_seriese_id, 'cat.programe_id'=> $programe_id));
             $this->db->order_by('cat.modified','DESC');
        } 
        $data2 = $this->db->get('')->result_array(); 
         if(!empty($data2))
        $data['error_message'] = [ "success" => 1, "message" => "success", "data"=> $data2];
        else
            $data['error_message'] = [ "success" => 0, "message" => "No Test added in this! Please go back and try another test.", "data"=> $data2];      
            $this->response($data, REST_Controller::HTTP_OK);
        }
    }     
    
        
}