<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/

    use Restserver\Libraries\REST_Controller;
    defined('BASEPATH') OR exit('No direct script access allowed');
    require APPPATH . 'libraries/REST_Controller.php';    
     
class Package_history extends REST_Controller {    
	
    public function __construct() {
       parent::__construct();
       $this->load->database();
       $this->load->model('Package_master_model');
    }

    public function index_get()
    {        
        if(!$this->Authenticate($this->input->get_request_header('token'))) {
            $this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND);
        }else{

            $id    = $this->input->get_request_header('id');
            $spData = $this->idRoutineCall($id);       
            $programe_id    = $spData['programe_id'];
            $test_module_id = $spData['test_module_id'];

            $pageTitle= 'PAYMENT HISTORY';
            $tag = 'Billing details';
            
            $data2 = $this->Package_master_model->get_all_package_history($id,$programe_id,$test_module_id);        

            if(!empty($data2)){

                $data['error_message'] = 
                ["success" => 1, "pageTitle"=> $pageTitle, "tag"=> $tag,"message" => "data found", "data"=> $data2];

            }else{
                $data['error_message'] = 
                ["success" => 0, "pageTitle"=> $pageTitle, "tag"=> $tag, "message" => "No Payment History found!", "data"=> $data2];
            }
            $this->response($data, REST_Controller::HTTP_OK);
        }
    }
    	
}