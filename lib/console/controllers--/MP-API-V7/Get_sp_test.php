<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';   
     
class Get_sp_test extends REST_Controller {  
     
    public function __construct() {
       parent::__construct();
        $this->load->database();        
        $this->load->model('Test_seriese_model');
    }       
    /**
     * Get All sp test from this method.
     *
     * @return Response
    */
    public function index_get()
    {        
        $token = $this->input->get_request_header('token');
        if(!$this->Authenticate($token)) {
            $this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND); 
        }else{ 

            $question_type_id = $this->input->get_request_header('questionTypeId');       
            $spData = $this->tokenRoutineCall($token);       
            $programe_id    = $spData['programe_id'];
            $test_module_id = $spData['test_module_id'];

            $getStaticTest = $this->Test_seriese_model->getStaticTest($question_type_id,$test_module_id,$programe_id);

            if(empty($getStaticTest)){
                
                $data['error_message'] = [ "success" => 0, "message" => 'No test', "data"=> $getStaticTest ];
            }else{
                $data['error_message'] = [ "success" => 1, "message" => 'yes test', "data"=> $getStaticTest ];
            }            
            $this->response($data, REST_Controller::HTTP_OK);
        }
    }
        
}