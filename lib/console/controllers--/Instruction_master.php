<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/ 
class Instruction_master extends MY_Controller{
    
    function __construct()
    {
        parent::__construct();
        if (!$this->_is_logged_in()) {redirect('/');}
        $this->load->model('Instruction_master_model');        
    }
    /*
     * Listing of instruction
     */
    function index()
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $this->load->library('pagination');
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('instruction_master/index?');
        $config['total_rows'] = $this->Instruction_master_model->get_all_instruction_masters_count();
        $this->pagination->initialize($config);
        $data['title'] = 'Instruction';
        $data['instruction_masters'] = $this->Instruction_master_model->get_all_instruction_masters($params);        
        $data['_view'] = 'instruction_master/index';
        $this->load->view('layouts/main',$data);
    }
    
    /*
     * Adding a new instruction
     */
    function add()
    {   
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $data['title'] = 'Add instruction';
        $this->load->library('form_validation');
		$this->form_validation->set_rules('category_id','Category','required');
		$this->form_validation->set_rules('content','Content','required|trim');
		
		if($this->form_validation->run())     
        {   
            $user = $this->session->userdata('admin_login_data');
            foreach ($user as $d){$by_user=$d->id;}
            $params = array(
				'active' => $this->input->post('active'),
				'category_id' => $this->input->post('category_id'),
				'content' => $this->input->post('content'),
                'by_user' => $by_user,
            );
            $dup_count = $this->Instruction_master_model->check_duplicate_instruction_master($params['category_id']);
            if($dup_count>0){
                $this->session->set_flashdata('flsh_msg', DUP_MSG);
                redirect('instruction_master/add');
            }else{

                $id = $this->Instruction_master_model->add_instruction_master($params);
                if($id){
                    $this->session->set_flashdata('flsh_msg', SUCCESS_MSG);
                    redirect('instruction_master/index');
                }else{
                    $this->session->set_flashdata('flsh_msg', FAILED_MSG);
                    redirect('instruction_master/add');
                }
            
            }            
            
        }
        else
        {
			$this->load->model('Category_master_model');
			$data['all_category_masters'] = $this->Category_master_model->get_all_category_masters_active();            
            $data['_view'] = 'instruction_master/add';
            $this->load->view('layouts/main',$data);
        }
    }
    /*
     * Editing a instruction
     */
    function edit($id)
    {   
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $data['title'] = 'Edit instruction';
        $data['instruction_master'] = $this->Instruction_master_model->get_instruction_master($id);
        
        if(isset($data['instruction_master']['id']))
        {
            $this->load->library('form_validation');

			$this->form_validation->set_rules('category_id','Category','required');
			$this->form_validation->set_rules('content','Content','required|trim');
		
			if($this->form_validation->run())     
            {   
                $user = $this->session->userdata('admin_login_data');
                foreach ($user as $d){$by_user=$d->id;}
                $params = array(
					'active' => $this->input->post('active'),
					'category_id' => $this->input->post('category_id'),
					'content' => $this->input->post('content'),
                    'by_user' => $by_user,
                );

                $idd = $this->Instruction_master_model->update_instruction_master($id,$params);
                if($idd){
                    $this->session->set_flashdata('flsh_msg', UPDATE_MSG);           
                    redirect('instruction_master/index');
                }else{
                    $this->session->set_flashdata('flsh_msg', UPDATE_FAILED_MSG);           
                    redirect('instruction_master/edit/'.$id);
                }
            }
            else
            {
				$this->load->model('Category_master_model');
				$data['all_category_masters'] = $this->Category_master_model->get_all_category_masters_active();
                $data['_view'] = 'instruction_master/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error(ITEM_NOT_EXIST);
    }
    /*
     * Deleting instruction
     */
    function remove($id)
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $instruction_master = $this->Instruction_master_model->get_instruction_master($id);
        if(isset($instruction_master['id']))
        {
            $this->Instruction_master_model->delete_instruction_master($id);
            $this->session->set_flashdata('flsh_msg', DEL_MSG);
            redirect('instruction_master/index');
        }
        else
            show_error(ITEM_NOT_EXIST);
    }
    
}
