<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/ 
class Center_location extends MY_Controller{
    
    function __construct()
    {
        parent::__construct();
        if (!$this->_is_logged_in()) {redirect('/');}
        $this->load->model('Center_location_model');
        $this->load->model('Country_model');
        $this->load->model('State_model');
        $this->load->model('City_model');         
    }
    /*
     * Listing of centers
     */
    function index()
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $this->load->library('pagination');
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('center_location/index?');
        $config['total_rows'] = $this->Center_location_model->get_all_center_location_count();
        $this->pagination->initialize($config);
        $data['title'] = 'Our Branches';
        $data['center_location'] = $this->Center_location_model->get_all_center_location($params);
        $data['_view'] = 'center_location/index';
        $this->load->view('layouts/main',$data);
    }
    
    /*
     * Adding a new centers
     */
    function add()
    {   
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $data['title'] = 'Add center location';
            $this->load->library('form_validation');
            $this->form_validation->set_rules('center_name','Center name','required|trim');
            //$this->form_validation->set_rules('contact','contact','required|trim');
            $this->form_validation->set_rules('country_id','country_id','required');        
            $this->form_validation->set_rules('state_id','state_id','required');
            $this->form_validation->set_rules('city_id','city_id','required');
            $this->form_validation->set_rules('address_line_1','address','required|trim');
            //$this->form_validation->set_rules('zip_code','zip code','required|trim'); 
		if($this->form_validation->run())     
        {   
            $user = $this->session->userdata('admin_login_data');
            foreach ($user as $d){$by_user=$d->id;}
            $params = array(				
                'center_name' => $this->input->post('center_name'),
				'contact' => $this->input->post('contact'),
				'email' => $this->input->post('email'),
                'country_id' => $this->input->post('country_id'), 
                'state_id' => $this->input->post('state_id'), 
                'city_id' => $this->input->post('city_id'), 
                'zip_code' => $this->input->post('zip_code'),
                'address_line_1' => $this->input->post('address_line_1'), 
                'latitude' => $this->input->post('latitude'),
                'longitude' => $this->input->post('longitude'),	
                'active' => $this->input->post('active'),			
                'by_user' => $by_user,
            );            
            $idd = $this->Center_location_model->add_center_location($params);
            if($idd){
                $this->session->set_flashdata('flsh_msg', SUCCESS_MSG);
                redirect('center_location/index');
            }else{
                $this->session->set_flashdata('flsh_msg', FAILED_MSG);
                redirect('center_location/add');
            }
            
        }
        else
        {            
            $data['all_country_list'] = $this->Country_model->get_all_country_active();
            $data['_view'] = 'center_location/add';
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Edit centers
     */
    function edit($id)
    {   
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $data['title'] = 'Edit center location';
        $data['center_location'] = $this->Center_location_model->get_center_location($id);
        if(isset($data['center_location']['center_id']))
        {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('center_name','Center name','required|trim');
            //$this->form_validation->set_rules('contact','contact','required|trim');
            $this->form_validation->set_rules('country_id','country_id','required');        
            $this->form_validation->set_rules('state_id','state_id','required');
            $this->form_validation->set_rules('city_id','city_id','required');
            $this->form_validation->set_rules('address_line_1','address','required|trim');
            //$this->form_validation->set_rules('zip_code','zip code','required|trim'); 	
		
			if($this->form_validation->run())     
            {   
                $user = $this->session->userdata('admin_login_data');
                foreach ($user as $d){$by_user=$d->id;}
                $params = array(                
                    'center_name' => $this->input->post('center_name'),
                    'contact' => $this->input->post('contact'),
                    'email' => $this->input->post('email'),
                    'country_id' => $this->input->post('country_id'), 
                    'state_id' => $this->input->post('state_id'), 
                    'city_id' => $this->input->post('city_id'), 
                    'zip_code' => $this->input->post('zip_code'),
                    'address_line_1' => $this->input->post('address_line_1'), 
                    'latitude' => $this->input->post('latitude'),
                    'longitude' => $this->input->post('longitude'), 
                    'active' => $this->input->post('active'),           
                    'by_user' => $by_user,
                );  
                $id = $this->Center_location_model->update_center_location($id,$params);
                if($id){
                    $this->session->set_flashdata('flsh_msg', UPDATE_MSG);           
                    redirect('center_location/index');
                }else{
                    $this->session->set_flashdata('flsh_msg', UPDATE_FAILED_MSG);           
                    redirect('center_location/edit/'.$id);
                }
            }
            else
            {
                $data['all_country_list'] = $this->Country_model->get_all_country_active();
                $data['all_state_list'] = $this->State_model->get_state_list($data['center_location']['country_id']);
                $data['all_city_list'] = $this->City_model->get_city_list($data['center_location']['state_id']);
                $data['_view'] = 'center_location/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error(ITEM_NOT_EXIST);
    }   
    /*
     * Deleting centers
     */
    function remove($id)
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $center_location = $this->Center_location_model->get_center_location($id);
        if(isset($center_location['center_id']))
        {
            $this->Center_location_model->delete_center_location($id);
            $this->session->set_flashdata('flsh_msg', DEL_MSG);
            redirect('center_location/index');
        }
        else
            show_error(ITEM_NOT_EXIST);
    }    
    
}
