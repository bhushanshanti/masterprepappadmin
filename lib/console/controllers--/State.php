<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/
   class State extends MY_Controller{

    function __construct()
    {
        parent::__construct();
        if(!$this->_is_logged_in()) {redirect('/');}
        $this->load->model('State_model');
        $this->load->model('Country_model');        
    }    
    /*
     * Listing of all category
     */
    function index()
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $this->load->library('pagination');
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('state/index?');
        $config['total_rows'] = $this->State_model->get_all_state_count();
        $this->pagination->initialize($config);
        $data['state'] = $this->State_model->get_all_state($params);
        $data['title'] = 'States';
        $data['_view'] = 'state/index';
        $this->load->view('layouts/main',$data);
    }
    /*
     * Adding a new category
     */
    function add()
    {   
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $data['title'] = 'Add state';
        $this->load->library('form_validation');
		$this->form_validation->set_rules('state_name','State name','required|trim|is_unique[state.state_name]');
		$this->form_validation->set_rules('country_id','Country','required');        
		
		if($this->form_validation->run())     
        {   
            $user = $this->session->userdata('admin_login_data');
            foreach ($user as $d){$by_user=$d->id;}
            $params = array(
				'active' => $this->input->post('active'),
				'country_id' => $this->input->post('country_id'),
				'state_name' => $this->input->post('state_name'),                
                'by_user' => $by_user,
            );
            $dup = $this->State_model->dupliacte_state($params['country_id'],$params['state_name']);
            if($dup=='DUPLICATE'){
                $this->session->set_flashdata('flsh_msg', DUP_MSG);
                redirect('state/add');
            }else{
                $id = $this->State_model->add_state($params);
                if($id){
                    $this->session->set_flashdata('flsh_msg', SUCCESS_MSG);
                    redirect('state/index');
                }else{                    
                    $this->session->set_flashdata('flsh_msg', FAILED_MSG);
                    redirect('state/add');
                } 
            }
        }
        else
        {			
			$data['all_country_list'] = $this->Country_model->get_all_country_active();            
            $data['_view'] = 'state/add';
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a category_master
     */
    function edit($state_id)
    {   
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $data['title'] = 'Edit state';
        $data['state'] = $this->State_model->get_state($state_id);
        if(isset($data['state']['state_id']))
        {
            $this->load->library('form_validation');
			$this->form_validation->set_rules('state_name','State name','required|trim');
			$this->form_validation->set_rules('country_id','Country','required');           
		
			if($this->form_validation->run())     
            {   
                $user = $this->session->userdata('admin_login_data');
                foreach ($user as $d){$by_user=$d->id;}
                $params = array(
					'active' => $this->input->post('active'),
					'country_id' => $this->input->post('country_id'),
					'state_name' => $this->input->post('state_name'),                    
                    'by_user' => $by_user,
                );
                $id = $this->State_model->update_state($state_id,$params);
                if($id){
                    $this->session->set_flashdata('flsh_msg', UPDATE_MSG);           
                    redirect('state/index');
                }else{
                    $this->session->set_flashdata('flsh_msg', UPDATE_FAILED_MSG);           
                    redirect('state/edit/'.$category_id);
                }                
            }
            else
            {				
				$data['all_country_list'] = $this->Country_model->get_all_country();
                $data['_view'] = 'state/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error(ITEM_NOT_EXIST);
    } 

    /*
     * Deleting category_master
     */
    function remove($state_id)
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $state = $this->State_model->get_state($state_id);
        if(isset($state['state_id']))
        {
            $this->State_model->delete_state($state_id);
            $this->session->set_flashdata('flsh_msg', DEL_MSG);
            redirect('state/index');
        }
        else
            show_error(ITEM_NOT_EXIST);
    }

    
    
}
