<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/
   class Cron_tab extends MY_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('Package_master_model'); 
        $this->load->model('Db_model');              
    }    
    
    /*
     * std pack deactivation
     */
    function update_expiring_package()
    {
        $today=date('Y-m-d');
        $cronData = $this->Package_master_model->update_expiring_package($today);        
    }

    /*
     * send SMS to all Expired Package today
     */
    function sendSMS_toExpiredPackage()
    {        
        $expiredData = $this->Package_master_model->getExpiredPackage();
        foreach ($expiredData as $exp) {

            $fname  = $exp['fname'];
            $mobile = $exp['contact'];
            $email  = $exp['email'];
            $package_name  = $exp['package_name'];

            define('SMS_MESSAGE', 'Dear '.$fname.',%0a%0aYour '.$package_name.' package has expired on '.TODAY.'.%0a%0aPlease renew it or buy a new package.%0a%0aRegards: %0aTeam MasterPrep');

            $data_sms ="username=".API_USERNAME."&password=".API_PASSWORD."&to=".$mobile."&from=".API_FROM."&text=".SMS_MESSAGE;
            $response = $this->curlpostdata(API_URL, $data_sms);

            //mail
            $subject = PACK_EXPIRY;
            $mail_data = array(
                'fname'    => $fname,
                'package_name'  => $package_name, 
            );  
            $this->sendEmail_expiredPackage($email,$subject,$mail_data);           
        }
    }

    public function sendEmail_expiredPackage($email,$subject,$mail_data){
         
        $this->load->library('email');
        $this->email->set_mailtype("html");
        $this->email->set_newline("\r\n");
        $this->email->from(FROM_EMAIL, FROM_NAME);
        $this->email->to($email);
        //$this->email->bcc(ADMIN_EMAIL_BCC);
        $this->email->subject($subject);
        $body = $this->load->view('emails/sendEmail_expiredPackage.php' ,$mail_data,TRUE);
        $this->email->message($body);
        $this->email->send();
    } 

    /*
     * std pack deactivation
     */
    function updateMockHistory()
    {
        $today=date('Y-m-d');
        $cronData = $this->Package_master_model->updateMockHistory($today);      
    }

    /*
     * DB backup
     */
    function get_db_backup_mp()
    {
       
        $db = $this->Db_model->get_db();
        // Database configuration
        $host = $db['env'];
        $username = $db['db_user'];
        $password = $db['db_pwd'];
        $database_name = $db['db_name'];

        // Get connection object and set the charset
        $conn = mysqli_connect($host, $username, $password, $database_name);
        $conn->set_charset("utf8");

        // Get All Table Names From the Database
        $tables = array();
        $sql = "SHOW TABLES";
        $result = mysqli_query($conn, $sql);

        while ($row = mysqli_fetch_row($result)) {
            $tables[] = $row[0];
        }

        $sqlScript = "";
        foreach ($tables as $table) {
            
            // Prepare SQLscript for creating table structure
            $query = "SHOW CREATE TABLE $table";
            $result = mysqli_query($conn, $query);
            $row = mysqli_fetch_row($result);
            
            $sqlScript .= "\n\n" . $row[1] . ";\n\n";
            
            
            $query = "SELECT * FROM $table";
            $result = mysqli_query($conn, $query);
            
            $columnCount = mysqli_num_fields($result);
            
            // Prepare SQLscript for dumping data for each table
            for ($i = 0; $i < $columnCount; $i ++) {
                while ($row = mysqli_fetch_row($result)) {
                    $sqlScript .= "INSERT INTO $table VALUES(";
                    for ($j = 0; $j < $columnCount; $j ++) {
                        $row[$j] = $row[$j];
                        
                        if (isset($row[$j])) {
                            $sqlScript .= '"' . $row[$j] . '"';
                        } else {
                            $sqlScript .= '""';
                        }
                        if ($j < ($columnCount - 1)) {
                            $sqlScript .= ',';
                        }
                    }
                    $sqlScript .= ");\n";
                }
            }
            
            $sqlScript .= "\n"; 
        }

        if(!empty($sqlScript))
        {
            // Save the SQL script to a backup file
            $date_today = date('Y-m-d');
            $backup_file_name = $database_name . '_backup_' . $date_today . '-'.time().' .sql';
            $fileHandler = fopen($backup_file_name, 'w+');
            $number_of_lines = fwrite($fileHandler, $sqlScript);
            fclose($fileHandler); 

            // Download the SQL backup file to the browser
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename=' . basename($backup_file_name));
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($backup_file_name));
            if (ob_get_contents()) 
            ob_end_clean();
            if (ob_get_length()) 
            ob_end_clean();
            flush();
            readfile($backup_file_name);
            exec('rm ' . $backup_file_name); 
        }
    }
    
}
