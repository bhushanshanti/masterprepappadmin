<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/ 
class Role extends MY_Controller{
    
    function __construct()
    {
        parent::__construct();
        if (!$this->_is_logged_in()) {redirect('/');}
        $this->load->model('Role_model');        
    }
    /*
     * Listing of roles
     */
    function index()
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends 
        $this->load->library('pagination');
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('role/index?');
        $config['total_rows'] = $this->Role_model->get_all_roles_count();
        $this->pagination->initialize($config);
        $data['title'] = 'Roles';
        $data['roles'] = $this->Role_model->get_all_roles($params);        
        $data['_view'] = 'role/index';
        $this->load->view('layouts/main',$data);
    }      

    /*
     * Adding a new role
     */
    function add()
    {   
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        
        $data['title'] = 'Add Role';
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name','Role name','required|trim');      
        if($this->form_validation->run())     
        {   
            $user = $this->session->userdata('admin_login_data');
            foreach ($user as $d){$by_user=$d->id;}
            $params = array(
                'active' => $this->input->post('active'),
                'name' => $this->input->post('name'),
                'by_user' => $by_user,
            );            
            $id = $this->Role_model->add_role($params);
            if($id){
                $this->session->set_flashdata('flsh_msg', SUCCESS_MSG);
                redirect('role/index');
            }else{
                $this->session->set_flashdata('flsh_msg', FAILED_MSG);
                redirect('role/add');
            }            
        }
        else
        {            
            $data['_view'] = 'role/add';
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a role
     */
    function edit($id)
    {         
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        
        $data['title'] = 'Edit Role';
        $data['role'] = $this->Role_model->get_role($id);
        
        if(isset($data['role']['id']))
        {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('name','Role name','required|trim');      
            if($this->form_validation->run())     
            {   
                $user = $this->session->userdata('admin_login_data');
                foreach ($user as $d){$by_user=$d->id;}
                $params = array(
                    'active' => $this->input->post('active'),
                    'name' => $this->input->post('name'),
                    'by_user' => $by_user,
                );
                $idd = $this->Role_model->update_role($id,$params); 
                if($idd){
                    $this->session->set_flashdata('flsh_msg', UPDATE_MSG);           
                    redirect('role/index');
                }else{
                    $this->session->set_flashdata('flsh_msg', UPDATE_FAILED_MSG);           
                    redirect('role/edit/'.$id);
                } 
            }
            else
            {
                $data['_view'] = 'role/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error(ITEM_NOT_EXIST);
    }

    /*
     * Deleting role
     */
    function remove($id)
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        
        $role = $this->Role_model->get_role($id);
        if(isset($role['id']))
        {
            $this->Role_model->delete_role($id);
            $this->session->set_flashdata('flsh_msg', DEL_MSG);
            redirect('role/index');
        }
        else
            show_error(ITEM_NOT_EXIST);
    }

    /*
     * assgin task to role
     */
    function manage_role($id){

        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $data['title'] = 'Manage Role';
        $data['role'] = $this->Role_model->get_role($id);
        $data['roledata'] = $this->Role_model->get_role_data($id);
        if(isset($data['role']['id']))
        {            
            $this->load->library('form_validation');
            $this->form_validation->set_rules('ok','OK required','trim');      
            if($this->form_validation->run())     
            {  
                $this->Role_model->delete_role_access($id); 
                $user = $this->session->userdata('admin_login_data');
                foreach ($user as $d){$by_user=$d->id;}

                $cb_method[]     = $this->input->post('cb_method');
                
                foreach ($cb_method[0] as $c) {

                    $arr = explode("~", $c, 2);
                    $controller_id = $arr[0];
                    $method_id = $arr[1];
                    $params = array(
                        'role_id' => $id,
                        'controller_id'=>$controller_id,
                        'method_id' => $method_id,
                        'by_user' => $by_user,
                    );
                    $idd = $this->Role_model->add_access($params);
                }

                if($idd){
                    $this->session->set_flashdata('flsh_msg', UPDATE_MSG);           
                    redirect('role/manage_role/'.$id);
                }else{
                    $this->session->set_flashdata('flsh_msg', UPDATE_FAILED_MSG);
                    redirect('role/manage_role/'.$id);
                }

            }
            else
            {
                $cData = $this->Role_model->get_all_controller();
                foreach ($cData as $key => $c) {
                    $cData[$key]['Methods'] = $this->Role_model->get_all_methods($c['id']);
                }
                $data['controllers'] = $cData;
                $data['_view'] = 'role/manage_role'; 
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error(ITEM_NOT_EXIST);
    }

    function is_checked($role_id,$controller_id,$method_id){
        
        return $this->Role_model->check_duplicate_access($role_id,$controller_id,$method_id);        
    }    

    function manage_controller()
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $this->load->library('pagination');
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('role/manage_controller?');
        $config['total_rows'] = $this->Role_model->get_all_controller_count();
        $this->pagination->initialize($config);
        $data['title'] = 'Controllers';
        $data['all_controller_list'] = $this->Role_model->get_all_controller($params);        
        $data['_view'] = 'role/manage_controller';
        $this->load->view('layouts/main',$data);
    }

    function manage_controller_method()
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $this->load->library('pagination');
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('role/manage_controller_method?');
        $config['total_rows'] = $this->Role_model->get_all_method_count();
        $this->pagination->initialize($config);
        $data['title'] = 'Methods';
        $data['all_method_list'] = $this->Role_model->get_all_method_list($params);        
        $data['_view'] = 'role/manage_controller_method';
        $this->load->view('layouts/main',$data);
    }
    /*
     * Deleting controller
     */
    function remove_controller($id)
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        
        $role = $this->Role_model->get_controller($id);
        if(isset($role['id']))
        {
            $this->Role_model->delete_controller($id);
            $this->session->set_flashdata('flsh_msg', DEL_MSG);
            redirect('role/manage_controller');
        }
        else
            show_error(ITEM_NOT_EXIST);
    }

     /*
     * Deleting methods
     */
    function remove_method($id)
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        
        $role = $this->Role_model->get_method($id);
        if(isset($role['id']))
        {
            $this->Role_model->delete_method($id);
            $this->session->set_flashdata('flsh_msg', DEL_MSG);
            redirect('role/manage_controller');
        }
        else
            show_error(ITEM_NOT_EXIST);
    }

     /*
     * role fetch/update in db
     */
    function run_role(){
        
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends        
        $controllers = get_filenames( APPPATH . 'controllers', FALSE, TRUE); 
        foreach( $controllers as $k => $v )
        {
            if( strpos( $v, '.php' ) === FALSE)
            {
                unset( $controllers[$k] );
            }
        }
        foreach( $controllers as $controller )
        {
            if($controller=='Ckeditor.php' or $controller=='Login.php' or $controller=='Error_cl.php' or $controller=='Cron_tab.php' or $controller=='Db.php'){

            }else{

                $controller_data = $this->Role_model->check_controller($controller);
                if(count($controller_data)>0){
                    $id = $controller_data[0]['id'];
                    $params = array(
                        'controller_name ' => $controller,
                    );
                    $this->Role_model->update_controller($id,$params);
                    $controller_id=$id;

                }else{
                    $params = array(
                        'controller_name ' => $controller,
                    );
                    $controller_id = $this->Role_model->add_controller($params);
                }
                
            }
           
            include_once APPPATH . 'controllers/' . $controller;
            $methods = get_class_methods( str_replace( '.php', '', $controller ) );
            foreach( $methods as $method )
            {   
                if($method=='_has_access' or $method=='get_instance' or $method=='__construct' or $method=='_is_logged_in' or $method=='logout' or $method=='walk_dir' or $method=='CheckImgExt' or $method=='browse' or $method=='curlpostdata' or $method=='insert_notification' or $method=='sendEmail_paperChecked ') {
                    
                }else{

                    $method_data = $this->Role_model->check_method($method,$controller_id);
                    if(count($method_data)>0){
                    }else{
                        $params = array(
                            'controller_id'=>$controller_id,
                            'method_name ' => $method,
                        );
                        $this->Role_model->add_method($params);
                    }
                    
                }         
                
            }           
        }
        redirect('role/index');

    }
    
}
