<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/ 
class Notification_subject extends MY_Controller{
    function __construct()
    {
        parent::__construct();
        if (!$this->_is_logged_in()) {redirect('/');}
        $this->load->model('Notification_subject_model');       
    }
    /*
     * Listing of notification_subject
     */
    function index()
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $this->load->library('pagination');
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('notification_subject/index?');
        $config['total_rows'] = $this->Notification_subject_model->get_all_notification_subject_count();
        $this->pagination->initialize($config);
        $data['title'] = 'Notification subject';
        $data['notification_subject'] = $this->Notification_subject_model->get_all_notification_subject($params);        
        $data['_view'] = 'notification_subject/index';
        $this->load->view('layouts/main',$data);
    }

    /*
     * Adding a new notification_subject
     */
    function add()
    {   
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $data['title'] = 'Add notification_subject';
        $this->load->library('form_validation');
		$this->form_validation->set_rules('subject','Subject','required|trim');	
        $this->form_validation->set_rules('icon','Icon URL','trim');	
		if($this->form_validation->run())     
        {   
            $user = $this->session->userdata('admin_login_data');
            foreach ($user as $d){$by_user=$d->id;}
            $params = array(
				'active' => $this->input->post('active'),
				'subject' => $this->input->post('subject'),
                'icon' => $this->input->post('icon') ? $this->input->post('icon') : NULL,
                'by_user' => $by_user,
            );            
            $id = $this->Notification_subject_model->add_notification_subject($params);
            if($id){
                $this->session->set_flashdata('flsh_msg', SUCCESS_MSG);
                redirect('notification_subject/index');
            }else{
                $this->session->set_flashdata('flsh_msg', FAILED_MSG);
                redirect('notification_subject/add');
            }            
        }
        else
        {            
            $data['_view'] = 'notification_subject/add';
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a notification_subject
     */
    function edit($id)
    {         
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $data['title'] = 'Edit Notification subject';
        $data['notification_subject'] = $this->Notification_subject_model->get_notification_subject($id);
        
        if(isset($data['notification_subject']['id']))
        {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('subject','Subject','required|trim');
            $this->form_validation->set_rules('icon','Icon URL','trim');		
			if($this->form_validation->run())     
            {   
                $user = $this->session->userdata('admin_login_data');
                foreach ($user as $d){$by_user=$d->id;}
                $params = array(
					'active' => $this->input->post('active'),
					'subject' => $this->input->post('subject'),
                    'icon' => $this->input->post('icon') ? $this->input->post('icon') : NULL,
                    'by_user' => $by_user,
                );
                $idd = $this->Notification_subject_model->update_notification_subject($id,$params); 
                if($idd){
                    $this->session->set_flashdata('flsh_msg', UPDATE_MSG);           
                    redirect('notification_subject/index');
                }else{
                    $this->session->set_flashdata('flsh_msg', UPDATE_FAILED_MSG);           
                    redirect('notification_subject/edit/'.$id);
                } 
            }
            else
            {
                $data['_view'] = 'notification_subject/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error(ITEM_NOT_EXIST);
    }    
    /*
     * Deleting notification_subject
     */
    function remove($id)
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $notification_subject = $this->Notification_subject_model->get_notification_subject($id);
        if(isset($notification_subject['id']))
        {
            $this->Notification_subject_model->delete_notification_subject($id);
            $this->session->set_flashdata('flsh_msg', DEL_MSG);
            redirect('notification_subject/index');
        }
        else
            show_error(ITEM_NOT_EXIST);
    }
    
}
