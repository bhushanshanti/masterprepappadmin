<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/ 
class Dashboard_academic extends MY_Controller{

    function __construct()
    {
        parent::__construct();
        if (!$this->_is_logged_in()) {redirect('/');}        
        error_reporting(0); 
        $this->load->model('Programe_master_model');
        $this->load->model('Category_master_model');
        $this->load->model('Test_seriese_model');
        $this->load->model('Student_model');  
        $this->load->model('Student_results_model');
        $this->load->model('Role_model');
        $this->load->model('Center_location_model');
        $this->load->model('Package_master_model');
        $this->load->model('Test_module_model');        
    }

    function index()
    {        
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}        
        //access control ends
       
        $data['ec']    = $this->Student_model->get_all_enquiry_count();
        $data['vec']    = $this->Student_model->get_all_visa_enquiry_count();
        $data['sc']     = $this->Student_model->get_all_students_count();
        $data['tc']     = $this->Test_seriese_model->get_all_test_seriese_count();
        $data['ic']     = $this->Test_seriese_model->get_all_et_count();
        $data['mc']     = $this->Test_seriese_model->get_all_mock_test_count();
        $data['pkgc']   = $this->Package_master_model->get_all_package_masters_count();
        $data['tpc']    = $this->Student_results_model->checked_paper_count(); 
        $data['tpuc']   = $this->Student_results_model->unchecked_paper_count();

        $user = $this->session->userdata('admin_login_data');
        foreach ($user as $d){$by_user=$d->id;}
        $data['ac']   = $this->Student_results_model->assigned_paper_count($by_user); 
        
        $data['title'] = 'Dashboard (ACD)';
        $data['_view'] = 'dashboard_academic';
        $this->load->view('layouts/main',$data);
    }

    
}
