<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/
 
class User extends MY_Controller{
    
    function __construct()
    {
        parent::__construct();
        if (!$this->_is_logged_in()) {redirect('/');}
        $this->load->model('User_model');
        $this->load->model('Role_model');
        $this->load->model('User_role_model');
        $this->load->model('Gender_model');
    } 
    /*
     * Listing of all admin user
     */
    function index()
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends

        $this->load->library('pagination');
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('user/index?');
        $config['total_rows'] = $this->User_model->get_all_user_count();
        $this->pagination->initialize($config);
        $data['title'] = 'Admin';  
        $data['user'] = $this->User_model->get_all_user($params);
        $data['_view'] = 'user/index';
        $this->load->view('layouts/main',$data);
    }

    function profile(){

        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends

        $data['title'] = 'Profile';
        $data['_view'] = 'user/profile';
        $this->load->view('layouts/main',$data);
    }

    function check_old_pwd(){

        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends

        $data = $this->session->userdata('admin_login_data');
            foreach ($data as $d) { 
                $id = $d->id;
            }
        $params = array(
            'password'   => md5($this->input->post('old_pwd', true)),                
        );
        $res = $this->User_model->check_old_pwd($id,$params['password']);
        if($res){ 
                header('Content-Type: application/json');
                $response = ['msg'=>'', 'status'=>'true'];
                echo json_encode($response);
            }else{
                header('Content-Type: application/json');
                $response = ['msg'=>'', 'status'=>'false'];
                echo json_encode($response);
            } 

    }

    function change_password()
    { 
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $data['title'] = 'Change password'; 
        $this->load->library('form_validation');        
        $this->form_validation->set_rules('op','Old password','required|max_length[12]');
        $this->form_validation->set_rules('np','New password','required|max_length[12]'); 
        $this->form_validation->set_rules('rnp','re-enter new password','required|max_length[12]|matches[np]');           

        if($this->form_validation->run())  
        {
            $data = $this->session->userdata('admin_login_data');
            foreach ($data as $d) { 
                $id = $d->id;
            } 
            $params = array(                
                'password' => md5($this->input->post('np')),              
            );            
            $id = $this->User_model->change_password($id,$params);
            if($id){                 
                $this->session->set_flashdata('flsh_msg', PWD_CHANGE_SUCCESS_MSG);                
                redirect('login/logout');
            }else{
                $this->session->set_flashdata('flsh_msg', PWD_CHANGE_FAILED_MSG);
                redirect('user/profile');
            }            
        }
        else
        {  
            $data['_view'] = 'user/profile';
            $this->load->view('layouts/main',$data);
        }

    }
    /*
     * Adding a new user
     */
    function add()
    {   
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $data['title'] = 'Add admin'; 
        $this->load->library('form_validation');		
		$this->form_validation->set_rules('email','Email','required|valid_email|max_length[60]');
		$this->form_validation->set_rules('mobile','Mobile','max_length[10]');
        $this->form_validation->set_rules('role_id','Role','required');
        $this->form_validation->set_rules('gender_name','Gender','required');      
		
		if($this->form_validation->run())     
        {   
            $user = $this->session->userdata('admin_login_data');
            foreach ($user as $d){$by_user=$d->id;}
            $plain_pwd=time();
            $params = array(
				'active'    => $this->input->post('active'),
				'fname'     => ucfirst($this->input->post('fname')),
				'lname'     => ucfirst($this->input->post('lname')),
                'gender'     => $this->input->post('gender_name'),
				'dob'       => $this->input->post('dob'),
				'email'     => $this->input->post('email'),
				'mobile'    => $this->input->post('mobile'),				
				'residential_address'   => $this->input->post('residential_address'),
                'password'              => md5($plain_pwd),
                'by_user' => $by_user,
            );            
            $id = $this->User_model->add_user($params);
            if($id){
                $params2=array(
                    'user_id' => $id,
                    'role_id'=> $this->input->post('role_id'),
                );
                $rid = $this->User_role_model->add_user_role($params2);    
                if ($params['email'] <> "" and $rid) {

                    $subject='Dear User, your are registered at Masterprep';
                    $username=$params['email'];                    
                   
                    $data = array(
                        'name'=> $params['fname'].' '.$params['lname'],
                        'email_message'=>'You are registered successfully at Masterprep.your login details are as follows:',
                        'username'=> $params['email'],
                        'password'=>$plain_pwd,
                        'thanks'=>'Thanks and Regards',
                        'team'=>'Team: Masterprep',
                    );
                    $this->load->library('email');
                    $this->email->set_mailtype("html");
                    $this->email->set_newline("\r\n");
                    $this->email->from('info@masterprepapp.in', 'Masterprep'); 
                    $this->email->to($params['email']);
                    $this->email->bcc(ADMIN_EMAIL_BCC);
                    $this->email->subject($subject);
                    $body = $this->load->view('emails/welcome-email-admin.php',$data,TRUE);
                    $this->email->message($body);
                    $this->email->send();
                }else{

                }

                $this->session->set_flashdata('flsh_msg', SUCCESS_MSG);
                redirect('user/index');
            }else{
                $this->session->set_flashdata('flsh_msg', FAILED_MSG);
                redirect('user/add');
            }
            
        }
        else
        {            
            $data['all_roles'] = $this->Role_model->get_all_roles_active();
            $data['all_genders'] = $this->Gender_model->get_all_gender_active();
            $data['_view'] = 'user/add';
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a user
     */
    function edit($id)
    {
        $data2 = $this->session->userdata('admin_login_data');
        foreach ($data2 as $d) { $uid = $d->id;$role_name=$d->name;$role_id = $d->roleid;}
        if($uid==$id or $role_name=='Admin'){  
                
        }else{
            redirect('error_cl/index');
        }
        $data['title'] = 'Edit admin/profile'; 
        $data['user'] = $this->User_model->get_user($id);
        
        if(isset($data['user']['id']))
        {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('email','Email','required|valid_email|max_length[60]');
            $this->form_validation->set_rules('mobile','Mobile','max_length[10]');
            $this->form_validation->set_rules('role_id','Role','required');
            $this->form_validation->set_rules('gender_name','Gender','required');
		
			if($this->form_validation->run())     
            {   
                $user = $this->session->userdata('admin_login_data');
                foreach ($user as $d){$by_user=$d->id;}
                $params = array(

    				'active' => $this->input->post('active'),
                    'fname' => ucfirst($this->input->post('fname')),
                    'lname' => ucfirst($this->input->post('lname')),
                    'gender'     => $this->input->post('gender_name'),
                    'dob' => $this->input->post('dob'),
                    'email' => $this->input->post('email'),
                    'mobile' => $this->input->post('mobile'),               
                    'residential_address' => $this->input->post('residential_address'),
                    'by_user' => $by_user,
                );

                $idd = $this->User_model->update_user($id,$params);  
                if($idd){

                    if($role_name=='Admin'){
                        $params2=array(
                            'role_id'=> $this->input->post('role_id'),
                        );
                        $rid = $this->User_role_model->update_user_role($id,$params2);
                    }
                    
                    $this->session->set_flashdata('flsh_msg', UPDATE_MSG);
                    if($role_name=='Admin')          
                    redirect('user/index');
                    else
                    redirect('user/edit/'.$id);
                }else{
                    $this->session->set_flashdata('flsh_msg', UPDATE_FAILED_MSG);           
                    redirect('user/edit/'.$id);
                }          
                
            }
            else
            {
                $data['all_roles'] = $this->Role_model->get_all_roles_active();
                $data['user_role'] = $this->User_model->get_user_role($id);
                $data['all_genders'] = $this->Gender_model->get_all_gender_active();
                $data['_view'] = 'user/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error(ITEM_NOT_EXIST);
    }
    /*
     * Deleting user
     */
    function remove($id)
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $user = $this->User_model->get_user($id);
        if(isset($user['id']))
        {
            $this->User_model->delete_user($id);
            $this->session->set_flashdata('flsh_msg', DEL_MSG);
            redirect('user/index');
        }
        else
            show_error(ITEM_NOT_EXIST);
    }
    
}
