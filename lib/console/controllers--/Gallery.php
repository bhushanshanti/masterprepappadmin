<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/
 
class Gallery extends MY_Controller{
    
    function __construct()
    {
        parent::__construct();
        if (!$this->_is_logged_in()) {redirect('/');}
        $this->load->model('Gallery_model');        
    }
    /*
     * Listing of gallery
     */
    function index()
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $this->load->library('pagination');
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('gallery/index?');
        $config['total_rows'] = $this->Gallery_model->get_all_gallery_count();
        $this->pagination->initialize($config);
        $data['title'] = 'Gallery';
        $data['gallery'] = $this->Gallery_model->get_all_gallery($params);        
        $data['_view'] = 'gallery/index';
        $this->load->view('layouts/main',$data);
    }
    /*
     * Adding a new gallery
     */
    function add()
    {   
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $this->load->library('form_validation');
        $this->form_validation->set_rules('title','Title','required|trim');
        $data['title'] = 'Add gallery';
        if($this->form_validation->run())     
        {   
            $user = $this->session->userdata('admin_login_data');
            foreach ($user as $d){$by_user=$d->id;}
            $params = array(
                'active' => $this->input->post('active'),
                'image'  => $this->input->post('image'),
                'title'  => $this->input->post('title'),
                'by_user' => $by_user,
            );
            $config['upload_path']   = GALLERY_IMAGE_PATH;
            $config['allowed_types'] = GALLERY_ALLOWED_TYPES;
            $config['encrypt_name']  = FALSE;         
            $this->load->library('upload',$config);

                if($this->upload->do_upload("image")){
                    $data = array('upload_data' => $this->upload->data());
                    $image= $data['upload_data']['file_name'];
                    $params = array(
                        'active' => $this->input->post('active'),
                        'image' => $image,
                        'title' => $this->input->post('title'),
                        'by_user' => $by_user,
                    );
                    $id = $this->Gallery_model->add_gallery($params);
                    if($id){
                        $this->session->set_flashdata('flsh_msg', SUCCESS_MSG);
                        redirect('gallery/index');
                    }else{
                        $this->session->set_flashdata('flsh_msg', FAILED_MSG);
                        redirect('gallery/add');
                    }                    
                    
                }else{
                    $params = array(
                        'active' => $this->input->post('active'),
                        'title' => $this->input->post('title'),
                        'by_user' => $by_user,
                    );
                    $this->session->set_flashdata('flsh_msg', FAILED_MSG);
                    redirect('gallery/add');
                } 
        }
        else
        {            
            $data['_view'] = 'gallery/add';
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a gallery
     */
    function edit($id)
    { 
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $data['gallery'] = $this->Gallery_model->get_gallery($id);
        $data['title'] = 'Edit gallery';
        if(isset($data['gallery']['id']))
        {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('title','Title','required|trim'); 
            if($this->form_validation->run())     
            {   
                $user = $this->session->userdata('admin_login_data');
                foreach ($user as $d){$by_user=$d->id;}
                $params = array(
                    'active' => $this->input->post('active'),
                    'title' => $this->input->post('title'),
                    'by_user' => $by_user,
                );
                $config['upload_path']   = GALLERY_IMAGE_PATH;
                $config['allowed_types'] = GALLERY_ALLOWED_TYPES;
                $config['encrypt_name']  = FALSE;         
                $this->load->library('upload',$config);

                if($this->upload->do_upload("image")){
                    $data = array('upload_data' => $this->upload->data());
                    $image= $data['upload_data']['file_name'];
                    $params = array(
                        'active' => $this->input->post('active'),
                        'image' => $image,
                        'title' => $this->input->post('title'),
                        'by_user' => $by_user,
                    );                    
                }else{
                    $params = array(
                        'active' => $this->input->post('active'),
                        'title' => $this->input->post('title'),
                        'by_user' => $by_user,
                    );
                }
                $idd = $this->Gallery_model->update_gallery($id,$params);
                if($idd){
                    $this->session->set_flashdata('flsh_msg', UPDATE_MSG);           
                    redirect('gallery/index');
                }else{
                    $this->session->set_flashdata('flsh_msg', UPDATE_FAILED_MSG);           
                    redirect('gallery/edit/'.$id);
                }                
            }
            else
            {
                $data['_view'] = 'gallery/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error(ITEM_NOT_EXIST);
    } 

    /*
     * Deleting gallery
     */
    function remove($id)
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $gallery = $this->Gallery_model->get_gallery($id);
        if(isset($gallery['id']))
        {
            $this->Gallery_model->delete_gallery($id);
            $del_picture=$gallery['image'];
            unlink(GALLERY_IMAGE_PATH.$del_picture);
            $this->session->set_flashdata('flsh_msg', DEL_MSG);
            redirect('gallery/index');
        }
        else
            show_error(ITEM_NOT_EXIST);
    }

    function activate_deactivete()
    {  
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends

        $id = $this->input->post('id', true);
        $active = $this->input->post('active', true);
        $table = $this->input->post('table', true);
        $pk = $this->input->post('pk', true);
        
        if($active==1)
            $id = $this->Gallery_model->update_one($id, $active, $table, $pk);
        else
            $id = $this->Gallery_model->update_null($id, $active, $table, $pk);
        echo $id;       
    }
    
}
