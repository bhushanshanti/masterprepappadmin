<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/
ob_start();
?>
  <title> data</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

  <script type="text/javascript">
    $(document).ready(function(){
      $("img").toggleClass("img-responsive");
  });
  </script>
</head>
<body style="background-color: #F9E79F">
<?php
class Paper extends MY_Controller{

function __construct()
    {
        parent::__construct();
        if (!$this->_is_logged_in()) {redirect('/');}
        $this->load->model('Programe_master_model');
        $this->load->model('Category_master_model');
        $this->load->model('Question_type_model');
        $this->load->model('Test_seriese_model');
        $this->load->model('Ts_cat_assoc_model');
        $this->load->model('Section_model');        
        $this->load->model('Question_set_model');
        $this->load->model('Question_model');
        $this->load->model('Paper_model');              
    }
    /*function view2($test_seriese_id,$ts_cat_assoc_id){  
       
        $data['title'] = 'Paper preview';
        $bs_arr=array();$json=array();
        $bs_arr = $this->Paper_model->get_basic_details($test_seriese_id, $ts_cat_assoc_id);
        $aud_arr = $this->Paper_model->get_audio_details($test_seriese_id, $ts_cat_assoc_id);
        $paperData = $this->Paper_model->get_section_details($ts_cat_assoc_id);
            foreach ($paperData as $key => $sec) {
                $questionSetData = $this->Paper_model->get_question_sets_details($sec['section_id']);
                foreach ($questionSetData as $key2 => $qestSet) {                
                    $paperData[$key]['Sets'][$key2]=$qestSet;
                    $paperData[$key]['Sets'][$key2]['Questions'] = $this->Paper_model->get_questions_details($qestSet['question_sets_id']);
                }
               
            }

        $json['BasicDetails']=$bs_arr;
        foreach ($aud_arr as $a) {
           if($a['audio_file']=='' or $a['audio_file']==NULL){
                $json['AudioDetails']=array();
             }else{
                $json['AudioDetails']=$a;
             }
        }            
        $json['Sections']=$paperData;
        echo '<pre>';
        print_r($json);       
    }*/

    function view($test_seriese_id,$ts_cat_assoc_id){  
        
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends

        $data['title'] = 'Paper preview';
        $date = date("M d, Y h:i:s a");
        $data['basic_data'] = $this->Paper_model->get_basic_details($test_seriese_id, $ts_cat_assoc_id);
            foreach ($data['basic_data'] as $p) {
                echo '<div class="container" style="background-color:#F4D03F;">
                      <div class="jumbotron" style="background-color:#ECF0F1; ">
                        <h2>'.$p['test_seriese_name'].' | '.$p['test_seriese_type'].'</h2>      
                        <p>'.$p['programe_name'].' | '.$p['category_name'].' / '.'Duration: '.$p['paper_duration'].' '.PAPER_DURATION_UNIT.' | '.'Max. Marks: '.$p['max_marks'].'</p>
                        <p class="pull-right"><i>'.$date.'</i></p>
                      </div>                          
                    </div>';
                    
                    if($p['category_name']=='Listening' or $p['category_name']=='Listen'){
                        $audio_file =$p['audio_file'];
                        $audio_time =$p['audio_time'];
                        $totalquest =$p['totalquest'];

                        echo '<div class="jumbotron" style="background-color: #fff; ">
                        <video controls="" autoplay="" name="media">
                        <source src="'.$audio_file.'" type="audio/mpeg"></video>
                        <p>Audio duration: '.$p['audio_time'].'</p>
                        <p>Total Question on basis of Audio: '.$p['totalquest'].'</p>
                        </div>';
                    }else{
                        unset($audio_file);unset($audio_time);unset($totalquest);
                    }

            }

        $data['sections_data'] = $this->Paper_model->get_section_details2($ts_cat_assoc_id);
        $q="Q-";
        foreach ($data['sections_data'] as $sec) {

            if($sec['section_image']=='' or is_null($sec['section_image'])){

                $img='';
            }else{
                $img='<img class="img-rounded" src="'.$sec['section_image'].'" />';
            }
            $section_id = $sec['section_id'];
            
            echo '<div class="jumbotron">
                    <h5>'.$sec['section_heading'].'</h5>
                    <p>'.$sec['section_desc'].'</p>
                    <p>'.$sec['section_para'].'</p>
                    <p>'.$img.'</p>
                </div>';
                                
           
            $data['question_sets_data'] = $this->Paper_model->get_question_sets_details2($section_id);
            foreach ($data['question_sets_data'] as $qsd) {
               
                $question_sets_id = $qsd['question_sets_id'];

                 if($qsd['question_sets_image']=='' or is_null($qsd['question_sets_image'])){
                    $img='';
                }else{
                    $img='<img class="img-rounded" src="'.$qsd['question_sets_image'].'" />';
                }

                echo '<div style="font-weight:bold; background-color:#ECF0F2;padding: 20px;">
                <p>'.$qsd["question_sets_heading"].'</p>
                <p>'.$qsd["question_sets_desc"].'</p>
                <p>'.$qsd["question_sets_para"].'</p> 
                <p>'.$img.'</p>               
                </div>';
               
                $data['questions_data'] = $this->Paper_model->get_questions_details($question_sets_id);

                foreach ($data['questions_data'] as $qd) {

                    if($qd["question"]==''){

                    }else{
                        echo '<div style="margin:35px;background-color:#FFF;padding:20px;">
                        <p> '.$q.' '.$qd["question_no"].'  '.$qd["question"].'</p>
                        </div>';
                    }
                                      
                }
                
            }
        }

        echo '<footer class="page-footer font-small blue" style="background-color: #fff;">
        <div class="footer-copyright text-center py-3">© 2019 Copyright:
        <a href="'.site_url('/dashboard').'">'.MASTERPREP.'</a>
        </div>
        </footer>';

        $this->load->view('paper/view', $data);
    }

    //CURL REQUEST
    /*function view3($test_seriese_id,$ts_cat_assoc_id){
        
        $url='http://localhost.masterprep.com/api/Paper/'.$test_seriese_id.'/'.$ts_cat_assoc_id;
        //echo $url;exit;
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('token:abc'));
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
 
        $data['paper']=curl_exec($ch);        
        curl_close($ch);
        //echo '<pre>';
        //print_r($data);exit;       

        //$data['_view'] = 'paper/view';
        $this->load->view('paper/view',$data);

    }*/






}

