<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/
 
class Student_answer extends MY_Controller {
    
    function __construct()
    {
        parent::__construct();
        if (!$this->_is_logged_in()) {redirect('/');}
        $this->load->model('Student_answer_model');
        $this->load->model('Student_results_model');
        $this->load->model('Student_model');
        $this->load->model('Band_score_model');
        $this->load->model('Category_master_model');
        $this->load->model('Ts_cat_assoc_model');
        $this->load->model('Notification_model');
        $this->load->library('email');
        $this->email->set_mailtype("html");
        $this->email->set_newline("\r\n");
        $this->email->from(FROM_EMAIL, FROM_NAME);
    }

    function index($id)
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends

        $id = $this->uri->segment(3);
        $this->load->library('pagination');
        $params['limit'] = RECORDS_PER_PAGE;
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('student_answer/index/'.$id.'/?');
        $config['total_rows'] = $this->Student_answer_model->get_all_student_attempts_count($id);
        $this->pagination->initialize($config);
        $data['title'] = 'Paper attempt';
        $data['student_info'] = $this->Student_model->get_student_info($id);
        $data['student_attempts'] = $this->Student_answer_model->get_all_student_attempts($id,$params);        
        $data['_view'] = 'student_answer/index';
        $this->load->view('layouts/main',$data);
    }

    public function get_result($collection_no,$ts_cat_assoc_id,$student_id){

        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends

        $data['title']   = 'Result';
        $collection_no   = $this->uri->segment(3);
        $ts_cat_assoc_id = $this->uri->segment(4);
        $student_id      = $this->uri->segment(5);

        $this->load->library('pagination');
        $data['get_students'] = $this->Student_model->get_student_info($student_id);
        $data['get_result'] = $this->Student_answer_model->get_results($collection_no,$ts_cat_assoc_id);
        $data['get_sum'] = $this->Student_answer_model->get_sum($collection_no,$ts_cat_assoc_id);
        $data['ts'] = $this->Ts_cat_assoc_model->get_test_seriese_id($ts_cat_assoc_id);
        $data['_view'] = 'student_answer/get_result';
        $this->load->view('layouts/main',$data);
    }

    function assigned_list()
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends

        $user = $this->session->userdata('admin_login_data');
        foreach ($user as $d){$by_user=$d->id;}

        $id = $this->uri->segment(3);
        $this->load->library('pagination');
        $params['limit']  = RECORDS_PER_PAGE;
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('student_answer/assigned_list/'.$by_user.'/?');
        $config['total_rows'] = $this->Student_answer_model->get_all_student_attempts_count_ass($by_user);
        $this->pagination->initialize($config);
        $data['title'] = 'Paper attempt';
        $data['student_attempts'] = $this->Student_answer_model->get_all_student_attempts_ass($by_user,$params);        
        $data['_view'] = 'student_answer/assigned_list';
        $this->load->view('layouts/main',$data);       
    }

    public function update_marks(){             
        
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $data=array(
        'student_answers_id' => $this->input->post('id', true),
        'marks_secured'      => $this->input->post('value', true), 
        'collection_no'      => $this->input->post('collection_no', true),        
        );            

            $params=array(                
                'marks_secured'       => $data['marks_secured'],                
            ); 
            $id = $this->Student_answer_model->update_marks($data['student_answers_id'],$params);
            if($id){  
                header('Content-Type: application/json');
                $response = ['msg'=>MARKS_UPDATE_MSG, 'status'=>'true'];
                echo json_encode($response);
            }else{  
                header('Content-Type: application/json');
                $response = ['msg'=>MARKS_UPDATE_FAILED_MSG, 'status'=>'false'];
                echo json_encode($response);
            }
                        
    }

    public function update_remarks(){             
        
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $data=array(
        'student_answers_id' => $this->input->post('id', true),
        'checker_remarks'    => $this->input->post('value', true), 
        'collection_no'      => $this->input->post('collection_no', true),        
        );            

            $params=array(                
                'checker_remarks'       => $data['checker_remarks'],                
            ); 
            $id = $this->Student_answer_model->update_remarks($data['student_answers_id'],$params);
            if($id){  
                header('Content-Type: application/json');
                $response = ['msg'=>MARKS_UPDATE_MSG, 'status'=>'true'];
                echo json_encode($response);
            }else{  
                header('Content-Type: application/json');
                $response = ['msg'=>MARKS_UPDATE_FAILED_MSG, 'status'=>'false'];
                echo json_encode($response);
            }
            
    } 

    public function update_final_marks(){            
        
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends

        $user = $this->session->userdata('admin_login_data');
        foreach ($user as $d){$by_user=$d->id;}
        $paper_checked = 1;
        $data=array(
            'total'           => $this->input->post('total', true),
            'count'           => $this->input->post('count', true),        
            'collection_no'   => $this->input->post('collection_no', true),
            'by_user'         => $by_user,
            'category_name'   => $this->input->post('category_name', true),
            'test_seriese_id' => $this->input->post('test_seriese_id', true),
            'ts_cat_assoc_id' => $this->input->post('ts_cat_assoc_id', true),
            'max_marks'       => $this->input->post('max_marks', true),
            'student_id'      => $this->input->post('student_id', true),
            'programe_id'     => $this->input->post('programe_id', true),
        );

        $get_student_name=$this->Student_model->get_student_name($data['student_id']);
        $fname   = $get_student_name['fname'];
        $mobile  = $get_student_name['mobile'];
        $email   = $get_student_name['email'];
               
        $cat_data=$this->Category_master_model->get_category_id($data['category_name'],$data['programe_id']);       
        $category_id=$cat_data['category_id'];
        $test_module_id=$cat_data['test_module_id'];
        
            if($data['category_name']==READING or $data['category_name']==LISTENING){

                $bs_data=$this->Band_score_model->get_band_score_for_total($data['total'], $category_id);
                if($bs_data){
                    
                    $band_score = $bs_data['band_score'];
                } 
                else{ $band_score = NULL;}

                if($data['max_marks']==0 or is_null($data['max_marks'])){
                    $percentage = NULL;
                }else{                        
                    $per  = ($data['total']/$data['max_marks'])*100;
                    $percentage  = number_format($per, 1);
                }                
                $marks_secured = $data['total'];

            }elseif($data['category_name']==WRITING or $data['category_name']==SPEAKING){

                //echo $data['total'];
                //echo $data['count'];die;
                $band_score = number_format( ($data['total']/$data['count']),2);
                $percentage     = NULL;
                $marks_secured  = NULL;
                $student_id = $data['student_id'];
                $bs_data=$this->Band_score_model->get_pro_id($band_score, $category_id);
                $pro_id = $bs_data['pro_id'];
                if($data['category_name']==WRITING){
                    $this->insert_notification(WRITE_RESULT_NOTIFY, $student_id,$data['programe_id'],$test_module_id,$data['ts_cat_assoc_id'],$data['collection_no'],$paper_checked);
                }else{
                    $this->insert_notification(SPEAK_RESULT_NOTIFY, $student_id,$data['programe_id'],$test_module_id,$data['ts_cat_assoc_id'],$data['collection_no'],$paper_checked);
                }

            }else{

                $band_score = NULL;
                $percentage = NULL;
                $marks_secured = NULL;
            }
           $idd = $this->Student_results_model->update_result_marks($data['collection_no'], $marks_secured, $band_score, $percentage, $data['by_user'], $data['test_seriese_id'], $data['ts_cat_assoc_id'], $data['category_name'],$pro_id);

            if($idd){
                    
                    if($data['category_name']==WRITING){

                        define('SMS_MESSAGE', 'Dear '.$fname.',%0a%0a'.$data['collection_no'].'%0atest has been evaluated by the examiner. You can check your scores. %0a%0aRegards: %0aTeam MasterPrep');

                        if($email!=''){
                            $subject = $data['category_name'].'-'.TEST_PAPER_CHECKED;            
                            $mail_data = array(
                                'fname'         => $fname,
                                'collection_no' => $data['collection_no'],
                            );
                        }else{}
                    }else{

                        define('SMS_MESSAGE', 'Dear '.$fname.',%0a%0aYour speaking band scores are now available.%0aLog-in to check the same. %0a%0aRegards: %0aTeam MasterPrep');
                        if($email!=''){
                                $subject = $data['category_name'].'-'.TEST_PAPER_CHECKED;            
                                $mail_data = array(
                                    'fname'         => $fname,
                                    'collection_no' => $data['collection_no'],
                                );
                        }else{}
                    }
                    $data_sms ="username=".API_USERNAME."&password=".API_PASSWORD."&to=".$mobile."&from=".API_FROM."&text=".SMS_MESSAGE;
                    $response = $this->curlpostdata(API_URL, $data_sms);
                    $this->sendEmail_paperChecked($email,$subject,$mail_data); 

                header('Content-Type: application/json');
                $response = ['msg'=>MARKS_UPDATE_MSG, 'status'=>'true'];
                echo json_encode($response);
            }else{  
                header('Content-Type: application/json');
                $response = ['msg'=>MARKS_UPDATE_FAILED_MSG, 'status'=>'false'];
                echo json_encode($response);
            }
            
    }

    public function sendEmail_paperChecked($email,$subject,$mail_data){
         
        $this->email->to($email);
        $this->email->subject($subject);
        $body = $this->load->view('emails/paperChecked.php' ,$mail_data,TRUE);
        $this->email->message($body);
        $this->email->send();
    }

    /*
     * Deleting result
     */
    function remove($collection_no,$ts_cat_assoc_id,$student_id)
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        
        $this->Student_answer_model->remove_student_result($collection_no,$ts_cat_assoc_id,$student_id);
        $this->session->set_flashdata('flsh_msg', DEL_MSG);
        redirect('student_answer/index/'.$student_id);
       
    }

    public function recheck_paper(){            
        
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends

        $data=array(
            'collection_no' => $this->input->post('collection_no', true),       
        );             
        $id = $this->Student_answer_model->recheck_paper($data['collection_no']);
            
            if($id){  
                header('Content-Type: application/json');
                $response = ['msg'=>'UPDATED', 'status'=>'true'];
                echo json_encode($response);
            }else{  
                header('Content-Type: application/json');
                $response = ['msg'=>'NOT UPDATED', 'status'=>'false'];
                echo json_encode($response);
            }           
    } 


}
