<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/ 
class Country extends MY_Controller{
    
    function __construct()
    {
        parent::__construct();
        if (!$this->_is_logged_in()) {redirect('/');}
        $this->load->model('Country_model');        
    }
    /*
     * Listing of country
     */
    function index()
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $this->load->library('pagination');
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('country/index?');
        $config['total_rows'] = $this->Country_model->get_all_country_count();
        $this->pagination->initialize($config);
        $data['title'] = 'Countries';
        $data['country'] = $this->Country_model->get_all_country($params);
        $data['_view'] = 'country/index';
        $this->load->view('layouts/main',$data);
    }
    /*
     * Adding a new country
     */
    function add()
    {   
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $data['title'] = 'Add country';
        $this->load->library('form_validation');
		$this->form_validation->set_rules('name','Country name','required|trim|is_unique[country.name]');
		$this->form_validation->set_rules('iso3','ISO3 name','required|trim|is_unique[country.iso3]');
		$this->form_validation->set_rules('phonecode','Country code','required|trim');		
		
		if($this->form_validation->run())     
        {   
            $user = $this->session->userdata('admin_login_data');
            foreach ($user as $d){$by_user=$d->id;}
            $params = array(
				'active' => $this->input->post('active'),
                'name' => $this->input->post('name'),
				'iso3' => $this->input->post('iso3'),
				'phonecode' => $this->input->post('phonecode'),
                'phoneNo_limit' => $this->input->post('phoneNo_limit'), 
                'flag' => $this->input->post('flag'), 				
                'by_user' => $by_user,
            );            
            $idd = $this->Country_model->add_country($params);
            if($idd){
                $this->session->set_flashdata('flsh_msg', SUCCESS_MSG);
                redirect('country/index');
            }else{
                $this->session->set_flashdata('flsh_msg', FAILED_MSG);
                redirect('country/add');
            }            
        }
        else
        {            
            $data['_view'] = 'country/add';
            $this->load->view('layouts/main',$data);
        }
    }  

   
    function edit($country_id)
    {   
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $data['title'] = 'Edit country';
        $data['country'] = $this->Country_model->get_country($country_id);
        if(isset($data['country']['country_id']))
        {
            
            $this->load->library('form_validation');
            $this->form_validation->set_rules('name','Country Name','required|trim');
            $this->form_validation->set_rules('iso3','ISO3 name','required|trim');
            $this->form_validation->set_rules('phonecode','Country code','required|trim'); 	
		
			if($this->form_validation->run())     
            {   
                $user = $this->session->userdata('admin_login_data');
                foreach ($user as $d){$by_user=$d->id;}
                $params = array(
                    'active' => $this->input->post('active'),
                    'name' => $this->input->post('name'),
                    'iso3' => $this->input->post('iso3'),
                    'phonecode' => $this->input->post('phonecode'),
                    'phoneNo_limit' => $this->input->post('phoneNo_limit'), 
                    'flag' => $this->input->post('flag'),              
                    'by_user' => $by_user,
                ); 
                $id = $this->Country_model->update_country($country_id,$params);
                if($id){
                    $this->session->set_flashdata('flsh_msg', UPDATE_MSG);           
                    redirect('country/index');
                }else{
                    $this->session->set_flashdata('flsh_msg', UPDATE_FAILED_MSG);           
                    redirect('country/edit/'.$id);
                }
            }
            else
            {
                $data['_view'] = 'country/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error(ITEM_NOT_EXIST);
    }   
    /*
     * Deleting country
     */
    function remove($id)
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $country = $this->Country_model->get_country($id);
        if(isset($country['country_id']))
        {
            $this->Country_model->delete_country($id);
            $this->session->set_flashdata('flsh_msg', DEL_MSG);
            redirect('country/index');
        }
        else
            show_error(ITEM_NOT_EXIST);
    }    
    
}
