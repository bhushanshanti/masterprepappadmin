<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/ 
class Dashboard_ee extends MY_Controller{

    function __construct()
    {
        parent::__construct();
        if (!$this->_is_logged_in()) {redirect('/');}        
        error_reporting(0); 
        $this->load->model('Programe_master_model');
        $this->load->model('Category_master_model');
        $this->load->model('Test_seriese_model');
        $this->load->model('Student_model');  
        $this->load->model('Student_results_model');
        $this->load->model('Role_model');
        $this->load->model('Center_location_model');
        $this->load->model('Package_master_model');
        $this->load->model('Test_module_model');        
    }

    function index()
    {        
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}        
        //access control ends
       
        $data['ec']     = $this->Test_seriese_model->get_all_englis_essential_count();
        $user = $this->session->userdata('admin_login_data');
        foreach ($user as $d){$by_user=$d->id;}
        $data['ac']   = $this->Student_results_model->assigned_paper_count($by_user); 
        
        $data['title'] = 'Dashboard (Grammar)';
        $data['_view'] = 'dashboard_ee';
        $this->load->view('layouts/main',$data);
    }

    
}
