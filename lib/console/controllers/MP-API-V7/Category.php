<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';   
     
class Category extends REST_Controller {  
     
    public function __construct() {
       parent::__construct();
        $this->load->database();
        $this->load->model('Programe_master_model');
        $this->load->model('Category_master_model');
        $this->load->model('Test_module_model'); 
    }       
    /**
     * Get All category for test module as per programme from this method.
     *
     * @return Response
    */
    public function index_get()
    {    
        $token = $this->input->get_request_header('token');    
        if (!$this->Authenticate($token)) {
            $this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND); 
        }else{

            $spData = $this->tokenRoutineCall($token);       
            $programe_id    = $spData['programe_id'];
            $test_module_id = $spData['test_module_id'];       

            $this->db->select('`category_id`,`category_name`,`icon`');
            $this->db->from('category_masters cat');
            $this->db->join('programe_masters pgm', 'pgm.programe_id = cat.programe_id');
            $this->db->join('test_module tm', 'tm.test_module_id = cat.test_module_id');
            $this->db->where(array('cat.active' => 1,'cat.test_module_id' => $test_module_id,'cat.programe_id' => $programe_id));  
            $this->db->order_by('cat.category_id', 'DESC');
            $data2 =  $this->db->get()->result_array();
            //$this->response($data, REST_Controller::HTTP_OK);

            if(!empty($data2)){
              $data['error_message'] = [ "success" => 1, "message" => "success", "data"=> $data2];     
            }else{
                  $data['error_message'] = [ "success" => 0, "message" => "No category found!", "data"=> $data2];     
            }        
            $this->set_response($data, REST_Controller::HTTP_CREATED);
        }
    }     
    
        
}