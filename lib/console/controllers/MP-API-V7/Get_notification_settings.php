<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/

    use Restserver\Libraries\REST_Controller;
    defined('BASEPATH') OR exit('No direct script access allowed');
    require APPPATH . 'libraries/REST_Controller.php';    
     
class Get_notification_settings extends REST_Controller {    
	
    public function __construct() {
       parent::__construct();
       $this->load->database();       
       $this->load->model('Notification_model');
    }   

    public function index_get()
    {     
        $token = $this->input->get_request_header('token');    
        if (!$this->Authenticate($token)) {
            $this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND);
        }else{
            $id    = $this->input->get_request_header('id');
            $pageTitle= 'SETTINGS';
            $tag = 'Bridging gaps with us';

            $data2 = $this->Notification_model->get_notification_settings($id);
            if(!empty($data2)){

                $data['error_message'] = 
                ["success" => 1, "pageTitle"=> $pageTitle, "tag"=> $tag, "message" => "data found", "data"=> $data2];

            }else{
                $data['error_message'] = 
                ["success" => 0, "message" => "No Settings found!", "data"=> $data2];
            }
            $this->response($data, REST_Controller::HTTP_OK);
        }
    }
    	
}