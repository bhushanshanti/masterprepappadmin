<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/

    use Restserver\Libraries\REST_Controller;
    defined('BASEPATH') OR exit('No direct script access allowed');
    require APPPATH . 'libraries/REST_Controller.php';    
     
class Student extends REST_Controller {    
	
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }
    /**
     * Get Register from this method.
     *
     * @return Response
    */
    public function index_post()
    {        
        $mno     = trim($this->post('mno', TRUE));
        $countryCode = trim($this->post('countryCode', TRUE));

        if($countryCode!=''){

            $countryCodeExist = $this->db->query("SELECT country_id FROM country where phonecode = '".$countryCode."' and active = 1 ");            
            $countryData = $countryCodeExist->row_array();
            $country_id  = $countryData['country_id'];

            if($countryCodeExist->num_rows() > 0){
                $mno2 = $countryCode.$mno;
            }else{
                $data2['error_message'] = ["success" => 0, "message" => "Invalid Country code!"];            
                return $this->set_response($data2, REST_Controller::HTTP_CREATED);
            }
            
        }else{
            $mno2='';
            $data2['error_message'] = ["success" => 0, "message" => "Please select country code!"];           
            return $this->set_response($data2, REST_Controller::HTTP_CREATED);
        } 
        
        $hashkey = trim($this->post('hashkey', TRUE));
        if($mno=='' || $hashkey==''){
            $data['error_message'] = ["success" => 0, "message" => "Something went wrong! Try again."];

        }else if($mno <> ""){
            $query = $this->db->query("SELECT id FROM students where mobile='" . $mno . "' and active = 1");
        }
        
        if ($query->num_rows() <= 0) {

            $otp = rand(1000, 9000);
            $token = $this->getTokens($mno);

            $data = ['token' => $token, 'country_code'=> $countryCode, 'mobile' => $mno, 'OTP' => $otp, 'country_id'=> $country_id,];  
            $del_old_inactive_user = $this->db->query("DELETE FROM students where active = 0 and  mobile='" . $mno . "' ");
            $this->db->insert('students', $data);
            $last_insert_id = $this->db->insert_id();            

            $query = $this->db->query("SELECT id FROM students where mobile = '" . $mno . "' and token = '" . $token . "' order by created DESC limit 1");
            if ($query->num_rows() > 0){
                $userdata = $query->result();
                if ($mno <> "") {
                    
                    $open  = '(';
                    $close = ')';

                    define('OTP_MESSAGE', ' Thank you for registering with Masterprep '.$open.$hashkey.$close.' . Your OTP is : ');
                    $message = OTP_MESSAGE.$otp;
                    $data1="username=".API_USERNAME."&password=".API_PASSWORD."&to=".$mno2."&from=".API_FROM."&text=".$message;
                    $response = $this->curlpostdata(API_URL, $data1);
                }                               

                $data2['error_message'] = [ "success" => 1, "message" => "OTP has been sent !", 'hashkey'=>$hashkey];
            }
        }else{
                $data2['error_message'] = ["success" => 0, "message" => "You are already registered with Masterprep. Please login!"];
        }
        $this->set_response($data2, REST_Controller::HTTP_CREATED);
    }     
       
    /**
     * Get profile from this method.
     *
     * @return Response
    */
	public function index_get()
    {        
        $token = $this->input->get_request_header('token');        
        if (!$this->Authenticate($token)) {
            $this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND); 
        }else{ 
            $id = $this->input->get_request_header('id');
            if(!empty($id and !empty($token))){
                
                $query = $this->db->query("select
                    std.`programe_id`, 
                    std.`test_module_id`, 
                    std.`active`,
                    std.`country_code`,
                    std.`mobile`, 
                    std.`email`, 
                    std.`fname`,
                    std.`lname`,
                    std.`gender`, 
                    std.`dob`, 
                    std.`profile_pic`, 
                    std.`residential_address`, 
                    std.`mobile_notification`, 
                    std.`email_notification`,
                    date_format(std.created, '%D %M %y %r') as created,
                    date_format(std.modified, '%D %M %y %r') as modified,
                    cnt.country_id,
                    cnt.name as country_name,
                    cnt.flag
                    from `students` std 
                    LEFT JOIN country cnt ON std.country_id = cnt.country_id
                    where std.id = '".$id."' and std.active=1 and std.token='".$token."' ");
                if ($query->num_rows() > 0) {
                $userdata = $query->row_array();
                $data['error_message'] = ["success" => 1, "message" => "success", 'userdetails' => $userdata ];
                $this->response($data, REST_Controller::HTTP_OK);
                }else{
                    $this->set_response(['status' => 0, 'message' => 'Invalid user'], REST_Controller::HTTP_NOT_FOUND);
                }

            }else{
                $data['error_message'] = ["success" => 0, "message" => "profile not found !", 'userdetails' => array()];
                $this->response($data, REST_Controller::HTTP_OK);
            }
        }
        
    } 
     
    /**
     * Get update profile from this method.
     *
     * @return Response
    */
    public function index_put()
    {       
        $token = $this->input->get_request_header('token');
        if(!$this->Authenticate($token)) {
            $this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND); 
        }else{  
       
        $id = $this->input->get_request_header('id');    
        $input = $this->put();
        $profile_pic =  $input['profile_pic'];

        if($input['programe_id']==11){
            $programe_name= ACD;
        }elseif($input['programe_id']==10){
            $programe_name= GT;
        }else{
            $programe_name= NULL;
        }
        
        if($profile_pic!=''){

            $unique_id =  uniqid().'-'.$id;
            $img  = str_replace('data:image/jpeg;base64,', '', $profile_pic);
            $img  = str_replace(' ', '+', $img);
            $data = base64_decode($img);
            $file = PROFILE_PIC_FILE_PATH . $unique_id . '.jpg';
            $profile_pic_url = site_url($file);
            $success = file_put_contents($file, $data);

            //image resize start
            $config['image_library'] = 'gd2';
            $config['source_image']  = $file;
            $config['create_thumb']  = TRUE;
            $config['maintain_ratio'] = TRUE;
            $config['width']          = 75;
            $config['height']         = 75;
            $config['thumb_marker']   = '';
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();
            //image resize end            

                if($success){

                    $input = array(
                        'fname'                 => trim($input['fname']),
                        'lname'                 => trim($input['lname']),
                        'gender'                => $input['gender'],
                        'email'                 => trim($input['email']),
                        'dob'                   => $input['dob'],
                        'programe_id'           => $input['programe_id'],
                        'residential_address'   => $input['residential_address'],
                        'test_module_id'        => $input['test_module_id'],
                        'profile_pic'           => $profile_pic_url,
                    );
                    
                }else{

                    $profile_pic_url = 
                    site_url(PROFILE_PIC_FILE_PATH.'default_profile_pic.png');
                    $input = array(
                        'fname'                 => trim($input['fname']),
                        'lname'                 => trim($input['lname']),
                        'gender'                => $input['gender'],
                        'email'                 => trim($input['email']),
                        'dob'                   => $input['dob'],
                        'programe_id'           => $input['programe_id'],
                        'residential_address'   => $input['residential_address'],
                        'test_module_id'        => $input['test_module_id'],
                        'profile_pic'           => $profile_pic_url,
                    );            
                }
        
        }else{

            $input = array(
                'fname'                 => trim($input['fname']),
                'lname'                 => trim($input['lname']),
                'gender'                => $input['gender'],
                'email'                 => trim($input['email']),
                'dob'                   => $input['dob'],
                'programe_id'           => $input['programe_id'],
                'residential_address'   => $input['residential_address'],
                'test_module_id'        => $input['test_module_id'],
                //'profile_pic'           => $profile_pic_url,
            );
        }      
            
            if($this->db->update('students', $input, array('id'=>$id,'token'=>$token))){

                $data2['error_message'] = ['success' => 1, 'message' => "Profile updated successfully.", 'data'=> $input, 'programe_name'=>$programe_name];
                
            }else{
                $data2['error_message'] = ['success' => 0, 'message' => "Profile updation failed! Try again", 'data' => $input, 'programe_name'=>$programe_name ];
            }

        $this->set_response($data2, REST_Controller::HTTP_CREATED);
        }
        
    }     
    
    /*public function index_delete($id)
    {
        $id = $this->uri->segment(4);
        if($this->db->delete('students', array('id'=>$id))){
            $this->response(['student deleted successfully.'], REST_Controller::HTTP_OK);
        }else{
            $this->response('failed', REST_Controller::HTTP_OK);
        }   
        
    }*/
    	
}