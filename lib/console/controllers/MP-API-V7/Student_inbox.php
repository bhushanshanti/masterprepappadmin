<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';    
     
class Student_inbox extends REST_Controller {
    
public function __construct() {
    
    parent::__construct();
    $this->load->database();   
    
    $this->load->model('Notification_subject_model'); 
    $this->load->model('Notification_message_model');
    $this->load->model('Notification_model');
    $this->load->model('User_model');
    $this->load->model('Student_model'); 
}

public function index_get(){

    if (!$this->Authenticate($this->input->get_request_header('token'))) {
            $this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND); 
    }else{ 
            
            $id = $this->input->get_request_header('id');
            $spData = $this->idRoutineCall($id);       
            $programe_id    = $spData['programe_id'];
            $test_module_id = $spData['test_module_id'];      
            $inboxData = $this->Student_model->get_std_inbox($id,$programe_id,$test_module_id);

            $pageTitle= 'ENQUIRY INBOX';
            $tag = 'How can I help you?';

        if(!empty($inboxData)){
            $data['error_message'] = [ "success" => 1, "pageTitle"=> $pageTitle, "tag"=> $tag, "message" => "success", "data"=> $inboxData];   
        }else{
            $data['error_message'] = [ "success" => 0, "pageTitle"=> $pageTitle, "tag"=> $tag, "message" => "No Enquiry History!", "data"=> $inboxData];     
        }        
        $this->set_response($data, REST_Controller::HTTP_CREATED);
    }
}

}//class closed