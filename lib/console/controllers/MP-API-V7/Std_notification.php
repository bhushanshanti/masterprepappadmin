<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/

    use Restserver\Libraries\REST_Controller;
    defined('BASEPATH') OR exit('No direct script access allowed');
    require APPPATH . 'libraries/REST_Controller.php';    
     
class Std_notification extends REST_Controller {    
	
    public function __construct() {
       parent::__construct();
       $this->load->database();
       $this->load->model('Notification_subject_model');
       $this->load->model('Notification_message_model');
       $this->load->model('Notification_model');
    }

    public function index_get()
    { 
        if (!$this->Authenticate($this->input->get_request_header('token'))) {
            $this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND);
        }else{

            $id    = $this->input->get_request_header('id');
            $spData = $this->idRoutineCall($id);       
            $programe_id    = $spData['programe_id'];
            $test_module_id = $spData['test_module_id'];

            $pageTitle= 'NOTIFICATIONS';
            $tag = 'Keep yourself updated';

            $data2 = $this->Notification_model->get_all_notification_message($id,$test_module_id,$programe_id);           

            if(!empty($data2)){

                $data['error_message'] = 
                ["success" => 1, "pageTitle"=> $pageTitle, "tag"=> $tag, "message" => "data found", "data"=> $data2];

            }else{
                $data['error_message'] = 
                ["success" => 0, "pageTitle"=> $pageTitle, "tag"=> $tag, "message" => "Hooray! There is no message to read.", "data"=> $data2];
            }
            $this->response($data, REST_Controller::HTTP_OK);
        }
    }


    /**
     * Get update notification read status from this method.
     *
     * @return Response
    */
    public function index_put()
    {        
        
        if(!$this->Authenticate($this->input->get_request_header('token'))) {
            $this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND); 
        }else{
            $id     = $this->input->get_request_header('id');
            $notification_id  = $this->input->get_request_header('nid');      
            $input = $this->put();
            $input = array(
                'read_status'  => 1,             
            );       
            if($this->db->update('student_notification', $input, array('student_id'=>$id,'notification_id'=>$notification_id))){

                $data['error_message'] = ['success' => 1, "message" => "Updated read status"];
                
            }else{
                $data['error_message'] = ['success' => 0, "message" => "Updation failed! try again"];
            }
            $this->set_response($data, REST_Controller::HTTP_CREATED);
        }
        
    }
    	
}