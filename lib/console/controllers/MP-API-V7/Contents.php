<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';   
     
class Contents extends REST_Controller {  
     
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }       
    /**
     * Get All contents.
     *
     * @return Response
    */
    public function index_get()
    {        
        if (!$this->Authenticate($this->input->get_request_header('token'))) {
            $this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND); 
        }else{
            
            $title    = $this->input->get_request_header('title');
            $this->db->select('`sub_title`, `tag`,`description`');
            $this->db->from('contents');
            $this->db->where(array('active' => 1,'title'=>$title));
            $data2 =  $this->db->get()->result_array();

            if(!empty($data2)){

                $data['error_message'] = 
                ["success" => 1, "message" => "data found", "data"=> $data2];

            }else{
                $data['error_message'] = 
                ["success" => 0, "message" => "Contents not found!", "data"=> $data2];
            }
            $this->response($data, REST_Controller::HTTP_OK);
        }
    }     
    
        
}