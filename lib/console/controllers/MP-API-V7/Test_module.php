<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/

    use Restserver\Libraries\REST_Controller;
    defined('BASEPATH') OR exit('No direct script access allowed');
    require APPPATH . 'libraries/REST_Controller.php';
    
     
class Test_module extends REST_Controller {
    
      /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_get()
    {
        $token = $this->input->get_request_header('token');
        if (!$this->Authenticate()) {
            $this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND); 
        }else{
        $spData = $this->tokenRoutineCall($token);       
        $programe_id    = $spData['programe_id'];
        $query = $this->db->query("SELECT test_module_id, test_module_name FROM test_module WHERE active = 1 AND programe_id = '".$programe_id."'  "); 
        $data2 = $query->result_array();

            if(!empty($data2)){

                $data['error_message'] = [ "success" => 1, "message" => "success", 'test_module' => $data2 ];  
            }else{
                $data['error_message'] = [ "success" => 0, "message" => "data not found", 'test_module' => $data2 ];
            }           
            $this->response($data, REST_Controller::HTTP_OK);
        }
    }
            
}