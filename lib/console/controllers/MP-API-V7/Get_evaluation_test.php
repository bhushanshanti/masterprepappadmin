<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
    
class Get_evaluation_test extends REST_Controller {   
     
    public function __construct() {
        
       parent::__construct();
       $this->load->database();
       $this->load->model('Test_seriese_model');     
    }
       
    /**
     * Get Get_evaluation_test eligibility from this method.
     *
     * @return Response
    */
    public function index_get()
    {
        if(!$this->Authenticate($this->input->get_request_header('token'))) {
            $this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND); 
        }else{ 

            $id = $this->input->get_request_header('id');        
            $spData = $this->idRoutineCall($id);       
            $programe_id    = $spData['programe_id'];
            $test_module_id = $spData['test_module_id'];
            $get_et_eligibility = $this->Test_seriese_model->get_et_eligibility($id,$test_module_id,$programe_id);
            if(!empty($get_et_eligibility)){
                $student_result_id = $get_et_eligibility['student_result_id'];
                $eligble = 'No';
                $data['error_message'] = [ "success" => 0, "message" => $eligble, "data"=> [] ];
            }else{
                $eligble = 'Yes';
            } 

            if($eligble == 'Yes'){
                $et_data = $this->Test_seriese_model->get_et_test_id($id,$test_module_id,$programe_id);
                $data['error_message'] = [ "success" => 1, "message" => $eligble, "data"=> $et_data];
            }else{
                $data['error_message'] = [ "success" => 0, "message" => $eligble, "data"=> [] ];
            }             
            $this->response($data, REST_Controller::HTTP_OK);
        }
    }      
    
        
}