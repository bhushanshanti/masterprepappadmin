<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/

    use Restserver\Libraries\REST_Controller;
    defined('BASEPATH') OR exit('No direct script access allowed');
    require APPPATH . 'libraries/REST_Controller.php';
    
     
class Test_seriese extends REST_Controller {
    
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }
       
    /**
     * Get All test seriese from this method.
     *
     * @return Response
    */
    public function index_get()
    {
        $token = $this->input->get_request_header('token');
        if (!$this->Authenticate($token)) {
            $this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND);
        }else{  
            
            $spData = $this->tokenRoutineCall($token);       
            $programe_id    = $spData['programe_id'];
            $test_module_id = $spData['test_module_id'];
            $subTest = $this->input->get_request_header('subTest');

            if($test_module_id == IELTS_ID){

                if($subTest=='MOCK'){ 
                    $is_MtEt = 2;
                }elseif($subTest=='GRAMMAR'){
                    $test_module_id = EE_ID; 
                    $is_MtEt = NULL;       
                }else{
                    $is_MtEt = NULL;
                }
                           
            }elseif($test_module_id == PTE_ID){
                
                if($subTest=='MOCK'){ 
                    $is_MtEt = 2;
                }elseif($subTest=='GRAMMAR'){
                    $test_module_id = EE_ID; 
                    $is_MtEt = NULL;       
                }else{
                    $is_MtEt = NULL;
                }

            }elseif($test_module_id == TOEFL_ID){

                if($subTest=='MOCK'){ 
                    $is_MtEt = 2;
                }elseif($subTest=='GRAMMAR'){
                    $test_module_id = EE_ID; 
                    $is_MtEt = NULL;       
                }else{
                    $is_MtEt = NULL;
                }

            }elseif($test_module_id == GMAT_ID){

                if($subTest=='MOCK'){ 
                    $is_MtEt = 2;
                }elseif($subTest=='GRAMMAR'){
                    $test_module_id = EE_ID; 
                    $is_MtEt = NULL;       
                }else{
                    $is_MtEt = NULL;
                }

            }else{
                $test_module_id= 0;
            }
            $this->db->select('`test_seriese_id`, `test_seriese_name`, `test_seriese_type`');
            $this->db->from('`test_seriese`'); 
            if($test_module_id == EE_ID ) {
                $this->db->where(array('active' => 1, 'test_module_id' => $test_module_id));
            }else{
                $this->db->where(array('active' => 1, 'test_module_id' => $test_module_id, 'programe_id' => $programe_id, 'is_MtEt'=> $is_MtEt));
            }
            $this->db->order_by('display_order ASC');       
            $data2 = $this->db->get('')->result_array();
            if(!empty($data2))
                $data['error_message'] = [ "success" => 1, "message" => "success", "data"=> $data2];
            else
                $data['error_message'] = [ "success" => 0, "message" => "No any Test found! Pleas Go back.", "data"=> $data2];
            $this->response($data, REST_Controller::HTTP_OK);       
        }
    }   
        
}