<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/  
   
    use Restserver\Libraries\REST_Controller;
    defined('BASEPATH') OR exit('No direct script access allowed');
    require APPPATH . 'libraries/REST_Controller.php';    
     
class Papernew extends REST_Controller {
    
    public function __construct() {

        parent::__construct();
        $this->load->database();
        $this->load->model('Programe_master_model');
        $this->load->model('Category_master_model');
        $this->load->model('Question_type_model');
        $this->load->model('Test_seriese_model');
        $this->load->model('Ts_cat_assoc_model');
        $this->load->model('Section_model');        
        $this->load->model('Question_set_model');
        $this->load->model('Question_model');
        $this->load->model('Paper_model');    
    }
       
    /**
     * Get paper from this method.
     *
     * @return Response
    */
    public function index_get()
    {        
        if (!$this->Authenticate($this->input->get_request_header('token'))) {
            $this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND); 
        }else{ 
            
            $json=array();
            $bs_arr = $this->Paper_model->get_basic_details2($this->input->get_request_header('tsId'), $this->input->get_request_header('tsCatId'));
            $paperData = $this->Paper_model->get_section_details2($this->input->get_request_header('tsCatId'));
                foreach ($paperData as $key => $sec) {
                    $questionSetData = $this->Paper_model->get_question_sets_details($sec['section_id']);
                    foreach ($questionSetData as $key2 => $qestSet) {                
                        $paperData[$key]['Sets'][$key2]=$qestSet;

                        $paperData[$key]['Sets'][$key2]['Questions'] = $this->Paper_model->get_questions_details($qestSet['question_sets_id']);

                        foreach ($paperData[$key]['Sets'][$key2]['Questions'] as $key3 => $opt) {

                            $opt['options']=$this->Paper_model->get_questions_details_opt($opt['question_id']);
                            $paperData[$key]['Sets'][$key2]['Questions'][$key3]=$opt;
                        }
                }                  
            }
            $json['BasicDetails']=$bs_arr;
            $json['Sections']=$paperData;

            $data2['BasicDetails'] =$bs_arr;
            $data2['Sections'] =$paperData;
           
            if(!empty($bs_arr) and !empty($paperData)){
                 $data['error_message'] = [ "success" => 1, "message" => "success","data" => $data2 ];
            }else{
                    $data['error_message'] = [ "success" => 0, "message" => "Failed","data" => [] ];
            }           
            $this->response($data, REST_Controller::HTTP_OK);
        }
    } 
        
}