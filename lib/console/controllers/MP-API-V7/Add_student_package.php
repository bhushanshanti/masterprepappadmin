<?php
//7.3.13
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/
    use Restserver\Libraries\REST_Controller;
    defined('BASEPATH') OR exit('No direct script access allowed');
    require APPPATH . 'libraries/REST_Controller.php';    
    require_once APPPATH . 'libraries/razorpay-php/Razorpay.php';
    use Razorpay\Api\Api;
     
class Add_student_package extends REST_Controller { 
    
    public function __construct() {

       error_reporting(0);
       parent::__construct();
       $this->load->database();
       $this->load->model('Package_master_model');
       $this->load->model('Student_model'); 
       $this->load->model('Notification_model');       
    }
    /**
     * Get payment capture from this method.
     *
     * @return Response
    */
    public function index_post()
    {    
        if(!$this->Authenticate($this->input->get_request_header('token'))) {
            $this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND);
        }else{

            $id = $this->input->get_request_header('id');
            $spData = $this->idRoutineCall($id);       
            $programe_id    = $spData['programe_id'];
            $test_module_id = $spData['test_module_id'];

            $package_id = $this->input->get_request_header('package');
            $amount = $this->input->get_request_header('amount');
            $currency = $this->input->get_request_header('currency');
            $payment_id = $this->input->get_request_header('paymentId');      
            $code = $this->input->get_request_header('code');
            $keyId = $this->input->get_request_header('keyId');
            $amount = $amount*100;   
            if(isset($payment_id) and $code == 1 and $keyId == KEY_ID){
                $this->capture_payment($id,$programe_id,$test_module_id,$package_id,$keyId,$amount,$currency,$payment_id);
            }elseif($keyId == KEY_ID and ($payment_id == '' or $code == 0) ){
                $this->payment_cancelled($id,$programe_id,$test_module_id,$package_id,$keyId,$amount,$currency,$payment_id);
            }elseif($keyId != KEY_ID and ($payment_id == '' or $code == 0) ){                
                
                $subject = PAYMENT_UT;
                $mail_data = array(
                        'payment_id'    => 'NILL',               
                        'amount'        => $amount,
                        'currency'      => $currency,
                        'student_id'    => $id,                 
                );  
                $this->sendEmail_ut(ADMIN_EMAIL_CC,$subject,$mail_data);
                $data['error_message'] =
                [ 'success' => 0, 'short_message'=> 'Un-Authenticated Transaction!', 'message'=>'Oops! Your payment failed. Try again.', 'order_id'=> '' ];
                $this->set_response($data, REST_Controller::HTTP_CREATED);
            }else{
                
               
                $data['error_message'] =
                [ 'success' => 0, 'short_message'=> 'Payment not attempted!', 'message'=>'Oh! You have not attempted for payment. Please try again.', 'order_id'=> '' ];
                $this->set_response($data, REST_Controller::HTTP_CREATED);

            }           
        }        
    }

    //capture_payment
    public function capture_payment($id,$programe_id,$test_module_id,$package_id,$keyId,$amount,$currency,$payment_id){
            
            try{
                
                $api = new Api(KEY_ID, KEY_SECRET);
                $payment = $api->payment->fetch($payment_id);
                $payment_captured_data_mp = $payment->capture(array('amount' => $amount, 'currency' => $currency));
                
                $pid = $payment_captured_data_mp->id;
                $email = $payment_captured_data_mp->email;
                $contact = $payment_captured_data_mp->contact;                
                $method = $payment_captured_data_mp->method;
                $captured = $payment_captured_data_mp->captured;
                $status = $payment_captured_data_mp->status;                
                $currency = $payment_captured_data_mp->currency;
                $created_at = $payment_captured_data_mp->created_at;

                $package_data = $this->Package_master_model->get_package_master($package_id); 
                $duration = $package_data['duration'];
                $package_name = $package_data['package_name'];
                $test_paper_limit =  $package_data['test_paper_limit'];

                $duration = $duration.' days';
                $today = date('Y-m-d');
                $datee=date_create($today);
                date_add($datee,date_interval_create_from_date_string($duration));
                $expired_on = date_format($datee,"Y-m-d");
                $order_id = $this->getorderTokens(6);
                $order_id = time().$order_id;

                $params = array(
                    'student_id'    => $id,
                    'programe_id'   => $programe_id,
                    'test_module_id'=> $test_module_id, 
                    'contact'       => $contact,
                    'email'         => $email,
                    'package_id'    => $package_id,                
                    'payment_id'    => $pid,
                    'order_id'      => $order_id,
                    'amount'        => $amount,
                    'currency'      => $currency,
                    'status'        => $status,
                    'captured'      => $captured, 
                    'method'        => $method,
                    'paper_left'    => $test_paper_limit,
                    'active'        => 1,
                    'created_at'    => $created_at,
                    'subscribed_on' => $today,
                    'expired_on'    => $expired_on,
                );              
                $this->db->insert('student_package', $params);
                $amount  =  $amount/100;
                $amount  = number_format($amount, 2);
                
                $ts_cat_assoc_id=0;
                $collection_no = 'IELTS-package-subscription-success';
                $paper_checked= NULL;
                $this->insert_notification(IELTS_PACKAGE_SUBS_NOTIFY,$id,$programe_id,$test_module_id,$ts_cat_assoc_id,$collection_no,$paper_checked);

                $std_data= $this->Student_model->get_student_info_forSMS($id);
                $fname   = $std_data['fname'];
                $mobile  = $std_data['mobile'];
                $email   = $params['email'];
                
                define('SMS_MESSAGE', 'Dear '.$fname.'%0a%0aThanks! Your '.$package_name.' costing '.$amount.' '.$currency.' has been subscribed successfully.%0a%0aNow you are able to explore '.$test_paper_limit.' Tests for '.$duration.' . %0a%0aRegards: %0aTeam Masterprep');
                $data_sms ="username=".API_USERNAME."&password=".API_PASSWORD."&to=".$mobile."&from=".API_FROM."&text=".SMS_MESSAGE;
                $response = $this->curlpostdata(API_URL, $data_sms);
                
                if($email){
                    $subject = PAYMENT_SUC;            
                    $mail_data = array(
                            'fname'         => $fname,
                            'package_name'  => $package_name,                
                            'amount'        => $amount,
                            'currency'      => $currency,
                            'test_paper_limit' => $test_paper_limit,
                            'duration'      => $duration, 
                            'subscribed_on' => $today,
                            'expired_on'    => $expired_on,  
                            'order_id'      => $order_id,
                    );  
                    $this->sendEmail_success($email,$subject,$mail_data);
                }
                
                $data['error_message'] =
                    [ 
                    'success' => 1, 'short_message'=> 'Payment successfull',
                    'message'=>'Thanks! Your Payment for '.$currency.' '.$amount.' has been done successfully. Now Your '.$package_name.' pack is unlocked.', 
                    'order_id' => $order_id ];
                
            }catch (Exception $e) {
                
                $data['error_message'] =
                [ 'success' => 0, 'short_message'=> 'Exception', 'message'=>$e->getMessage(), 'order_id' => '' ];
            }
            $this->set_response($data, REST_Controller::HTTP_CREATED);
    }


    //payment_cancelled
    public function payment_cancelled($id,$programe_id,$test_module_id,$package_id,$keyId,$amount,$currency,$payment_id){

            $package_data = $this->Package_master_model->get_package_master($package_id); 
            $package_name = $package_data['package_name'];
            
            $std_data= $this->Student_model->get_student_info_forSMS($id);
            $fname   = $std_data['fname'];
            $mobile  = $std_data['mobile'];
            $email   = $std_data['email'];

            $params = array(
                'student_id'    => $id,
                'programe_id'   => $programe_id,
                'test_module_id'=> $test_module_id,
                'contact'       => $mobile,
                'email'         => $email,
                'package_id'    => $package_id,                
                'payment_id'    => $payment_id,
                'amount'        => $amount,
                'currency'      => $currency,                    
                'active'        => 0,            
            );              
            $this->db->insert('student_package', $params);
            $ts_cat_assoc_id=0;
            $collection_no = 'IELTS-package-subscription-failed';
            $paper_checked= NULL;
            $this->insert_notification(IELTS_PACKAGE_SUBS_FLD_NOTIFY,$id,$programe_id,$test_module_id,$ts_cat_assoc_id,$collection_no,$paper_checked);

            define('SMS_MESSAGE', 'Dear '.$fname.',%0a%0aYour '.$package_name.' package subscription failed due to some technical issue. Please try again. %0a%0aRegards: %0aTeam Masterprep');

            $data_sms ="username=".API_USERNAME."&password=".API_PASSWORD."&to=".$mobile."&from=".API_FROM."&text=".SMS_MESSAGE;
            $response = $this->curlpostdata(API_URL, $data_sms);
            
            if($email){
                $subject = PAYMENT_FLD;
                $mail_data = array(
                        'fname'         => $fname,
                        'package_name'  => $package_name,                
                        'amount'        => $amount,
                        'currency'      => $currency,                   
                );  
                $this->sendEmail_failed($email,$subject,$mail_data);
            }
            
            $data['error_message'] =
                [ 'success' => 0, 'short_message'=> 'Payment cancelled', 'message'=>'Oops! Your Payment cancelled! Please try again',  'order_id' => '' ];                
            $this->set_response($data, REST_Controller::HTTP_CREATED);
    }  


    public function sendEmail_failed($email,$subject,$mail_data){
         
        $this->load->library('email');
        $this->email->set_mailtype("html");
        $this->email->set_newline("\r\n");
        $this->email->from(FROM_EMAIL, FROM_NAME);
        $this->email->to($email);
        $this->email->bcc(PAYMENT_INFO_ADMIN);
        $this->email->subject($subject);
        $body = $this->load->view('emails/payment-fail.php' ,$mail_data,TRUE);
        $this->email->message($body);
        $this->email->send();
    } 
    
    public function sendEmail_success($email,$subject,$mail_data){
         
        $this->load->library('email');
        $this->email->set_mailtype("html");
        $this->email->set_newline("\r\n");
        $this->email->from(FROM_EMAIL, FROM_NAME);
        $this->email->to($email);
        $this->email->bcc(PAYMENT_INFO_ADMIN);
        $this->email->subject($subject);
        $body = $this->load->view('emails/payment-success.php' ,$mail_data,TRUE);
        $this->email->message($body);
        $this->email->send();
    } 
    
    public function sendEmail_ut($email,$subject,$mail_data){
         
        $this->load->library('email');
        $this->email->set_mailtype("html");
        $this->email->set_newline("\r\n");
        $this->email->from(FROM_EMAIL, FROM_NAME);
        $this->email->to($email);
        $this->email->subject($subject);
        $body = $this->load->view('emails/payment-ut.php' ,$mail_data,TRUE);
        $this->email->message($body);
        $this->email->send();
    }    
        
}