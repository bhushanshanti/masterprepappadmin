<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';   
     
class Get_course extends REST_Controller {  
     
    public function __construct() {
       parent::__construct();
        $this->load->database();        
        $this->load->model('Test_module_model'); 
    }       
    /**
     * Get All course at enquiry from this method.
     *
     * @return Response
    */
    public function index_get()
    {
        if (!$this->Authenticate($this->input->get_request_header('token'))) {
            $this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND); 
        }else{         
            
            $id = $this->input->get_request_header('id');
            $spData = $this->idRoutineCall($id);       
            $programe_id    = $spData['programe_id'];
            $cData = $this->Test_module_model->get_course($programe_id);

            if(!empty($cData)){
              $data['error_message'] = [ "success" => 1, "message" => "success", "data"=> $cData];    
            }else{
                  $data['error_message'] = [ "success" => 0, "message" => "No Courses found!", "data"=> $cData];     
            }        
            $this->set_response($data, REST_Controller::HTTP_CREATED);
        }
    }     
    
        
}