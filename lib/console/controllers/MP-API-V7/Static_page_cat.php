<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/

    use Restserver\Libraries\REST_Controller;
    defined('BASEPATH') OR exit('No direct script access allowed');
    require APPPATH . 'libraries/REST_Controller.php';
    
     
class Static_page_cat extends REST_Controller {
    
    /**
     * Get All LL catg from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
       $this->load->model('Static_page_model');
    }   

    public function index_get()
    {
        $token = $this->input->get_request_header('token');
        if (!$this->Authenticate($token)) {
            $this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND); 
        }else{

        $spData = $this->tokenRoutineCall($token);       
        $programe_id    = $spData['programe_id'];
        $test_module_id = $spData['test_module_id'];       

        $data2 = $this->Static_page_model->getStaticPageCatg($programe_id,$test_module_id);

          if(!empty($data2)){
            $data['error_message'] = [ "success" => 1, "message" => "success", "data" => $data2];
          }else{
            $data['error_message'] = [ "success" => 0, "message" => "data not found","data" => $data2];
          }               
          $this->set_response($data, REST_Controller::HTTP_CREATED);
        }
    }
        
}