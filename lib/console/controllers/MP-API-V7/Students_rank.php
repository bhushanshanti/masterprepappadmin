<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';   
     
class Students_rank extends REST_Controller {  
     
    public function __construct() {
       parent::__construct();
        $this->load->database();        
        $this->load->model('Student_results_model'); 
    }       
    /**
     * Get All packages for test module as per programme from this method.
     *
     * @return Response
    */
    public function index_get()
    {
        if (!$this->Authenticate($this->input->get_request_header('token'))) {
            $this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND); 
        }else{ 
             
            $id = $this->input->get_request_header('id');
            $spData = $this->idRoutineCall($id);       
            $programe_id    = $spData['programe_id'];
            $test_module_id = $spData['test_module_id'];      
           
            $rData = $this->Student_results_model->getRank($programe_id,$test_module_id,$id);

            if(!empty($rData)){
              $data['error_message'] = [ "success" => 1, "message" => "success", "data"=> $rData];    
            }else{
                  $data['error_message'] = [ "success" => 0, "message" => "No Rank data available now!", "data"=> $rData];     
            }        
            $this->set_response($data, REST_Controller::HTTP_CREATED);
        }
    }     
    
        
}