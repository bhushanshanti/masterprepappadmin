<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/
   
    use Restserver\Libraries\REST_Controller;
    defined('BASEPATH') OR exit('No direct script access allowed');
    require APPPATH . 'libraries/REST_Controller.php';    
     
class Student_otp_login_verify extends REST_Controller {
    
	/**
     * Get verify the user OTP from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
    } 

    function index_post() {
        
        $otp = trim($this->post('otp', TRUE)); 
        $mno = trim($this->post('mno', TRUE));
        $countryCode = trim($this->post('countryCode', TRUE));

        if($countryCode!=''){

            $countryCodeExist = $this->db->query("SELECT country_id FROM country where phonecode = '".$countryCode."' and active = 1 ");

            if($countryCodeExist->num_rows() > 0){
                $mno2 = $countryCode.$mno;
            }else{
                $data2['error_message'] = ["success" => 0, "message" => "Invalid Country code!"];            
                return $this->set_response($data2, REST_Controller::HTTP_CREATED);
            }
            
        }else{
            $mno2='';
            $data2['error_message'] = ["success" => 0, "message" => "Please select Country code!"];            
            return $this->set_response($data2, REST_Controller::HTTP_CREATED);
        }
        if ($otp <> "" and $mno <> "" and $mno2 <> "") {
            
                //echo 'ok';die;
                $query = $this->db->query("SELECT id FROM students where OTP ='" . $otp . "' and active=1 and mobile='".$mno."' and country_code = '" . $countryCode . "' ");
               
                if ($query->num_rows() > 0) {

                    $userdata = $query->result();
                    $token = $this->getTokens($mno);
                    $update_token_data = array('token'=>$token);
                    $this->db->update('students', $update_token_data, array('id' => $userdata[0]->id, 'OTP' => $otp, 'active' => 1));

                $query2 = $this->db->query("SELECT 
                    s.id,s.fname,s.lname,s.dob,s.email,s.country_code,s.mobile,s.programe_id,s.test_module_id,s.token,s.profile_pic,pgm.programe_name,cnt.name as country_name,cnt.flag
                    FROM students s 
                    LEFT JOIN programe_masters pgm ON pgm.programe_id = s.programe_id
                    LEFT JOIN country cnt ON cnt.country_id = s.country_id
                    where s.country_code = '".$countryCode."' and s.mobile = '". $mno."' and s.active = 1 and s.OTP = '".trim($otp)."' 
                ");
                $userdata2 = $query2->result();
                    
                    $data['error_message'] = ["success" => 1, "message" => "OTP has been verified", 'userdetails' => $userdata2[0] ];
                    
                }else {
                    $data['error_message'] = ["success" => 0, "message" => "Invalid OTP Code or Mobile No.!"];
                }
            }else {
                $data['error_message'] = ["success" => 0, "message" => "Please enter OTP Code !"];
            }
            $this->set_response($data, REST_Controller::HTTP_CREATED);
    }    
    	
}