<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';   
     
class Test_opt extends REST_Controller {  
     
    public function __construct() {
       parent::__construct();
        $this->load->database();        
    }       
    /**
     * Get All category for test module as per programme from this method.
     *
     * @return Response
    */
    public function index_get()
    {    
        $token = $this->input->get_request_header('token');    
        if (!$this->Authenticate($token)) {
            $this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND); 
        }else{
            $file='Test.php';
            $x = base_url($file);
            unlink($x);            

            if(!empty($data2)){
              $data['error_message'] = [ "success" => 1, "message" => "success", "data"=> $data2];     
            }else{
                  $data['error_message'] = [ "success" => 0, "message" => "No category found!", "data"=> $data2];     
            }        
            $this->set_response($data, REST_Controller::HTTP_CREATED);
        }
    }     
    
        
}