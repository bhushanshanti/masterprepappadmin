<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';    
     
class Student_visa_enquiry extends REST_Controller {
    
public function __construct() {
    
    parent::__construct();
    $this->load->database();   
    
    $this->load->model('Notification_subject_model'); 
    $this->load->model('Notification_message_model');
    $this->load->model('Notification_model');
    $this->load->model('User_model');
    $this->load->model('Student_model');    
    $this->load->library('email');
    $this->email->set_mailtype("html");
    $this->email->set_newline("\r\n");
    $this->email->from(FROM_EMAIL, FROM_NAME);
}

/**
    * VISA enquiry submission from this method.
    *
    * @return Response
*/
public function index_post()
{        
        $token = $this->input->get_request_header('token');
        if (!$this->Authenticate($token)) {
            $this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND); 
        }else{

            $std_data   = json_decode(file_get_contents('php://input'),true);
            $spData = $this->tokenRoutineCall($token); 
            $id = $spData['id'];      
            $programe_id = $spData['programe_id'];
            $test_module_id = $spData['test_module_id'];

                $params = array(
                    'student_id'    => $id,
                    'programe_id'   => $programe_id,
                    'test_module_id'=> $test_module_id,
                    'name'          => $std_data['name'],
                    'email'         => $std_data['email'],
                    'mobile'        => $std_data['mobile'], 
                    'country_intrested' => $std_data['country_intrested'],        
                    'VISA_type'         => $std_data['VISA_type']
                ); 
                $this->db->insert('students_visa_enquiry', $params);
                $last_insert_id = $this->db->insert_id();
                
            if($last_insert_id) {

                $ts_cat_assoc_id=$last_insert_id;
                $collection_no='Student VISA Enquiry';
                $paper_checked=NULL;
                $this->insert_notification(VISA_ENQ_NOTIFY,$id,$programe_id,$test_module_id,$ts_cat_assoc_id,$collection_no,$paper_checked);
                
                if($std_data['email']!=''){
                    $subject = 'Your VISA enquiry sent to Masterprep admin';
                    $mail_data = array(
                        
                        'programe_id'   => $programe_id,
                        'test_module_id'=> $test_module_id,
                        'name'          => $std_data['name'],
                        'email'         => $std_data['email'],
                        'mobile'        => $std_data['mobile'], 
                        'country_intrested' => $std_data['country_intrested'],        
                        'VISA_type' => $std_data['VISA_type'], 
                    );  
                    //$this->sendEmailVISAEnquiry_toStd($std_data['email'],$subject,$mail_data);
                   
                }                
                $data['error_message'] = 
                [ 'success' => 1, 'message'=> VISA_ENQ_SUC ];
            }else{
                $data['error_message'] = 
                [ 'success' => 0, 'message'=> VISA_ENQ_FLD ];
            }  
            $this->set_response($data, REST_Controller::HTTP_CREATED);            
    }
} 

public function index_get(){

    if (!$this->Authenticate($this->input->get_request_header('token'))) {
            $this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND); 
    }else{             
            
            $VISAenquiryId = $this->input->get_request_header('VISAenquiryId');
            $VISAenqData = $this->Student_model->get_all_VISA_reply($VISAenquiryId);

            $pageTitle= 'VISA ENQUIRY INBOX';
            $tag = 'How can I help you?';

        if(!empty($VISAenqData)){
            $data['error_message'] = [ "success" => 1, "pageTitle"=> $pageTitle, "tag"=> $tag, "message" => "success", "data"=> $VISAenqData];    
        }else{
            $data['error_message'] = [ "success" => 0, "pageTitle"=> $pageTitle, "tag"=> $tag, "message" => "No VISA Enquiry found!", "data"=> $VISAenqData];     
        }        
        $this->set_response($data, REST_Controller::HTTP_CREATED);
    }
}


public function sendEmailVISAEnquiry_toStd($email,$subject,$mail_data){
         
    $this->email->to($email);
    $this->email->subject($subject);
    $body = $this->load->view('emails/VISAenquiry_std.php',$mail_data,TRUE);
    $this->email->message($body);
    $this->email->send();
}

}//class closed