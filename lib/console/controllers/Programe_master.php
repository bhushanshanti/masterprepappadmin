<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/
 
class Programe_master extends MY_Controller{
    
    function __construct()
    {
        parent::__construct();
        if (!$this->_is_logged_in()) {redirect('/');}
        $this->load->model('Programe_master_model');       
    }
    /*
     * Listing of all programmes
     */
    function index()
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $this->load->library('pagination');
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('programe_master/index?');
        $config['total_rows'] = $this->Programe_master_model->get_all_programe_masters_count();
        $this->pagination->initialize($config);
        $data['title'] = 'Program';
        $data['programe_masters'] = $this->Programe_master_model->get_all_programe_masters($params);
        $data['_view'] = 'programe_master/index';
        $this->load->view('layouts/main',$data);
    }
    /*
     * Adding a new programme
     */
    function add()
    {   
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $data['title'] = 'Add Program';
        $this->load->library('form_validation');
		$this->form_validation->set_rules('programe_name','Program name','required|trim|min_length[3]|max_length[30]|is_unique[programe_masters.programe_name]');		
		if($this->form_validation->run())     
        {   
            $user = $this->session->userdata('admin_login_data');
            foreach ($user as $d){$by_user=$d->id;}
            $params = array(
				'active' => $this->input->post('active'),
				'programe_name' => $this->input->post('programe_name'),
                'by_user' => $by_user,
            );            
            $id = $this->Programe_master_model->add_programe_master($params);
            if($id){
                $this->session->set_flashdata('flsh_msg', SUCCESS_MSG);
                redirect('Programe_master/index');
            }else{
                $this->session->set_flashdata('flsh_msg', FAILED_MSG);
                redirect('Programe_master/add');
            }  
        }
        else
        {            
            $data['_view'] = 'programe_master/add';
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a programe
     */
    function edit($programe_id)
    {   
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $data['title'] = 'Edit Program';
        $data['programe_master'] = $this->Programe_master_model->get_programe_master($programe_id);
        
        if(isset($data['programe_master']['programe_id']))
        {
            $this->load->library('form_validation');
			$this->form_validation->set_rules('programe_name','Program name','required|trim|min_length[3]|max_length[30]');		
			if($this->form_validation->run())     
            {   
                $user = $this->session->userdata('admin_login_data');
            foreach ($user as $d){$by_user=$d->id;}
                $params = array(
					'active' => $this->input->post('active'),
					'programe_name' => $this->input->post('programe_name'),
                    'by_user' => $by_user,
                );
                $id = $this->Programe_master_model->update_programe_master($programe_id,$params);
                if($id){
                    $this->session->set_flashdata('flsh_msg', UPDATE_MSG);           
                    redirect('programe_master/index');
                }else{
                    $this->session->set_flashdata('flsh_msg', UPDATE_FAILED_MSG);           
                    redirect('programe_master/edit/'.$programe_id);
                }  

            }
            else
            {
                $data['_view'] = 'programe_master/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error(ITEM_NOT_EXIST);
    }
    /*
     * Deleting programe
     */
    function remove($programe_id)
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $programe_master = $this->Programe_master_model->get_programe_master($programe_id);
        if(isset($programe_master['programe_id']))
        {
            $del = $this->Programe_master_model->delete_programe_master($programe_id);
            if($del){
                $this->session->set_flashdata('flsh_msg', DEL_MSG);
                redirect('programe_master/index');
            }else{
                $this->session->set_flashdata('flsh_msg', DEL_MSG_FAILED);
                redirect('programe_master/index');
            }
        }
        else
            show_error(ITEM_NOT_EXIST);
    }
    
}
