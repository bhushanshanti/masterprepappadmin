<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/
 class Question_behavior extends MY_Controller{
    
    function __construct()
    {
        parent::__construct();
        if (!$this->_is_logged_in()) {redirect('/');}
        $this->load->model('Question_behavior_model');        
    }
    /*
     * Listing of all question behaviors
     */
    function index()
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $this->load->library('pagination');
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('question_behavior/index?');
        $config['total_rows'] = $this->Question_behavior_model->get_all_question_behavior_count();
        $this->pagination->initialize($config);
        $data['title'] = 'Question behaviors';
        $data['question_behavior'] = $this->Question_behavior_model->get_all_question_behavior($params);        
        $data['_view'] = 'question_behavior/index';
        $this->load->view('layouts/main',$data);
    }
    /*
     * Adding a new question_behavior
     */
    function add()
    {   
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $data['title'] = 'Add question behavior';
        $this->load->library('form_validation');
		$this->form_validation->set_rules('behavior_name','Question behavior','required|trim|min_length[3]|max_length[40]|is_unique[question_behavior.behavior_name]');
		
		if($this->form_validation->run())     
        {   
            $user = $this->session->userdata('admin_login_data');
            foreach ($user as $d){$by_user=$d->id;}
            $params = array(
				'active' => $this->input->post('active'),
				'behavior_name' => $this->input->post('behavior_name'),
                'by_user' => $by_user,
            );            
            $id = $this->Question_behavior_model->add_question_behavior($params);  
            if($id){
                $this->session->set_flashdata('flsh_msg', SUCCESS_MSG);
                redirect('question_behavior/index');
            }else{
                $this->session->set_flashdata('flsh_msg', FAILED_MSG);
                redirect('question_behavior/add');
            }
        }
        else
        {            
            $data['_view'] = 'question_behavior/add';
            $this->load->view('layouts/main',$data);
        }
    }
    /*
     * Editing a question_behavior
     */
    function edit($question_behavior_id)
    {   
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $data['title'] = 'Edit question behavior';
        $data['question_behavior'] = $this->Question_behavior_model->get_question_behavior($question_behavior_id);
        if(isset($data['question_behavior']['question_behavior_id']))
        {
            $this->load->library('form_validation');
			$this->form_validation->set_rules('behavior_name','Question behavior','required|trim|min_length[3]|max_length[40]');
		
			if($this->form_validation->run())     
            {   
                $user = $this->session->userdata('admin_login_data');
                foreach ($user as $d){$by_user=$d->id;}
                $params = array(
					'active' => $this->input->post('active'),
					'behavior_name' => $this->input->post('behavior_name'),
                    'by_user' => $by_user,
                );
                $id = $this->Question_behavior_model->update_question_behavior($question_behavior_id,$params);
                if($id){
                    $this->session->set_flashdata('flsh_msg', UPDATE_MSG);           
                    redirect('question_behavior/index');
                }else{
                    $this->session->set_flashdata('flsh_msg', UPDATE_FAILED_MSG);           
                    redirect('question_behavior/edit/'.$question_behavior_id);
                }                
            }
            else
            {
                $data['_view'] = 'question_behavior/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error(ITEM_NOT_EXIST);
    }
    /*
     * Deleting question_behavior
     */
    function remove($question_behavior_id)
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $question_behavior = $this->Question_behavior_model->get_question_behavior($question_behavior_id);
        if(isset($question_behavior['question_behavior_id']))
        {
            $this->Question_behavior_model->delete_question_behavior($question_behavior_id);
            $this->session->set_flashdata('flsh_msg', DEL_MSG);
            redirect('question_behavior/index');
        }
        else
            show_error(ITEM_NOT_EXIST);
    }
    
}
