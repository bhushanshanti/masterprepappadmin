<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/ 
class Static_page extends MY_Controller{
    function __construct()
    {
        parent::__construct();
        if (!$this->_is_logged_in()) {redirect('/');}        
        $this->load->model('Static_page_model');
        $this->load->model('Question_type_model');   
        //$this->load->library('m_pdf');
    }
    /*
     * Listing of static_pages
     */
    function index($pid=0)
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $this->load->library('pagination');
        $params['limit'] = RECORDS_PER_PAGE;
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('static_page/index?');
        $config['total_rows'] = $this->Static_page_model->get_all_static_pages_count_parent();
        $this->pagination->initialize($config);
        $data['title'] = 'Static pages- '.IELTS.'-'.ACD;
        $data['static_pages'] = $this->Static_page_model->get_all_static_pages_parent($params,$pid);
        $data['_view'] = 'static_page/index';
        $this->load->view('layouts/main',$data);
    }

    function index_gt($pid=0)
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $this->load->library('pagination');
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('static_page/index_gt?');
        $config['total_rows'] = $this->Static_page_model->get_all_static_pages_count_parent_gt();
        $this->pagination->initialize($config);
        $data['title'] = 'Static pages- '.IELTS.'-'.GT;
        $data['static_pages'] = $this->Static_page_model->get_all_static_pages_parent_gt($params,$pid);
        $data['_view'] = 'static_page/index';
        $this->load->view('layouts/main',$data);
    }

    function index_ee($pid=0)
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $this->load->library('pagination');
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('static_page/index_ee?');
        $config['total_rows'] = $this->Static_page_model->get_all_static_pages_count_parent_ee();
        $this->pagination->initialize($config);
        $data['title'] = 'Static pages- '.IELTS.'-'.ENGLISH_ESSENTIAL;
        $data['static_pages'] = $this->Static_page_model->get_all_static_pages_parent_ee($params,$pid);
        $data['_view'] = 'static_page/index';
        $this->load->view('layouts/main',$data);
    }
    /*
     * Adding a new static_page
     */
    function add()
    {   
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $data['title'] = 'Add static page';
        $this->load->library('form_validation');
		$this->form_validation->set_rules('category_id','Category','required');
		$this->form_validation->set_rules('title','Title','required|trim|min_length[3]|max_length[100]');		
		if($this->form_validation->run())     
        { 
            $user = $this->session->userdata('admin_login_data');
            foreach ($user as $d){$by_user=$d->id;}
            $config['upload_path']      = STATIC_PAGE_IMAGE_PATH;
            $config['allowed_types']    = STATIC_PAGE_ALLOWED_TYPES;
            $config['encrypt_name']     = FALSE;         
            $this->load->library('upload',$config);
            if($this->upload->do_upload("image")){
                $data_img = array('upload_data' => $this->upload->data());
                $image= site_url(STATIC_PAGE_IMAGE_PATH).$data_img['upload_data']['file_name'];
            }else{
                $image=NULL;
            }            
            if($this->upload->do_upload("video")){
                $data_vid = array('upload_data' => $this->upload->data());
                $video= site_url(STATIC_PAGE_IMAGE_PATH).$data_vid['upload_data']['file_name'];
            }else{
                $video=NULL;
            }
                $params = array(
                    'category_id' => $this->input->post('category_id'),
                    'question_type_id'=> $this->input->post('question_type_id'),
                    'active' => $this->input->post('active'),
                    'pid' => $this->input->post('pid'),             
                    'title' => $this->input->post('title'),
                    'image' => $image,
                    'video' => $video,
                    'contents' => $this->input->post('contents'),
                    'audio' => $this->input->post('audio') ? $this->input->post('audio') : NULL,
                    'by_user' => $by_user
                );
                $id = $this->Static_page_model->add_static_page($params);
                $catId = $params['category_id'];
                if($id){
                    
                    $this->session->set_flashdata('flsh_msg', SUCCESS_MSG);
                    if($id and ($catId>=44 and $catId<=47))
                    redirect('static_page/index');
                    elseif($id and ($catId>=48 and $catId<=51))
                    redirect('static_page/index_gt');
                    else
                    redirect('static_page/index_ee');
                }else{
                    $this->session->set_flashdata('flsh_msg', FAILED_MSG);
                    redirect('static_page/add');
                }            
        }
        else
        {
			$this->load->model('Category_master_model');
			$data['all_category_masters'] = $this->Category_master_model->get_all_category_masters_active();
			$this->load->model('Static_page_model');
			$data['static_pages2'] = $this->Static_page_model->get_all_static_pages2();
            $data['all_question_types'] = $this->Question_type_model->get_all_question_types_active();
            $data['_view'] = 'static_page/add';
            $this->load->view('layouts/main',$data);
        }
    }
    /*
     * Editing a static_page
     */
    function edit($static_page_id)
    {   
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $data['title'] = 'Edit static page';
        $data['static_page'] = $this->Static_page_model->get_static_page($static_page_id);
        $data['static_page_parent'] = $this->Static_page_model->get_static_page($data['static_page']['pid']);        
        if(isset($data['static_page']['static_page_id']))
        {           
            $user = $this->session->userdata('admin_login_data');
            foreach ($user as $d){$by_user=$d->id;}
            $this->load->library('form_validation');
			$this->form_validation->set_rules('category_id','Category','required');
			$this->form_validation->set_rules('title','Title','required|trim|min_length[3]|max_length[100]');
            //$this->form_validation->set_rules('icon','Icon URL','trim');
			if($this->form_validation->run())     
            {
                $params = array(
                    'category_id' => $this->input->post('category_id'),
                    'question_type_id'=> $this->input->post('question_type_id'),
                    'active' => $this->input->post('active'),
                    'pid' => $this->input->post('pid'),             
                    'title' => $this->input->post('title'),
                    'contents' => $this->input->post('contents'),
                    //'icon' => $this->input->post('icon') ? $this->input->post('icon') : NULL,
                    'audio' => $this->input->post('audio') ? $this->input->post('audio') : NULL,
                    'by_user' => $by_user
                );
                $config['upload_path']  = STATIC_PAGE_IMAGE_PATH;
                $config['allowed_types'] = STATIC_PAGE_ALLOWED_TYPES;
                $config['encrypt_name'] = FALSE;         
                $this->load->library('upload',$config);
                if($this->upload->do_upload("image")){
                    $data_img = array('upload_data' => $this->upload->data());
                    $image= site_url(STATIC_PAGE_IMAGE_PATH).$data_img['upload_data']['file_name'];                    
                }else{
                    $image=NULL;
                }
                $params = array(
                    'category_id' => $this->input->post('category_id'),
                    'question_type_id'=> $this->input->post('question_type_id'),
                    'active' => $this->input->post('active'),
                    'pid' => $this->input->post('pid'),             
                    'title' => $this->input->post('title'),
                    'image' => $image, 
                    'contents' => $this->input->post('contents'),
                    'audio' => $this->input->post('audio') ? $this->input->post('audio') : NULL,
                    'by_user' => $by_user
                    );

                if($this->upload->do_upload("video")){
                $data_vid = array('upload_data' => $this->upload->data());
                $video= site_url(STATIC_PAGE_IMAGE_PATH).$data_vid['upload_data']['file_name'];
                
                    if(isset($image)and $image!=NULL){
                        $params = array(
                            'category_id' => $this->input->post('category_id'),
                            'question_type_id'=> $this->input->post('question_type_id'),
                            'active' => $this->input->post('active'),
                            'pid' => $this->input->post('pid'),             
                            'title' => $this->input->post('title'),
                            'image' => $image,
                            'video' => $video,
                            'contents' => $this->input->post('contents'),
                            'audio' => $this->input->post('audio') ? $this->input->post('audio') : NULL,
                            'by_user' => $by_user
                        );
                    }else{
                        $params = array(
                            'category_id' => $this->input->post('category_id'),
                            'question_type_id'=> $this->input->post('question_type_id'),
                            'active' => $this->input->post('active'),
                            'pid' => $this->input->post('pid'),             
                            'title' => $this->input->post('title'),
                            'video' => $video,
                            'contents' => $this->input->post('contents'),
                            'audio' => $this->input->post('audio') ? $this->input->post('audio') : NULL,
                            'by_user' => $by_user
                        );
                    }
                    
                }else{
                    $video=NULL;
                    if(isset($image)and $image!=NULL){
                        $params = array(
                            'category_id' => $this->input->post('category_id'),
                            'question_type_id'=> $this->input->post('question_type_id'),
                            'active' => $this->input->post('active'),
                            'pid' => $this->input->post('pid'),             
                            'title' => $this->input->post('title'),
                            'image' => $image,
                            'contents' => $this->input->post('contents'),
                            'audio' => $this->input->post('audio') ? $this->input->post('audio') : NULL,
                            'by_user' => $by_user
                        );
                    }else{
                        $params = array(
                            'category_id' => $this->input->post('category_id'),
                            'question_type_id'=> $this->input->post('question_type_id'),
                            'active' => $this->input->post('active'),
                            'pid' => $this->input->post('pid'),             
                            'title' => $this->input->post('title'),
                            'contents' => $this->input->post('contents'),
                            'audio' => $this->input->post('audio') ? $this->input->post('audio') : NULL,
                            'by_user' => $by_user
                        );
                    }
                }
                $id = $this->Static_page_model->update_static_page($static_page_id,$params);
                $catId = $params['category_id'];
                if($id){
                    $this->session->set_flashdata('flsh_msg', UPDATE_MSG);
                    if($catId>=44 and $catId<=47)
                    redirect('static_page/index');
                    elseif($catId>=48 and $catId<=51)
                    redirect('static_page/index_gt');
                    else
                    redirect('static_page/index_ee');
                }else{
                    $this->session->set_flashdata('flsh_msg', UPDATE_FAILED_MSG);           
                    redirect('static_page/edit/'.$static_page_id);
                }               
            }
            else
            {
				$this->load->model('Category_master_model');
                $data['all_category_masters'] = $this->Category_master_model->get_all_category_masters_active();
                $this->load->model('Static_page_model');
                $data['static_pages2'] = $this->Static_page_model->get_all_static_pages2();
                $data['all_question_types'] = $this->Question_type_model->get_all_question_types();
                $data['_view'] = 'static_page/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error(ITEM_NOT_EXIST);
    }
    /*
     * Deleting static_page
     */
    function remove($static_page_id)
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $static_page = $this->Static_page_model->get_static_page($static_page_id);
        if(isset($static_page['static_page_id']))
        {
            $this->Static_page_model->delete_static_page($static_page_id);
            $this->session->set_flashdata('flsh_msg', DEL_MSG);

            if($static_page['category_id']>=44 and $static_page['category_id']<=47)
                redirect('static_page/index');
            elseif($static_page['category_id']>=48 and $static_page['category_id']<=51)
                redirect('static_page/index_gt');
            else
                redirect('static_page/index_ee');
            
        }
        else
            show_error(ITEM_NOT_EXIST);
    }


    function print_sp($category_id){

        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $this->load->library('pagination');
        $params['limit'] = RECORDS_PER_PAGE;
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('static_page/print_sp?');
        $config['total_rows'] = $this->Static_page_model->get_all_static_pages_count_parent();
        $this->pagination->initialize($config);
        $data['title'] = 'Print Static page';
        $data['sp_all'] = $this->Static_page_model->get_sp_to_print($category_id);
        $data['_view'] = 'static_page/print_sp';
        $this->load->view('layouts/main',$data);
    }
    
}
