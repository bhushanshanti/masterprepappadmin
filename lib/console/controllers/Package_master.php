<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/ 
class Package_master extends MY_Controller{
    
    function __construct()
    {
        parent::__construct();
        if (!$this->_is_logged_in()) {redirect('/');}
        $this->load->model('Package_master_model');  
        $this->load->model('Test_module_model');   
        $this->load->model('Programe_master_model');   
    }
    /*
     * Listing of packages Academic
     */
    function index()
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $this->load->library('pagination');
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('package_master/index?');
        $config['total_rows'] = $this->Package_master_model->get_all_package_masters_count();
        $this->pagination->initialize($config);
        $data['title'] = 'Packages- '.ACD;
        $data['package_masters'] = $this->Package_master_model->get_all_package_masters($params);
        $data['_view'] = 'package_master/index';
        $this->load->view('layouts/main',$data);
    }

    /*
     * Listing of packages GT
     */
    function index_gt()
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $this->load->library('pagination');
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('package_master/index_gt?');
        $config['total_rows'] = $this->Package_master_model->get_all_package_masters_count_gt();
        $this->pagination->initialize($config);
        $data['title'] = 'Packages- '.GT;
        $data['package_masters'] = $this->Package_master_model->get_all_package_masters_gt($params);
        $data['_view'] = 'package_master/index';
        $this->load->view('layouts/main',$data);
    }
    /*
     * Adding a new package_master
     */
    function add()
    {   
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $data['title'] = 'Add package';
        $this->load->library('form_validation');
		$this->form_validation->set_rules('package_name','Package name','required|trim|is_unique[package_masters.package_name]');
		$this->form_validation->set_rules('amount','Amount','required|trim');
		$this->form_validation->set_rules('duration','Duration','required|trim');	
        $this->form_validation->set_rules('programe_id','Programe','required');
        $this->form_validation->set_rules('test_module_id','Test Module','required');
        $this->form_validation->set_rules('test_paper_limit','Test Limit','required|trim');	
		
		if($this->form_validation->run())     
        {   
            $user = $this->session->userdata('admin_login_data');
            foreach ($user as $d){$by_user=$d->id;}
            $params = array(
				'active' => $this->input->post('active'),
				'package_name' => $this->input->post('package_name'),
                'package_color' => $this->input->post('package_color'),
				'amount' => $this->input->post('amount'),
				'duration' => $this->input->post('duration'),				
				'test_paper_limit' => $this->input->post('test_paper_limit'),
				'test_module_id' => $this->input->post('test_module_id'),
                'programe_id' => $this->input->post('programe_id'),
				'package_desc' => $this->input->post('package_desc'),
                'by_user' => $by_user,
            );            
            $id = $this->Package_master_model->add_package_master($params);
            if($id and $params['programe_id'] == ACD_ID){
                $this->session->set_flashdata('flsh_msg', SUCCESS_MSG);
                redirect('package_master/index');
            }elseif($id and $params['programe_id'] == GT_ID){
                $this->session->set_flashdata('flsh_msg', SUCCESS_MSG);
                redirect('package_master/index_gt');
            }
            else{
                $this->session->set_flashdata('flsh_msg', FAILED_MSG);
                redirect('package_master/add');
            }            
        }
        else
        {            
           $data['all_test_module'] = $this->Test_module_model->get_all_test_module_active();
           $data['all_programe_masters'] = $this->Programe_master_model->get_all_programe_masters_active();
            $data['_view'] = 'package_master/add';
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a package_master
     */
    function edit($package_id)
    {   
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $data['title'] = 'Edit package';
        $data['package_master'] = $this->Package_master_model->get_package_master($package_id);
        if(isset($data['package_master']['package_id']))
        {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('package_name','Package name','required|trim');
            $this->form_validation->set_rules('amount','Amount','required|trim');
            $this->form_validation->set_rules('duration','Duration','required|trim');
            $this->form_validation->set_rules('programe_id','Programe','required');
            $this->form_validation->set_rules('test_module_id','Test Module','required');
            $this->form_validation->set_rules('test_paper_limit','Test Limit','required|trim'); 			
		
			if($this->form_validation->run())     
            {   
                $user = $this->session->userdata('admin_login_data');
                foreach ($user as $d){$by_user=$d->id;}
                $params = array(
					'active' => $this->input->post('active'),
					'package_name' => $this->input->post('package_name'),
                    'package_color' => $this->input->post('package_color'),
					'amount' => $this->input->post('amount'),
					'duration' => $this->input->post('duration'),					
					'test_paper_limit' => $this->input->post('test_paper_limit'),
                    'test_module_id' => $this->input->post('test_module_id'),
                    'programe_id' => $this->input->post('programe_id'),
					'package_desc' => $this->input->post('package_desc'),
                    'by_user' => $by_user,
                );
                $id = $this->Package_master_model->update_package_master($package_id,$params);

                if($id and $params['programe_id'] == ACD_ID){
                $this->session->set_flashdata('flsh_msg', UPDATE_MSG);
                redirect('package_master/index');
                }elseif($id and $params['programe_id'] == GT_ID){
                    $this->session->set_flashdata('flsh_msg', UPDATE_MSG);
                    redirect('package_master/index_gt');
                }else{
                    $this->session->set_flashdata('flsh_msg', UPDATE_FAILED_MSG);           
                    redirect('package_master/edit/'.$package_id);
                }                
            }
            else
            {
                $data['all_test_module'] = $this->Test_module_model->get_all_test_module_active();
                $data['all_programe_masters'] = $this->Programe_master_model->get_all_programe_masters_active();
                $data['_view'] = 'package_master/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error(ITEM_NOT_EXIST);
    }   
    /*
     * Deleting package_master
     */
    function remove($package_id)
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $package_master = $this->Package_master_model->get_package_master($package_id);
        if(isset($package_master['package_id']))
        {
            $this->Package_master_model->delete_package_master($package_id);
            $this->session->set_flashdata('flsh_msg', DEL_MSG);
            redirect('package_master/index');
        }
        else
            show_error(ITEM_NOT_EXIST);
    }    
    
}
