<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/
 class Real_test_dates extends MY_Controller{
    
    function __construct()
    {
        parent::__construct();
        if (!$this->_is_logged_in()) {redirect('/');}
        $this->load->model('Real_test_model');        
        $this->load->model('Test_module_model');
        $this->load->model('Programe_master_model');
    }    
    /*
     * Listing of all Real_test_dates
     */
    function index()
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends

        $this->load->library('pagination');
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('Real_test_dates/index?');
        $config['total_rows'] = $this->Real_test_model->get_all_test_dates_count();
        $this->pagination->initialize($config);
        $data['title'] = 'Real test dates';
        $data['Real_test_dates'] = $this->Real_test_model->get_all_test_dates($params);
        $data['all_programe_masters'] = $this->Programe_master_model->get_all_programe_masters_active();
        $data['all_test_module'] = $this->Test_module_model->get_all_test_module_active();
        $data['_view'] = 'Real_test_dates/index';
        $this->load->view('layouts/main',$data);
    }
    


    /*
     * Adding a new Real_test_dates
     */
    function add()
    { 
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends

        $data['title'] = 'Add Real test dates';  
        $this->load->library('form_validation');
        $this->form_validation->set_rules('testDate','Date','required|min_length[10]|max_length[10]');
        $this->form_validation->set_rules('programe_id','Programme','required');
        $this->form_validation->set_rules('test_module_id','Test module','required');
        if($this->form_validation->run())     
        {
            $user = $this->session->userdata('admin_login_data');
            foreach ($user as $d){$by_user=$d->id;}
            $params = array(
                'active' => $this->input->post('active'),
                'programe_id'        => $this->input->post('programe_id'),
                'testDate'           => $this->input->post('testDate'), 
                'test_module_id'     => $this->input->post('test_module_id'),                
                'by_user'            => $by_user,                             
            );
            $data['_view'] = 'Real_test_dates/add';
            $this->load->view('layouts/main',$data);
            $id = $this->Real_test_model->add_real_test_dates($params); 
            if($id){
                $this->session->set_flashdata('flsh_msg', SUCCESS_MSG);
                redirect('Real_test_dates/index');
            }else{
                $this->session->set_flashdata('flsh_msg', FAILED_MSG);
                redirect('Real_test_dates/add');
            }            
        }else{            
             
            $data['all_test_module'] = $this->Test_module_model->get_all_test_module_active();
            $data['all_programe_masters'] = $this->Programe_master_model->get_all_programe_masters_active();
            $data['_view'] = 'Real_test_dates/add';
            $this->load->view('layouts/main',$data);
        }
    }
    /*
     * Editing a test_seriese IELTS/englis_essential/PTE/Mock/ET..
     */
    function edit($realTestDatesId)
    {  
        //echo $realTestDatesId;die;
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $data['title'] = 'Edit Real test dates';  
        $data['Real_test_dates'] = $this->Real_test_model->get_test_dates($realTestDatesId);   
        
        if(isset($data['Real_test_dates']['realTestDatesId']))
        {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('testDate','Date','required|min_length[10]|max_length[10]');
            $this->form_validation->set_rules('programe_id','Programme','required');
            $this->form_validation->set_rules('test_module_id','Test module','required');       
            if($this->form_validation->run())     
            {   
                $user = $this->session->userdata('admin_login_data');
                foreach ($user as $d){$by_user=$d->id;}
                $params = array(
                    'active' => $this->input->post('active'),
                    'programe_id'        => $this->input->post('programe_id'),
                    'testDate'           => $this->input->post('testDate'), 
                    'test_module_id'     => $this->input->post('test_module_id'),                
                    'by_user'            => $by_user,                  
                );                
                $id = $this->Real_test_model->update_test_date($realTestDatesId,$params);
                if($id){
                    $this->session->set_flashdata('flsh_msg', UPDATE_MSG);
                    redirect('Real_test_dates/index');
                       
                }else{
                    $this->session->set_flashdata('flsh_msg', UPDATE_FAILED_MSG);
                    redirect('Real_test_dates/edit/'.$realTestDatesId);
                }   
            }else{
                
                $data['all_test_module'] = $this->Test_module_model->get_all_test_module_active();
                $data['all_programe_masters'] = $this->Programe_master_model->get_all_programe_masters_active();
                $data['_view'] = 'Real_test_dates/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error(ITEM_NOT_EXIST);
    }
    
    function remove_ts($test_seriese_id,$test_module_id)
    {        
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $this->load->model('Ts_cat_assoc_model');
        $ts = $this->Test_seriese_model->get_test_seriese($test_seriese_id);
        $programe_id = $ts['programe_id'];
        if(isset($ts['test_seriese_id']))
        {
            $this->Test_seriese_model->delete_test_seriese($test_seriese_id);
            $this->session->set_flashdata('flsh_msg', TS_DEL_MSG);
            
            if($test_module_id==IELTS_ID ){

                        if($programe_id==ACD_ID){

                            if($data['test_seriese']['is_MtEt']==NULL){
                                redirect('test_seriese/index');
                            }elseif($data['test_seriese']['is_MtEt']==1){
                                redirect('test_seriese/index_et');
                            }else{
                                redirect('test_seriese/index_mt');
                            }

                        }elseif ($programe_id==GT_ID) {
                            if($data['test_seriese']['is_MtEt']==NULL){
                                redirect('test_seriese/index_gt');
                            }elseif($data['test_seriese']['is_MtEt']==1){
                                redirect('test_seriese/index_et_gt');
                            }else{
                                redirect('test_seriese/index_mt_gt');
                            }
                        }else{}

                    }elseif ($test_module_id==EE_ID) {

                        if($programe_id==ACD_ID or $programe_id==GT_ID){
                             redirect('test_seriese/english_essential');
                        }else{
                            redirect('test_seriese/english_essential');
                        }
                    }
        }
        else
            show_error(ITEM_NOT_EXIST);        
    }
    
}