<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/
 class Question_type extends MY_Controller{
    
    function __construct()
    {
        parent::__construct();
        if (!$this->_is_logged_in()) {redirect('/');}
        $this->load->model('Question_type_model'); 
        $this->load->model('Question_behavior_model');             
    }
    /*
     * Listing of all question types
     */
    function index()
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $this->load->library('pagination');
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('question_type/index?');
        $config['total_rows'] = $this->Question_type_model->get_all_question_types_count();
        $this->pagination->initialize($config);
        $data['title'] = 'Question types';
        $data['question_types'] = $this->Question_type_model->get_all_question_types($params);        
        $data['_view'] = 'question_type/index';
        $this->load->view('layouts/main',$data);
    }
    /*
     * Adding a new question_type
     */
    function add()
    {   
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $data['title'] = 'Add question type';
        $this->load->library('form_validation');
		$this->form_validation->set_rules('type_name','Question type','required|trim|min_length[3]|max_length[40]|is_unique[question_types.type_name]');
		$this->form_validation->set_rules('question_behavior_id','Behavior','required');
		if($this->form_validation->run())     
        {   
            $user = $this->session->userdata('admin_login_data');
            foreach ($user as $d){$by_user=$d->id;}
            $params = array(
				'active' => $this->input->post('active'),
				'type_name' => $this->input->post('type_name'),
                'question_behavior_id' => $this->input->post('question_behavior_id'),
                'by_user' => $by_user,
            );            
            $id = $this->Question_type_model->add_question_type($params);  
            if($id){
                $this->session->set_flashdata('flsh_msg', SUCCESS_MSG);
                redirect('question_type/index');
            }else{
                $this->session->set_flashdata('flsh_msg', FAILED_MSG);
                redirect('question_type/add');
            }
        }
        else
        {            
            $data['all_behavior_masters'] = $this->Question_behavior_model->get_all_question_behavior_active();
            $data['_view'] = 'question_type/add';
            $this->load->view('layouts/main',$data);
        }
    }
    /*
     * Editing a question_type
     */
    function edit($question_type_id)
    {   
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $data['title'] = 'Edit question type';
        $data['question_type'] = $this->Question_type_model->get_question_type($question_type_id);
        if(isset($data['question_type']['question_type_id']))
        {
            $this->load->library('form_validation');
			$this->form_validation->set_rules('type_name','Question type','required|trim|min_length[3]|max_length[40]');
            $this->form_validation->set_rules('question_behavior_id','Behavior','required');
		
			if($this->form_validation->run())     
            {   
                $user = $this->session->userdata('admin_login_data');
            foreach ($user as $d){$by_user=$d->id;}
                $params = array(
					'active' => $this->input->post('active'),
					'type_name' => $this->input->post('type_name'),
                    'question_behavior_id' => $this->input->post('question_behavior_id'),
                    'by_user' => $by_user,
                );
                $id = $this->Question_type_model->update_question_type($question_type_id,$params);
                if($id){
                    $this->session->set_flashdata('flsh_msg', UPDATE_MSG);           
                    redirect('question_type/index');
                }else{
                    $this->session->set_flashdata('flsh_msg', UPDATE_FAILED_MSG);           
                    redirect('question_type/edit/'.$question_type_id);
                }                
            }
            else
            {
                $data['all_behavior_masters'] = $this->Question_behavior_model->get_all_question_behavior_active();
                $data['_view'] = 'question_type/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error(ITEM_NOT_EXIST);
    }
    /*
     * Deleting question_type
     */
    function remove($question_type_id)
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $question_type = $this->Question_type_model->get_question_type($question_type_id);
        if(isset($question_type['question_type_id']))
        {
            $this->Question_type_model->delete_question_type($question_type_id);
            $this->session->set_flashdata('flsh_msg', DEL_MSG);
            redirect('question_type/index');
        }
        else
            show_error(ITEM_NOT_EXIST);
    }

    function get_qb(){

         //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $question_type_id = $this->input->post('question_type_id', true);
        $data=$this->Question_type_model->get_qb_id($question_type_id);
        $question_behavior_id = $data['question_behavior_id'];
        echo json_encode($question_behavior_id);


     }
    
}
