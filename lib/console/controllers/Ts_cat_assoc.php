<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/ 
class Ts_cat_assoc extends MY_Controller{
    
    function __construct()
    {
        parent::__construct();
        if (!$this->_is_logged_in()) {redirect('/');}
        $this->load->model('Ts_cat_assoc_model');
        $this->load->model('Test_seriese_model'); 
        $this->load->model('Category_master_model');        
    }
    /*
     * Listing of ts_cat_assoc ACD
     */
    function index()
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $this->load->library('pagination');
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('ts_cat_assoc/index?');
        $config['total_rows'] = $this->Ts_cat_assoc_model->get_all_ts_cat_assoc_count();
        $this->pagination->initialize($config);
        $data['title'] = 'Test&Category association -'.IELTS.'- '.ACD;  
        $data['ts_cat_assoc'] = $this->Ts_cat_assoc_model->get_all_ts_cat_assoc($params);
        $data['_view'] = 'ts_cat_assoc/index';
        $this->load->view('layouts/main',$data); 
    }

    /*
     * Listing of ts_cat_assoc ACD
     */
    function index_gt()
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $this->load->library('pagination');
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('ts_cat_assoc/index_gt?');
        $config['total_rows'] = $this->Ts_cat_assoc_model->get_all_ts_cat_assoc_count_gt();
        $this->pagination->initialize($config);
        $data['title'] = 'Test&Category association -'.IELTS.'- '.GT;  
        $data['ts_cat_assoc'] = $this->Ts_cat_assoc_model->get_all_ts_cat_assoc_gt($params);
        $data['_view'] = 'ts_cat_assoc/index';
        $this->load->view('layouts/main',$data); 
    }

    /*
     * Listing of ts_cat_assoc
     */
    function index_ee()
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $this->load->library('pagination');
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('ts_cat_assoc/index_ee?');
        $config['total_rows'] = $this->Ts_cat_assoc_model->get_all_ts_cat_assoc_count_ee();
        $this->pagination->initialize($config);
        $data['title'] = 'Test&Category association- '.ENGLISH_ESSENTIAL;  
        $data['ts_cat_assoc'] = $this->Ts_cat_assoc_model->get_all_ts_cat_assoc_ee($params);
        $data['_view'] = 'ts_cat_assoc/index';
        $this->load->view('layouts/main',$data);
    }

    /*
     * Listing of ts_cat_assoc ET
     */
    function index_et()
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $this->load->library('pagination');
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('ts_cat_assoc/index_ee?');
        $config['total_rows'] = $this->Ts_cat_assoc_model->get_all_ts_cat_assoc_count_et();
        $this->pagination->initialize($config);
        $data['title'] = 'Test&Category association -'.IELTS.'- '.ET;  
        $data['ts_cat_assoc'] = $this->Ts_cat_assoc_model->get_all_ts_cat_assoc_et($params);
        $data['_view'] = 'ts_cat_assoc/index';
        $this->load->view('layouts/main',$data);
    }

    /*
     * Listing of ts_cat_assoc ET
     */
    function index_sptest()
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $this->load->library('pagination');
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('ts_cat_assoc/index_sptest?');
        $config['total_rows'] = $this->Ts_cat_assoc_model->get_all_ts_cat_assoc_count_sptest();
        $this->pagination->initialize($config);
        $data['title'] = 'Test&Category association -'.IELTS.'- SP test';  
        $data['ts_cat_assoc'] = $this->Ts_cat_assoc_model->get_all_ts_cat_assoc_sptest($params);
        $data['_view'] = 'ts_cat_assoc/index';
        $this->load->view('layouts/main',$data);
    }
    /*
     * Adding a new ts_cat_assoc
     */
    function add()
    {   
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $data['title'] = 'Add test association';  
        $this->load->library('form_validation');        
		$this->form_validation->set_rules('test_seriese_id','Test name','required');
		$this->form_validation->set_rules('category_id','Category','required');
        $this->form_validation->set_rules('paper_duration','Paper duration','required');
        $this->form_validation->set_rules('max_marks','Max. marks','trim|min_length[1]|max_length[3]|numeric');
        $this->form_validation->set_rules('totalquest','Total question','trim|min_length[1]|max_length[2]|numeric|required');
        $this->form_validation->set_rules('icon','Icon URL','trim');
		
		if($this->form_validation->run())     
        {  
            $user = $this->session->userdata('admin_login_data');
            foreach ($user as $d){$by_user=$d->id;}
            $params = array(
                'test_seriese_id' => $this->input->post('test_seriese_id'),
                'category_id' => $this->input->post('category_id'),
                'paper_duration' => $this->input->post('paper_duration'),
                'max_marks' => $this->input->post('max_marks'),
                'audio_time' => $this->input->post('audio_time'),
                'totalquest' => $this->input->post('totalquest'),
                'icon' => $this->input->post('icon') ? $this->input->post('icon') : NULL,
                'by_user' => $by_user,
            );

            $duplicate_check_id= $this->Ts_cat_assoc_model->check_duplicate_ts_cat_assoc($params['test_seriese_id'], $params['category_id']);
            if($duplicate_check_id){
                $this->session->set_flashdata('flsh_msg', DUP_MSG);
                redirect('ts_cat_assoc/add');
            }else{

                    $config['upload_path']      = AUDIO_FILE_PATH;
                    $config['allowed_types']    = TS_CAT_ASSOC_ALLOWED_TYPES;
                    $config['file_ext_tolower'] = TRUE;
                    $config['overwrite']        = FALSE;
                    $config['encrypt_name'] = FALSE;         
                    $this->load->library('upload',$config);
                    if($params['category_id']==46 or $params['category_id']==50){
                        if($this->upload->do_upload("audio_file")){
                        $data = array('upload_data' => $this->upload->data());
                        $image= site_url(AUDIO_FILE_PATH).$data["upload_data"]["file_name"];

                        $params = array(
                            'test_seriese_id' => $this->input->post('test_seriese_id'),
                            'category_id' => $this->input->post('category_id'),
                            'paper_duration' => $this->input->post('paper_duration'),
                            'max_marks' => $this->input->post('max_marks'),
                            'audio_time' => $this->input->post('audio_time'),
                            'totalquest' => $this->input->post('totalquest'),
                            'audio_file' => $image,
                            'icon' => $this->input->post('icon') ? $this->input->post('icon') : NULL,
                            'by_user' => $by_user,
                        );
                        $id = $this->Ts_cat_assoc_model->add_ts_cat_assoc($params);
                        }else{
                             $this->session->set_flashdata('flsh_msg', FILE_FAILED_MSG);
                             redirect('ts_cat_assoc/add');
                        }
                            if($id){
                                $this->session->set_flashdata('flsh_msg', SUCCESS_MSG);
                                redirect('ts_cat_assoc/add');
                            }else{
                                $this->session->set_flashdata('flsh_msg', FAILED_MSG);
                                redirect('ts_cat_assoc/add');
                            }

                    }else{
                        $params = array(
                            'test_seriese_id' => $this->input->post('test_seriese_id'),
                            'category_id' => $this->input->post('category_id'),
                            'paper_duration' => $this->input->post('paper_duration'),
                            'max_marks' => $this->input->post('max_marks'),
                            'audio_time' => $this->input->post('audio_time'),
                            'totalquest' => $this->input->post('totalquest'),
                            'audio_file' => NULL,
                            'icon' => $this->input->post('icon') ? $this->input->post('icon') : NULL,
                            'by_user' => $by_user,
                        );
                        $id = $this->Ts_cat_assoc_model->add_ts_cat_assoc($params);
                            if($id){
                                $this->session->set_flashdata('flsh_msg', SUCCESS_MSG);
                                redirect('ts_cat_assoc/add');
                            }else{
                                $this->session->set_flashdata('flsh_msg', FAILED_MSG);
                                redirect('ts_cat_assoc/add');
                            }
                    }                    
            }           
            
            
        }
        else
        {			
			$data['all_test_seriese_for_assoc'] = $this->Test_seriese_model->get_all_test_seriese_for_assoc();			
			$data['all_category_masters_for_assoc'] = $this->Category_master_model->get_all_category_masters_active();            
            $data['_view'] = 'ts_cat_assoc/add';
            $this->load->view('layouts/main',$data);
        }
    } 
    /*
     * Editing a ts_cat_assoc
     */
    function edit($ts_cat_assoc_id)
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $data['title'] = 'Edit test association';  
        $data['ts_cat_assoc'] = $this->Ts_cat_assoc_model->get_ts_cat_assoc($ts_cat_assoc_id);
        $category_id =  $data['ts_cat_assoc']['category_id'];
        $data['cat_name'] = $this->Category_master_model->get_category_name_audio($category_id);
        
        if(isset($data['ts_cat_assoc']['ts_cat_assoc_id']))
        {
            $this->load->library('form_validation');        
            $this->form_validation->set_rules('test_seriese_id','Test name','required');
            $this->form_validation->set_rules('category_id','Category','required');
            $this->form_validation->set_rules('paper_duration','Paper duration','required');
            $this->form_validation->set_rules('max_marks','Max. marks','trim|min_length[1]|max_length[3]|numeric');
            $this->form_validation->set_rules('totalquest','Total question','trim|min_length[1]|max_length[2]|numeric|required');
            $this->form_validation->set_rules('icon','Icon URL','trim');

            if($this->form_validation->run())     
            {   
                $user = $this->session->userdata('admin_login_data');
                foreach ($user as $d){$by_user=$d->id;}
                $params = array(
                    'test_seriese_id' => $this->input->post('test_seriese_id'),
                    'category_id' => $this->input->post('category_id'),
                    'paper_duration' => $this->input->post('paper_duration'),
                    'max_marks' => $this->input->post('max_marks'),
                    'audio_time' => $this->input->post('audio_time'),
                    'totalquest' => $this->input->post('totalquest'),
                    'icon' => $this->input->post('icon') ? $this->input->post('icon') : NULL,
                    'by_user' => $by_user,
                );

                    $config['upload_path'] = AUDIO_FILE_PATH;
                    $config['allowed_types']= TS_CAT_ASSOC_ALLOWED_TYPES;
                    $config['file_ext_tolower']=TRUE;
                    $config['overwrite']=FALSE;
                    $config['max_size'] = 0;
                    $config['encrypt_name'] = FALSE;         
                    $this->load->library('upload',$config);

                    if($params['category_id']==46 or $params['category_id']==50){

                        if($this->upload->do_upload("audio_file")){
                        $data = array('upload_data' => $this->upload->data());
                        $image= site_url(AUDIO_FILE_PATH).$data["upload_data"]["file_name"];
                        $params = array(
                            'test_seriese_id' => $this->input->post('test_seriese_id'),
                            'category_id' => $this->input->post('category_id'),
                            'paper_duration' => $this->input->post('paper_duration'),
                            'max_marks' => $this->input->post('max_marks'),
                            'audio_time' => $this->input->post('audio_time'),
                            'totalquest' => $this->input->post('totalquest'),
                            'audio_file' => $image,
                            'icon' => $this->input->post('icon') ? $this->input->post('icon') : NULL,
                            'by_user' => $by_user,
                        );
                        }else{
                            $params = array(
                                'test_seriese_id' => $this->input->post('test_seriese_id'),
                                'category_id' => $this->input->post('category_id'),
                                'paper_duration' => $this->input->post('paper_duration'),
                                'max_marks' => $this->input->post('max_marks'),                    
                                'audio_time' => $this->input->post('audio_time'),
                                'totalquest' => $this->input->post('totalquest'),
                                'icon' => $this->input->post('icon') ? $this->input->post('icon') : NULL,
                                'by_user' => $by_user,
                            );
                            //print_r($params);die;
                        }
                    }
                    $id = $this->Ts_cat_assoc_model->update_ts_cat_assoc($ts_cat_assoc_id,$params);
                    if($id){
                        $this->session->set_flashdata('flsh_msg', UPDATE_MSG);
                        if($params['category_id']>=44 and $params['category_id']<=51){
                           redirect('ts_cat_assoc/index');                        
                        }elseif($params['category_id']==74){
                            redirect('ts_cat_assoc/index_et');
                        }
                        else{
                            redirect('ts_cat_assoc/index_ee');
                        }
                    }else{
                        $this->session->set_flashdata('flsh_msg', UPDATE_FAILED_MSG);           
                        redirect('ts_cat_assoc/edit/'.$ts_cat_assoc_id);
                    }
                
            }
            else
            {
                $this->load->model('Test_seriese_model');
                $data['all_test_seriese_for_assoc'] = $this->Test_seriese_model->get_all_test_seriese_for_assoc();
                $this->load->model('Category_master_model');
                $data['all_category_masters_for_assoc'] = $this->Category_master_model->get_all_category_masters_active();                
                $data['_view'] = 'ts_cat_assoc/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error(ITEM_NOT_EXIST);
    }
    /*
     * Deleting ts_cat_assoc
     */
    function remove($ts_cat_assoc_id)
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $ts_cat_assoc = $this->Ts_cat_assoc_model->get_ts_cat_assoc($ts_cat_assoc_id);
        if(isset($ts_cat_assoc['ts_cat_assoc_id']))
        {
            $this->Ts_cat_assoc_model->delete_ts_cat_assoc($ts_cat_assoc_id);
            $this->session->set_flashdata('flsh_msg', DEL_MSG);
            redirect('ts_cat_assoc/index');
        }
        else
            show_error(ITEM_NOT_EXIST);
    }

    function delete_file(){

        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $id = $this->input->post('id');
        $filepath = $this->Ts_cat_assoc_model->get_file_path($id);
        foreach ($filepath as $f) {
            $path=$f['audio_file'];
        }       
        $params=array(
            'audio_file' => NULL,
            'audio_time' => NULL,
        );
        if(isset($id)){            
            $response = $this->Ts_cat_assoc_model->update_file($id,$params);
            echo json_encode($response);
        }else{
            header('Content-Type: application/json');
            $response = ['msg'=>'Error!', 'status'=>'false'];
            echo json_encode($response);
        }

    }
    
}
