<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/ 
class Package_transaction extends MY_Controller{
    
    function __construct()
    {
        parent::__construct();
        if (!$this->_is_logged_in()) {redirect('/');}        
        $this->load->model('Student_model');
        $this->load->model('Test_module_model');
        $this->load->model('Programe_master_model');
        $this->load->model('Package_master_model');
    }
    /*
     * Listing of all students transaction acd
     */
    function transaction_acd()
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends

        $data['title'] = 'Transaction (Academic)';
        $this->load->library('pagination');
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('Package_transaction/transaction_acd?');
        $config['total_rows'] = $this->Package_master_model->get_all_transaction_count();
        $this->pagination->initialize($config);        
        $data['transaction'] = $this->Package_master_model->get_all_package_history_admin($params);
        $data['_view'] = 'package_transaction/index';
        $this->load->view('layouts/main',$data);
    }

    /*
     * Listing of all students transaction GT
     */
    function transaction_gt()
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends

        $data['title'] = 'Transaction (GT)';
        $this->load->library('pagination');
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('Package_transaction/transaction_gt?');
        $config['total_rows'] = $this->Package_master_model->get_all_transaction_count_gt();
        $this->pagination->initialize($config);        
        $data['transaction'] = $this->Package_master_model->get_all_package_history_admin_gt($params);
        $data['_view'] = 'package_transaction/index';
        $this->load->view('layouts/main',$data);
    }

    
    
}
