<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/ 
class Dashboard_gt extends MY_Controller{

    function __construct()
    {
        parent::__construct();
        if (!$this->_is_logged_in()) {redirect('/');}        
        error_reporting(0); 
        $this->load->model('Programe_master_model');
        $this->load->model('Category_master_model');
        $this->load->model('Test_seriese_model');
        $this->load->model('Student_model');  
        $this->load->model('Student_results_model');
        $this->load->model('Role_model');
        $this->load->model('Center_location_model');
        $this->load->model('Package_master_model');
        $this->load->model('Test_module_model');        
    }

    function index()
    {        
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}        
        //access control ends        
       
        $data['tc_gt']  = $this->Test_seriese_model->get_all_test_seriese_count_gt();
        $data['ic_gt']     = $this->Test_seriese_model->get_all_et_count_gt();
        $data['ptec']   = $this->Test_seriese_model->get_all_pte_count();
        $data['mc_gt']  = $this->Test_seriese_model->get_all_mock_test_count_gt();
        $data['sc_gt']  = $this->Student_model->get_all_students_count_gt();
        $data['ec_gt']  = $this->Student_model->get_all_enquiry_count_gt(); 
        $data['vec_gt']  = $this->Student_model->get_all_visa_enquiry_count_gt(); 
        $data['tpc']    = $this->Student_results_model->checked_paper_count_gt(); 
        $data['tpuc']   = $this->Student_results_model->unchecked_paper_count_gt();
        $data['pkgc_gt']= $this->Package_master_model->get_all_package_masters_count_gt();  

        $user = $this->session->userdata('admin_login_data');
        foreach ($user as $d){$by_user=$d->id;}
        $data['ac']   = $this->Student_results_model->assigned_paper_count($by_user); 
        
        $data['title'] = 'Dashboard (GT)';
        $data['_view'] = 'dashboard_gt';
        $this->load->view('layouts/main',$data);
    }

    
}
