<?php

 
class Live_lecture_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get live_lecture by live_lecture_id
     */
    function get_live_lecture($live_lecture_id)
    {
        return $this->db->get_where('live_lectures',array('live_lecture_id'=>$live_lecture_id))->row_array();
    }


    function dupliacte_live_lecture_check($category_id){

        $this->db->where('category_id', $category_id);        
        $query = $this->db->get('live_lectures');
        $count_row = $query->num_rows();
        if ($count_row > 0) {          
            return 'NOT DUPLICATE';
         } else {          
            
            return 'NOT DUPLICATE';
         }
    }
    
    /*
     * Get all live_lectures count ACD
     */
    function get_all_live_lectures_count()
    {
        $this->db->from('live_lectures ll');
        $this->db->join('`category_masters` cat', 'cat.`category_id`= ll.`category_id`');
        $this->db->join('`programe_masters` pgm', 'cat.`programe_id`= pgm.`programe_id`');
        $this->db->where(array('pgm.programe_id'=> ACD_ID, 'cat.test_module_id'=>IELTS_ID ));
        return $this->db->count_all_results();
    }

    /*
     * Get all live_lectures count GT
     */
    function get_all_live_lectures_count_gt()
    {
        $this->db->from('live_lectures ll');
        $this->db->join('`category_masters` cat', 'cat.`category_id`= ll.`category_id`');
        $this->db->join('`programe_masters` pgm', 'cat.`programe_id`= pgm.`programe_id`');
        $this->db->where(array('pgm.programe_id'=> GT_ID, 'cat.test_module_id'=>IELTS_ID ));
        return $this->db->count_all_results();
    }

    /*
     * Get all live_lectures count EE
     */
    function get_all_live_lectures_count_ee()
    {
        $this->db->from('live_lectures ll');
        $this->db->join('`category_masters` cat', 'cat.`category_id`= ll.`category_id`');
        $this->db->join('`programe_masters` pgm', 'cat.`programe_id`= pgm.`programe_id`');
        $this->db->where(array('pgm.programe_id'=> ACD_ID, 'cat.test_module_id'=>EE_ID ));
        return $this->db->count_all_results();
    }
        
    /*
     * Get all live_lectures ACD
     */
    function get_all_live_lectures($params = array())
    {        
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        $this->db->select('ll.live_lecture_id, ll.live_lecture_title,ll.short_desc,ll.video_url,ll.screenshot,ll.active,
            cat.`category_name`, pgm.`programe_name`,tm.`test_module_name`');
        $this->db->from('`live_lectures` ll');       ;
        $this->db->join('`category_masters` cat', 'll.`category_id`= cat.`category_id`');
        $this->db->join('`programe_masters` pgm', 'cat.`programe_id`= pgm.`programe_id`');
        $this->db->join('`test_module` tm', 'tm.`test_module_id`= cat.`test_module_id`');
        $this->db->where(array('pgm.programe_id'=> ACD_ID, 'cat.test_module_id'=>IELTS_ID));
        $this->db->order_by('live_lecture_id', 'desc');
        return $this->db->get('')->result_array();
    }

    /*
     * Get all live_lectures GT
     */
    function get_all_live_lectures_gt($params = array())
    {        
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        $this->db->select('ll.live_lecture_id, ll.live_lecture_title,ll.short_desc,ll.video_url,ll.screenshot,ll.active,
            cat.`category_name`, pgm.`programe_name`,tm.`test_module_name`');
        $this->db->from('`live_lectures` ll');       ;
        $this->db->join('`category_masters` cat', 'll.`category_id`= cat.`category_id`');
        $this->db->join('`programe_masters` pgm', 'cat.`programe_id`= pgm.`programe_id`');
        $this->db->join('`test_module` tm', 'tm.`test_module_id`= cat.`test_module_id`');
        $this->db->where(array('pgm.programe_id'=> GT_ID, 'cat.test_module_id'=>IELTS_ID));
        $this->db->order_by('live_lecture_id', 'desc');
        return $this->db->get('')->result_array();
    }

    /*
     * Get all live_lectures EE
     */
    function get_all_live_lectures_ee($params = array())
    {        
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        $this->db->select('ll.live_lecture_id, ll.live_lecture_title,ll.short_desc,ll.video_url,ll.screenshot,ll.active,
            cat.`category_name`, pgm.`programe_name`,tm.`test_module_name`');
        $this->db->from('`live_lectures` ll');       ;
        $this->db->join('`category_masters` cat', 'll.`category_id`= cat.`category_id`');
        $this->db->join('`programe_masters` pgm', 'cat.`programe_id`= pgm.`programe_id`');
        $this->db->join('`test_module` tm', 'tm.`test_module_id`= cat.`test_module_id`');
        $this->db->where(array('pgm.programe_id'=> ACD_ID, 'cat.test_module_id'=>EE_ID));
        $this->db->order_by('live_lecture_id', 'desc');
        return $this->db->get('')->result_array();
    }
    

    function get_liveLecture_cat($test_module_id,$programe_id){

        $this->db->select('cat.category_id, cat.`category_name`');
        $this->db->from('`category_masters` cat');        
        $this->db->join('`programe_masters` pgm', 'pgm.`programe_id`= cat.`programe_id`', 'left');
        $this->db->where(array('cat.programe_id'=> $programe_id, 'cat.active'=>1, 'cat.test_module_id'=> $test_module_id  ));
        $this->db->not_like('cat.category_name', 'ET1');
        $this->db->order_by('cat.modified','DESC');
        return $this->db->get('')->result_array();
    }

    function get_liveLecture_cat_for_grammar(){

        $this->db->select('cat.category_id, cat.`category_name`');
        $this->db->from('`category_masters` cat');        
        $this->db->join('`programe_masters` pgm', 'pgm.`programe_id`= cat.`programe_id`', 'left');
        $this->db->where(array('cat.active'=>1, 'cat.test_module_id'=> EE_ID,'cat.category_name'=>'Grammar'));
        return $this->db->get('')->result_array();
    }

    function get_liveLecture($category_id){        
        
        $this->db->select('ll.live_lecture_id, ll.live_lecture_title,ll.short_desc,ll.video_url,ll.screenshot');
        $this->db->from('`live_lectures` ll');
        $this->db->where(array('ll.category_id'=> $category_id,'ll.active'=>1));
        $this->db->order_by('live_lecture_id', 'desc');
        return $this->db->get('')->result_array();
    }

    function recommended_live_lecture($test_module_id,$programe_id){

        $this->db->select('ll.live_lecture_id, ll.live_lecture_title,ll.short_desc,ll.video_url,ll.screenshot');
        $this->db->from('`live_lectures` ll');
        $this->db->join('`category_masters` cat', 'll.`category_id`= cat.`category_id`');
        $this->db->join('`programe_masters` pgm', 'cat.`programe_id`= pgm.`programe_id`');
        $this->db->join('`test_module` tm', 'tm.`test_module_id`= cat.`test_module_id`');
        if($test_module_id==EE_ID){
            $this->db->where(array('ll.active'=>1,'tm.test_module_id'=>$test_module_id));
        }else{
            $this->db->where(array('pgm.programe_id'=> $programe_id,'ll.active'=>1,'tm.test_module_id'=>$test_module_id));
        }        
        $this->db->order_by('cat.modified', 'DESC');
        $this->db->limit(RECOMMENDED_LL_LIMIT);
        return $this->db->get('')->result_array();
    }
        
    /*
     * function to add new live_lecture
     */
    function add_live_lecture($params)
    {
        $this->db->insert('live_lectures',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update live_lecture
     */
    function update_live_lecture($live_lecture_id,$params)
    {
        $this->db->where('live_lecture_id',$live_lecture_id);
        return $this->db->update('live_lectures',$params);
    }
    
    /*
     * function to delete live_lecture
     */
    function delete_live_lecture($live_lecture_id)
    {
        return $this->db->delete('live_lectures',array('live_lecture_id'=>$live_lecture_id));
    }
}
