<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/
 
class City_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get city by city_id
     */
    function get_city($city_id)
    {
        return $this->db->get_where('city',array('city_id'=>$city_id))->row_array();
    }
    
    /*
     * Get all city count
     */
    function get_all_city_count()
    {
        $this->db->from('city');
        return $this->db->count_all_results();
    }
        
    /*
     * Get all city
     */
    function get_all_city($params = array())
    {        
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }  
        $this->db->select('st.`state_id`,st.`state_name`,cnt.`name`,c.city_id,c.`city_name`,c.`state_id`,c.`country_id`,c.`active`');
        $this->db->from('city c'); 
        $this->db->join('`country` cnt', 'c.`country_id`=cnt.`country_id`'); 
        $this->db->join('`state` st', 'st.`state_id`=c.`state_id`');       
        $this->db->order_by('cnt.`name`', 'ASC');
        return $this->db->get()->result_array();
    } 
    

    function get_all_city_active($params = array()){
       
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }  
        $this->db->select('st.`state_id`,st.`state_name`,cnt.`name`,c.city_id,c.`city_name`,c.`state_id`,c.`country_id`');
        $this->db->from('city c'); 
        $this->db->join('`country` cnt', 'st.`country_id`=cnt.`country_id`'); 
        $this->db->join('`state` st', 'st.`state_id`=c.`state_id`');       
        $this->db->order_by('cnt.`name`', 'ASC');
        return $this->db->get()->result_array();
    }    
        
    /*
     * function to add new city
     */
    function add_city($params)
    {       
       
        $country_id =  $params['country_id'];
        $state_id =  $params['state_id'];
        $city_name =  $params['city_name'];
        $this->db->where('city_name', $city_name);
        $this->db->where('country_id', $country_id);
        $this->db->where('state_id', $state_id);
        $query = $this->db->get('city');
        $count_row = $query->num_rows();
        if ($count_row > 0) {          
            return FALSE;
        }else{
          
            $this->db->insert('city',$params);
            return $this->db->insert_id();
         }
    }

    
    /*
     * function to update city
     */
    function update_city($city_id,$params)
    {
        $this->db->where('city_id',$city_id);
        return $this->db->update('city',$params);
    }
    
    /*
     * function to delete city
     */
    function delete_city($city_id)
    {
        return $this->db->delete('city',array('city_id'=>$city_id));
    }

    function get_city_list($state_id){       
        
        $this->db->select('city_id,city_name');
        $this->db->from('city');
        $this->db->where(array('state_id'=> $state_id,'active'=>1));  
        $this->db->order_by('city_name', 'desc');
        return $this->db->get()->result_array();
    }
    
}
