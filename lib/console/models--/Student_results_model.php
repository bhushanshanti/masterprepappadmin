<?php

class Student_results_model extends CI_Model
{
    
    function __construct()
    {
        parent::__construct();
    }    

    function update_result_marks($collection_no,$marks_secured,$band_score,$percentage,$by_user,$test_seriese_id,$ts_cat_assoc_id,$category_name,$pro_id)
    {

        $this->db->query("update `student_results` set `marks_secured` = '".$marks_secured."', `band_score` = '".$band_score."', `percentage` = '".$percentage."', `by_user` = '".$by_user."', `pro_id` = '".$pro_id."',`paper_checked` = 1 where collection_no ='".$collection_no."' ");
        //print_r($this->db->last_query());exit;

        if($category_name==READING or $category_name==LISTENING or $category_name==EX1 or $category_name==EX2 or $category_name==EX3 or $category_name==EX4 or $category_name==EX5){

            //update rank
            $query  =   "SET @currentRank := 0, @lastRating := null, @rowNumber := 1";
            $this->db->query($query);
            $sql = "update 
            `student_results` r
            inner join (
                select
                    `student_result_id`,
                    @currentRank := if(@lastRating = `marks_secured`, @currentRank, @rowNumber) `rank`,
                    @rowNumber := @rowNumber + if(@lastRating = `marks_secured`, 0, 1) `rowNumber`,
                    @lastRating := `marks_secured`
                from `student_results`
                where `test_seriese_id` = ".$test_seriese_id." and `ts_cat_assoc_id` = ".$ts_cat_assoc_id."
                order by `marks_secured` desc
            )var on
                var.`student_result_id` = r.`student_result_id`
            set r.`rank` = var.`rank`";
            return $this->db->query($sql);

        }elseif($category_name==WRITING or $category_name==SPEAKING){

             //update rank
            $query  =   "SET @currentRank := 0, @lastRating := null, @rowNumber := 1";
            $this->db->query($query);
            $sql = "update 
            `student_results` r
            inner join (
                select
                    `student_result_id`,
                    @currentRank := if(@lastRating = `band_score`, @currentRank, @rowNumber) `rank`,
                    @rowNumber := @rowNumber + if(@lastRating = `band_score`, 0, 1) `rowNumber`,
                    @lastRating := `band_score`
                from `student_results`
                where `test_seriese_id` = ".$test_seriese_id." and `ts_cat_assoc_id` = ".$ts_cat_assoc_id."
                order by `band_score` desc
            )var on
                var.`student_result_id` = r.`student_result_id`
            set r.`rank` = var.`rank`";
            return $this->db->query($sql);

        }else{
            
        }
    } 

    function checked_paper_count()
    {        

        $this->db->from('student_results');
        $this->db->where(array('paper_checked'=>1,'programe_id'=>ACD_ID));
        return $this->db->count_all_results();
    }

    function checked_paper_count_gt()
    {        

        $this->db->from('student_results');
        $this->db->where(array('paper_checked'=>1,'programe_id'=>GT_ID));
        return $this->db->count_all_results();
    } 

    function unchecked_paper_count()
    {    

        $this->db->from('student_results');
        $this->db->where(array('paper_checked'=>NULL,'programe_id'=>ACD_ID));
        return $this->db->count_all_results();
    }

    function unchecked_paper_count_gt()
    {    

        $this->db->from('student_results');
        $this->db->where(array('paper_checked'=>NULL,'programe_id'=>GT_ID));
        return $this->db->count_all_results();
    } 

    function assigned_paper_count($by_user)
    {
        $this->db->from('student_results');
        $this->db->where(array('assigned_to'=> $by_user));
        return $this->db->count_all_results();
    } 

    function get_distinct_given_test($id,$test_module_id,$programe_id){

        $this->db->distinct('');
        $this->db->select('test_seriese_id');
        $this->db->from('student_results');
        $this->db->where(array('student_id'=>$id,'test_module_id'=>$test_module_id,'programe_id'=>$programe_id));
        $this->db->not_like('collection_no', 'ET');
        $this->db->not_like('collection_no', 'MOCK');
        $this->db->order_by('created', 'DESC');
        return $this->db->get('')->row_array();
        //print_r($this->db->last_query());exit;
    }

    function get_count_distinct_given_tscat($id,$test_seriese_id,$test_module_id){
       
        $this->db->distinct('');
        $this->db->select('ts_cat_assoc_id');
        $this->db->from('student_results');
        $this->db->where(array('student_id' => $id,'test_seriese_id' => $test_seriese_id,'test_module_id' => $test_module_id));
        $this->db->not_like('collection_no', 'ET');
        $this->db->not_like('collection_no', 'MOCK');
        return $this->db->get('')->result_array();
    }

    function get_incompleted_tests($arr,$test_seriese_id){
        
        $this->db->select('
            ts_cat.ts_cat_assoc_id,
            ts_cat.test_seriese_id,
            ts_cat.category_id,
            cat.category_name,
            ts.test_seriese_name,
            ts.test_seriese_type,'
        );
        $this->db->from('ts_cat_assoc ts_cat');
        $this->db->join('category_masters cat', 'cat.category_id = ts_cat.category_id');
        $this->db->join('test_seriese ts', 'ts.test_seriese_id = ts_cat.test_seriese_id');
        $this->db->where(array('ts.test_seriese_id'=>$test_seriese_id));
        $this->db->where_not_in('ts_cat.ts_cat_assoc_id', $arr);
        return $this->db->get('')->result_array();
    }

    function getRank($programe_id,$test_module_id,$id){

        $rank1 = base_url('uploads/gallery/Rank1.jpg');        
        $one = 1;        
        $this->db->select('
            std.profile_pic,
            std.fname,
            FORMAT(AVG(sr.`rank`), 0) as `rankx`,
            IF(std.`id` = '.$id.', "A", "Z") as `pro`, 
            IF( AVG(sr.`rank`) = '.$one.', "'.$rank1.'", "") as `icon`,     
        ');
        $this->db->from('`student_results` sr');
        $this->db->join('`students` std', 'std.`id`= sr.`student_id`');
        $this->db->where(array('sr.`test_module_id`'=>$test_module_id, 'sr.`programe_id`'=>$programe_id));
        $this->db->group_by('sr.`student_id`');
        $this->db->order_by('`pro` ASC,`rankx` ASC');
        return $this->db->get('')->result_array();
    }

    function getGraphData($programe_id,$test_module_id,$id,$category_name){

        $this->db->select('
           `collection_no`,`band_score`,`created`     
        ');
        $this->db->from('`student_results`');
        $this->db->where(array('`test_module_id`'=>$test_module_id, '`programe_id`'=>$programe_id,'student_id'=>$id,'paper_checked'=>1));
        $this->db->like('collection_no', $category_name);
        $this->db->order_by('`created` ASC');
        return $this->db->get('')->result_array();
    }
    
}
