<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/
 
class Test_type_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function get_all_test_type_active($params = array()){
       
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        } 
        $this->db->select('*');
        $this->db->from('test_type');        
        $this->db->where('active', 1);  
        $this->db->order_by('test_type_id', 'desc');
        return $this->db->get()->result_array();
    }
        
    
}
