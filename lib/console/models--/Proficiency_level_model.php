<?php

 
class Proficiency_level_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    /*
     * Get proficiency_level by pro_id
     */
    function get_proficiency_level($pro_id)
    {
        return $this->db->get_where('proficiency_level',array('pro_id'=>$pro_id))->row_array();
    }
    /*
     * Get all proficiency_level count
     */
    function get_all_proficiency_level_count()
    {
        $this->db->from('proficiency_level');
        return $this->db->count_all_results();
    }  
    /*
     * Get all proficiency_level
     */
    function get_all_proficiency_level($params = array())
    {
        $this->db->order_by('pro_id', 'desc');
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        return $this->db->get('proficiency_level')->result_array();
    }

    function get_all_proficiency_level_active($params = array())
    {
        $this->db->where('active', '1');
        $this->db->order_by('pro_id', 'DESC');
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        return $this->db->get('proficiency_level')->result_array();
    }
    /*
     * function to add new proficiency_level
     */
    function add_proficiency_level($params)
    {
        $this->db->insert('proficiency_level',$params);
        return $this->db->insert_id();
    }    
    /*
     * function to update proficiency_level
     */
    function update_proficiency_level($pro_id,$params)
    {
        $this->db->where('pro_id',$pro_id);
        return $this->db->update('proficiency_level',$params);
    } 
    /*
     * function to delete proficiency_level
     */
    function delete_proficiency_level($pro_id)
    {
        return $this->db->delete('proficiency_level',array('pro_id'=>$pro_id));
    }

    
}
