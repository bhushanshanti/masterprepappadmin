<?php

 
class Student_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function mobile_no_exists($mobile){
        $this->db->select('id');
        return $this->db->get_where('students',array('mobile'=>$mobile))->result_array();
    }

    function check_status($mobile){

        $this->db->select('active');
        return $this->db->get_where('students',array('mobile'=>$mobile))->result_array();
    }
    
    /*
     * Get student by student_id
     */
    function get_student($id)
    {
        return $this->db->get_where('students',array('id'=>$id))->row_array();
    }

    /*
     * Get student by student_id
     */
    function get_student_info($id)
    { 
        $this->db->distinct('');
        $this->db->select('s.id,s.programe_id,s.fname,s.lname,s.dob,s.gender,s.email,s.mobile,s.residential_address, pgm.`programe_name`,g.`gender_name`');
        $this->db->from('`students` s');        
        $this->db->join('`programe_masters` pgm', 's.`programe_id`= pgm.`programe_id`', 'left'); 
        $this->db->join('`student_results` sr', 'sr.`student_id`= s.`id`', 'left'); 
        $this->db->join('`gender` g', 'g.`id`= s.`gender`', 'left');
        $this->db->where(array('s.id'=>$id));     
        return $this->db->get('')->result_array();      
    }

    
    function get_student_info_forSMS($id)
    {       
        $this->db->select('fname,email,mobile');
        $this->db->from('`students`');   
        $this->db->where(array('id'=>$id));     
        return $this->db->get('')->row_array();      
    }

    function getMockHistory($id,$programe_id,$test_module_id)
    {       
        $this->db->select('booking_date,booking_time,active,created');
        $this->db->from('`mock_test_bookings`');   
        $this->db->where(array('student_id'=> $id,'programe_id'=> $programe_id,'test_module_id'=> $test_module_id));     
        return $this->db->get('')->result_array();      
    }
    
    /*
     * Get all students count acad
     */
    function get_all_students_count()
    {
        $this->db->from('students');
        $this->db->where(array('programe_id'=> ACD_ID));
        return $this->db->count_all_results();
    } 

    /*
     * Get all students count acad
     */
    function get_all_students_count_fresh()
    {
        $this->db->from('students');
        $this->db->where(array('programe_id'=>0));
        return $this->db->count_all_results();
    }  

    /*
     * Get all students count GT
     */
    function get_all_students_count_gt()
    {
        $this->db->from('students');
        $this->db->where(array('programe_id'=>GT_ID));
        return $this->db->count_all_results();
    }

    /*
     * Get all students enquiry count ACD
     */
    function get_all_enquiry_count()
    {
        $this->db->from('students_enquiry');
        $this->db->where(array('programe_id'=>ACD_ID));
        return $this->db->count_all_results();
    }

    /*
     * Get all students enquiry count GT
     */
    function get_all_enquiry_count_gt()
    {
        $this->db->from('students_enquiry');
        $this->db->where(array('programe_id'=>GT_ID));
        return $this->db->count_all_results();
    }

    /*
     * Get all students visa enquiry count ACD
     */
    function get_all_visa_enquiry_count()
    {
        $this->db->from('students_visa_enquiry');
        $this->db->where(array('programe_id'=>ACD_ID));
        return $this->db->count_all_results();
    }

    /*
     * Get all students visa enquiry count GT
     */
    function get_all_visa_enquiry_count_gt()
    {
        $this->db->from('students_visa_enquiry');
        $this->db->where(array('programe_id'=>GT_ID));
        return $this->db->count_all_results();
    }
        
    /*
     * Get all students Acad
     */
    function get_all_students($params = array())
    {        
        
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        $this->db->select('s.id,s.token,s.fname,s.lname,s.dob,s.gender,s.email,s.mobile,s.residential_address,s.profile_pic,s.active, pgm.`programe_name`,g.`gender_name`, count(sr.student_result_id) as cnt ');
        $this->db->from('`students` s');        
        $this->db->join('`programe_masters` pgm', 's.`programe_id`= pgm.`programe_id`', 'left'); 
        $this->db->join('`student_results` sr', 'sr.`student_id`= s.`id`', 'left'); 
        $this->db->join('`gender` g', 'g.`id`= s.`gender`', 'left');
        $this->db->where(array('s.programe_id'=>ACD_ID));
        $this->db->order_by('id', 'desc');
        $this->db->group_by('s.id');     
        return $this->db->get('')->result_array();
    }

    function get_all_enquiry($params = array(),$programe_id){

        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        $this->db->select('*');
        $this->db->from('`students_enquiry`'); 
        $this->db->where(array('programe_id'=>$programe_id));
        $this->db->order_by('enquiry_id', 'DESC'); 
        return $this->db->get('')->result_array();

    }

    function get_all_visa_enquiry($params = array(),$programe_id){

        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        $this->db->select('*');
        $this->db->from('`students_visa_enquiry` sv'); 
        //$this->db->join('`students_visa_enquiry_reply` svr', 'sv.`visa_enquiry_id`= svr.`visa_enquiry_id`', 'left');
        $this->db->where(array('sv.programe_id'=>$programe_id));
        $this->db->order_by('sv.visa_enquiry_id', 'DESC'); 
        return $this->db->get('')->result_array();

    }

    function get_enquiry($enquiry_id){

        return $this->db->get_where('students_enquiry',array('enquiry_id'=>$enquiry_id))->row_array();
    }

    function get_visa_enquiry($visa_enquiry_id){

        return $this->db->get_where('students_visa_enquiry',array('visa_enquiry_id'=>$visa_enquiry_id))->row_array();
    }

    function get_std_enquiry($id){

        return $this->db->get_where('students_enquiry',array('student_id'=>$id))->result_array();
    }

    function get_preReplies($enquiry_id){

        return $this->db->get_where('students_enquiry_reply',array('enquiry_id'=>$enquiry_id))->result_array();
    }

    function get_visa_preReplies($visa_enquiry_id){

        return $this->db->get_where('students_visa_enquiry_reply',array('visa_enquiry_id'=>$visa_enquiry_id))->result_array();
    }

    function get_all_reply($enquiry_id){

        $this->db->select('
            se.message as student_query,
            date_format(se.created, "%D %b %y %I:%i %p") as sent_on,
            ser.admin_reply,
            date_format(ser.created, "%D %b %y %I:%i %p") as replied_on
        ');
        $this->db->from('`students_enquiry` se');       
        $this->db->join('`students_enquiry_reply` ser', 'se.`enquiry_id`= ser.`enquiry_id`', 'left');
        $this->db->where(array('se.enquiry_id'=>$enquiry_id));
        $this->db->order_by('ser.created', 'DESC');
        return $this->db->get('')->result_array();
    }

    function get_all_VISA_reply($VISAenquiryId){

        $this->db->select('
            sv.country_intrested,sv.VISA_type,
            date_format(sv.created, "%D %b %y %I:%i %p") as sent_on,
            svr.admin_reply,
            date_format(svr.created, "%D %b %y %I:%i %p") as replied_on
        ');
        $this->db->from('`students_visa_enquiry` sv');       
        $this->db->join('`students_visa_enquiry_reply` svr', 'sv.`visa_enquiry_id`= svr.`visa_enquiry_id`', 'left');
        $this->db->where(array('sv.visa_enquiry_id'=> $VISAenquiryId));
        $this->db->order_by('svr.created', 'DESC');
        return $this->db->get('')->result_array();
    }

    function get_std_inbox($id,$programe_id,$test_module_id){

        $this->db->select('
            se.message as student_query,
            date_format(se.created, "%D %b %y %I:%i %p") as sent_on,
            ser.admin_reply,
            date_format(ser.created, "%D %b %y %I:%i %p") as replied_on
        ');
        $this->db->from('`students_enquiry` se');       
        $this->db->join('`students_enquiry_reply` ser', 'se.`enquiry_id`= ser.`enquiry_id`', 'left');
        $this->db->where(array('se.student_id'=> $id,'se.programe_id'=> $programe_id,'se.test_module_id'=> $test_module_id));
        $this->db->order_by('ser.created', 'DESC');
        return $this->db->get('')->result_array();
    }

    function get_std_visa_inbox($id,$programe_id,$test_module_id){

        $this->db->select('
            sv.country_intrested,sv.VISA_type,
            date_format(sv.created, "%D %b %y %I:%i %p") as sent_on,
            svr.admin_reply,
            date_format(svr.created, "%D %b %y %I:%i %p") as replied_on
        ');
        $this->db->from('`students_visa_enquiry` sv');       
        $this->db->join('`students_visa_enquiry_reply` svr', 'sv.`visa_enquiry_id`= svr.`visa_enquiry_id`', 'left');
        $this->db->where(array('sv.student_id'=> $id,'sv.programe_id'=> $programe_id,'sv.test_module_id'=> $test_module_id));
        $this->db->order_by('svr.created', 'DESC');
        return $this->db->get('')->result_array();
    }

    /*
     * Get all students Acad
     */
    function get_all_students_fresh($params = array())
    {        
        
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        $this->db->select('s.id,s.token,s.fname,s.lname,s.dob,s.gender,s.email,s.mobile,s.residential_address,s.profile_pic,s.active, pgm.`programe_name`,g.`gender_name`, count(sr.student_result_id) as cnt ');
        $this->db->from('`students` s');        
        $this->db->join('`programe_masters` pgm', 's.`programe_id`= pgm.`programe_id`', 'left'); 
        $this->db->join('`student_results` sr', 'sr.`student_id`= s.`id`', 'left'); 
        $this->db->join('`gender` g', 'g.`id`= s.`gender`', 'left');
        $this->db->where(array('s.programe_id'=>0));
        $this->db->order_by('id', 'desc');
        $this->db->group_by('s.id');     
        return $this->db->get('')->result_array();
    }
    
    function get_student_name($student_id){

        $this->db->select('fname,mobile,email');
        $this->db->from('`students`'); 
        $this->db->where(array('id'=>$student_id));     
        return $this->db->get('')->row_array();
    }

    /*
     * Get all students Acad
     */
    function get_all_students_gt($params = array())
    {        
        $this->db->order_by('id', 'desc');
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        $this->db->select('s.id,s.token,s.fname,s.lname,s.dob,s.gender,s.email,s.mobile,s.residential_address,s.profile_pic,s.active, pgm.`programe_name`,g.`gender_name`, count(sr.student_result_id) as cnt ');
        $this->db->from('`students` s');        
        $this->db->join('`programe_masters` pgm', 's.`programe_id`= pgm.`programe_id`', 'left'); 
        $this->db->join('`student_results` sr', 'sr.`student_id`= s.`id`', 'left'); 
        $this->db->join('`gender` g', 'g.`id`= s.`gender`', 'left');
        $this->db->where(array('s.programe_id'=> GT_ID));
        $this->db->group_by('s.id');     
        return $this->db->get('')->result_array();
    }
        
    /*
     * function to add new student
     */
    function add_student($params)
    {
        $this->db->insert('students',$params);
        return $this->db->insert_id();
    }

    /*
     * function to add reply
     */
    function add_reply($params)
    {
        $this->db->insert('students_enquiry_reply',$params);
        return $this->db->insert_id();
    }

    /*
     * function to add VISA reply
     */
    function add_visa_reply($params)
    {
        $this->db->insert('students_visa_enquiry_reply',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update student
     */
    function update_student($id,$params)
    {
        $this->db->where('id',$id);
        return $this->db->update('students',$params);
    }
    
    /*
     * function to delete student
    */
    function delete_student($id)
    {
        return $this->db->delete('students',array('id'=>$id));
    }

    function getGoal($token){

        $this->db->select('
            Goal_l,
            Goal_r,
            Goal_w,
            Goal_s,
            Goal_avg,
            date_format(targateDate, "%D %b %y") as `goalDate`,
            targateDate
        ');
        $this->db->from('`students`'); 
        $this->db->where(array('token'=> $token));
        return $this->db->get('')->row_array();
    }
}
