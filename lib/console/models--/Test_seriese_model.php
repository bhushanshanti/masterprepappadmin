<?php
/* 
 * Author: Mohammad haroon 
*/
 
class Test_seriese_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get test_seriese by test_seriese_id
     */
    function get_test_seriese($test_seriese_id)
    {
        return $this->db->get_where('test_seriese',array('test_seriese_id'=>$test_seriese_id))->row_array();
    }

    function get_programe_id($test_seriese_id){

        $this->db->select("programe_id");
        $this->db->from('test_seriese');       
        $this->db->where(array('test_seriese_id' => $test_seriese_id));        
        return $this->db->get()->row_array();

    }

    function get_test_module_id($test_seriese_id)
    {
        $this->db->select("test_module_id,is_MtEt");
        $this->db->from('test_seriese');       
        $this->db->where(array('test_seriese_id' => $test_seriese_id));        
        return $this->db->get()->row_array();
    }

    function get_paper_type($test_seriese_id)
    {
        $this->db->select("test_seriese_type");
        $this->db->from('test_seriese');       
        $this->db->where(array('test_seriese_id' => $test_seriese_id));        
        return $this->db->get()->row_array();
    }

    function get_test_name($test_seriese_id)
    {
        $this->db->select('ts.test_seriese_name, tm.test_module_name');
        $this->db->from('test_seriese ts');
        $this->db->join('test_module tm', 'tm.test_module_id = ts.test_module_id', 'left');       
        $this->db->where(array('test_seriese_id' => $test_seriese_id));        
        return $this->db->get()->row_array();
    }
    
    /*
     * Get all test_seriese count Academic
     */
    function get_all_test_seriese_count()
    {        
        $this->db->from('ts_cat_assoc ts_cat');
        $this->db->join('test_seriese ts', 'ts.test_seriese_id=ts_cat.test_seriese_id', 'left');
        $this->db->join('test_module tm', 'tm.test_module_id=ts.test_module_id', 'left');
        $this->db->where(array('tm.test_module_name'=>IELTS,'ts.programe_id'=>ACD_ID,'ts.is_MtEt'=>NULL));
        return $this->db->count_all_results();        
    }        

    /*
     * Get all test_seriese count GT
     */
    function get_all_test_seriese_count_gt()
    {        
        $this->db->from('ts_cat_assoc ts_cat');
        $this->db->join('test_seriese ts', 'ts.test_seriese_id=ts_cat.test_seriese_id', 'left');
        $this->db->join('test_module tm', 'tm.test_module_id=ts.test_module_id', 'left');
        $this->db->where(array('tm.test_module_name'=>IELTS,'ts.programe_id'=>GT_ID,'ts.is_MtEt'=>NULL));
        return $this->db->count_all_results();         
    }

    /*
     * Get all EE count
     */
    function get_all_englis_essential_count()
    {
        $this->db->from('ts_cat_assoc ts_cat');
        $this->db->join('test_seriese ts', 'ts.test_seriese_id=ts_cat.test_seriese_id', 'left');
        $this->db->join('test_module tm', 'tm.test_module_id=ts.test_module_id', 'left');
        $this->db->where(array('tm.test_module_name'=>ENGLISH_ESSENTIAL,'ts.programe_id'=> ACD_ID,'ts.is_MtEt'=>NULL));
        return $this->db->count_all_results();        
    }

    /*
     * Get all ET count ACD
     */
    function get_all_et_count()
    {
        $this->db->from('ts_cat_assoc ts_cat');
        $this->db->join('test_seriese ts', 'ts.test_seriese_id=ts_cat.test_seriese_id', 'left');
        $this->db->join('test_module tm', 'tm.test_module_id=ts.test_module_id', 'left');
        $this->db->where(array('ts.is_MtEt'=>1,'ts.programe_id'=>ACD_ID));
        return $this->db->count_all_results();
    }

    /*
     * Get all ET count GT
     */
    function get_all_et_count_gt()
    {       
        $this->db->from('ts_cat_assoc ts_cat');
        $this->db->join('test_seriese ts', 'ts.test_seriese_id=ts_cat.test_seriese_id', 'left');
        $this->db->join('test_module tm', 'tm.test_module_id=ts.test_module_id', 'left');
        $this->db->where(array('ts.is_MtEt'=>1,'ts.programe_id'=>GT_ID));
        return $this->db->count_all_results();
    }

    /*
     * Get all ET count ACD
     */
    function get_all_mt_count()
    {       
        $this->db->from('ts_cat_assoc ts_cat');
        $this->db->join('test_seriese ts', 'ts.test_seriese_id=ts_cat.test_seriese_id', 'left');
        $this->db->join('test_module tm', 'tm.test_module_id=ts.test_module_id', 'left');
        $this->db->where(array('ts.is_MtEt'=>2,'ts.programe_id'=>ACD_ID));
        return $this->db->count_all_results();
    }

    /*
     * Get all ET count ACD
     */
    function get_all_mt_count_gt()
    {
       
        $this->db->from('ts_cat_assoc ts_cat');
        $this->db->join('test_seriese ts', 'ts.test_seriese_id=ts_cat.test_seriese_id', 'left');
        $this->db->join('test_module tm', 'tm.test_module_id=ts.test_module_id', 'left');
        $this->db->where(array('ts.is_MtEt'=>2,'ts.programe_id'=>GT_ID));
        return $this->db->count_all_results();
    }    

     /*
     * Get all ET count ACD
     */
    function get_all_spTest_count()
    {
       
        $this->db->from('ts_cat_assoc ts_cat');
        $this->db->join('test_seriese ts', 'ts.test_seriese_id=ts_cat.test_seriese_id', 'left');
        $this->db->join('test_module tm', 'tm.test_module_id=ts.test_module_id', 'left');
        $this->db->where(array('ts.is_MtEt'=>3));
        return $this->db->count_all_results();
    }

    /*
     * Get all PTE count
     */
    function get_all_pte_count()
    {
        $this->db->from('test_seriese ts');
        $this->db->join('test_module tm', 'tm.test_module_id=ts.test_module_id', 'left');
        $this->db->where(array('tm.test_module_name'=>PTE,'ts.is_MtEt'=>NULL));
        return $this->db->count_all_results();
    }

    /*
     * Get all Mock test count Academic
     */
    function get_all_mock_test_count()
    {        
        $this->db->from('ts_cat_assoc ts_cat');
        $this->db->join('test_seriese ts', 'ts.test_seriese_id=ts_cat.test_seriese_id', 'left');
        $this->db->join('test_module tm', 'tm.test_module_id=ts.test_module_id', 'left');
        $this->db->where(array('ts.is_MtEt'=>2, 'ts.programe_id'=>ACD_ID));
        return $this->db->count_all_results();        
    }

    /*
     * Get all Mock test count Academic
     */
    function get_all_mock_test_count_gt()
    {        
        $this->db->from('ts_cat_assoc ts_cat');
        $this->db->join('test_seriese ts', 'ts.test_seriese_id=ts_cat.test_seriese_id', 'left');
        $this->db->join('test_module tm', 'tm.test_module_id=ts.test_module_id', 'left');
        $this->db->where(array('ts.is_MtEt'=>2,'ts.programe_id'=>GT_ID));
        return $this->db->count_all_results();        
    }

      
    /*
     * Get all test_seriese ACD
     */
    function get_all_test_seriese($params = array())
    {
        
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        $this->db->select('ts.`test_seriese_name`, ts.`test_seriese_type`, ts.`test_seriese_id`,ts.`active`,ts.`test_seriese_color`,ts.`icon`,ts.`test_module_id`,tm.`test_module_id`,tm.`test_module_name`,
        ts_cat.`ts_cat_assoc_id`,ts_cat.`category_id`,ts_cat.`paper_duration`,ts_cat.`max_marks`,ts_cat.`audio_file`,ts_cat.`totalquest`,cat.`category_name`,pgm.`programe_name`,pgm.`programe_id`,       
        (select group_concat(sec.`section_heading` SEPARATOR " | ") from sections sec where sec.`ts_cat_assoc_id` in (ts_cat.`ts_cat_assoc_id`)) as sec_head');
        $this->db->from('`test_seriese` ts');
        $this->db->join('`ts_cat_assoc` ts_cat', 'ts.`test_seriese_id`=ts_cat.`test_seriese_id`', 'left');
        $this->db->join('`category_masters` cat', 'ts_cat.`category_id`=cat.`category_id`', 'left');
        $this->db->join('`test_module` tm', 'tm.`test_module_id`=ts.`test_module_id`', 'left');
        $this->db->where(array('tm.test_module_name'=>IELTS,'ts.programe_id'=>ACD_ID,'ts.is_MtEt'=>NULL));
        $this->db->join('`programe_masters` pgm', 'cat.`programe_id`=pgm.`programe_id`', 'left');
        $this->db->order_by('ts.`display_order` ASC');
        return $this->db->get()->result_array();
    }

    /*
     * Get all test_seriese GT
     */
    function get_all_test_seriese_gt($params = array())
    {
        
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        $this->db->select('ts.`test_seriese_name`, ts.`test_seriese_type`, ts.`test_seriese_id`,ts.`active`,ts.`test_seriese_color`,ts.`icon`,ts.`test_module_id`,tm.`test_module_id`,tm.`test_module_name`,
        ts_cat.`ts_cat_assoc_id`,ts_cat.`category_id`,ts_cat.`paper_duration`,ts_cat.`max_marks`,ts_cat.`audio_file`,ts_cat.`totalquest`,cat.`category_name`,pgm.`programe_name`,pgm.`programe_id`,       
        (select group_concat(sec.`section_heading` SEPARATOR " | ") from sections sec where sec.`ts_cat_assoc_id` in (ts_cat.`ts_cat_assoc_id`)) as sec_head');
        $this->db->from('`test_seriese` ts');
        $this->db->join('`ts_cat_assoc` ts_cat', 'ts.`test_seriese_id`=ts_cat.`test_seriese_id`', 'left');
        $this->db->join('`category_masters` cat', 'ts_cat.`category_id`=cat.`category_id`', 'left');
        $this->db->join('`test_module` tm', 'tm.`test_module_id`=ts.`test_module_id`', 'left');
        $this->db->where(array('tm.test_module_name'=>IELTS,'ts.programe_id'=>GT_ID,'ts.is_MtEt'=>NULL));
        $this->db->join('`programe_masters` pgm', 'cat.`programe_id`=pgm.`programe_id`', 'left');
        $this->db->order_by('ts.`display_order` ASC');
        return $this->db->get()->result_array();
    }


    /*
     * Get all test_seriese ET
     */
    function get_all_test_seriese_et($params = array())
    {
        
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        $this->db->select('ts.`test_seriese_name`, ts.`test_seriese_type`, ts.`test_seriese_id`,ts.`active`,ts.`test_seriese_color`,ts.`icon`,ts.`test_module_id`,tm.`test_module_id`,tm.`test_module_name`,
        ts_cat.`ts_cat_assoc_id`,ts_cat.`category_id`,ts_cat.`paper_duration`,ts_cat.`max_marks`,ts_cat.`audio_file`,ts_cat.`totalquest`,cat.`category_name`,pgm.`programe_name`,pgm.`programe_id`,       
        (select group_concat(sec.`section_heading` SEPARATOR " | ") from sections sec where sec.`ts_cat_assoc_id` in (ts_cat.`ts_cat_assoc_id`)) as sec_head');
        $this->db->from('`test_seriese` ts');
        $this->db->join('`ts_cat_assoc` ts_cat', 'ts.`test_seriese_id`=ts_cat.`test_seriese_id`', 'left');
        $this->db->join('`category_masters` cat', 'ts_cat.`category_id`=cat.`category_id`', 'left');
        $this->db->join('`test_module` tm', 'tm.`test_module_id`=ts.`test_module_id`', 'left');
        $this->db->where(array('ts.programe_id'=>ACD_ID,'ts.is_MtEt'=>1));
        $this->db->join('`programe_masters` pgm', 'cat.`programe_id`=pgm.`programe_id`', 'left');
        $this->db->order_by('ts.`display_order` ASC');
        return $this->db->get()->result_array();
    }

    /*
     * Get all test_seriese ET
     */
    function get_all_test_seriese_et_gt($params = array())
    {
        
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        $this->db->select('ts.`test_seriese_name`, ts.`test_seriese_type`, ts.`test_seriese_id`,ts.`active`,ts.`test_seriese_color`,ts.`icon`,ts.`test_module_id`,tm.`test_module_id`,tm.`test_module_name`,
        ts_cat.`ts_cat_assoc_id`,ts_cat.`category_id`,ts_cat.`paper_duration`,ts_cat.`max_marks`,ts_cat.`audio_file`,ts_cat.`totalquest`,cat.`category_name`,pgm.`programe_name`,pgm.`programe_id`,       
        (select group_concat(sec.`section_heading` SEPARATOR " | ") from sections sec where sec.`ts_cat_assoc_id` in (ts_cat.`ts_cat_assoc_id`)) as sec_head');
        $this->db->from('`test_seriese` ts');
        $this->db->join('`ts_cat_assoc` ts_cat', 'ts.`test_seriese_id`=ts_cat.`test_seriese_id`', 'left');
        $this->db->join('`category_masters` cat', 'ts_cat.`category_id`=cat.`category_id`', 'left');
        $this->db->join('`test_module` tm', 'tm.`test_module_id`=ts.`test_module_id`', 'left');
        $this->db->where(array('ts.programe_id'=>GT_ID,'ts.is_MtEt'=>1));
        $this->db->join('`programe_masters` pgm', 'cat.`programe_id`=pgm.`programe_id`', 'left');
        $this->db->order_by('ts.`display_order` ASC');
        return $this->db->get()->result_array();
    }

    /*
     * Get all EE
     */
    function get_all_englis_essential($params = array())
    {
        
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        $this->db->select('ts.`test_seriese_name`, ts.`test_seriese_type`, ts.`test_seriese_id`,ts.`active`,ts.`test_seriese_color`,ts.`icon`,ts.`test_module_id`,tm.`test_module_id`,tm.`test_module_name`,
        ts_cat.`ts_cat_assoc_id`,ts_cat.`category_id`,ts_cat.`paper_duration`,ts_cat.`max_marks`,ts_cat.`audio_file`,ts_cat.`totalquest`,cat.`category_name`,pgm.`programe_name`,pgm.`programe_id`,       
        (select group_concat(sec.`section_heading` SEPARATOR " | ") from sections sec where sec.`ts_cat_assoc_id` in (ts_cat.`ts_cat_assoc_id`)) as sec_head');
        $this->db->from('`test_seriese` ts');
        $this->db->join('`ts_cat_assoc` ts_cat', 'ts.`test_seriese_id`=ts_cat.`test_seriese_id`', 'left');
        $this->db->join('`category_masters` cat', 'ts_cat.`category_id`=cat.`category_id`', 'left');
        $this->db->join('`test_module` tm', 'tm.`test_module_id`=ts.`test_module_id`', 'left');
        $this->db->where(array('tm.test_module_name'=>ENGLISH_ESSENTIAL,'ts.is_MtEt'=>NULL));
        $this->db->join('`programe_masters` pgm', 'cat.`programe_id`=pgm.`programe_id`', 'left');
        $this->db->order_by('ts.`display_order` ASC');
        return $this->db->get()->result_array();
    }

    /*
     * Get all test_seriese Mock
     */
    function get_all_test_seriese_mt($params = array())
    {
        
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        $this->db->select('ts.`test_seriese_name`, ts.`test_seriese_type`, ts.`test_seriese_id`,ts.`active`,ts.`test_seriese_color`,ts.`icon`,ts.`test_module_id`,tm.`test_module_id`,tm.`test_module_name`,
        ts_cat.`ts_cat_assoc_id`,ts_cat.`category_id`,ts_cat.`paper_duration`,ts_cat.`max_marks`,ts_cat.`audio_file`,ts_cat.`totalquest`,cat.`category_name`,pgm.`programe_name`,pgm.`programe_id`,       
        (select group_concat(sec.`section_heading` SEPARATOR " | ") from sections sec where sec.`ts_cat_assoc_id` in (ts_cat.`ts_cat_assoc_id`)) as sec_head');
        $this->db->from('`test_seriese` ts');
        $this->db->join('`ts_cat_assoc` ts_cat', 'ts.`test_seriese_id`=ts_cat.`test_seriese_id`', 'left');
        $this->db->join('`category_masters` cat', 'ts_cat.`category_id`=cat.`category_id`', 'left');
        $this->db->join('`test_module` tm', 'tm.`test_module_id`=ts.`test_module_id`', 'left');
        $this->db->where(array('ts.programe_id'=>ACD_ID,'ts.is_MtEt'=>2));
        $this->db->join('`programe_masters` pgm', 'cat.`programe_id`=pgm.`programe_id`', 'left');
        $this->db->order_by('ts.`display_order` ASC');
        return $this->db->get()->result_array();
    }

    /*
     * Get all test_seriese Mock
     */
    function get_all_test_seriese_mt_gt($params = array())
    {
        
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        $this->db->select('ts.`test_seriese_name`, ts.`test_seriese_type`, ts.`test_seriese_id`,ts.`active`,ts.`test_seriese_color`,ts.`icon`,ts.`test_module_id`,tm.`test_module_id`,tm.`test_module_name`,
        ts_cat.`ts_cat_assoc_id`,ts_cat.`category_id`,ts_cat.`paper_duration`,ts_cat.`max_marks`,ts_cat.`audio_file`,ts_cat.`totalquest`,cat.`category_name`,pgm.`programe_name`,pgm.`programe_id`,       
        (select group_concat(sec.`section_heading` SEPARATOR " | ") from sections sec where sec.`ts_cat_assoc_id` in (ts_cat.`ts_cat_assoc_id`)) as sec_head');
        $this->db->from('`test_seriese` ts');
        $this->db->join('`ts_cat_assoc` ts_cat', 'ts.`test_seriese_id`=ts_cat.`test_seriese_id`', 'left');
        $this->db->join('`category_masters` cat', 'ts_cat.`category_id`=cat.`category_id`', 'left');
        $this->db->join('`test_module` tm', 'tm.`test_module_id`=ts.`test_module_id`', 'left');
        $this->db->where(array('ts.programe_id'=>GT_ID,'ts.is_MtEt'=>2));
        $this->db->join('`programe_masters` pgm', 'cat.`programe_id`=pgm.`programe_id`', 'left');
        $this->db->order_by('ts.`display_order` ASC');
        return $this->db->get()->result_array();
    }

    /*
     * Get all test_seriese Static
     */
    function get_all_spTest($params = array())
    {
        
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        $this->db->select('ts.`test_seriese_name`, ts.`test_seriese_type`, ts.`test_seriese_id`,ts.`active`,ts.`test_seriese_color`,ts.`icon`,ts.`test_module_id`,tm.`test_module_id`,tm.`test_module_name`,
        ts_cat.`ts_cat_assoc_id`,ts_cat.`category_id`,ts_cat.`paper_duration`,ts_cat.`max_marks`,ts_cat.`audio_file`,ts_cat.`totalquest`,cat.`category_name`,pgm.`programe_name`,pgm.`programe_id`,       
        (select group_concat(sec.`section_heading` SEPARATOR " | ") from sections sec where sec.`ts_cat_assoc_id` in (ts_cat.`ts_cat_assoc_id`)) as sec_head');
        $this->db->from('`test_seriese` ts');
        $this->db->join('`ts_cat_assoc` ts_cat', 'ts.`test_seriese_id`=ts_cat.`test_seriese_id`', 'left');
        $this->db->join('`category_masters` cat', 'ts_cat.`category_id`=cat.`category_id`', 'left');
        $this->db->join('`test_module` tm', 'tm.`test_module_id`=ts.`test_module_id`', 'left');
        $this->db->where(array('ts.is_MtEt'=>3));
        $this->db->join('`programe_masters` pgm', 'cat.`programe_id`=pgm.`programe_id`', 'left');
        $this->db->order_by('ts.`display_order` ASC');
        return $this->db->get()->result_array();
    }

    /*
     * Get all EE
     */
    function get_all_pte($params = array())
    {
        
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        $this->db->select('ts.`test_seriese_name`, ts.`test_seriese_type`, ts.`test_seriese_id`,ts.`active`,ts.`test_seriese_color`,ts.`icon`,ts.`test_module_id`,tm.`test_module_id`,tm.`test_module_name`,
        ts_cat.`ts_cat_assoc_id`,ts_cat.`category_id`,ts_cat.`paper_duration`,ts_cat.`max_marks`,ts_cat.`audio_file`,ts_cat.`totalquest`,cat.`category_name`,pgm.`programe_name`,pgm.`programe_id`,       
        (select group_concat(sec.`section_heading` SEPARATOR " | ") from sections sec where sec.`ts_cat_assoc_id` in (ts_cat.`ts_cat_assoc_id`)) as sec_head');
        $this->db->from('`test_seriese` ts');
        $this->db->join('`ts_cat_assoc` ts_cat', 'ts.`test_seriese_id`=ts_cat.`test_seriese_id`', 'left');
        $this->db->join('`category_masters` cat', 'ts_cat.`category_id`=cat.`category_id`', 'left');
        $this->db->join('`test_module` tm', 'tm.`test_module_id`=ts.`test_module_id`', 'left');
        $this->db->where(array('tm.test_module_name'=>PTE,'ts.is_MtEt'=>NULL));
        $this->db->join('`programe_masters` pgm', 'cat.`programe_id`=pgm.`programe_id`', 'left');
        $this->db->order_by('ts.`display_order` ASC');
        return $this->db->get()->result_array();
    }


    function get_all_test_seriese_for_assoc($params = array())
    {        
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        $this->db->select(
            'ts.`test_seriese_name`,
            ts.`test_seriese_type`,
            ts.`test_seriese_id`,
            ts.`test_module_id`,
            pgm.`programe_name`,
        ');
        $this->db->from('`test_seriese` ts'); 
        $this->db->join('`programe_masters` pgm', 'ts.`programe_id`= pgm.`programe_id`', 'left');
        $this->db->order_by('ts.`test_seriese_id` desc');
        return $this->db->get()->result_array();
    }
        
    /*
     * function to add new test_seriese
     */
    function add_test_seriese($params)
    {        
        $test_seriese_name =  $params['test_seriese_name'];
        $programe_id =  $params['programe_id'];
        $test_module_id =  $params['test_module_id'];
        $this->db->where(array('test_seriese_name'=> $test_seriese_name,'programe_id'=>$programe_id,'test_module_id'=>$test_module_id));
        $query = $this->db->get('test_seriese');
        $count_row = $query->num_rows();
        if ($count_row > 0) {          
            return 'FALSE';
        }else{
          
            $this->db->insert('test_seriese',$params);
            return $this->db->insert_id();
        }
    }


    function dupliacte_ts_master($test_seriese_name){

        $this->db->where('test_seriese_name', $test_seriese_name);
        $query = $this->db->get('test_seriese');
        $count_row = $query->num_rows();
        if ($count_row > 1) {          
            return 'FALSE';
         } else {          
            
            return 'TRUE';
         }
    }
    
    /*
     * function to update test_seriese
     */
    function update_test_seriese($test_seriese_id,$params)
    {        
        $this->db->where('test_seriese_id',$test_seriese_id);
        return $this->db->update('test_seriese',$params);
    }     
    /*
     * function to delete test_seriese
     */
    function delete_test_seriese($test_seriese_id)
    {
        return $this->db->delete('test_seriese',array('test_seriese_id'=>$test_seriese_id));
    }


    function get_et_eligibility($id,$test_module_id,$programe_id){

        $this->db->select('`student_result_id`');
        $this->db->from('`student_results`');        
        $this->db->where(array('programe_id'=>$programe_id,
            'test_module_id'=>$test_module_id,'student_id'=>$id));
        $this->db->like('collection_no', 'ET');
        return $this->db->get()->row_array();
    }

    /*
     * Get all ET
     */
    function get_et_test_id($id,$test_module_id,$programe_id)
    {  
        $this->db->select('
            ts.`test_seriese_id`,
            ts_cat.`ts_cat_assoc_id`,
        ');
        $this->db->from('`test_seriese` ts');
        $this->db->join('`ts_cat_assoc` ts_cat', 'ts.`test_seriese_id`=ts_cat.`test_seriese_id`', 'left');
        $this->db->where(array('ts.programe_id'=>$programe_id,
            'ts.test_module_id'=>$test_module_id,'ts.active'=>1,'is_MtEt'=>1));
        $this->db->order_by('rand()');
        return $this->db->get()->result_array();
    }

    function getStaticTest($question_type_id,$test_module_id,$programe_id){

        $this->db->select('
            ts.`test_seriese_id`,
            ts_cat.`ts_cat_assoc_id`,
            ts.`test_seriese_name`,
        ');
        $this->db->from('`test_seriese` ts');
        $this->db->join('`ts_cat_assoc` ts_cat', 'ts.`test_seriese_id`=ts_cat.`test_seriese_id`', 'left');
        $this->db->join('`sections` sec', 'sec.`ts_cat_assoc_id`=ts_cat.`ts_cat_assoc_id`', 'left');
        $this->db->join('`question_sets` qs', 'qs.`section_id`=sec.`section_id`', 'left');
        $this->db->where(array('ts.programe_id'=>$programe_id,
            'ts.test_module_id'=> $test_module_id,'ts.active'=> 1,'ts.is_MtEt'=> 3,'qs.question_type_id'=> $question_type_id));
        $this->db->order_by('ts.`test_seriese_name` ASC');
        return $this->db->get()->result_array();
    }
}