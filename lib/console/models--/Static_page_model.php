<?php

class Static_page_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get static_page by static_page_id
     */
    function get_static_page($static_page_id)
    {
        return $this->db->get_where('static_pages',array('static_page_id'=>$static_page_id))->row_array();
    }

    /*
     * Get all static_pages count IELTS Academic
     */
    function get_all_static_pages_count_parent()
    {
        $this->db->from('static_pages sp');
        $this->db->join('`category_masters` cat', 'sp.`category_id`= cat.`category_id`');
        $this->db->join('`programe_masters` pgm', 'cat.`programe_id`= pgm.`programe_id`');
        $this->db->join('`test_module` tm', 'tm.`test_module_id`= cat.`test_module_id`');
        $this->db->where(array('sp.`pid`'=> 0, 'tm.test_module_name'=>IELTS,'cat.`programe_id'=>ACD_ID));
        return $this->db->count_all_results();
    }

     /*
     * Get all static_pages count IELTS GT
     */
    function get_all_static_pages_count_parent_gt()
    {
        $this->db->from('static_pages sp');
        $this->db->join('`category_masters` cat', 'sp.`category_id`= cat.`category_id`');
        $this->db->join('`programe_masters` pgm', 'cat.`programe_id`= pgm.`programe_id`');
        $this->db->join('`test_module` tm', 'tm.`test_module_id`= cat.`test_module_id`');
        $this->db->where(array('sp.`pid`'=> 0, 'tm.test_module_name'=>IELTS,'cat.`programe_id'=>GT_ID));
        return $this->db->count_all_results();
    }

     /*
     * Get all static_pages count EE
     */
    function get_all_static_pages_count_parent_ee()
    {
        $this->db->from('static_pages sp');
        $this->db->join('`category_masters` cat', 'sp.`category_id`= cat.`category_id`');
        $this->db->join('`programe_masters` pgm', 'cat.`programe_id`= pgm.`programe_id`');
        $this->db->join('`test_module` tm', 'tm.`test_module_id`= cat.`test_module_id`');
        $this->db->where(array('sp.`pid`'=> 0, 'tm.test_module_name'=>ENGLISH_ESSENTIAL));
        return $this->db->count_all_results();
    }

    /*
     * Get all static_pages count
     */
    function get_all_static_pages_count()
    {
        $this->db->from('static_pages');
        $this->db->where(array('pid !=', 0, FALSE));
        return $this->db->count_all_results();
    }
        
    /*
     * Get all static_pages Academic
     */
    function get_all_static_pages_parent($params = array(), $pid)
    {
       
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        $this->db->select('sp.*, 
            cat.`category_name`, pgm.`programe_name`,tm.`test_module_name`');
        $this->db->from('`static_pages` sp');       ;
        $this->db->join('`category_masters` cat', 'sp.`category_id`= cat.`category_id`');
        $this->db->join('`programe_masters` pgm', 'cat.`programe_id`= pgm.`programe_id`');
        $this->db->join('`test_module` tm', 'tm.`test_module_id`= cat.`test_module_id`');
        $this->db->where(array('sp.`pid`'=> $pid, 'tm.test_module_name'=>IELTS,'cat.`programe_id'=>ACD_ID));
        $this->db->order_by('sp.`title`','ASC');
        return $this->db->get()->result_array();
    }

    /*
     * Get all static_pages GT
     */
    function get_all_static_pages_parent_gt($params = array(), $pid)
    {
       
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        $this->db->select('sp.*, 
            cat.`category_name`, pgm.`programe_name`,tm.`test_module_name`');
        $this->db->from('`static_pages` sp');       ;
        $this->db->join('`category_masters` cat', 'sp.`category_id`= cat.`category_id`');
        $this->db->join('`programe_masters` pgm', 'cat.`programe_id`= pgm.`programe_id`');
        $this->db->join('`test_module` tm', 'tm.`test_module_id`= cat.`test_module_id`');
        $this->db->where(array('sp.`pid`'=> $pid, 'tm.test_module_name'=>IELTS,'cat.`programe_id'=>GT_ID));
        $this->db->order_by('sp.`title`','ASC');
        return $this->db->get()->result_array();
    }

    /*
     * Get all static_pages
     */
    function get_all_static_pages_parent_ee($params = array(), $pid)
    {
       
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        $this->db->select('sp.*, 
            cat.`category_name`, pgm.`programe_name`,tm.`test_module_name`');
        $this->db->from('`static_pages` sp');       ;
        $this->db->join('`category_masters` cat', 'sp.`category_id`= cat.`category_id`');
        $this->db->join('`programe_masters` pgm', 'cat.`programe_id`= pgm.`programe_id`');
        $this->db->join('`test_module` tm', 'tm.`test_module_id`= cat.`test_module_id`');
        $this->db->where(array('sp.`pid`'=> $pid, 'tm.test_module_name'=>ENGLISH_ESSENTIAL));
        $this->db->order_by('sp.`display_order`','ASC');
        return $this->db->get()->result_array();
    }

    function get_all_static_pages2($params = array())
    {
        $this->db->order_by('title', 'ASC');
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        $this->db->select('sp.*, 
            cat.`category_name`, pgm.`programe_name`');
        $this->db->from('`static_pages` sp');       ;
        $this->db->join('`category_masters` cat', 'sp.`category_id`= cat.`category_id`');
        $this->db->join('`programe_masters` pgm', 'cat.`programe_id`= pgm.`programe_id`');
        $this->db->order_by('sp.`pid` desc');
        return $this->db->get()->result_array();
    }    

    function get_all_static_pages_child($params = array(), $pid)
    {
        $this->db->order_by('static_page_id', 'desc');
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        $this->db->select('sp.*, 
            cat.`category_name`, pgm.`programe_name`');
        $this->db->from('`static_pages` sp');       ;
        $this->db->join('`category_masters` cat', 'sp.`category_id`= cat.`category_id`');
        $this->db->join('`programe_masters` pgm', 'cat.`programe_id`= pgm.`programe_id`');
        $this->db->where('sp.`pid`', $pid);
        $this->db->order_by('sp.`pid` desc');
        return $this->db->get()->result_array();
    }

    function get_all_pid(){
        
        $this->db->select('sp.`static_page_id`, sp.`title`, 
            cat.`category_name`, pgm.`programe_name`');
        $this->db->from('`static_pages` sp');       ;
        $this->db->join('`category_masters` cat', 'sp.`category_id`= cat.`category_id`');
        $this->db->join('`programe_masters` pgm', 'cat.`programe_id`= pgm.`programe_id`');
        $this->db->where('sp.active', 1);
        $this->db->order_by('sp.`static_page_id` desc');
        return $this->db->get()->result_array();
       //print_r($this->db->last_query());exit;
    }
        
    /*
     * function to add new static_page
     */
    function add_static_page($params)
    {
        $this->db->insert('static_pages',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update static_page
     */
    function update_static_page($static_page_id,$params)
    {
        $this->db->where('static_page_id',$static_page_id);
        return $this->db->update('static_pages',$params);
        //print_r($this->db->last_query());exit;
    }
    
    /*
     * function to delete static_page
     */
    function delete_static_page($static_page_id)
    {
        return $this->db->delete('static_pages',array('static_page_id'=>$static_page_id));
    }


    function get_sp_to_print($category_id){

        $this->db->select('`title`,`contents`');
        $this->db->from('`static_pages`');        
        $this->db->where(array('category_id'=> $category_id, 'contents!='=>''));
        $this->db->order_by('`pid`','ASC');
        return $this->db->get()->result_array();
    }

    function getStaticPageCatg($programe_id,$test_module_id){

        $this->db->select('cat.`category_id`, cat.`category_name`, `cat.icon` ');
        $this->db->from('`category_masters` cat');        
        $this->db->join('`programe_masters` pgm', 'pgm.`programe_id`= cat.`programe_id`', 'left');
        $this->db->where(array('cat.programe_id'=> $programe_id, 'cat.active'=>1, 'cat.test_module_id'=> $test_module_id,'category_name!='=>'ET1'));
        $this->db->order_by('cat.modified','DESC');
        return $this->db->get('')->result_array();
    }

    function getStaticPageCatgEE($programe_id,$test_module_id){

        $this->db->select('cat.`category_id`, cat.`category_name`, `cat.icon` ');
          $this->db->from('`category_masters` cat');       
          $this->db->join('`programe_masters` pgm', 'pgm.`programe_id`= cat.`programe_id`', 'left');
          if($test_module_id==IELTS_ID){
            $this->db->where(array('cat.programe_id'=> $programe_id, 'cat.active'=>1, 'cat.test_module_id'=> $test_module_id));
          }else{
            $this->db->where(array('cat.active'=>1, 'cat.test_module_id'=> $test_module_id));
          }
          $this->db->group_start();
          $this->db->where('cat.category_name',POS)
           ->or_where('cat.category_name',TENSE)
           ->or_where('cat.category_name',DI)
           ->or_where('cat.category_name',AP);
          $this->db->group_end();
          $this->db->order_by('cat.category_name','ASC');
          return $this->db->get('')->result_array();
    }

    function getStaticPageContents($pid,$category_id){

        $this->db->select('`static_page_id`,`pid`,`category_id`,`question_type_id`,`title`,`audio`,`contents`');
            $this->db->from('`static_pages`');
        if($pid>0){
            
            $this->db->where(array('pid'=> $pid, 'category_id'=>$category_id, 'active'=> 1));
        }else{
            
            $this->db->where(array('pid'=> 0, 'category_id'=>$category_id, 'active'=> 1));
        }        
        $this->db->order_by('display_order','ASC');
        return $this->db->get('')->result_array();
    }
}
