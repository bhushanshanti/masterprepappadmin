<?php

 
class Student_package_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }    
    
    /*
     * Get pack status
     */
    function getPackStatus($id,$programe_id,$test_module_id)
    {
        $this->db->select('active as havePackage');
        $this->db->from('student_package');
        $this->db->where(array('student_id'=> $id,'programe_id'=> $programe_id,'test_module_id'=> $test_module_id, 'active'=>1,'paper_left>'=>0));
        return $this->db->get()->row_array();
    }
    
}
