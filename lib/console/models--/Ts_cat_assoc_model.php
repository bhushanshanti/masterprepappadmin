<?php
 
class Ts_cat_assoc_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get ts_cat_assoc by ts_cat_assoc_id
     */
    function get_ts_cat_assoc($ts_cat_assoc_id)
    {
        return $this->db->get_where('ts_cat_assoc',array('ts_cat_assoc_id'=>$ts_cat_assoc_id))->row_array();
    }

    function get_test_seriese_id($ts_cat_assoc_id)
    {
        return $this->db->get_where('ts_cat_assoc',array('ts_cat_assoc_id'=>$ts_cat_assoc_id))->row_array();
    }
    
    /*
     * Get all ts_cat_assoc count IELTS ACD
     */
    function get_all_ts_cat_assoc_count()
    {
        $this->db->from('ts_cat_assoc ts_cat');
        $this->db->join('`category_masters` cat', 'ts_cat.`category_id`= cat.`category_id`');
        $this->db->where(array('cat.test_module_id'=>IELTS_ID,'cat.programe_id'=>ACD_ID));
        return $this->db->count_all_results();
    }

    /*
     * Get all ts_cat_assoc count IELTS ACD
     */
    function get_all_ts_cat_assoc_count_gt()
    {
        $this->db->from('ts_cat_assoc ts_cat');
        $this->db->join('`category_masters` cat', 'ts_cat.`category_id`= cat.`category_id`');
        $this->db->where(array('cat.test_module_id'=>IELTS_ID,'cat.programe_id'=>GT_ID));
        return $this->db->count_all_results();
    }

    /*
     * Get all ts_cat_assoc count EE
     */
    function get_all_ts_cat_assoc_count_ee()
    {
        $this->db->from('ts_cat_assoc ts_cat');
        $this->db->join('`category_masters` cat', 'ts_cat.`category_id`= cat.`category_id`');
        $this->db->where(array('cat.test_module_id'=>EE_ID));
        return $this->db->count_all_results();
    }

    /*
     * Get all ts_cat_assoc count EE
     */
    function get_all_ts_cat_assoc_count_et()
    {
        $this->db->from('ts_cat_assoc ts_cat');
        $this->db->join('`category_masters` cat', 'ts_cat.`category_id`= cat.`category_id`');
        $this->db->where(array('cat.test_module_id'=>IELTS_ID,'cat.category_name'=>'ET1'));
        return $this->db->count_all_results();
    }
        
    /*
     * Get all ts_cat_assoc IELTS ACD
     */
    function get_all_ts_cat_assoc($params = array())
    {
        
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        $this->db->select('ts.`test_seriese_name`, ts.`test_seriese_type`,ts.`test_seriese_color`,
        ts_cat.`ts_cat_assoc_id`,ts_cat.`category_id`,
        ts_cat.`paper_duration`,ts_cat.`max_marks`,ts_cat.`audio_file`,
        ts_cat.`audio_time`,ts_cat.`totalquest`,ts_cat.`icon`,
        cat.`category_name`,
        pgm.`programe_name`,pgm.`programe_id`,tm.`test_module_name`');
        $this->db->from('`ts_cat_assoc` ts_cat');
        $this->db->join('`test_seriese` ts', 'ts.`test_seriese_id`= ts_cat.`test_seriese_id`');
        $this->db->join('`category_masters` cat', 'ts_cat.`category_id`= cat.`category_id`');
        $this->db->join('`programe_masters` pgm', 'cat.`programe_id`= pgm.`programe_id`');
        $this->db->join('`test_module` tm', 'tm.`test_module_id`=cat.`test_module_id`');
         $this->db->where(array('cat.test_module_id'=>IELTS_ID,'ts.programe_id'=>ACD_ID));
        $this->db->order_by('ts.`display_order` ASC');
        return $this->db->get('')->result_array();
        //print_r($this->db->last_query());exit;
        
    }

    /*
     * Get all ts_cat_assoc IELTS GT
     */
    function get_all_ts_cat_assoc_gt($params = array())
    {
        
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        $this->db->select('ts.`test_seriese_name`, ts.`test_seriese_type`,ts.`test_seriese_color`,
        ts_cat.`ts_cat_assoc_id`,ts_cat.`category_id`,
        ts_cat.`paper_duration`,ts_cat.`max_marks`,ts_cat.`audio_file`,
        ts_cat.`audio_time`,ts_cat.`totalquest`,ts_cat.`icon`,
        cat.`category_name`,
        pgm.`programe_name`,pgm.`programe_id`,tm.`test_module_name`');
        $this->db->from('`ts_cat_assoc` ts_cat');
        $this->db->join('`test_seriese` ts', 'ts.`test_seriese_id`= ts_cat.`test_seriese_id`');
        $this->db->join('`category_masters` cat', 'ts_cat.`category_id`= cat.`category_id`');
        $this->db->join('`programe_masters` pgm', 'cat.`programe_id`= pgm.`programe_id`');
        $this->db->join('`test_module` tm', 'tm.`test_module_id`=cat.`test_module_id`');
         $this->db->where(array('cat.test_module_id'=>IELTS_ID,'ts.programe_id'=>GT_ID));
        $this->db->order_by('ts.`display_order` ASC');
        return $this->db->get('')->result_array();
        //print_r($this->db->last_query());exit;
        
    }

    /*
     * Get all ts_cat_assoc EE
     */
    function get_all_ts_cat_assoc_ee($params = array())
    {
        
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        $this->db->select('ts.`test_seriese_name`, ts.`test_seriese_type`,ts.`test_seriese_color`,
        ts_cat.`ts_cat_assoc_id`,ts_cat.`category_id`,
        ts_cat.`paper_duration`,ts_cat.`max_marks`,ts_cat.`audio_file`,
        ts_cat.`audio_time`,ts_cat.`totalquest`,ts_cat.`icon`,
        cat.`category_name`,
        pgm.`programe_name`,pgm.`programe_id`,tm.`test_module_name`');
        $this->db->from('`ts_cat_assoc` ts_cat');
        $this->db->join('`test_seriese` ts', 'ts.`test_seriese_id`= ts_cat.`test_seriese_id`');
        $this->db->join('`category_masters` cat', 'ts_cat.`category_id`= cat.`category_id`');
        $this->db->join('`programe_masters` pgm', 'cat.`programe_id`= pgm.`programe_id`');
        $this->db->join('`test_module` tm', 'tm.`test_module_id`=cat.`test_module_id`');
         $this->db->where(array('cat.test_module_id'=>EE_ID));
        $this->db->order_by('ts.`test_seriese_name` desc');
        return $this->db->get('')->result_array();
        //print_r($this->db->last_query());exit;
        
    }


    /*
     * Get all ts_cat_assoc EE
     */
    function get_all_ts_cat_assoc_et($params = array())
    {
        
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        $this->db->select('ts.`test_seriese_name`, ts.`test_seriese_type`,ts.`test_seriese_color`,
        ts_cat.`ts_cat_assoc_id`,ts_cat.`category_id`,
        ts_cat.`paper_duration`,ts_cat.`max_marks`,ts_cat.`audio_file`,
        ts_cat.`audio_time`,ts_cat.`totalquest`,ts_cat.`icon`,
        cat.`category_name`,
        pgm.`programe_name`,pgm.`programe_id`,tm.`test_module_name`');
        $this->db->from('`ts_cat_assoc` ts_cat');
        $this->db->join('`test_seriese` ts', 'ts.`test_seriese_id`= ts_cat.`test_seriese_id`');
        $this->db->join('`category_masters` cat', 'ts_cat.`category_id`= cat.`category_id`');
        $this->db->join('`programe_masters` pgm', 'cat.`programe_id`= pgm.`programe_id`');
        $this->db->join('`test_module` tm', 'tm.`test_module_id`=cat.`test_module_id`');
         $this->db->where(array('cat.test_module_id'=>IELTS_ID, 'cat.category_name'=>'ET1'));
        $this->db->order_by('ts.`test_seriese_name` DESC');
        return $this->db->get('')->result_array();
        //print_r($this->db->last_query());exit;
        
    }

    function get_particular_ts_cat_assoc($test_seriese_id)
    {
        $this->db->where('test_seriese_id', $test_seriese_id);
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        return $this->db->get('ts_cat_assoc')->result_array();
    }   
        
    /*
     * function to add new ts_cat_assoc
     */
    function add_ts_cat_assoc($params)
    {
        $this->db->insert('ts_cat_assoc',$params);
        return $this->db->insert_id();
    }

    function check_duplicate_ts_cat_assoc($test_seriese_id, $category_id){

        return $this->db->get_where('ts_cat_assoc',array('category_id'=>$category_id, 'test_seriese_id'=> $test_seriese_id))->result_array();

    }
    
    /*
     * function to update ts_cat_assoc
     */
    function update_ts_cat_assoc($ts_cat_assoc_id,$params)
    {
        $this->db->where('ts_cat_assoc_id',$ts_cat_assoc_id);
        return $this->db->update('ts_cat_assoc',$params);
    }
    
    /*
     * function to delete ts_cat_assoc
     */
    function delete_ts_cat_assoc($ts_cat_assoc_id)
    {
        return $this->db->delete('ts_cat_assoc',array('ts_cat_assoc_id'=>$ts_cat_assoc_id));
    }

    function update_file($ts_cat_assoc_id,$params)
    {
        $this->db->where('ts_cat_assoc_id',$ts_cat_assoc_id);
        return $this->db->update('ts_cat_assoc',$params);
    }

    function get_file_path($ts_cat_assoc_id){

        $this->db->select('audio_file');
        $this->db->from('ts_cat_assoc');        
        $this->db->where(array('ts_cat_assoc_id'=>$ts_cat_assoc_id));
        return $this->db->get('')->result_array();
        //print_r($this->db->last_query());exit;
    }
}
