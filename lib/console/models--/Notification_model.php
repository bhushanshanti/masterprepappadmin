<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/
 
class Notification_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }    

    function get_all_notification_message($id,$test_module_id,$programe_id){
       
        $this->db->select('
            sn.`notification_id`,
            sn.test_module_id,
            sn.ts_cat_assoc_id,
            sn.collection_no,
            sn.`read_status`, 
            nm.`message`,
            ns.`subject`,
            ns.`icon`,
            date_format(sn.created, "%D %b %y %I:%i %p") as created,
        ');
        $this->db->from('student_notification sn');        
        $this->db->join('notification_message nm', 'sn.messege_id = nm.messege_id');
        $this->db->join('notification_subject ns', 'ns.id = nm.subject_id');
        $this->db->where(array('sn.active'=> 1,'sn.student_id'=> $id,'sn.programe_id'=>$programe_id,'sn.test_module_id'=> $test_module_id)); 
        $this->db->or_where(array('sn.test_module_id'=> EE_ID ));
        $this->db->order_by('sn.created', 'DESC');
        $this->db->limit(NOTIFICATION_LIMIT);
        return $this->db->get()->result_array();
        //print_r($this->db->last_query());exit;
    }

    function get_notification_message($subject){
       
        $this->db->select('nm.`messege_id`');
        $this->db->from('notification_message nm');
        $this->db->join('notification_subject ns', 'ns.id = nm.subject_id');
        $this->db->where(array('nm.active'=> 1,'ns.subject' => $subject));
        return $this->db->get()->row_array();
    }

    function add_notification_message($student_id,$messege_id,$programe_id,$test_module_id,$ts_cat_assoc_id,$collection_no,$paper_checked){

        $params = array(
            'student_id'    =>  $student_id,
            'programe_id'   =>  $programe_id,
            'test_module_id' => $test_module_id,
            'ts_cat_assoc_id'=> $ts_cat_assoc_id,
            'collection_no' =>  $collection_no,
            'messege_id'    =>  $messege_id,
            'read_status'   =>  0,
            'active'        =>  1,
            'paper_checked' =>  $paper_checked
        );
        $this->db->insert('student_notification',$params);
        return $this->db->insert_id();
    }

    function get_notification_settings($id){
        
        $this->db->select('
            `mobile_notification`,
            `email_notification`,            
        ');
        $this->db->from('students');
        $this->db->where(array('id'=> $id));
        return $this->db->get()->row_array();
    }
    
}
