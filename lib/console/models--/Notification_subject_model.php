<?php

 
class Notification_subject_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    /*
     * Get notification_subject by id
     */
    function get_notification_subject($id)
    {
        return $this->db->get_where('notification_subject',array('id'=>$id))->row_array();
    }
    /*
     * Get all notification_subject count
     */
    function get_all_notification_subject_count()
    {
        $this->db->from('notification_subject');
        return $this->db->count_all_results();
    }  
    /*
     * Get all notification_subject
     */
    function get_all_notification_subject($params = array())
    {
        $this->db->order_by('id', 'DESC');
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        return $this->db->get('notification_subject')->result_array();
    }

    function get_all_notification_subject_active($params = array())
    {
        $this->db->where('active', '1');
        $this->db->order_by('id', 'DESC');
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        return $this->db->get('notification_subject')->result_array();
    }
    /*
     * function to add new notification_subject
     */
    function add_notification_subject($params)
    {
        $this->db->insert('notification_subject',$params);
        return $this->db->insert_id();
    }    
    /*
     * function to update notification_subject
     */
    function update_notification_subject($id,$params)
    {
        $this->db->where('id',$id);
        return $this->db->update('notification_subject',$params);
    } 
    /*
     * function to delete notification_subject
     */
    function delete_notification_subject($id)
    {
        return $this->db->delete('notification_subject',array('id'=>$id));
    }

    
}
