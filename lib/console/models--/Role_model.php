<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/
 
class Role_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }    
    /*
     * Get role by id
     */
    function get_role($id)
    {
        return $this->db->get_where('roles',array('id'=>$id))->row_array();
    }
    /*
     * Get controller by id
     */
    function get_controller($id)
    {
        return $this->db->get_where('controller_list',array('id'=>$id))->row_array();
    }
    /*
     * Get method by id
     */
    function get_method($id)
    {
        return $this->db->get_where('method_list',array('id'=>$id))->row_array();
    }

    function get_role_data($id)
    {        
        $this->db->select('name');
        $this->db->from('roles');
        $this->db->where(array('id'=>$id));
        return $this->db->get('')->result_array();
    } 
    /*
     * Get all roles count
     */
    function get_all_roles_count()
    {
        $this->db->from('roles');
        return $this->db->count_all_results();
    }
    /*
     * Get all controller count
     */
    function get_all_controller_count(){

        $this->db->from('controller_list');
        return $this->db->count_all_results();
    } 
    /*
     * Get all method count
     */
    function get_all_method_count(){

        $this->db->from('method_list');
        return $this->db->count_all_results();
    }      
    /*
     * Get all roles
     */
    function get_all_roles($params = array())
    {
        $this->db->order_by('id', 'desc');
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        return $this->db->get('roles')->result_array();
    }

    function get_all_controller($params = array())
    {
        
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        $this->db->order_by('controller_name', 'ASC');
        return $this->db->get('controller_list')->result_array();
    }

    function get_all_methods($controller_id){
        
        return $this->db->get_where('method_list',array('controller_id'=>$controller_id))->result_array();
    }

    function get_all_method_list(){
        
        $this->db->select('c.controller_name,c.id,m.id,m.controller_id,m.active,m.method_name');
        $this->db->from('method_list m');
        $this->db->join('controller_list c', 'c.id= m.controller_id');        
        $this->db->order_by('c.controller_name ASC');
        return $this->db->get('')->result_array();

    }

    function truncate_cm(){
        $this->db->query('truncate controller_list');
        $this->db->query('truncate method_list');
    }

    function get_all_roles_active()
    {   
        return $this->db->get_where('roles',array('active'=>1))->result_array();        
    }        
    /*
     * function to add new role
     */
    function add_role($params)
    {
        $this->db->insert('roles',$params);
        return $this->db->insert_id();
    }
    /*
     * function to add new role
     */
    function add_access($params)
    {
        $this->db->insert('role_access',$params);
        return $this->db->insert_id();
    }

    function update_access($id,$params)
    {
        $this->db->where('id',$id);
        return $this->db->update('role_access',$params);
        //print_r($this->db->last_query());exit;
    }
     /*
     * function to add controller
     */
    function add_controller($params)
    {
        $this->db->insert('controller_list',$params);
        return $this->db->insert_id();
    }
     /*
     * function to add method
     */
    function add_method($params)
    {
        $this->db->insert('method_list',$params);
        return $this->db->insert_id();
    }    
    
    /*
     * function to update role
     */
    function update_role($id,$params)
    {
        $this->db->where('id',$id);
        return $this->db->update('roles',$params);
    }

    function update_controller($id,$params)
    {
        $this->db->where('id',$id);
        return $this->db->update('controller_list',$params);
    }
    
    /*
     * function to delete role
     */
    function delete_role($id)
    {
        
        $this->db->delete('role_access',array('role_id'=>$id));
        $this->db->delete('user_role',array('role_id'=>$id));
        return $this->db->delete('roles',array('id'=>$id));
    } 

    /*
     * function to del controller 
     */
    function delete_controller($id)
    {
        
        $this->db->delete('method_list',array('controller_id'=>$id));
        return $this->db->delete('controller_list',array('id'=>$id));
    }

    /*
     * function to method 
     */
    function delete_method($id)
    {
        return $this->db->delete('method_list',array('id'=>$id));
    }

    function delete_role_access($id)
    {
        return $this->db->delete('role_access',array('role_id'=>$id));
    }    

    function check_controller($controller_name){

        $this->db->select('id');
        $this->db->from('controller_list');
        $this->db->where(array('controller_name'=>$controller_name));
        return $this->db->get('')->result_array();
        //print_r($this->db->last_query());exit;
    }

    function check_method($method_name,$controller_id){

        $this->db->select('id');
        $this->db->from('method_list');
        $this->db->where(array('method_name'=>$method_name,'controller_id'=>$controller_id));
        return $this->db->get('')->result_array();
        //print_r($this->db->last_query());exit;
    }

    function check_duplicate_access($role_id,$controller_id,$method_id){

        $this->db->select('id');
        $this->db->from('role_access');
        $this->db->where(array('role_id'=>$role_id,'controller_id'=>$controller_id,'method_id'=>$method_id));
        return $this->db->get('')->result_array();
        //print_r($this->db->last_query());exit;
    }

    function check_role_acess($role_id,$controller_id,$method_id){

        $this->db->from('role_access');
        $this->db->where(array('role_id'=>$role_id, 'controller_id'=> $controller_id,'method_id'=>$method_id));
        return $this->db->count_all_results();
        //print_r($this->db->last_query());exit;
    }
}
