<?php

 
class Section_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get section by section_id
     */
    function get_section($ts_cat_assoc_id)
    {
        $response = array(); 
        if($ts_cat_assoc_id){  
          $this->db->select('sec.section_id,sec.ts_cat_assoc_id,sec.section_heading,sec.active, (select group_concat(qs.`question_sets_heading` SEPARATOR " | ") from question_sets qs where qs.`section_id` in (sec.`section_id`)) as qs_head');
          $this->db->where('ts_cat_assoc_id', $ts_cat_assoc_id);
          $q = $this->db->get('sections sec');
          $response = $q->result();     
        } 
        return $response;
    }

    function get_section2($section_id)
    {        
      return $this->db->get_where('sections', array('section_id'=>$section_id))->row_array();
    }
    
    /*
     * Get all sections count
     */
    function get_all_sections_count()
    {
        $this->db->from('sections');
        return $this->db->count_all_results();
    }
        
    /*
     * Get all sections
     */
    function get_all_sections($params = array())
    {
        $this->db->order_by('section_id', 'desc');
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        return $this->db->get('sections')->result_array();
    }
        
    /*
     * function to add new section
     */
    function add_section($params)
    {
        $this->db->insert('sections',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update section
     */
    function update_section($section_id,$params)
    {
        $this->db->where('section_id',$section_id);
        return $this->db->update('sections',$params);
    }
    
    /*
     * function to delete section
     */
    function delete_section($section_id)
    {
        return $this->db->delete('sections',array('section_id'=>$section_id));
    }
}
