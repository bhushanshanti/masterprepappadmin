<?php

 
class Programe_master_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get programe_master by programe_id
     */
    function get_programe_master($programe_id)
    {
        return $this->db->get_where('programe_masters',array('programe_id'=>$programe_id))->row_array();
    }
    
    /*
     * Get all programe_masters count
     */
    function get_all_programe_masters_count()
    {
        $this->db->from('programe_masters');
        return $this->db->count_all_results();
    }
        
    /*
     * Get all programe_masters
     */
    function get_all_programe_masters_active($params = array())
    {
        $this->db->where('active', '1');
        $this->db->order_by('programe_id', 'desc');
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        return $this->db->get('programe_masters')->result_array();
    }

    function get_all_programe_masters($params = array())
    {
       
        $this->db->order_by('programe_id', 'desc');
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        return $this->db->get('programe_masters')->result_array();
    }

    function getProgram(){

        $this->db->select('`programe_id`, `programe_name`');
        $this->db->from('`programe_masters`');        
        $this->db->where(array('active'=> 1));
        $this->db->order_by('programe_name', 'ASC');
        return $this->db->get('')->result_array();
    }
        
    /*
     * function to add new programe_master
     */
    function add_programe_master($params)
    {
        
        $programe_name =  $params['programe_name'];
        $this->db->where('programe_name', $programe_name);
        $query = $this->db->get('programe_masters');
        $count_row = $query->num_rows();
        if ($count_row > 0) {          
            return FALSE;
         } else {
          
            $this->db->insert('programe_masters',$params);
            return $this->db->insert_id();
         }        
    }

    function dupliacte_programe_master($programe_name){

        $this->db->where('programe_name', $programe_name);
        $query = $this->db->get('programe_masters');
        $count_row = $query->num_rows();
        if ($count_row > 0) {          
            return 'FALSE';
         } else {          
            
            return 'TRUE';
         }
    }
    
    /*
     * function to update programe_master
     */
    function update_programe_master($programe_id,$params)
    {
        $this->db->where('programe_id',$programe_id);
        return $this->db->update('programe_masters',$params);
    }
    
    /*
     * function to delete programe_master
     */
    function delete_programe_master($programe_id)
    {
        return 0;
        return $this->db->delete('programe_masters',array('programe_id'=>$programe_id));
    }
}
