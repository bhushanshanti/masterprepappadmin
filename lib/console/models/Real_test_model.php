<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/

 
class Real_test_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    } 

    function get_test_dates($realTestDatesId)
    {
        return $this->db->get_where('real_test_dates',array('realTestDatesId'=>$realTestDatesId))->row_array();
    }

     /*
     * Get all test_dates count IELTS Academic
     */
    function get_all_test_dates_count()
    {        
        $this->db->from('real_test_dates rtd');        
        $this->db->join('test_module tm', 'tm.test_module_id = rtd.test_module_id', 'left');
        $this->db->where(array('tm.test_module_name'=>IELTS,'rtd.programe_id'=>ACD_ID));
        return $this->db->count_all_results();        
    }
    /*
     * Get all real_test_dates
     */
    function get_all_test_dates($params = array())
    {
        
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        $this->db->select('
            rtd.`realTestDatesId`,
            rtd.`testDate`,
            rtd.`active`,
            rtd.`created`,
            pgm.programe_name,
            tm.test_module_name
        ');
        $this->db->from('`real_test_dates` rtd');
        $this->db->join('test_module tm', 'tm.test_module_id = rtd.test_module_id', 'left');
        $this->db->join('`programe_masters` pgm', 'rtd.`programe_id`=pgm.`programe_id`', 'left');
        $this->db->where(array());
        $this->db->order_by('`testDate` ASC');
        return $this->db->get()->result_array();
    }

    function add_real_test_dates($params)
    {  
        $this->db->insert('real_test_dates',$params);
        return $this->db->insert_id();        
    }

    /*
     * function to update real_test_dates
     */
    function update_test_date($realTestDatesId,$params)
    {        
        $this->db->where('realTestDatesId',$realTestDatesId);
        return $this->db->update('real_test_dates',$params);
    } 
}
