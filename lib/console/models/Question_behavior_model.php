<?php
 
class Question_behavior_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get question_behavior by question_behavior_id
     */
    function get_question_behavior($question_behavior_id)
    {
        return $this->db->get_where('question_behavior',array('question_behavior_id'=>$question_behavior_id))->row_array();
    }
    
    /*
     * Get all question_behaviors count
     */
    function get_all_question_behavior_count()
    {
        $this->db->from('question_behavior');
        return $this->db->count_all_results();
    }
        
    /*
     * Get all question_behaviors
     */
    function get_all_question_behavior($params = array())
    {
        $this->db->order_by('question_behavior_id', 'desc');
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        return $this->db->get('question_behavior')->result_array();
    }

    function get_all_question_behavior_active($params = array())
    {
        $this->db->order_by('question_behavior_id', 'desc');
        $this->db->where('active', '1');
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        return $this->db->get('question_behavior')->result_array();
    }
        
    /*
     * function to add new question_behavior
     */
    function add_question_behavior($params)
    {
        
        $behavior_name =  $params['behavior_name'];
        $this->db->where('behavior_name', $behavior_name);
        $query = $this->db->get('question_behavior');
        $count_row = $query->num_rows();
        if ($count_row > 0) {          
            return FALSE;
        }else {

            $this->db->insert('question_behavior',$params);
            return $this->db->insert_id();
        }
    }   
    
    /*
     * function to update question_behavior
     */
    function update_question_behavior($question_behavior_id,$params)
    {
        $this->db->where('question_behavior_id',$question_behavior_id);
        return $this->db->update('question_behavior',$params);
    }
    
    /*
     * function to delete question_behavior
     */
    function delete_question_behavior($question_behavior_id)
    {
        return $this->db->delete('question_behavior',array('question_behavior_id'=>$question_behavior_id));
    }
}
