<?php

 
class User_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    public function checkLogin($email,$password,$checkadmin=false){
        $subquery="";
        if($checkadmin){
            $subquery=" and (roles.name='Admin' or roles.name='Tester' or roles.name='Editor' or roles.name='app_developer' or roles.name='Content Writer' or roles.name='Content Writer SP' or roles.name='Examiner' or roles.name='Enquiry Admin')";
        }
       $query = $this->db->query("select user.id,user.fname,user.lname,user.email,user.mobile,user.dob,user.residential_address,user.created,roles.name,roles.id as roleid from user "
             ." LEFT JOIN user_role ON user_role.user_id=user.id"
             ." LEFT JOIN roles ON roles.id=user_role.role_id"
             ." WHERE user.email='".$email."' and user.password='".md5($password)."' and user.active='1' $subquery");
      //echo $this->db->last_query();die;
      return $query->result();
    }
    
    /*
     * Get user by id
     */
    function get_user($id)
    {
        return $this->db->get_where('user',array('id'=>$id))->row_array();
    }
    
    
    /*
     * Get all user count
     */
    function get_all_user_count()
    {
        $this->db->from('user');
        return $this->db->count_all_results();
    }
        
    /*
     * Get all user
     */
    function get_all_user($params = array())
    {
        
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        $this->db->select('u.id,u.fname,u.lname,u.email,u.mobile,u.dob,u.active, 
            ur.role_id, r.name,g.gender_name');
        $this->db->from('user u');
        $this->db->join('user_role ur', 'ur.user_id = u.id','left');
        $this->db->join('roles r', 'r.id = ur.role_id','left');
        $this->db->join('gender g', 'g.id = u.gender','left');
        $this->db->order_by('r.name ASC');
        return $this->db->get('')->result_array();
    } 

    /*
     * Get all examiner
     */
    function get_examiner()
    {                
        $this->db->select('u.id,u.fname,u.email,u.mobile');
        $this->db->from('user u');
        $this->db->join('user_role ur', 'ur.user_id = u.id','left');
        $this->db->join('roles r', 'r.id = ur.role_id','left');
        $this->db->where(array('r.name'=>'Examiner','u.active'=>1));
        $this->db->order_by('assigned_count ASC');
        return $this->db->get('')->row_array();
    }

    /*
     * Get enquiry admin
     */
    function get_enquiry_admin()
    {                
        $this->db->select('u.id,u.fname,u.email,u.mobile');
        $this->db->from('user u');
        $this->db->join('user_role ur', 'ur.user_id = u.id','left');
        $this->db->join('roles r', 'r.id = ur.role_id','left');
        $this->db->where(array('r.name'=>'Enquiry Admin','u.active'=>1));
        $this->db->order_by('assigned_count ASC');
        return $this->db->get('')->row_array();
    } 

    /*
     * Get mock admin
     */
    function get_mock_admin()
    {                
        $this->db->select('u.id,u.fname,u.email,u.mobile');
        $this->db->from('user u');
        $this->db->join('user_role ur', 'ur.user_id = u.id','left');
        $this->db->join('roles r', 'r.id = ur.role_id','left');
        $this->db->where(array('r.name'=>'MockTest Admin','u.active'=>1));
        $this->db->order_by('assigned_count ASC');
        return $this->db->get('')->row_array();
    } 

    function get_user_role($id){

      $this->db->select('role_id');
        $this->db->from('user_role');
        $this->db->where(array('user_id'=>$id));
        return $this->db->get('')->result_array();  

    }   
        
    /*
     * function to add new user
     */
    function add_user($params)
    {
        $this->db->insert('user',$params);
        return $this->db->insert_id();
    }    
    /*
     * function to update user
     */
    function update_user($id,$params)
    {
        $this->db->where('id',$id);
        return $this->db->update('user',$params);
    }

    function update_assigned_count($id){
       
         return $this->db->query('update `user` 
          set `assigned_count` = `assigned_count` + 1 where id = '.$id.' ');

    }

    function change_password($id,$params)
    {
        $this->db->where(array('id'=>$id));
        return $this->db->update('user',$params);
    }

    function check_old_pwd($id,$pwd){

        $this->db->select('password');
        $this->db->from('user');        
        $this->db->where(array('id'=>$id,'password'=>$pwd));
        return $this->db->get('')->result_array();
        //print_r($this->db->last_query());exit;
    }
    
    /*
     * function to delete user
     */
    function delete_user($id)
    {
        $this->db->delete('user_role',array('user_id'=>$id));
        return $this->db->delete('user',array('id'=>$id));
    }

    
}
