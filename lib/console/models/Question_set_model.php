<?php

 
class Question_set_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get question_set by question_sets_id
     */
    function get_question_set($question_sets_id)
    {
        return $this->db->get_where('question_sets',array('question_sets_id'=>$question_sets_id))->row_array();
    }
    
    function get_qs($section_id)
    {
        
      $response = array();
      $this->db->select('*');
      $this->db->from('question_sets qs');
      $this->db->join('question_types qt', 'qs.question_type_id = qt.`question_type_id`', 'left');
      $this->db->where('qs.section_id', $section_id);
      return $this->db->get()->result();
    }

    function get_qs_para($question_sets_id)
    {        
      $response = array();
      $this->db->select('question_sets_para');
      $this->db->from('question_sets');      
      $this->db->where('question_sets_id', $question_sets_id);
      return $this->db->get()->result();
    }
    
    /*
     * Get all question_sets count
     */
    function get_all_question_sets_count()
    {
        $this->db->from('question_sets');
        return $this->db->count_all_results();
    }

    function get_set_limit($question_sets_id){

      $this->db->where('question_sets_id', $question_sets_id);        
        $query = $this->db->get('questions');
        $count_row = $query->num_rows();
        return $count_row;
    }
        
    /*
     * Get all question_sets
     */
    function get_all_question_sets($params = array())
    {
        $this->db->order_by('question_sets_id', 'desc');
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        return $this->db->get('question_sets')->result_array();
    }

    function get_question_type_id($question_sets_id){

      $this->db->select('question_type_id');
      $this->db->from('question_sets');      
      $this->db->where('question_sets_id', $question_sets_id);
      return $this->db->get()->row_array();
    }
        
    /*
     * function to add new question_set
     */
    function add_question_set($params)
    {
        $this->db->insert('question_sets',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update question_set
     */
    function update_question_set($question_sets_id,$params)
    {
        $this->db->where('question_sets_id',$question_sets_id);
        return $this->db->update('question_sets',$params);
    }
    
    /*
     * function to delete question_set
     */
    function delete_question_set($question_sets_id)
    {
        return $this->db->delete('question_sets',array('question_sets_id'=>$question_sets_id));
    }
}
