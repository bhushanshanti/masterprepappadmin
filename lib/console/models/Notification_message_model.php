<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/
 
class Notification_message_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get category_master by category_id
     */
    function get_message($messege_id)
    {
        return $this->db->get_where('notification_message',array('messege_id'=>$messege_id))->row_array();
    }
    
    /*
     * Get all category_masters count
     */
    function get_all_message_count()
    {
        $this->db->from('notification_message');
        return $this->db->count_all_results();
    }
        
    /*
     * Get all category_masters
     */
    function get_all_message($params = array())
    {        
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }  
        $this->db->select('nm.`messege_id`,nm.`message`,nm.`active`, ns.`subject`');
        $this->db->from('notification_message nm'); 
        $this->db->join('`notification_subject` ns', 'nm.`subject_id`=ns.`id`');        
        $this->db->order_by('ns.`subject`', 'ASC');
        return $this->db->get()->result_array();
    } 
    

    function get_all_notification_message($params = array()){
       
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        } 
        $this->db->select('nm.`messege_id`,nm.`message`,nm.`active`, ns.`subject`');
        $this->db->from('notification_message nm');
        $this->db->join('notification_subject ns', 'ns.id = nm.subject_id', 'left');
        $this->db->where('nm.active', 1);  
        $this->db->order_by('messege_id', 'DESC');
        return $this->db->get()->result_array();
    }

    function get_message_list($subject_id){       
        
        $this->db->select('messege_id, message');
        $this->db->from('notification_message');
        $this->db->where(array('subject_id'=> $subject_id,'active'=>1));  
        $this->db->order_by('message', 'desc');
        return $this->db->get()->result_array();
    }    
        
    /*
     * function to add new category_master
     */
    function add_message($params)
    {
        
        $message =  $params['message'];
        $subject_id =  $params['subject_id'];
        $this->db->where('message', $message);
        $this->db->where('subject_id', $subject_id);
        $query = $this->db->get('notification_message');
        $count_row = $query->num_rows();
        if ($count_row > 0) {          
            return FALSE;
         } else {
          
            $this->db->insert('notification_message',$params);
            return $this->db->insert_id();
         }
    }

    function dupliacte_message($subject_id,$message){

        $this->db->where('message', $message);
        $this->db->where('subject_id', $subject_id);
        $query = $this->db->get('notification_message');
        $count_row = $query->num_rows();
        if ($count_row > 0) {          
            return 'DUPLICATE';
         } else {          
            
            return 'NOT DUPLICATE';
         }
    }
    
    /*
     * function to update category_master
     */
    function update_message($messege_id,$params)
    {
        $this->db->where('messege_id',$messege_id);
        return $this->db->update('notification_message',$params);
    }
    
    /*
     * function to delete category_master
     */
    function delete_message($messege_id)
    {
        return $this->db->delete('notification_message',array('messege_id'=>$messege_id));
    }
}
