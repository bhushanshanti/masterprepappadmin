<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/

 
class Band_score_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    } 

    /*
     * Get band_score by band_score_id
     */
    function get_band_score($band_score_id)
    {
        return $this->db->get_where('band_scores',array('band_score_id'=>$band_score_id))->row_array();
    }

    function get_band_score_for_total($total, $category_id){
        $this->db->select("bs.band_score,bs.pro_id,pl.pro_name");
        $this->db->from('band_scores bs');   
        $this->db->join('`proficiency_level` pl', 'pl.`pro_id`= bs.`pro_id`', 'left');    
        $this->db->where(array('bs.band_range_lower<=' => $total,'bs.band_range_upper>=' =>$total, 'bs.category_id'=>$category_id,'bs.active'=>1));        
        return $this->db->get()->row_array();
    }    

    function get_pro_id($band_score, $category_id){

        $this->db->select("bs.pro_id");
        $this->db->from('band_scores bs');
        $this->db->where(array('bs.band_range_lower<=' => $band_score,'bs.band_range_upper>=' =>$band_score, 'bs.category_id'=>$category_id,'bs.active'=>1));        
        return $this->db->get()->row_array();
    }
    
    /*
     * Get all band_scores count
     */
    function get_all_band_scores_count()
    {
        $this->db->from('band_scores bs');
        $this->db->join('`category_masters` cat', 'bs.`category_id`=cat.`category_id`', 'left');
        $this->db->join('`programe_masters` pgm', 'cat.`programe_id`=pgm.`programe_id`', 'left');
        $this->db->where(array('pgm.programe_name'=>ACD));
        return $this->db->count_all_results();
    }

    function get_all_band_scores_count_gt()
    {
        $this->db->from('band_scores bs');
        $this->db->join('`category_masters` cat', 'bs.`category_id`=cat.`category_id`', 'left');
        $this->db->join('`programe_masters` pgm', 'cat.`programe_id`=pgm.`programe_id`', 'left');
        $this->db->where(array('pgm.programe_name'=>GT));
        return $this->db->count_all_results();
    }
        
    /*
     * Get all band_scores academic
     */
    function get_all_band_scores($params = array())
    {        
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        $this->db->select('bs.`band_score_id`, bs.`band_score`,  bs.`band_range_lower`, bs.`band_range_upper`,bs.`active`,pl.`pro_name`, cat.`category_name`,pgm.`programe_name`,pgm.`programe_id`,tm.`test_module_name`');
        $this->db->from('`band_scores` bs');
        $this->db->join('`category_masters` cat', 'bs.`category_id`=cat.`category_id`', 'left');
        $this->db->join('`programe_masters` pgm', 'cat.`programe_id`=pgm.`programe_id`', 'left');
        $this->db->join('`test_module` tm', 'tm.`test_module_id`= cat.`test_module_id`');
        $this->db->join('`proficiency_level` pl', 'pl.`pro_id`= bs.`pro_id`', 'left');
        $this->db->where(array('pgm.programe_name'=>ACD));        
        $this->db->order_by('cat.`category_name` ASC, bs.`band_score` DESC');
        return $this->db->get('')->result_array();
    }

    /*
     * Get all band_scores GT
     */
    function get_all_band_scores_gt($params = array())
    {        
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        $this->db->select('bs.`band_score_id`, bs.`band_score`,  bs.`band_range_lower`, bs.`band_range_upper`,bs.`active`,pl.`pro_name`, cat.`category_name`,pgm.`programe_name`,pgm.`programe_id`,tm.`test_module_name`');
        $this->db->from('`band_scores` bs');
        $this->db->join('`category_masters` cat', 'bs.`category_id`=cat.`category_id`', 'left');
        $this->db->join('`programe_masters` pgm', 'cat.`programe_id`=pgm.`programe_id`', 'left');
        $this->db->join('`test_module` tm', 'tm.`test_module_id`= cat.`test_module_id`');
        $this->db->join('`proficiency_level` pl', 'pl.`pro_id`= bs.`pro_id`', 'left');
        $this->db->where(array('pgm.programe_name'=>GT));
        $this->db->order_by('cat.`category_name` ASC, bs.`band_score` DESC');
        return $this->db->get('')->result_array();
    }
        
    /*
     * function to add new band_score
     */
    function add_band_score($params)
    {
        $this->db->insert('band_scores',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update band_score
     */
    function update_band_score($band_score_id,$params)
    {
        $this->db->where('band_score_id',$band_score_id);
        return $this->db->update('band_scores',$params);
    }
    
    /*
     * function to delete band_score
     */
    function delete_band_score($band_score_id)
    {
        return $this->db->delete('band_scores',array('band_score_id'=>$band_score_id));
    }
}
