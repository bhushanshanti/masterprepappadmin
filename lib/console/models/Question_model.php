<?php

 
class Question_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get question by question_id
     */
    function get_question($question_id)
    {
        return $this->db->get_where('questions',array('question_id'=>$question_id))->row_array();
    }
    
    /*
     * Get all questions count
     */
    function get_all_questions_count()
    {
        $this->db->from('questions');
        return $this->db->count_all_results();
    }
        
    /*
     * Get all questions
     */
    function get_all_questions($params = array())
    {
        $this->db->order_by('question_id', 'asc');
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        $this->db->select('q.question_id,q.question,q.correct_answer,q.correct_answer_explaination,q.active,qs.question_sets_heading');
        $this->db->from('`questions` q');
        $this->db->join('`question_sets` qs', 'q.`question_sets_id`=qs.`question_sets_id`', 'left');
        $this->db->order_by('q.`question_sets_id` desc');
        return $this->db->get()->result_array();
    }

    function get_all_questions2($question_sets_id)
    {
      $response = array();       
      $this->db->select('*');
      $this->db->from('questions qs');
      $this->db->where('qs.question_sets_id', $question_sets_id);
      $this->db->order_by('qs.question_no', 'ASC');
      return $this->db->get()->result();
    }
        
    /*
     * function to add new question
     */
    function add_question($params)
    {
        $this->db->insert('questions',$params);
        return $this->db->insert_id();
    }

    function get_question_options($question_id)
    {
        $this->db->select('option_value');
        $this->db->from('question_options');
        $this->db->where('question_id', $question_id);
        return $this->db->get()->result_array();
    }

    function getCorrectAns($question_id)
    {
        $this->db->select('correct_answer');
        $this->db->from('questions');
        $this->db->where('question_id', $question_id);        
        return $this->db->get()->row_array();
    }

    function add_question_options($params)
    {
        $this->db->insert('question_options',$params);
        return $this->db->insert_id();
    }


    /*
     * function to update question
     */
    function update_question($question_id,$params)
    {
        $this->db->where('question_id',$question_id);
        return $this->db->update('questions',$params);
    }
    
    /*
     * function to delete question
     */
    function delete_question($question_id)
    {
        $this->db->delete('questions',array('question_id'=>$question_id));
        return $this->db->delete('question_options',array('question_id'=>$question_id));

    }

    function delete_question_options($question_id)
    {
        return $this->db->delete('question_options',array('question_id'=>$question_id));
    }
}
