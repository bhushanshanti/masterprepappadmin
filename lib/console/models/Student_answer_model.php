<?php

class Student_answer_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function get_all_student_attempts_count($id){

        $this->db->distinct('');
        $this->db->select('collection_no');
        $this->db->from('student_answers');
        $this->db->where(array('student_id'=> $id ));
        return $this->db->count_all_results();
    }

    function get_all_student_attempts_count_ass($id){

        $this->db->distinct('');
        $this->db->select('collection_no');
        $this->db->from('student_answers');
        $this->db->where(array('student_id'=> $id ));
        return $this->db->count_all_results();
    }   

    function get_all_student_attempts($id, $params){
        
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }      
        $this->db->select('sr.collection_no, sr.ts_cat_assoc_id, sr.student_id,sr.band_score,sr.percentage, sr.created, ts.test_seriese_name,ts.test_seriese_type,cat.category_name, pgm.programe_name, ts_cat.paper_duration,ts_cat.max_marks,ts_cat.audio_file,ts_cat.audio_time,ts_cat.totalquest,sr.paper_checked,tm.test_module_name');
        $this->db->from('student_results sr');        
        $this->db->join('`ts_cat_assoc` ts_cat', 'sr.`ts_cat_assoc_id`= ts_cat.`ts_cat_assoc_id`');
        $this->db->join('`test_seriese` ts', 'ts.`test_seriese_id`= ts_cat.`test_seriese_id`');
        $this->db->join('`category_masters` cat', 'ts_cat.`category_id`= cat.`category_id`');
        $this->db->join('`programe_masters` pgm', 'cat.`programe_id`= pgm.`programe_id`');
        $this->db->join('`test_module` tm', 'tm.`test_module_id`=cat.`test_module_id`');       
        $this->db->where(array('sr.student_id' => $id));
        $this->db->order_by('sr.`paper_checked` ASC');
        return $this->db->get('')->result_array();
    }

    function get_student_attempts_reading($id){
        
        $this->db->select('sr.collection_no, sr.ts_cat_assoc_id, sr.student_id, sr.created,sr.band_score,sr.percentage,ts.test_seriese_name,ts.test_seriese_type,cat.category_name, pgm.programe_name, ts_cat.paper_duration,ts_cat.max_marks,ts_cat.audio_file,ts_cat.audio_time,ts_cat.totalquest,sr.paper_checked,tm.test_module_name');
        $this->db->from('student_results sr');        
        $this->db->join('`ts_cat_assoc` ts_cat', 'sr.`ts_cat_assoc_id`= ts_cat.`ts_cat_assoc_id`');
        $this->db->join('`test_seriese` ts', 'ts.`test_seriese_id`= ts_cat.`test_seriese_id`');
        $this->db->join('`category_masters` cat', 'ts_cat.`category_id`= cat.`category_id`');
        $this->db->join('`programe_masters` pgm', 'cat.`programe_id`= pgm.`programe_id`');
        $this->db->join('`test_module` tm', 'tm.`test_module_id`=cat.`test_module_id`');       
        $this->db->where(array('sr.student_id' => $id));
        $this->db->like('collection_no', READING);
        $this->db->order_by('sr.`paper_checked` ASC');
        return $this->db->get('')->result_array();
    }

    function get_student_attempts_listening($id){
        
        $this->db->select('sr.collection_no, sr.ts_cat_assoc_id, sr.student_id, sr.created,sr.band_score,sr.percentage, ts.test_seriese_name,ts.test_seriese_type,cat.category_name, pgm.programe_name, ts_cat.paper_duration,ts_cat.max_marks,ts_cat.audio_file,ts_cat.audio_time,ts_cat.totalquest,sr.paper_checked,tm.test_module_name');
        $this->db->from('student_results sr');        
        $this->db->join('`ts_cat_assoc` ts_cat', 'sr.`ts_cat_assoc_id`= ts_cat.`ts_cat_assoc_id`');
        $this->db->join('`test_seriese` ts', 'ts.`test_seriese_id`= ts_cat.`test_seriese_id`');
        $this->db->join('`category_masters` cat', 'ts_cat.`category_id`= cat.`category_id`');
        $this->db->join('`programe_masters` pgm', 'cat.`programe_id`= pgm.`programe_id`');
        $this->db->join('`test_module` tm', 'tm.`test_module_id`=cat.`test_module_id`');       
        $this->db->where(array('sr.student_id' => $id));
        $this->db->like('collection_no', LISTENING);
        $this->db->order_by('sr.`paper_checked` ASC');
        return $this->db->get('')->result_array();
    }

    function get_student_attempts_writing($id){
        
        $this->db->select('sr.collection_no, sr.ts_cat_assoc_id, sr.student_id, sr.created,sr.band_score,sr.percentage, ts.test_seriese_name,ts.test_seriese_type,cat.category_name, pgm.programe_name, ts_cat.paper_duration,ts_cat.max_marks,ts_cat.audio_file,ts_cat.audio_time,ts_cat.totalquest,sr.paper_checked,tm.test_module_name');
        $this->db->from('student_results sr');        
        $this->db->join('`ts_cat_assoc` ts_cat', 'sr.`ts_cat_assoc_id`= ts_cat.`ts_cat_assoc_id`');
        $this->db->join('`test_seriese` ts', 'ts.`test_seriese_id`= ts_cat.`test_seriese_id`');
        $this->db->join('`category_masters` cat', 'ts_cat.`category_id`= cat.`category_id`');
        $this->db->join('`programe_masters` pgm', 'cat.`programe_id`= pgm.`programe_id`');
        $this->db->join('`test_module` tm', 'tm.`test_module_id`=cat.`test_module_id`');       
        $this->db->where(array('sr.student_id' => $id));
        $this->db->like('collection_no', WRITING);
        $this->db->order_by('sr.`paper_checked` ASC');
        return $this->db->get('')->result_array();
    }

    function get_student_attempts_speaking($id){
        
        $this->db->select('sr.collection_no, sr.ts_cat_assoc_id, sr.student_id, sr.created,sr.band_score,sr.percentage, ts.test_seriese_name,ts.test_seriese_type,cat.category_name, pgm.programe_name, ts_cat.paper_duration,ts_cat.max_marks,ts_cat.audio_file,ts_cat.audio_time,ts_cat.totalquest,sr.paper_checked,tm.test_module_name');
        $this->db->from('student_results sr');        
        $this->db->join('`ts_cat_assoc` ts_cat', 'sr.`ts_cat_assoc_id`= ts_cat.`ts_cat_assoc_id`');
        $this->db->join('`test_seriese` ts', 'ts.`test_seriese_id`= ts_cat.`test_seriese_id`');
        $this->db->join('`category_masters` cat', 'ts_cat.`category_id`= cat.`category_id`');
        $this->db->join('`programe_masters` pgm', 'cat.`programe_id`= pgm.`programe_id`');
        $this->db->join('`test_module` tm', 'tm.`test_module_id`=cat.`test_module_id`');       
        $this->db->where(array('sr.student_id' => $id));
        $this->db->like('collection_no', SPEAKING);
        $this->db->order_by('sr.`paper_checked` ASC');
        return $this->db->get('')->result_array();
    }

    function get_student_attempts_ee($id){
        
        $this->db->select('sr.collection_no, sr.ts_cat_assoc_id, sr.student_id, sr.created,sr.band_score,sr.percentage, ts.test_seriese_name,ts.test_seriese_type,cat.category_name, pgm.programe_name, ts_cat.paper_duration,ts_cat.max_marks,ts_cat.audio_file,ts_cat.audio_time,ts_cat.totalquest,sr.paper_checked,tm.test_module_name');
        $this->db->from('student_results sr');        
        $this->db->join('`ts_cat_assoc` ts_cat', 'sr.`ts_cat_assoc_id`= ts_cat.`ts_cat_assoc_id`');
        $this->db->join('`test_seriese` ts', 'ts.`test_seriese_id`= ts_cat.`test_seriese_id`');
        $this->db->join('`category_masters` cat', 'ts_cat.`category_id`= cat.`category_id`');
        $this->db->join('`programe_masters` pgm', 'cat.`programe_id`= pgm.`programe_id`');
        $this->db->join('`test_module` tm', 'tm.`test_module_id`=cat.`test_module_id`');       
        $this->db->where(array('sr.student_id' => $id));
        $this->db->like('collection_no', 'EE');
        $this->db->order_by('sr.`paper_checked` ASC');
        return $this->db->get('')->result_array();
    }

    function get_student_attempts_et($id){
        
        $this->db->select('sr.collection_no, sr.ts_cat_assoc_id, sr.student_id, sr.created,sr.band_score,sr.percentage, ts.test_seriese_name,ts.test_seriese_type,cat.category_name, pgm.programe_name, ts_cat.paper_duration,ts_cat.max_marks,ts_cat.audio_file,ts_cat.audio_time,ts_cat.totalquest,sr.paper_checked,tm.test_module_name');
        $this->db->from('student_results sr');        
        $this->db->join('`ts_cat_assoc` ts_cat', 'sr.`ts_cat_assoc_id`= ts_cat.`ts_cat_assoc_id`');
        $this->db->join('`test_seriese` ts', 'ts.`test_seriese_id`= ts_cat.`test_seriese_id`');
        $this->db->join('`category_masters` cat', 'ts_cat.`category_id`= cat.`category_id`');
        $this->db->join('`programe_masters` pgm', 'cat.`programe_id`= pgm.`programe_id`');
        $this->db->join('`test_module` tm', 'tm.`test_module_id`=cat.`test_module_id`');       
        $this->db->where(array('sr.student_id' => $id));
        $this->db->like('collection_no', 'ET');
        $this->db->order_by('sr.`paper_checked` ASC');
        return $this->db->get('')->result_array();
    }

    function get_student_avgScore_l($id){
        
        $this->db->select('FORMAT(AVG(band_score),1) bs');
        $this->db->from('student_results');
        $this->db->where(array('student_id' => $id,'paper_checked'=>1));
        $this->db->like('collection_no', 'Listening');
        $this->db->group_by('`student_id`');
        return $this->db->get('')->row_array();
    }

    function get_student_avgScore_r($id){
        
        $this->db->select('FORMAT(AVG(band_score),1) bs');
        $this->db->from('student_results');
        $this->db->where(array('student_id' => $id,'paper_checked'=>1));
        $this->db->like('collection_no', 'Reading');
        $this->db->group_by('`student_id`');
        return $this->db->get('')->row_array();
    }

    function get_student_avgScore_w($id){
        
        $this->db->select('FORMAT(AVG(band_score),1) bs');
        $this->db->from('student_results');
        $this->db->where(array('student_id' => $id,'paper_checked'=>1));
        $this->db->like('collection_no', 'Writing');
        $this->db->group_by('`student_id`');
        return $this->db->get('')->row_array();
    }

    function get_student_avgScore_s($id){
        
        $this->db->select('FORMAT(AVG(band_score),1) bs');
        $this->db->from('student_results');
        $this->db->where(array('student_id' => $id,'paper_checked'=>1));
        $this->db->like('collection_no', 'Speaking');
        $this->db->group_by('`student_id`');
        return $this->db->get('')->row_array();
    }

    function get_student_avgScore_e($id){
        
        $this->db->select('FORMAT(AVG(percentage),1) bs');
        $this->db->from('student_results');
        $this->db->where(array('student_id' => $id,'paper_checked'=>1));
        $this->db->like('collection_no', 'EE');
        $this->db->group_by('`student_id`');
        return $this->db->get('')->row_array();
    }

    function get_all_student_attempts_ass($id, $params){
        
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        $this->db->select('sr.collection_no, sr.ts_cat_assoc_id, sr.student_id, sr.created, ts.test_seriese_name,ts.test_seriese_type,cat.category_name, pgm.programe_name, ts_cat.paper_duration,ts_cat.max_marks,ts_cat.audio_file,ts_cat.audio_time,ts_cat.totalquest,sr.paper_checked,tm.test_module_name');
        $this->db->from('student_results sr');        
        $this->db->join('`ts_cat_assoc` ts_cat', 'sr.`ts_cat_assoc_id`= ts_cat.`ts_cat_assoc_id`');
        $this->db->join('`test_seriese` ts', 'ts.`test_seriese_id`= ts_cat.`test_seriese_id`');
        $this->db->join('`category_masters` cat', 'ts_cat.`category_id`= cat.`category_id`');
        $this->db->join('`programe_masters` pgm', 'cat.`programe_id`= pgm.`programe_id`');
        $this->db->join('`test_module` tm', 'tm.`test_module_id`=cat.`test_module_id`');       
        $this->db->where(array('sr.assigned_to' => $id ));
        $this->db->order_by('sr.`created` desc');
        return $this->db->get('')->result_array();
    }

    function get_attempts_api($id){

        $this->db->distinct('');
        $this->db->select('sa.collection_no, sa.ts_cat_assoc_id, sa.created,ts.test_seriese_name,ts.test_seriese_type,cat.category_name, pgm.programe_name, ts_cat.paper_duration,ts_cat.max_marks,ts_cat.audio_file,ts_cat.audio_time,ts_cat.totalquest');
        $this->db->from('student_answers sa');          
        $this->db->join('`ts_cat_assoc` ts_cat', 'sa.`ts_cat_assoc_id`= ts_cat.`ts_cat_assoc_id`');
        $this->db->join('`test_seriese` ts', 'ts.`test_seriese_id`= ts_cat.`test_seriese_id`');
        $this->db->join('`category_masters` cat', 'ts_cat.`category_id`= cat.`category_id`');
        $this->db->join('`programe_masters` pgm', 'cat.`programe_id`= pgm.`programe_id`');        
        $this->db->where(array('sa.student_id'=>$id));
        $this->db->order_by('sa.`created` desc');
        return $this->db->get('')->result_array();
        //print_r($this->db->last_query());exit;
    }    

    function get_attempts_api2($id,$test_module_id,$programe_id,$subTest){

        $this->db->distinct('');
        $this->db->select('           
            sr.test_seriese_id,
            ts.test_seriese_name
        ');
        $this->db->from('student_results sr');
        $this->db->join('test_seriese ts', 'ts.test_seriese_id= sr.test_seriese_id');
        if($subTest == 'GRAMMAR'){
           $this->db->where(array('sr.student_id'=>$id,'sr.test_module_id' => EE_ID,'sr.type'=>'GRAMMAR')); 
        }elseif($subTest == 'SPI'){
            $this->db->where(array('sr.student_id'=> $id,'sr.test_module_id'=>$test_module_id,'sr.programe_id'=> $programe_id,'sr.type'=>'SPI'));
        }elseif($subTest == 'PARENT'){
            $this->db->where(array('sr.student_id'=> $id,'sr.test_module_id'=> $test_module_id,'sr.programe_id'=> $programe_id,'sr.type'=>'PARENT'));
        }else{}
        $this->db->order_by('ts.test_seriese_name ASC');
        return $this->db->get('')->result_array();
    }

    function get_attempts_testcat($test_seriese_id,$id,$test_module_id,$programe_id,$subTest){

        $this->db->select('
            sr.student_result_id,
            sr.collection_no,            
            sr.ts_cat_assoc_id,
            cat.category_name, 
            sr.rank, 
            sr.marks_secured,
            CONCAT(sr.percentage, " %") as percentage,
            sr.band_score,
            sr.time_taken,
            sr.totalQuestion,
            sr.skipped,
            sr.attempted,
            sr.paper_checked,            
            date_format(sr.created, "%D %b %y %I:%i %p") as created,         
            ts_cat.paper_duration,
            ts_cat.max_marks,
            pl.pro_name 
        ');
        $this->db->from('student_results sr');            
        $this->db->join('`ts_cat_assoc` ts_cat', 'sr.`ts_cat_assoc_id`= ts_cat.`ts_cat_assoc_id`');
        $this->db->join('`category_masters` cat', 'ts_cat.`category_id`= cat.`category_id`');
        $this->db->join('`proficiency_level` pl', 'pl.`pro_id`= sr.`pro_id`', 'left');
        
        if($subTest == 'GRAMMAR'){

            $this->db->where(array('sr.student_id' => $id, 'sr.test_seriese_id' => $test_seriese_id, 'sr.test_module_id' => EE_ID ,'sr.type'=>'GRAMMAR' ));

        }elseif($subTest == 'SPI'){

            $this->db->where(array('sr.student_id' => $id, 'sr.test_seriese_id' => $test_seriese_id, 'sr.test_module_id' => $test_module_id,'sr.programe_id' => $programe_id,'sr.type'=>'SPI'));
            
        }elseif($subTest == 'PARENT'){

            $this->db->where(array('sr.student_id' => $id, 'sr.test_seriese_id' => $test_seriese_id, 'sr.test_module_id' => $test_module_id,'sr.programe_id' => $programe_id, 'sr.type'=>'PARENT'));
        }else{}
        $this->db->order_by('sr.created DESC');
        return $this->db->get('')->result_array();
    }

    function get_results($collection_no,$ts_cat_assoc_id){

        $this->db->select('
            sa.student_answers_id,
            sa.collection_no,
            sa.question_id,
            sa.is_img,
            sa.student_answers,
            sa.checker_remarks,
            sa.created,
            sa.modified,
            q.question_id, 
            q.question_sets_id, 
            q.question_no,
            q.question,
            q.correct_answer, 
            q.correct_answer_explaination, 
            sa.marks_secured, 
            qs.question_sets_heading, 
            qs.question_sets_desc, 
            REPLACE(qs.question_sets_para,"~~~",".........") as question_sets_para,
            sec.section_heading,
            ts_cat.paper_duration,
            ts_cat.max_marks,
            ts.test_seriese_name,
            ts.test_seriese_type,
            ts.test_module_id,
            cat.category_name,
            sr.band_score,
            sr.paper_checked,
            tm.test_module_name');        
        $this->db->from('student_answers sa');        
        $this->db->join('`questions` q', 'q.`question_id`= sa.`question_id`', 'left');
        $this->db->join('`question_sets` qs', 'qs.`question_sets_id`= q.`question_sets_id`', 'left');
        $this->db->join('`sections` sec', 'sec.`section_id`= qs.`section_id`', 'left'); 
        $this->db->join('`ts_cat_assoc` ts_cat', 'ts_cat.`ts_cat_assoc_id`= sa.`ts_cat_assoc_id`', 'left');
        $this->db->join('`test_seriese` ts', 'ts.`test_seriese_id`= ts_cat.`test_seriese_id`', 'left');
        $this->db->join('`category_masters` cat', 'cat.`category_id`= ts_cat.`category_id`', 'left');
        $this->db->join('`student_results` sr', 'sr.`collection_no`= sa.`collection_no`', 'left');
        $this->db->join('`test_module` tm', 'tm.`test_module_id`=cat.`test_module_id`');             
        $this->db->where(array('sa.collection_no' => $collection_no,'sa.ts_cat_assoc_id'=>$ts_cat_assoc_id));
        $this->db->order_by('q.`question_no` asc');
        return $this->db->get('')->result_array();
    }

    function get_results_basic_api_old($ts_cat_assoc_id){

        $this->db->select("ts.test_seriese_name, ts.test_seriese_type, 
        cat.category_name, pgm.programe_name, ts_cat.paper_duration, ts_cat.max_marks,ts_cat.audio_file,ts_cat.audio_time,ts_cat.totalquest");
        $this->db->from('test_seriese ts');
        $this->db->join('ts_cat_assoc ts_cat', 'ts_cat.test_seriese_id = ts.test_seriese_id');
        $this->db->join('category_masters cat', 'cat.category_id = ts_cat.category_id');
        $this->db->join('programe_masters pgm', 'pgm.programe_id = cat.programe_id');
        $this->db->where(array('ts_cat.ts_cat_assoc_id' => $ts_cat_assoc_id));
        return $this->db->get()->result_array();
    }

    function get_results_basic_api($ts_cat_assoc_id){

        $this->db->select("ts.test_seriese_name, ts.test_seriese_type, 
        cat.category_name, pgm.programe_name, ts_cat.paper_duration, ts_cat.max_marks,ts_cat.audio_file,ts_cat.audio_time,ts_cat.totalquest");
        $this->db->from('test_seriese ts');
        $this->db->join('ts_cat_assoc ts_cat', 'ts_cat.test_seriese_id = ts.test_seriese_id');
        $this->db->join('category_masters cat', 'cat.category_id = ts_cat.category_id');
        $this->db->join('programe_masters pgm', 'pgm.programe_id = cat.programe_id');
        $this->db->where(array('ts_cat.ts_cat_assoc_id' => $ts_cat_assoc_id));
        return $this->db->get()->row_array();
    }

    function get_results_api($collection_no,$ts_cat_assoc_id){

        $this->db->select('
            sec.section_heading,
            qs.question_sets_heading,
            REPLACE(qs.question_sets_para,"~~~",".........") as question_sets_para,
            q.question_no,q.question,
            sa.student_answers,
            q.correct_answer,
            q.correct_answer_explaination,
             sa.marks_secured');        
        $this->db->from('student_answers sa');        
        $this->db->join('`questions` q', 'q.`question_id`= sa.`question_id`', 'left');
        $this->db->join('`question_sets` qs', 'qs.`question_sets_id`= q.`question_sets_id`', 'left');
        $this->db->join('`sections` sec', 'sec.`section_id`= qs.`section_id`', 'left');                    
        $this->db->where(array('sa.collection_no'=>$collection_no,'sa.ts_cat_assoc_id'=>$ts_cat_assoc_id));
        $this->db->group_by('sa.student_answers_id');
        $this->db->order_by('q.`question_no` asc');
        return $this->db->get('')->result_array();
    }

    function get_sum($collection_no){

        return $this->db->query("SELECT SUM(`marks_secured`) as `total` FROM `student_answers` WHERE `collection_no`='".$collection_no."' ")->row_array();
    }
    
    /*
     * Get student_answer by student_answers_id
     */
    function get_student_answer($student_answers_id)
    {
        return $this->db->get_where('student_answers',array('student_answers_id'=>$student_answers_id))->row_array();
    }

      
    /*
     * Get all student_answers
     */
    function get_all_student_answers()
    {
        $this->db->order_by('student_answers_id', 'desc');
        return $this->db->get('student_answers')->result_array();
    }
        
    /*
     * function to add new student_answer
     */
    function add_student_answer($params)
    {
        $this->db->insert('student_answers',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update student_answer
     */
    function update_student_answer($student_answers_id,$params)
    {
        $this->db->where('student_answers_id',$student_answers_id);
        return $this->db->update('student_answers',$params);
    }


    function update_marks($student_answers_id,$params)
    {
        $this->db->where('student_answers_id',$student_answers_id);
        return $this->db->update('student_answers',$params);
    }

    function update_remarks($student_answers_id,$params)
    {
        $this->db->where('student_answers_id',$student_answers_id);
        return $this->db->update('student_answers',$params);
    }

    function recheck_paper($collection_no)
    {
        $params=array(                
            'paper_checked'  => NULL,                
        ); 
        $this->db->where('collection_no',$collection_no);
        return $this->db->update('student_results',$params);
    }

    function update_rank($test_seriese_id,$ts_cat_assoc_id)
    {
        
        $query  =   "SET @currentRank := 0, @lastRating := null, @rowNumber := 1";
        $this->db->query($query);
        $sql = "update 
        `student_results` r
        inner join (
            select
                `student_result_id`,
                @currentRank := if(@lastRating = `marks_secured`, @currentRank, @rowNumber) `rank`,
                @rowNumber := @rowNumber + if(@lastRating = `marks_secured`, 0, 1) `rowNumber`,
                @lastRating := `marks_secured`
            from `student_results`
            where `test_seriese_id` = ".$test_seriese_id." and `ts_cat_assoc_id` = ".$ts_cat_assoc_id."
            order by `marks_secured` desc
        )var on
            var.`student_result_id` = r.`student_result_id`
        set r.`rank` = var.`rank`";
        return $this->db->query($sql);
    }
    
    /*
     * function to delete student_answer
     */
    function delete_student_answer($student_answers_id)
    {
        return $this->db->delete('student_answers',array('student_answers_id'=>$student_answers_id));
    }

    /*
     * function to delete student_answer
     */
    function remove_student_result($collection_no,$ts_cat_assoc_id,$student_id)
    {
        $this->db->trans_start();
        $this->db->delete('student_answers',
            array('collection_no'=>$collection_no,
                'ts_cat_assoc_id'=>$ts_cat_assoc_id,
                'student_id'=>$student_id,

            )
        );
        $this->db->delete('student_results',
            array('collection_no'=>$collection_no,
                'ts_cat_assoc_id'=>$ts_cat_assoc_id,
                'student_id'=>$student_id,

            )
        );
        $this->db->delete('student_notification',
            array('collection_no'=>$collection_no,
                'ts_cat_assoc_id'=>$ts_cat_assoc_id,
                'student_id'=>$student_id,

            )
        );
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE){
            return true;
        }else{
            return false;
        }
    }
}
