<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/
 
class State_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get category_master by category_id
     */
    function get_state($state_id)
    {
        return $this->db->get_where('state',array('state_id'=>$state_id))->row_array();
    }
    
    /*
     * Get all category_masters count
     */
    function get_all_state_count()
    {
        $this->db->from('state');
        return $this->db->count_all_results();
    }
        
    /*
     * Get all category_masters
     */
    function get_all_state($params = array())
    {        
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }  
        $this->db->select('st.`state_id`,st.`state_name`,st.`active`, cnt.`name`');
        $this->db->from('state st'); 
        $this->db->join('`country` cnt', 'st.`country_id`=cnt.`country_id`');
        $this->db->order_by('cnt.`name`', 'ASC');
        return $this->db->get()->result_array();
    } 
    

    function get_all_state_active($params = array()){
       
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        } 
        $this->db->select('st.`state_id`,st.`state_name`,st.`active`, cnt.`name`');
        $this->db->from('state');
        $this->db->join('country', 'country.id = st.country_id', 'left');
        $this->db->where('st.active', 1);  
        $this->db->order_by('state_id', 'desc');
        return $this->db->get()->result_array();
    }

    function get_state_list($country_id){       
        
        $this->db->select('state_id,state_name');
        $this->db->from('state');
        $this->db->where(array('country_id'=> $country_id,'active'=>1));  
        $this->db->order_by('state_name', 'desc');
        return $this->db->get()->result_array();
    }    
        
    /*
     * function to add new category_master
     */
    function add_state($params)
    {        
        $state_name =  $params['state_name'];
        $country_id =  $params['country_id'];
        $this->db->where('state_name', $state_name);
        $this->db->where('country_id', $country_id);
        $query = $this->db->get('state');
        $count_row = $query->num_rows();
        if ($count_row > 0) {          
            return FALSE;
        }else {
          
            $this->db->insert('state',$params);
            return $this->db->insert_id();
        }
    }

    function dupliacte_state($country_id,$state_name){

        $this->db->where('state_name', $state_name);
        $this->db->where('country_id', $country_id);
        $query = $this->db->get('state');
        $count_row = $query->num_rows();
        if ($count_row > 0) {          
            return 'DUPLICATE';
         } else {          
            
            return 'NOT DUPLICATE';
         }
    }
    
    /*
     * function to update category_master
     */
    function update_state($state_id,$params)
    {
        $this->db->where('state_id',$state_id);
        return $this->db->update('state',$params);
    }
    
    /*
     * function to delete category_master
     */
    function delete_state($state_id)
    {
        return $this->db->delete('state',array('state_id'=>$state_id));
    }
}
