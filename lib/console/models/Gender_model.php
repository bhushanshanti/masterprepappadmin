<?php

 
class Gender_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    /*
     * Get gender by id
     */
    function get_gender($id)
    {
        return $this->db->get_where('gender',array('id'=>$id))->row_array();
    }
    /*
     * Get all gender count
     */
    function get_all_gender_count()
    {
        $this->db->from('gender');
        return $this->db->count_all_results();
    }  
    /*
     * Get all gender
     */
    function get_all_gender($params = array())
    {
         $this->db->order_by('id', 'desc');
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        return $this->db->get('gender')->result_array();
    }

    function get_all_gender_active($params = array())
    {
        $this->db->where('active', '1');
        $this->db->order_by('id', 'desc');
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        return $this->db->get('gender')->result_array();
    }
    /*
     * function to add new gender
     */
    function add_gender($params)
    {
        $this->db->insert('gender',$params);
        return $this->db->insert_id();
    }    
    /*
     * function to update gender
     */
    function update_gender($id,$params)
    {
        $this->db->where('id',$id);
        return $this->db->update('gender',$params);
    } 
    /*
     * function to delete gender
     */
    function delete_gender($id)
    {
        return $this->db->delete('gender',array('id'=>$id));
    }

    
}
