<?php

class Question_type_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }    
    /*
     * Get question_type by question_type_id
     */
    function get_question_type($question_type_id)
    {
        return $this->db->get_where('question_types',array('question_type_id'=>$question_type_id))->row_array();
    }    
    /*
     * Get all question_types count
     */
    function get_all_question_types_count()
    {
        $this->db->from('question_types');
        return $this->db->count_all_results();
    }        
    /*
     * Get all question_types
     */
    function get_all_question_types($params = array())
    {       
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }  
        $this->db->select('qt.`question_type_id`,qt.`type_name`,qt.`active`, qb.`behavior_name`');
        $this->db->from('question_types qt'); 
        $this->db->join('`question_behavior` qb', 'qt.`question_behavior_id`=qb.`question_behavior_id`','left');
        $this->db->order_by('qb.`behavior_name`', 'ASC');
        return $this->db->get()->result_array();
    }

    function get_all_question_types_active($params = array())
    {   
        $this->db->select('qt.`question_type_id`,qt.`type_name`,qt.`active`,qt.`question_behavior_id`, qb.`behavior_name`');
        $this->db->from('question_types qt'); 
        $this->db->join('`question_behavior` qb', 'qt.`question_behavior_id`=qb.`question_behavior_id`','left');
        $this->db->where('qt.active', '1');
        $this->db->order_by('qt.type_name', 'ASC');
        return $this->db->get()->result_array();
    } 

    function get_qb_id($question_type_id)
    {        
      $response = array();
      $this->db->select('question_behavior_id');
      $this->db->from('question_types');      
      $this->db->where('question_type_id', $question_type_id);
      return $this->db->get()->row_array();
    }       
    /*
     * function to add new question_type
     */
    function add_question_type($params)
    {        
        $type_name =  $params['type_name'];
        $this->db->where('type_name', $type_name);
        $query = $this->db->get('question_types');
        $count_row = $query->num_rows();
        if ($count_row > 0) {          
            return FALSE;
         } else {

            $this->db->insert('question_types',$params);
            return $this->db->insert_id();
         }
    }

    function dupliacte_question_type_name_master($type_name){

        $this->db->where('type_name', $type_name);
        $query = $this->db->get('question_types');
        $count_row = $query->num_rows();
        if ($count_row > 0) {          
            return 'FALSE';
        }else{            
            return 'TRUE';
        }
    }    
    /*
     * function to update question_type
     */
    function update_question_type($question_type_id,$params)
    {
        $this->db->where('question_type_id',$question_type_id);
        return $this->db->update('question_types',$params);
    }    
    /*
     * function to delete question_type
     */
    function delete_question_type($question_type_id)
    {
        return $this->db->delete('question_types',array('question_type_id'=>$question_type_id));
    }
}
