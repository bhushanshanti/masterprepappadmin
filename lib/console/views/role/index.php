<input class="form-control" type="text" placeholder="Search" aria-label="Search" id="myInput">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header bg-danger">
                <h3 class="box-title text-primary"><?php echo $title;?></h3><br/>
                <h5 class="box-title text-danger"><i>Note: Before assigning role please refresh role</i></h5>
            	<div class="box-tools">
                    <a href="<?php echo site_url('role/add'); ?>" class="btn btn-success btn-sm">Add Role</a>                    
                </div>
                <a href="<?php echo site_url('role/run_role');?>" class="btn btn-danger btn-sm">
                    <i class="fa fa-refresh"></i> Refresh role </a>
                <?php echo $this->session->flashdata('flsh_msg'); ?>
            </div>
            <div class="box-body table-responsive">
                <table class="table table-striped table-bordered table-sm">
                    <thead>
                    <tr>
                        <th><?php echo SR;?></th>
						<!-- <th>ID</th>	 -->					
						<th>Role name</th>
                        <th><?php echo STATUS;?></th>
                        <th><?php echo ACTION;?></th>
                    </tr>
                </thead>
                <tbody id="myTable">
                    <?php $sr=0;foreach($roles as $r){$zero=0;$one=1;$pk='id'; $table='roles';$sr++; ?>
                    <tr>
						<td><?php echo $sr; ?></td>
                       <!--  <td><?php echo $r['id']; ?></td> -->						 
						<td><?php echo $r['name']; ?></td>
                        <td>
                            <?php 
                            if($r['active']==1){
                                echo '<span class="text-success"><a href="javascript:void(0);" id='.$r['id'].' data-toggle="tooltip" title="Click to De-activate" onclick=activate_deactivete('.$r['id'].','.$zero.',"'.$table.'","'.$pk.'") >'.ACTIVE.'</a></span>';
                            }else{
                                echo '<span class="text-danger"><a href="javascript:void(0);" id='.$r['id'].' title="Click to Activate" onclick=activate_deactivete('.$r['id'].','.$one.',"'.$table.'","'.$pk.'") >'.DEACTIVE.'</a></span>';
                            }
                            ?>                                
                        </td>
						<td>
                            <a href="<?php echo site_url('role/manage_role/'.$r['id']); ?>" class="btn btn-info btn-xs" data-toggle="tooltip" title="Manage & assign roles"><span class="fa fa-users"></span> Manage role</a>
                            <a href="<?php echo site_url('role/edit/'.$r['id']); ?>" class="btn btn-info btn-xs" data-toggle="tooltip" title="Edit"><span class="fa fa-pencil"></span> </a> 
                            <a href="<?php echo site_url('role/remove/'.$r['id']); ?>" class="btn btn-danger btn-xs" data-toggle="tooltip" title="Delete" onclick="return confirm('Are you sure you want to delete this item?');"><span class="fa fa-trash"></span> </a>


                        </td>
                    </tr>

                    <?php } ?>
                    </tbody>
                </table>
                <div class="pull-right">
                    <?php echo $this->pagination->create_links(); ?>                    
                </div>                
            </div>
        </div>
    </div>
</div>
