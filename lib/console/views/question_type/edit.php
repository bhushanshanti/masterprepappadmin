<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Edit Question Type</h3>
            </div>
            <?php echo $this->session->flashdata('flsh_msg'); ?>
			<?php echo form_open('question_type/edit/'.$question_type['question_type_id']); ?>
			<div class="box-body">
				<div class="row clearfix">
					
					<div class="col-md-6">
						<label for="type_name" class="control-label"><span class="text-danger">*</span>Question Type Name</label>
						<div class="form-group has-feedback">
							<input type="text" name="type_name" value="<?php echo ($this->input->post('type_name') ? $this->input->post('type_name') : $question_type['type_name']); ?>" class="form-control" id="type_name" maxlength="50"/>
							<span class="glyphicon glyphicon-th-large form-control-feedback"></span>
							<span class="text-danger"><?php echo form_error('type_name');?></span>
						</div>
					</div>

					<div class="col-md-6">
						<label for="question_behavior_id" class="control-label"><span class="text-danger">*</span>Behavior</label>
						<div class="form-group">
							<select name="question_behavior_id" class="form-control selectpicker" data-show-subtext="true" data-live-search="true">
								<option value="">Select behavior</option>
								<?php 
								foreach($all_behavior_masters as $b)
								{
									$selected = ($b['question_behavior_id'] == $question_type['question_behavior_id']) ? ' selected="selected"' : "";

									echo '<option value="'.$b['question_behavior_id'].'" '.$selected.'>'.$b['behavior_name'].'</option>';
								} 
								?>
							</select>
							<span class="text-danger"><?php echo form_error('question_behavior_id');?></span>
						</div>
					</div>

					<div class="col-md-12">
						<div class="form-group">
							<input type="checkbox" name="active" value="1" <?php echo ($question_type['active']==1 ? 'checked="checked"' : ''); ?> id='active' />
							<label for="active" class="control-label">Active</label>
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
            	<button type="submit" class="btn btn-success">
					<i class="fa fa-check"></i> Save
				</button>
	        </div>				
			<?php echo form_close(); ?>
		</div>
    </div>
</div>