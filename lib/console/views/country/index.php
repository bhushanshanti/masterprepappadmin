<input class="form-control" type="text" placeholder="Search" aria-label="Search" id="myInput">
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header bg-danger">
                <h3 class="box-title text-primary"><?php echo $title;?></h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('country/add'); ?>" class="btn btn-success btn-sm">Add</a> 
                </div>
                <?php echo $this->session->flashdata('flsh_msg'); ?>
            </div>
            <div class="box-body table-responsive">
                <table class="table table-striped table-bordered table-sm">
                    <thead>
                    <tr>
                        <th><?php echo SR;?></th>
                        <th>ISO</th>					
						<th>Country name</th>						
						<th>Phone code</th>
                        <th>Flag</th>	
                        <th>validation</th>   
						<th><?php echo STATUS;?></th>
                        <th><?php echo ACTION;?></th>
                    </tr>
                   </thead>
                    <tbody id="myTable">
                    <?php $sr=0;foreach($country as $p){ $zero=0;$one=1;$pk='country_id'; $table='country';$sr++; ?>
                    <tr>
						<td><?php echo $sr; ?></td>
                        <td><?php echo $p['iso3']; ?></td>					
						<td><?php echo $p['name']; ?></td>						
						<td><?php echo $p['phonecode']; ?></td>					
						<td>
                            <?php if($p['flag']){ ?>
                            <img src='<?php echo $p['flag'];?>' style="width:40px;height:30px">
                        <?php }else{ echo NO_ICON;?>
                                 
                        <?php } ?>
                        </td>
                        <td><?php echo $p['phoneNo_limit']; ?></td>
                        <td>
                            <?php 
                            if($p['active']==1){
                                echo '<span class="text-success"><a href="javascript:void(0);" id='.$p['country_id'].' data-toggle="tooltip" title="Click to De-activate" onclick=activate_deactivete('.$p['country_id'].','.$zero.',"'.$table.'","'.$pk.'") >'.ACTIVE.'</a></span>';
                            }else{
                                echo '<span class="text-danger"><a href="javascript:void(0);" id='.$p['country_id'].' title="Click to Activate" onclick=activate_deactivete('.$p['country_id'].','.$one.',"'.$table.'","'.$pk.'") >'.DEACTIVE.'</a></span>';
                            }
                            ?>                                
                        </td>
						<td>
                            <a href="<?php echo site_url('country/edit/'.$p['country_id']); ?>" class="btn btn-info btn-xs" data-toggle="tooltip" title="Edit"><span class="fa fa-pencil"></span> </a> 
                            <a href="<?php echo site_url('country/remove/'.$p['country_id']); ?>" class="btn btn-danger btn-xs" data-toggle="tooltip" title="Delete" onclick="return confirm('Are you sure you want to delete this item?');"><span class="fa fa-trash"></span> </a>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
                </table>
                <div class="pull-right">
                    <?php echo $this->pagination->create_links(); ?>                    
                </div>                
            </div>
        </div>
    </div>
</div>
