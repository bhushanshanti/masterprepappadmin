<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Edit Live Lecture </h3>
            </div>
            <?php echo $this->session->flashdata('flsh_msg'); ?>
			<?php echo form_open_multipart('live_lecture/edit/'.$live_lecture['live_lecture_id']); ?>
			<div class="box-body">
				<div class="row clearfix">
					
					<div class="col-md-6">
						<label for="category_id" class="control-label"><span class="text-danger">*</span>Category </label>
						<div class="form-group">
							<select name="category_id" class="form-control  selectpicker" data-show-subtext="true" data-live-search="true">
								<option value="">Select category</option>
								<?php 
								foreach($all_category_masters as $category_master)
								{
									$selected = ($category_master['category_id'] == $live_lecture['category_id']) ? ' selected="selected"' : "";

									echo '<option value="'.$category_master['category_id'].'" '.$selected.'>'.$category_master['programe_name'].' | '.$category_master['category_name'].'</option>';
								} 
								?>
							</select>
							<span class="text-danger"><?php echo form_error('category_id');?></span>
						</div>
					</div>
					<div class="col-md-6">
						<label for="live_lecture_title" class="control-label"><span class="text-danger">*</span>Live Lecture Title</label>
						<div class="form-group">
							<input type="text" name="live_lecture_title" value="<?php echo ($this->input->post('live_lecture_title') ? $this->input->post('live_lecture_title') : $live_lecture['live_lecture_title']); ?>" class="form-control" id="live_lecture_title" maxlength="100"/>
							<span class="text-danger"><?php echo form_error('live_lecture_title');?></span>
						</div>
					</div>
					<div class="col-md-6">
						<label for="video_url" class="control-label"><span class="text-danger">*</span>Video URL</label>
						[
						<span class="text-danger"><a href="<?php echo site_url('gallery/add');?>" target="_blank">
							<?php echo GALLERY_URL_LABEL;?></a>
						</span>
						]&nbsp;
						[
						<span class="text-danger"><a href="<?php echo site_url('gallery/index');?>" target="_blank">
							<?php echo GALLERY_URL_LABEL_LIST;?></a>
						</span>
						]						
						<div class="form-group has-feedback">
							<input type="text" name="video_url" value="<?php echo ($this->input->post('video_url') ? $this->input->post('video_url') : $live_lecture['video_url']); ?>" class="form-control" id="video_url" />
							<span class="glyphicon glyphicon-link form-control-feedback"></span>
							<span class="text-danger"><?php echo form_error('video_url');?></span>
						</div>
					</div>
					<div class="col-md-6">
						<label for="screenshot" class="control-label">Video Screen Image</label>
						<?php echo LIVE_LECTURE_ALLOWED_TYPES_LABEL;?>
						<div class="form-group">
							<input type="file" name="screenshot" value="<?php echo ($this->input->post('screenshot') ? $this->input->post('screenshot') : $live_lecture['screenshot']); ?>" class="form-control" id="screenshot" />
							<span>
								<?php 
								if(isset($live_lecture['screenshot'])){      
                                    echo '<span>
                                            <a href="'.$live_lecture['screenshot'].'" target="_blank">'.OPEN_FILE.'</a>
                                        </span>';
                                }else{
                                    echo NO_FILE;
                                }
                                ?>
						</span>
						</div>
						
					</div>
					<div class="col-md-12">
						<label for="short_desc" class="control-label">Short Description</label>
						<div class="form-group has-feedback">
							<textarea name="short_desc" class="form-control myckeditor" id="short_desc"><?php echo ($this->input->post('short_desc') ? $this->input->post('short_desc') : $live_lecture['short_desc']); ?></textarea>
							<span class="glyphicon glyphicon-text-size form-control-feedback"></span>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<input type="checkbox" name="active" value="1" <?php echo ($live_lecture['active']==1 ? 'checked="checked"' : ''); ?> id='active' />
							<label for="active" class="control-label">Active</label>
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
            	<button type="submit" class="btn btn-success">
					<i class="fa fa-check"></i> Save
				</button>
	        </div>				
			<?php echo form_close(); ?>
		</div>
    </div>
</div>