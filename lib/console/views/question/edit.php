<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Edit Question </h3>
            </div>
            <?php echo $this->session->flashdata('flsh_msg'); ?>
			<?php echo form_open('question/edit/'.$question['question_id']); ?>
			<div class="box-body">
				<div class="row clearfix">

					<?php 
					if(!empty($question_options)){

						foreach ($question_options as $qo) {
							$opt[] = $qo['option_value'];
						}
						$options = implode("~~",$opt);

					}else{
						$options='';
					}
					
					?>
					<div class="col-md-6">
						<label for="question_sets_id" class="control-label"><span class="text-danger">*</span>Question Set</label>
						<div class="form-group">
							<select name="question_sets_id" class="form-control">
								<option value="">Select question set</option>
								<?php 
								foreach($all_question_sets as $question_set)
								{
									$selected = ($question_set['question_sets_id'] == $question['question_sets_id']) ? ' selected="selected"' : "";

									echo '<option value="'.$question_set['question_sets_id'].'" '.$selected.'>'.$question_set['question_sets_heading'].'</option>';
								} 
								?>
							</select>
							<span class="text-danger"><?php echo form_error('question_sets_id');?></span>
						</div>
					</div>

					<div class="col-md-6">
						<label for="question_no" class="control-label"><span class="text-danger">*</span>Question No.</label>
						<div class="form-group">
							<input type="text" name="question_no" value="<?php echo ($this->input->post('question_no') ? $this->input->post('question_no') : $question['question_no']); ?>" class="form-control" id="question_no" maxlength="2"/>
							<span class="text-danger"><?php echo form_error('question_no');?></span>
							<span class="text-danger"><?php echo $this->session->flashdata('flsh_msg'); ?></span>
						</div>
					</div>


					<div class="col-md-12">
						<label for="question" class="control-label">Question</label>
						<div class="form-group">
							
							<textarea name="question" class="form-control myckeditor" id="question"><?php echo ($this->input->post('question') ? $this->input->post('question') : $question['question']); ?></textarea>
							<span class="text-danger"><?php echo form_error('question');?></span>
						</div>
					</div>


					<?php
					if(						 
						$qt_name['type_name']=='Pick from a list' OR 
						$qt_name['type_name']=='Matching' OR 
						$qt_name['type_name']=='Multiple choice' OR 
						$qt_name['type_name']=='Classification' OR 
						$qt_name['type_name']=='Sentence completion with a box' ){
					?>
					<div class="col-md-6">
						<label for="correct_answer" class="control-label"><span class="text-danger">*</span>correct_answer</label>
						<div class="form-group">
							<select name="correct_answer" class="form-control">
								<option value="">Select correct answer</option>
								<?php 								
								$correct_answer_values = array(
									'A'=>'A','B'=>'B','C'=>'C','D'=>'D',
									'E'=>'E','F'=>'F','G'=>'G','H'=>'H',									
								);
								foreach($correct_answer_values as $value => $display_text)
								{
									$selected = ($value == $question['correct_answer']) ? ' selected="selected"' : "";
									echo '<option value="'.$value.'" '.$selected.'>'.$display_text.'</option>';
								} 
								?>
							</select>
							<span class="text-danger"><?php echo form_error('correct_answer');?></span>
						</div>
					</div>

					<?php }elseif($qt_name['type_name']=='True/False/Not Given'){ ?>
					
					<div class="col-md-6">
						<label for="correct_answer" class="control-label"><span class="text-danger">*</span>correct_answer</label>
						<div class="form-group">
							<select name="correct_answer" class="form-control">
								<option value="">Select correct answer</option>
								<?php 								
								$correct_answer_values = array(
									'A'=>'True','B'=>'False','C'=>'Not Given',
								);
								foreach($correct_answer_values as $value => $display_text)
								{
									$selected = ($value == $question['correct_answer']) ? ' selected="selected"' : "";
									echo '<option value="'.$value.'" '.$selected.'>'.$display_text.'</option>';
								} 
								?>
							</select>
							<span class="text-danger"><?php echo form_error('correct_answer');?></span>
						</div>
					</div>

					<?php }elseif($qt_name['type_name']=='Yes/No/Not Given'){  ?>

					<div class="col-md-6">
						<label for="correct_answer" class="control-label"><span class="text-danger">*</span>correct_answer</label>
						<div class="form-group">
							<select name="correct_answer" class="form-control">
								<option value="">Select correct answer</option>
								<?php 								
								$correct_answer_values = array(
									'A'=>'Yes','B'=>'No','C'=>'Not Given',
								);
								foreach($correct_answer_values as $value => $display_text)
								{
									$selected = ($value == $question['correct_answer']) ? ' selected="selected"' : "";
									echo '<option value="'.$value.'" '.$selected.'>'.$display_text.'</option>';
								} 
								?>
							</select>
							<span class="text-danger"><?php echo form_error('correct_answer');?></span>
						</div>
					</div>

					<?php }else{ ?>
					<div class="col-md-12">
						<label for="correct_answer" class="control-label"><span class="text-danger">*</span>Correct Answer</label>
						<div class="form-group">
							<textarea name="correct_answer" class="form-control" id="correct_answer"><?php echo ($this->input->post('correct_answer') ? $this->input->post('correct_answer') : $question['correct_answer']); ?></textarea>
							<span class="text-danger"><?php echo form_error('correct_answer');?></span>
						</div>
					</div>
					<?php } ?>

					
					<div class="col-md-12">
						<label for="opt" class="control-label"><span class="text-danger">*</span>Options-<span class="text-info">Options should be seperated with sign</span> ~~ <span class="text-danger">For Ex.</span> <span class="text-success">Ambala~~New Delhi~~Mohali~~Patna</span> OR <span class="text-info">True~~False~~Not Given</span> OR Yes~~No~~Not Given</label>
						<div class="form-group">
							<textarea name="opt" class="form-control" id="opt"><?php echo $options;?></textarea>
							<span class="text-danger"><?php echo form_error('opt');?></span>
						</div>
					</div>

					<div class="col-md-12">
						<label for="correct_answer_explaination" class="control-label">Correct Answer Explaination</label>
						<div class="form-group">
							<textarea name="correct_answer_explaination" class="form-control" id="correct_answer_explaination"><?php echo ($this->input->post('correct_answer') ? $this->input->post('correct_answer_explaination') : $question['correct_answer_explaination']); ?></textarea>
							<span class="text-danger"><?php echo form_error('correct_answer_explaination');?></span>
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group">
							<input type="checkbox" name="active" value="1" <?php echo ($question['active']==1 ? 'checked="checked"' : ''); ?> id='active' />
							<label for="active" class="control-label">Active</label>
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
            	<button type="submit" class="btn btn-success">
					<i class="fa fa-check"></i> Save
				</button>
	        </div>				
			<?php echo form_close(); ?>
		</div>
    </div>
</div>