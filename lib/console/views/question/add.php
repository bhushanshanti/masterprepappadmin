<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Add Question </h3>
            </div>
            <?php echo $this->session->flashdata('flsh_msg'); ?>
            <?php echo form_open('question/add'); ?>
          	<div class="box-body">
          		<div class="row clearfix">
					
					<div class="col-md-6">
						<label for="question_sets_id" class="control-label"><span class="text-danger">*</span>Question Set</label>
						<div class="form-group">
							<select name="question_sets_id" class="form-control">
								<option value="">Select question set</option>
								<?php 
								foreach($all_question_sets as $question_set)
								{
									$selected = ($question_set['question_sets_id'] == $this->input->post('question_sets_id')) ? ' selected="selected"' : "";

									echo '<option value="'.$question_set['question_sets_id'].'" '.$selected.'>'.$question_set['question_sets_heading'].'</option>';
								} 
								?>
							</select>
							<span class="text-danger"><?php echo form_error('question_sets_id');?></span>
						</div>
					</div>
					<div class="col-md-12">
						<label for="question" class="control-label"><span class="text-danger">*</span>Question</label>
						<div class="form-group">
							<input type="text" name="question" value="<?php echo $this->input->post('question'); ?>" class="form-control myckeditor" id="question" />
							<span class="text-danger"><?php echo form_error('question');?></span>
						</div>
					</div>
					<div class="col-md-12">
						<label for="correct_answer" class="control-label"><span class="text-danger">*</span>Correct Answer</label>
						<div class="form-group">								
							<textarea name="correct_answer" class="form-control" id="correct_answer"><?php echo ($this->input->post('correct_answer')); ?></textarea>
							<span class="text-danger"><?php echo form_error('correct_answer');?></span>
						</div>
					</div>
				</div>
				<div class="col-md-6">
						<div class="form-group">
							<input type="checkbox" name="active" value="1"  id="active" checked="checked"/>
							<label for="active" class="control-label">Active</label>
						</div>
					</div>
			</div>
          	<div class="box-footer">
            	<button type="submit" class="btn btn-success">
            		<i class="fa fa-check"></i> Save
            	</button>
          	</div>
            <?php echo form_close(); ?>
      	</div>
    </div>
</div>