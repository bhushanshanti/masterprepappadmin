<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Section Add</h3>
            </div>
            <?php echo $this->session->flashdata('flsh_msg'); ?>
            <?php echo form_open('section/add'); ?>
          	<div class="box-body">
          		<div class="row clearfix">
					
					<div class="col-md-6">
						<label for="section_heading" class="control-label"><span class="text-danger">*</span>Section Heading</label>
						<div class="form-group">
							<input type="text" name="section_heading" value="<?php echo $this->input->post('section_heading'); ?>" class="form-control" id="section_heading" />
							<span class="text-danger"><?php echo form_error('section_heading');?></span>
						</div>
					</div>
					<div class="col-md-6">
						<label for="section_desc" class="control-label"><span class="text-danger">*</span>Section Description</label>
						<div class="form-group">
							<textarea name="section_desc" class="form-control" id="section_desc"><?php echo $this->input->post('section_desc'); ?></textarea>
							<span class="text-danger"><?php echo form_error('section_desc');?></span>
						</div>
					</div>
					<div class="col-md-12">
						<label for="section_para" class="control-label"><span class="text-danger">*</span>Section Paragraph</label>
						<div class="form-group">
							<textarea name="section_para" class="form-control" id="section_para"><?php echo $this->input->post('section_para'); ?></textarea>
							<span class="text-danger"><?php echo form_error('section_para');?></span>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<input type="checkbox" name="active" value="1"  id="active" checked="checked"/>
							<label for="active" class="control-label">Active</label>
						</div>
					</div>
				</div>
			</div>
          	<div class="box-footer">
            	<button type="submit" class="btn btn-success">
            		<i class="fa fa-check"></i> Save
            	</button>
          	</div>
            <?php echo form_close(); ?>
      	</div>
    </div>
</div>