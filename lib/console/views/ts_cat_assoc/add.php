<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Associate Test to Category</h3>              	
            </div>
            <?php echo $this->session->flashdata('flsh_msg'); ?>
            <?php echo form_open_multipart('ts_cat_assoc/add'); ?>
          	<div class="box-body">
          		<div class="row clearfix">
					
					<div class="col-md-4">
						<label for="test_seriese_id" class="control-label"><span class="text-danger">*</span>Test Series</label>
						<div class="form-group">
							<select name="test_seriese_id" class="form-control selectpicker" data-show-subtext="true" data-live-search="true" onchange="get_category_list(this.value);">
								<option data-subtext="" value="">Select test</option>
								<?php 
								foreach($all_test_seriese_for_assoc as $test_seriese)
								{									
									$selected = ($test_seriese['test_seriese_id'] == $this->input->post('test_seriese_id')) ? ' selected="selected"' : "";
									$subtext = $test_seriese['programe_name'].'-'.$test_seriese['test_seriese_type'];
									echo '<option data-subtext="'.$subtext.'" value="'.$test_seriese['test_seriese_id'].'" '.$selected.'>'.$test_seriese['test_seriese_id'].' | '.$test_seriese['test_seriese_name'].'</option>';
								}
								?>
							</select>
							<span class="text-danger"><?php echo form_error('test_seriese_id');?></span>
						</div>
					</div>

					<div class="col-md-4">
						<label for="category_id" class="control-label"><span class="text-danger">*</span>Category </label>
						<div class="form-group" id="state_dd">	
							<select name="category_id" id="category_id" class="form-control selectpicker" data-show-subtext="true" data-live-search="true" onchange="get_category(this.value);">
								<option data-subtext="" value="">Select category</option>
							</select>						
							<span class="text-danger"><?php echo form_error('category_id');?></span>
						</div>
					</div>	

					<div class="col-md-4">
						<label for="paper_duration" class="control-label"><span class="text-danger">*</span>Paper duration</label>
						<div class="form-group">
							<select name="paper_duration" class="form-control selectpicker" data-show-subtext="true" data-live-search="true">
								<option value="">Select duration</option>
								<?php 
								$paper_duration_values = array(
									
									'5'=>'5 Minutes',
									'10'=>'10 Minutes',
									'15'=>'15 Minutes',
									'20'=>'20 Minutes',
									'25'=>'25 Minutes',
									'30'=>'30 Minutes',									
									'35'=>'35 Minutes',
									'40'=>'40 Minutes',
									'45'=>'45 Minutes',
									'50'=>'50 Minutes',
									'55'=>'55 Minutes',
									'60'=>'60 Minutes',
								);

								foreach($paper_duration_values as $value => $display_text)
								{
									$selected = ($value == $this->input->post('paper_duration')) ? ' selected="selected"' : "";

									echo '<option value="'.$value.'" '.$selected.'>'.$display_text.'</option>';
								} 
								?>
							</select>
							<span class="text-danger"><?php echo form_error('paper_duration');?></span>
						</div>
					</div>

					<div class="col-md-4">
						<label for="max_marks" class="control-label">Max. marks</label>
						<div class="form-group">
							<input type="text" name="max_marks" value="<?php echo $this->input->post('max_marks'); ?>" class="form-control" id="max_marks" maxlength="3" placeholder='eg. 40' />
							<span class="text-danger" id="max_marks_err"></span>
							<span class="text-danger"><?php echo form_error('max_marks');?></span>
						</div>						
					</div>

					<div class="col-md-4">
						<label for="totalquest" class="control-label"><span class="text-danger">*</span>Total question in paper</label>
						<div class="form-group">
							<input type="text" name="totalquest" value="<?php echo $this->input->post('totalquest'); ?>" class="form-control" id="totalquest" maxlength="2" placeholder='eg. 15'/>
							<span class="text-danger"><?php echo form_error('totalquest');?></span>
						</div>						
					</div>

					<div class="col-md-4">
						<label for="icon" class="control-label">Icon URL</label>
						[
						<span class="text-danger"><a href="<?php echo site_url('gallery/add');?>" target="_blank">
							<?php echo GALLERY_URL_LABEL;?></a>
						</span>
						]&nbsp;
						[
						<span class="text-danger"><a href="<?php echo site_url('gallery/index');?>" target="_blank">
							<?php echo GALLERY_URL_LABEL_LIST;?></a>
						</span>
						]
						<div class="form-group has-feedback">
							<input type="url" name="icon" value="<?php echo $this->input->post('icon'); ?>" class="form-control" id="icon" placeholder='Copy link from gallery & paste here'/>
							<span class="glyphicon glyphicon-link form-control-feedback"></span>
							<span class="text-danger"><?php echo form_error('icon');?></span>
						</div>
					</div>

					<div class="col-md-4">
						<label for="audio_file" class="control-label">Audio(For Listening)</label>
						<?php echo TS_CAT_ASSOC_ALLOWED_TYPES_LABEL;?>
						<div class="form-group">
							<input type='file' class='form-group' id='audio_file' name='audio_file' disabled="disabled">
							<span class="text-danger"><?php echo form_error('audio_file');?></span>
						</div>
			 		</div>

					<div class="col-md-4">
						<label for="audio_time" class="control-label">Audio duration(For Listening)</label>
						<div class="form-group has-feedback">
							<input type="text" name="audio_time" value="<?php echo $this->input->post('audio_time'); ?>" class="form-control" id="audio_time" maxlength="6" disabled="disabled" placeholder="mm:ss eg. 05:25"/>
							<span class="glyphicon glyphicon-time form-control-feedback"></span>
						</div>
						<span class="text-danger"><?php echo form_error('audio_time');?></span>
					</div>

					
				</div>
			</div>
          	<div class="box-footer">
            	<button type="submit" class="btn btn-success">
            		<i class="fa fa-check"></i> Save
            	</button> 
          	</div>
            <?php echo form_close(); ?>
      	</div>
    </div>
</div>