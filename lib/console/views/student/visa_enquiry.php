<input class="form-control" type="text" placeholder="Search" aria-label="Search" id="myInput">
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header bg-danger">
                <h3 class="box-title text-primary"><?php echo $title;?></h3>
            	<div class="box-tools">
                   <!--  <a href="<?php echo site_url('student/add'); ?>" class="btn btn-success btn-sm">Add</a> --> 
                </div>
                <?php echo $this->session->flashdata('flsh_msg');?>
            </div>
            <div class="box-body table-responsive">
                <table class="table table-striped table-bordered table-sm">
                    <thead>
                    <tr>
						<th><?php echo SR;?></th>                        
						<th>Name</th>
						<th>Email Id</th>						
						<th>Contact no.</th>
						<th>VISA type</th>	
						<th>Country</th>             
                        <th>Created</th>
                        <th><?php echo ACTION;?></th>
                    </tr>
                    </thead>
                    <tbody id="myTable">
                    <?php $sr=0;foreach($visa_enquiry as $s){$zero=0;$one=1;$pk='visa_enquiry_id'; $table='students_visa_enquiry';$sr++; ?>
                    <tr>
						<td><?php echo $sr; ?></td>                        
						<td><?php echo $s['name']; ?></td>						
						<td><a href="mailto:<?php echo $s['email'];?>"><?php echo $s['email']; ?></a></td>
                        <td><?php echo $s['mobile']; ?></td>	
                        <td><?php echo $s['VISA_type']; ?></td>					
						<td><?php echo $s['country_intrested']; ?></td>
						<td>
                            <?php 
                            $date=date_create($s['created']);
                            echo $created = date_format($date,"M d, Y");
                            ?>                                
                        </td>					
						 <?php  if($s['isReplied']==0){ ?> 
						<td> 
                            <a href="<?php echo site_url('student/reply_to_student_visa_enquiry/'.$s['visa_enquiry_id']); ?>" class="btn btn-info btn-xs" data-toggle="tooltip" title="Reply">Reply</a>
                            
                        </td>
                         <?php }else{ ?>

                            <td> 
                            <a href="<?php echo site_url('student/reply_to_student_visa_enquiry/'.$s['visa_enquiry_id']); ?>" class="btn btn-warning btn-xs" data-toggle="tooltip" title="Reply">Reply again</a>
                            
                        </td>

                        <?php } ?>
                    </tr>
                    <?php } ?>
                </tbody>
                </table>
                <div class="pull-right">
                    <?php echo $this->pagination->create_links(); ?>                    
                </div>                
            </div>
        </div>
    </div>
</div>