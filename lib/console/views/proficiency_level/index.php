<input class="form-control" type="text" placeholder="Search" aria-label="Search" pro_id="myInput">
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header bg-danger">
                <h3 class="box-title text-primary"><?php echo $title;?></h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('proficiency_level/add'); ?>" class="btn btn-success btn-sm">Add</a> 
                </div>
                <?php echo $this->session->flashdata('flsh_msg'); ?>
            </div>
            <div class="box-body table-responsive">
                <table class="table table-striped table-bordered table-sm">
                    <thead>
                    <tr>
                        <th><?php echo SR;?></th>				
						<th>Pro Level Image</th>
                        <th><?php echo STATUS;?></th>
                        <th><?php echo ACTION;?></th>
                    </tr>
                    </thead>
                    <tbody pro_id="myTable">
                    <?php $sr=0;foreach($proficiency_level as $r){$zero=0;$one=1;$pk='pro_id'; $table='proficiency_level';$sr++; ?>
                    <tr>
						<td><?php echo $sr; ?></td>					 
						<td>
                            <?php 
                            echo '<img src="'.$r['pro_name'].'" style="width:180px;">';
                            ?>                                
                        </td>
                        <td>
                            <?php 
                            if($r['active']==1){
                                echo '<span class="text-success"><a href="javascript:vopro_id(0);" pro_id='.$r['pro_id'].' data-toggle="tooltip" title="Click to De-activate" onclick=activate_deactivete('.$r['pro_id'].','.$zero.',"'.$table.'","'.$pk.'") >'.ACTIVE.'</a></span>';
                            }else{
                                echo '<span class="text-danger"><a href="javascript:vopro_id(0);" pro_id='.$r['pro_id'].' title="Click to Activate" onclick=activate_deactivete('.$r['pro_id'].','.$one.',"'.$table.'","'.$pk.'") >'.DEACTIVE.'</a></span>';
                            }
                            ?>                                
                        </td>
						<td>
                            
                            <a href="<?php echo site_url('proficiency_level/edit/'.$r['pro_id']); ?>" class="btn btn-info btn-xs" data-toggle="tooltip" title="Edit"><span class="fa fa-pencil"></span> </a> 
                            <a href="<?php echo site_url('proficiency_level/remove/'.$r['pro_id']); ?>" class="btn btn-danger btn-xs" data-toggle="tooltip" title="Delete" onclick="return confirm('Are you sure you want to delete this item?');"><span class="fa fa-trash"></span> </a>


                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
                </table>
                <div class="pull-right">
                    <?php echo $this->pagination->create_links(); ?>                    
                </div>                
            </div>
        </div>
    </div>
</div>
