<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Email-MasterPrep</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="margin: 0; padding: 0;">
    <table border="0" cellpadding="0" cellspacing="0" width="100%"> 
        <tr>
            <td style="padding: 10px 10px 30px 10px;">
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="border: 1px solid #cccccc; border-collapse: collapse;">
                    <tr>
                        <td align="left" style="padding: 40px 0 30px 30px;">
                            <img src="<?php echo site_url('resources/img/mp-logo.jpg');?>" alt="MasterPrep Logo" style="display: block;" />
                        </td>
						<td align="right" style="vertical-align: top;">
                            <img src="<?php echo site_url('resources/img/header.png');?>" height="200" alt="MasterPrep Logo Theme" style="display: block;width:100%; max-width: 200px" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding: 40px 30px 20px 30px;">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td align="left" style="color: #ed3237; text-transform:uppercase; font-family: Arial, sans-serif; font-size: 16px;">
                                        <b>Dear <?php echo $name;?>,</b>
                                    </td>
                                </tr>								
                                <tr>
                                    <td align="left" style="padding: 20px 0 0px 0; color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">
                                        <?php echo $email_message.'<br/><br/>'; ?>
										USERNAME: <?php echo $username.'<br/>'; ?>
										PASSWORD: <?php echo $password.'<br/><br/>'; ?>

										<?php echo $thanks.'<br/>'; ?>
										<?php echo $team; ?>
                                    </td>
                                </tr>
                                
                                <tr>
                                	<td align="center">
                                    	<a href="<?php echo site_url('');?>" target="_blank" 
                                    		style="-webkit-text-size-adjust: none; text-decoration: none; display: inline-block; 
                                    		color: #ffffff; background-color: #ed3237; border-radius: 15px; -webkit-border-radius: 15px; 
                                    		-moz-border-radius: 15px; width: auto; width: auto; border-top: 1px solid #ed3237; border-right: 1px solid #ed3237; 
                                    		border-bottom: 1px solid #ed3237; border-left: 1px solid #ed3237; padding-top: 10px; padding-bottom: 10px; 
                                    		font-family: 'Lato', Tahoma, Verdana, Segoe, sans-serif; text-align: center; mso-border-alt: none; word-break: keep-all;" 
                                    		target="_blank"><span style="padding-left:40px;padding-right:40px;font-size:16px;display:inline-block;">
    		                                <span style="font-size: 16px; line-height: 2; mso-line-height-alt: 32px;"><strong>Go to Login</strong></span>
                                            </span>
                                        </a>
                                	</td>
                                </tr>
                                
                            </table>
                        </td>
                    </tr>
					<?php include('email-footer.php');?>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>