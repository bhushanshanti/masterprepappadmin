<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title"><?php echo $title;?></h3>
            </div>
            <?php echo $this->session->flashdata('flsh_msg');?>
            <?php echo form_open('Real_test_dates/add'); ?>
          	<div class="box-body">
          		<div class="row clearfix">
					<div class="col-md-3">
						<label for="testDate" class="control-label"><span class="text-danger">*</span>Date</label>
						<div class="form-group">
							<input type="text" name="testDate" value="<?php echo $this->input->post('testDate'); ?>" class="has-datepicker form-control" id="testDate" maxlength="10"/>
							<span class="glyphicon glyphicon-arrow-down form-control-feedback"></span>
							<span class="text-danger"><?php echo form_error('testDate');?></span>
							<span class="text-danger"><?php echo $this->session->flashdata('flsh_msg'); ?></span>
						</div>
					</div>					 

					<div class="col-md-3">
						<label for="programe_id" class="control-label"><span class="text-danger">*</span>Program</label>
						<div class="form-group">
							<select name="programe_id" class="form-control selectpicker" data-show-subtext="true" data-live-search="true">
								<option data-subtext="" value="">Select Program</option>
								<?php 
								foreach($all_programe_masters as $p)
								{
									$selected = ($p['programe_id'] == $this->input->post('programe_id')) ? ' selected="selected"' : "";

									echo '<option data-subtext="'.$p['programe_name'].'" value="'.$p['programe_id'].'" '.$selected.'>'.$p['programe_name'].'</option>';
								} 
								?>
							</select>
							<span class="text-danger"><?php echo form_error('programe_id');?></span>
						</div>
					</div>				

					<div class="col-md-3">
						<label for="test_module_id" class="control-label"><span class="text-danger">*</span>Test module</label>
						<div class="form-group">
							<select name="test_module_id" class="form-control selectpicker" data-show-subtext="true" data-live-search="true">
								<option data-subtext="" value="">Select</option>
								<?php 
								foreach($all_test_module as $p)
								{
									$selected = ($p['test_module_id'] == $this->input->post('test_module_id')) ? ' selected="selected"' : "";

									echo '<option data-subtext="'.$t['test_module_name'].'" value="'.$p['test_module_id'].'" >'.$p['test_module_name'].'</option>';
								} 
								?>
							</select>
							<span class="text-danger"><?php echo form_error('test_module_id');?></span>
						</div>
					</div>					

					<div class="col-md-3">
						<div class="form-group">
							<input type="checkbox" name="active" value="1"  id="active" checked="checked"/>
							<label for="active" class="control-label">Active</label>
						</div>
					</div>

				</div>
			</div>       

   			<div class="box-footer">   				
            	<button type="submit" class="btn btn-success">
            		<i class="fa fa-check"></i> Save
            	</button>            	
          	</div>

            <?php echo form_close(); ?>
      	</div>
    </div>
</div>



  
    

    
  