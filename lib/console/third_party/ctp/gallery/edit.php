<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Edit Gallery</h3>
            </div>
            <?php echo $this->session->flashdata('flsh_msg');?>
			<?php echo form_open_multipart('gallery/edit/'.$gallery['id']); ?>
			<div class="box-body">
				<div class="row clearfix">
					
					<div class="col-md-6">
						<label for="image" class="control-label"><span class="text-danger">*</span>Image<?php echo SEP;?>Icon<?php echo SEP;?>Video<?php echo SEP;?>File<?php echo SEP;?>Audio</label><?php echo GALLERY_ALLOWED_TYPES_LABEL;?>
						<div class="form-group">
							<input type="file" name="image" value="<?php echo ($this->input->post('image') ? $this->input->post('image') : $gallery['image']); ?>" class="form-control" id="image" />
							<span>
								<?php 
								if(isset($gallery['image'])){      
                                    echo '<span>
                                            <a href="'.site_url(GALLERY_IMAGE_PATH.$gallery['image']).'" target="_blank">'.$gallery['image'].'</a>
                                        </span>';
                                }else{
                                    echo NO_FILE;
                                }
                                ?>
						</span>	
							<span class="text-danger"><?php echo form_error('image');?></span>
						</div>						
					</div>

					<div class="col-md-6">
			            <label for="image" class="control-label"><span class="text-danger">*</span>Title</label>
			            <div class="form-group">
			              <input type="text" name="title" value="<?php echo ($this->input->post('title') ? $this->input->post('title') : $gallery['title']); ?>" class="form-control" id="title" required="required" maxlength="50"/>
			              <span class="text-danger"><?php echo form_error('title');?></span>
			            </div>
			         </div>

					<div class="col-md-6">
						<div class="form-group">
							<input type="checkbox" name="active" value="1" <?php echo ($gallery['active']==1 ? 'checked="checked"' : ''); ?> id='active' />
							<label for="active" class="control-label">Active</label>
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
            	<button type="submit" class="btn btn-success">
					<i class="fa fa-check"></i> Save
				</button>
	        </div>				
			<?php echo form_close(); ?>
		</div>
    </div>
</div>