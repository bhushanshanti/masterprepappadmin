<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Edit Package</h3>
            </div>
            <?php echo $this->session->flashdata('flsh_msg'); ?>
			<?php echo form_open('package_master/edit/'.$package_master['package_id']); ?>
			<div class="box-body">
				<div class="row clearfix">
					
					<div class="col-md-4">
						<label for="package_name" class="control-label"><span class="text-danger">*</span>Package Name</label>
						<div class="form-group">
							<input type="text" name="package_name" value="<?php echo ($this->input->post('package_name') ? $this->input->post('package_name') : $package_master['package_name']); ?>" class="form-control" id="package_name" />
							<span class="text-danger"><?php echo form_error('package_name');?></span>
						</div>
					</div>

					<div class="col-md-3">
						<label for="package_color" class="control-label">Color</label>
						<div class="form-group">
							<input type="color" name="package_color" value="<?php echo ($this->input->post('package_color') ? $this->input->post('package_color') : $package_master['package_color']); ?>" class="form-control">
							<span class="text-danger"><?php echo form_error('package_color');?></span>
						</div>
					</div>

					<div class="col-md-4">
						<label for="amount" class="control-label"><span class="text-danger">*</span>Amount</label>
						<div class="form-group has-feedback">
							<input type="text" name="amount" value="<?php echo ($this->input->post('amount') ? $this->input->post('amount') : $package_master['amount']); ?>" class="form-control" id="amount" />
							<span class="glyphicon glyphicon-usd form-control-feedback"></span>
							<span class="text-danger"><?php echo form_error('amount');?></span>
						</div>
					</div>
					<div class="col-md-4">
						<label for="duration" class="control-label"><span class="text-danger">*</span>Duration (in days)</label>
						<div class="form-group has-feedback">
							<input type="text" name="duration" value="<?php echo ($this->input->post('duration') ? $this->input->post('duration') : $package_master['duration']); ?>" class="form-control" id="duration" />
							<span class="glyphicon glyphicon-time form-control-feedback"></span>
							<span class="text-danger"><?php echo form_error('duration');?></span>
						</div>
					</div>

					<div class="col-md-4">
						<label for="programe_id" class="control-label"><span class="text-danger">*</span>Programme</label>
						<div class="form-group">
							<select name="programe_id" class="form-control selectpicker" data-show-subtext="true" data-live-search="true">
								<option data-subtext="" value="">Select programme</option>
								<?php 
								foreach($all_programe_masters as $p)
								{
									$selected = ($p['programe_id'] == $package_master['programe_id']) ? ' selected="selected"' : "";

									echo '<option data-subtext="'.$p['programe_name'].'" value="'.$p['programe_id'].'" '.$selected.'>'.$p['programe_name'].'</option>';
								} 
								?>
							</select>
							<span class="text-danger"><?php echo form_error('programe_id');?></span>
						</div>
					</div>
					
					<div class="col-md-4">
						<label for="test_module_id" class="control-label"><span class="text-danger">*</span>Test module</label>
						<div class="form-group">
							<select name="test_module_id" class="form-control selectpicker" data-show-subtext="true" data-live-search="true">
								<option data-subtext="" value="">Select </option>
								<?php 
								foreach($all_test_module as $t)
								{
									$selected = ($t['test_module_id'] == $package_master['test_module_id']) ? ' selected="selected"' : "";

									echo '<option data-subtext="'.$t['test_module_name'].'" value="'.$t['test_module_id'].'" '.$selected.'>'.$t['test_module_name'].'</option>';
								} 
								?>
							</select>
							<span class="text-danger"><?php echo form_error('test_module_id');?></span>
						</div>
					</div>	
					<div class="col-md-4">
						<label for="test_paper_limit" class="control-label">Test Paper Limit</label>
						<div class="form-group has-feedback">
							<input type="text" name="test_paper_limit" value="<?php echo ($this->input->post('test_paper_limit') ? $this->input->post('test_paper_limit') : $package_master['test_paper_limit']); ?>" class="form-control" id="test_paper_limit" />
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label for="active" class="control-label">Active</label>
							<input type="checkbox" name="active" value="1" <?php echo ($package_master['active']==1 ? 'checked="checked"' : ''); ?> id='active' />	
						</div>
					</div>
					<div class="col-md-12">
						<label for="package_desc" class="control-label">Package Description</label>
						<div class="form-group has-feedback">
							<textarea name="package_desc" class="form-control myckeditor" id="package_desc"><?php echo ($this->input->post('package_desc') ? $this->input->post('package_desc') : $package_master['package_desc']); ?></textarea>
							<span class="glyphicon glyphicon-text-size form-control-feedback"></span>
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
            	<button type="submit" class="btn btn-success">
					<i class="fa fa-check"></i> Save
				</button>
	        </div>				
			<?php echo form_close(); ?>
		</div>
    </div>
</div>