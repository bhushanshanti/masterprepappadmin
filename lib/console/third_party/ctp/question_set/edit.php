<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Edit Question Set</h3>
            </div>
            <?php echo $this->session->flashdata('flsh_msg'); ?>
			<?php echo form_open_multipart('question_set/edit/'.$question_set['question_sets_id']); ?>
			<div class="box-body">
				<div class="row clearfix">
					
					
					<div class="col-md-4">
						<label for="question_sets_heading" class="control-label"><span class="text-danger">*</span>Question Sets Heading</label>
						<div class="form-group">
							<input type="text" name="question_sets_heading" value="<?php echo ($this->input->post('question_sets_heading') ? $this->input->post('question_sets_heading') : $question_set['question_sets_heading']); ?>" class="form-control" id="question_sets_heading" />
							<span class="text-danger"><?php echo form_error('question_sets_heading');?></span>
						</div>
					</div>
					<div class="col-md-4">
						<label for="question_type_id" class="control-label"><span class="text-danger">*</span>Question Type</label>
						<div class="form-group">
							<select name="question_type_id" class="form-control selectpicker" data-show-subtext="true" data-live-search="true" onchange="get_qb(this.value)">
								<option data-subtext="" value="">Select Question Type</option>
								<?php 
								foreach($all_question_types as $question_type)
								{
									$selected = ($question_type['question_type_id'] == $question_set['question_type_id']) ? ' selected="selected"' : "";

									echo '<option data-subtext="'.$question_type['behavior_name'].'" value="'.$question_type['question_type_id'].'" '.$selected.'>'.$question_type['type_name'].'</option>';
								} 
								?>
							</select>
							<span class="text-danger"><?php echo form_error('question_type_id');?></span>
						</div>
						<input type="hidden" name="qb" id="qb">
					</div>

					
					<div class="col-md-4">
                        <label for="question_sets_image" class="control-label"> Set Image</label> <span class="text-danger"><?php echo QUESTTION_SET_ALLOWED_TYPES_LABEL;?></span>
                        <div class="form-group">
                        <input type="file" name="question_sets_image" class="form-control"/>
                            <span id="question_sets_image_msg"></span> 
                             <span>
                             	
                             	<?php 
								if(isset($question_set['question_sets_image'])){      
                                    echo '<span>
                                            <a href="'.$question_set['question_sets_image'].'" target="_blank">'.OPEN_FILE.'</a>
                                        </span>';
                                }else{
                                    echo NO_FILE;
                                }
                                ?>
                             </span> 
                        </div>                                            
                    </div>

					<div class="col-md-4">
						<label for="total_questions" class="control-label"><span class="text-danger">*</span>Total No Of Question in set</label>
						<div class="form-group">
							<input type="text" name="total_questions" value="<?php echo ($this->input->post('total_questions') ? $this->input->post('total_questions') : $question_set['total_questions']); ?>" class="form-control" id="total_questions" maxlength="2" />
							<span class="text-danger"><?php echo form_error('total_questions');?></span>
						</div>
					</div>

					<div class="col-md-4">
						<label for="display_no_of_questions" class="control-label">No Of Questions to Display</label>
						<div class="form-group">
							<input type="text" name="display_no_of_questions" value="<?php echo ($this->input->post('display_no_of_questions') ? $this->input->post('display_no_of_questions') : $question_set['display_no_of_questions']); ?>" class="form-control" id="display_no_of_questions" maxlength="2"/>
							<span class="text-danger"><?php echo form_error('display_no_of_questions');?></span>
						</div>
					</div>

					<div class="col-md-4">
                        <label for="answer_audio_recrod_duration" class="control-label"><span class="text-danger"></span>Answer Audio Duration</label>
                        <div class="form-group">
                         <input type="text" id="answer_audio_recrod_duration" name="answer_audio_recrod_duration" class="form-control" maxlength="2" placeholder="Time in Minutes/per question" value="<?php echo ($this->input->post('answer_audio_recrod_duration') ? $this->input->post('answer_audio_recrod_duration') : $question_set['answer_audio_recrod_duration']); ?>"/>
                         <span class="text-danger" id="answer_audio_recrod_duration_err"></span>
                         </div>
                    </div>

					
					
					<div class="col-md-12">
						<label for="question_sets_desc" class="control-label">Question Sets Description</label>
						<div class="form-group">
							<textarea name="question_sets_desc" class="form-control" id="question_sets_desc"><?php echo ($this->input->post('question_sets_desc') ? $this->input->post('question_sets_desc') : $question_set['question_sets_desc']); ?></textarea>
							<span class="text-danger"><?php echo form_error('question_sets_desc');?></span>
						</div>
					</div>


					<div class="col-md-12">
						<label for="question_sets_para" class="control-label">Question Sets Paragraph</label>
						<?php echo QS_PARA_NOTE;?>
						<div class="form-group">
							<textarea name="question_sets_para" class="form-control myckeditor" id="question_sets_para"><?php echo ($this->input->post('question_sets_para') ? $this->input->post('question_sets_para') : $question_set['question_sets_para']); ?></textarea>
							<span class="text-danger"><?php echo form_error('question_sets_para');?></span>
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="form-group">
							<input type="checkbox" name="active" value="1" <?php echo ($question_set['active']==1 ? 'checked="checked"' : ''); ?> id='active' />
							<label for="active" class="control-label">Active</label>
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
            	<button type="submit" class="btn btn-success">
					<i class="fa fa-check"></i> Save
				</button>
	        </div>				
			<?php echo form_close(); ?>
		</div>
    </div>
</div>

<script type="text/javascript">
	

	function get_qb(question_type_id){

    //alert(question_type_id);

    $.ajax({
        url: "<?php echo site_url('Question_type/get_qb_id');?>",
        async : true,
        type: 'post',
        data: {question_type_id: question_type_id},
        dataType: 'json',
        success: function(data){
            //alert(data);
            $('#qb_id').val(data)
        }
    });
}
</script>