<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Question sets listing</h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('question_set/add'); ?>" class="btn btn-success btn-sm">Add</a> 
                </div>
                <?php echo $this->session->flashdata('flsh_msg'); ?>
            </div>
            <div class="box-body table-responsive">
                <table class="table table-striped table-bordered table-sm">
                    <tr>
						<th><?php echo SR;?></th>
                        <th>Id</th>						
						<th>Section Id</th>
						<th>Type Id</th>
						<th>Display No Of Questions</th>
						<th>Sets Heading</th>
						<th>Sets Desc</th>
						<th><?php echo STATUS;?></th>
                        <th><?php echo ACTION;?></th>
                    </tr>
                    <?php $sr=0;foreach($question_sets as $q){$sr++; ?>
                    <tr>
						<td><?php echo $sr; ?></td>
                        <td><?php echo $q['question_sets_id']; ?></td>						
						<td><?php echo $q['section_id']; ?></td>
						<td><?php echo $q['question_type_id']; ?></td>
						<td><?php echo $q['display_no_of_questions']; ?></td>
						<td><?php echo $q['question_sets_heading']; ?></td>
						<td><?php echo $q['question_sets_desc']; ?></td>
                        <td>
                            <?php 
                            if($q['active']==1){
                                echo '<span class="text-success"><a href="#" data-toggle="tooltip" title="Click to De-activate" onclick="activate_deactivete('.$q['question_sets_id'].','.$q['active'].');">'.ACTIVE.'</a></span>';
                            }else{
                                echo '<span class="text-danger"><a href="#" title="Click to Activate" onclick="activate_deactivete('.$q['question_sets_id'].','.$q['active'].');">'.DEACTIVE.'</a></span>';
                            }
                            ?>                                
                        </td>
						<td>
                            <a href="<?php echo site_url('question_set/edit/'.$q['question_sets_id']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Edit</a> 
                            <a href="<?php echo site_url('question_set/remove/'.$q['question_sets_id']); ?>" class="btn btn-danger btn-xs" onclick="return confirm('Are you sure you want to delete this item?');"><span class="fa fa-trash"></span> Delete</a>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
                <div class="pull-right">
                    <?php echo $this->pagination->create_links(); ?>                    
                </div>                
            </div>
        </div>
    </div>
</div>
