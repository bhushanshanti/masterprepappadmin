<input class="form-control" type="text" placeholder="Search" aria-label="Search" id="myInput">
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header bg-danger">
                <h3 class="box-title text-primary"><?php echo $title;?></h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('instruction_master/add'); ?>" class="btn btn-success btn-sm">Add</a> 
                </div>
                <?php echo $this->session->flashdata('flsh_msg'); ?>
            </div>
            <div class="box-body table-responsive">
                <table class="table table-striped table-bordered table-sm">
                   <thead>
                    <tr>
						<th><?php echo SR;?></th>
                        <th>Module</th>
                        <th style="width:155px">Programme<?php echo SEP;?>Category</th>
						<th>Content</th>
                       <th><?php echo STATUS;?></th>
                        <th><?php echo ACTION;?></th>
                    </tr>
                    </thead>
                    <tbody id="myTable">
                    <?php $sr=0; foreach($instruction_masters as $i){ $zero=0;$one=1;$pk='id'; $table='instruction_masters';$sr++; ?>
                    <tr>						
						<td><?php echo $sr; ?></td>
                        <td><?php echo $i['test_module_name']; ?></td>
						<td><?php echo $i['programe_name'].SEP.$i['category_name']; ?></td>
						<td><?php echo $i['content']; ?></td>
                        <td>
                            <?php 
                            if($i['active']==1){
                                echo '<span class="text-success"><a href="javascript:void(0);" id='.$i['id'].' data-toggle="tooltip" title="Click to De-activate" onclick=activate_deactivete('.$i['id'].','.$zero.',"'.$table.'","'.$pk.'") >'.ACTIVE.'</a></span>';
                            }else{
                                echo '<span class="text-danger"><a href="javascript:void(0);" id='.$i['id'].' title="Click to Activate" onclick=activate_deactivete('.$i['id'].','.$one.',"'.$table.'","'.$pk.'") >'.DEACTIVE.'</a></span>';
                            }
                            ?>                                
                        </td> 
						<td>
                            <a href="<?php echo site_url('instruction_master/edit/'.$i['id']); ?>" class="btn btn-info btn-xs" data-toggle="tooltip" title="Edit"><span class="fa fa-pencil"></span> </a> 
                            <a href="<?php echo site_url('instruction_master/remove/'.$i['id']); ?>" class="btn btn-danger btn-xs" data-toggle="tooltip" title="Delete" onclick="return confirm('Are you sure you want to delete this item?');"><span class="fa fa-trash"></span> </a>
                        </td>
                    </tr>
                    <?php } ?>
                    </tbody>
                </table>
                <div class="pull-right">
                    <?php echo $this->pagination->create_links(); ?>                    
                </div>                
            </div>
        </div>
    </div>
</div>
