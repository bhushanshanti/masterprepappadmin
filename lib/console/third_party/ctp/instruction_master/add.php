<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Add Instruction </h3>
              	 <?php echo form_open('instruction_master/add'); ?>
            </div>
            <?php echo $this->session->flashdata('flsh_msg'); ?>
            <?php echo form_open('instruction_master/add'); ?>
          	<div class="box-body">
          		<div class="row clearfix">
					<span class="text-danger"><?php echo $this->session->flashdata('flsh_msg'); ?></span>
					<div class="col-md-6">
						<label for="category_id" class="control-label"><span class="text-danger">*</span>Category</label>
						<div class="form-group">
							<select name="category_id" class="form-control selectpicker" data-show-subtext="true" data-live-search="true">
								<option value="">Select category</option>
								<?php 
								foreach($all_category_masters as $category_master)
								{
									$selected = ($category_master['category_id'] == $this->input->post('category_id')) ? ' selected="selected"' : "";

									echo '<option value="'.$category_master['category_id'].'" '.$selected.'>'.$category_master['programe_name'].' | '.$category_master['category_name'].'</option>';
								} 
								?>
							</select>
							<span class="text-danger"><?php echo form_error('category_id');?></span>
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group">
							<input type="checkbox" name="active" value="1"  id="active" checked="checked"/>
							<label for="active" class="control-label">Active</label>
						</div>
					</div>

					<div class="col-md-12">
						<label for="content" class="control-label"><span class="text-danger">*</span>Content</label>
						<div class="form-group has-feedback">
							<textarea name="content" class="form-control myckeditor" id="content"><?php echo $this->input->post('content'); ?></textarea>
							<span class="glyphicon glyphicon-text-size form-control-feedback"></span>
							<span class="text-danger"><?php echo form_error('content');?></span>
						</div>
					</div>
					
				</div>
			</div>
          	<div class="box-footer">
            	<button type="submit" class="btn btn-success">
            		<i class="fa fa-check"></i> Save
            	</button>
          	</div>
            <?php echo form_close(); ?>
      	</div>
    </div>
</div>

<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
<!-- <script src="<?php echo site_url('resources/js/ckeditor.js');?>"></script> -->
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo site_url('resources/js/bootstrap3-wysihtml5.all.min.js');?>"></script>
<script type="text/javascript">    

    
    CKEDITOR.replace('content');
    $(".textarea").wysihtml5();

</script>