<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title></title>
</head>
<body style="background-color: #fff !important">	
	<input class="btn btn-info btn-xs" type="button" value="Print Contents" 
               onclick="printx()" data-toggle="tooltip" title="Print Contents"/>
    <div id='printid' style="background-color: lightgray;">        
	<?php
		foreach ($sp_all as $d) {
	?>	

	<p style="color: red; text-align: center;">
		<?php 
		echo $d['title'];
		
		?>
	</p>

	<p>
		<?php 
		
		echo $d['contents']; 
		?>
	</p>
	
	<hr>
	<?php } ?>
	</div>		
			
</body>
</html>

<script type="text/javascript">
	function printx(){
        
    var prtContent = document.getElementById("printid");
    var WinPrint = window.open('', '', 'left=0,top=0,width=800,height=900,toolbar=0,scrollbars=0,status=0');
    WinPrint.document.write(prtContent.innerHTML);
    WinPrint.document.close();
    WinPrint.focus();
    WinPrint.print();
    WinPrint.close();
}
</script>