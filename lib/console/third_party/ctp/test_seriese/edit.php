<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Edit <?php echo TS_EE;?></h3>
            </div>
            <?php echo $this->session->flashdata('flsh_msg');?>
			<?php echo form_open('test_seriese/edit/'.$test_seriese['test_seriese_id']); ?>
			<div class="box-body">
				<div class="row clearfix">
					
					
					<div class="col-md-3">
						<label for="test_seriese_name" class="control-label"><span class="text-danger">*</span><?php echo TS_EE;?> name</label>
						<div class="form-group">
							<input type="text" name="test_seriese_name" value="<?php echo ($this->input->post('test_seriese_name') ? $this->input->post('test_seriese_name') : $test_seriese['test_seriese_name']); ?>" class="form-control" id="test_seriese_name" maxlength="30" placeholder='eg. Test 1'/>
							<span class="text-danger"><?php echo form_error('test_seriese_name');?></span>
							<span class="text-danger"><?php echo $this->session->flashdata('flsh_msg'); ?></span>
						</div>
					</div>

					<div class="col-md-3">
						<label for="test_seriese_type" class="control-label"><span class="text-danger">*</span>Type</label>
						<div class="form-group">
							<select name="test_seriese_type" class="form-control selectpicker" data-show-subtext="true" data-live-search="true">
								<option data-subtext="" value="">Select </option>
								<?php 
								foreach($all_test_type as $t)
								{
									$selected = ($t['test_type_name'] == $test_seriese['test_seriese_type']) ? ' selected="selected"' : "";

									echo '<option data-subtext="'.$t['test_type_name'].'" value="'.$t['test_type_name'].'" '.$selected.'>'.$t['test_type_name'].'</option>';
								} 
								?>
							</select>
							<span class="text-danger"><?php echo form_error('test_type_name');?></span>
						</div>
					</div>

					<div class="col-md-3">
						<label for="programe_id" class="control-label"><span class="text-danger">*</span>Program</label>
						<div class="form-group">
							<select name="programe_id" class="form-control selectpicker" data-show-subtext="true" data-live-search="true">
								<option data-subtext="" value="">Select Program</option>
								<?php 
								foreach($all_programe_masters as $p)
								{
									$selected = ($p['programe_id'] == $test_seriese['programe_id']) ? ' selected="selected"' : "";

									echo '<option data-subtext="'.$p['programe_name'].'" value="'.$p['programe_id'].'" '.$selected.'>'.$p['programe_name'].'</option>';
								} 
								?>
							</select>
							<span class="text-danger"><?php echo form_error('programe_id');?></span>
						</div>
					</div>					

					<div class="col-md-3">
						<label for="test_seriese_color" class="control-label"><?php echo TS_EE;?> Group color</label>
						<div class="form-group">
							<input type="color" name="test_seriese_color" value="<?php echo ($this->input->post('test_seriese_color') ? $this->input->post('test_seriese_color') : $test_seriese['test_seriese_color']); ?>" class="form-control">
							<span class="text-danger"><?php echo form_error('test_seriese_color');?></span>
						</div>
					</div>					

					<div class="col-md-3">
						<label for="test_module_id" class="control-label"><span class="text-danger">*</span>Test module</label>
						<div class="form-group">
							<select name="test_module_id" class="form-control selectpicker" data-show-subtext="true" data-live-search="true">
								<option data-subtext="" value="">Select </option>
								<?php 
								foreach($all_test_module as $t)
								{
									$selected = ($t['test_module_id'] == $test_seriese['test_module_id']) ? ' selected="selected"' : "";

									echo '<option data-subtext="'.$t['test_module_name'].'" value="'.$t['test_module_id'].'" '.$selected.'>'.$t['test_module_name'].'</option>';
								} 
								?>
							</select>
							<span class="text-danger"><?php echo form_error('test_module_id');?></span>
						</div>
					</div>	

					<div class="col-md-3">
						<label for="is_MtEt" class="control-label"><span class="text-danger">*</span>Sub test</label>
						<div class="form-group">
							<select name="is_MtEt" class="form-control selectpicker" data-show-subtext="true" data-live-search="true">
								<?php 
									if($test_seriese['is_MtEt']==1){
								?>
								<option value="">Select</option>
								<option value="1" selected="selected">Evaluation Test</option>
								<option value="3">Static Test</option>
								
							
							<?php }elseif($test_seriese['is_MtEt']==3){ ?>
								<option value="">Select</option>
								<option value="1">Evaluation Test</option>
								<option value="3" selected="selected">Static Test</option>
							<?php }else{ ?>
								<option value="" selected="selected">Select</option>
								<option value="1">Evaluation Test</option>
								<option value="3" >Static Test</option>
							<?php } ?>
							</select>
							<span class="text-danger"><?php echo form_error('is_MtEt');?></span>
						</div>
					</div>

					<div class="col-md-3">
						<div class="form-group">
							<input type="checkbox" name="active" value="1" <?php echo ($test_seriese['active']==1 ? 'checked="checked"' : ''); ?> id='active' />
							<label for="active" class="control-label">Active</label>
						</div>
					</div>


					<div class="col-md-12">
						<label for="test_seriese_desc" class="control-label"><?php echo TS_EE;?> Description</label>
						<div class="form-group has-feedback">
							<textarea name="test_seriese_desc" class="form-control" id="test_seriese_desc"><?php echo ($this->input->post('test_seriese_desc') ? $this->input->post('test_seriese_desc') : $test_seriese['test_seriese_desc']); ?></textarea>
							<span class="glyphicon glyphicon-text-size form-control-feedback"></span>
						</div>
					</div>				
					

				</div>
			</div>
			<div class="box-footer">
				
            	<button type="submit" class="btn btn-success">
					<i class="fa fa-check"></i> Save
				</button>
	        </div>				
			<?php echo form_close(); ?>
		</div>
    </div>
</div>
