<input class="form-control" type="text" placeholder="Search" aria-label="Search" id="myInput">
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header bg-danger">
                <h3 class="box-title text-primary"><?php echo $title;?></h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('ts_cat_assoc/add'); ?>" class="btn btn-success btn-sm">Add</a> 
                </div>
                <?php echo $this->session->flashdata('flsh_msg'); ?>
            </div>
            <div class="box-body table-responsive">
                <table class="table table-striped table-bordered table-sm">
                    <thead>
                    <tr>
						<th><?php echo SR;?></th>
                        <!-- <th>Module</th> -->
                        <th>Test Name<?php echo SEP;?>Type</th>
                        <th>Program<?php echo SEP;?>Category</th> 
                        <th title="Paper duration">Duration</th>
                        <th title="Maximum marks of the paper">M.M.</th>
                        <th title="Total no of question in paper">Qs.</th>
                        <th>Icon</th>
                        <th>Audio File</th>
                        <th>Audio Length</th>                      						
                        <th><?php echo ACTION;?></th>
                    </tr>
                    </thead>
                    <tbody id="myTable">
                    <?php $sr=0; foreach($ts_cat_assoc as $t){ $zero=0;$one=1;$pk='ts_cat_assoc_id'; $table='ts_cat_assoc';$sr++; ?>
                    <tr>
                        <td style="background: <?php echo $t['test_seriese_color'];?>"><?php echo $sr; ?></td>
                        <!-- <td><?php echo $t['test_module_name']; ?></td>  -->
						<td><?php echo $t['test_seriese_name'].SEP.$t['test_seriese_type']; ?></td>
						<td><?php echo $t['programe_name'].SEP.$t['category_name']; ?></td>
                        <td>
                            <?php 
                                if(isset($t['paper_duration']) or $t['paper_duration']!=''){
                                    echo $t['paper_duration'].PAPER_DURATION_UNIT; 
                                }else{
                                    echo NILL;
                                }                                
                            ?> 
                        </td>
                        <td><?php echo $t['max_marks']; ?></td> 
                        <td>
                            <?php 
                                if(isset($t['totalquest'])){
                                    echo $t['totalquest']; 
                                }else{
                                    echo NA;
                                }                                
                            ?>                                
                        </td>

                        <td>
                            <?php if(isset($t['icon'])){ ?>
                            <img src= '<?php echo $t["icon"];?>' style="width:25px;height:25px;"/>
                            <?php }else{echo NO_ICON;}  ?>
                        </td>

                         <td>
                            <?php 
                                if(isset($t['audio_file'])){      
                                    echo '<span>
                                            <a href="'.$t['audio_file'].'" target="_blank">'.OPEN_FILE.'</a>
                                        </span>';
                                }else{
                                    echo NA;
                                }                                
                            ?>   
                        </td>

                        <td>
                            <?php 
                                if(isset($t['audio_time'])){

                                    echo $t['audio_time'];
                                }else{
                                    echo NA;
                                }                                
                            ?>
                                
                        </td>
                       
                        <td>
                            <a href="<?php echo site_url('ts_cat_assoc/edit/'.$t['ts_cat_assoc_id']); ?>" class="btn btn-info btn-xs" data-toggle="tooltip" title="Edit"><span class="fa fa-pencil"></span> </a> 
                            <a href="<?php echo site_url('ts_cat_assoc/remove/'.$t['ts_cat_assoc_id']); ?>" class="btn btn-danger btn-xs" data-toggle="tooltip" title="Delete" onclick="return confirm('Are you sure you want to delete this item?');"><span class="fa fa-trash"></span> </a>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
                </table>
                <div class="pull-right">
                    <?php echo $this->pagination->create_links(); ?>                    
                </div>                
            </div>
        </div>
    </div>
</div>