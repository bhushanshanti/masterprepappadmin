<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header bg-danger">
                <?php
                foreach ($roledata as $r) {
                   $role_name=$r['name'];
                }               
                ?> 
                <h3 class="box-title text-primary">Access list for <b><?php echo $role_name;?></b></h3>
                <h5 class="text-danger">
                    Note:
                    <ul>
                    <li>index <i class="fa fa-arrow-right"></i> All Data Lising Page</li>
                    <li>gt <i class="fa fa-arrow-right"></i> <?php echo GT;?></li>
                    <li>ee <i class="fa fa-arrow-right"></i> <?php echo ENGLISH_ESSENTIAL;?></li>
                    <li>et <i class="fa fa-arrow-right"></i> <?php echo ET;?></li>
                    <li>mt <i class="fa fa-arrow-right"></i> <?php echo MOCK;?></li>
                    <li>sp <i class="fa fa-arrow-right"></i> Static Pages</li>
                    </ul>
                </h5>            	
                <?php echo $this->session->flashdata('flsh_msg');?>
                <?php echo form_open('role/manage_role/'.$role['id']); ?>
            </div>
            <div class="box-body table-responsive">
                <table class="table table-striped table-bordered table-sm">
                    <tr>
						<th><?php echo SR;?></th>
                        <th>Id</th>	
						<th>Module</th>
                        <th><input type="checkbox" id="select_all"> Task</th>                       
                    </tr>

                    <?php $sr=0; foreach($controllers as $c){ $sr++;?>
                    <tr>
						<td><?php echo $sr; ?></td>	
                        <td><?php echo $c['id']; ?></td>					 
						<td><b>
                            <?php 
                            //$l = count($c['controller_name'])-5;
                            //echo substr($c['controller_name'], 0, $l); 
                            echo $c['controller_name'];
                            ?>
                            </b> 
                            <!-- <input type="checkbox" id="select_all_c" name="cb_controller[]" value="<?php echo $c['id'];?>" > -->
                                                     
                        </td>
                        <?php $CI=&get_instance();?>
                        <td>
                            <?php 
                            foreach ($c['Methods'] as $m) { 
                                $is_checked = $CI->is_checked($role['id'],$c['id'],$m['id']);
                                if($is_checked){
                                    $ch= 'checked="checked"';
                                }else{
                                    $ch='';
                                }
                            ?>
                                <span>
                                    <label>
                                    <input type="checkbox" name="cb_method[]" value="<?php echo $c['id'].'~'.$m['id'];?>" id='<?php echo $c['id'];?>' <?php echo $ch;?> class="checkbox_allmethod"> 
                                    <?php echo $m['method_name'];?>&nbsp;
                                </label>
                                </span>
                               
                            <?php } ?>
                        </td>


                    </tr>
                    <?php } ?>
                </table><input type="text" name="ok" id='ok' value='save the access' disabled="disabled">
                <span class="text-danger"><?php echo form_error('ok');?></span>
                               
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-success">
                    <i class="fa fa-check"></i> Save
                </button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>
<script src="<?php echo site_url('resources/js/jquery.min.js');?>"></script> 
<script type="text/javascript">
$(document).ready(function(){
    $('#select_all').on('click',function(){
        if(this.checked){
            $('.checkbox_allmethod').each(function(){
                this.checked = true;
            });
        }else{
             $('.checkbox_allmethod').each(function(){
                this.checked = false;
            });
        }
    });
    
    $('.checkbox_allmethod').on('click',function(){
        if($('.checkbox:checked').length == $('.checkbox').length){
            $('#select_all').prop('checked',true);
        }else{
            $('#select_all').prop('checked',false);
        }
    });
});
</script>
