<input class="form-control" type="text" placeholder="Search" aria-label="Search" id="myInput">
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header bg-danger">
                <h3 class="box-title text-primary"><?php echo $title;?></h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('package_master/add'); ?>" class="btn btn-success btn-sm">Add</a> 
                </div>
                <?php echo $this->session->flashdata('flsh_msg'); ?>
            </div>
            <div class="box-body table-responsive">
                <table class="table table-striped table-bordered table-sm">
                    <thead>
                    <tr>
                        <th><?php echo SR;?></th>
                        <th>Test Module</th>
                        <!-- <th>Programe</th> -->					
						<th>Package name</th>
						<th>Amount (in Rs.)</th>
						<th>Duration (in days)</th>						
						<th>Limit</th>						
                        <th>Desc.</th>
						<th><?php echo STATUS;?></th>
                        <th><?php echo ACTION;?></th>
                    </tr>
                    </thead>
                    <tbody id="myTable">
                    <?php $sr=0;foreach($package_masters as $p){ $zero=0;$one=1;$pk='package_id'; $table='package_masters';$sr++; ?>
                    <tr>
						<td style="background: <?php echo $p['package_color'];?>"><?php echo $sr; ?></td>	
                        <td><?php echo $p['test_module_name']; ?></td>
                        <!-- <td><?php echo $p['programe_name']; ?></td> -->				
						<td><?php echo $p['package_name']; ?></td>
						<td><?php echo $p['amount'].'/-'; ?></td>
						<td><?php echo $p['duration']; ?></td>						
						<td><?php echo $p['test_paper_limit']; ?></td>
						
						<td><?php echo $p['package_desc']; ?></td>
                        <td>
                            <?php 
                            if($p['active']==1){
                                echo '<span class="text-success"><a href="javascript:void(0);" id='.$p['package_id'].' data-toggle="tooltip" title="Click to De-activate" onclick=activate_deactivete('.$p['package_id'].','.$zero.',"'.$table.'","'.$pk.'") >'.ACTIVE.'</a></span>';
                            }else{
                                echo '<span class="text-danger"><a href="javascript:void(0);" id='.$p['package_id'].' title="Click to Activate" onclick=activate_deactivete('.$p['package_id'].','.$one.',"'.$table.'","'.$pk.'") >'.DEACTIVE.'</a></span>';
                            }
                            ?>                                
                        </td>
						<td>
                            <a href="<?php echo site_url('package_master/edit/'.$p['package_id']); ?>" class="btn btn-info btn-xs" data-toggle="tooltip" title="Edit"><span class="fa fa-pencil"></span> </a> 
                            <a href="<?php echo site_url('package_master/remove/'.$p['package_id']); ?>" class="btn btn-danger btn-xs" data-toggle="tooltip" title="Delete" onclick="return confirm('Are you sure you want to delete this item?');"><span class="fa fa-trash"></span> </a>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
                </table>
                <div class="pull-right">
                    <?php echo $this->pagination->create_links(); ?>                    
                </div>                
            </div>
        </div>
    </div>
</div>
