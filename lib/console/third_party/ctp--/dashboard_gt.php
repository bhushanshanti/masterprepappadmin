<span class="badge badge-danger text-uppercase">Dashboard -General Training: </span><br/><br/>

<button type="button" name="ACD" class="btn btn-warning btn-lg" onclick="show_acd_dashboard();">Academic Dashboard</button>
<button type="button" name="EE" class="btn btn-info btn-lg" onclick="show_ee_dashboard();">English essentials Dashboard</button><br/><br/>

<div class="row bg-primary">
  <br/>

  <div class="col-md-3 gt" data-toggle="tooltip">
    <a href="<?php echo site_url('Student/enquiry_gt');?>" target="<?php echo TARGET_S;?>" class="text-danger">
        <div class="info-box">  
        <span class="info-box-icon <?php echo BG;?>">
          <span class="fa fa-commenting"></span>
        </span>
        <div class="info-box-content">
          <span class="info-box-text text-muted">Enquiry</span>
          <span class="info-box-number text-muted"><?php echo $ec_gt;?></span>
        </div>
      </div>
      </a>
  </div>

  <div class="col-md-3 gt" data-toggle="tooltip">
    <a href="<?php echo site_url('Student/visa_enquiry_gt');?>" target="<?php echo TARGET_S;?>" class="text-danger">
        <div class="info-box">  
        <span class="info-box-icon <?php echo BG;?>">
          <span class="fa fa-commenting"></span>
        </span>
        <div class="info-box-content">
          <span class="info-box-text text-muted">VISA Enquiry</span>
          <span class="info-box-number text-muted"><?php echo $vec_gt;?></span>
        </div>
      </div>
      </a>
  </div>

  <div class="col-md-3">
    <a href="<?php echo site_url('student/index_gt');?>" target="<?php echo TARGET_S;?>" class="text-danger">

        <div class="info-box">
        <span class="info-box-icon <?php echo BG;?>"><i class="fa fa-check text-success"></i></span>
        <div class="info-box-content">
          <span class="info-box-text text-muted">Paper checked</span>
          <span class="info-box-number text-muted"><?php echo $tpc;?></span>
        </div>
      </div>
      </a>
  </div>

  <div class="col-md-3">
    <a href="<?php echo site_url('student/index_gt');?>" target="<?php echo TARGET_S;?>" class="text-danger">

        <div class="info-box">
        <span class="info-box-icon <?php echo BG;?>"><i class="fa fa-close text-danger"></i></span>
        <div class="info-box-content">
          <span class="info-box-text text-muted">Paper un-checked</span>
          <span class="info-box-number text-muted"><?php echo $tpuc;?></span>
        </div>
      </div>
      </a>
  </div>

  <div class="col-md-3 gt">
    <a href="<?php echo site_url('test_seriese/index_gt');?>" target="<?php echo TARGET_S;?>" class="text-danger">
        <div class="info-box">
        <span class="info-box-icon <?php echo BG;?>">
          <i class="fa fa-book text-warning"></i></span>
        <div class="info-box-content">
          <span class="info-box-text text-muted"><?php echo IELTS;?> Tests</span>
          <span class="info-box-number text-muted"><?php echo $tc_gt;?></span>
        </div>
      </div>
      </a>
  </div>

  <div class="col-md-3 ET">
    <a href="<?php echo site_url('test_seriese/index_et_gt');?>" target="<?php echo TARGET_S;?>" class="text-danger">
        <div class="info-box">
        <span class="info-box-icon <?php echo BG;?>">
          <i class="fa fa-book text-warning"></i></span>
        <div class="info-box-content">
          <span class="info-box-text text-muted">Evaluation Tests</span>
          <span class="info-box-number text-muted"><?php echo $ic_gt;?></span>
        </div>
      </div>
      </a>
  </div>

  <div class="col-md-3 acd">
    <a href="<?php echo site_url('test_seriese/index_mt_gt');?>" target="<?php echo TARGET_S;?>" class="text-danger">
        <div class="info-box">
        <span class="info-box-icon <?php echo BG;?>">
          <i class="fa fa-book text-warning"></i></span>
        <div class="info-box-content">
          <span class="info-box-text text-muted"><?php echo MOCK;?> Tests</span>
          <span class="info-box-number text-muted"><?php echo $mc_gt;?></span>
        </div>
      </div>
      </a>
  </div> 

   <div class="col-md-3 gt">
    <a href="<?php echo site_url('static_page/index_gt');?>" target="<?php echo TARGET_S;?>" class="text-danger">
        <div class="info-box">
        <span class="info-box-icon <?php echo BG;?>">
          <i class="fa fa-lightbulb-o text-danger"></i></span>
        <div class="info-box-content">
          <span class="info-box-text text-muted">Static Pages</span>
        </div>
      </div>
      </a>
  </div>  

  <div class="col-md-3 gt">
    <a href="<?php echo site_url('student/index_gt');?>" target="<?php echo TARGET_S;?>" class="text-danger">
        <div class="info-box">
        <span class="info-box-icon <?php echo BG;?>">
          <i class="fa fa-users text-success"></i></span>
        <div class="info-box-content">
          <span class="info-box-text text-muted">Students</span>
          <span class="info-box-number text-muted"><?php echo $sc_gt;?></span>
        </div>
      </div>
      </a>
  </div>

  <div class="col-md-3 gt">
    <a href="<?php echo site_url('band_score/index_gt');?>" target="<?php echo TARGET_S;?>" class="text-danger">
        <div class="info-box">
        <span class="info-box-icon <?php echo BG;?>">
          <i class="fa fa-question text-success"></i></span>
        <div class="info-box-content">
          <span class="info-box-text text-muted">Band Score</span>
        </div>
      </div>
      </a>
  </div> 

  <div class="col-md-3 gt">
    <a href="<?php echo site_url('package_master/index_gt');?>" target="<?php echo TARGET_S;?>" class="text-danger">
        <div class="info-box">
        <span class="info-box-icon <?php echo BG;?>">
          <i class="fa fa-briefcase text-primary"></i></span>
        <div class="info-box-content">
          <span class="info-box-text text-muted">Packages</span>
          <span class="info-box-number text-muted"><?php echo $pkgc_gt;?></span>
        </div>
      </div>
      </a>
  </div>  

  <div class="col-md-3 gt">
    <a href="<?php echo site_url('static_page/print_sp/50');?>" target="<?php echo TARGET_S;?>" class="text-danger">
        <div class="info-box">
        <span class="info-box-icon <?php echo BG;?>">
          <i class="fa fa-print text-danger"></i></span>
        <div class="info-box-content">
          <span class="info-box-text text-muted">Listening Content</span>
        </div>
      </div>
      </a>
  </div>

  <div class="col-md-3 gt">
    <a href="<?php echo site_url('static_page/print_sp/51');?>" target="<?php echo TARGET_S;?>" class="text-danger">
        <div class="info-box">
        <span class="info-box-icon <?php echo BG;?>">
          <i class="fa fa-print text-danger"></i></span>
        <div class="info-box-content">
          <span class="info-box-text text-muted">Reading Content</span>
        </div>
      </div>
      </a>
  </div>

  <div class="col-md-3 gt">
    <a href="<?php echo site_url('static_page/print_sp/48');?>" target="<?php echo TARGET_S;?>" class="text-danger">
        <div class="info-box">
        <span class="info-box-icon <?php echo BG;?>">
          <i class="fa fa-print text-danger"></i></span>
        <div class="info-box-content">
          <span class="info-box-text text-muted">Speaking Content</span>
        </div>
      </div>
      </a>
  </div>

  <div class="col-md-3 gt">
    <a href="<?php echo site_url('static_page/print_sp/49');?>" target="<?php echo TARGET_S;?>" class="text-danger">
        <div class="info-box">
        <span class="info-box-icon <?php echo BG;?>">
          <i class="fa fa-print text-danger"></i></span>
        <div class="info-box-content">
          <span class="info-box-text text-muted">Writing Content</span>
        </div>
      </div>
      </a>
  </div>