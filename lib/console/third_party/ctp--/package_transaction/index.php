<input class="form-control" type="text" placeholder="Search" aria-label="Search" id="myInput">
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header bg-danger">
                <h3 class="box-title text-primary"><?php echo $title;?></h3>
                <?php echo $this->session->flashdata('flsh_msg');?>
            </div>
            <div class="box-body table-responsive">
                <table class="table table-striped table-bordered table-sm">
                    <thead>
                    <tr>
						<th><?php echo SR;?></th>
                        <th>Name</th>						
						<th>Email Id</th>						
						<th>Contact no.</th>
						<th>Payment Id</th>
                        <!-- <th>Order Id</th> -->	
						<th>Pack</th>						
                        <th>Amount Paid</th>
                        <th>Method</th>
                        <th>Left</th>
						<th>Pack Status</th>
                        <th>Subscribed</th>
                        <th>Expiry</th>
                        <th>Requested</th>
                        <!-- <th><?php echo ACTION;?></th> -->
                    </tr>
                    </thead>
                    <tbody id="myTable">
                    <?php $sr=0;foreach($transaction as $s){$zero=0;$one=1;$pk='student_package_id'; $table='student_package';$sr++; ?>
                    <tr>
						<td><?php echo $sr; ?></td>
                        <td><?php echo $s['fname'].' '.$s['lname']; ?></td>	
						<td><a href="mailto:<?php echo $s['email'];?>"><?php echo $s['email']; ?></a></td>	
                        <td><?php echo $s['contact']; ?></td>					
						<td><?php echo $s['payment_id']; ?></td>
						<!-- <td><?php echo $s['order_id']; ?></td> -->
						<td><?php echo $s['package_name']; ?></td>
						<td><?php echo $s['amount_paid']; ?></td>
                        <td><?php echo $s['method']; ?></td>
                        <td><?php echo $s['paper_left']; ?></td>
                        <td>
                            <?php 
                            if($s['package_status']==1){
                                echo '<span class="text-danger">'.ACTIVE.'</span>';
                            }else{
                                echo '<span class="text-danger">'.DEACTIVE.'</span>';
                            }
                            ?>
                                
                        </td>
                        <td><?php echo $s['subscribed_on']; ?></td>
                        <td><?php echo $s['expired_on']; ?></td>
                        <td><?php echo $s['requested_on']; ?></td>
                       	 
						
                    </tr>
                    <?php } ?>
                </tbody>
                </table>
                <div class="pull-right">
                    <?php echo $this->pagination->create_links(); ?>                    
                </div>                
            </div>
        </div>
    </div>
</div>