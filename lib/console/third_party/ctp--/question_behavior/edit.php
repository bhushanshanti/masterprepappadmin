<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Edit Question Behavior</h3>
            </div>
            <?php echo $this->session->flashdata('flsh_msg'); ?>
			<?php echo form_open('question_behavior/edit/'.$question_behavior['question_behavior_id']); ?>
			<div class="box-body">
				<div class="row clearfix">
					
					<div class="col-md-6">
						<label for="behavior_name" class="control-label"><span class="text-danger">*</span>Question Behavior Name</label>
						<div class="form-group has-feedback">
							<input type="text" name="behavior_name" value="<?php echo ($this->input->post('behavior_name') ? $this->input->post('behavior_name') : $question_behavior['behavior_name']); ?>" class="form-control" id="behavior_name" maxlength="50"/>
							<span class="glyphicon glyphicon-th-large form-control-feedback"></span>
							<span class="text-danger"><?php echo form_error('behavior_name');?></span>
						</div>
					</div>

					<div class="col-md-12">
						<div class="form-group">
							<input type="checkbox" name="active" value="1" <?php echo ($question_behavior['active']==1 ? 'checked="checked"' : ''); ?> id='active' />
							<label for="active" class="control-label">Active</label>
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
            	<button type="submit" class="btn btn-success">
					<i class="fa fa-check"></i> Save
				</button>
	        </div>				
			<?php echo form_close(); ?>
		</div>
    </div>
</div>