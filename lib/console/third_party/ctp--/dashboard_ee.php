<span class="badge badge-danger text-uppercase">Dashboard -English essentials: </span><br/></br>
<button type="button" name="GT" class="btn btn-primary btn-lg" onclick="show_gt_dashboard();">General Training Dashboard</button>
<button type="button" name="ACD" class="btn btn-warning btn-lg" onclick="show_acd_dashboard();">Academic Dashboard</button>
<br/><br/>

<div class="row bg-info">
<br/>
 <div class="col-md-3 ee">
    <a href="<?php echo site_url('test_seriese/english_essential');?>" target="<?php echo TARGET_S;?>" class="text-danger">
        <div class="info-box">
        <span class="info-box-icon <?php echo BG;?>">
          <i class="fa fa-book text-warning"></i></span>
        <div class="info-box-content">
          <span class="info-box-text text-muted"><?php echo GRAMMAR;?> Tests</span>
          <span class="info-box-number text-muted"><?php echo $ec;?></span>
        </div>
      </div>
      </a>
  </div>

  <div class="col-md-3 ee">
    <a href="<?php echo site_url('static_page/print_sp/57');?>" target="<?php echo TARGET_S;?>" class="text-danger">
        <div class="info-box">
        <span class="info-box-icon <?php echo BG;?>">
          <i class="fa fa-print text-danger"></i></span>
        <div class="info-box-content">
          <span class="info-box-text text-muted"><?php echo POS;?></span>
        </div>
      </div>
      </a>
  </div>

  <div class="col-md-3 ee">
    <a href="<?php echo site_url('static_page/print_sp/71');?>" target="<?php echo TARGET_S;?>" class="text-danger">
        <div class="info-box">
        <span class="info-box-icon <?php echo BG;?>">
          <i class="fa fa-print text-danger"></i></span>
        <div class="info-box-content">
          <span class="info-box-text text-muted"><?php echo TENSE;?></span>
        </div>
      </div>
      </a>
  </div>

  <div class="col-md-3 ee">
    <a href="<?php echo site_url('static_page/print_sp/72');?>" target="<?php echo TARGET_S;?>" class="text-danger">
        <div class="info-box">
        <span class="info-box-icon <?php echo BG;?>">
          <i class="fa fa-print text-danger"></i></span>
        <div class="info-box-content">
          <span class="info-box-text text-muted"><?php echo DI;?></span>
        </div>
      </div>
      </a>
  </div>

  <div class="col-md-3 ee">
    <a href="<?php echo site_url('static_page/print_sp/73');?>" target="<?php echo TARGET_S;?>" class="text-danger">
        <div class="info-box">
        <span class="info-box-icon <?php echo BG;?>">
          <i class="fa fa-print text-danger"></i></span>
        <div class="info-box-content">
          <span class="info-box-text text-muted"><?php echo AP;?></span>
        </div>
      </div>
      </a>
  </div>

</div>