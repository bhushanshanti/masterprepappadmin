<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Edit test_module</h3>
            </div>
            <?php echo $this->session->flashdata('flsh_msg'); ?>
			<?php echo form_open('test_module/edit/'.$test_module['test_module_id']); ?>
			<div class="box-body">
				<div class="row clearfix">
					
					<div class="col-md-6">
						<label for="test_module_name" class="control-label"><span class="text-danger">*</span>Test Module Name</label>
						<div class="form-group">
							<input type="text" name="test_module_name" value="<?php echo ($this->input->post('test_module_name') ? $this->input->post('test_module_name') : $test_module['test_module_name']); ?>" class="form-control" id="test_module_name" />
							<span class="text-danger"><?php echo form_error('test_module_name');?></span>
						</div>
					</div>

					<div class="col-md-6">
						<label for="programe_id" class="control-label"><span class="text-danger">*</span>Program</label>
						<div class="form-group">
							<select name="programe_id" class="form-control selectpicker" data-show-subtext="true" data-live-search="true">
								<option data-subtext="" value="">Select programme</option>
								<?php 
								foreach($all_programe_masters as $p)
								{
									$selected = ($p['programe_id'] == $test_module['programe_id']) ? ' selected="selected"' : "";

									echo '<option data-subtext="'.$p['programe_name'].'" value="'.$p['programe_id'].'" '.$selected.'>'.$p['programe_name'].'</option>';
								} 
								?>
							</select>
							<span class="text-danger"><?php echo form_error('programe_id');?></span>
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="form-group">
							<label for="active" class="control-label">Active</label>
							<input type="checkbox" name="active" value="1" <?php echo ($test_module['active']==1 ? 'checked="checked"' : ''); ?> id='active' />	
						</div>
					</div>
					<div class="col-md-12">
						<label for="test_module_desc" class="control-label">Test Module Description</label>
						<div class="form-group has-feedback">
							<textarea name="test_module_desc" class="form-control" id="test_module_desc"><?php echo ($this->input->post('test_module_desc') ? $this->input->post('test_module_desc') : $test_module['test_module_desc']); ?></textarea>
							<span class="glyphicon glyphicon-text-size form-control-feedback"></span>
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
            	<button type="submit" class="btn btn-success">
					<i class="fa fa-check"></i> Save
				</button>
	        </div>				
			<?php echo form_close(); ?>
		</div>
    </div>
</div>