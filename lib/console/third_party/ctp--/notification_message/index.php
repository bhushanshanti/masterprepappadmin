<input class="form-control" type="text" placeholder="Search" aria-label="Search" id="myInput">
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header bg-danger">
                <h3 class="box-title text-primary"><?php echo $title;?></h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('notification_message/add'); ?>" class="btn btn-success btn-sm">Add</a> 
                </div>
                <?php echo $this->session->flashdata('flsh_msg'); ?>
            </div>
            <div class="box-body table-responsive">
                <table class="table table-striped table-bordered table-sm">
                    <thead>
                    <tr>
						<th><?php echo SR;?></th>
                        <th>Subject</th>
                        <th>Message</th>
                        <th><?php echo STATUS;?></th>
                        <th><?php echo ACTION;?></th>
                    </tr>
                </thead>
                <tbody id="myTable">
                    <?php $sr=0;foreach($notification_message as $c){$zero=0;$one=1;$pk='messege_id'; $table='notification_message';$sr++; ?>
                    <tr>
						<td><?php echo $sr; ?></td>
                        <td><?php echo $c['subject']; ?></td>     
                        <td><?php echo $c['message']; ?></td> 
                        <td>
                            <?php 
                            if($c['active']==1){
                                echo '<span class="text-success"><a href="javascript:void(0);" id='.$c['messege_id'].' data-toggle="tooltip" title="Click to De-activate" onclick=activate_deactivete('.$c['messege_id'].','.$zero.',"'.$table.'","'.$pk.'") >'.ACTIVE.'</a></span>';
                            }else{
                                echo '<span class="text-danger"><a href="javascript:void(0);" id='.$c['messege_id'].' title="Click to Activate" onclick=activate_deactivete('.$c['messege_id'].','.$one.',"'.$table.'","'.$pk.'") >'.DEACTIVE.'</a></span>';
                            }
                            ?>                                
                        </td>
						<td>
                            <a href="<?php echo site_url('notification_message/edit/'.$c['messege_id']); ?>" class="btn btn-info btn-xs" data-toggle="tooltip" title="Edit"><span class="fa fa-pencil"></span> </a> 
                            <a href="<?php echo site_url('notification_message/remove/'.$c['messege_id']); ?>" class="btn btn-danger btn-xs" data-toggle="tooltip" title="Delete" onclick="return confirm('Are you sure you want to delete this item?');"><span class="fa fa-trash"></span> </a>
                        </td>
                    </tr>
                    <?php } ?>
                    </tbody>
                </table>
                <div class="pull-right">
                    <?php echo $this->pagination->create_links(); ?>                    
                </div>                
            </div>
        </div>
    </div>
</div>