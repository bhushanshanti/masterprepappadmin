<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Edit Proficiency level image</h3>
            </div>
            <?php echo $this->session->flashdata('flsh_msg'); ?>
			<?php echo form_open('proficiency_level/edit/'.$proficiency_level['pro_id']); ?>
			<div class="box-body">
				<div class="row clearfix">
					
					<div class="col-md-6">
						<label for="pro_name" class="control-label"><span class="text-danger">*</span>Image URL</label>
						<div class="form-group has-feedback">
							<input type="text" name="pro_name" value="<?php echo ($this->input->post('pro_name') ? $this->input->post('pro_name') : $proficiency_level['pro_name']); ?>" class="form-control" id="pro_name" />
							<span class="glyphicon glyphicon-link form-control-feedback"></span>
							
							<span class="text-danger"><?php echo form_error('pro_name');?></span>
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group">
							<input type="checkbox" name="active" value="1" <?php echo ($proficiency_level['active']==1 ? 'checked="checked"' : ''); ?> id='active' />
							<label for="active" class="control-label">Active</label>
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
            	<button type="submit" class="btn btn-success">
					<i class="fa fa-check"></i> Save
				</button>
	        </div>				
			<?php echo form_close(); ?>
		</div>
    </div>
</div>