<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Student Edit</h3>
            </div>
            <?php echo $this->session->flashdata('flsh_msg');?>
			<?php echo form_open('student/edit/'.$student['id']); ?>
			<div class="box-body">
				<div class="row clearfix">
					
					<div class="col-md-4">
						<label for="programe_id" class="control-label"><span class="text-danger">*</span>Program</label>
						<div class="form-group">
							<select name="programe_id" class="form-control selectpicker" data-show-subtext="true" data-live-search="true">
								<option value="">Select Program</option>
								<?php 
								foreach($all_programe_masters as $programe_master)
								{
									$selected = ($programe_master['programe_id'] == $student['programe_id']) ? ' selected="selected"' : "";

									echo '<option value="'.$programe_master['programe_id'].'" '.$selected.'>'.$programe_master['programe_name'].'</option>';
								} 
								?>
							</select>
							<span class="text-danger"><?php echo form_error('programe_id');?></span>
						</div>
					</div>

					<div class="col-md-4">
						<label for="gender_name" class="control-label"><span class="text-danger">*</span>Gender</label>
						<div class="form-group">
							<select name="gender_name" class="form-control selectpicker" data-show-subtext="true" data-live-search="true">
								<option value="">Select gender</option>
								<?php 
								foreach($all_genders as $g)
								{
									$selected = ($g['id'] == $student['gender']) ? ' selected="selected"' : "";

									echo '<option '.$selected.'>'.$g['gender_name'].'</option>';
								} 
								?>
							</select>
							<span class="text-danger"><?php echo form_error('gender_name');?></span>
						</div>
					</div>

					<div class="col-md-4">
						<label for="mobile" class="control-label"><span class="text-danger">*</span>Mobile no.</label>
						<div class="form-group has-feedback">
							<input type="text" name="mobile" value="<?php echo ($this->input->post('mobile') ? $this->input->post('mobile') : $student['mobile']); ?>" class="form-control" id="mobile" maxlength="10" minlength="10"/>
							<span class="glyphicon glyphicon-phone form-control-feedback"></span>
							<span class="text-danger"><?php echo form_error('mobile');?></span>
						</div>
					</div>				

					<div class="col-md-3">
						<label for="email" class="control-label">Email Id</label>
						<div class="form-group has-feedback">
							<input type="text" name="email" value="<?php echo ($this->input->post('email') ? $this->input->post('email') : $student['email']); ?>" class="form-control" id="email" maxlength="60"/>
							<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
							<span class="text-danger"><?php echo form_error('email');?></span>
						</div>
					</div>
					

					<div class="col-md-3">
						<label for="fname" class="control-label"><span class="text-danger">*</span>First name</label>
						<div class="form-group">
							<input type="text" name="fname" value="<?php echo ($this->input->post('fname') ? $this->input->post('fname') : $student['fname']); ?>" class="form-control" id="fname" />
							<span class="text-danger"><?php echo form_error('fname');?></span>
						</div>
					</div>
					
					<div class="col-md-3">
						<label for="lname" class="control-label">Last name</label>
						<div class="form-group">
							<input type="text" name="lname" value="<?php echo ($this->input->post('lname') ? $this->input->post('lname') : $student['lname']); ?>" class="form-control" id="lname" />
						</div>
					</div>

					<div class="col-md-3">
						<label for="dob" class="control-label">Date of birth</label>
						<div class="form-group has-feedback">
							<input type="text" name="dob" value="<?php echo ($this->input->post('dob') ? $this->input->post('dob') : $student['dob']); ?>" class="has-datepicker form-control" id="dob" />
							<span class="glyphicon glyphicon-calendar form-control-feedback"></span>
						</div>
					</div>
					
					<div class="col-md-12">
						<label for="residential_address" class="control-label">Residential address</label>
						<div class="form-group">
							<textarea name="residential_address" class="form-control" id="residential_address"><?php echo ($this->input->post('residential_address') ? $this->input->post('residential_address') : $student['residential_address']); ?></textarea>
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group">
							<input type="checkbox" name="active" value="1" <?php echo ($student['active']==1 ? 'checked="checked"' : ''); ?> id='active' />
							<label for="active" class="control-label">Active</label>
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
            	<button type="submit" class="btn btn-success">
					<i class="fa fa-check"></i> Save
				</button>
	        </div>				
			<?php echo form_close(); ?>
		</div>
    </div>
</div>