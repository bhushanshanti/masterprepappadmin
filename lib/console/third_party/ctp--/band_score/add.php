<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Add Band Score </h3>
            </div>
            <?php echo $this->session->flashdata('flsh_msg'); ?>
            <?php echo form_open('band_score/add'); ?>
          	<div class="box-body">
          		<div class="row clearfix">
					
					<div class="col-md-3">
						<label for="category_id" class="control-label"><span class="text-danger">*</span>Category</label>
						<div class="form-group">
							<select name="category_id" class="form-control selectpicker" data-show-subtext="true" data-live-search="true">
								<option value="">Select Category</option>
								<?php 
								foreach($all_category_masters as $category_master)
								{
									$selected = ($category_master['category_id'] == $this->input->post('category_id')) ? ' selected="selected"' : "";

									echo '<option value="'.$category_master['category_id'].'" '.$selected.'>'.$category_master['programe_name'].' | '.$category_master['category_name'].'</option>';
								} 
								?>
							</select>
							<span class="text-danger"><?php echo form_error('category_id');?></span>
						</div>
					</div>
					<div class="col-md-3">
						<label for="band_score" class="control-label"><span class="text-danger">*</span>Band Score</label>
						<div class="form-group">
							<input type="text" name="band_score" value="<?php echo $this->input->post('band_score'); ?>" class="form-control" id="band_score" maxlength="4"/>
							<span class="text-danger"><?php echo form_error('band_score');?></span>
						</div>
					</div>
					<div class="col-md-3">
						<label for="band_range_lower" class="control-label"><span class="text-danger">*</span>Band Lower Range</label>
						<div class="form-group has-feedback">
							<input type="text" name="band_range_lower" value="<?php echo $this->input->post('band_range_lower'); ?>" class="form-control" id="band_range_lower" maxlength="3"/>
							<span class="glyphicon glyphicon-arrow-down form-control-feedback"></span>
							<span class="text-danger"><?php echo form_error('band_range_lower');?></span>
						</div>
					</div>

					<div class="col-md-3">
						<label for="band_range_upper" class="control-label"><span class="text-danger">*</span>Band Upper Range</label>
						<div class="form-group has-feedback">
							<input type="text" name="band_range_upper" value="<?php echo $this->input->post('band_range_upper'); ?>" class="form-control" id="band_range_upper" maxlength="3"/>
							<span class="glyphicon glyphicon-arrow-up form-control-feedback"></span>
							<span class="text-danger"><?php echo form_error('band_range_upper');?></span>
						</div>
					</div>

					

					<div class="col-md-3">
						<label for="pro_id" class="control-label"><span class="text-danger">*</span>Proficiency Level</label>
						<div class="form-group">
							<select name="pro_id" class="form-control selectpicker" data-show-subtext="true" data-live-search="true">
								<option value="">Select Proficiency Level</option>
								<?php 
								foreach($all_pro_level as $pro_level)
								{
									$selected = ($pro_level['pro_id'] == $this->input->post('pro_id')) ? ' selected="selected"' : "";

									echo '<option value="'.$pro_level['pro_id'].'" '.$selected.'>'.$pro_level['pro_name'].'</option>';
								} 
								?>
							</select>
							<span class="text-danger"><?php echo form_error('pro_id');?></span>
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group">
							<input type="checkbox" name="active" value="1"  id="active" checked="checked"/>
							<label for="active" class="control-label">Active</label>
						</div>
					</div>

				</div>
			</div>
          	<div class="box-footer">
            	<button type="submit" class="btn btn-success">
            		<i class="fa fa-check"></i> Save
            	</button>
          	</div>
            <?php echo form_close(); ?>
      	</div>
    </div>
</div>