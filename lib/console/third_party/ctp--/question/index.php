<input class="form-control" type="text" placeholder="Search" aria-label="Search" id="myInput">
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header bg-danger">
                <h3 class="box-title text-primary"><?php echo $title;?></h3>           	
            </div>
            <?php echo $this->session->flashdata('flsh_msg'); ?>
            <div class="box-body table-responsive">
                <table class="table table-striped table-bordered table-sm">
                    <thead>
                    <tr>
						<th><?php echo SR;?></th>
                        <th>Q.Id</th>	  
                        <th>Sets heading</th>                     
						<th>Question</th>
						<th>Correct Answer</th>
                        <th>Answer Explaination</th>
                        <th><?php echo STATUS;?></th>
                        <th><?php echo ACTION;?></th>
                    </tr>
                    </thead>
                    <tbody id="myTable">
                    <?php $sr= 0;foreach($questions as $q){ $sr++; ?>
                    <tr>
						<td><?php echo $sr; ?></td>
						<td><?php echo $q['question_id']; ?></td>						
                        <td><?php echo $q['question_sets_heading']; ?></td>
						<td><?php echo $q['question']; ?></td>
						<td><?php echo $q['correct_answer']; ?></td>
                        <td><?php echo $q['correct_answer_explaination']; ?></td>
                       <td>
                            <?php 
                            if($q['active']==1){
                                echo '<span class="text-success"><a href="#" data-toggle="tooltip" title="Click to De-activate" onclick="activate_deactivete('.$q['question_id'].','.$q['active'].');">'.ACTIVE.'</a></span>';
                            }else{
                                echo '<span class="text-danger"><a href="#" title="Click to Activate" onclick="activate_deactivete('.$q['question_id'].','.$q['active'].');">'.DEACTIVE.'</a></span>';
                            }
                            ?>                                
                        </td>
						<td>
                            <a href="<?php echo site_url('question/edit/'.$q['question_id']); ?>" class="btn btn-info btn-xs" data-toggle="tooltip" title="Edit"><span class="fa fa-pencil"></span> </a> 
                            <a href="<?php echo site_url('question/remove/'.$q['question_id']); ?>" class="btn btn-danger btn-xs" data-toggle="tooltip" title="Delete" onclick="return confirm('Are you sure you want to delete this item?');"><span class="fa fa-trash"></span> </a>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
                </table>
           
                <div class="pull-right">
                    <?php echo $this->pagination->create_links(); ?>                    
                </div>                
            </div>
        </div>
    </div>
</div>

