<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Sections listing</h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('section/add'); ?>" class="btn btn-success btn-sm">Add</a> 
                </div>
                <?php echo $this->session->flashdata('flsh_msg'); ?>
            </div>
            <div class="box-body table-responsive">
                <table class="table table-striped table-bordered table-sm">
                    <tr>
                        <th><?php echo SR;?></th>
						<th>Section Id</th>						
						<th>Ts Cat assoc Id</th>
						<th>Section heading</th>
						<th>Section desc</th>
						<th>Section para</th>
						<th><?php echo STATUS;?></th>
                        <th><?php echo ACTION;?></th>
                    </tr>
                    <?php $sr=0;foreach($sections as $s){$sr++;?>
                    <tr>
						<td><?php echo $sr; ?></td>
                        <td><?php echo $s['section_id']; ?></td>						
						<td><?php echo $s['ts_cat_assoc_id']; ?></td>
						<td><?php echo $s['section_heading']; ?></td>
						<td><?php echo $s['section_desc']; ?></td>
						<td><?php echo $s['section_para']; ?></td>
                       <td>
                            <?php 
                            if($s['active']==1){
                                echo '<span class="text-success"><a href="#" data-toggle="tooltip" title="Click to De-activate" onclick="activate_deactivete('.$s['section_id'].','.$s['active'].');">'.ACTIVE.'</a></span>';
                            }else{
                                echo '<span class="text-danger"><a href="#" title="Click to Activate" onclick="activate_deactivete('.$s['section_id'].','.$s['active'].');">'.DEACTIVE.'</a></span>';
                            }
                            ?>                                
                        </td>
						<td>
                            <a href="<?php echo site_url('section/edit/'.$s['section_id']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Edit</a> 
                            <a href="<?php echo site_url('section/remove/'.$s['section_id']); ?>" class="btn btn-danger btn-xs" onclick="return confirm('Are you sure you want to delete this item?');"><span class="fa fa-trash"></span> Delete</a>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
                <div class="pull-right">
                    <?php echo $this->pagination->create_links(); ?>                    
                </div>                
            </div>
        </div>
    </div>
</div>
