<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Edit Section </h3>
            </div>
            <?php echo $this->session->flashdata('flsh_msg'); ?>
			<?php echo form_open_multipart('section/edit/'.$section['section_id']); ?>
			<div class="box-body">
				<div class="row clearfix">
					
					<div class="col-md-5">
						<label for="section_heading" class="control-label"><span class="text-danger">*</span>Section Heading</label>
						<div class="form-group">
							<input type="text" name="section_heading" value="<?php echo ($this->input->post('section_heading') ? $this->input->post('section_heading') : $section['section_heading']); ?>" class="form-control" id="section_heading" />
							<span class="text-danger"><?php echo form_error('section_heading');?></span>
						</div>
					</div>

					<div class="col-md-5">
                        <label for="section_image" class="control-label">Section Image</label>
                        <span class="text-danger"><?php echo SECTION_ALLOWED_TYPES_LABEL;?></span>
                        <div class="form-group">
                        <input type="file" name="section_image" class="form-control"/>
                            <span id="section_image_msg"></span> 
                             <span>
                             	
                             	<?php 
								if(isset($section['section_image'])){      
                                    echo '<span>
                                            <a href="'.$section['section_image'].'" target="_blank">'.OPEN_FILE.'</a>
                                        </span>';
                                }else{
                                    echo NO_FILE;
                                }
                                ?>
                             </span> 
                        </div>                                            
                    </div>

                    <div class="col-md-2">
						<div class="form-group">							
							<label for="active" class="control-label">Active</label>
							<input type="checkbox" name="active" value="1" <?php echo ($section['active']==1 ? 'checked="checked"' : ''); ?> id='active' />
						</div>
					</div>

					<div class="col-md-12">
						<label for="section_desc" class="control-label">Section Description</label>
						<div class="form-group">
							<textarea name="section_desc" class="form-control myckeditor" id="section_desc"><?php echo ($this->input->post('section_desc') ? $this->input->post('section_desc') : $section['section_desc']); ?></textarea>
							<span class="text-danger"><?php echo form_error('section_desc');?></span>
						</div>
					</div>
					<div class="col-md-12">
						<label for="section_para" class="control-label">Section Paragraph</label>
						<div class="form-group">
							<textarea name="section_para" class="form-control myckeditor" id="section_para"><?php echo ($this->input->post('section_para') ? $this->input->post('section_para') : $section['section_para']); ?></textarea>
							<span class="text-danger"><?php echo form_error('section_para');?></span>
						</div>
					</div>
					
				</div>
			</div>
			<div class="box-footer">
            	<button type="submit" class="btn btn-success">
					<i class="fa fa-check"></i> Save
				</button>
	        </div>				
			<?php echo form_close(); ?>
		</div>
    </div>
</div>