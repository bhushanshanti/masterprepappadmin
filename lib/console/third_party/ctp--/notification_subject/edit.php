<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Edit Notification subject </h3>
            </div>
            <?php echo $this->session->flashdata('flsh_msg'); ?>
			<?php echo form_open('notification_subject/edit/'.$notification_subject['id']); ?>
			<div class="box-body">
				<div class="row clearfix">
					
					<div class="col-md-6">
						<label for="subject" class="control-label"><span class="text-danger">*</span>Name</label>
						<div class="form-group has-feedback">
							<input type="text" name="subject" value="<?php echo ($this->input->post('subject') ? $this->input->post('subject') : $notification_subject['subject']); ?>" class="form-control" id="subject" />							
							<span class="text-danger"><?php echo form_error('subject');?></span>
						</div>
					</div>

					<div class="col-md-6">
						<label for="icon" class="control-label">Icon URL</label>
						[
						<span class="text-danger"><a href="<?php echo site_url('gallery/add');?>" target="_blank">
							<?php echo GALLERY_URL_LABEL;?></a>
						</span>
						]&nbsp;
						[
						<span class="text-danger"><a href="<?php echo site_url('gallery/index');?>" target="_blank">
							<?php echo GALLERY_URL_LABEL_LIST;?></a>
						</span>
						]
						<div class="form-group has-feedback">
							<input type="url" name="icon" value="<?php echo ($this->input->post('icon') ? $this->input->post('icon') : $notification_subject['icon']); ?>" class="form-control" id="icon" />
							<span class="glyphicon glyphicon-link form-control-feedback"></span>
							<span class="text-danger"><?php echo form_error('icon');?></span>
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group">
							<input type="checkbox" name="active" value="1" <?php echo ($notification_subject['active']==1 ? 'checked="checked"' : ''); ?> id='active' />
							<label for="active" class="control-label">Active</label>
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
            	<button type="submit" class="btn btn-success">
					<i class="fa fa-check"></i> Save
				</button>
	        </div>				
			<?php echo form_close(); ?>
		</div>
    </div>
</div>