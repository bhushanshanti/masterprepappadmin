<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Add center location</h3>              	
            </div>
            <?php echo $this->session->flashdata('flsh_msg'); ?>
            <?php echo form_open('center_location/add'); ?>
          	<div class="box-body">
          		<div class="row clearfix">
					
					<div class="col-md-4">
						<label for="center_name" class="control-label"><span class="text-danger">*</span>Center name</label>
						<div class="form-group">
							<input type="text" name="center_name" value="<?php echo $this->input->post('center_name'); ?>" class="form-control" id="center_name" />	
							<span class="text-danger"><?php echo form_error('center_name');?></span>
						</div>						
					</div>

					<div class="col-md-4">
						<label for="contact" class="control-label"><span class="text-danger">*</span>Contact no.(if multiple write with comma seperated)</label>
						<div class="form-group has-feedback">
							<input type="text" name="contact" value="<?php echo $this->input->post('contact'); ?>" class="form-control" id="contact" />
							<span class="glyphicon glyphicon-phone form-control-feedback"></span>
							<span class="text-danger"><?php echo form_error('contact');?></span>
						</div>						
					</div>

					<div class="col-md-4">
						<label for="email" class="control-label">Email id</label>
						<div class="form-group has-feedback">
							<input type="email" name="email" value="<?php echo $this->input->post('email'); ?>" class="form-control" id="email" maxlength="60"/>
							<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
							<span class="text-danger"><?php echo form_error('email');?></span>
						</div>						
					</div>
					
					<div class="col-md-4">
						<label for="country_id" class="control-label"><span class="text-danger">*</span>Country</label>
						<div class="form-group">
							<select name="country_id" class="form-control selectpicker" data-show-subtext="true" data-live-search="true" onchange="get_state_list(this.value)">
								<option data-subtext="" value="">Select country</option>
								<?php 
								foreach($all_country_list as $p)
								{	
									echo '<option data-subtext="'.$p['name'].'" value="'.$p['country_id'].'" >'.$p['name'].'</option>';
								} 
								?>
							</select>
							<span class="text-danger"><?php echo form_error('country_id');?></span>
						</div>
					</div>

					<div class="col-md-4">
						<label for="state_id" class="control-label"><span class="text-danger">*</span>State </label>
						<div class="form-group" id="state_dd">	
							<select name="state_id" id="state_id" class="form-control selectpicker" data-show-subtext="true" data-live-search="true" onchange="get_city_list(this.value)">
								<option data-subtext="" value="">Select state</option>
							</select>						
							<span class="text-danger"><?php echo form_error('state_id');?></span>
						</div>
					</div>					

					<div class="col-md-4">
						<label for="city_id" class="control-label"><span class="text-danger">*</span>City </label>
						<div class="form-group" id="city_dd">	
							<select name="city_id" id="city_id" class="form-control selectpicker" data-show-subtext="true" data-live-search="true" >
								<option data-subtext="" value="">Select city</option>
							</select>						
							<span class="text-danger"><?php echo form_error('city_id');?></span>
						</div>
					</div>

					<div class="col-md-12">
						<label for="address_line_1" class="control-label"><span class="text-danger">*</span>Address</label>
						<div class="form-group has-feedback">
							<textarea name="address_line_1" class="form-control" id="address_line_1"><?php echo $this->input->post('address_line_1'); ?></textarea>
							<span class="glyphicon glyphicon-home form-control-feedback"></span>
							<span class="text-danger"><?php echo form_error('address_line_1');?></span>
						</div>
					</div>

					<div class="col-md-4">
						<label for="zip_code" class="control-label"><span class="text-danger">*</span>ZIP Code</label>
						<div class="form-group">
							<input type="text" name="zip_code" value="<?php echo $this->input->post('zip_code'); ?>" class="form-control" id="zip_code" maxlength="6"/>
							<span class="text-danger"><?php echo form_error('zip_code');?></span>
						</div>						
					</div>

					<div class="col-md-4">
						<label for="latitude" class="control-label">Latitude</label>
						<div class="form-group has-feedback">
							<input type="text" name="latitude" value="<?php echo $this->input->post('latitude'); ?>" class="form-control" id="latitude" />
							<span class="glyphicon glyphicon-map-marker form-control-feedback"></span>
							<span class="text-danger"><?php echo form_error('latitude');?></span>
						</div>						
					</div>

					<div class="col-md-4">
						<label for="longitude" class="control-label">Longitude</label>
						<div class="form-group has-feedback">
							<input type="text" name="longitude" value="<?php echo $this->input->post('longitude'); ?>" class="form-control" id="longitude" />
							<span class="glyphicon glyphicon-map-marker form-control-feedback"></span>
							<span class="text-danger"><?php echo form_error('longitude');?></span>
						</div>						
					</div>
					
					<div class="col-md-12">
						<div class="form-group">
							<label for="active" class="control-label">Active</label>
							<input type="checkbox" name="active" value="1"  id="active" checked="checked"/>							
						</div>
					</div>

					
				</div>
			</div>
          	<div class="box-footer">
            	<button type="submit" class="btn btn-success">
            		<i class="fa fa-check"></i> Save
            	</button> 
          	</div>
            <?php echo form_close(); ?>
      	</div>
    </div>
</div>