<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Add Country </h3>
            </div>
            <?php echo $this->session->flashdata('flsh_msg'); ?>
            <?php echo form_open('country/add'); ?>
          	<div class="box-body">
          		<div class="row clearfix">
					
					<div class="col-md-4">
						<label for="name" class="control-label"><span class="text-danger">*</span>Country Name</label>
						<div class="form-group has-feedback">
							<input type="text" name="name" value="<?php echo $this->input->post('name'); ?>" class="form-control" id="name" />
							<span class="glyphicon glyphicon-flag form-control-feedback"></span>
							<span class="text-danger"><?php echo form_error('name');?></span>
						</div>
					</div>

					<div class="col-md-4">
						<label for="iso3" class="control-label"><span class="text-danger">*</span>ISO-3</label>
						<div class="form-group">
							<input type="text" name="iso3" value="<?php echo $this->input->post('iso3'); ?>" class="form-control" id="iso3" maxlength="3"/>	
							<span class="text-danger"><?php echo form_error('iso3');?></span>
						</div>
					</div>

					<div class="col-md-4">
						<label for="phonecode" class="control-label"><span class="text-danger">*</span>Phone code</label>
						<div class="form-group has-feedback">
								<input type="text" name="phonecode" value="<?php echo $this->input->post('phonecode'); ?>" class="form-control" id="phonecode" />
							<span class="glyphicon glyphicon-phone form-control-feedback"></span>
							<span class="text-danger"><?php echo form_error('phonecode');?></span>
						</div>
					</div>	

					<div class="col-md-4">
						<label for="flag" class="control-label">Flag URL</label>
						[
						<span class="text-danger"><a href="<?php echo site_url('gallery/add');?>" target="_blank">
							<?php echo GALLERY_URL_LABEL;?></a>
						</span>
						]&nbsp;
						[
						<span class="text-danger"><a href="<?php echo site_url('gallery/index');?>" target="_blank">
							<?php echo GALLERY_URL_LABEL_LIST;?></a>
						</span>
						]
						<div class="form-group has-feedback">
							<input type="url" name="flag" value="<?php echo $this->input->post('flag'); ?>" class="form-control" id="flag" />
							<span class="glyphicon glyphicon-link form-control-feedback"></span>
							<span class="text-danger"><?php echo form_error('flag');?></span>
						</div>
					</div>

					<div class="col-md-4">
						<label for="phoneNo_limit" class="control-label">Contact no. Limit</label>
						<div class="form-group has-feedback">
								<input type="text" name="phoneNo_limit" value="<?php echo $this->input->post('phoneNo_limit'); ?>" class="form-control" id="phoneNo_limit" />
							<span class="text-danger"><?php echo form_error('phoneNo_limit');?></span>
						</div>
					</div>					
					
					<div class="col-md-4">
						<div class="form-group">
							<label for="active" class="control-label">Active</label>
							<input type="checkbox" name="active" value="1"  id="active" checked="checked"/>
							
						</div>
					</div>
					
					
				</div>
			</div>
          	<div class="box-footer">
            	<button type="submit" class="btn btn-success">
            		<i class="fa fa-check"></i> Save
            	</button>
          	</div>
            <?php echo form_close(); ?>
      	</div>
    </div>
</div>