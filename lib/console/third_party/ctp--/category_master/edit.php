<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title"> Edit Category</h3>
            </div>
            <?php echo $this->session->flashdata('flsh_msg'); ?>
			<?php echo form_open_multipart('category_master/edit/'.$category_master['category_id']); ?>
			<div class="box-body">
				<div class="row clearfix">
					
					
					<div class="col-md-4">
						<label for="category_name" class="control-label"><span class="text-danger">*</span>Category Name</label>
						<div class="form-group">
							<input type="text" name="category_name" value="<?php echo ($this->input->post('category_name') ? $this->input->post('category_name') : $category_master['category_name']); ?>" class="form-control" id="category_name" maxlength="30"/>
							<span class="text-danger"><?php echo form_error('category_name');?></span>
							<span class="text-danger"><?php echo $this->session->flashdata('flsh_msg'); ?></span>
						</div>
					</div>

					<div class="col-md-4">
						<label for="programe_id" class="control-label"><span class="text-danger">*</span>Program</label>
						<div class="form-group">
							<select name="programe_id" class="form-control selectpicker" data-show-subtext="true" data-live-search="true">
								<option data-subtext="" value="">Select Program</option>
								<?php 
								foreach($all_programe_masters as $p)
								{
									$selected = ($p['programe_id'] == $category_master['programe_id']) ? ' selected="selected"' : "";

									echo '<option data-subtext="'.$p['programe_name'].'" value="'.$p['programe_id'].'" '.$selected.'>'.$p['programe_name'].'</option>';
								} 
								?>
							</select>
							<span class="text-danger"><?php echo form_error('programe_id');?></span>
						</div>
					</div>

					<div class="col-md-4">
						<label for="icon" class="control-label">Icon URL</label>
						[
						<span class="text-danger"><a href="<?php echo site_url('gallery/add');?>" target="_blank">
							<?php echo GALLERY_URL_LABEL;?></a>
						</span>
						]&nbsp;
						[
						<span class="text-danger"><a href="<?php echo site_url('gallery/index');?>" target="_blank">
							<?php echo GALLERY_URL_LABEL_LIST;?></a>
						</span>
						]
						<div class="form-group has-feedback">
							<input type="url" name="icon" value="<?php echo ($this->input->post('icon') ? $this->input->post('icon') : $category_master['icon']); ?>" class="form-control" id="icon" />
							<span class="glyphicon glyphicon-link form-control-feedback"></span>
							<span class="text-danger"><?php echo form_error('icon');?></span>
						</div>
					</div>

					

					<!-- <?php
						if($category_master['test_module_id']==0){
							$ee= "checked";
						}else{
							$ee='';
						}

						if($category_master['test_module_id']==1){
							$tt= "checked";
						}else{
							$tt='';
						}

						if($category_master['test_module_id']==2){
							$pte= "checked";
						}else{
							$pte='';
						}
					?>

					<div class="col-md-12">
						<div class="form-group">
							<label for="active" class="control-label">Category for</label>
							<?php 
								$ee  = array('id'=>'0','name'=>'test_module_id', 'value'=>'0', 'checked'=>$ee);
								$tt  = array('id'=>'1','name'=>'test_module_id', 'value'=>'1', 'checked'=>$tt);
								$pte = array('id'=>'2','name'=>'test_module_id', 'value'=>'2', 'checked'=>$pte);    
								echo form_radio($ee)." English essentials".' '.form_radio($tt)." IELTS".' '.form_radio($pte)." PTE";
								?>
								<span class="text-danger"><?php echo form_error('test_module_id');?></span>
						</div>
						
					</div> -->

					<div class="col-md-4">
						<label for="test_module_id" class="control-label"><span class="text-danger">*</span>Test module</label>
						<div class="form-group">
							<select name="test_module_id" class="form-control selectpicker" data-show-subtext="true" data-live-search="true">
								<option data-subtext="" value="">Select </option>
								<?php 
								foreach($all_test_module as $t)
								{
									$selected = ($t['test_module_id'] == $category_master['test_module_id']) ? ' selected="selected"' : "";

									echo '<option data-subtext="'.$t['test_module_name'].'" value="'.$t['test_module_id'].'" '.$selected.'>'.$t['test_module_name'].'</option>';
								} 
								?>
							</select>
							<span class="text-danger"><?php echo form_error('test_module_id');?></span>
						</div>
					</div>	
					
					<div class="col-md-12">
						<div class="form-group">
							<input type="checkbox" name="active" value="1" <?php echo ($category_master['active']==1 ? 'checked="checked"' : ''); ?> id='active' />
							<label for="active" class="control-label">Active</label>
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
            	<button type="submit" class="btn btn-success">
					<i class="fa fa-check"></i> Save
				</button>
	        </div>				
			<?php echo form_close(); ?>
		</div>
    </div>
</div>