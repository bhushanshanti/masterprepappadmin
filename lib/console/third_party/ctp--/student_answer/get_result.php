<style type="text/css">
.spn{
    color: #000;
    font-weight: bold;
}
</style>
<?php foreach($get_result as $a){ 
                
    $collection_no =    $a['collection_no']; 
    $max_marks =        $a['max_marks'];
    if(!isset($max_marks) or empty($max_marks)) {
        $max_marks='null';  
    }
    $paper_duration=$a['paper_duration']; 
    $test_seriese_name=$a['test_seriese_name'];
    $test_seriese_type=$a['test_seriese_type'];
    $test_module_name=$a['test_module_name'];
                
    $category_name=$a['category_name'];
    $band_score=$a['band_score'];
    if(!isset($band_score) or empty($band_score)) {
        $band_score='null';  
    }
    $paper_checked=$a['paper_checked'];
}
?>
<div class="row">
    <div class="col-md-12">
        <div class="box">

            <?php 
            foreach($get_students as $gs){ 
                if($gs['gender']=='' or $gs['gender']==NULL){
                    $gs['gender']='null';
                }
                if($gs['email']=='' or $gs['email']==NULL){
                    $gs['email']='null';
                }
                if($gs['residential_address']=='' or $gs['residential_address']==NULL){
                    $gs['residential_address']='null';
                }

                $student_id  = $gs['id'];
                $programe_id =$gs['programe_id'];
            ?>
            
            <div class="box-header pull-left col-md-6">                
                <span class="spn">Name: </span><?php echo $gs['fname'].' '.$gs['lname']; ?><br/>
                <span class="spn">Contact no: </span> <?php echo $gs['mobile'].SEP;?>
                <span class="spn">Email id: </span> <a href="mailto:<?php echo $gs['email'];?>"><?php echo $gs['email'];?></a><br/>
                <span class="spn">Gender: </span> <?php echo $gs['gender_name'].SEP;?> 
                <span class="spn">Address: </span> <?php echo $gs['residential_address'];?><br/>
                <span class="spn">Program: </span><?php echo $gs['programe_name'];?>
            </div>
            <?php } ?>  

            <div class="box-header pull-right col-md-6">
                <?php 
                    
                    $m = $get_sum['total'];                    
                    
                    if(!isset($m) or empty($m)) {
                    $m='null';  
                    }                 
                ?>
                <span class="spn">Test Name, type & category: </span> <?php echo $test_seriese_name.SEP.$test_seriese_type.SEP.$category_name.SEP.$test_module_name;?><br/>                
                <span class="spn">Test attempt id: </span> <?php echo $collection_no;?><br/> 
                <span class="spn">M.M.:  </span> <?php echo $max_marks;?>
                <span class="spn">Duration: </span> <?php echo $paper_duration.''.PAPER_DURATION_UNIT;?><br/>
                <span class="spn">Total marks obtained: </span> <?php echo $m.'/'.$max_marks;?><br/>
                <span class="spn">Band score: </span> <?php echo $band_score;?><br/>
                <span class="spn">Submission date:</span>
                <?php 
                    $date=date_create($a['created']);
                    echo $created = date_format($date,"M d, Y | H:i:s");
                ?><br/>
                <span class="spn">Result date:</span>
                <?php 
                    $date=date_create($a['modified']);
                    echo $modified = date_format($date,"M d, Y | H:i:s");
                ?>
            </div> 

            <div class="box-body">
                <!-- <input class="btn btn-info btn-xs" type="button" value="Print result" 
               onclick="printx()" data-toggle="tooltip" title="Print full result"/> -->
               <?php 
                  $test_seriese_id=$ts['test_seriese_id'];
                  $ts_cat_assoc_id=$ts['ts_cat_assoc_id'];
                  $url = site_url()."paper/view/".$test_seriese_id.'/'.$ts_cat_assoc_id;
               ?>
                <a href='<?php echo $url;?>' target="_blank">
                    <input class="btn btn-success btn-xs" data-toggle="tooltip" title="View full paper" type="button" value="View paper"/>
                </a>
                <a href='javascript:void(0);'>
                    <input class="btn btn-info btn-xs" data-toggle="tooltip" title="Re check paper" type="button" value="Re Check" onclick ="recheck_paper('<?php echo $collection_no;?>')" />
                </a>

                <table class="table table-striped table-bordered table-sm">
                    <thead>
                    <tr>
						<th>Qn.</th>	
						<th>Section</th>
                        <th>Question set</th>
                        <th>Set paragraph</th>	
                        <th>Question</th>                        
                        <th>Answer by student</th>                      
                        <th>Correct answer</th>
                        <th>Answer explaination</th>
                        <?php 
                            if($category_name=="Reading" or $category_name=="Listening"){ 
                        ?>
                        <th>Marks secured</th>
                        <?php }elseif($paper_checked==NULL){ ?>	
                        <th>Check & Assign Marks<?php echo SEP;?>Band score</th>
                        <?php }else{ ?>	
                        <th>Checked</th>
                        <?php }?>		
						
                    </tr></thead>
                    <?php 
                        $sr=0; foreach($get_result as $a){ $sr++;
                    ?>
                    <tr>						
                        <td><?php echo $a['question_no'];?></td>						 
						<td><?php echo $a['section_heading']; ?></td>
                        <td><?php echo $a['question_sets_heading']; ?></td>
                        <td><?php echo $a['question_sets_para']; ?></td>
                        <td style="text-align: justify;"><?php echo $a['question']; ?></td>                           
                        <td style="text-align: justify;">
                            <?php 
                                if($a['is_img']==0){
                                    echo $a['student_answers']; 
                                }elseif($a['is_img']==1){
                                    echo '<a href="'.$a["student_answers"].'" target="'.TARGET_B.'">'.OPEN_FILE.'</a>';
                                }else{
                                    echo NULL;
                                }
                            ?>                            
                        </td> 
                        <td style="text-align: justify;"><?php echo $a['correct_answer']; ?></td>
                        <td style="text-align: justify;"><?php echo $a['correct_answer_explaination']; ?></td>

                        <?php if($category_name==READING or $category_name==LISTENING or $category_name==ENGLISH_ESSENTIAL){ ?>
                        <td><?php echo $a['marks_secured']; ?></td>						
						<?php }elseif($paper_checked==NULL){ ?>
                            <td>
                                <select class="form-control myinput" name="<?php echo $a['student_answers_id'];?>" id="marks_secured[]" style="width:130px;" onchange='update_marks(this.name,this.value)'>
                                    <?php 
                                        for ($x = 0; $x <= 9; $x+= 0.5 ){ 
                                        if($x==$a['marks_secured']){
                                            $selected='selected';
                                        }else{
                                            $selected='';
                                        }
                                    ?>
                                    <option value="<?php echo $x;?>" <?php echo $selected;?>>
                                    <?php echo $x;?>                           
                                    </option>
                                    <?php } ?>
                                </select>

                                <textarea placeholder="Enter Remarks" class="form-control" name="<?php echo $a['student_answers_id'].'cr';?>" id="checker_remarks[]" onblur='update_remarks(this.name,this.value)'><?php echo $a['checker_remarks'];?></textarea>
                            </td>
                            
                        <?php }else{ ?>
                            <td><input type="text" class="form-control myinput" value="<?php echo $a['marks_secured']; ?>" name="<?php echo $a['student_answers_id'];?>" id="marks_secured[]" style="width:73px;" maxlength="2" onblur='update_marks(this.name,this.value)' disabled>
                                 <textarea placeholder="Enter Remarks" class="form-control" name="<?php echo $a['student_answers_id'].'cr';?>" id="checker_remarks[]" onblur='update_remarks(this.name,this.value)' disabled><?php echo $a['checker_remarks'];?></textarea>
                            </td>
                        <?php } ?>
                        <td><input type="hidden" class="form-control" value="<?php echo $collection_no; ?>" name="collection_no" id="collection_no"></td>
						
                    </tr>
                    <?php } ?>
                </table>
            
            </div>

                <!--  <div class="pull-right">
                    <?php echo $this->pagination->create_links(); ?>
                </div>  -->
                <?php if($paper_checked==NULL and ($category_name=="Speaking" or $category_name=="Writing")){ ?>
                <div class="pull-right">
                    <button type="button" class="btn btn-success btn-xs" name="<?php echo $collection_no;?>" id="" data-toggle="tooltip" title="Finally Update Marks" onClick="update_final_marks(this.name,'<?php echo $category_name;?>','<?php echo $test_seriese_id;?>', '<?php echo $ts_cat_assoc_id;?>','<?php echo $max_marks;?>','<?php echo $student_id ?>','<?php echo $programe_id ?>');"> Update</span>
                    </button>
                </div>
            <?php } ?>                         
            </div>
            <span id="msg"></span>
        </div>
    </div>
</div>
