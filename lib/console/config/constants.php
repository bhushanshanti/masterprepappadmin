<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');


defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0);// no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1);// generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3);// configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4);// file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5);// unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6);// unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7);// invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8);// database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9);//lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

//file paths
define('AUDIO_FILE_PATH', './uploads/paper_audio/');
define('SECTION_IMAGE_PATH', './uploads/');
define('QS_IMAGE_PATH', './uploads/question_set_images/');
define('ICON_IMAGE_PATH', './uploads/icon/');
define('LIVE_LECTURE_IMAGE_PATH', './uploads/live_lecture/');
define('STATIC_PAGE_IMAGE_PATH', './uploads/static_page/');
define('GALLERY_IMAGE_PATH', './uploads/gallery/');
define('ANSWER_RECORD_FILE_PATH', './uploads/answer_record/');
define('ANSWER_IMAGE_FILE_PATH', './uploads/answer_image/');
define('PROFILE_PIC_FILE_PATH', './uploads/profile_pic/');

//allowed file types
define('GALLERY_ALLOWED_TYPES', 'gif|jpg|png|jpeg|mp4|mp3|svg|ico');
define('CATEGORY_ALLOWED_TYPES','gif|jpg|png|jpeg');
define('LIVE_LECTURE_ALLOWED_TYPES', 'gif|jpg|png|jpeg');
define('QUESTTION_SET_ALLOWED_TYPES', 'gif|jpg|png|jpeg');
define('SECTION_ALLOWED_TYPES', 'gif|jpg|png|jpeg');
define('STATIC_PAGE_ALLOWED_TYPES', 'gif|jpg|png|jpeg|mp4');
define('TS_CAT_ASSOC_ALLOWED_TYPES', 'mp3');

//file type label
define('GALLERY_ALLOWED_TYPES_LABEL', '<span class="text-info"> [  <i><b>Allowed File:</b>  gif, jpg, png, jpeg, mp4, mp3 </i>  ] </span>');
define('CATEGORY_ALLOWED_TYPES_LABEL','<span class="text-info"> [  <i><b>Allowed File:</b>  gif, jpg, png, jpeg </i>  ] </span>');
define('LIVE_LECTURE_ALLOWED_TYPES_LABEL', '<span class="text-info"> [  <i><b>Allowed File:</b>  gif, jpg, png, jpeg </i>  ] </span>');
define('QUESTTION_SET_ALLOWED_TYPES_LABEL', '<span class="text-info"> [  <i><b>Allowed File:</b>  gif, jpg, png, jpeg </i>  ] </span>');
define('SECTION_ALLOWED_TYPES_LABEL', '<span class="text-info"> [  <i><b>Allowed File:</b>  gif, jpg, png, jpeg </i>  ] </span>');
define('STATIC_PAGE_ALLOWED_TYPES_LABEL', '<span class="text-info"> [  <i><b>Allowed File:</b>  gif, jpg, png, jpeg, mp4 </i>  ] </span>');
define('TS_CAT_ASSOC_ALLOWED_TYPES_LABEL', '<span class="text-info"> [  <i><b>Allowed File:</b>  mp3 </i>  ] </span>');

define('GALLERY_URL_LABEL','Add');
define('GALLERY_URL_LABEL_LIST','List');

//result msg
define("RESULT_SUC_MSG_RL", 'Dear Student, your paper submitted successfully.');
define("RESULT_SUC_MSG_RLWS_SP", 'Dear Student, your SP practice paper submitted successfully.');
define('RESULT_SUC_MSG_WS', 'Dear Student, your paper submitted successfully. Result will be declared within 2 working days.');
define('PAPER_SUBMISSION_FAILED', 'Your paper submission failed! Please re-submit it again');
define('PAPER_TYPE_ERROR', 'Test paper error.Try again!');
define('UNAUTHORIZED', 'Not Authorized OR Token is missing');
define('UNAUTHORIZED_LOGIN', 'Invalid Login Details!');
define('DELETE_CONFIRM', 'Are you sure you want to delete this item?');
define('TS_EE_CB_LABEL', '<span class="text-info"> [ <i>Keep checked for test series and unchecked for English essentials</i> ]</span>');

//Tests all
define('IELTS', 'IELTS');
define('PTE', 'PTE');
define('TOEFL', 'TOEFL');
define('GMAT', 'GMAT');
define('ENGLISH_ESSENTIAL','English Essentials');
define('ET', 'Evaluation Test');
define('MOCK', 'Mock Test');

//marks
define('MARKS_READ', 1);
define('MARKS_LISTEN', 1);
define('MARKS_WRITE', NULL);
define('MARKS_SPEAK', NULL);
define('MARKS_WRITE_SP', 1);
define('MARKS_SPEAK_SP', 1);
define('MARKS_EE', 1);
define('MARKS_ET', 1);
define('MARKS_NULL', NULL);
define('MARKS_ZERO', 0);

//category names IELTS
define('READING',      'Reading');
define('LISTENING',    'Listening');
define('WRITING',      'Writing');
define('SPEAKING',     'Speaking');

//category names EE
define('EX1', 'Exercise 1');
define('EX2', 'Exercise 2');
define('EX3', 'Exercise 3');
define('EX4', 'Exercise 4');
define('EX5', 'Exercise 5');
define('EX6', 'Exercise 6');
define('EX7', 'Exercise 7');
define('EX8', 'Exercise 8');
define('EX9', 'Exercise 9');
define('EX10','Exercise 10');

 //EE static pages category
define('POS', 'Parts of Speech');
define('TENSE', 'Tenses');
define('DI', 'Direct/Indirect Speech');
define('AP', 'Active and Passive Voice');

//Programs
define('ACD','Academic');
define('GT','General Training');

//titles
define('MASTERPREP', 'Team Masterprep');
define('COMPANY', 'Masterprep Education Ltd.');
define('PAYDESC', 'Pay with Debit, Credit, UPI, Wallet & Net banking');
define('CURRENCY', 'INR');
//dates
$today=date('Y-m-d');
define('TODAY', $today);
$cyear  = date("y");
$cmonth = date("M");

//Prefixes

define('PREFIX_IELTS', 'IELTS-'.$cmonth.$cyear.'-');
define('PREFIX_IELTS_SP', 'SPI-'.$cmonth.$cyear.'-');
define('PREFIX_PTE', 'PTE-'.$cmonth.$cyear.'-');
define('PREFIX_TOEFL', 'TOEFL-'.$cmonth.$cyear.'-');
define('PREFIX_GMAT', 'GMAT-'.$cmonth.$cyear.'-');
define('PREFIX_EE', 'EE-'.$cmonth.$cyear);
define('PREFIX_MT', 'MOCK-'.$cmonth.$cyear.'-');
define('PREFIX_ET', 'ET-'.$cmonth.$cyear);

//seperator
define('SEP', '<span class="text-warning"> | </span>');

//Paginations
define('RECORDS_PER_PAGE', 60);
define('RECOMMENDED_LL_LIMIT', 6);
define('NOTIFICATION_LIMIT', 30);
define('COOKIE_EXPIRY', 86400);

define('ACD_ID', 11);
define('GT_ID', 10);

define('IELTS_ID', 1);
define('PTE_ID', 2);
define('TOEFL_ID', 3);
define('GMAT_ID', 4);
define('EE_ID', 5);

//Units
define('PAPER_DURATION_UNIT', 'Min.');
define('PERCENT','%');

//login/reg
define('INVALID_LOGIN', 'Invalid Login Details!');
define('VALID_LOGIN', 'Loggedin Successfully.');
define('INCOMPLETE_TESTS', 'Your Below Listed Test(s) are Incomplete, Please complete it first for overall band score.');

//Link targets
define('TARGET_B', '_blank');
define('TARGET_S', '_self');

//Labels
define('STATUS', '<span class="text-dark" title="Status"><i class="fa fa-check"></i></span>');
define('ACTION', 'Action');
define('SR',     'Sr.');
define('TS_EE','Test');
define('BG', 'bg-gray');

define('ITEM_NOT_EXIST', 'The item you are trying for does not exist.');

define('OPEN_FILE', 'Open File');
define('NO_FILE',   '<span class="text-info">No File</span>');
define('NO_ICON',   '<span class="text-info">No Icon</span>');
define('NA',        '<span class="text-info">N/A</span>');
define('NILL',      '<span class="text-info">NILL</span>');
define('NO_SECTION', '<strike>No Section</strike>');
define('NO_ASSOCIATION', '<span class="text-info"><strike>No Association</strike></span>');
define('NO_ASSOCIATION_INST', 'This test is not associated with any category click to associate');
define('NO_SECTION_INST', 'There is no section for this Test. First associate (if not) and then add new sections');

define('ACTIVE',    '<span class="text-success"><i class="fa fa-check"></i></span>');
define('DEACTIVE',  '<span class="text-danger"><i class="fa fa-close"></i></span>');
define('NOSET',  '<span class="text-danger">No Set</span>');
define('BLANK_QUESTION',  '<span class="text-danger">SEE QUESTION IN QUESTION-SET OR PARA</span>');

//COMMOM MSG
define('PWD_CHANGE_SUCCESS_MSG', '<div class="alert alert-success alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>SUCCESS:</strong> Your password changed successfully.<a href="#" class="alert-link"></a>.
                </div>');

define('PWD_CHANGE_FAILED_MSG', '<div class="alert alert-danger alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>FAILED:</strong> Your password change failed? Try again!.<a href="#" class="alert-link"></a>.
                </div>');


define('SUCCESS_MSG','<div class="alert alert-success alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>SUCCESS:</strong> Saved successfully.<a href="#" class="alert-link"></a>.
                </div>');

define('ADMIN_LOGIN_ERR_MSG','<div class="alert alert-danger alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>ERROR:</strong> username or password is wrong! Please try again!<a href="#" class="alert-link"></a>.
                </div>');

define('ADMIN_LOGIN_SUC_MSG','<div class="alert alert-success alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>SUCCESS:</strong> Redirecting ! Please wait!<a href="#" class="alert-link"></a>.
                </div>');

define('UPDATE_MSG','<div class="alert alert-success alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>SUCCESS:</strong> Updated successfully.<a href="#" class="alert-link"></a>.
                </div>');

define('UPDATE_FAILED_MSG','<div class="alert alert-danger alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>UPDATION FAILED:</strong> Failed to update! Try again.<a href="#" class="alert-link"></a>.
                </div>');

define('FAILED_MSG','<div class="alert alert-danger alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>FAILED:</strong> Failed to save! Try again.<a href="#" class="alert-link"></a>.
                </div>');

define('DEL_MSG','<div class="alert alert-success alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>SUCCESS:</strong> Deleted successfully<a href="#" class="alert-link"></a>
                    </div>');

define('DEL_MSG_FAILED','<div class="alert alert-danger alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>SUCCESS:</strong> Delete FAILED<a href="#" class="alert-link"></a>
                    </div>');

define('DUP_MSG','<div class="alert alert-warning alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>DUPLICATE:</strong> Already exist, please try different!<a href="#" class="alert-link"></a>.
                </div>');

define('TS_SUCCESS_MSG','<div class="alert alert-success alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>SUCCESS:</strong> Test added successfully, Now associate to one OR multiple categories, one by one <a href="#" class="alert-link"></a>
                    </div>');

define('SECTION_ADD_MSG','<div class="alert alert-success alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>SUCCESS:</strong> Section added successfully<a href="#" class="alert-link"></a>
                    </div>');

define('SECTION_ADD_MSG_FAILED','<div class="alert alert-danger alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>FAILED:</strong> Section not added Try again!<a href="#" class="alert-link"></a>
                    </div>');

define('SECTION_DEL_MSG','<div class="alert alert-success alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>SUCCESS:</strong> Section deleted successfully<a href="#" class="alert-link"></a>
                    </div>');

define('SECTION_EDIT_MSG','<div class="alert alert-success alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>SUCCESS:</strong> Section updated successfully<a href="#" class="alert-link"></a>
                    </div>');

define('TS_DEL_MSG','<div class="alert alert-success alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>SUCCESS:</strong> Test deleted successfully<a href="#" class="alert-link"></a>
                    </div>');

define('TS_ASSOC_DEL_MSG','<div class="alert alert-success alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>SUCCESS:</strong> Test association deleted successfully but Test may exist with other or null association<a href="#" class="alert-link"></a>
                    </div>');

define('TS_EDIT_MSG','<div class="alert alert-success alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>SUCCESS:</strong> Test Series updated successfully<a href="#" class="alert-link"></a>
                    </div>');

define('QS_ADD_MSG', 'Question set added successfully');
define('QS_ADD_MSG_FAILED', 'Question set not added! try again');
define('QS_EDIT_MSG', '<div class="alert alert-success alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>SUCCESS:</strong> Question set updated successfully<a href="#" class="alert-link"></a>
                    </div>');

define('QS_DEL_MSG', '<div class="alert alert-success alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>SUCCESS:</strong> Question set deleted successfully<a href="#" class="alert-link"></a>
                    </div>');

define('QUESTION_ADD_MSG', '<div class="alert alert-success alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>SUCCESS:</strong> Question added successfully<a href="#" class="alert-link"></a>
                    </div>');

define('QUESTION_ADD_FAILED_MSG', '<div class="alert alert-danger alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>SUCCESS:</strong> Question addition Failed. try again!<a href="#" class="alert-link"></a>
                    </div>');

define('QUESTION_UPDATE_MSG', '<div class="alert alert-success alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>UPDATE:</strong> Question updated successfully<a href="#" class="alert-link"></a>
                    </div>');


define('FILE_SUCCESS_MSG', '<h5 class="text-success"><strong><i>File uploaded successfully</strong></i></h5>');

define('FILE_FAILED_MSG', '<h5 class="text-warning"><strong><i>File upload failed! please try again</strong></i></h5>');

define('MARKS_UPDATE_MSG', '<div class="alert alert-success alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>SUCCESS:</strong> Marks updated successfully<a href="#" class="alert-link"></a>
                    </div>');

define('MARKS_UPDATE_FAILED_MSG', 'Marks not updated? Please try again');

define('QS_PARA_NOTE', '<br/>NOTE-1: <span class="text-warning">Table completion, Note completion,Summary completion,Form filling, Sentence completion, Summary completion with a box</span><br/>NOTE-2: <span class="text-warning">Fill in the blanks dash should be seperated with ~~~ sign.</span><br/>NOTE-3: <span class="text-warning">Images are not allowed here. For image browse <b>Set image</b> given above.</span>');

//SMS
define('API_KEY', 'crscanam007');
define('API_SENDER', 'CRS');
define('API_URL', 'http://203.212.70.200/smpp/sendsms');
define('API_USERNAME', 'canamhtpotp');
define('API_PASSWORD', 'canm0011');
define('API_FROM', 'CANUWS');

//Notifications
define('LISTEN_SUBMISSION_NOTIFY','IELTS- Listening Test Submission');
define('READ_SUBMISSION_NOTIFY','IELTS- Reading Test Submission');
define('WRITE_SUBMISSION_NOTIFY','IELTS- Writing Test Submission');
define('SPEAK_SUBMISSION_NOTIFY','IELTS- Speaking Test Submission');
define('SP_SUBMISSION_NOTIFY','IELTS- SP-Test Submission');
define('EE_SUBMISSION_NOTIFY','English Essentials Test Submission');
define('SPEAK_RESULT_NOTIFY','IELTS- Speaking Result Prepared');
define('WRITE_RESULT_NOTIFY','IELTS- Writing Result Prepared');
define('MOCK_SUBMISSION_NOTIFY', 'Mock Test Submission');
define('IELTS_PACKAGE_SUBS_NOTIFY', 'IELTS- Package Subscription Done');
define('IELTS_PACKAGE_SUBS_FLD_NOTIFY', 'IELTS- Package Subscription Failed');
define('ET_SUBMISSION_NOTIFY','Evaluation Test Submission');
define('ENQ_NOTIFY', 'Enquiry submitted to Masterprep Team');
define('VISA_ENQ_NOTIFY', 'VISA Enquiry submitted to Masterprep Team');
define('ENQUIRY_REPLY_NOTIFY', 'Masterprep Admin replied for your query');
define('VISA_ENQUIRY_REPLY_NOTIFY', 'Masterprep Admin replied for your VISA query');
define('MOC_BOOKING_NOTIFY', 'Mock Test Booking Submission');
//enquiry
define('ENQ_SUC', 'Thanks! Your query sent successfully to Masterprep Admin. We will get back to you soon.');
define('ENQ_FLD', 'Oops! Your query submission failed! Please try again.');

//VISa enquiry
define('VISA_ENQ_SUC', 'Thanks! Your VISA query sent successfully to Masterprep Admin. We will get back to you soon.');
define('VISA_ENQ_FLD', 'Oops! Your VISA query submission failed! Please try again.');

//MOCK
define('MOCK_SUC', 'Thanks! Your Mock test booking sent successfully to Masterprep faculty. We will get back to you soon.');
define('MOCK_FLD', 'Oops! Your Mock test booking submission failed! Please try again.');

//mail ids
define('FROM_EMAIL','info@masterprepapp.in');
define('FROM_NAME','Masterprep Education Ltd.');
define('ADMIN_EMAIL_CC','support4.it@canamgroup.com');
define('ADMIN_EMAIL_BCC','support4.it@canamgroup.com');
define('ENQUIRY_ADMIN','support4.it@canamgroup.com');
define('PAYMENT_INFO_ADMIN', 'marketing.chd@masterprep.in');

//mail subjects
define('PAYMENT_FLD', 'Masterprep- Payment Failed');
define('PAYMENT_SUC', 'Masterprep- Payment Done Successfully');
define('PAYMENT_UT', 'Unauthorized Transaction!');
define('PACK_EXPIRY', 'Package Expiry Alert');
define('TEST_PAPER_CHECKED', 'Test Paper evaluated by the examiner');

//social medias
define('FB',  'https://www.facebook.com/canamconsultants/');
define('YTD', 'https://www.youtube.com/c/canamconsultants');
define('GP',  'https://plus.google.com/109696843254232501595');
define('INST','https://www.instagram.com/canam.consultants/');
define('TWT', 'https://twitter.com/canamconsultant');
define('LI',  'https://www.linkedin.com/company/canamconsultants');
define('PNT', 'https://in.pinterest.com/canamconsultant/');

//Test
//define('KEY_ID', 'rzp_test_lmaPSOblfsFcqA');
//define('KEY_SECRET', 'M9hOlGdV1DNdNKaeoJz8Y7pZ');

//live
define('KEY_ID', 'rzp_live_F4t7HonHz8WUwR');
define('KEY_SECRET', 'tCTCNaDnZYrgL7yjn7nY1AkK');
