<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Role_model');
    }

    function _is_logged_in() {
        $user = $this->session->userdata('admin_login_data');
        if (!empty($user)) {
            return true;
        }
        return false;
    }

    function _has_access($cn,$mn){

        $user = $this->session->userdata('admin_login_data');
        foreach ($user as $d){
            $role_id=$d->roleid;
            $role_name=$d->name;
        }

        if($role_name!='Admin'){

            $controller_data = $this->Role_model->check_controller($cn);
            foreach ($controller_data as $c) {
                $controller_id=$c['id'];
            }
            $method_data = $this->Role_model->check_method($mn,$controller_id);
            foreach ($method_data as $m) {
                $method_id=$m['id'];
            }
            $num = $this->Role_model->check_role_acess($role_id,$controller_id,$method_id);
                if($num<=0){
                    return false;
                }else{

                    return true;
                }
        }else{
            return true;//for admin return alway true
        }
        
    }

    function curlpostdata($url, $data_sms) {

        $objURL = curl_init($url);
        curl_setopt($objURL, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($objURL, CURLOPT_POST, 1);
        curl_setopt($objURL, CURLOPT_POSTFIELDS, $data_sms);
        $retval = trim(curl_exec($objURL));
        if ($errno = curl_errno($objURL)) {
            echo 'Curl error: ' . curl_strerror($errno);
            echo 'Curl error: ' . curl_error($objURL);
        }
        curl_close($objURL);
        return $retval;
    }

    function insert_notification($subject,$student_id,$programe_id,$test_module_id,$ts_cat_assoc_id,$collection_no,$paper_checked){
    
        $message_data = $this->Notification_model->get_notification_message($subject);
        $messege_id   = $message_data['messege_id'];
        $insert_message_data = $this->Notification_model->add_notification_message($student_id,$messege_id,$programe_id,$test_module_id,$ts_cat_assoc_id,$collection_no,$paper_checked);
    }

}
