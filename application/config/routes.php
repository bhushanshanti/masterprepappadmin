<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'login';
$route['404_override'] = '';
$route['translate_uri_dashes'] = TRUE;

//left part in url

////static page api routes
$route['api/static_page'] = 'api/static_page/index_get';
$route['api/static_page/(:num)'] = 'api/static_page/index_get/$1';
$route['api/static_page/(:num)/(:num)'] = 'api/static_page/index_get/$1/$2';

$route['api/ee/static_page_cat_ee'] = 'api/ee/static_page_cat_ee/index_get';

////instruction api routes
$route['api/instruction'] = 'api/instruction/index_get';
$route['api/instruction/(:num)'] = 'api/instruction/index_get/$1';

////live lecture
$route['api/live_lecture_cat/(:num)'] 	= 'api/live_lecture_cat/index_get/$1';
$route['api/live_lecture/(:num)'] 		= 'api/live_lecture/index_get/$1';

////programes and categiry
$route['api/programe'] = 'api/programe/index_get';
$route['api/category'] = 'api/category/index_get/$1';

$route['api/category_ee'] = 'api/category_ee/index_get';

////english essential
$route['api/ee/english_essential_assoc'] = 'api/ee/english_essential_assoc/index_get';
//$route['api/english_essential_cat/(:num)/(:num)'] = 'api/english_essential_cat/index_get/$1/$2';

////student reg
$route['api/student/create'] = 'api/student/index_post';

////otp verification
$route['api/otp/verifyOTP'] = 'api/otp/index_post';

////student login
$route['api/student_login/login'] = 'api/student_login/index_post';

////student profile
$route['api/student'] = 'api/student/index_get';
$route['api/student/(:num)'] = 'api/student/index_get/$1';


////profile update
$route['api/student/update'] = 'api/student/index_put';
$route['api/student/delete/(:num)'] = 'api/student/index_delete/$1';

////test series api routes
$route['api/test_seriese'] = 'api/test_seriese/index_get';
$route['api/test_seriese/(:num)'] = 'api/test_seriese/index_get/$1';

////test cat api route
$route['api/test_seriese_cat'] = 'api/test_seriese_cat/index_get';
$route['api/test_seriese_cat/(:num)/(:num)'] = 'api/test_seriese_cat/index_get/$1/$2';

////get paper
$route['api/paper/(:num)/(:num)'] = 'api/paper/index_get/$1/$2';

////answer post
$route['api/student_answer/(:num)'] = 'api/student_answer/index_post/$1';

////get all attempts by students
$route['MP-API-V7/student_attempts'] = 'MP-API-V7/student_answer/index_get';

////quick results
$route['api/student_quick_result/(:num)/(:num)/(:any)'] = 'api/student_quick_result/index_get/$1/$2/$3';

////deatiled result
$route['api/student_detailed_result/(:num)/(:num)/(:any)'] = 'api/student_detailed_result/index_get/$1/$2/$3';













