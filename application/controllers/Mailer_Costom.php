<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/
 
class Mailer_Costom extends MY_Controller {
    
    function __construct()
    {
        parent::__construct();        
    }

    public function sendEmailEnquiry_toStd($email,$subject,$mail_data){
         
    $this->email->to($email);
    $this->email->subject($subject);
    $body = $this->load->view('emails/enquiry_std.php',$mail_data,TRUE);
    $this->email->message($body);
    $this->email->send();
}

     


}
