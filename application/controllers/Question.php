<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/ 
class Question extends MY_Controller{
    function __construct()
    {
        parent::__construct();
       if (!$this->_is_logged_in()) {$this->session->set_userdata('lock_url', $_SERVER['REDIRECT_QUERY_STRING']); redirect('/login');}
        $this->load->model('Question_model');
        $this->load->model('Question_set_model');
        $this->load->model('Question_type_model');      
    }
    /*
     * Listing of questions
     */
    function index()
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $this->load->library('pagination');
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('question/index?');
        $config['total_rows'] = $this->Question_model->get_all_questions_count();
        $this->pagination->initialize($config);
        $data['title'] = 'Question';
        $data['questions'] = $this->Question_model->get_all_questions($params);        
        $data['_view'] = 'question/index';
        $this->load->view('layouts/main',$data);
    }    
    /*
     * Adding a new question
     */
    function add(){             
        
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $dataq=array(
            'total_questions'       => trim($this->input->post('total_questions', true)),
            'question_no'           => trim($this->input->post('question_nos', true)),
            'question_sets_id'      => $this->input->post('question_sets_id', true),
            'question_set'          => trim($this->input->post('questions', true)),
            'correct_answer_set'    => trim($this->input->post('correct_answers', true)),
            'correct_answer_explaination_set'=> trim($this->input->post('correct_answer_explainations', true)),
        );
        $ca = $dataq['correct_answer_set'];
        //for remove more than one space within string -if there
        $output = preg_replace('!\s+!', ' ', $ca);
        //for add space after each comma -if not
        $x = preg_replace('/\s*,\s*/',', ', trim($output));


            $optq = array('opt' => $this->input->post('opt', true));
            $user = $this->session->userdata('admin_login_data');
            foreach ($user as $d){$by_user=$d->id;}
            $params=array(
                'question_sets_id'  => $dataq['question_sets_id'],
                'question_no'       => $dataq['question_no'],
                'question'          => $dataq['question_set'],
                'correct_answer'    => $x,
                'correct_answer_explaination' => $dataq['correct_answer_explaination_set'],
                'by_user'           => $by_user,
            ); 

            $opt_string = trim($optq['opt']);
            $opt_array = explode("~~",$opt_string);
            $id = $this->Question_model->add_question($params);
            if($id){ 
                 $key=65;
                foreach ($opt_array as $optval) {

                    $optkey= chr($key);
                    $params2 = array('question_id'=>$id,'option_key'=>$optkey,'option_value'=>$optval);
                    if($optval){
                        $op_id = $this->Question_model->add_question_options($params2);
                    }                    
                    $key++;
                 } 
                
                header('Content-Type: application/json');
                $response = ['msg'=>QUESTION_ADD_MSG, 'status'=>'true'];
                echo json_encode($response);
            }else{  
                header('Content-Type: application/json');
                $response = ['msg'=>QUESTION_ADD_FAILED_MSG, 'status'=>'false'];
                echo json_encode($response);
            }            
    }
    /*
     * Editing a question
     */
    function edit($question_id)
    {  
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $data['title'] = 'Edit question';
        $data['question'] = $this->Question_model->get_question($question_id);
        $data['question_options'] = $this->Question_model->get_question_options($question_id);
        //echo '<pre>';
        //print_r($data['question_options']);die;
        $data['qt_id'] = $this->Question_set_model->get_question_type_id($data['question']['question_sets_id']);        
        $data['qt_name'] = $this->Question_type_model->get_question_type($data['qt_id']['question_type_id']);
        
        if(isset($data['question']['question_id']))
        {
            $this->load->library('form_validation');
			$this->form_validation->set_rules('question_no','Question no','required|trim');
            $this->form_validation->set_rules('question','Question','trim');
			$this->form_validation->set_rules('question_sets_id','Question set','required');
			$this->form_validation->set_rules('correct_answer','Correct answer','required|trim');
            $this->form_validation->set_rules('correct_answer_explaination','Correct answer explaination','trim');
		
			if($this->form_validation->run())     
            {   
                $user = $this->session->userdata('admin_login_data');
                foreach ($user as $d){$by_user=$d->id;}
                $params = array(
					'active' => $this->input->post('active'),
					'question_sets_id' => $this->input->post('question_sets_id'),
                    'question_no' => trim($this->input->post('question_no')),
					'question' => trim($this->input->post('question')),
					'correct_answer' => trim($this->input->post('correct_answer')),
                    'correct_answer_explaination'=> trim($this->input->post('correct_answer_explaination')),
                    'by_user' => $by_user,
                );
                $ca = $params['correct_answer'];
                //for remove more than one space within string -if there
                $output = preg_replace('!\s+!', ' ', $ca);
                //for add space after each comma -if not
                $x = preg_replace('/\s*,\s*/',', ', trim($output));
                $params = array(
                    'active' => $this->input->post('active'),
                    'question_sets_id' => $this->input->post('question_sets_id'),
                    'question_no' => trim($this->input->post('question_no')),
                    'question' => trim($this->input->post('question')),
                    'correct_answer' => $x,
                    'correct_answer_explaination'=> trim($this->input->post('correct_answer_explaination')),
                    'by_user' => $by_user,
                );

                $id = $this->Question_model->update_question($question_id,$params); 
                if($id){

                    $this->Question_model->delete_question_options($question_id);

                    $opt_string = trim($this->input->post('opt'));
                    $opt_array = explode("~~",$opt_string);
                    $key=65;
                    foreach ($opt_array as $optval) {

                        $optkey= chr($key);
                        $params2 = array('question_id'=>$question_id,'option_key'=>$optkey,'option_value'=>$optval);
                        if($optval){
                            $op_id = $this->Question_model->add_question_options($params2);
                        }                    
                        $key++;
                     }
                    $this->session->set_flashdata('flsh_msg', QUESTION_UPDATE_MSG);           
                    redirect('test_seriese/index');
                }else{
                    $this->session->set_flashdata('flsh_msg', UPDATE_FAILED_MSG);           
                    redirect('question/edit/'.$question_id);
                } 
            }
            else
            {
				$this->load->model('Question_set_model');
				$data['all_question_sets'] = $this->Question_set_model->get_all_question_sets();
                $data['_view'] = 'question/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error(ITEM_NOT_EXIST);
    }
    /*
     * Deleting question
     */
    function remove($question_id)
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $question = $this->Question_model->get_question($question_id);
        if(isset($question['question_id']))
        {
            $this->Question_model->delete_question($question_id);
            $this->session->set_flashdata('flsh_msg', DEL_MSG);
            redirect('question/index');
        }
        else
            show_error(ITEM_NOT_EXIST);
    }
    /*
     * Set questions
     */
    function get_question(){
        
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $question_sets_id = $this->input->post('question_sets_id', true);
        $data = $this->Question_model->get_all_questions2($question_sets_id);
        echo json_encode($data);
    }
    
}
