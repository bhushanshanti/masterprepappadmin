<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/
 
class App_slides extends MY_Controller{
    
    function __construct()
    {
        parent::__construct();
       if (!$this->_is_logged_in()) {$this->session->set_userdata('lock_url', $_SERVER['REDIRECT_QUERY_STRING']); redirect('/login');}
        $this->load->model('App_slides_model');        
    }
    /*
     * Listing of App slides
     */
    function index()
    {
        
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $this->load->library('pagination');
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('app_slides/index?');
        $config['total_rows'] = $this->App_slides_model->get_all_app_slides_count();
        $this->pagination->initialize($config);
        $data['title'] = 'App slides';
        $data['app_slides'] = $this->App_slides_model->get_all_app_slides($params);        
        $data['_view'] = 'app_slides/index';

        $this->load->view('layouts/main',$data);
    }
    /*
     * Adding a new App slides
     */
    function add()
    {   
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $this->load->library('form_validation');
        $this->form_validation->set_rules('title','Title','required|trim');
        $data['title'] = 'Add Slides';
        if($this->form_validation->run())     
        {   
            $user = $this->session->userdata('admin_login_data');
            foreach ($user as $d){$by_user=$d->id;}
            $params = array(
                'active' => $this->input->post('active'),
                'image'  => $this->input->post('image'),
                'title'  => $this->input->post('title'),
                'contents'  => $this->input->post('contents'),
                'by_user' => $by_user,
            );



            $config['upload_path']   = APPSLIDE_IMAGE_PATH;
            $config['allowed_types'] = SLIDES_ALLOWED_TYPES;
            $config['encrypt_name']  = FALSE;         
            $this->load->library('upload',$config);

                if($this->upload->do_upload("image")){
                    $data = array('upload_data' => $this->upload->data());
                    $image= $data['upload_data']['file_name'];
                    $params = array(
                        'active' => $this->input->post('active'),
                        'slide_image' => site_url(APPSLIDE_IMAGE_PATH.$image),
                        'slide_title' => $this->input->post('title'),
                        'slide_content' => $this->input->post('contents'),                        
                        'by_user' => $by_user,
                    );
                    $id = $this->App_slides_model->add_app_slides($params);
                    if($id){
                        $this->session->set_flashdata('flsh_msg', SUCCESS_MSG);
                        redirect('app_slides/index');
                    }else{
                        $this->session->set_flashdata('flsh_msg', FAILED_MSG);
                        redirect('app_slides/add');
                    }                    
                    
                }else{
                    $params = array(
                        'active' => $this->input->post('active'),
                        'title' => $this->input->post('title'),
                        'by_user' => $by_user,
                    );
                    $this->session->set_flashdata('flsh_msg', FAILED_MSG);
                    redirect('app_slides/add');
                } 
        }
        else
        {            
            $data['_view'] = 'app_slides/add';
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a App slides
     */
    function edit($id)
    { 
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $data['app_slides'] = $this->App_slides_model->get_app_slides($id);
        $data['title'] = 'Edit App Slide';
        
        if(isset($data['app_slides']['id']))
        {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('title','Title','required|trim'); 
            if($this->form_validation->run())     
            {   
                $user = $this->session->userdata('admin_login_data');
                foreach ($user as $d){$by_user=$d->id;}
                $params = array(
                    'active' => $this->input->post('active'),                       
                        'slide_title' => $this->input->post('title'),
                        'slide_content' => $this->input->post('contents'),                        
                        'by_user' => $by_user,
                );
                $config['upload_path']   = APPSLIDE_IMAGE_PATH;
                $config['allowed_types'] = SLIDES_ALLOWED_TYPES;
                $config['encrypt_name']  = FALSE;         
                $this->load->library('upload',$config);

                if($this->upload->do_upload("image")){
                    $data = array('upload_data' => $this->upload->data());
                    $image= $data['upload_data']['file_name'];
                    $params = array(
                        'active' => $this->input->post('active'),
                        'slide_image' => site_url(APPSLIDE_IMAGE_PATH.$image),
                        'slide_title' => $this->input->post('title'),
                        'slide_content' => $this->input->post('contents'),                        
                        'by_user' => $by_user,
                    );                    
                }else{
                    $params = array(
                        'active' => $this->input->post('active'),                        
                        'slide_title' => $this->input->post('title'),
                        'slide_content' => $this->input->post('contents'),                        
                        'by_user' => $by_user,
                    );
                }
                $idd = $this->App_slides_model->update_app_slides($id,$params);
                if($idd){
                    $this->session->set_flashdata('flsh_msg', UPDATE_MSG);           
                    redirect('app_slides/index');
                }else{
                    $this->session->set_flashdata('flsh_msg', UPDATE_FAILED_MSG);           
                    redirect('app_slides/edit/'.$id);
                }                
            }
            else
            {
                $data['_view'] = 'app_slides/edit';                

                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error(ITEM_NOT_EXIST);
    } 

    /*
     * Deleting App slides
     */
    function remove($id){
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $app_slides = $this->App_slides_model->get_app_slides($id);
        if(isset($app_slides['id']))
        {
            $this->App_slides_model->delete_app_slides($id);
            $del_picture=$app_slides['slide_image'];
            unlink($del_picture);
            $this->session->set_flashdata('flsh_msg', DEL_MSG);
            redirect('app_slides/index');
        }
        else
            show_error(ITEM_NOT_EXIST);
    }


	function send_mobile_notification(){
        $regId = "fvx5h3osRt6Fog7UoWKoE7:APA91bEUPM_IsK2QU3H7UWI3HaKYegHzoHoC0W2ce0NzHDKFpQYinMpv8dCuPDK7AKWCyyFfpCyTTT1HgjSfHpS-2NZlRrlVNqykHkcHE0UaF1pfT8gqTqDqCtfaXF8lVwKI9gwNgCaA"; 
       
        $notification = array();
        $arrNotification= array();          
        $arrData = array();
        $arrNotification["title"] = "Masterprep 1";                                  
        $arrNotification["body"] ="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s";        
        $arrNotification["image"] ="http://localhost.masterprep.com/resources/img/favicon.png";        
        $arrNotification["name"] ="Masterprep";        
        $arrNotification["sound"] = "default";
        $arrNotification["type"] = 1;       
        $result = $this->send_mo_notification($regId, $arrNotification,"Android");

        print_r($result);
    }

}
