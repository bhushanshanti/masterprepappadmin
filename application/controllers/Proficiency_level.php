<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/ 
class Proficiency_level extends MY_Controller{
    function __construct()
    {
        parent::__construct();
        if (!$this->_is_logged_in()) {$this->session->set_userdata('lock_url', $_SERVER['REDIRECT_QUERY_STRING']); redirect('/login');}
        $this->load->model('Proficiency_level_model');       
    }
    /*
     * Listing of proficiency_level
     */
    function index()
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $this->load->library('pagination');
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('proficiency_level/index?');
        $config['total_rows'] = $this->Proficiency_level_model->get_all_proficiency_level_count();
        $this->pagination->initialize($config);
        $data['title'] = 'Proficiency level';
        $data['proficiency_level'] = $this->Proficiency_level_model->get_all_proficiency_level($params);        
        $data['_view'] = 'proficiency_level/index';
        $this->load->view('layouts/main',$data);
    }

    /*
     * Adding a new gender
     */
    function add()
    {   
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $data['title'] = 'Add Proficiency Level';
        $this->load->library('form_validation');
		$this->form_validation->set_rules('pro_name','proficiency level name','required|min_length[4]|max_length[25]|is_unique[proficiency_level.pro_name]');		
		if($this->form_validation->run())     
        {   
            $user = $this->session->userdata('admin_login_data');
            foreach ($user as $d){$by_user=$d->id;}
            $params = array(
				'active' => $this->input->post('active'),
				'pro_name' => ucfirst($this->input->post('pro_name')),
                'by_user' => $by_user,
            );            
            $id = $this->Proficiency_level_model->add_proficiency_level($params);
            if($id){
                $this->session->set_flashdata('flsh_msg', SUCCESS_MSG);
                redirect('proficiency_level/index');
            }else{
                $this->session->set_flashdata('flsh_msg', FAILED_MSG);
                redirect('proficiency_level/add');
            }            
        }
        else
        {            
            $data['_view'] = 'proficiency_level/add';
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a gender
     */
    function edit($pro_id)
    {         
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $data['title'] = 'Edit Proficiency level';
        $data['proficiency_level'] = $this->Proficiency_level_model->get_proficiency_level($pro_id);
        
        if(isset($data['proficiency_level']['pro_id']))
        {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('pro_name','proficiency level name','required|trim');		
			if($this->form_validation->run())     
            {   
                $user = $this->session->userdata('admin_login_data');
                foreach ($user as $d){$by_user=$d->id;}
                $params = array(
					'active' => $this->input->post('active'),
					'pro_name' => ucfirst($this->input->post('pro_name')),
                    'by_user' => $by_user,
                );
                $idd = $this->Proficiency_level_model->update_proficiency_level($pro_id,$params); 
                if($idd){
                    $this->session->set_flashdata('flsh_msg', UPDATE_MSG);           
                    redirect('proficiency_level/index');
                }else{
                    $this->session->set_flashdata('flsh_msg', UPDATE_FAILED_MSG);           
                    redirect('proficiency_level/edit/'.$pro_id);
                } 
            }
            else
            {
                $data['_view'] = 'proficiency_level/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error(ITEM_NOT_EXIST);
    }    
    /*
     * Deleting proficiency_level
     */
    function remove($pro_id)
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $proficiency_level = $this->Proficiency_level_model->get_proficiency_level($pro_id);
        if(isset($proficiency_level['pro_id']))
        {
            $this->Proficiency_level_model->delete_proficiency_level($pro_id);
            $this->session->set_flashdata('flsh_msg', DEL_MSG);
            redirect('proficiency_level/index');
        }
        else
            show_error(ITEM_NOT_EXIST);
    }
    
}
