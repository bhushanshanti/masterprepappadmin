<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/
 class Question_set extends MY_Controller{
    
    function __construct()
    {
        parent::__construct();
        if (!$this->_is_logged_in()) {$this->session->set_userdata('lock_url', $_SERVER['REDIRECT_QUERY_STRING']); redirect('/login');}
        $this->load->model('Question_set_model');        
    }
    /*
     * Listing of question_sets
     */
    function index()
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $this->load->library('pagination');
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('question_set/index?');
        $config['total_rows'] = $this->Question_set_model->get_all_question_sets_count();
        $this->pagination->initialize($config);
        $data['title'] = 'Question set';
        $data['question_sets'] = $this->Question_set_model->get_all_question_sets($params);        
        $data['_view'] = 'question_set/index';
        $this->load->view('layouts/main',$data);
    }

    function view(){
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $section_id = $this->input->post('section_id', true);
        $data=$this->Question_set_model->get_qs($section_id);
        echo json_encode($data);
    }

    function get_qs_para(){
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $question_sets_id = $this->input->post('question_sets_id', true);
        $data=$this->Question_set_model->get_qs_para($question_sets_id);
        echo json_encode($data);

    }

    function check_set_limit(){
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $question_sets_id = $this->input->post('question_sets_id', true);
        $data=$this->Question_set_model->get_set_limit($question_sets_id);
        echo json_encode($data);
    }
    /*
     * Adding a new question_set
     */
    function add()
    {   
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $this->load->library('form_validation');
        $this->form_validation->set_rules('question_sets_heading','Set heading','required|trim');
        $this->form_validation->set_rules('question_sets_desc','Set description','trim');
        $this->form_validation->set_rules('question_sets_para','Set paragraph','trim');
        $this->form_validation->set_rules('question_type_id','Question type','required');
		$this->form_validation->set_rules('total_questions','Total question','required|trim');
        $this->form_validation->set_rules('answer_audio_recrod_duration','Audio recrod duration','trim'); 	

		if ($this->form_validation->run() == false) {
            header('Content-Type: application/json');
            $response = ['msg'=>'<span class="text-danger">validation error!</span>', 'status'=>'false'];
            echo json_encode($response);
        }else{

            $user = $this->session->userdata('admin_login_data');
            foreach ($user as $d){$by_user=$d->id;}
            $params = array(
                'section_id'   => $this->input->post('section_id', true),
				'question_sets_heading' => $this->input->post('question_sets_heading', true),
                'question_type_id' => $this->input->post('question_type_id', true),
                'set_behavior' => $this->input->post('set_behavior', true),
				'total_questions' => $this->input->post('total_questions', true),
				'display_no_of_questions' => $this->input->post('display_no_of_questions', true),
				'question_sets_desc' => $this->input->post('question_sets_desc', true),
                'question_sets_para' => $this->input->post('question_sets_para', true),
                'question_sets_image'     => $this->input->post('question_sets_image_id', true),
                'question_sets_image' => $this->input->post('question_sets_image_id', true) ? $this->input->post('question_sets_image_id', true) : NULL,
                'answer_audio_recrod_duration'  => $this->input->post('answer_audio_recrod_duration', true),
                'active' => $this->input->post('active'),
                'by_user' => $by_user,
                
            );
            $question_set_id = $this->Question_set_model->add_question_set($params);
            if($question_set_id){ 
                header('Content-Type: application/json');               
                $response = ['msg'=>QS_ADD_MSG, 'status'=>'true'];
                echo json_encode($response);
            }else{
                header('Content-Type: application/json');
                $response = ['msg'=>QS_ADD_MSG_FAILED, 'status'=>'false'];
                echo json_encode($response);
            }
            
        }
    } 

    function do_upload_qs_image(){
        
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $config['upload_path']      = QS_IMAGE_PATH;
        $config['allowed_types']    = QUESTTION_SET_ALLOWED_TYPES;
        $config['encrypt_name']     = FALSE;         
        $this->load->library('upload',$config);

        if($this->upload->do_upload("question_sets_image")){
            $data = array('upload_data' => $this->upload->data());
            $image= $data['upload_data']['file_name'];
            echo site_url(QS_IMAGE_PATH).$image;
        }else{
            echo 'false';
        }
 
     }

     
    /*
     * Editing a question_set
     */
    function edit($question_sets_id)
    {   
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $data['title'] = 'Edit question set';
        $data['question_set'] = $this->Question_set_model->get_question_set($question_sets_id);
        
        if(isset($data['question_set']['question_sets_id']))
        {
            $this->load->library('form_validation');
            //$this->form_validation->set_rules('question_sets_heading','Set heading','required|trim');
            $this->form_validation->set_rules('question_sets_desc','Set description','trim');
            $this->form_validation->set_rules('question_sets_para','Set paragraph','trim');
            $this->form_validation->set_rules('question_type_id','Question type','required');
            $this->form_validation->set_rules('total_questions','Total question','required|trim');
            $this->form_validation->set_rules('answer_audio_recrod_duration','Audio recrod duration','trim'); 
		
			if($this->form_validation->run())     
            {   
                $user = $this->session->userdata('admin_login_data');
                foreach ($user as $d){$by_user=$d->id;}
                $params = array(					
					'total_questions' => $this->input->post('total_questions'),
					'question_type_id' => $this->input->post('question_type_id'),
                    'set_behavior' => $this->input->post('qb'),
					'display_no_of_questions' => $this->input->post('display_no_of_questions'),
					'question_sets_heading' => $this->input->post('question_sets_heading'),
					'question_sets_desc' => $this->input->post('question_sets_desc'),
                    'question_sets_para' => $this->input->post('question_sets_para'),
                    'answer_audio_recrod_duration'  => $this->input->post('answer_audio_recrod_duration', true),
                    'active' => $this->input->post('active'),
                    'by_user' => $by_user,
                );

                $config['upload_path']  = QS_IMAGE_PATH;
                $config['allowed_types']= QUESTTION_SET_ALLOWED_TYPES;
                $config['encrypt_name'] = FALSE;         
                $this->load->library('upload',$config);
                if($this->upload->do_upload("question_sets_image")){
                    $data = array('upload_data' => $this->upload->data());
                    $image= $data['upload_data']['file_name'];
                    $params = array(                    
                        'total_questions' => $this->input->post('total_questions'),
                        'question_type_id' => $this->input->post('question_type_id'),
                        'set_behavior' => $this->input->post('qb'),
                        'display_no_of_questions' => $this->input->post('display_no_of_questions'),
                        'question_sets_heading' => $this->input->post('question_sets_heading'),
                        'question_sets_desc' => $this->input->post('question_sets_desc'),
                        'question_sets_para' => $this->input->post('question_sets_para'),
                        'answer_audio_recrod_duration'  => $this->input->post('answer_audio_recrod_duration', true),
                        'active' => $this->input->post('active'),
                        'question_sets_image' => site_url(QS_IMAGE_PATH).$image,
                        'by_user' => $by_user,
                    );
                    
                }else{
                    $params = array(                    
                        'total_questions' => $this->input->post('total_questions'),
                        'question_type_id' => $this->input->post('question_type_id'),
                        'set_behavior' => $this->input->post('qb'),
                        'display_no_of_questions' => $this->input->post('display_no_of_questions'),
                        'question_sets_heading' => $this->input->post('question_sets_heading'),
                        'question_sets_desc' => $this->input->post('question_sets_desc'),
                        'question_sets_para' => $this->input->post('question_sets_para'),
                        'question_sets_image'     => NULL,
                        'answer_audio_recrod_duration'  => $this->input->post('answer_audio_recrod_duration', true),
                        'active' => $this->input->post('active'),
                        'by_user' => $by_user,
                    );
                }

                 //echo $params['question_type_id'];echo $params['set_behavior'];die;
                $id = $this->Question_set_model->update_question_set($question_sets_id,$params);
                if($id){
                    $this->session->set_flashdata('flsh_msg', QS_EDIT_MSG);           
                    redirect('question_set/edit/'.$question_sets_id);
                }else{
                    $this->session->set_flashdata('flsh_msg', UPDATE_FAILED_MSG);           
                    redirect('question_set/edit/'.$question_sets_id);
                }
            }
            else
            {
				$this->load->model('Section_model');
				$data['all_sections'] = $this->Section_model->get_all_sections();
				$this->load->model('Question_type_model');
				$data['all_question_types'] = $this->Question_type_model->get_all_question_types();
                $data['_view'] = 'question_set/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error(ITEM_NOT_EXIST);
    }
    /*
     * Deleting question_set
     */
    function remove($question_sets_id)
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $question_set = $this->Question_set_model->get_question_set($question_sets_id);
        if(isset($question_set['question_sets_id']))
        {
            $this->Question_set_model->delete_question_set($question_sets_id);
            $this->session->set_flashdata('flsh_msg', QS_DEL_MSG);             
            redirect('test_seriese/index');
        }
        else
            show_error(ITEM_NOT_EXIST);
    }
    
}
