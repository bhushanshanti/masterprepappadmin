<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/ 
class Live_lecture extends MY_Controller{
    
    function __construct()
    {
        parent::__construct();
       if (!$this->_is_logged_in()) {$this->session->set_userdata('lock_url', $_SERVER['REDIRECT_QUERY_STRING']); redirect('/login');}
        $this->load->model('Live_lecture_model');
        
    }

    /*
     * Listing of live lectures ACD
     */
    function index()
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $this->load->library('pagination');
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('live_lecture/index?');
        $config['total_rows'] = $this->Live_lecture_model->get_all_live_lectures_count();
        $this->pagination->initialize($config);
        $data['title'] = 'Live Lecture'.'- '.ACD;
        $data['live_lectures'] = $this->Live_lecture_model->get_all_live_lectures($params);        
        $data['_view'] = 'live_lecture/index';
        $this->load->view('layouts/main',$data);
    }

    /*
     * Listing of live lectures GT
     */
    function index_gt()
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $this->load->library('pagination');
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('live_lecture/index_gt?');
        $config['total_rows'] = $this->Live_lecture_model->get_all_live_lectures_count_gt();
        $this->pagination->initialize($config);
        $data['title'] = 'Live Lecture'.'- '.GT;
        $data['live_lectures'] = $this->Live_lecture_model->get_all_live_lectures_gt($params);        
        $data['_view'] = 'live_lecture/index';
        $this->load->view('layouts/main',$data);
    }

    /*
     * Listing of live lectures EE
     */
    function index_ee()
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $this->load->library('pagination');
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('live_lecture/index_ee?');
        $config['total_rows'] = $this->Live_lecture_model->get_all_live_lectures_count_ee();
        $this->pagination->initialize($config);
        $data['title'] = 'Live Lecture'.'- '.ENGLISH_ESSENTIAL;
        $data['live_lectures'] = $this->Live_lecture_model->get_all_live_lectures_ee($params);        
        $data['_view'] = 'live_lecture/index';
        $this->load->view('layouts/main',$data);
    }
    /*
     * Adding a new live_lecture
     */
    function add()
    {   
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $data['title'] = 'Add lecture';
        $this->load->library('form_validation');
		$this->form_validation->set_rules('category_id','Category','required');
		$this->form_validation->set_rules('live_lecture_title','Live lecture title','required|trim');
		$this->form_validation->set_rules('video_url','Video URL','required|trim');
		if($this->form_validation->run())
        {   
            $user = $this->session->userdata('admin_login_data');
            foreach ($user as $d){$by_user=$d->id;}
            $params = array(
				'active' => $this->input->post('active'),
				'category_id' => $this->input->post('category_id'),
				'live_lecture_title' => $this->input->post('live_lecture_title'),
				'video_url' => $this->input->post('video_url') ? $this->input->post('video_url') : NULL,
				'screenshot' => $this->input->post('screenshot') ? $this->input->post('screenshot') : NULL,
				'short_desc' => $this->input->post('short_desc'),
                'by_user' => $by_user,
            );

            $dup = $this->Live_lecture_model->dupliacte_live_lecture_check($params['category_id']);
            
                if($dup=='DUPLICATE'){
                    $this->session->set_flashdata('flsh_msg', DUP_MSG);
                    redirect('live_lecture/add');
                }else{                
                        $config['upload_path']      = LIVE_LECTURE_IMAGE_PATH;
                        $config['allowed_types']    = LIVE_LECTURE_ALLOWED_TYPES;
                        $config['encrypt_name']     = FALSE;         
                        $this->load->library('upload',$config);

                        if($this->upload->do_upload("screenshot")){
                            $data = array('upload_data' => $this->upload->data());
                            $image= site_url(LIVE_LECTURE_IMAGE_PATH).$data['upload_data']['file_name'];

                            $params = array(
                                'active' => $this->input->post('active'),
                                'category_id' => $this->input->post('category_id'),
                                'live_lecture_title' => $this->input->post('live_lecture_title'),
                                'video_url' => $this->input->post('video_url') ? $this->input->post('video_url') : NULL,
                                'screenshot' => $image,
                                'short_desc' => $this->input->post('short_desc'),
                                'by_user' => $by_user,
                            );  

                        }else{
                           
                            $params = array(
                                'active' => $this->input->post('active'),
                                'category_id' => $this->input->post('category_id'),
                                'live_lecture_title' => $this->input->post('live_lecture_title'),
                                'video_url' => $this->input->post('video_url') ? $this->input->post('video_url') : NULL,
                                //'screenshot' => $image,
                                'short_desc' => $this->input->post('short_desc'),
                                'by_user' => $by_user,
                            ); 
                        }
                    $id = $this->Live_lecture_model->add_live_lecture($params);
                    $catId = $params['category_id'];
                    if($id and ($catId = $params['category_id']>=44 and $catId<=47) ){
                        $this->session->set_flashdata('flsh_msg', SUCCESS_MSG);
                                redirect('live_lecture/index');
                    }elseif ($id and ($catId>=48 and $catId<=51)) {
                        $this->session->set_flashdata('flsh_msg', SUCCESS_MSG);
                                redirect('live_lecture/index_gt');
                    } 
                    elseif ($id and $catId==75) {
                        $this->session->set_flashdata('flsh_msg', SUCCESS_MSG);
                                redirect('live_lecture/index_ee');
                    }                                
                    else{
                        $this->session->set_flashdata('flsh_msg', FAILED_MSG);
                                redirect('live_lecture/index');
                    }               
                }
        }
        else
        {
			$this->load->model('Category_master_model');
            $data['all_category_masters'] = $this->Category_master_model->get_all_category_masters_active();            
            $data['_view'] = 'live_lecture/add';
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a live_lecture
     */
    function edit($live_lecture_id)
    {   
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $data['title'] = 'Edit lecture';
        $data['live_lecture'] = $this->Live_lecture_model->get_live_lecture($live_lecture_id);
        
        if(isset($data['live_lecture']['live_lecture_id']))
        {
            $this->load->library('form_validation');
			$this->form_validation->set_rules('category_id','Category','required');
			$this->form_validation->set_rules('live_lecture_title','Live lecture title','required|trim');
            $this->form_validation->set_rules('video_url','Video URL','required|trim');		
			if($this->form_validation->run())     
            {   
                $user = $this->session->userdata('admin_login_data');
                foreach ($user as $d){$by_user=$d->id;}
                $params = array(
					'active' => $this->input->post('active'),
					'category_id' => $this->input->post('category_id'),
					'live_lecture_title' => $this->input->post('live_lecture_title'),
					'video_url' => $this->input->post('video_url') ? $this->input->post('video_url') : NULL,
					'screenshot' => $this->input->post('screenshot') ? $this->input->post('screenshot') : NULL,
					'short_desc' => $this->input->post('short_desc'),
                    'by_user' => $by_user,
                );

                        $config['upload_path']  = LIVE_LECTURE_IMAGE_PATH;
                        $config['allowed_types']= LIVE_LECTURE_ALLOWED_TYPES;
                        $config['encrypt_name'] = FALSE;         
                        $this->load->library('upload',$config);

                        if($this->upload->do_upload("screenshot")){
                            $data = array('upload_data' => $this->upload->data());
                            $image= site_url(LIVE_LECTURE_IMAGE_PATH).$data['upload_data']['file_name'];

                            $params = array(
                                'active' => $this->input->post('active'),
                                'category_id' => $this->input->post('category_id'),
                                'live_lecture_title' => $this->input->post('live_lecture_title'),
                                'video_url' => $this->input->post('video_url') ? $this->input->post('video_url') : NULL,
                                'screenshot' => $image,
                                'short_desc' => $this->input->post('short_desc'),
                                'by_user' => $by_user,
                            ); 

                        }else{
                            
                            $params = array(
                                'active' => $this->input->post('active'),
                                'category_id' => $this->input->post('category_id'),
                                'live_lecture_title' => $this->input->post('live_lecture_title'),
                                'video_url' => $this->input->post('video_url') ? $this->input->post('video_url') : NULL,
                                'short_desc' => $this->input->post('short_desc'),
                                'by_user' => $by_user,
                            ); 
                        }

                $id = $this->Live_lecture_model->update_live_lecture($live_lecture_id,$params); 
                $catId = $params['category_id'];           
                if($id and ( $catId >= 44 and $catId <= 47) ){
                    $this->session->set_flashdata('flsh_msg', UPDATE_MSG);
                    redirect('live_lecture/index');
                }elseif($id and ( $catId >= 48 and $catId <= 51) ){
                    $this->session->set_flashdata('flsh_msg', UPDATE_MSG);redirect('live_lecture/index_gt');           
                    
                }elseif($id and $catId == 75){
                    $this->session->set_flashdata('flsh_msg', UPDATE_MSG);redirect('live_lecture/index_ee');
                }else{
                     $this->session->set_flashdata('flsh_msg', UPDATE_FAILED_MSG);
                    redirect('live_lecture/edit/'.$live_lecture_id);
                }           
                
            }
            else
            {
				$this->load->model('Category_master_model');
				$data['all_category_masters'] = $this->Category_master_model->get_all_category_masters_active();
                $data['_view'] = 'live_lecture/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error(ITEM_NOT_EXIST);
    }
    /*
     * Deleting live_lecture
     */
    function remove($live_lecture_id)
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $live_lecture = $this->Live_lecture_model->get_live_lecture($live_lecture_id);
        if(isset($live_lecture['live_lecture_id']))
        {
            $this->Live_lecture_model->delete_live_lecture($live_lecture_id);
            $this->session->set_flashdata('flsh_msg', DEL_MSG);
            redirect('live_lecture/index');
        }
        else
            show_error(ITEM_NOT_EXIST);
    }
    
}
