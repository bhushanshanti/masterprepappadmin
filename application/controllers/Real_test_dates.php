<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/
 class Real_test_dates extends MY_Controller{
    
    function __construct()
    {
        parent::__construct();
        if (!$this->_is_logged_in()) {$this->session->set_userdata('lock_url', $_SERVER['REDIRECT_QUERY_STRING']); redirect('/login');}
        $this->load->model('Real_test_model');        
        $this->load->model('Test_module_model');
        $this->load->model('Programe_master_model');
        $this->load->model('Country_model');
    }    
    /*
     * Listing of all Real_test_dates
     */
    function index()
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends

        $this->load->library('pagination');
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('Real_test_dates/index?');
        $config['total_rows'] = $this->Real_test_model->get_all_test_dates_count();
        $this->pagination->initialize($config);
        $data['title'] = 'Real test dates';
        $data['Real_test_dates'] = $this->Real_test_model->get_all_test_dates($params);
        $data['all_programe_masters'] = $this->Programe_master_model->get_all_programe_masters_active();
        $data['all_test_module'] = $this->Test_module_model->get_all_test_module_active();
        $data['_view'] = 'real_test_dates/index';
        $this->load->view('layouts/main',$data);
    } 

    /*
     * Adding a new Real_test_dates
     */
    function add()
    { 
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends

        $data['title'] = 'Add Real test dates';  
        $this->load->library('form_validation');
        $this->form_validation->set_rules('testDate','Date','min_length[10]|max_length[10]');
        $this->form_validation->set_rules('test_url','Test URL','min_length[10]|max_length[255]');
        $this->form_validation->set_rules('programe_id','Programme','required');
        $this->form_validation->set_rules('test_module_id','Test module','required');
        $this->form_validation->set_rules('country_id','Country','required');
        $this->form_validation->set_rules('type','Type','required');
        
        if($this->form_validation->run())     
        {
            $user = $this->session->userdata('admin_login_data');
            foreach ($user as $d){$by_user=$d->id;}
            $params = array(
                'active' => $this->input->post('active'),
                'programe_id'    => $this->input->post('programe_id'),
                'testDate' => $this->input->post('testDate') ? $this->input->post('testDate') : NULL,
                'test_url' => $this->input->post('test_url') ? $this->input->post('test_url') : NULL,
                'test_module_id' => $this->input->post('test_module_id'),
                'country_id'     => $this->input->post('country_id'),  
                'type'           => $this->input->post('type'),              
                'by_user'        => $by_user,                             
            );
            $data['_view'] = 'real_test_dates/add';
            $this->load->view('layouts/main',$data);
            $id = $this->Real_test_model->add_real_test_dates($params); 
            if($id!='DUPLICATE'){
                $this->session->set_flashdata('flsh_msg', SUCCESS_MSG);
                redirect('real_test_dates/index');
            }elseif($id=='DUPLICATE'){
                $this->session->set_flashdata('flsh_msg', DUP_MSG);
                redirect('real_test_dates/add');
            }else{
                $this->session->set_flashdata('flsh_msg', FAILED_MSG);
                redirect('real_test_dates/add');
            }            
        }else{            
             
            $data['all_test_module'] = $this->Test_module_model->get_all_test_module_active();
            $data['all_programe_masters'] = $this->Programe_master_model->get_all_programe_masters_active();
            $data['all_country_list'] = $this->Country_model->get_all_country_active();
            $data['_view'] = 'real_test_dates/add';
            $this->load->view('layouts/main',$data);
        }
    }
    /*
     * Editing a Real_test_dates
     */
    function edit($realTestDatesId)
    {          
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $data['title'] = 'Edit Real test dates';  
        $data['Real_test_dates'] = $this->Real_test_model->get_test_dates($realTestDatesId);   
        
        if(isset($data['Real_test_dates']['realTestDatesId']))
        {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('testDate','Date','min_length[10]|max_length[10]');
            $this->form_validation->set_rules('test_url','Test URL','min_length[10]|max_length[255]');
            $this->form_validation->set_rules('programe_id','Programme','required');
            $this->form_validation->set_rules('test_module_id','Test module','required');   
            $this->form_validation->set_rules('country_id','Country','required');
            $this->form_validation->set_rules('type','Type','required');    
            if($this->form_validation->run())     
            {   
                $user = $this->session->userdata('admin_login_data');
                foreach ($user as $d){$by_user=$d->id;}
                
                $params = array(
                    'active'      => $this->input->post('active'),
                    'programe_id' => $this->input->post('programe_id'),
                    'testDate'    => $this->input->post('testDate') ? $this->input->post('testDate') : NULL,
                    'test_url' => $this->input->post('test_url') ? $this->input->post('test_url') : NULL, 
                    'test_module_id'=> $this->input->post('test_module_id'),
                    'country_id'    => $this->input->post('country_id'),
                    'type'          => $this->input->post('type'),                 
                    'by_user'       => $by_user,                  
                );                
                $id = $this->Real_test_model->update_test_date($realTestDatesId,$params);
                if($id){
                    $this->session->set_flashdata('flsh_msg', UPDATE_MSG);
                    redirect('real_test_dates/index');
                       
                }else{
                    $this->session->set_flashdata('flsh_msg', UPDATE_FAILED_MSG);
                    redirect('real_test_dates/edit/'.$realTestDatesId);
                }   
            }else{
                
                $data['all_test_module'] = $this->Test_module_model->get_all_test_module_active();
                $data['all_programe_masters'] = $this->Programe_master_model->get_all_programe_masters_active();
                $data['all_country_list'] = $this->Country_model->get_all_country_active();
                $data['_view'] = 'real_test_dates/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error(ITEM_NOT_EXIST);
    } 

    /*
     * Deleting band_score
     */
    function remove($realTestDatesId)
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $Real_test_dates = $this->Real_test_model->get_test_dates($realTestDatesId);
        if(isset($Real_test_dates['realTestDatesId']))
        {
            $this->Real_test_model->delete_test_dates($realTestDatesId);
            $this->session->set_flashdata('flsh_msg', DEL_MSG);
            redirect('Real_test_dates/index');
        }
        else
            show_error(ITEM_NOT_EXIST);
    } 
    
    
}