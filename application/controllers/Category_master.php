<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/
class Category_master extends MY_Controller{

    function __construct()
    {
        parent::__construct();
        if (!$this->_is_logged_in()) {$this->session->set_userdata('lock_url', $_SERVER['REDIRECT_QUERY_STRING']); redirect('/login');}
        $this->load->model('Category_master_model');
        $this->load->model('Test_seriese_model');
        $this->load->model('Test_module_model');
        $this->load->model('Category_master_model');
        $controller_name='Category_master';
        $cn=$controller_name.''.'.php';
    }
    
    /*
     * Listing of all category
     */
    function index()
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $this->load->library('pagination');
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('category_master/index?');
        $config['total_rows'] = $this->Category_master_model->get_all_category_masters_count();
        $this->pagination->initialize($config);
        $data['category_masters'] = $this->Category_master_model->get_all_category_masters($params);
        $data['title'] = 'Category- '.IELTS;
        $data['_view'] = 'category_master/index';
        $this->load->view('layouts/main',$data);
    }

    function index_ee()
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $this->load->library('pagination');
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('category_master/index_ee?');
        $config['total_rows'] = $this->Category_master_model->get_all_category_masters_count_ee();
        $this->pagination->initialize($config);
        $data['category_masters'] = $this->Category_master_model->get_all_category_masters_ee($params);
        $data['title'] = 'Category- '.ENGLISH_ESSENTIAL;
        $data['_view'] = 'category_master/index';
        $this->load->view('layouts/main',$data);
    }

    
    /*
     * Adding a new category
     */
    function add()
    {   
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $data['title'] = 'Add category';
        $this->load->library('form_validation');
		$this->form_validation->set_rules('category_name','Category Name','required|trim|min_length[3]|max_length[30]');
		$this->form_validation->set_rules('programe_id','Program','required');
        $this->form_validation->set_rules('icon','Icon','trim');
        $this->form_validation->set_rules('test_module_id','Test module','required');
		
		if($this->form_validation->run())     
        {   
            $user = $this->session->userdata('admin_login_data');
            foreach ($user as $d){$by_user=$d->id;}
            $params = array(
				'active' => $this->input->post('active'),
				'programe_id' => $this->input->post('programe_id'),
				'category_name' => $this->input->post('category_name'),
                'icon' => $this->input->post('icon') ? $this->input->post('icon') : NULL,
                'test_module_id' => $this->input->post('test_module_id'),
                'packages' => $packages,
                'by_user' => $by_user,
            );
            $dup = $this->Category_master_model->dupliacte_category_master($params['programe_id'],$params['category_name']);
            if($dup=='DUPLICATE'){
                $this->session->set_flashdata('flsh_msg', DUP_MSG);
                redirect('category_master/add');
            }else{
                $id = $this->Category_master_model->add_category_master($params);
                if($id){
                    $this->session->set_flashdata('flsh_msg', SUCCESS_MSG);
                    if($params['test_module_id']==1)
                        redirect('category_master/index');
                    else if($params['test_module_id']==5)
                        redirect('category_master/index_ee');
                    else
                        redirect('category_master/index');
                }else{                    
                    $this->session->set_flashdata('flsh_msg', FAILED_MSG);
                    redirect('category_master/add');
                } 
            }
        }
        else
        {
			$this->load->model('Programe_master_model');
            $data['all_test_module'] = $this->Test_module_model->get_all_test_module_active();
			$data['all_programe_masters'] = $this->Programe_master_model->get_all_programe_masters_active();
			$data['all_packages'] = $this->Category_master_model->get_all_packages();            
            $data['_view'] = 'category_master/add';
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a category_master
     */
    function edit($category_id)
    {   
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $data['title'] = 'Edit category';
        $data['category_master'] = $this->Category_master_model->get_category_master($category_id);
        if(isset($data['category_master']['category_id']))
        {
            $this->load->library('form_validation');
			$this->form_validation->set_rules('category_name','Category Name','required|trim|min_length[3]|max_length[30]');
			$this->form_validation->set_rules('programe_id','Program','required');
            $this->form_validation->set_rules('icon','Icon','trim');
            $this->form_validation->set_rules('test_module_id','Test module','required');
		
			if($this->form_validation->run())     
            {   
                $user = $this->session->userdata('admin_login_data');
                foreach ($user as $d){$by_user=$d->id;}
                
                $packages = implode("," , $this->input->post('package_id'));
                $params = array(
					'active' => $this->input->post('active'),
					'programe_id' => $this->input->post('programe_id'),
					'category_name' => $this->input->post('category_name'),
                    'icon' => $this->input->post('icon') ? $this->input->post('icon') : NULL,
                    'test_module_id' => $this->input->post('test_module_id'),
                    'packages' => $packages,
                    'by_user' => $by_user,
                );
                $id = $this->Category_master_model->update_category_master($category_id,$params);
                if($id){
                    $this->session->set_flashdata('flsh_msg', UPDATE_MSG);           
                    if($params['test_module_id']==1)
                        redirect('category_master/index');
                    else if($params['test_module_id']==5)
                        redirect('category_master/index_ee');
                    else
                        redirect('category_master/index');
                }else{
                    $this->session->set_flashdata('flsh_msg', UPDATE_FAILED_MSG);           
                    redirect('category_master/edit/'.$category_id);
                }                
            }
            else
            {
				$this->load->model('Programe_master_model');
                $data['all_test_module'] = $this->Test_module_model->get_all_test_module_active();
				$data['all_programe_masters'] = $this->Programe_master_model->get_all_programe_masters_active();
				$data['all_packages'] = $this->Category_master_model->get_all_packages();
                $data['_view'] = 'category_master/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error(ITEM_NOT_EXIST);
    } 

    /*
     * Deleting category_master
     */
    function remove($category_id)
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $category_master = $this->Category_master_model->get_category_master($category_id);
        if(isset($category_master['category_id']))
        {
            $del = $this->Category_master_model->delete_category_master($category_id);
            if($del){
                $this->session->set_flashdata('flsh_msg', DEL_MSG);
                if($category_master['test_module_id']==1)
                    redirect('category_master/index');
                else if($params['test_module_id']==5)
                    redirect('category_master/index_ee');
                else
                    redirect('category_master/index');
            }else{
                $this->session->set_flashdata('flsh_msg', DEL_MSG_FAILED);
                redirect('category_master/index');
            }
            
        }
        else
            show_error(ITEM_NOT_EXIST);
    }

    function get_category_list()
    {        
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $test_seriese_id = $this->input->post('test_seriese_id');
        if(isset($test_seriese_id)){
            $tm = $this->Test_seriese_model->get_test_module_id($test_seriese_id);
            $test_module_id = $tm['test_module_id'];
            
            $response =  $this->Category_master_model->get_all_category_masters_active_tt($test_module_id);
            echo json_encode($response);
        }else{
            header('Content-Type: application/json');
            $response = ['msg'=>'list not available!', 'status'=>'false'];
            echo json_encode($response);
        }       
        
    }

    function get_category_name(){

        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $category_id = $this->input->post('category_id');
        if(isset($category_id)){
            $response = $this->Category_master_model->get_category_name_audio($category_id);
            echo json_encode($response);
        }else{
            header('Content-Type: application/json');
            $response = ['msg'=>'value not exist!', 'status'=>'false'];
            echo json_encode($response);
        }
    }
    
}
