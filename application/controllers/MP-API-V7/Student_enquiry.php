<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';    
     
class Student_enquiry extends REST_Controller {
    
public function __construct() {
    
    error_reporting(0);
    parent::__construct();
    $this->load->database();
    $this->load->model('Notification_subject_model'); 
    $this->load->model('Notification_message_model');
    $this->load->model('Notification_model');
    $this->load->model('User_model');
    $this->load->model('Student_model');
}

/**
    * enquiry submission from this method.
    *
    * @return Response
*/
public function index_post()
{        
        if (!$this->Authenticate($this->input->get_request_header('token'))) {
            $this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND); 
        }else{
            //$json       = file_get_contents('php://input');
            $std_data   = json_decode(file_get_contents('php://input'),true);
            $id = $this->input->get_request_header('id');
            $spData = $this->idRoutineCall($id);       
            $programe_id = $spData['programe_id'];
            $test_module_id = $spData['test_module_id'];

                $params = array(
                    'student_id'    => $id,
                    'programe_id'   => $programe_id,
                    'name'          => $std_data['name'],
                    'email'         => $std_data['email'],
                    'mobile'        => $std_data['mobile'],
                    'center_id'     => $std_data['center_id'],
                    'center_name'   => $std_data['center_name'],
                    'city_name'     => $std_data['city_name'],
                    'test_module_id'=> $test_module_id,
                    'test_id'       => $std_data['test_id'],
                    'test_module_name'=> $std_data['test_module_name'],
                    'message'       => $std_data['message'],
                ); 
                $this->db->insert('students_enquiry', $params);
                $last_insert_id = $this->db->insert_id();
        
            if($last_insert_id) {

                $ts_cat_assoc_id=$last_insert_id;
                $collection_no='Student Enquiry';
                $paper_checked=NULL;
                //$this->insert_notification(ENQ_NOTIFY,$id,$programe_id,$test_module_id,$ts_cat_assoc_id,$collection_no,$paper_checked);
                
                if($std_data['email']!=''){
                    $subject = 'Your enquiry sent to Masterprep admin';
                    $mail_data = array(
                        'programe_id'   => $programe_id,
                        'name'          => $std_data['name'],
                        'email'         => $std_data['email'],
                        'mobile'        => $std_data['mobile'], 
                        'message'       => $std_data['message'],
                        'center_name'   => $std_data['center_name'],
                        'city_name'     => $std_data['city_name'],                    
                        'test_module_name'=> $std_data['test_module_name'],         
                    );  
                   $this->sendEmailEnquiry_toStd($std_data['email'],$subject,$mail_data);
                   
                }
                //get enquiry admin to send message
                $enquiry_data       = $this->User_model->get_enquiry_admin();       
                $enquiry_to_id      = $enquiry_data['id'];
                $enquiry_to_fname   = $enquiry_data['fname'];
                $enquiry_to_mobile  = $enquiry_data['mobile'];
                $enquiry_to_email   = $enquiry_data['email'];
                if($enquiry_to_email!=''){
                    $subject2 = 'Enquiry from Masterprep student';
                    $mail_data2 = array(
                        'programe_id'   => $programe_id,
                        'name'          => $enquiry_to_fname,
                        'student_name'  => $std_data['name'],
                        'email'         => $std_data['email'],
                        'mobile'        => $std_data['mobile'], 
                        'message'       => $std_data['message'],
                        'center_name'   => $std_data['center_name'],
                        'city_name'     => $std_data['city_name'],                    
                        'test_module_name'=> $std_data['test_module_name'],        
                    );
                    $this->sendEmail_toEnquiryAdmin($enquiry_to_email,$subject2,$mail_data2);
                }
                $data['error_message'] = [ 'success' => 1, 'message'=> ENQ_SUC ];
            }else{
                $data['error_message'] = [ 'success' => 0, 'message'=> ENQ_FLD ];
            }  
            $this->set_response($data, REST_Controller::HTTP_CREATED);            
    }
} 

public function index_get(){

    if (!$this->Authenticate($this->input->get_request_header('token'))) {
            $this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND); 
    }else{             
            
            $enquiry_id = $this->input->get_request_header('enquiryId');
            $enqData = $this->Student_model->get_all_reply($enquiry_id);

            $pageTitle= 'ENQUIRY INBOX';
            $tag = 'How can I help you?';

        if(!empty($enqData)){
            $data['error_message'] = [ "success" => 1, "pageTitle"=> $pageTitle, "tag"=> $tag, "message" => "success", "data"=> $enqData];    
        }else{
            $data['error_message'] = [ "success" => 0, "pageTitle"=> $pageTitle, "tag"=> $tag, "message" => "No Enquiry found!", "data"=> $enqData];     
        }        
        $this->set_response($data, REST_Controller::HTTP_CREATED);
    }
}


public function sendEmailEnquiry_toStd($email,$subject,$mail_data){

    $this->load->library('email');
    $this->email->set_mailtype("html");
    $this->email->set_newline("\r\n");
    $this->email->from(FROM_EMAIL, FROM_NAME);         
    $this->email->to($email);
    $this->email->subject($subject);
    $body = $this->load->view('emails/enquiry_std.php',$mail_data,TRUE);
    $this->email->message($body);
    $this->email->send();
}

public function sendEmail_toEnquiryAdmin($email,$subject,$mail_data){
         
    $this->load->library('email');
    $this->email->set_mailtype("html");
    $this->email->set_newline("\r\n");
    $this->email->from(FROM_EMAIL, FROM_NAME);
    $this->email->to($email);
    $this->email->subject($subject);
    $body = $this->load->view('emails/enquiry_adm.php',$mail_data,TRUE);
    $this->email->message($body);
    $this->email->send();
}


}//class closed