<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
    
class Student_detailed_result extends REST_Controller {   
     
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }
    /**
     * Load full result from this method.
     *
     * @return Response
    */
    function index_get(){

        if (!$this->Authenticate($this->input->get_request_header('token'))) {
            $this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND); 
        }else{
        
            $id = $this->input->get_request_header('id');
            $ts_cat_assoc_id = $this->input->get_request_header('tsCatId');
            $collection_no = $this->input->get_request_header('collNo');
        
        $this->db->trans_start();
        $this->db->select('
            sec.section_heading,
            qs.question_sets_heading,
            REPLACE(qs.question_sets_para,"~~~",".........") as question_sets_para,
            q.question_no,
            q.question,
            REPLACE(sa.student_answers,"~~"," & ") as student_answers,
            sa.is_img,
            sa.checker_remarks,
            REPLACE(q.correct_answer,"~~"," & ") as correct_answer,
            q.correct_answer_explaination,
            sa.marks_secured
        ');        
        $this->db->from('student_answers sa');        
        $this->db->join('`questions` q', 'q.`question_id`= sa.`question_id`');
        $this->db->join('`question_sets` qs', 'qs.`question_sets_id`= q.`question_sets_id`');
        $this->db->join('`sections` sec', 'sec.`section_id`= qs.`section_id`');
        $this->db->where(array('sa.collection_no'=>$collection_no,'sa.ts_cat_assoc_id'=>$ts_cat_assoc_id,'sa.student_id'=>$id));
        $this->db->group_by('sa.student_answers_id');
        $this->db->order_by('q.`question_no` asc');
        $qa_data =$this->db->get('')->result_array();

        $sum_data=$this->db->query('SELECT 
            sr.`rank`,
            sr.`marks_secured`,
            sr.`percentage`,
            sr.`band_score`,
            sr.`time_taken`,
            sr.`totalQuestion`,
            sr.`skipped`,
            sr.`attempted`,
            sr.`paper_checked`,
            pl.`pro_name`,
            date_format(sr.`created`, "%D %b %y %I:%i %p") as created
            FROM `student_results` sr
            LEFT JOIN `proficiency_level` pl ON pl.`pro_id`=sr.`pro_id`
            WHERE `collection_no` = "'.$collection_no.'" and  ts_cat_assoc_id = "'.$ts_cat_assoc_id.'" and student_id = "'.$id.'" ');                          
        $get_sum = $sum_data->result_array();
        
        if(isset($get_sum) and !empty($get_sum)){
            foreach($get_sum as $p){ 
                
                $pro_name = $p['pro_name'];
                $total = $p['marks_secured'];
                $marks_secured = $p['marks_secured']; 
                $band_score = $p['band_score'];
                $created = $p['created'];

                $rank = $p['rank'];  
                $percentage = $p['percentage'];
                if($percentage!=''){
                    $percentage = $percentage.PERCENT;
                }
                $time_taken     = $p['time_taken'];                 
                $totalQuestion  = $p['totalQuestion'];  
                $skipped        = $p['skipped'];
                $attempted      = $p['attempted'];
                $paper_checked  = $p['paper_checked'];
            }
        }else{
            $total =        NULL;  
            $band_score =   NULL;
            $created =      NULL;
        }
        $this->db->select('ts.test_seriese_name, ts.test_seriese_type,
        ts_cat.paper_duration,ts_cat.max_marks,ts_cat.audio_file, ts_cat.audio_time, ts_cat.totalquest, UPPER(cat.category_name) as category_name, pgm.programe_name');
        $this->db->from('ts_cat_assoc ts_cat');
        $this->db->join('test_seriese ts', 'ts_cat.test_seriese_id= ts.test_seriese_id');
        $this->db->join('category_masters cat', 'cat.category_id= ts_cat.category_id');
        $this->db->join('programe_masters pgm', 'pgm.programe_id= cat.`programe_id');
        $this->db->where(array('ts_cat.ts_cat_assoc_id'=>$ts_cat_assoc_id));
        $basic_data =$this->db->get('')->row_array();
        
        $mm = $basic_data['max_marks'];
        if(isset($mm)){
            $total= $total.'/'.$mm;
        }else{
            $mm=NULL;
            $total= $total.'/'.$mm;
        }
        $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE)        {
                $data['error_message']=

                ['success'=>0, "message" => "Failed to load result!", 'created'=>NULL,'Total'=>NULL,'rank'=>NULL, 'band_score'=>NULL, 'percentage'=>NULL, 'totalQuestion'=>NULL,'skipped'=>NULL, 'attempted'=>NULL,'paper_checked'=>NULL, 'result_id'=>NULL, 'basic'=>[],'QA' => [] ];
            }else{
                    
                if(!empty($qa_data) and !empty($total) and !empty($basic_data))
                $data['error_message'] = 

                ["success" => 1, "message" => "Your result loaded successfully", 'created'=> $created, 'Total'=>$total, 'marks_secured'=> $marks_secured, 'pro_name'=>$pro_name, 'rank'=>$rank, 'band_score'=>$band_score, 'percentage'=>$percentage, 'totalQuestion'=>$totalQuestion,'skipped'=>$skipped, 'attempted'=>$attempted,'paper_checked'=>$paper_checked, 'result_id'=>" ".$collection_no." ", 'basic'=>$basic_data, 'QA' => $qa_data];
                else
                $data['error_message']=

                ['success'=>0, "message" => "Failed to load result!", 'created'=>NULL,'Total'=>NULL,'marks_secured'=> NULL,'pro_name'=>NULL, 'rank'=>NULL, 'band_score'=>NULL, 'percentage'=>NULL, 'totalQuestion'=>NULL,'skipped'=>NULL, 'attempted'=>NULL,'paper_checked'=>NULL, 'result_id'=>NULL, 'basic'=>[],'QA' => [] ];
                $this->set_response($data, REST_Controller::HTTP_CREATED);
            }            
        }
    }  

    
}