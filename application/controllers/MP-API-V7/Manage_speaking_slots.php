<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Harman uppal
 *
 **/
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';    
     
class Manage_speaking_slots extends REST_Controller {
    
	public function __construct() {
		parent::__construct();
	    $this->load->database();
	    $this->load->model('Manage_speaking_slots_model');    
	    $this->load->model('User_model');    
	}

	public function index_get(){
		$token = $this->input->get_request_header('token'); 
	    if (!$this->Authenticate($token)) {
	    	$this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND); 
	    }else{
            $spData = $this->tokenRoutineCall($token);       
            $programe_id    = $spData['programe_id'];
            $stud_id        = $spData['id'];
            $test_module_id = $spData['test_module_id'];
            $time_zone      = $spData['time_zone'];			
			$test_date      = $this->input->get_request_header('tdate');
			$zone_Info      = $this->get_zone_info($time_zone);
			$zoneName       = $zone_Info['zoneName'];
			
			/* check if user is eligible for MOCK test starts */
			$eligible = $this->Manage_speaking_slots_model->check_mock_eligibility($stud_id);			
			
			if($eligible['status'] == "success"){
				/* check if examiner available in same timezone */
				$available_teachers = $this->Manage_speaking_slots_model->get_available_teacher_same_timezone($time_zone , $test_date);
				$booking_count = $this->Manage_speaking_slots_model->get_examiner_speaking_booking_count();
				$time_conversion = FALSE;
				$teacher_assigned_count = array();
				if(!empty($booking_count)){
					foreach($booking_count as $teacher){
						$teacher_assigned_count["examiner_id_".$teacher['user_id']] = $teacher['cnt'];
					}
				}
				/* Get default timezone teachers */
				
				if(empty($available_teachers)){					
					$available_teachers = $this->Manage_speaking_slots_model->get_available_teacher_from_def_timezone($time_zone , $test_date);
					$time_conversion = TRUE;
				}
				
				
				
				
				//$date_now = time();
				
				$date_now = date("Y-m-d H:i:s", strtotime('+4 hours'));
				
				if(!empty($available_teachers)){
					foreach ($available_teachers as $key => $value) {
						$assigned_count = 0;
						if(isset($teacher_assigned_count["examiner_id_".$value['user_id']]))
							$assigned_count = $teacher_assigned_count["examiner_id_".$value['user_id']];
						
						$inner_array     = array();
						$inner_array     = $value;
						$start_date_time = $value['date']." ".$value['start_time'];						
						$date2           = strtotime($start_date_time);
											
						if (strtotime($date_now) > $date2)
							continue;
							
						if($time_conversion){
							$start_time1 = $start_date_time;
							$end_time1   = $value['date']." ".$value['end_time'];
							
							$start_time_conv = new DateTime($start_time1 , new DateTimeZone(DFT_SPEAKING_EXAMINER_TIMEZONE));							
							$start_time_conv->setTimezone(new DateTimeZone($zoneName));
							$start_time_conv1 = $start_time_conv->format('H:i');
							$inner_array['date']       = $start_time_conv->format('Y-m-d');						
						}
						else{
							$start_time_conv1 = $value['start_time'];
							//$start_time1 = "";
						}
						
						
						$inner_array['start_time'] = $start_time_conv1;
						
						//t($inner_array);
						$inner_array['assigned_count'] = $assigned_count;
						$sorted_teachers[] = $inner_array;						
					}
				}			
				//t($sorted_teachers , 1);
				usort($sorted_teachers, function($a, $b) {
					$retval = $a['assigned_count'] <=> $b['assigned_count'];
					if ($retval == 0) {
						$retval = $a['suborder'] <=> $b['suborder'];
						if ($retval == 0) {
							$retval = $a['details']['subsuborder'] <=> $b['details']['subsuborder'];
						}
					}
					return $retval;
				});
				
			 	$send_availble_slots = array();
			
				foreach($sorted_teachers as $teacher){
					$inner_array = array();	
					if(!isset($send_availble_slots[$teacher['date']][$teacher['start_time']])):	
						$send_availble_slots[$teacher['date']][$teacher['start_time']] = array("user_id" => $teacher['user_id'] ,"start_time" => $teacher['start_time'] ,"end_time" => $teacher['end_time'] ,"avail_id" => $teacher['avail_id']);						
					endif;
				}
				
				$final_slots = array();
				foreach($send_availble_slots as $date => $teacher ){
					
					usort($teacher, function($a, $b) {
					$retval = $a['start_time'] <=> $b['start_time'];
					if ($retval == 0) {
						$retval = $a['suborder'] <=> $b['suborder'];
						if ($retval == 0) {
							$retval = $a['details']['subsuborder'] <=> $b['details']['subsuborder'];
						}
					}
					return $retval;
				});
				
				//$in_array = array()
				$final_slots[] = array("date" => $date , "slots" => $teacher );
					
				}
				
				
				if(!empty($final_slots)){
				  $data['error_message'] = [ "success" => 1, "message" => "success","other_info" => $final_slots];
				}else{
				  $data['error_message'] = [ "success" => 0, "message" => "Examiner not found","other_info" => array() ];
				}
			}
			else{
				$data['error_message'] = [ "success" => 0, "message" => $eligible['message'],"other_info" => $eligible['other_info'] ];
			}		
			
			//t($data);
	       $this->set_response($data, REST_Controller::HTTP_CREATED);
        }
	}

	public function book_speaking_slot_post(){
        $token = $this->input->get_request_header('token');   
        if (!$this->Authenticate($token)) {
            $this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND); 
        }else{
            $spData = $this->tokenRoutineCall($token);       
            $programe_id    = $spData['programe_id'];
            $test_module_id = $spData['test_module_id'];
            $student_id     = $spData['id'];
            
            $json          = file_get_contents('php://input');
            $booking_info  = json_decode(file_get_contents('php://input'), true);           
           	$avail_id      = $booking_info['avail_id'];
           	$booking_date      = $booking_info['booking_date'];
           	$booking_starttime      = $booking_info['booking_starttime'];
			
			$json = json_encode(array("avail_id" =>  $avail_id ,"booking_date" =>  $booking_date ,"booking_starttime" => $booking_starttime ));
			$eligible = $this->Manage_speaking_slots_model->check_mock_eligibility($student_id);
			
			
			if($eligible['status'] == "success"){			
				$available_slot = $this->Manage_speaking_slots_model->check_availble_slot($avail_id);

				$unique_id = "";
				if(!empty($available_slot)){
					$examiner_id    = $available_slot['user_id'];
					$examiner_email = $available_slot['email'];
					$examiner_fname = $available_slot['fname'];
					$unique_id = $this->Manage_speaking_slots_model->book_speaking_slot($avail_id , $student_id , $examiner_id , $json) ;
					$message = "Your slot has been booked.";
				}
				else{
					$message = "Sorry! Please choose some other slot.";
				}
				
				$data = array();
				if($unique_id != ""){
					/* Send booking Notification to student*/				
					 $this->insert_notification(MOC_BOOKING_NOTIFY,$student_id,$programe_id,$test_module_id,"","","");
					/* Send booking Notification to Examiner , Mock test examiner */
										
					$mockAdmin_data       = $this->User_model->get_mock_admin();
					$mockAdmin_id         = $mockAdmin_data['id'];
					$mockAdmin_to_fname   = $mockAdmin_data['fname'];
					$mockAdmin_to_fname   = $mockAdmin_data['fname'];
					$mockAdmin_to_email   = $mockAdmin_data['email'];
					$notification_message1 = "You have new speaking booking for mocktest.";
					$notification_message = "New speaking booking for mocktest.";
					$other_info           = array("examiner_id" => $examiner_id ,"student_id" => $student_id ,"avail_id" => $avail_id ,"notify_type" => "speaking_booking" );
					
					/* insert notofictaion to exminer */
					$teacher_notification = $this->Manage_speaking_slots_model->insert_examiner_notification( $examiner_id , $notification_message1, json_encode($other_info , true));
					
					/* insert notofictaion to MOCKTEST ADMIN */
					$admin_notification = $this->Manage_speaking_slots_model->insert_examiner_notification($mockAdmin_id , $notification_message, json_encode($other_info , true));
					
					if(isset($teacher_notification) && ( $examiner_email!='')){
						$subject2 = 'New Mock test speaking booking';
						$mail_data2 = array('programe_id' => $programe_id,'name' => $examiner_fname,'Notification_url' => site_url("notification/view_notification/$teacher_notification"));
						$this->sendMOCKEmail_toExaminer($examiner_email,$subject2,$mail_data2);
					}
					
					
					if(isset($admin_notification) && ( $mockAdmin_to_email!='')){						
						$subject2 = 'New Mock test speaking booking';
						$mail_data2 = array('programe_id' => $programe_id,'name' => $mockAdmin_to_fname,'Notification_url' => site_url("notification/view_notification/$admin_notification"));			
						$this->sendMOCKEmail_toExaminer($mockAdmin_to_email,$subject2,$mail_data2);
					}
					
					
					
					$data['error_message'] = [ "success" => 1, "message" => $message, "other_info" => $unique_id];	
				}else{
					$data['error_message'] = [ "success" => 1, "message" => $message,"other_info" => ""];
				}
			}else{
				$data['error_message'] = [ "success" => 0, "message" => $eligible['message'],"other_info" => $eligible['other_info']];
			}
            
        }

        $this->set_response($data, REST_Controller::HTTP_CREATED);
	}
	
	public function sendMOCKEmail_toExaminer($email,$subject,$mail_data){
			 
		$this->load->library('email');
		$this->email->set_mailtype("html");
		$this->email->set_newline("\r\n");
		$this->email->from(FROM_EMAIL, FROM_NAME);
		$this->email->to($email);
		$this->email->subject($subject);
		$body = $this->load->view('emails/mock_speaking_booking.php',$mail_data,TRUE);
		$this->email->message($body);
		$this->email->send();
	}

}
