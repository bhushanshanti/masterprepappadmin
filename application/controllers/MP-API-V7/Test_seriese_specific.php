<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/

    use Restserver\Libraries\REST_Controller;
    defined('BASEPATH') OR exit('No direct script access allowed');
    require APPPATH . 'libraries/REST_Controller.php';
    
     
class Test_seriese_specific extends REST_Controller {
    
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
        
        parent::__construct();
        $this->load->database();
        $this->load->model('Category_master_model');
        $this->load->model('Test_seriese_model');
    }
       
    /**
     * Get All test seriese for specific category paper data from this method.
     *
     * @return Response
    */
    public function index_get()
    {
        $token = $this->input->get_request_header('token');
        if (!$this->Authenticate()) {
            $this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND);
        }else{ 

        $spData = $this->tokenRoutineCall($token);       
        $programe_id    = $spData['programe_id'];
        $test_module_id = $spData['test_module_id'];
        $category_name  = $this->input->get_request_header('categoryName');   
        
        $this->db->select('
            ts.`test_seriese_id`, 
            ts.`test_seriese_name`, 
            ts.`test_seriese_type`,
            ts_cat.`ts_cat_assoc_id`,
            ts_cat.`icon`,
            cat.category_id,
            cat.category_name
        ');
        $this->db->from('`test_seriese` ts');
        $this->db->join('`ts_cat_assoc` ts_cat', 'ts_cat.`test_seriese_id`= ts.`test_seriese_id`', 'left');
        $this->db->join('`category_masters` cat', 'ts_cat.`category_id`= cat.`category_id`', 'left');       
        $this->db->where(array('ts.active' => 1, 'ts.test_module_id' => $test_module_id, 'cat.category_name'=>$category_name));
        $this->db->order_by('display_order ASC');       
        $data2 = $this->db->get('')->result_array();
        if(!empty($data2))
            $data['error_message'] = [ "success" => 1, "message" => "success", "data"=> $data2];
        else
            $data['error_message'] = [ "success" => 0, "message" => "data not found", "data"=> $data2];
        $this->response($data, REST_Controller::HTTP_OK);  
        
        }
    }   
        
}