<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';   
     
class Test_opt extends REST_Controller {  
     
    public function __construct() {
       parent::__construct();
        $this->load->database();
        $this->load->model('Student_results_model');        
    }       
    /**
     * Get All profile pic un-neccessary from this method.
     *
     * @return Response
    */
    public function index_get()
    {    
        $token = $this->input->get_request_header('token');    
        if (!$this->Authenticate($token)) {
            $this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND); 
        }else{            

            $log_directory = PROFILE_PIC_FILE_PATH;            
            $arr = array();
            $files1 = scandir($log_directory);

            foreach ($files1 as $f) {
              
              if($f!='default_profile_pic.png' and $f!='index.html' and $f!='.' and $f!='..'){
                array_push($arr, $f);
              }else{

              }
            }

            foreach ($arr as $a) {

              //echo PROFILE_PIC_FILE_PATH.$a;die;
              $x = $this->Student_results_model->get_f($a);
              if(!$x['profile_pic']){
                unlink(PROFILE_PIC_FILE_PATH.$a);
              }
            }
            die;          
            if(!empty($arr)){
              $data['error_message'] = [ "success" => 1, "message" => "success", "data"=> $arr];     
            }else{
                  $data['error_message'] = [ "success" => 0, "message" => "failed", "data"=> $arr];     
            }        
            $this->set_response($data, REST_Controller::HTTP_CREATED);
        }
    }     
    
        
}