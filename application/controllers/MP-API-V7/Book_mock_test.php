<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';    
     
class Book_mock_test extends REST_Controller {
    
public function __construct() {
    
    parent::__construct();
    $this->load->database();
    $this->load->model('Notification_subject_model');
    $this->load->model('Notification_message_model');
    $this->load->model('Notification_model');
    $this->load->model('User_model');
    $this->load->model('Student_model');
    $this->load->model('Student_package_model');    
    $this->load->library('email');
    $this->email->set_mailtype("html");
    $this->email->set_newline("\r\n");
    $this->email->from(FROM_EMAIL, FROM_NAME);
}

public function index_get(){

    $token = $this->input->get_request_header('token'); 
    if (!$this->Authenticate($token)) {
            $this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND); 
    }else{             
            $spData = $this->tokenRoutineCall($token);       
            $programe_id    = $spData['programe_id'];
            $test_module_id = $spData['test_module_id'];
            $id = $spData['id'];
            $x = array();
            $y = array();
            $time= '3:00 to 5:30';
            for ($i=0; $i < 4; $i++) { 
              $slot = date("l,j M Y", strtotime("next saturday + ".$i." week"));
              $y['booking_date']=$slot;
              $y['booking_time']=$time;
              array_push($x, $y);  
            }

             //check pack status
            $data3 = $this->Student_package_model->getPackStatus($id,$programe_id,$test_module_id);
            $havePackage = $data3['havePackage'];
            //check pack status
            if($havePackage==1){
               
                $PG_data =[];
                $button = [];
            }else{
                 $PG_data = array(
                    'key'=> KEY_ID,
                    'currency'=> CURRENCY,              
                    'name'=> COMPANY,
                    'description'=> PAYDESC,
                );
                $button = ['Pay now','Pay later'];
            } 
            if(!empty($x)){
                $data['error_message'] = [ "success" => 1, "message" => "success", "data"=> $x, 'havePackage'=> $havePackage,'PG_data'=> $PG_data,'button'=>$button];    
            }else{
                $data['error_message'] = [ "success" => 0, "message" => "data not found", "data"=> $x, 'havePackage'=>$havePackage,'PG_data'=> $PG_data,'button'=>$button];     
            }    
            $this->set_response($data, REST_Controller::HTTP_CREATED);
    }
}

/**
    * mock test booking submission from this method.
    *
    * @return Response
*/
public function index_post()
{        
        $token = $this->input->get_request_header('token');        
        if (!$this->Authenticate($token)) {
            $this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND);
        }else{
            
            $std_data   = json_decode(file_get_contents('php://input'),true);
            $id     = $this->input->get_request_header('id');
            $spData = $this->idRoutineCall($id);       
            $programe_id    = $spData['programe_id'];
            $test_module_id = $spData['test_module_id'];

                $params = array(
                    'student_id'    => $id,
                    'programe_id'   => $programe_id,
                    'test_module_id'=> $test_module_id,
                    'booking_date'  => $std_data['booking_date'],
                    'booking_time'  => $std_data['booking_time'],
                    'active'        => 1
                ); 
                $this->db->insert('mock_test_bookings', $params);
                $last_insert_id = $this->db->insert_id();
        
            if($last_insert_id) {

                $ts_cat_assoc_id=$last_insert_id;
                $collection_no='Mock Test Booking';
                $paper_checked=NULL;
                $this->insert_notification(MOC_BOOKING_NOTIFY,$id,$programe_id,$test_module_id,$ts_cat_assoc_id,$collection_no,$paper_checked);
                
                //Mail/sms
                $stdData = $this->Student_model->get_student_info_forSMS($id);
                if($stdData['email']!=''){
                    $subject = 'Mock test booking request sent';
                    $mail_data = array(                        
                        'name'  => $stdData['fname'],         
                    );  
                    $this->sendMockMail_toStd($stdData['email'],$subject,$mail_data);                   
                }
                //get enquiry admin to send message
                $mock_data       = $this->User_model->get_mock_admin();       
                $mock_to_id      = $mock_data['id'];
                $mock_to_fname   = $mock_data['fname'];
                $mock_to_mobile  = $mock_data['mobile'];
                $mock_to_email   = $mock_data['email'];

                if($mock_to_email!=''){
                    $subject2 = 'Mock Test Booking from Masterprep student';
                    $mail_data2 = array(
                        
                        'name'           => $enquiry_to_fname,
                        'student_name'   => $stdData['name'],
                        'student_email'  => $stdData['email'],
                        'student_mobile' => $stdData['mobile'],
                        'booking_date'   => $std_data['booking_date'],
                        'booking_time'   => $std_data['booking_time'],        
                    );
                $this->sendMockMail_toAdm($mock_to_email,$subject2,$mail_data2);
                }

                $data['error_message'] = [ 'success' => 1, 'message'=>MOCK_SUC ];
            }else{
                $data['error_message'] = [ 'success' => 0, 'message'=>MOCK_FLD ];
            }  
            $this->set_response($data, REST_Controller::HTTP_CREATED);            
    }
} 




public function sendMockMail_toStd($email,$subject,$mail_data){
         
    $this->email->to($email);
    $this->email->subject($subject);
    $body = $this->load->view('emails/mock_std.php',$mail_data,TRUE);
    $this->email->message($body);
    $this->email->send();
}

public function sendMockMail_toAdm($email,$subject,$mail_data){
         
    $this->email->to($email);
    $this->email->subject($subject);
    $body = $this->load->view('emails/mock_adm.php',$mail_data,TRUE);
    $this->email->message($body);
    $this->email->send();
}


}//class closed
