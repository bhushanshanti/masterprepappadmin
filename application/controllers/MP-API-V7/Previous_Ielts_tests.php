<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Harman
 *
 **/
 
class Previous_Ielts_tests extends MY_Controller{
    
    function __construct()
    {
        parent::__construct();
        if (!$this->_is_logged_in()) {redirect('/');}
        $this->load->model('Previous_Ielts_tests_model');	       
        $this->load->model('Static_page_model');
        $this->load->model('Question_type_model'); 
        $this->load->model('Category_master_model');  	
    }
    /*
     * Listing of Previous Ielts tests
     */
    function index()
    {
		
	
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $this->load->library('pagination');
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('Previous_Ielts_tests/index?');
        $config['total_rows'] = $this->Previous_Ielts_tests_model->get_all_previous_test_ac_count();
        $this->pagination->initialize($config);
        $data['title'] = 'Previous Ielts tests';
        $data['all_tests'] = $this->Previous_Ielts_tests_model->get_all_previous_test_ac($params);
        $data['all_category_masters'] = $this->Category_master_model->get_all_category_masters_active();        
         
        $data['_view'] = 'pre_ielts_test/index'; 
        
       
        $this->load->view('layouts/main',$data);
    }
    
    function index_gt()
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $this->load->library('pagination');
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('Previous_Ielts_tests/index?');
        $config['total_rows'] = $this->Previous_Ielts_tests_model->get_all_previous_test_gt_count();
        $this->pagination->initialize($config);
        $data['title'] = 'Previous Ielts tests';
        $data['all_tests'] = $this->Previous_Ielts_tests_model->get_all_previous_test_gt($params);
        $data['all_category_masters'] = $this->Category_master_model->get_all_category_masters_active();       
       
        $data['_view'] = 'pre_ielts_test/index'; 
        
       
        $this->load->view('layouts/main',$data);
    }
    
    /*
     * Adding a new Previous Ielts tests
     */
    function add()
    {   
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $this->load->library('form_validation');
        $this->form_validation->set_rules('exam_date','Exam date','required|trim');
        $data['title'] = 'Add previous test';
        if($this->form_validation->run())     
        {		
            $user = $this->session->userdata('admin_login_data');
            foreach ($user as $d){$by_user=$d->id;}
            $params = array(
                'active' => $this->input->post('active'),
                'module_id'  => 1,
                'date'  => $this->input->post('exam_date'),
                'category_id'  => $this->input->post('category_id'),
                'content'  => $this->input->post('contents'),
                'byuser' => $by_user,
            );
	

			$id = $this->Previous_Ielts_tests_model->add_p_tests($params);
			if($id){
				$this->session->set_flashdata('flsh_msg', SUCCESS_MSG);
				redirect('Previous_Ielts_tests/index');
			}else{
				$this->session->set_flashdata('flsh_msg', FAILED_MSG);
				redirect('Previous_Ielts_tests/add');
			}      
        }
        else
        {  
			
			$data['all_category_masters'] = $this->Category_master_model->get_all_category_masters_active();			
            $data['_view'] = 'pre_ielts_test/add';
            $this->load->view('layouts/main',$data);
        }
    } 
    
    
    

    /*
     * Editing a Previous Ielts tests
     */
    function edit($id)
    { 
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $data['all_tests'] = $this->Previous_Ielts_tests_model->get_p_tests($id);
        $data['title'] = 'Edit Test';
           
        if(isset($data['all_tests'][0]['id']))
        {
			
			
            $this->load->library('form_validation');
            $this->form_validation->set_rules('exam_date','Exam date','required|trim');
            if($this->form_validation->run())     
            {   
				
                $user = $this->session->userdata('admin_login_data');
                foreach ($user as $d){$by_user=$d->id;}
                $params = array(
					'active' => $this->input->post('active'),
					'module_id'  => 1,
					'date'  => $this->input->post('exam_date'),
					'category_id'  => $this->input->post('category_id'),
					'content'  => $this->input->post('contents'),
					'byuser' => $by_user,
				);
                
                $idd = $this->Previous_Ielts_tests_model->update_p_tests($id,$params);
                if($idd){
                    $this->session->set_flashdata('flsh_msg', UPDATE_MSG);           
                    redirect('Previous_Ielts_tests/index');
                }else{
                    $this->session->set_flashdata('flsh_msg', UPDATE_FAILED_MSG);           
                    redirect('Previous_Ielts_tests/edit/'.$id);
                } 
			}else
            {				
				$data['all_category_masters'] = $this->Category_master_model->get_all_category_masters_active();			
				$data['_view'] = 'pre_ielts_test/edit';
				$this->load->view('layouts/main',$data);
                
            }               
            
             }
        else
            show_error(ITEM_NOT_EXIST);
            
        
        
    } 

    /*
     * Deleting Previous Ielts tests
     */
    function remove($id)
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $app_slides = $this->Previous_Ielts_tests_model->get_p_tests($id);
        if(isset($app_slides['id']))
        {
            $this->Previous_Ielts_tests_model->delete_p_tests($id);            
            $this->session->set_flashdata('flsh_msg', DEL_MSG);
            redirect('Previous_Ielts_tests/index');
        }
        else
            show_error(ITEM_NOT_EXIST);
    }
}
?>
