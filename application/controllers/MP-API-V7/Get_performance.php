<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Harman
 *
 **/

    use Restserver\Libraries\REST_Controller;
    defined('BASEPATH') OR exit('No direct script access allowed');
    require APPPATH . 'libraries/REST_Controller.php';
    
     
class Get_performance extends REST_Controller {
    
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
       $this->load->model('Performance_model');
       $this->load->model('Student_model');
    }
       
    /**
     * Get All test seriese from this method.
     *
     * @return Response
    */
    
    /* all practice test and mock test */
    public function index_get(){
        $token = $this->input->get_request_header('token');
        
        if (!$this->Authenticate($token)) {
            $this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND);
        }else{
			$spData         = $this->tokenRoutineCall($token);
            $student_id     = $spData['id'];
            $programe_id    = $spData['programe_id'];           
            $type           = $this->input->get_request_header('type');
            
            
            
             if($type == "practice_test"):				
				$select_type = "PARENT";				
            elseif($type == "mock_test"):				
				$select_type = "MOCK";				
            elseif($type == "listening"):				
				$select_type = "Listening";					
            elseif($type == "reading"):				
				$select_type = "Reading";				
            elseif($type == "writing"):				
				$select_type = "Writing";				
            elseif($type == "speaking"):				
				$select_type = "Speaking";				
            else:
				$select_type = "";				
			endif; 
			
			$test_given     = $this->Performance_model->get_student_given_tests($student_id , $select_type);
			
			$final_array = array();
			$graph_array = array();
			foreach($test_given as $single_test){
				
				$tsid = $single_test['test_seriese_id'];				
				$total_attempts = max(array(count($single_test['Writing']) ,count($single_test['Listening']) ,count($single_test['Reading']) ,count($single_test['Speaking'])));
				$inner_array = array("Title" => $single_test['Title'] ,"Last_attempt" => $single_test['Last_attempt'] ,"test_seriese_id" => $single_test['test_seriese_id'] ,"count_attempts" => $single_test['count_attempts']);
				$inner_array["count_attempts"] = $total_attempts;				
				$attempts                      = array();			
				
				for($i=0;$i<=PRACTICE_TEST_LIMIT;$i++){
					$Listening = $Reading = $Writing = $Speaking = 0;					
					
					if(isset($single_test['Listening'][$i]))
						$Listening = $single_test['Listening'][$i];
					
					if(isset($single_test['Reading'][$i]))
						$Reading = $single_test['Reading'][$i];
					
					if(isset($single_test['Writing'][$i]))
						$Writing = $single_test['Writing'][$i];
					
					if(isset($single_test['Speaking'][$i]))
						$Speaking = $single_test['Speaking'][$i];					
					
					if($Listening == 0 && $Reading == 0 && $Writing == 0 && $Speaking == 0 ){
						
					}
					else{
						$average = (($Listening + $Reading + $Writing + $Speaking)/4);
						$overall = $this->calc_total_band($Listening , $Reading , $Writing , $Speaking);
						$cefr    = $this->calc_cefr_level($Listening , $Reading , $Writing , $Speaking);
						$attempts[] = array("Listening"=>$Listening,"Reading"=>$Reading,"Writing"=>$Writing,"Speaking"=>$Speaking,"average" => $average,"overall" => $overall , "cefr" => $cefr);
					}
				}
				
				$attempt_overall               = 0;
				
				foreach($attempts as $attempt){
					$attempt_overall = $attempt['overall'] + $attempt_overall;					
				}
				
				$average = $attempt_overall/$total_attempts;
				
				$graph_array["band_scored"][] = $inner_array["band_scored"]  = $this->round_off_IELTS($average);
				$graph_array["Goal_avg"]      = $single_test['Goal_avg'];
				$inner_array["cefr"]          = $this->calc_cefr_level($inner_array["band_scored"]);				
				$final_array[]                = $inner_array;
			}				
			
            if(!empty($final_array)){
				$data['error_message'] = ["success" => 1, "message" => "success", "data"=> $final_array , "graph_data" => $graph_array ];
			}
			else{
				$data['error_message'] = ["success" => 1, "message" => "success", "data"=> [] , "graph_data" => [] ];
			}
			
			$this->response($data, REST_Controller::HTTP_OK);
		}
        
	}

	/* Practice test and mock detail test by test Id */
	public function test_attempt_id_get($testId){
		$token = $this->input->get_request_header('token');
        
        if (!$this->Authenticate($token)) {
            $this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND);
        }else{
			$spData         = $this->tokenRoutineCall($token);
            $student_id     = $spData['id'];
            $programe_id    = $spData['programe_id'];
            
            
            $test_given     = $this->Performance_model->get_student_given_tests_by_tsid($student_id , $testId);
			
			$final_array = array();
			$graph_array = array();
			foreach($test_given as $single_test){
				
				$tsid = $single_test['test_seriese_id'];				
				$total_attempts = max(array(count($single_test['Writing']) ,count($single_test['Listening']) ,count($single_test['Reading']) ,count($single_test['Speaking'])));
				$inner_array = array("Title" => $single_test['Title'] ,"Last_attempt" => $single_test['Last_attempt'] ,"test_seriese_id" => $single_test['test_seriese_id'] ,"count_attempts" => $single_test['count_attempts']);
				$inner_array["count_attempts"] = $total_attempts;				
				$attempts                      = array();			
				
				for($i=0;$i<=PRACTICE_TEST_LIMIT;$i++){
					$Listening = $Reading = $Writing = $Speaking = 0;					
					
					if(isset($single_test['Listening'][$i]))
						$Listening = $single_test['Listening'][$i];
					
					if(isset($single_test['Reading'][$i]))
						$Reading = $single_test['Reading'][$i];
					
					if(isset($single_test['Writing'][$i]))
						$Writing = $single_test['Writing'][$i];
					
					if(isset($single_test['Speaking'][$i]))
						$Speaking = $single_test['Speaking'][$i];					
					
					if($Listening == 0 && $Reading == 0 && $Writing == 0 && $Speaking == 0 ){
						
					}
					else{
						$average = $this->round_off_IELTS(($Listening + $Reading + $Writing + $Speaking)/4);
						$overall = $this->calc_total_band($Listening , $Reading , $Writing , $Speaking);
						$cefr    = $this->calc_cefr_level($Listening , $Reading , $Writing , $Speaking);
						$attempts[] = array("Listening"=>$Listening,"Reading"=>$Reading,"Writing"=>$Writing,"Speaking"=>$Speaking,"average" => $average,"overall" => $overall , "cefr" => $cefr);
					}
				}
				
				$attempt_overall               = 0;
				
				foreach($attempts as $attempt){
					$attempt_overall = $attempt['overall'] + $attempt_overall;					
				}
				
				$average                 = $attempt_overall/$total_attempts;
				$inner_array['attempts'] = $attempts;
				$graph_array[]           = $inner_array["band_scored"]    = $this->round_off_IELTS($average);
				$inner_array["cefr"]     = $this->calc_cefr_level($inner_array["band_scored"]);				
				$final_array[]           = $inner_array;
			}
				
			
            if(!empty($final_array)){
				$data['error_message'] = ["success" => 1, "message" => "success", "data"=> $final_array , "graph_data" => $graph_array ];
			}
			else{
				$data['error_message'] = ["success" => 1, "message" => "success", "data"=> [] , "graph_data" => [] ];
			}
			
			$this->response($data, REST_Controller::HTTP_OK);
            
            
		}      
		
	}

	/* Practice test detail by attempt sequence */
	public function test_attempts_byid_seq_get($testId , $atmpt){
		$token = $this->input->get_request_header('token');
        
        if (!$this->Authenticate($token)) {
            $this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND);
        }else{
			$spData         = $this->tokenRoutineCall($token);
            $student_id     = $spData['id'];
            $programe_id    = $spData['programe_id'];           
            $test_given     = $this->Performance_model->get_student_given_tests_by_tsid_detail($student_id , $testId);
			
			$final_array = array();
			$graph_array = array();
			
			//t($test_given , 1);
			
			foreach($test_given as $single_test){
				$tsid = $single_test['test_seriese_id'];				
				$total_attempts = max(array(count($single_test['Writing']) ,count($single_test['Listening']) ,count($single_test['Reading']) ,count($single_test['Speaking'])));
				$inner_array = array("Title" => $single_test['Title'] ,"Last_attempt" => $single_test['Last_attempt'] ,"test_seriese_id" => $single_test['test_seriese_id'] ,"count_attempts" => $single_test['count_attempts']);
				$inner_array["count_attempts"] = $total_attempts;				
				$attempts                      = array();			
				
				for($i=0;$i<=PRACTICE_TEST_LIMIT;$i++){
					$Listening_bs = $Reading_bs = $Writing_bs = $Speaking_bs = 0;
					
					if(isset($single_test['Listening'][$i])){
						$Listening_std_result_id = $single_test['Listening'][$i]['student_result_id'];
						$Listening_bs = $single_test['Listening'][$i]['band_score'];
						$Listening_ms = $single_test['Listening'][$i]['marks_secured'];
						$Listening_tq = $single_test['Listening'][$i]['totalQuestion'];
					}
					if(isset($single_test['Reading'][$i])){
						$Reading_std_result_id = $single_test['Reading'][$i]['student_result_id'];
						$Reading_bs = $single_test['Reading'][$i]['band_score'];
						$Reading_ms = $single_test['Reading'][$i]['marks_secured'];
						$Reading_tq = $single_test['Reading'][$i]['totalQuestion'];
					}
					if(isset($single_test['Writing'][$i])){
						$Writing_std_result_id = $single_test['Writing'][$i]['student_result_id'];
						$Writing_bs = $single_test['Writing'][$i]['band_score'];
						$Writing_ms = $single_test['Writing'][$i]['marks_secured'];
						$Writing_tq = $single_test['Writing'][$i]['totalQuestion'];
					}
					if(isset($single_test['Speaking'][$i])){
						$Speaking_std_result_id = $single_test['Speaking'][$i]['student_result_id'];
						$Speaking_bs = $single_test['Speaking'][$i]['band_score'];
						$Speaking_ms = $single_test['Speaking'][$i]['marks_secured'];
						$Speaking_tq = $single_test['Speaking'][$i]['totalQuestion'];					
					}
					
					if($Listening_bs == 0 && $Reading_bs == 0 && $Writing_bs == 0 && $Speaking_bs == 0 ){
						
					}
					else{
						$average = $this->round_off_IELTS(($Listening_bs + $Reading_bs + $Writing_bs + $Speaking_bs)/4);
						$overall = $this->calc_total_band($Listening_bs , $Reading_bs , $Writing_bs , $Speaking_bs);
						$cefr    = $this->calc_cefr_level($Listening_bs , $Reading_bs , $Writing_bs , $Speaking_bs);
						$attempts[] = array("Listening"=>$Listening_bs,"Reading"=>$Reading_bs,"Writing"=>$Writing_bs,"Speaking"=>$Speaking_bs,"average" => $average,"overall" => $overall , "cefr" => $cefr);
						$modules[] = array("Listening"=>array("band_scored" => $Listening_bs ,"marks_secured" => $Listening_ms ,"totalQuestion" => $Listening_tq,"student_result_id" => $Listening_std_result_id),"Reading"=>array("band_scored" => $Reading_bs ,"marks_secured" => $Reading_ms ,"totalQuestion" => $Reading_tq,"student_result_id" => $Reading_std_result_id),"Writing"=>array("band_scored" => $Writing_bs ,"marks_secured" => $Writing_ms ,"totalQuestion" => $Writing_tq,"student_result_id" => $Writing_std_result_id),"Speaking"=>array("band_scored" => $Speaking_bs ,"marks_secured" => $Speaking_ms ,"totalQuestion" => $Speaking_tq,"student_result_id" => $Speaking_std_result_id));					
					}				
					
				}				
						
				$average                                 = $attempts[$atmpt]['overall'];				
				$graph_array['band_scored']['Listening'] = $attempts[$atmpt]['Listening'];
				$graph_array['band_scored']['Reading']   = $attempts[$atmpt]['Reading'];
				$graph_array['band_scored']['Writing']   = $attempts[$atmpt]['Writing'];
				$graph_array['band_scored']['Speaking']  = $attempts[$atmpt]['Speaking'];
				$graph_array['Goal_avg']                 = $single_test['Goal_avg'];				
				$inner_array['modules']                  = $modules[$atmpt];
				$inner_array["band_scored"]              = $attempts[$atmpt]['overall'];
				$inner_array["cefr"]                     = $attempts[$atmpt]['cefr'];				
				$final_array[]                           = $inner_array;
			}
				
			
            if(!empty($final_array)){
				$data['error_message'] = ["success" => 1, "message" => "success", "data"=> $final_array , "graph_data" => $graph_array ];
			}
			else{
				$data['error_message'] = ["success" => 1, "message" => "success", "data"=> [] , "graph_data" => [] ];
			}
			
			$this->response($data, REST_Controller::HTTP_OK);           
		}		
	}

	/* Module wise performance */
	
	public function module_wise_get($module_name , $testId){
		$token = $this->input->get_request_header('token');
        
        if (!$this->Authenticate($token)) {
            $this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND);
        }else{
			$spData         = $this->tokenRoutineCall($token);
            $student_id     = $spData['id'];
            $programe_id    = $spData['programe_id']; 
			$goal_column    = "";
			
			if($module_name == "Listening"):
				$goal_column = "Goal_l";
			elseif($module_name == "Reading"):
				$goal_column = "Goal_r";
			elseif($module_name == "Writing"):
				$goal_column = "Goal_w";
			else:
				$goal_column = "Goal_s";
			endif;
						
			$result_detail = $this->Performance_model->get_student_resultbyId($goal_column , $testId);
			
			
			
			t($result_detail , 1);
			
		}
		
	}
}
