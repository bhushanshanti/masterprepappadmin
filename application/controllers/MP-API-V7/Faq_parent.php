<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Harman
 *
 **/
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';   
     
class Faq_parent extends REST_Controller {  
     
    public function __construct() {
       parent::__construct();
        $this->load->database();
        $this->load->model('FAQ_master_model');
    }       
    /**
     * Get All tips_tricks_parent for test module from this method.
     *
     * @return Response
    */
    public function index_get()
    {    
        $token = $this->input->get_request_header('token');    
        if (!$this->Authenticate($token)) {
            $this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND); 
        }else{

            $spData = $this->tokenRoutineCall($token);
            $test_module_id = $spData['test_module_id'];

            $data2 = $this->FAQ_master_model->getFaqParent($test_module_id); 

            if(!empty($data2)){
              $data['error_message'] = [ "success" => 1, "message" => "success", "data"=> $data2];     
            }else{
                  $data['error_message'] = [ "success" => 0, "message" => "No Tips found!", "data"=> $data2];     
            }        
            $this->set_response($data, REST_Controller::HTTP_CREATED);
        }
    }     
    
        
}
