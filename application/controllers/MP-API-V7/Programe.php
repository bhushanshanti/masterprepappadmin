<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/

    use Restserver\Libraries\REST_Controller;
    defined('BASEPATH') OR exit('No direct script access allowed');
    require APPPATH . 'libraries/REST_Controller.php';
    
     
class Programe extends REST_Controller {
    
      /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
       $this->load->model('Programe_master_model');
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_get()
    {
        if (!$this->Authenticate($this->input->get_request_header('token'))) {
            $this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND);
        }else{  
            $pageTitle= 'PROGRAM';
            $tag = 'Add your program';
            $data2 = $this->Programe_master_model->getProgram(); 
            if(!empty($data2)){

                $data['error_message'] = [ "success" => 1, "pageTitle"=> $pageTitle, "tag"=> $tag, "message" => "success", 'programmes' => $data2 ];  
            }else{
                $data['error_message'] = [ "success" => 0, "pageTitle"=> $pageTitle, "tag"=> $tag, "message" => "No Program found!", 'programmes' => $data2 ];
            }
            $this->set_response($data, REST_Controller::HTTP_CREATED); 
        }
    }
            
}