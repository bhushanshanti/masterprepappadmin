<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/

    use Restserver\Libraries\REST_Controller;
    defined('BASEPATH') OR exit('No direct script access allowed');
    require APPPATH . 'libraries/REST_Controller.php';
    
     
class Test_seriese extends REST_Controller {
    
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
       $this->load->model('Student_model');
    }
       
    /**
     * Get All test seriese from this method.
     *
     * @return Response
    */
    public function index_get()
    {
        $token = $this->input->get_request_header('token');
        if (!$this->Authenticate($token)) {
            $this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND);
        }else{  
            
            $spData         = $this->tokenRoutineCall($token);
            $student_id     = $spData['id'];
            $programe_id    = $spData['programe_id'];
            $test_module_id = $spData['test_module_id'];
            $subTest        = $this->input->get_request_header('subTest');
			$active_package = $this->Student_model->get_student_active_package($student_id);
			$test_given     = $this->Student_model->get_student_given_tests($student_id);
			
			
			
			if(!empty($active_package)){
				$package_id = $active_package['package_id'];
				if($test_module_id == IELTS_ID){

					if($subTest=='MOCK'){ 
						$is_MtEt = 2;
					}elseif($subTest=='GRAMMAR'){
						$test_module_id = EE_ID; 
						$is_MtEt = NULL;       
					}else{
						$is_MtEt = NULL;
					}
							   
				}elseif($test_module_id == PTE_ID){
					
					if($subTest=='MOCK'){ 
						$is_MtEt = 2;
					}elseif($subTest=='GRAMMAR'){
						$test_module_id = EE_ID; 
						$is_MtEt = NULL;       
					}else{
						$is_MtEt = NULL;
					}

				}elseif($test_module_id == TOEFL_ID){

					if($subTest=='MOCK'){ 
						$is_MtEt = 2;
					}elseif($subTest=='GRAMMAR'){
						$test_module_id = EE_ID; 
						$is_MtEt = NULL;       
					}else{
						$is_MtEt = NULL;
					}

				}elseif($test_module_id == GMAT_ID){

					if($subTest=='MOCK'){ 
						$is_MtEt = 2;
					}elseif($subTest=='GRAMMAR'){
						$test_module_id = EE_ID; 
						$is_MtEt = NULL;       
					}else{
						$is_MtEt = NULL;
					}

				}else{
					$test_module_id= 0;
				}
				$this->db->select(' ts.`test_seriese_id`, ts.`test_seriese_name`, ts.`package_id`, ts.`test_seriese_type, pkm.`star_count`');				
				$this->db->from('`test_seriese` ts');
				$this->db->join('`package_masters` pkm', 'pkm.`package_id` = ts.`package_id`');
				
				/* $this->db->select('mss.`book_speaking_slots_id`, mss.`stud_id`, mss.`unique_test_id` ,mss.`band_score` ,mss.`examiner_comment` , std.`mobile`, std.`email`, std.`fname`, std.`lname`');
				$this->db->from('`mock_speaking_slots` mss');
				$this->db->join('`students` std', 'std.`id`= mss.`stud_id`' , 'left'); */
				
				if($test_module_id == EE_ID ) {
					$this->db->where(array('ts.`active`' => 1, 'ts.`test_module_id`' => $test_module_id));
				}else{
					$this->db->where(array('ts.`active' => 1, 'ts.`test_module_id`' => $test_module_id, 'ts.`programe_id`' => $programe_id, 'ts.`is_MtEt`'=> $is_MtEt));
				}
				$this->db->order_by('ts.`display_order` ASC');       
				$data2 = $this->db->get('')->result_array();
				
				
				$final_tests = array();
				
				
				//t($test_given);
				
				foreach($data2 as $testkey => $test){
					$inner_test = array();
					$inner_test = $test;
					$test_in_package = explode("," ,$test['package_id']);
					$test_seriese_id = $test['test_seriese_id'];
										
					if(in_array($package_id , $test_in_package)){
						$inner_test['retake_eligible'] = TRUE;
						
						if(isset($test_given["test_id_$test_seriese_id"])){
							 $inner_test['already_given'] = TRUE;
							
							if($test_given["test_id_$test_seriese_id"]["Listening"] >= PRACTICE_TEST_LIMIT && $test_given["test_id_$test_seriese_id"]["Reading"] >= PRACTICE_TEST_LIMIT && test_given["test_id_$test_seriese_id"]["Writing"] >= PRACTICE_TEST_LIMIT && $test_given["test_id_$test_seriese_id"]["Speaking"] >= PRACTICE_TEST_LIMIT ){
								$inner_test['retake_eligible'] = FALSE;
							}
							
							$inner_test['attempts'] = array("Listening" => $test_given["test_id_$test_seriese_id"]["Listening"] ,"Reading" => $test_given["test_id_$test_seriese_id"]["Reading"] ,"Writing" => $test_given["test_id_$test_seriese_id"]["Writing"] ,"Speaking" => $test_given["test_id_$test_seriese_id"]["Speaking"] );
							
						}else{
							$inner_test['already_given'] = FALSE;
						}
						
						$inner_test['locked'] = FALSE;						
					}
					else{						
						$inner_test['locked'] = TRUE;
					}
					
					$final_tests[] = $inner_test;	
				}
				
				
				
				if(!empty($final_tests))
					$data['error_message'] = ["success" => 1, "message" => "success", "data"=> $final_tests ];
				else
					$data['error_message'] = ["success" => 0, "message" => "No test found in your package! Please Go back.", "data"=> array() ];
					
			}
			else{
				$data['error_message'] = ["success" => 0, "message" => "No active package found.", "data"=> array() ];
			}
            $this->response($data, REST_Controller::HTTP_OK);       
        }
    }   
     
     public function test_attempts_get(){
		 
		$token = $this->input->get_request_header('token');
        if (!$this->Authenticate($token)) {
            $this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND);
        }else{  
            
            $spData          = $this->tokenRoutineCall($token);
            $student_id      = $spData['id'];
            $programe_id     = $spData['programe_id'];
            $test_seriese_id = $spData['tsid'];
            $test_seriese_id = $this->input->get_request_header('tsid');
			$test_given      = $this->Student_model->get_student_tests_by_testid($student_id , $test_seriese_id);
			
			
			$inner_test = array();
			$inner_test['retake_eligible'] = TRUE;			
			
			if(isset($test_given) && !empty($test_given)){
				if($test_given["test_id_$test_seriese_id"]["Listening"] >= PRACTICE_TEST_LIMIT && $test_given["test_id_$test_seriese_id"]["Reading"] >= PRACTICE_TEST_LIMIT && $test_given["test_id_$test_seriese_id"]["Writing"] >= PRACTICE_TEST_LIMIT && $test_given["test_id_$test_seriese_id"]["Speaking"] >= PRACTICE_TEST_LIMIT ){
					$inner_test['retake_eligible'] = FALSE;
				}

				$inner_test['attempts'] = array("Listening" => $test_given["test_id_$test_seriese_id"]["Listening"] ,"Reading" => $test_given["test_id_$test_seriese_id"]["Reading"] ,"Writing" => $test_given["test_id_$test_seriese_id"]["Writing"] ,"Speaking" => $test_given["test_id_$test_seriese_id"]["Speaking"] );
				
				$data['error_message'] = ["success" => 1, "message" => "success", "data"=> $inner_test ];
			}else{
				$data['error_message'] = ["success" => 1, "message" => "error", "data"=> [] ];
			}
			$this->response($data, REST_Controller::HTTP_OK);
		}
	}
        
}
