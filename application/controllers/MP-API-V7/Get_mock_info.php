<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Harman
 *
 **/

    use Restserver\Libraries\REST_Controller;
    defined('BASEPATH') OR exit('No direct script access allowed');
    require APPPATH . 'libraries/REST_Controller.php';
    
     
class Get_mock_info extends REST_Controller {
    
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    
    public function __construct() {
       parent::__construct();
       $this->load->database();
       $this->load->model('Static_page_model');
       $this->load->model('Student_model');
       $this->load->model('Student_results_model');
    }

    public function index_get($untest_id)
    {
		if (!$this->Authenticate($this->input->get_request_header('token'))) {
			$this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND); 
		}else{
			$token         = $this->input->get_request_header('token');
			$spData        = $this->tokenRoutineCall($token);			
			$bookings_info = $this->Student_results_model->get_bookings_information($untest_id);
			
			if(!empty($bookings_info)){
				$data['error_message'] = [ "success" => 1, "message" => "success", "data" => $bookings_info ];
			}else{
				$data['error_message'] = [ "success" => 0, "message" => "No info found", "data" => [] ];
			}
			
			

			$this->set_response($data, REST_Controller::HTTP_CREATED);
		}
		
	}
	
	public function result_get($untest_id)
    {
		if (!$this->Authenticate($this->input->get_request_header('token'))) {
			$this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND); 
		}else{
			$token           = $this->input->get_request_header('token');
			$spData          = $this->tokenRoutineCall($token);			
			$results_to_send = $this->Student_results_model->getMocktestbyId($untest_id);			
			
			$Listening = $Reading = $Writing = $Speaking = "";
			
			if(!isset($results_to_send['Listening']['band_score']) && $results_to_send['Listening']['band_score'] != "")
				$Listening = $results_to_send['Listening']['band_score'];
			
			if(!isset($results_to_send['Reading']['band_score']) && $results_to_send['Reading']['band_score'] != "")
				$Reading = $results_to_send['Reading']['band_score'];
			
			if(!isset($results_to_send['Writing']['band_score']) && $results_to_send['Writing']['band_score'] != "")
				$Writing = $results_to_send['Writing']['band_score'];
			
			if(!isset($results_to_send['Speaking']['band_score']) && $results_to_send['Speaking']['band_score'] != "")
				$Speaking = $results_to_send['Speaking']['band_score'];
			
			
			
			
			$results_to_send['overall_band'] = $this->calc_total_band($Listening , $Reading['Reading']['band_score'] , $Writing['Writing']['band_score'] , $Speaking['Speaking']['band_score'] );
			
			if(!empty($results_to_send)){
				$data['error_message'] = [ "success" => 1, "message" => "success", "data" => $results_to_send ];				
			}else{
				$data['error_message'] = [ "success" => 0, "message" => "No result found", "data" => [] ];
			}
			
			

			$this->set_response($data, REST_Controller::HTTP_CREATED);
		}
		
	}
}
