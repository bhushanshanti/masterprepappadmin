<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/

    use Restserver\Libraries\REST_Controller;
    defined('BASEPATH') OR exit('No direct script access allowed');
    require APPPATH . 'libraries/REST_Controller.php';    
     
class Notification_mobile extends REST_Controller { 
    
    public function __construct() {
       parent::__construct();
       $this->load->database();
       $this->load->model('Student_package_model');
    }

    /**
     * Get update notification settings from this method.
     *
     * @return Response
    */
    public function index_put()
    {     
        $token = $this->input->get_request_header('token');   
        if(!$this->Authenticate($token)) {
            $this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND); 
        }else{  

            $id = $this->input->get_request_header('id');    
            $input = $this->put();
            $input = array(
                'mobile_notification'=> $input['mobile_notification'],
                'email_notification' => $input['email_notification'],                
            );       
            if($this->db->update('students', $input, array('id'=>$id,'token'=>$token))){

                $data2['error_message'] = ['success' => 1, "message" => "Saved successfully."];
                
            }else{
                $data2['error_message'] = ['success' => 0, "message" => "Updation failed! Please Try again."];
            }
            $this->set_response($data2, REST_Controller::HTTP_CREATED);
        }
        
    }

    //get notification count unread
    public function index_get()
    {        
        if(!$this->Authenticate($this->input->get_request_header('token'))) {
            $this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND);
        }else{            
            
            $id     = $this->input->get_request_header('id');
            $spData = $this->idRoutineCall($id);       
            $programe_id = $spData['programe_id'];
            $test_module_id = $spData['test_module_id'];
            
            $this->db->select('count(notification_id) as unread_notification_count');
            $this->db->from('student_notification');
            $this->db->where(array('active'=> 1, 'read_status'=> 0,'student_id'=> $id,'programe_id'=> $programe_id ));
            $data2 =  $this->db->get()->row_array();
            $unread_notification_count = $data2['unread_notification_count'];
            
            //check pack status
            $data3 = $this->Student_package_model->getPackStatus($id,$programe_id,$test_module_id);
            $havePackage = $data3['havePackage'];
            //check pack status
            if(!empty($data2)){

                $data['error_message'] = 
                [ "success" => 1, "message" => "data found", "unread_notification_count"=> $unread_notification_count, 'havePackage'=> $havePackage];

            }else{
                $data['error_message'] = 
                [ "success" => 0, "message" => "data not found", "unread_notification_count"=> $unread_notification_count, 'havePackage'=> $havePackage];
            }
            $this->response($data, REST_Controller::HTTP_OK);
        }
    }
        
}