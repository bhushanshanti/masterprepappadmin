<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';   
     
class getCategory_bc extends REST_Controller {  
     
    public function __construct() {
       parent::__construct();
        $this->load->database();
        $this->load->model('Programe_master_model');
        $this->load->model('Category_master_model');
    }       
    /**
     * Get All category for band calculation as per programme from this method.
     *
     * @return Response
    */
    public function index_get()
    {    
        $token = $this->input->get_request_header('token');    
        if (!$this->Authenticate($token)) {
            $this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND); 
        }else{                   

            $programe_id = $this->input->get_request_header('programe'); 
            $this->db->select('`category_id`,`category_name`');
            $this->db->from('category_masters');
            $this->db->where(array('active' => 1, 'programe_id' => $programe_id));
            $this->db->group_start();
            $this->db->where('category_name',READING)
              ->or_where('category_name',LISTENING)
              ->or_where('category_name',WRITING)
              ->or_where('category_name',SPEAKING);
           $this->db->group_end();
           $this->db->order_by('modified','DESC');
           $data2 = $this->db->get('')->result_array();

            if(!empty($data2)){
              $data['error_message'] = [ "success" => 1, "message" => "success", "data"=> $data2];     
            }else{
                  $data['error_message'] = [ "success" => 0, "message" => "No category found!", "data"=> $data2];     
            }        
            $this->set_response($data, REST_Controller::HTTP_CREATED);
        }
    }     
    
        
}