<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';    
     
class Mock_test_history extends REST_Controller {
    
public function __construct() {
    
    parent::__construct();
    $this->load->database();
    $this->load->model('Notification_subject_model');
    $this->load->model('Notification_message_model');
    $this->load->model('Notification_model');
    $this->load->model('User_model');
    $this->load->model('Student_model');  
    
}

public function index_get(){

    $token = $this->input->get_request_header('token');
    if (!$this->Authenticate($token)) {
            $this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND); 
    }else{             
            $id = $this->input->get_request_header('id');
            $spData = $this->tokenRoutineCall($token);       
            $programe_id    = $spData['programe_id'];
            $test_module_id = $spData['test_module_id'];

            $mockData = $this->Student_model->getMockHistory($id,$programe_id,$test_module_id);
           
        if(!empty($mockData)){
            $data['error_message'] = [ "success" => 1, "message" => "success", "data"=> $mockData];    
        }else{
            $data['error_message'] = [ "success" => 0, "message" => "data not found", "data"=> $mockData];     
        }        
    $this->set_response($data, REST_Controller::HTTP_CREATED);
    }
}

}//class closed