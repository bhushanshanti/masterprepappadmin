<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';    
     
class Student_answer extends REST_Controller {
    
public function __construct() {
    
    error_reporting(0);
    parent::__construct();
    $this->load->database();
    $this->load->model('Student_answer_model');
    $this->load->model('Band_score_model');
    $this->load->model('Ts_cat_assoc_model'); 
    $this->load->model('Notification_subject_model'); 
    $this->load->model('Notification_message_model'); 
    $this->load->model('Notification_model');
    $this->load->model('User_model');  
    $this->load->model('Test_seriese_model');
    $this->load->model('Package_master_model');
    $this->load->model('Question_model');    
    $this->load->model('Student_model');    
    
}
/**
    * Load all paper attempts/histoy for students from this method.
    *
    * @return Response
*/
public function index_get(){

    if (!$this->Authenticate($this->input->get_request_header('token'))) {
        $this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND); 
    }else{

        $id = $this->input->get_request_header('id'); 
        $subTest = $this->input->get_request_header('subTest');

        $spData = $this->idRoutineCall($id);       
        $programe_id    = $spData['programe_id'];
        $test_module_id = $spData['test_module_id'];

        if($test_module_id == IELTS_ID){

            if($subTest=='MOCK'){
                $pageTitle= 'MOCK TEST HISTORY';
            }elseif($subTest=='GRAMMAR'){
                $pageTitle= 'GRAMMAR TEST HISTORY';
            }elseif($subTest=='ET'){
                $pageTitle= 'EVALUATION TEST HISTORY';
            }elseif($subTest=='SPI'){
                $pageTitle= 'SP TEST HISTORY';
            }
            elseif($subTest='PARENT'){
               $pageTitle= 'IELTS TEST HISTORY'; 
            }else{}
                       
        }elseif($test_module_id == PTE_ID){
            
            if($subTest=='MOCK'){
                $pageTitle= 'MOCK TEST HISTORY'; 
            }elseif($subTest=='GRAMMAR'){
                $pageTitle= 'GRAMMAR TEST HISTORY';
            }elseif($subTest=='ET'){
                $pageTitle= 'EVALUATION TEST HISTORY';
            }elseif($subTest=='SPI'){
                $pageTitle= 'SP TEST HISTORY';
            }
            elseif($subTest='PARENT'){
               $pageTitle= 'IELTS TEST HISTORY'; 
            }else{}

        }elseif($test_module_id == TOEFL_ID){

            if($subTest=='MOCK'){
                $pageTitle= 'MOCK TEST HISTORY';
            }elseif($subTest=='GRAMMAR'){
                $pageTitle= 'GRAMMAR TEST HISTORY';
            }elseif($subTest=='ET'){
                $pageTitle= 'EVALUATION TEST HISTORY';
            }elseif($subTest=='SPI'){
                $pageTitle= 'SP TEST HISTORY';
            }
            elseif($subTest='PARENT'){
               $pageTitle= 'IELTS TEST HISTORY'; 
            }else{}

        }elseif($test_module_id == GMAT_ID){

            if($subTest=='MOCK'){
                $pageTitle= 'MOCK TEST HISTORY'; 
            }elseif($subTest=='GRAMMAR'){
                $pageTitle= 'GRAMMAR TEST HISTORY';
            }elseif($subTest=='ET'){
                $pageTitle= 'EVALUATION TEST HISTORY';
            }elseif($subTest=='SPI'){
                $pageTitle= 'SP TEST HISTORY';
            }
            elseif($subTest='PARENT'){
               $pageTitle= 'IELTS TEST HISTORY'; 
            }else{}

        }else{
            $pageTitle= 'NO HISTORY';
        }
        $tag = 'Progress Report';
       
        $attemptsData = $this->Student_answer_model->get_attempts_api2($id,$test_module_id,$programe_id,$subTest);

        //print_r($attemptsData);die;
        foreach ($attemptsData as $key => $paper) {

            $pData = $this->Student_answer_model->get_attempts_testcat($paper['test_seriese_id'],$id,$test_module_id,$programe_id,$subTest);
            foreach ($pData as $key2 => $ppSet) {                
                $attemptsData[$key]['papers'][$key2] = $ppSet;
            }
        }
        if(!empty($attemptsData))
            $data['error_message'] = ["success" => 1, "pageTitle"=> $pageTitle, "tag"=> $tag, "message" => "all the paper attempts", 'attempts' => $attemptsData];
        else
            $data['error_message'] = ["success" => 0, "pageTitle"=> $pageTitle, "tag"=> $tag, "message" => "No Test attempted by you!", 'attempts' => $attemptsData];
        $this->set_response($data, REST_Controller::HTTP_CREATED);
    }          
         
}

/**
    * paper submission for all category from this method.
    *
    * @return Response
*/
public function index_post(){
	$token = $this->input->get_request_header('token');   
	if (!$this->Authenticate($token)) {
		$this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND); 
	}else{

		$spData = $this->tokenRoutineCall($token);       
		$programe_id    = $spData['programe_id'];
		$test_module_id = $spData['test_module_id'];

		$json          = file_get_contents('php://input');
		$std_data      = json_decode(file_get_contents('php://input'), true);
		$category_name = $std_data['category_name'];
		$is_MtEt       = $std_data['is_MtEt'];
		if($category_name==READING or $category_name==LISTENING){

		//READING, LISTENIG,EE CASE:
		if($is_MtEt==NULL){
		$this->read_listen($std_data,$json,$test_module_id,$programe_id);
		}elseif($is_MtEt==3){                    
		$this->sp_test_LRWS($std_data,$json,$test_module_id,$programe_id);
		}elseif($is_MtEt==2){                    
		$this->sp_test_LRWS($std_data,$json,$test_module_id,$programe_id);
		}else{ }


		}elseif($category_name==WRITING){
		//WRITING CASE:
		if($is_MtEt==NULL){
		$this->writting($std_data,$json,$test_module_id,$programe_id);
		}elseif($is_MtEt==3){
		$this->writting($std_data,$json,$test_module_id,$programe_id);
		}elseif($is_MtEt==2){                    
		$this->writting($std_data,$json,$test_module_id,$programe_id);
		}else{ }


		}elseif($category_name==SPEAKING){

		//SPEAKING case:                
		if($is_MtEt==NULL){
		$this->speaking($std_data,$json,$test_module_id,$programe_id);
		}elseif($is_MtEt==3){
		$this->sp_test_LRWS($std_data,$json,$test_module_id,$programe_id);
		}else{/*...*/}

		}elseif($category_name==EX1 or $category_name==EX2 or $category_name==EX3 or $category_name==EX4 or $category_name==EX5){

		//Grammar case:
		$test_module_id = EE_ID;
		$this->grammar_test($std_data,$json,$test_module_id,$programe_id);

		}elseif($category_name=='ET1'){

		//Evaluation test case:
		$this->et_test($std_data,$json,$test_module_id,$programe_id);
		}
		else{

		$data['error_message'] = [ 'success' => 0, 'message'=>PAPER_TYPE_ERROR, 'Total'=> NULL, 'band_score' => NULL, 'percentage'=> NULL, 'result_id' => NULL, 'basic' => [], 'QA' => [] ];
		$this->set_response($data, REST_Controller::HTTP_CREATED);
		}
	}
} 


//Reading, Listening
public function read_listen($std_data,$json,$test_module_id,$programe_id){
    
    $paper_checked = 1;
    $totalQuestion = count($std_data['std_ans']);
    $skipped = 0;
    foreach ($std_data['std_ans'] as $s) {
        if($s['student_answers']==''){
            $skipped++;
        }
    }
    $attempted          = $totalQuestion-$skipped;
    $ts_cat_assoc_id    = $std_data['ts_cat_assoc_id'];
    $category_name      = $std_data['category_name'];
    $student_id         = $std_data['student_id'];
    $time_taken         = $std_data['time_taken'];
    $max_marks          = $std_data['max_marks'];
    $category_id        = $std_data['category_id'];
    $test_seriese_id    = $std_data['test_seriese_id'];
    if(isset($std_data['un_mock_id']) && $std_data['un_mock_id'] != "")
		$un_mock_id         = $std_data['un_mock_id'];
	else
		$un_mock_id         = $std_data['un_mock_id'];
    $sum                = 0;
    $band_score         = NULL;
    $arr=array();
    $test_seriese_type = $this->get_paper_type($test_seriese_id);
    $type = 'PARENT';
    if($category_name == READING){
        
        $marks_secured = MARKS_READ;
        if($test_module_id == IELTS_ID)
            $collection_no  = PREFIX_IELTS.$category_name.'-'.time();
        elseif($test_module_id == PTE_ID)
            $collection_no  = PREFIX_PTE.$category_name.'-'.time();
        elseif($test_module_id == TOEFL_ID)
            $collection_no  = PREFIX_TOEFL.$category_name.'-'.time();
        elseif($test_module_id == GMAT_ID)
            $collection_no  = PREFIX_GMAT.$category_name.'-'.time(); 
        else
            $collection_no  = '';

    }elseif($category_name == LISTENING){
        
        $marks_secured = MARKS_LISTEN;
        if($test_module_id == IELTS_ID)
            $collection_no  = PREFIX_IELTS.$category_name.'-'.time();
        elseif($test_module_id == PTE_ID)
            $collection_no  = PREFIX_PTE.$category_name.'-'.time();
        elseif($test_module_id == TOEFL_ID)
            $collection_no  = PREFIX_TOEFL.$category_name.'-'.time();
        elseif($test_module_id == GMAT_ID)
            $collection_no  = PREFIX_GMAT.$category_name.'-'.time();
        else
            $collection_no  = '';

    }else{
        $marks_secured = MARKS_NULL;
        $collection_no  = '';
        $data['error_message'] =
        [ 'success' => 0, 'message'=> PAPER_SUBMISSION_FAILED, 'data'=>[] ];
        $this->set_response($data, REST_Controller::HTTP_CREATED);
    }

    //tran start
    $this->db->trans_start();                
    foreach ($std_data['std_ans'] as $s) {                           
        
        $res=$this->Question_model->getCorrectAns($s['question_id']);
        $x = preg_replace('/\s*,\s*/',', ', trim(preg_replace('!\s+!', ' ', $s['student_answers'])));        
        $cmp = strcasecmp($res['correct_answer'],$x);                                   
            if($cmp==0){ 

                $params = array(
                    'ts_cat_assoc_id'   => $ts_cat_assoc_id,
                    'student_id'        => $student_id,
                    'collection_no'     => $collection_no,
                    'question_id'       => $s['question_id'],
                    'student_answers'   => $x,
                    'marks_secured'     => $marks_secured            
                );            

            }else{ 

                $params = array(
                    'ts_cat_assoc_id'   => $ts_cat_assoc_id,
                    'student_id'        => $student_id,
                    'collection_no'     => $collection_no,
                    'question_id'       => $s['question_id'],
                    'student_answers'   => $x,
                    'marks_secured'     => MARKS_ZERO
                );                    
            }
            array_push($arr, $params);           
                                       
    }//for loop closed

    foreach($arr as $a){
        $this->db->query("
            INSERT INTO student_answers (ts_cat_assoc_id, student_id, collection_no,question_id,student_answers,marks_secured ) 
            VALUES (
            '".$a['ts_cat_assoc_id']."',
            '".$a['student_id']."',
            '".$a['collection_no']."',
            '".$a['question_id']."',
            '".$a['student_answers']."',
            '".$a['marks_secured']."'
            )
        ");
    }
    //get sum                           
    $sum_data=$this->Student_answer_model->get_sum($collection_no);
            if($sum_data){
                $sum = $sum_data['total'];  
            }else{$sum = NULL;}
                            
            //get band score
            if(isset($sum)){
                $per  = ($sum/$max_marks)*100;
                $percentage  = number_format($per, 1);
                $bs_data=$this->Band_score_model->get_band_score_for_total($sum,$category_id);
            }
            if($bs_data){                
                $band_score = number_format($bs_data['band_score'],1);
                $pro_id     = $bs_data['pro_id'];
                $pro_name   = $bs_data['pro_name'];                 
            }else{
                $band_score = NULL;
                $pro_id     = NULL;
                $pro_name   = NULL;
            }
    $params = array(
        'student_id'        => $student_id,
        'programe_id'       => $programe_id,
        'test_module_id'    => $test_module_id,
        'test_seriese_id'   => $test_seriese_id,
        'ts_cat_assoc_id'   => $ts_cat_assoc_id,
        'collection_no'     => $collection_no,
        'type'              => $type,
        'marks_secured'     => $sum,
        'percentage'        => $percentage,
        'time_taken'        => $time_taken,
        'band_score'        => $band_score,
        'pro_id'            => $pro_id,
        'totalQuestion'     => $totalQuestion,
        'skipped'           => $skipped,
        'attempted'         => $attempted,
        'un_mock_id'        => $un_mock_id,
        'paper_checked'     => 1,
        'json'              => $json
    );    
    $this->db->insert('student_results', $params);
    $rank_data=$this->Student_answer_model->update_rank($test_seriese_id,$ts_cat_assoc_id);
    $result_data_basic=$this->Student_answer_model->get_results_basic_api($ts_cat_assoc_id);                        
    $result_data=$this->Student_answer_model->get_results_api($collection_no,$ts_cat_assoc_id);     
    $this->db->trans_complete();

    if ($this->db->trans_status() === FALSE){

        $data['error_message'] =
        [ 'success' => 0, 'message'=>PAPER_SUBMISSION_FAILED, 'totalQuestion'=>$totalQuestion, 'skipped'=>$skipped, 'attempted'=>$attempted, 'time_taken'=>$time_taken, 'Total'=> NULL, 'band_score' => $band_score, 'pro_level'=>$pro_name, 'percentage'=> NULL, 'result_id' => NULL, 'basic' => [], 'QA' => [] ];
    }else{
        if($category_name==READING){
            $this->insert_notification(READ_SUBMISSION_NOTIFY,$student_id,$programe_id,$test_module_id,$ts_cat_assoc_id,$collection_no,$paper_checked);
        }elseif($category_name==LISTENING){
            $this->insert_notification(LISTEN_SUBMISSION_NOTIFY,$student_id,$programe_id,$test_module_id,$ts_cat_assoc_id,$collection_no,$paper_checked);
        }else{}         
        
        $data['error_message'] = 
        [ 'success'=> 1, 'message' => RESULT_SUC_MSG_RL, 'totalQuestion'=>$totalQuestion, 'skipped'=>$skipped, 'attempted'=>$attempted, 'time_taken'=>$time_taken, 'Total' => " ". $sum. "/".$max_marks." ", 'band_score' => $band_score, 'pro_level'=>$pro_name, 'percentage'=>$percentage, 'result_id' =>" ".$collection_no." ", 'basic' => $result_data_basic,'QA'=> $result_data];
    }
    if($test_seriese_type=='PAID'){
        $this->update_package_limit($student_id,$programe_id,$test_module_id);
    }
    $this->set_response($data, REST_Controller::HTTP_CREATED);
} 

//writting
public function writting($std_data,$json,$test_module_id,$programe_id){

    $paper_checked=NULL;
    $totalQuestion = count($std_data['std_ans']);
    $skipped = 0;
    foreach ($std_data['std_ans'] as $s) {
        if($s['student_answers']==''){
            $skipped++;
        }
    }
    $attempted = $totalQuestion-$skipped;

    $ts_cat_assoc_id    = $std_data['ts_cat_assoc_id'];
    $category_name      = $std_data['category_name'];
    $student_id         = $std_data['student_id'];
    $time_taken         = $std_data['time_taken'];
    $max_marks          = $std_data['max_marks'];
    $category_id        = $std_data['category_id'];
    $test_seriese_id    = $std_data['test_seriese_id'];
    
    if(isset($std_data['un_mock_id']) && $std_data['un_mock_id'] != ""){
		$un_mock_id         = $std_data['un_mock_id'];  
		$type = 'MOCK';
	}
	else{
		$un_mock_id         = "";  
		$type = 'PARENT';
	}

        if($test_module_id == IELTS_ID)
            $collection_no  = PREFIX_IELTS.$category_name.'-'.time();
        elseif($test_module_id == PTE_ID)
            $collection_no  = PREFIX_PTE.$category_name.'-'.time();
        elseif($test_module_id == TOEFL_ID)
            $collection_no  = PREFIX_TOEFL.$category_name.'-'.time();
        elseif($test_module_id == GMAT_ID)
            $collection_no  = PREFIX_GMAT.$category_name.'-'.time();
        else
            $collection_no  = '';
      
    $sum        = 0;
    $band_score = NULL;
    $arr = array();
    $test_seriese_type = $this->get_paper_type($test_seriese_id);

    //tran start
    $this->db->trans_start();
    foreach ($std_data['std_ans'] as $s) {  

        if($s['is_img']==0){
            //text case:
            $x = preg_replace('/\s*,\s*/',', ', trim(preg_replace('!\s+!', ' ', $s['student_answers'])));
            $params = array(
                'student_id'        => $student_id,
                'ts_cat_assoc_id'   => $ts_cat_assoc_id,                        
                'collection_no'     => $collection_no,
                'question_id'       => $s['question_id'],
                'student_answers'   => $x,
                'is_img'            => 0,
            );array_push($arr, $params);
        }else{
            //image case:
            $pic = $s['student_answers'];
            if($pic){

                $file_name_prefix = $student_id.'-'.$category_name.'-'.uniqid();
                $img = str_replace('data:image/jpeg;base64,', '', $pic);
                $img = str_replace(' ', '+', $img);
                $img_data = base64_decode($img);
                $file = ANSWER_IMAGE_FILE_PATH . $file_name_prefix . '.jpg';
                $pic_url= site_url($file);
                $success = file_put_contents($file, $img_data);

                $params = array(
                    'student_id'        => $student_id,
                    'ts_cat_assoc_id'   => $ts_cat_assoc_id,                        
                    'collection_no'     => $collection_no,
                    'question_id'       => $s['question_id'],
                    'student_answers'   => $pic_url,
                    'is_img'            => 1,
                );array_push($arr, $params);
            }else{

                $pic_url=NULL;
                $params = array(
                    'student_id'        => $student_id,
                    'ts_cat_assoc_id'   => $ts_cat_assoc_id,                        
                    'collection_no'     => $collection_no,
                    'question_id'       => $s['question_id'],
                    'student_answers'   => $pic_url,
                    'is_img'            => 1,
                );array_push($arr, $params);
            }
        }
    }//for loop closed
    foreach($arr as $a){
        $this->db->query("
            INSERT INTO student_answers (ts_cat_assoc_id, student_id, collection_no,question_id,is_img,student_answers ) 
            VALUES (
            '".$a['ts_cat_assoc_id']."',
            '".$a['student_id']."',
            '".$a['collection_no']."',
            '".$a['question_id']."',
            '".$a['is_img']."',
            '".$a['student_answers']."'
            )
        ");
    }
    
    $examiner_data       = $this->User_model->get_examiner();       
    $assigned_to_id      = $examiner_data['id'];
    $assigned_to_fname   = $examiner_data['fname'];
    $assigned_to_mobile  = $examiner_data['mobile'];
    $assigned_to_email   = $examiner_data['email'];
       
    $params = array(
        'student_id'        => $student_id,
        'programe_id'       => $programe_id,
        'test_module_id'    => $test_module_id,
        'test_seriese_id'   => $test_seriese_id, 
        'ts_cat_assoc_id'   => $ts_cat_assoc_id,
        'collection_no'     => $collection_no,
        'type'              => $type,
        'time_taken'        => $time_taken,
        'totalQuestion'     => $totalQuestion,
        'skipped'           => $skipped,
        'attempted'         => $attempted,
        'assigned_to'       => $assigned_to_id,
        'un_mock_id'        => $un_mock_id,
        'json'              => $json,                                      
    ); 
    $this->db->insert('student_results', $params);
    $this->User_model->update_assigned_count($assigned_to_id); 
    $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE){

            $data['error_message'] = 
            [ 'success' => 0, 'message' => PAPER_SUBMISSION_FAILED, 'totalQuestion'=>$totalQuestion, 'skipped'=>$skipped, 'attempted'=>$attempted, 'time_taken'=>$time_taken, 'Total'=>NULL, 'band_score' => NULL, 'result_id' => NULL, 'basic'=>[], 'QA'=>[] ];
        }else{           
           
           $data['error_message'] = 
            [ 'success' => 1, 'message' => RESULT_SUC_MSG_WS, 'totalQuestion'=>$totalQuestion, 'skipped'=>$skipped, 'attempted'=>$attempted, 'time_taken'=>$time_taken, 'Total'=>NULL, 'band_score' => NULL, 'result_id' =>" ".$collection_no." ", 'basic'=> [], 'QA' => [] ];

			if($type != 'MOCK'):
				$this->insert_notification(WRITE_SUBMISSION_NOTIFY,$student_id,$programe_id,$test_module_id,$ts_cat_assoc_id,$collection_no,$paper_checked); 

				/* SMS/email to checker start */
				$paper_link = base_url('student_answer/get_result/'.$collection_no.'/'.$ts_cat_assoc_id.'/'.$student_id);

				define('SMS_MESSAGE', 'Dear '.$assigned_to_fname.',%0a%0a'.$category_name.'%0aTest paper has been assigned to you. Click here%0a'.$paper_link.'%0ato check the test paper. %0a%0aRegards: %0aTeam Masterprep');

				$data_sms ="username=".API_USERNAME."&password=".API_PASSWORD."&to=".$assigned_to_mobile."&from=".API_FROM."&text=".SMS_MESSAGE;
				$response = $this->curlpostdata(API_URL, $data_sms);

				if($assigned_to_email){
					$subject = ''.$collection_no.' Test paper has been assigned';
					$mail_data = array(
					'fname'         => $assigned_to_fname,
					'collection_no'  => $collection_no,                
					'paper_link'     => $paper_link,                
					);  
					$this->sendEmail_paperAssigned($assigned_to_email,$subject,$mail_data);
				}
				/* SMS/email to checker start */
			endif;
            
        }
        if($test_seriese_type=='PAID'){
           $this->update_package_limit($student_id,$programe_id,$test_module_id); 
        }
        $this->set_response($data, REST_Controller::HTTP_CREATED);
}

//speaking
public function speaking($std_data,$json,$test_module_id,$programe_id){

    $paper_checked=NULL;
    $totalQuestion      = count($std_data['std_ans']);
    $skipped = 0;
    foreach ($std_data['std_ans'] as $s) {
        if($s['student_answers']==''){
            $skipped++;
        }
    }
    $attempted = $totalQuestion-$skipped;

    $ts_cat_assoc_id    = $std_data['ts_cat_assoc_id'];
    $category_name      = $std_data['category_name'];
    $student_id         = $std_data['student_id'];
    $time_taken         = $std_data['time_taken'];
    $max_marks          = $std_data['max_marks'];
    $category_id        = $std_data['category_id'];
    $test_seriese_id    = $std_data['test_seriese_id'];
    
    if(isset($std_data['un_mock_id']) && $std_data['un_mock_id'] != "")
		$un_mock_id         = $std_data['un_mock_id'];
	else
		$un_mock_id         = "";
		
    $arr = array(); 
    $type = 'PARENT';

        if($test_module_id == IELTS_ID)
            $collection_no  = PREFIX_IELTS.$category_name.'-'.time();
        elseif($test_module_id == PTE_ID)
                $collection_no  = PREFIX_PTE.$category_name.'-'.time();
        elseif($test_module_id == TOEFL_ID)
                $collection_no  = PREFIX_TOEFL.$category_name.'-'.time();
        elseif($test_module_id == GMAT_ID)
                $collection_no  = PREFIX_GMAT.$category_name.'-'.time();
        else
            $collection_no  = '';
   
    $sum                = 0;
    $band_score         = NULL;
    $test_seriese_type = $this->get_paper_type($test_seriese_id);
    //tran start
    $this->db->trans_start();
    foreach ($std_data['std_ans'] as $s) {                   
        
        $pic = $s['student_answers'];
        if($pic){

            $file_name_prefix = $student_id.'-'.$category_name.'-'.uniqid();
            $img = str_replace('data:image/mp3;base64,', '', $pic);
            $img = str_replace(' ', '+', $img);
            $img_data = base64_decode($img);
            $file = ANSWER_RECORD_FILE_PATH . $file_name_prefix . '.mp3';
            $pic_url= site_url($file);
            $success = file_put_contents($file, $img_data);

            $params = array(
                'student_id'        => $student_id,
                'ts_cat_assoc_id'   => $ts_cat_assoc_id,                        
                'collection_no'     => $collection_no,
                'question_id'       => $s['question_id'],
                'student_answers'   => $pic_url,
                'is_img'            => 1,
            );array_push($arr, $params);
        }else{
            $params = array(
                'student_id'        => $student_id,
                'ts_cat_assoc_id'   => $ts_cat_assoc_id,                        
                'collection_no'     => $collection_no,
                'question_id'       => $s['question_id'],
                'student_answers'   => '',
                'is_img'            => 0,
            );array_push($arr, $params);
        }

    }//for loop closed

    foreach($arr as $a){
        $this->db->query("
            INSERT INTO student_answers (ts_cat_assoc_id, student_id, collection_no,question_id,is_img,student_answers) 
            VALUES (
            '".$a['ts_cat_assoc_id']."',
            '".$a['student_id']."',
            '".$a['collection_no']."',
            '".$a['question_id']."',
            '".$a['is_img']."',
            '".$a['student_answers']."'
            )
        ");
    }
    
    $examiner_data       = $this->User_model->get_examiner();       
    $assigned_to_id      = $examiner_data['id'];
    $assigned_to_fname   = $examiner_data['fname'];
    $assigned_to_mobile  = $examiner_data['mobile'];
    $assigned_to_email   = $examiner_data['email'];

    $params = array(
        'student_id'        => $student_id,
        'programe_id'       => $programe_id,
        'test_module_id'    => $test_module_id,
        'test_seriese_id'   => $test_seriese_id,
        'ts_cat_assoc_id'   => $ts_cat_assoc_id,
        'collection_no'     => $collection_no,
        'type'              => $type,
        'time_taken'        => $time_taken,
        'totalQuestion'     => $totalQuestion,
        'skipped'           => $skipped,
        'attempted'         => $attempted,
        'assigned_to'       => $assigned_to_id,
        'un_mock_id'        => $un_mock_id,
        'json'              => $json,                                       
    ); 
    $this->db->insert('student_results', $params);  
    $this->User_model->update_assigned_count($assigned_to_id);              
    $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE){

            $data['error_message'] = 
            [ 'success' => 0, 'message' => PAPER_SUBMISSION_FAILED, 'totalQuestion'=>$totalQuestion, 'skipped'=>$skipped, 'attempted'=>$attempted, 'time_taken'=>$time_taken, 'Total'=>NULL, 'band_score' => NULL, 'result_id' => NULL, 'basic'=>[], 'QA'=>[] ];
        }else{
           
            $this->insert_notification(SPEAK_SUBMISSION_NOTIFY,$student_id,$programe_id,$test_module_id,$ts_cat_assoc_id,$collection_no,$paper_checked);            
            
            $data['error_message'] = 
            [ 'success' => 1, 'message' => RESULT_SUC_MSG_WS, 'totalQuestion'=>$totalQuestion, 'skipped'=>$skipped, 'attempted'=>$attempted, 'time_taken'=>$time_taken, 'Total'=>NULL, 'band_score' => NULL, 'result_id' =>" ".$collection_no." ", 'basic'=> [], 'QA' => [] ];
            
            /* SMS/email to checker start */
                $paper_link = base_url('student_answer/get_result/'.$collection_no.'/'.$ts_cat_assoc_id.'/'.$student_id);

                define('SMS_MESSAGE', 'Dear '.$assigned_to_fname.',%0a%0a'.$category_name.'%0aTest paper has been assigned to you. Click here%0a'.$paper_link.'%0ato check the test paper. %0a%0aRegards: %0aTeam MasterPrep');
                
                $data_sms ="username=".API_USERNAME."&password=".API_PASSWORD."&to=".$assigned_to_mobile."&from=".API_FROM."&text=".SMS_MESSAGE;
                $response = $this->curlpostdata(API_URL, $data_sms);
                if($assigned_to_email){
                    $subject = ''.$collection_no.' Test paper has been assigned';
                    $mail_data = array(
                        'fname'         => $assigned_to_fname,
                        'collection_no'  => $collection_no,                
                        'paper_link'     => $paper_link,                
                    );  
                    $this->sendEmail_paperAssigned($assigned_to_email,$subject,$mail_data);
                }
            /* SMS/email to checker start */
        }
        if($test_seriese_type=='PAID'){
           $this->update_package_limit($student_id,$programe_id,$test_module_id); 
        }
        $this->set_response($data, REST_Controller::HTTP_CREATED);

}

//grammar_test
public function grammar_test($std_data,$json,$test_module_id,$programe_id){
    
    $paper_checked=1;
    $totalQuestion      = count($std_data['std_ans']);
    $skipped = 0;
    foreach ($std_data['std_ans'] as $s) {
        if($s['student_answers']==''){
            $skipped++;
        }
    }
    $attempted          = $totalQuestion-$skipped;
    $ts_cat_assoc_id    = $std_data['ts_cat_assoc_id'];
    $category_name      = $std_data['category_name'];
    $student_id         = $std_data['student_id'];
    $time_taken         = $std_data['time_taken'];
    $max_marks          = $std_data['max_marks'];
    $category_id        = $std_data['category_id'];
    $test_seriese_id    = $std_data['test_seriese_id'];
    $sum                = 0;
    $band_score         = NULL;    
    $marks_secured      = MARKS_EE;
    $collection_no      = PREFIX_EE.'-'.time();
    $arr                = array();
    $type = 'GRAMMAR';   

    //tran start
    $this->db->trans_start();                
    foreach ($std_data['std_ans'] as $s) {   
                            
        $res=$this->Question_model->getCorrectAns($s['question_id']);
        $x = preg_replace('/\s*,\s*/',', ', trim(preg_replace('!\s+!', ' ', $s['student_answers'])));        
        $cmp = strcasecmp($res['correct_answer'],$x);                                    
            if($cmp==0){ 

                $params = array(
                    'ts_cat_assoc_id'   => $ts_cat_assoc_id,
                    'student_id'        => $student_id,
                    'collection_no'     => $collection_no,
                    'question_id'       => $s['question_id'],
                    'student_answers'   => $x,
                    'marks_secured'     => MARKS_EE            
                );array_push($arr, $params);          

            }else{ 

                $params = array(
                    'ts_cat_assoc_id'   => $ts_cat_assoc_id,
                    'student_id'        => $student_id,
                    'collection_no'     => $collection_no,
                    'question_id'       => $s['question_id'],
                    'student_answers'   => $x,
                    'marks_secured'     => MARKS_ZERO
                );array_push($arr, $params);                    
            }          
                                       
    }//for loop closed

    foreach($arr as $a){
        $this->db->query("
            INSERT INTO student_answers (ts_cat_assoc_id, student_id, collection_no,question_id,student_answers,marks_secured ) 
            VALUES (
            '".$a['ts_cat_assoc_id']."',
            '".$a['student_id']."',
            '".$a['collection_no']."',
            '".$a['question_id']."',
            '".$a['student_answers']."',
            '".$a['marks_secured']."'
            )
        ");
    }

    //get sum                           
    $sum_data=$this->Student_answer_model->get_sum($collection_no);
            if($sum_data){
                $sum = $sum_data['total'];  
            }else{$sum = NULL;}
                            
            //get band score
            if(isset($sum)){
                $per  = ($sum/$max_marks)*100;
                $percentage  = number_format($per, 1);                

                if($percentage>=10 and $percentage<=20){
                    $band_score = NULL;
                    $pro_id     = 14;
                    $pro_name   = base_url(GALLERY_IMAGE_PATH).'non_user.png';
                }elseif ($percentage>=21 and $percentage<=30){
                    $band_score = NULL;
                    $pro_id     = 13;
                    $pro_name   = base_url(GALLERY_IMAGE_PATH).'intermittent_user.png';
                }elseif ($percentage>=31 and $percentage<=40){
                    $band_score = NULL;
                    $pro_id     = 12;
                    $pro_name   = base_url(GALLERY_IMAGE_PATH).'extremely_limited_user.png';
                }elseif ($percentage>=41 and $percentage<=50){
                    $band_score = NULL;
                    $pro_id     = 11;
                    $pro_name   = base_url(GALLERY_IMAGE_PATH).'limited_user.png';
                }elseif ($percentage>=51 and $percentage<=60){
                    $band_score = NULL;
                    $pro_id     = 9;
                    $pro_name   = base_url(GALLERY_IMAGE_PATH).'modest_user.png';
                }elseif ($percentage>=61 and $percentage<=70){
                    $band_score = NULL;
                    $pro_id     = 8;
                    $pro_name   = base_url(GALLERY_IMAGE_PATH).'competent_user.png';
                }elseif ($percentage>=71 and $percentage<=80){
                    $band_score = NULL;
                    $pro_id     = 3;
                    $pro_name   = base_url(GALLERY_IMAGE_PATH).'good_user.png';
                }elseif ($percentage>=81 and $percentage<=90){
                    $band_score = NULL;
                    $pro_id     = 5;
                    $pro_name   = base_url(GALLERY_IMAGE_PATH).'very_good_user.png';
                }else if ($percentage>=91 and $percentage<=100){
                    $band_score = NULL;
                    $pro_id     = 10;
                    $pro_name   = base_url(GALLERY_IMAGE_PATH).'expert_user.png';
                }else{
                    $band_score = NULL;
                    $pro_id     = NULL;
                    $pro_name   = NULL;
                }              
                
            }
    $params = array(
        'student_id'        => $student_id,
        'programe_id'       => $programe_id,
        'test_module_id'    => $test_module_id,
        'test_seriese_id'   => $test_seriese_id,
        'ts_cat_assoc_id'   => $ts_cat_assoc_id,
        'collection_no'     => $collection_no,
        'type'              => $type,
        'marks_secured'     => $sum,
        'percentage'        => $percentage,
        'time_taken'        => $time_taken,
        'band_score'        => $band_score,
        'pro_id'            => $pro_id,
        'totalQuestion'     => $totalQuestion,
        'skipped'           => $skipped,
        'attempted'         => $attempted,
        'paper_checked'     => 1,
        'json'              => $json                                    
    );    
    $this->db->insert('student_results', $params);
    $rank_data=$this->Student_answer_model->update_rank($test_seriese_id,$ts_cat_assoc_id);
    $result_data_basic=$this->Student_answer_model->get_results_basic_api($ts_cat_assoc_id);                        
    $result_data=$this->Student_answer_model->get_results_api($collection_no,$ts_cat_assoc_id);                           
    $this->db->trans_complete();

    if ($this->db->trans_status() === FALSE){

        $data['error_message'] =
        [ 'success' => 0, 'message'=>PAPER_SUBMISSION_FAILED, 'totalQuestion'=>$totalQuestion, 'skipped'=>$skipped, 'attempted'=>$attempted, 'time_taken'=>$time_taken, 'Total'=> NULL, 'band_score' => $band_score, 'pro_level'=>$pro_name, 'percentage'=> NULL, 'result_id' => NULL, 'basic' => [], 'QA' => [] ];
    }else{
        
        $this->insert_notification(EE_SUBMISSION_NOTIFY,$student_id,$programe_id,$test_module_id,$ts_cat_assoc_id,$collection_no,$paper_checked);
        
        $data['error_message'] = 
        [ 'success'=> 1, 'message' => RESULT_SUC_MSG_RL, 'totalQuestion'=> $totalQuestion, 'skipped'=> $skipped, 'attempted'=> $attempted, 'time_taken'=> $time_taken, 'Total' => " ". $sum. "/".$max_marks." ", 'band_score' => $band_score, 'pro_level'=>$pro_name, 'percentage'=> $percentage, 'result_id' =>" ".$collection_no." ", 'basic' => $result_data_basic,'QA'=> $result_data];
    }
    $this->set_response($data, REST_Controller::HTTP_CREATED);
}

//Evaluation test
public function et_test($std_data,$json,$test_module_id,$programe_id){
    
    $paper_checked = 1;
    $totalQuestion = count($std_data['std_ans']);
    $skipped = 0;
    foreach ($std_data['std_ans'] as $s) {
        if($s['student_answers']==''){
            $skipped++;
        }
    }
    $attempted          = $totalQuestion-$skipped;
    $ts_cat_assoc_id    = $std_data['ts_cat_assoc_id'];
    $category_name      = $std_data['category_name'];
    $student_id         = $std_data['student_id'];
    $time_taken         = $std_data['time_taken'];
    $max_marks          = $std_data['max_marks'];
    $category_id        = $std_data['category_id'];
    $test_seriese_id    = $std_data['test_seriese_id'];
    $sum                = 0;
    $band_score         = NULL;    
    $marks_secured = MARKS_ET;
    $collection_no = PREFIX_ET.'-'.time();
    $arr = array();   
    $type='ET';

    //tran start
    $this->db->trans_start();                
    foreach ($std_data['std_ans'] as $s) {   
                            
        $res=$this->Question_model->getCorrectAns($s['question_id']);
        $x = preg_replace('/\s*,\s*/',', ', trim(preg_replace('!\s+!', ' ', $s['student_answers'])));        
        $cmp = strcasecmp($res['correct_answer'],$x);                                    
            if($cmp==0){ 

                $params = array(
                    'ts_cat_assoc_id'   => $ts_cat_assoc_id,
                    'student_id'        => $student_id,
                    'collection_no'     => $collection_no,
                    'question_id'       => $s['question_id'],
                    'student_answers'   => $x,
                    'marks_secured'     => MARKS_ET            
                );            

            }else{ 

                $params = array(
                    'ts_cat_assoc_id'   => $ts_cat_assoc_id,
                    'student_id'        => $student_id,
                    'collection_no'     => $collection_no,
                    'question_id'       => $s['question_id'],
                    'student_answers'   => $x,
                    'marks_secured'     => MARKS_ZERO
                );                    
            }            
            array_push($arr, $params);
                                       
    }//for loop closed
    
    foreach($arr as $a){
        $this->db->query("
            INSERT INTO student_answers (ts_cat_assoc_id, student_id, collection_no,question_id,student_answers,marks_secured ) 
            VALUES (
            '".$a['ts_cat_assoc_id']."',
            '".$a['student_id']."',
            '".$a['collection_no']."',
            '".$a['question_id']."',
            '".$a['student_answers']."',
            '".$a['marks_secured']."'
            )
        ");
    }
    $sum_data=$this->Student_answer_model->get_sum($collection_no);
            if($sum_data){
                $sum = $sum_data['total'];  
            }else{$sum = NULL;}

            //get band score
            if(isset($sum)){
                $per  = ($sum/$max_marks)*100;
                $percentage  = number_format($per, 1);                

                if($percentage>=0 and $percentage<=33){
                    $band_score = NULL;
                    $pro_id     = 14;
                    $pro_name   = base_url(GALLERY_IMAGE_PATH).'intermittent_user.png';
                    $wt = 1;
                }elseif ($percentage>=34 and $percentage<=66){
                    $band_score = NULL;
                    $pro_id     = 13;
                    $pro_name   = base_url(GALLERY_IMAGE_PATH).'modest_user.png';
                    $wt = 2;
                }elseif ($percentage>=67 and $percentage<=100){
                    $band_score = NULL;
                    $pro_id     = 12;
                    $pro_name   = base_url(GALLERY_IMAGE_PATH).'very_good_user.png';
                    $wt = 3;
                }else{
                    $band_score = NULL;
                    $pro_id     = NULL;
                    $pro_name   = NULL;
                    $wt = 0;
                }
            }
    $params = array(
        'student_id'        => $student_id,
        'programe_id'       => $programe_id,
        'test_module_id'    => $test_module_id,
        'test_seriese_id'   => $test_seriese_id,
        'ts_cat_assoc_id'   => $ts_cat_assoc_id,
        'collection_no'     => $collection_no,
        'type'              => $type,
        'marks_secured'     => $sum,
        'percentage'        => $percentage,
        'time_taken'        => $time_taken,
        'band_score'        => $band_score,
        'pro_id'            => $pro_id,
        'totalQuestion'     => $totalQuestion,
        'skipped'           => $skipped,
        'attempted'         => $attempted,
        'paper_checked'     => 1,
        'json'              => $json                                    
    );    
    $this->db->insert('student_results', $params);
    $rank_data=$this->Student_answer_model->update_rank($test_seriese_id,$ts_cat_assoc_id);
    $result_data_basic=$this->Student_answer_model->get_results_basic_api($ts_cat_assoc_id);                        
    $result_data=$this->Student_answer_model->get_results_api($collection_no,$ts_cat_assoc_id);                           
    $this->db->trans_complete();

    if ($this->db->trans_status() === FALSE){

        $data['error_message'] =
        [ 'success' => 0, 'message'=>PAPER_SUBMISSION_FAILED, 'totalQuestion'=>$totalQuestion, 'skipped'=>$skipped, 'attempted'=>$attempted, 'time_taken'=>$time_taken, 'Total'=> NULL, 'band_score' => $band_score, 'pro_level'=>$pro_name, 'percentage'=> NULL, 'result_id' => NULL, 'basic' => [], 'QA' => [],'recommended_pack'=> []
        ];
    }else{
        
        //GET Recommended PACK       
        $packData = $this->Package_master_model->get_recommended_package($programe_id,$test_module_id,$wt);
        $this->insert_notification(ET_SUBMISSION_NOTIFY,$student_id,$programe_id,$test_module_id,$ts_cat_assoc_id,$collection_no,$paper_checked);
        $PG_data = array(
            'key'=> KEY_ID,
            'currency'=> CURRENCY,             
            'name'=> COMPANY,
            'description'=> PAYDESC,
        );
        $data['error_message'] = 
        [ 'success'=> 1, 'message' => RESULT_SUC_MSG_RL, 'totalQuestion'=> $totalQuestion, 'skipped'=> $skipped, 'attempted'=> $attempted, 'time_taken'=> $time_taken, 'Total' => " ". $sum. "/".$max_marks." ", 'band_score' => $band_score, 'pro_level'=>$pro_name, 'percentage'=> $percentage, 'result_id' =>" ".$collection_no." ", 'basic' => $result_data_basic,'QA'=> $result_data,
        'recommended_pack'=> $packData, 'PG_data'=> $PG_data];
    }
    $this->set_response($data, REST_Controller::HTTP_CREATED);
}



//SP test
public function sp_test_LRWS($std_data,$json,$test_module_id,$programe_id){
    
    //t($std_data , 1);
    
    if(isset($std_data['is_MtEt']) && $std_data['is_MtEt'] == 2){
		$m_subject = MOCK_SUBMISSION_NOTIFY;
		$m_test_prefix = PREFIX_MT;
		$type = 'MOCK';
	}
	else{
		$m_subject = SP_SUBMISSION_NOTIFY;
		$m_test_prefix = PREFIX_IELTS_SP;
		$type = 'SPI';
	}
	
	//die($m_subject);
    
    $paper_checked = 1;
    $totalQuestion = count($std_data['std_ans']);
    $skipped = 0;
    foreach ($std_data['std_ans'] as $s) {
        if($s['student_answers']==''){
            $skipped++;
        }
    }
    $attempted          = $totalQuestion-$skipped;
    $ts_cat_assoc_id    = $std_data['ts_cat_assoc_id'];
    $category_name      = $std_data['category_name'];
    $student_id         = $std_data['student_id'];
    $time_taken         = $std_data['time_taken'];
    $max_marks          = $std_data['max_marks'];
    $category_id        = $std_data['category_id'];
    $test_seriese_id    = $std_data['test_seriese_id'];
    $un_mock_id         = "";
    if(isset($std_data['un_mock_id']) && $std_data['un_mock_id'] != "")
		$un_mock_id         = $std_data['un_mock_id'];
	
	$package_info = $this->Student_model->get_student_active_package($student_id);
	$package_id   = $package_info['package_id'];	
    $sum                = 0;
    $band_score         = NULL;
    $arr=array();
    

    if($category_name == READING){
        
        $marks_secured = MARKS_READ;
        if($test_module_id == IELTS_ID)
            $collection_no  = $m_test_prefix.$category_name.'-'.time();
        elseif($test_module_id == PTE_ID)
            $collection_no  = PREFIX_PTE.$category_name.'-'.time();
        elseif($test_module_id == TOEFL_ID)
            $collection_no  = PREFIX_TOEFL.$category_name.'-'.time();
        elseif($test_module_id == GMAT_ID)
            $collection_no  = PREFIX_GMAT.$category_name.'-'.time();            
        else
            $collection_no  = '';

    }elseif($category_name == LISTENING){
        
        $marks_secured = MARKS_LISTEN;
        if($test_module_id == IELTS_ID)
            $collection_no  = $m_test_prefix.$category_name.'-'.time();
        elseif($test_module_id == PTE_ID)
            $collection_no  = PREFIX_PTE.$category_name.'-'.time();
        elseif($test_module_id == TOEFL_ID)
            $collection_no  = PREFIX_TOEFL.$category_name.'-'.time();
        elseif($test_module_id == GMAT_ID)
            $collection_no  = PREFIX_GMAT.$category_name.'-'.time();            
        else
            $collection_no  = '';

    }elseif($category_name == WRITING){
        
        $marks_secured = MARKS_WRITE_SP;
        if($test_module_id == IELTS_ID)
            $collection_no  = $m_test_prefix.$category_name.'-'.time();
        elseif($test_module_id == PTE_ID)
            $collection_no  = PREFIX_PTE.$category_name.'-'.time();
        elseif($test_module_id == TOEFL_ID)
            $collection_no  = PREFIX_TOEFL.$category_name.'-'.time();
        elseif($test_module_id == GMAT_ID)
            $collection_no  = PREFIX_GMAT.$category_name.'-'.time();            
        else
            $collection_no  = '';

    }elseif($category_name == SPEAKING){
        
        $marks_secured = MARKS_SPEAK_SP;
        if($test_module_id == IELTS_ID)
            $collection_no  = $m_test_prefix.$category_name.'-'.time();
        elseif($test_module_id == PTE_ID)
            $collection_no  = PREFIX_PTE.$category_name.'-'.time();
        elseif($test_module_id == TOEFL_ID)
            $collection_no  = PREFIX_TOEFL.$category_name.'-'.time();
        elseif($test_module_id == GMAT_ID)
            $collection_no  = PREFIX_GMAT.$category_name.'-'.time();            
        else
            $collection_no  = '';

    }else{
        $marks_secured = MARKS_NULL;
        $collection_no  = '';
        $data['error_message'] =
        [ 'success' => 0, 'message'=> PAPER_SUBMISSION_FAILED, 'data'=>[] ];
        $this->set_response($data, REST_Controller::HTTP_CREATED);
    }

    //tran start
    $this->db->trans_start();                
    foreach ($std_data['std_ans'] as $s) {                           
        
        $res=$this->Question_model->getCorrectAns($s['question_id']);
        $x = preg_replace('/\s*,\s*/',', ', trim(preg_replace('!\s+!', ' ', $s['student_answers'])));        
        $cmp = strcasecmp($res['correct_answer'],$x);                                   
            if($cmp==0){ 

                $params = array(
                    'ts_cat_assoc_id'   => $ts_cat_assoc_id,
                    'student_id'        => $student_id,
                    'collection_no'     => $collection_no,
                    'question_id'       => $s['question_id'],
                    'student_answers'   => $x,
                    'marks_secured'     => $marks_secured            
                );            

            }else{ 

                $params = array(
                    'ts_cat_assoc_id'   => $ts_cat_assoc_id,
                    'student_id'        => $student_id,
                    'collection_no'     => $collection_no,
                    'question_id'       => $s['question_id'],
                    'student_answers'   => $x,
                    'marks_secured'     => MARKS_ZERO
                );                    
            }
            array_push($arr, $params);           
                                       
    }//for loop closed

    foreach($arr as $a){
        $this->db->query("
            INSERT INTO student_answers (ts_cat_assoc_id, student_id, collection_no,question_id,student_answers,marks_secured ) 
            VALUES (
            '".$a['ts_cat_assoc_id']."',
            '".$a['student_id']."',
            '".$a['collection_no']."',
            '".$a['question_id']."',
            '".$a['student_answers']."',
            '".$a['marks_secured']."'
            )
        ");
    }
    //get sum                           
    $sum_data=$this->Student_answer_model->get_sum($collection_no);
            if($sum_data){
                $sum = $sum_data['total'];  
            }else{$sum = NULL;}
                            
            //get band score
            if(isset($sum)){
                $per  = ($sum/$max_marks)*100;
                $percentage  = number_format($per, 1);
                $bs_data=$this->Band_score_model->get_band_score_for_total($sum,$category_id);
            }
            if($bs_data){                
                $band_score = number_format($bs_data['band_score'],1);
                $pro_id     = $bs_data['pro_id'];
                $pro_name   = $bs_data['pro_name'];                 
            }else{
                $band_score = NULL;
                $pro_id     = NULL;
                $pro_name   = NULL;
            }
    $params = array(
        'student_id'        => $student_id,
        'programe_id'       => $programe_id,
        'test_module_id'    => $test_module_id,
        'test_seriese_id'   => $test_seriese_id,
        'ts_cat_assoc_id'   => $ts_cat_assoc_id,
        'collection_no'     => $collection_no,
        'type'              => $type,
        'marks_secured'     => $sum,
        'percentage'        => $percentage,
        'time_taken'        => $time_taken,
        'band_score'        => $band_score,
        'pro_id'            => $pro_id,
        'totalQuestion'     => $totalQuestion,
        'skipped'           => $skipped,
        'attempted'         => $attempted,
        'un_mock_id'         => $un_mock_id,
        'package_id'         => $package_id,
        'paper_checked'     => 1,
        'json'              => $json                                    
    );    
    $this->db->insert('student_results', $params);
    $rank_data=$this->Student_answer_model->update_rank($test_seriese_id,$ts_cat_assoc_id);
    $result_data_basic=$this->Student_answer_model->get_results_basic_api($ts_cat_assoc_id);                        
    $result_data=$this->Student_answer_model->get_results_api($collection_no,$ts_cat_assoc_id);     
    $this->db->trans_complete();

    if ($this->db->trans_status() === FALSE){

        $data['error_message'] =
        [ 'success' => 0, 'message'=>PAPER_SUBMISSION_FAILED, 'totalQuestion'=>$totalQuestion, 'skipped'=>$skipped, 'attempted'=>$attempted, 'time_taken'=>$time_taken, 'Total'=> NULL, 'band_score' => $band_score, 'pro_level'=>$pro_name, 'percentage'=> NULL, 'result_id' => NULL, 'basic' => [], 'QA' => [] ];
    }else{
        
            $this->insert_notification( $m_subject ,$student_id,$programe_id,$test_module_id,$ts_cat_assoc_id,$collection_no,$paper_checked);
        
        $data['error_message'] = 
        [ 'success'=> 1, 'message' => RESULT_SUC_MSG_RLWS_SP, 'totalQuestion'=>$totalQuestion, 'skipped'=>$skipped, 'attempted'=>$attempted, 'time_taken'=> $time_taken, 'Total' => " ". $sum. "/".$max_marks." ", 'band_score' => $band_score, 'pro_level'=>$pro_name, 'percentage'=>$percentage, 'result_id' =>" ".$collection_no." ", 'basic' => $result_data_basic,'QA'=> $result_data];
    }
    $this->set_response($data, REST_Controller::HTTP_CREATED);
}


public function get_paper_type($test_seriese_id){

    $tst = $this->Test_seriese_model->get_paper_type($test_seriese_id);
    return $tst['test_seriese_type'];
}

public function update_package_limit($student_id,$programe_id,$test_module_id){
    
    $this->Package_master_model->update_package_limit($student_id,$programe_id,$test_module_id);

    $ps = $this->Package_master_model->get_package_limit_status($student_id,$programe_id,$test_module_id);
    $paper_left = $ps['paper_left'];
    if($paper_left==0){
        $this->Package_master_model->update_expiring_package_byPaperLimit($student_id,$programe_id,$test_module_id);
    }else{ }
}

public function sendEmail_paperAssigned($email,$subject,$mail_data){
         
    $this->load->library('email');
    $this->email->set_mailtype("html");
    $this->email->set_newline("\r\n");
    $this->email->from(FROM_EMAIL, FROM_NAME);
    $this->email->to($email);
    $this->email->bcc(ADMIN_EMAIL_BCC);
    $this->email->subject($subject);
    $body = $this->load->view('emails/paperAssigned.php',$mail_data,TRUE);
    $this->email->message($body);
    $this->email->send();
}

}//class closed
