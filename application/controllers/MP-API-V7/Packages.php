<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';   
     
class Packages extends REST_Controller {  
     
    public function __construct() {
       parent::__construct();
        $this->load->database();        
        $this->load->model('Package_master_model'); 
    }       
    /**
     * Get All packages for test module as per programme from this method.
     *
     * @return Response
    */
    public function index_get()
    {
        if(!$this->Authenticate($this->input->get_request_header('token'))) {
            $this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND); 
        }else{ 

            $id = $this->input->get_request_header('id');
            $spData = $this->idRoutineCall($id);       
            $programe_id    = $spData['programe_id'];
            $test_module_id = $spData['test_module_id'];

            $pageTitle= 'PACKAGES';
            $tag = 'Subscribe your package';    
            
            $PG_data = array(
              'key'=> KEY_ID,
              'currency'=> CURRENCY,              
              'name'=> COMPANY,
              'description'=> PAYDESC,
            );
            $pData = $this->Package_master_model->get_all_package($programe_id,$test_module_id,$id);

            if(!empty($pData)){
              $data['error_message'] = [ "success" => 1, "pageTitle"=> $pageTitle, "tag"=> $tag, "message" => "success", "PG_data"=> $PG_data, "data"=> $pData];    
            }else{
                  $data['error_message'] = [ "success" => 0, "pageTitle"=> $pageTitle, "tag"=> $tag, "message" => "No Packages found!", "data"=> $pData];     
            }        
            $this->set_response($data, REST_Controller::HTTP_CREATED);
        }
    }
    
    public function index_post()
    {
		if(!$this->Authenticate($this->input->get_request_header('token'))) {
            $this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND); 
        }else{ 

            $id = $this->input->get_request_header('id');
            $spData = $this->idRoutineCall($id);       
            $programe_id    = $spData['programe_id'];
            $test_module_id = $spData['test_module_id'];


			$json          = file_get_contents('php://input');
            $package_info  = json_decode(file_get_contents('php://input'), true);           
            $package_id      = $package_info['packId'];
			
            
            $pData = $this->Package_master_model->get_package_master($package_id);

            if(!empty($pData)){
              $data['error_message'] = [ "success" => 1, "message" => "success", "data"=> $pData];    
            }else{
                  $data['error_message'] = [ "success" => 0, "message" => "No Packages found!", "data"=> []];     
            }        
            $this->set_response($data, REST_Controller::HTTP_CREATED);
        }
	}
        
}
