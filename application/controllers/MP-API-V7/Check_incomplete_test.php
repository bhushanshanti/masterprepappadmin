<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';    
     
class Check_incomplete_test extends REST_Controller {
    
public function __construct() {
    parent::__construct();
    $this->load->database();
    $this->load->model('Test_seriese_model');
    $this->load->model('Ts_cat_assoc_model');
    $this->load->model('Test_module_model');
    $this->load->model('Student_results_model');
    $this->load->model('Student_package_model');
}
/**
    * load all Check_incomplete_test for students from this method.
    *
    * @return Response
*/
public function index_get(){

    if (!$this->Authenticate($this->input->get_request_header('token'))) {
        $this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND); 
    }else{

        $id = $this->input->get_request_header('id');
        $spData = $this->idRoutineCall($id);       
        $programe_id    = $spData['programe_id'];
        $test_module_id = $spData['test_module_id'];

        $ts_ids = $this->Student_results_model->get_distinct_given_test($id,$test_module_id,$programe_id);
        //print_r($ts_ids);die;
        $test_seriese_id = $ts_ids['test_seriese_id'];
        $ts_name = $this->Test_seriese_model->get_test_name($test_seriese_id);

        $test_seriese_name = $ts_name['test_seriese_name'];
        $test_module_name  = $ts_name['test_module_name'];

        if($test_module_name==IELTS or $test_module_name==PTE or $test_module_name==TOEFL or $test_module_name==GMAT){

            $ts_cat_ids = $this->Student_results_model->get_count_distinct_given_tscat($id,$test_seriese_id,$test_module_id);
            $count=count($ts_cat_ids);           
            
            if($count<4){
                $arr=array();
                foreach ($ts_cat_ids as $t) {
                $arr[] = $t['ts_cat_assoc_id'];
                }
                $incompleted_tests = $this->Student_results_model->get_incompleted_tests($arr,$test_seriese_id);
                
                //check pack status
                $data3 = $this->Student_package_model->getPackStatus($id,$programe_id,$test_module_id);                                
                $havePackage = $data3['havePackage'];
                //check pack status

                $data['error_message'] = 
                    ["success" => 1, "message" => INCOMPLETE_TESTS, "incomplete_tests" => $incompleted_tests, "alert" => 1,'havePackage'=> $havePackage];
            }else{ 
                $incompleted_tests=[];
                $data['error_message'] = 
                ["success" => 1, "message" => "complete", "incomplete_tests" => $incompleted_tests, "alert" => 0]; 
            }
            

        }else{
            $incompleted_tests=[];
            $data['error_message'] = 
                ["success" => 1, "message" => "complete", "incomplete_tests" => $incompleted_tests, "alert" => 0];
        }
        $this->set_response($data, REST_Controller::HTTP_CREATED);
    }           
         
}


}//class closed