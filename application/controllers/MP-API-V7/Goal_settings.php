<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';    
     
class Goal_settings extends REST_Controller {
    
    public function __construct() {
        
        parent::__construct();
        $this->load->database();
        $this->load->model('Student_model');
        $this->load->model('Real_test_model');
    }

/**
    * goal submission from this method.
    *
    * @return Response
*/
    public function index_post()
    {        
            $token = $this->input->get_request_header('token');
            if (!$this->Authenticate($token)) {
                $this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND); 
            }else{

                $goal_data   = json_decode(file_get_contents('php://input'),true);
                $params = array(
                    'Goal_l' => $goal_data['Goal_l'],
                    'Goal_r' => $goal_data['Goal_r'],
                    'Goal_w' => $goal_data['Goal_w'],
                    'Goal_s' => $goal_data['Goal_s'],
                    'targateDate' => $goal_data['targateDate'],
                    'Goal_avg'    => $goal_data['Goal_avg']
                ); 
                if($this->db->update('students', $params, array('token'=>$token))){

                    $goalData = $this->Student_model->getGoal($token);
                    $targateDate = $goalData['targateDate'];
                    $today = date('Y-m-d');
                    $date1 = date_create($targateDate);
                    $date2 = date_create(TODAY);
                    $diff  = date_diff($date2,$date1,FALSE);
                    $daysLeft = $diff->format("%R%a days");
                    $goalData['daysLeft']=str_replace('days', '', $daysLeft);

                    $data['error_message'] = ['success' => 1, 'message' => "Goal updated successfully.", "data"=> $goalData];
                    
                }else{
                    $data['error_message'] = ['success' => 0, 'message' => "Goal updation failed! Try again","data"=> $goalData ];
                }             
                $this->set_response($data, REST_Controller::HTTP_CREATED);            
        }
    } 

    public function index_get(){

        $token = $this->input->get_request_header('token');
        if (!$this->Authenticate($token)) {
                $this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND); 
        }else{ 
                $spData = $this->tokenRoutineCall($token);       
                //$programe_id    = $spData['programe_id'];
                $test_module_id = $spData['test_module_id'];
                $country_id = $spData['country_id'];
                //$type = $this->input->get_request_header('type');                
                $goalData = $this->Student_model->getGoal($token);
                $targateDate = $goalData['targateDate'];
                $today=date('Y-m-d');
                $date1=date_create($targateDate);
                $date2=date_create(TODAY);
                $diff  = date_diff($date2,$date1,FALSE);
                $daysLeft = $diff->format("%R%a days");
                $goalData['daysLeft']=str_replace('days', '', $daysLeft);;
                

                $realTestdates = $this->Real_test_model->getRealTestDates($test_module_id,$country_id);
            if(!empty($goalData)){
                $data['error_message'] = [ "success" => 1,  "message" => "success", "data"=> $goalData, 'realTestData'=> $realTestdates];    
            }else{
                $data['error_message'] = [ "success" => 0, "message" => "No Goal data found!", "data"=> $goalData, 'realTestdates'=> $realTestdates];     
            }        
            $this->set_response($data, REST_Controller::HTTP_CREATED);
        }
    }


}//class closed