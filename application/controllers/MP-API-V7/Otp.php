<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/
   
    use Restserver\Libraries\REST_Controller;
    defined('BASEPATH') OR exit('No direct script access allowed');
    require APPPATH . 'libraries/REST_Controller.php';    
     
class Otp extends REST_Controller {
    
	/**
     * Get verify the user OTP from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }   
    

    function index_post() {
        
        $otp         = trim($this->post('otp', TRUE)); 
        $mno         = trim($this->post('mno', TRUE));
        $countryCode = trim($this->post('countryCode', TRUE));
		$dtoken      = trim($this->post('dtoken', TRUE));
		$dtype       = trim($this->post('dtype', TRUE));
		
        if($countryCode!=''){

            $countryCodeExist = $this->db->query("SELECT country_id FROM country where phonecode = '".$countryCode."' and active = 1 ");

            if($countryCodeExist->num_rows() > 0){
                $mno2 = $countryCode.$mno;
            }else{
                $data2['error_message'] = ["success" => 0, "message" => "Invalid Country code!"];            
                return $this->set_response($data2, REST_Controller::HTTP_CREATED);
            }
            
        }else{
            $mno2='';
            $data2['error_message'] = ["success" => 0, "message" => "Please select Country code!"];            
            return $this->set_response($data2, REST_Controller::HTTP_CREATED);
        }

        if ($otp <> "" and $mno <> "") {
            $query = $this->db->query("SELECT id,token FROM students where OTP ='" . $otp . "' and active = 0 and mobile = '" . $mno . "' and country_code =  '" . $countryCode . "' ");
               
            if ($query->num_rows() > 0) {

                $userdata = $query->result();
                $activate_user_data = array('active' => 1 ,'dtoken' => $dtoken  ,'dtype' => $dtype);
                $this->db->update('students', $activate_user_data, array('id' => $userdata[0]->id));
                $data['error_message'] = 
                ["success" => 1, "message" => "OTP has been verified", "programe_id"=> 0, "id" => $userdata[0]->id, "token" => $userdata[0]->token];
                   
            }else{
                
                $data['error_message'] = 
                ["success" => 0, "message" => "Invalid OTP!"];
            }
        }else {
            $data['error_message'] = 
            ["success" => 0, "message" => "Please enter OTP!"];
        }
        $this->set_response($data, REST_Controller::HTTP_CREATED);        
    }    
    	
}
