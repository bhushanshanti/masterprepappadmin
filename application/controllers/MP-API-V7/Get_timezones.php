<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';   
     
class Get_timezones extends REST_Controller {  
     
    public function __construct() {
       parent::__construct();
        $this->load->database();        
        $this->load->model('Get_timezones_model');
    }       
    /**
     * Get All country for at enquiry from this method.
     *
     * @return Response
    */
    public function index_get()
    {

      if(!$this->Authenticate($this->input->get_request_header('token'))) {
         $this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND); 
      }else{
		  
		  
       $phonecode    = $this->input->get_request_header('phonecode');
       $timezone_data = $this->Get_timezones_model->get_all_timezonesbycountry($phonecode);       

        if(!empty($timezone_data)){
          $data['error_message'] = [ "success" => 1, "message" => "success", "data"=> $timezone_data];    
        }else{
              $data['error_message'] = [ "success" => 0, "message" => "No data found!", "data"=> $timezone_data];     
        }   
        
      
        $this->set_response($data, REST_Controller::HTTP_CREATED);
      }
          
    }
        
}
