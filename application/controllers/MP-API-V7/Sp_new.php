<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Harman
 *
 **/

    use Restserver\Libraries\REST_Controller;
    defined('BASEPATH') OR exit('No direct script access allowed');
    require APPPATH . 'libraries/REST_Controller.php';
    
     
class Sp_new extends REST_Controller {
    
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    
    public function __construct() {
       parent::__construct();
       $this->load->database();
       $this->load->model('Static_page_model');
       $this->load->model('Student_model');
    }

    public function index_get($category_id = 0, $pid = 0){
		if (!$this->Authenticate($this->input->get_request_header('token'))) {
            $this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND); 
        }else{
			$token = $this->input->get_request_header('token');
			$spData = $this->tokenRoutineCall($token);       
			$student_id     = $spData['id'];
			$programe_id    = $spData['programe_id'];
			$test_module_id = $spData['test_module_id'];
			//t($spData);
			$active_package = $this->Student_model->get_student_active_package($student_id);
			if(!empty($active_package)){
				$package_id = $active_package['package_id'];

				$category_id = $this->uri->segment(3);
				$pid = $this->uri->segment(4);
				$data2 = $this->Static_page_model->getStaticPageContents($pid,$category_id);
				
				//t($data2  , 1);
				$final_cats = array();
					
				foreach($data2 as $testkey => $test){					
					$test_in_package = explode("," ,$test['packages']);					
					$static_page_id = $test['static_page_id'];					
					if(in_array($package_id , $test_in_package)){
						$final_cats['parent'] = $test;	
						$child                = $this->Static_page_model->get_all_static_pages_child([] , $static_page_id);
						
						
						foreach($child as $tk => $sp){							
							$test_in_package = explode("," ,$sp['packages']);					
							$static_page_id = $sp['static_page_id'];					
							if(in_array($package_id , $test_in_package)){
								$final_cats['child'][]  = $sp;
							}
						}
					}
					else{
						//$inner_test['locked'] = TRUE;
					}				
					//$final_cats[] = $inner_test;	
				}
				if(!empty($final_cats)){
					$data['error_message'] = [ "success" => 1, "message" => "success", "data" => $final_cats];
				}else{
					$data['error_message'] = [ "success" => 0, "message" => "data not found","data" => []];
				}
			}
			else{
				$data['error_message'] = ["success" => 0, "message" => "No active package found.", "data"=> array() ];
			}
			$this->set_response($data, REST_Controller::HTTP_CREATED);
        }
	}
	
	public function index_post($page_id){
		if (!$this->Authenticate($this->input->get_request_header('token'))) {
            $this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND); 
        }else{
			$token = $this->input->get_request_header('token');
			$spData = $this->tokenRoutineCall($token);       
			$student_id     = $spData['id'];
			$programe_id    = $spData['programe_id'];
			$test_module_id = $spData['test_module_id'];
			//t($spData);
			$active_package = $this->Student_model->get_student_active_package($student_id);
			if(!empty($active_package)){
				$package_id = $active_package['package_id'];

				$category_id = $this->uri->segment(3);
				$pid = $this->uri->segment(4);
				$data2 = $this->Static_page_model->getStaticPageByPageID($page_id);
				
				//t($data2  , 1);
				$final_cats = array();
					
				foreach($data2 as $testkey => $test){					
					$test_in_package = explode("," ,$test['packages']);					
					$static_page_id = $test['static_page_id'];					
					$template_type = $test['template_type'];
					
					$counter = 0;
					if($template_type == "Nested Listing"){
						$final_cats['parent'] = $test;	
						$child  = $this->Static_page_model->get_all_static_pages_child_nested([] , $page_id);
						foreach($child as $tk => $sp){
							if($counter == 0):
								$test_in_package = explode("," ,$sp['packages']);					
								$static_page_id = $sp['static_page_id'];					
								if(in_array($package_id , $test_in_package)){
									$in_array = $sp;
									$inner_child  = $this->Static_page_model->get_all_static_pages_child_nested([] , $static_page_id);
									foreach($inner_child as $intk => $insp){
										$inner_child_package = explode("," ,$insp['packages']);
										$in_static_page_id = $insp['static_page_id'];
										if(in_array($package_id , $inner_child_package)){
										  $inn_array = $insp;
											$inner_inner_child  = $this->Static_page_model->get_all_static_pages_child_nested([] , $in_static_page_id);
											foreach($inner_inner_child as $inntk => $innsp){
												$inner_inner_child_package = explode("," ,$innsp['packages']);	
												if(in_array($package_id , $inner_inner_child_package)){									
													$inn_array['child'][] = $innsp;									
												}
											}											
											$in_array['child'][] = $inn_array;																		
										}
									}
									$final_cats['child'][]  = $in_array;
								}
							endif;
							//$counter++;
						}
					}									
					else if(in_array($package_id , $test_in_package)){
						$final_cats['parent'] = $test;	
						$child                = $this->Static_page_model->get_all_static_pages_child([] , $page_id);						
						
						foreach($child as $tk => $sp){							
							$test_in_package = explode("," ,$sp['packages']);					
							$static_page_id = $sp['static_page_id'];					
							if(in_array($package_id , $test_in_package)){
								$final_cats['child'][]  = $sp;
							}
						}
					}
					else{
						
					}				
					
				}
				if(!empty($final_cats)){
					$data['error_message'] = [ "success" => 1, "message" => "success", "data" => $final_cats];
				}else{
					$data['error_message'] = [ "success" => 0, "message" => "data not found","data" => []];
				}
			}
			else{
				$data['error_message'] = ["success" => 0, "message" => "No active package found.", "data"=> array() ];
			}
			$this->set_response($data, REST_Controller::HTTP_CREATED);
	}
}

}
