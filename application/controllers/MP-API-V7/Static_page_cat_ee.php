<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/

    use Restserver\Libraries\REST_Controller;
    defined('BASEPATH') OR exit('No direct script access allowed');
    require APPPATH . 'libraries/REST_Controller.php';
    
     
class Static_page_cat_ee extends REST_Controller {
    
      /**
     * Get All sp catg from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
       $this->load->model('Static_page_model');
       $this->load->model('Student_model');
    }   

	public function index_get()
	{
		$token = $this->input->get_request_header('token');
		if (!$this->Authenticate($token)) {
			$this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND); 
		}else{
			
			$spData = $this->tokenRoutineCall($token); 
			$student_id     = $spData['id'];   
			$programe_id    = $spData['programe_id'];
			$test_module_id = EE_ID;        
			$active_package = $this->Student_model->get_student_active_package($student_id);

			if(!empty($active_package)){
				$package_id = $active_package['package_id'];
				$data2 = $this->Static_page_model->getStaticPageCatgEE($programe_id,$test_module_id);
				$final_cats = array();
				
				
				foreach($data2 as $testkey => $test){
					$inner_test = array();
					$inner_test = $test;
					$test_in_package = explode("," ,$test['packages']);					
					if(in_array($package_id , $test_in_package)){						
						//$inner_test['locked'] = FALSE;
						$final_cats[] = $inner_test;				
					}
					else{
						//$inner_test['locked'] = TRUE;
					}
						
				}

				if(!empty($final_cats)){	
					$data['error_message'] = [ "success" => 1, "message" => "success", "data" => $final_cats];
				}else{
					$data['error_message'] = [ "success" => 0, "message" => "data not found","data" => []];
				}

			}         
			else{
				$data['error_message'] = ["success" => 0, "message" => "No active package found.", "data"=> array() ];
			}               
			$this->set_response($data, REST_Controller::HTTP_CREATED);
		}
	}


      
    
        
}
