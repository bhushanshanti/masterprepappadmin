<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Harman
 *
 **/

    use Restserver\Libraries\REST_Controller;
    defined('BASEPATH') OR exit('No direct script access allowed');
    require APPPATH . 'libraries/REST_Controller.php';
    
     
class Advertisements extends REST_Controller {
    
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    
    public function __construct() {
       parent::__construct();
       $this->load->database();
       $this->load->model('Advertisement_model');
    }

    public function index_get()
    {        
        $data2 = $this->Advertisement_model->get_all_advertisements_active();        
        if(!empty($data2)){
          $data['error_message'] = [ "success" => 1, "message" => "success","data" => $data2];
        }else{
          $data['error_message'] = [ "success" => 0, "message" => "Data not found","data" => $data2];
        }
        $this->set_response($data, REST_Controller::HTTP_CREATED);
        
    } 
        
}
