<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/

    use Restserver\Libraries\REST_Controller;
    defined('BASEPATH') OR exit('No direct script access allowed');
    require APPPATH . 'libraries/REST_Controller.php';
    
     
class Recommended_live_lecture extends REST_Controller {
    
    /**
     * Get All LL catg+ lectures from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
       $this->load->model('Category_master_model');
       $this->load->model('Programe_master_model');
       $this->load->model('Live_lecture_model');
    }
   

    public function index_get()
    {   
        $token = $this->input->get_request_header('token');  
        if (!$this->Authenticate($token)) {
            $this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND); 
        }else{

          $spData = $this->tokenRoutineCall($token);       
          $programe_id    = $spData['programe_id'];
          $test_module_id = $spData['test_module_id'];
          $isGrammar = $this->input->get_request_header('isGrammar');//0,1

          if( $isGrammar==0 and ($test_module_id==IELTS_ID or $test_module_id==PTE_ID or $test_module_id==GMAT_ID or $test_module_id==TOEFL_ID) ){

            $LLData = $this->Live_lecture_model->recommended_live_lecture($test_module_id,$programe_id);
          }else{
              $test_module_id = EE_ID;
              $LLData = $this->Live_lecture_model->recommended_live_lecture($test_module_id,$programe_id);
          }         
          
          if(!empty($LLData)){
              $data['error_message'] = [ "success" => 1, "message" => "success", "data"=> $LLData];     
          }else{
              $data['error_message'] = [ "success" => 0, "message" => "No Recommended Lecture found!", "data"=> $LLData];     
          }      
          $this->set_response($data, REST_Controller::HTTP_CREATED);
        }
    }


      
    
        
}