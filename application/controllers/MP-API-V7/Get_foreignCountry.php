<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';   
     
class Get_foreignCountry extends REST_Controller {  
     
    public function __construct() {
       parent::__construct();
        $this->load->database();        
        $this->load->model('Center_location_model');
    }       
    /**
     * Get All country for at enquiry from this method.
     *
     * @return Response
    */
    public function index_get()
    { 
            $bData = $this->Center_location_model->getAllForeignCountry();
            if(!empty($bData)){
              $data['error_message'] = [ "success" => 1, "message" => "success", "data"=> $bData];    
            }else{
                  $data['error_message'] = [ "success" => 0, "message" => "No Branches found!", "data"=> $bData];     
            }        
            $this->set_response($data, REST_Controller::HTTP_CREATED);       
    }
        
}