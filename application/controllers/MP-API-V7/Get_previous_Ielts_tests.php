<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Harman
 *
 **/
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
    
class Get_previous_Ielts_tests extends REST_Controller {   
     
    public function __construct() {
        
       parent::__construct();
       $this->load->database();     
       $this->load->model('Previous_Ielts_tests_model');     
    }
       
    /**
     * Get All test category from this method.
     *
     * @return Response
    */
    public function index_get()
    {
		$token = $this->input->get_request_header('token');
        if (!$this->Authenticate($token)) {
            $this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND);    
        }else{ 

        $spData = $this->tokenRoutineCall($token);
        
		$student_id     = $spData['id'];
        $programe_id    = $spData['programe_id'];
        $test_module_id = $spData['test_module_id'];
        $params = array();
        $params['limit'] = $this->input->get_request_header('limit'); 
        $params['offset'] = $this->input->get_request_header('offset');
        
        $all_tests = $this->Previous_Ielts_tests_model->get_previous_tests($programe_id , $test_module_id , $student_id , $params);
        $all_shortlisted = $this->Previous_Ielts_tests_model->get_shortlisted_previous_tests($student_id);        
        $count_all_tests = $this->Previous_Ielts_tests_model->count_previous_tests($programe_id , $test_module_id , $student_id);
		$final_tests = array();
        
        foreach($all_tests as $test){
			if(in_array($test['id'] , $all_shortlisted))
				$test['shortlisted'] = true;
			else
				$test['shortlisted'] = false;
			
			
			if (strtotime($test['created']) < strtotime('-30 days'))
				$test['tag_new'] = false;
			else
				$test['tag_new'] = true;

			$final_tests[] = $test;
		}       
       
		if(!empty($final_tests)){
          $data['error_message'] = [ "success" => 1, "total_rows" => $count_all_tests, "message" => "success","data" => $final_tests];
        }else{
          $data['error_message'] = [ "success" => 0, "total_rows" => 0, "message" => "Data not found","data" => array()];
        }
        $this->set_response($data, REST_Controller::HTTP_CREATED);
		}
		
	}

	public function index_post()
    {
		$token = $this->input->get_request_header('token');
        if (!$this->Authenticate($token)) {
            $this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND);    
        }else{
			$spData = $this->tokenRoutineCall($token);   
		  
			$student_id     = $spData['id'];
			$programe_id    = $spData['programe_id'];
			$test_module_id = $spData['test_module_id'];
			
			$json            = file_get_contents('php://input');
            $shortlist_info  = json_decode(file_get_contents('php://input'), true);           
           	$test_id         = $shortlist_info['test_id'];
			
			$shortlisted = $this->Previous_Ielts_tests_model->check_if_shortlisted($test_id , $student_id);
			
			
			
			if(!empty($shortlisted)){
				/* Remove from shortlist*/
				if($this->Previous_Ielts_tests_model->remove_shortlist($test_id , $student_id))
					$return = [ "success" => 1,"shortlist" => FALSE , "message" => "Test has been removed from shortlist."];	
				else
					$return = [ "success" => 0,"shortlist" => FALSE, "message" => "Something went wrong."];
				
			}else{
				/* Add to shortlist */
				$shortlist_array = array("pre_test_id" => $test_id ,"student_id" => $student_id);
				$shortlisted = $this->Previous_Ielts_tests_model->add_shortlist($shortlist_array);
				
				if($shortlisted)
					$return = [ "success" => 1,"shortlist" => TRUE, "message" => "Test has been shortlisted."];				
				else
					$return = [ "success" => 0,"shortlist" => FALSE, "message" => "Something went wrong."];
			}
			
			
			//t($spData , 1);
		    if(!empty($return)){
			  $data['error_message'] = $return;
			}else{
			  $data['error_message'] = [ "success" => 0,"shortlist" => FALSE, "message" => "Something went wrong."];
			}
			$this->set_response($data, REST_Controller::HTTP_CREATED); 
		}
	}
		
	public function previous_test_shortlisted_get()
    {
		$token = $this->input->get_request_header('token');
        if (!$this->Authenticate($token)) {
            $this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND);    
        }else{

			$spData = $this->tokenRoutineCall($token);        
			$student_id     = $spData['id'];
			$programe_id    = $spData['programe_id'];
			$test_module_id = $spData['test_module_id'];        
			
			$all_shortlisted = $this->Previous_Ielts_tests_model->get_detailed_shortlisted_previous_tests($programe_id , $test_module_id , $student_id);
			
			
		   // t($all_shortlisted , 1);
			if(!empty($all_shortlisted)){
			  $data['error_message'] = [ "success" => 1, "message" => "success","data" => $all_shortlisted];
			}else{
			  $data['error_message'] = [ "success" => 0, "message" => "Data not found","data" => array()];
			}
			$this->set_response($data, REST_Controller::HTTP_CREATED);
		}
		
	}
	
	public function read_history_get()
    {
		$token = $this->input->get_request_header('token');
        if (!$this->Authenticate($token)) {
            $this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND);    
        }else{
			$spData = $this->tokenRoutineCall($token);   
		  
			$student_id     = $spData['id'];
			$programe_id    = $spData['programe_id'];
			$test_module_id = $spData['test_module_id'];
			
			$params = array();
			$params['limit'] = $this->input->get_request_header('limit'); 
			$params['offset'] = $this->input->get_request_header('offset');
			
			$return = $this->Previous_Ielts_tests_model->get_user_read_test($programe_id , $test_module_id , $student_id , $params);
			$all_shortlisted = $this->Previous_Ielts_tests_model->get_shortlisted_previous_tests($student_id);  
			
			$final_tests = array();
        
			foreach($return as $test){
				if(in_array($test['id'] , $all_shortlisted))
					$test['shortlisted'] = true;
				else
					$test['shortlisted'] = false;
				
				
				
					$test['tag_new'] = false;
				

				$final_tests[] = $test;
			}
			
			//t($spData , 1);
		    if(!empty($final_tests)){
			  $data['error_message'] = [ "success" => 1, "message" => "success","data" => $final_tests];
			}else{
			  $data['error_message'] = [ "success" => 0,"shortlist" => 0, "message" => "No test found in your history."];
			}
			$this->set_response($data, REST_Controller::HTTP_CREATED); 
		}
	}
		
	public function read_history_add_post()
    {
		$token = $this->input->get_request_header('token');
        if (!$this->Authenticate($token)) {
            $this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND);    
        }else{
			$spData = $this->tokenRoutineCall($token);   
		  
			$student_id     = $spData['id'];
			$programe_id    = $spData['programe_id'];
			$test_module_id = $spData['test_module_id'];
			
			$json            = file_get_contents('php://input');
            $read_list_info  = json_decode(file_get_contents('php://input'), true);           
           	$test_id         = $read_list_info['testid'];
			
			
			/* Add to shortlist */
			$readlist_array = array("pre_test_id" => $test_id ,"stud_id" => $student_id);
			$readlist = $this->Previous_Ielts_tests_model->add_readlist($readlist_array);
			
			if($readlist)
				$return = [ "success" => 1,"shortlist" => 1, "message" => "Test has been mark as read."];				
			else
				$return = [ "success" => 0,"shortlist" => 0, "message" => "Something went wrong."];
			
			
			
			//t($spData , 1);
		    if(!empty($return)){
			  $data['error_message'] = $return;
			}else{
			  $data['error_message'] = [ "success" => 0,"shortlist" => 0, "message" => "Something went wrong."];
			}
			$this->set_response($data, REST_Controller::HTTP_CREATED); 
		}
	}
	
}
