<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';    
     
class Error_email extends REST_Controller {
    
public function __construct() {
    
    error_reporting(0);
    parent::__construct();
  //  $this->load->database();
   
}

/**
    * enquiry submission from this method.
    *
    * @return Response
*/
public function index_post()
{        
       // if (!$this->Authenticate($this->input->get_request_header('token'))) {
           // $this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND); 
        //}else{
            //$json       = file_get_contents('php://input');
            $std_data   = json_decode(file_get_contents('php://input'),true);
              
            $mail_data2 = $std_data['message'];
            $enquiry_to_email='support7.it@canamgroup.com';
            $subject2='Error handling';
                    $this->sendEmailEnquiry_toStd($enquiry_to_email,$subject2,$mail_data2);
                    $data['error_message'] = [ 'success' => 1, 'message'=> ENQ_SUC ];
             //   }
                
             
            $this->set_response($data, REST_Controller::HTTP_CREATED);            
    }


public function sendEmailEnquiry_toStd($email,$subject,$mail_data){

    $this->load->library('email');
    $this->email->set_mailtype("html");
    $this->email->set_newline("\r\n");
    $this->email->from(FROM_EMAIL, FROM_NAME);         
    $this->email->to($email);
    $this->email->subject($subject);
    $body =$mail_data;
    $this->email->message($body);
    $this->email->send();
}




}//class closed
