<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/
  
    use Restserver\Libraries\REST_Controller;
    defined('BASEPATH') OR exit('No direct script access allowed');
    require APPPATH . 'libraries/REST_Controller.php';
    
     
class Student_quick_result extends REST_Controller {
    
      /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }


    /**
     * load quick result from this method.
     *
     * @return Response
    */
    public function index_get($ts_cat_assoc_id,$collection_no){

        if (!$this->Authenticate($this->input->get_request_header('token'))) {
            $this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND); 
        }else{

            $id = $this->input->get_request_header('id');
            $ts_cat_assoc_id = $this->uri->segment(3);
            $collection_no = $this->uri->segment(4);

            $this->db->select('
                sr.student_result_id,
                sr.rank,
                sr.band_score, 
                sr.marks_secured,
                CONCAT(sr.percentage, "%") as percentage,
                ts_cat.max_marks,
                cat.category_name,
                sr.time_taken,
                sr.totalQuestion,
                sr.skipped,
                sr.attempted,
                sr.paper_checked,
                sr.created,
                ts.test_seriese_name,
                pl.pro_name,
            ');
            $this->db->from('student_results sr');            
            $this->db->join('`ts_cat_assoc` ts_cat', 'sr.`ts_cat_assoc_id`= ts_cat.`ts_cat_assoc_id`');
            $this->db->join('`category_masters` cat', 'cat.`category_id`= ts_cat.`category_id`');
            $this->db->join('`proficiency_level` pl', 'pl.`pro_id`= sr.`pro_id`', 'left');
            $this->db->join('test_seriese ts', 'ts.test_seriese_id= sr.test_seriese_id');
            $this->db->where(array('sr.student_id'=>$id,'sr.ts_cat_assoc_id'=>$ts_cat_assoc_id,'sr.collection_no'=>$collection_no));

            $data2 = $this->db->get('')->row_array();
            if(!empty($data2))
            $data['error_message'] = ["success" => 1, "message" => "Your result loaded successfully", 'result' => $data2];           

            else
            $data['error_message'] = ["success" => 0, "message" => "Failed to load result!", 'result' => $data2];
            $this->set_response($data, REST_Controller::HTTP_CREATED);
        }        
    }

    
}