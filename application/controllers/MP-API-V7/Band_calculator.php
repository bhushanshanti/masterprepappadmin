<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';    
     
class Band_calculator extends REST_Controller {
    
    public function __construct() {
            
        parent::__construct();
        $this->load->database();
        $this->load->model('Band_score_model');
    }

/**
    * goal submission from this method.
    *
    * @return Response
*/
    public function index_post()
    {        
            $token = $this->input->get_request_header('token');
            if (!$this->Authenticate($token)) {
                $this->set_response(['status' => 0, 'message' => UNAUTHORIZED], REST_Controller::HTTP_NOT_FOUND); 
            }else{

                $band_data =json_decode(file_get_contents('php://input'),true);
                $params = array(                    
                    'category_id' => $band_data['categoryId'],
                    'sum'         => $band_data['sum']
                );
                $bs_data=$this->Band_score_model->get_band_score_api($params['sum'],$params['category_id'] );
                    if($bs_data){                
                        $band_score = number_format($bs_data['band_score'],1);
                    }else{
                        $band_score = NULL;
                    } 
                if($band_score){

                    $data['error_message'] = ['success' => 1, 'message' => "success", "data"=> $band_score];
                    
                }else{
                    $data['error_message'] = ['success' => 0, 'message' => "failed! Try again","data"=> $band_score ];
                }             
                $this->set_response($data, REST_Controller::HTTP_CREATED);
        }
    }
    

}//class closed