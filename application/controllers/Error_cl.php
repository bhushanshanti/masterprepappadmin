<?php
/**
 * @package         MasterPrep
 * @subpackage      Error
 * @author          Mohammad Haroon
 *
 **/ 
class Error_cl extends MY_Controller{
    
    function __construct()
    {
        parent::__construct();
        if (!$this->_is_logged_in()) {
            redirect('/login');
        }
    }
    /*
     * error page
     */
    function index()
    {       
        //$data['title']   = 'Forbidden';
        $data['heading'] = 'Forbidden'; 
        $data['message'] = 'You are not authorize to access this page !';      
        $this->load->view('error/error_forbidden', $data);
    }

    
    
}
