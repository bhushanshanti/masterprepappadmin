<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Harman uppal
 *
 **/
 
class Previous_Ielts_tests extends MY_Controller{
    
    function __construct()
    {
        parent::__construct();
        if (!$this->_is_logged_in()) {$this->session->set_userdata('lock_url', $_SERVER['REDIRECT_QUERY_STRING']); redirect('/login');}
        $this->load->model('Previous_Ielts_tests_model');	       
        $this->load->model('Static_page_model');
        $this->load->model('Question_type_model'); 
        $this->load->model('Category_master_model');
        $this->load->model('Programe_master_model');
        $this->load->model('Country_model');
    }
    /*
     * Listing of Previous Ielts tests
     */
    function index()
    {	
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $this->load->library('pagination');
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('Previous_Ielts_tests/index?');
        $config['total_rows'] = $this->Previous_Ielts_tests_model->get_all_previous_test_ac_count();
        $this->pagination->initialize($config);
        $data['title'] = 'Previous Ielts tests';
        $data['all_tests'] = $this->Previous_Ielts_tests_model->get_all_previous_test_ac($params);        
        $data['all_category_masters'] = $this->Category_master_model->get_all_category_masters_active();        
         
        $data['_view'] = 'pre_ielts_test/index'; 
        
       
        $this->load->view('layouts/main',$data);
    }
    
    function index_gt()
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $this->load->library('pagination');
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('Previous_Ielts_tests/index?');
        $config['total_rows'] = $this->Previous_Ielts_tests_model->get_all_previous_test_gt_count();
        $this->pagination->initialize($config);
        $data['title'] = 'Previous Ielts tests';
        $data['all_tests'] = $this->Previous_Ielts_tests_model->get_all_previous_test_gt($params);
        $data['all_category_masters'] = $this->Category_master_model->get_all_category_masters_active();       
       
        $data['_view'] = 'pre_ielts_test/index'; 
        
       
        $this->load->view('layouts/main',$data);
    }
    
    /*
     * Adding a new Previous Ielts tests
     */
    function add()
    {   
        //access control start
        $return_info = array();
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $this->load->library('form_validation');
        $this->form_validation->set_rules('category_id','Category','required');
        $this->form_validation->set_rules('contents','contents','required');
        $data['title'] = 'Add previous test';
        $data['all_countries'] = $this->Country_model->get_all_country();
        if($this->form_validation->run())     
        {		
            $user = $this->session->userdata('admin_login_data');
            foreach ($user as $d){$by_user=$d->id;}
            $params = array(
                'active' => $this->input->post('active'),
                'module_id'  => 1,
                'country_id'  => $this->input->post('country_id'),
                'category_id'  => $this->input->post('category_id'),
                'content'  => $this->input->post('contents'),
                'byuser' => $by_user,
            );
	
	
			//t($params , 1);
			$catId = $params['category_id'];
			$id = $this->Previous_Ielts_tests_model->add_p_tests($params);
			if($id){
				$this->session->set_flashdata('flsh_msg', SUCCESS_MSG);				
				$return_info = array("status" => "success" , "message" => SUCCESS_MSG );
				
			}else{
				$this->session->set_flashdata('flsh_msg', FAILED_MSG);
				$return_info = array("status" => "error" , "message" => FAILED_MSG );
			}
			
			echo json_encode($return_info);      
        }
        else
        {  
			
			$data['all_programe_masters'] = $this->Programe_master_model->get_all_programe_masters_active();			
            $data['_view'] = 'pre_ielts_test/add';
            $this->load->view('layouts/main',$data);
        }
    } 
    
    
    

    /*
     * Editing a Previous Ielts tests
     */
    function edit($id)
    { 
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $data['all_tests'] = $this->Previous_Ielts_tests_model->get_p_tests($id);
        $data['title'] = 'Edit Test';
        $data['all_countries'] = $this->Country_model->get_all_country();
        if(isset($data['all_tests'][0]['id']))
        {
			
			
            $this->load->library('form_validation');
            $this->form_validation->set_rules('country_id','Country','required');
            if($this->form_validation->run())     
            {   
				
                $user = $this->session->userdata('admin_login_data');
                foreach ($user as $d){$by_user=$d->id;}
                $params = array(
					'active' => $this->input->post('active'),
					'module_id'  => 1,
					'country_id'  => $this->input->post('country_id'),
					'category_id'  => $this->input->post('category_id'),
					'content'  => $this->input->post('contents'),
					'byuser' => $by_user,
				);
                
                $idd = $this->Previous_Ielts_tests_model->update_p_tests($id,$params);
                $catId = $params['category_id'];
                if($idd){
                    $this->session->set_flashdata('flsh_msg', UPDATE_MSG);           
                   
                    if($catId == 11)
						redirect('Previous_Ielts_tests/index');
					else
						redirect('Previous_Ielts_tests/index_gt');	
                    
                    
                }else{
                    $this->session->set_flashdata('flsh_msg', UPDATE_FAILED_MSG);           
                    redirect('Previous_Ielts_tests/edit/'.$id);
                } 
			}else
            {				
				$data['all_programe_masters'] = $this->Programe_master_model->get_all_programe_masters_active();	
				$data['_view'] = 'pre_ielts_test/edit';
				$this->load->view('layouts/main',$data);
                
            }               
            
             }
        else
            show_error(ITEM_NOT_EXIST);
            
        
        
    } 

    /*
     * Deleting Previous Ielts tests
     */
    function remove($id)
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $app_slides = $this->Previous_Ielts_tests_model->get_p_tests($id);
        if(isset($app_slides['id']))
        {
            $this->Previous_Ielts_tests_model->delete_p_tests($id);            
            $this->session->set_flashdata('flsh_msg', DEL_MSG);
            redirect('Previous_Ielts_tests/index');
        }
        else
            show_error(ITEM_NOT_EXIST);
    }

	
	function add_ajax()
    {
		$return_info = "";
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $this->load->library('form_validation');
        $this->form_validation->set_rules('category_id','Category','required');
        $this->form_validation->set_rules('contents','contents','required');
        $data['title'] = 'Add previous test';
        $data['all_countries'] = $this->Country_model->get_all_country();
        if($this->form_validation->run())     
        {		
            $user = $this->session->userdata('admin_login_data');
            foreach ($user as $d){$by_user=$d->id;}
            $params = array(
                'active' => $this->input->post('active'),
                'module_id'  => 1,
                'country_id'  => $this->input->post('country_id'),
                'category_id'  => $this->input->post('category_id'),
                'content'  => $this->input->post('contents'),
                'byuser' => $by_user,
            );
	
	
			//t($params , 1);
			$catId = $params['category_id'];
			$id = $this->Previous_Ielts_tests_model->add_p_tests($params);
			if($id){
				$this->session->set_flashdata('flsh_msg', SUCCESS_MSG);				
				$return_info = "success";
				
			}else{
				$this->session->set_flashdata('flsh_msg', FAILED_MSG);
				$return_info = "error";
			}
			
			
        }
        else
        {  
			
			$this->session->set_flashdata('flsh_msg', FAILED_MSG);
				$return_info = error;
        }
        echo $return_info;
	}
	
	
}
?>
