<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/
 
class User extends MY_Controller{
    
    function __construct()
    {
        parent::__construct();if (!$this->_is_logged_in()) {$this->session->set_userdata('lock_url', $_SERVER['REDIRECT_QUERY_STRING']); redirect('/login');}
        $this->load->model('User_model');
        $this->load->model('Role_model');
        $this->load->model('User_role_model');
        $this->load->model('Gender_model');
        $this->load->model('Country_model');
        $this->load->model('Get_timezones_model');
        $this->load->model('Student_model');
    } 
    /*
     * Listing of all admin user
     */
    function index()
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends

        $this->load->library('pagination');
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('user/index?');
        $config['total_rows'] = $this->User_model->get_all_user_count();
        $this->pagination->initialize($config);
        $data['title'] = 'Admin';  
        $data['user'] = $this->User_model->get_all_user($params);
        $data['_view'] = 'user/index';
        $this->load->view('layouts/main',$data);
    }

    function profile(){

        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends

        $data['title'] = 'Profile';
        $data['_view'] = 'user/profile';
        $this->load->view('layouts/main',$data);
    }

    function check_old_pwd(){

        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends

        $data = $this->session->userdata('admin_login_data');
            foreach ($data as $d) { 
                $id = $d->id;
            }
        $params = array(
            'password'   => md5($this->input->post('old_pwd', true)),                
        );
        $res = $this->User_model->check_old_pwd($id,$params['password']);
        if($res){ 
                header('Content-Type: application/json');
                $response = ['msg'=>'', 'status'=>'true'];
                echo json_encode($response);
            }else{
                header('Content-Type: application/json');
                $response = ['msg'=>'', 'status'=>'false'];
                echo json_encode($response);
            } 

    }

    function change_password()
    { 
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $data['title'] = 'Change password'; 
        $this->load->library('form_validation');        
        $this->form_validation->set_rules('op','Old password','required|max_length[12]');
        $this->form_validation->set_rules('np','New password','required|max_length[12]'); 
        $this->form_validation->set_rules('rnp','re-enter new password','required|max_length[12]|matches[np]');           

        if($this->form_validation->run())  
        {
            $data = $this->session->userdata('admin_login_data');
            foreach ($data as $d) { 
                $id = $d->id;
            } 
            $params = array(                
                'password' => md5($this->input->post('np')),              
            );            
            $id = $this->User_model->change_password($id,$params);
            if($id){                 
                $this->session->set_flashdata('flsh_msg', PWD_CHANGE_SUCCESS_MSG);                
                redirect('login/logout');
            }else{
                $this->session->set_flashdata('flsh_msg', PWD_CHANGE_FAILED_MSG);
                redirect('user/profile');
            }            
        }
        else
        {  
            $data['_view'] = 'user/profile';
            $this->load->view('layouts/main',$data);
        }

    }
    /*
     * Adding a new user
     */
    function add()
    {   
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $data['title'] = 'Add admin'; 
        $this->load->library('form_validation');		
		$this->form_validation->set_rules('email','Email','required|valid_email|max_length[60]');
		$this->form_validation->set_rules('mobile','Mobile','max_length[10]');
        $this->form_validation->set_rules('role_id','Role','required');
        $this->form_validation->set_rules('gender_name','Gender','required');      
		
		if($this->form_validation->run())     
        {   
            $user = $this->session->userdata('admin_login_data');
            foreach ($user as $d){$by_user=$d->id;}
            $plain_pwd=time();
            $params = array(
				'active'    => $this->input->post('active'),
				'time_zone'    => $this->input->post('time_zone'),
                'country_iso'    => $this->input->post('country_iso'),
				'fname'     => ucfirst($this->input->post('fname')),
				'lname'     => ucfirst($this->input->post('lname')),
                'gender'     => $this->input->post('gender_name'),
				'dob'       => $this->input->post('dob'),
				'email'     => $this->input->post('email'),
				'mobile'    => $this->input->post('mobile'),				
				'residential_address'   => $this->input->post('residential_address'),
                'password'              => md5($plain_pwd),
                'by_user' => $by_user,
            );            
            $id = $this->User_model->add_user($params);
            if($id){
                $params2=array(
                    'user_id' => $id,
                    'role_id'=> $this->input->post('role_id'),
                );
                $rid = $this->User_role_model->add_user_role($params2);    
                if ($params['email'] <> "" and $rid) {

                    $subject='Dear User, your are registered at Masterprep';
                    $username=$params['email'];                    
                   
                    $data = array(
                        'name'=> $params['fname'].' '.$params['lname'],
                        'email_message'=>'You are registered successfully at Masterprep.your login details are as follows:',
                        'username'=> $params['email'],
                        'password'=>$plain_pwd,
                        'thanks'=>'Thanks and Regards',
                        'team'=>'Team: Masterprep',
                    );
                    $this->load->library('email');
                    $this->email->set_mailtype("html");
                    $this->email->set_newline("\r\n");
                    $this->email->from('info@masterprepapp.in', 'Masterprep'); 
                    $this->email->to($params['email']);
                    $this->email->bcc(ADMIN_EMAIL_BCC);
                    $this->email->subject($subject);
                    $body = $this->load->view('emails/welcome-email-admin.php',$data,TRUE);
                    $this->email->message($body);
                    $this->email->send();
                }else{

                }

                $this->session->set_flashdata('flsh_msg', SUCCESS_MSG);
                redirect('user/index');
            }else{
                $this->session->set_flashdata('flsh_msg', FAILED_MSG);
                redirect('user/add');
            }
            
        }
        else
        {            
            $data['all_roles'] = $this->Role_model->get_all_roles_active();
            $data['all_genders'] = $this->Gender_model->get_all_gender_active();
            $data['all_countries'] = $this->Country_model->get_all_country();
            $data['_view'] = 'user/add';
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a user
     */
    function edit($id)
    {
        $data2 = $this->session->userdata('admin_login_data');
        foreach ($data2 as $d) { $uid = $d->id;$role_name=$d->name;$role_id = $d->roleid;}
        if($uid==$id or $role_name=='Admin'){  
                
        }else{
            redirect('error_cl/index');
        }
        $data['title'] = 'Edit admin/profile'; 
        $data['user'] = $this->User_model->get_user($id);
        
        if(isset($data['user']['id']))
        {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('email','Email','required|valid_email|max_length[60]');
            $this->form_validation->set_rules('mobile','Mobile','max_length[10]');
            $this->form_validation->set_rules('role_id','Role','required');
            $this->form_validation->set_rules('gender_name','Gender','required');
		
			if($this->form_validation->run())     
            {   
                $user = $this->session->userdata('admin_login_data');
                foreach ($user as $d){$by_user=$d->id;}
                $params = array(

    				'active' => $this->input->post('active'),
    				'time_zone'    => $this->input->post('time_zone'),
					'country_iso'    => $this->input->post('country_iso'),
                    'fname' => ucfirst($this->input->post('fname')),
                    'lname' => ucfirst($this->input->post('lname')),
                    'gender'     => $this->input->post('gender_name'),
                    'dob' => $this->input->post('dob'),
                    'email' => $this->input->post('email'),
                    'mobile' => $this->input->post('mobile'),               
                    'residential_address' => $this->input->post('residential_address'),
                    'by_user' => $by_user,
                );

                $idd = $this->User_model->update_user($id,$params);  
                if($idd){

                    if($role_name=='Admin'){
                        $params2=array(
                            'role_id'=> $this->input->post('role_id'),
                        );
                        $rid = $this->User_role_model->update_user_role($id,$params2);
                    }
                    
                    $this->session->set_flashdata('flsh_msg', UPDATE_MSG);
                    if($role_name=='Admin')          
                    redirect('user/index');
                    else
                    redirect('user/edit/'.$id);
                }else{
                    $this->session->set_flashdata('flsh_msg', UPDATE_FAILED_MSG);           
                    redirect('user/edit/'.$id);
                }          
                
            }
            else
            {
                $data['all_roles'] = $this->Role_model->get_all_roles_active();
                $data['user_role'] = $this->User_model->get_user_role($id);
                $data['all_genders'] = $this->Gender_model->get_all_gender_active();
                $data['all_countries'] = $this->Country_model->get_all_country();
                $data['all_zones'] = $this->Get_timezones_model->get_all_timezonesbycountry_code($data['user']['country_iso']);
                $data['_view'] = 'user/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error(ITEM_NOT_EXIST);
    }
    /*
     * Deleting user
     */
    function remove($id)
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $user = $this->User_model->get_user($id);
        if(isset($user['id']))
        {
            $this->User_model->delete_user($id);
            $this->session->set_flashdata('flsh_msg', DEL_MSG);
            redirect('user/index');
        }
        else
            show_error(ITEM_NOT_EXIST);
    }
    
    function manage_schedule(){

		//access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
       // if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
         $user = $this->session->userdata('admin_login_data');
                  
        foreach ($user as $d){$by_user=$d->id; $role_id = $d->roleid;}
        if($role_id != 12)
			redirect('error_cl/index');
		        
        
        $this->load->library('pagination');
        $params['limit'] = 10; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('user/index?');
        $config['total_rows'] = $this->User_model->get_availability_count($by_user);
        $this->pagination->initialize($config);         
        $data['availability_info'] = $this->User_model->get_availability($by_user , $params);
        $data['title'] = 'Manage schedule';
        $data['_view'] = 'user/manage_schedule';
        $this->load->view('layouts/main',$data);
    }

    function add_schedule(){
 //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
       // if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $data['title'] = 'Add schedule';
        $user = $this->session->userdata('admin_login_data');
        foreach ($user as $d){$by_user=$d->id;}      
        $data['_view'] = 'user/add_schedule';
        $this->load->view('layouts/main',$data);
    }

    function create_schedule(){
       $data_array = json_decode($_POST['data_array'] , true);
        $user = $this->session->userdata('admin_login_data');
        foreach ($user as $d){$by_user=$d->id;}      
        if(isset($by_user))
        {
            if($_POST['byuser'] == 'self')           
                $count = $this->User_model->add_availability($by_user , $data_array , $by_user);
             else
                $count = $this->User_model->add_availability('' , $data_array , $by_user);
           


           if($count)
           {
                $redirect = site_url('user/manage_schedule/');
                $this->session->set_flashdata('flsh_msg', UPDATE_MSG);           
                $data = array('status'=>'success' , 'redirect' => $redirect);
           }else{
                $redirect = site_url('user/add_schedule/');
                $this->session->set_flashdata('flsh_msg', UPDATE_FAILED_MSG);           
                $data = array('status'=>'error' , 'redirect' => $redirect);
           }

           echo json_encode($data);
        }    
    }


    function update_schedule(){
       $data_array = json_decode($_POST['data_array'] , true);
        $user = $this->session->userdata('admin_login_data');
        foreach ($user as $d){$by_user=$d->id;}      
        if(isset($by_user))
        {
            if($_POST['byuser'] == 'self')           
                $count = $this->User_model->update_availability($by_user , $data_array , $by_user);
             else
                $count = $this->User_model->update_availability('' , $data_array , $by_user);
           


           if($count)
           {
                $redirect = site_url('user/manage_schedule/');
                $this->session->set_flashdata('flsh_msg', UPDATE_MSG);           
                $data = array('status'=>'success' , 'redirect' => $redirect);
           }else{
                $redirect = site_url('user/add_schedule/');
                $this->session->set_flashdata('flsh_msg', UPDATE_FAILED_MSG);           
                $data = array('status'=>'error' , 'redirect' => $redirect);
           }

           echo json_encode($data);
        }    
    }

 /*
     * Editing a user
     */
    function edit_schedule($date)
    {
        $data2 = $this->session->userdata('admin_login_data');
        foreach ($data2 as $d) { $uid = $d->id;$role_name=$d->name;$role_id = $d->roleid;}
        $data['avail_schedule'] = $this->User_model->get_availability_bydate($uid , $date );
        $data['title'] = 'Edit schedule';
        $data['date'] = $date;
        $data['_view'] = 'user/edit_schedule';        
        $this->load->view('layouts/main',$data);
    }


     function get_slots($date){
       $html ='';



$starttime = EX_START_TIME;  // your start time
$endtime = EX_END_TIME;;  // End time
$duration = EX_SLOT_DIFF;;  // split by 30 mins

$array_of_time = array ();
$start_time    = strtotime ($starttime); //change to strtotime
$end_time      = strtotime ($endtime); //change to strtotime

$add_mins  = $duration * 60;

$counter = 0;
$k = 1;

while ($start_time <= $end_time) // loop between time
{
   $array_of_time["counter_".$counter]['start_time'] = date ("H:i", $start_time);
   $start_time += $add_mins; // to check endtie=me
   $array_of_time["counter_".$counter]['end_time'] = date ("H:i", $start_time);
   $k++;
   $counter++;
}



foreach ($array_of_time as $key => $value) {

    if(isset($value['start_time']) && isset($value['end_time'])){
        $active = "";
        $html .= '<li data-date="'.$date.'" start="'.$value['start_time'].'" end="'.$value['end_time'].'" data-time="'.$value['start_time'].'-'.$value['end_time'].'" class="cnm-select-avail-li '.$active.'">'.$value['start_time'].'-'.$value['end_time'].'</li>';
    }
}

       return $html;
    } 

    function check_schedule_dates(){
       $dates = $_REQUEST['dates'];
       $html = "";
       $booked_dates = array();
        $user = $this->session->userdata('admin_login_data');
        foreach ($user as $d){$by_user=$d->id;}  
        $availability_info = $this->User_model->get_availability($by_user , array());
        foreach ($availability_info as $key => $value) {
            $booked_dates[] = $value['date'];
        }

        $counter = 0;

       foreach ($dates as $key => $value) {
        $dates_array = explode(" ", $value);
        $single_date = $dates_array[0]." ".$dates_array[1]." ".$dates_array[2]." ".$dates_array[3];


       $f_date = date("Y-m-d", strtotime($single_date));
      
       if(in_array($f_date, $booked_dates)){
            $html .= "<div class='cnm-already-booked'><h2>$f_date</h2><p>You already added schedule for this date.</p></div>";
       }else{
        $html .= "<div class='cnm-dates-empty'><ul data-date='$f_date' data-schedule='' data-un='$counter' class='cnm-select-avail-ul'><h2>$f_date</h2>".$this->get_slots($f_date)."</ul></div>";
       }            
            $counter++;
       }

    $html .= "<div class='cnm-bottom-section'><button class='btn btn-primary cnm-save-shd-btn'>Save schedule</button></div>";
    echo $html;   

    }
    
    function remove_schedule($date)
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
       // if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
       
        if(isset($date))
        {
            $this->User_model->remove_schedule($date);
            $this->session->set_flashdata('flsh_msg', DEL_MSG);
            redirect('user/manage_schedule');
        }
        else
            show_error(ITEM_NOT_EXIST);
            
    }
    
    
    function view_bookings($date)
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        //if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        
        
        
        $data2 = $this->session->userdata('admin_login_data');
        foreach ($data2 as $d) { $uid = $d->id;$role_name=$d->name;$role_id = $d->roleid;$by_user=$d->id;}
                
        $data['avail_schedule'] = $this->User_model->get_bookings_bydate($uid , $date );
       // t($data['avail_schedule']);
        $data['title'] = 'Edit schedule';
        $data['date'] = $date;
        $data['_view'] = 'user/view_bookings';        
        $this->load->view('layouts/main',$data);
    }
    
    function update_booking_schedule($id , $date)
    {				
		if(isset($_REQUEST['update_schedule']) && $_REQUEST['update_schedule'] == "update")     
		{		
			$student_id  = $this->input->post('booked_by');	
			$examiner_id = $this->input->post('user_id');
			$meeting_url_sent = 0;	
			if($this->input->post('meeting_id') != "" || $this->input->post('meeting_url') != ""){
				$meeting_url_sent = 1;
			}
			$params = array(
				'meeting_id'       => $this->input->post('meeting_id'),
				'meeting_url'      => $this->input->post('meeting_url'),
				'band_score'       => $this->input->post('band_score'),
				'examiner_comment' => $this->input->post('examiner_comment'),
				'FC'               => $this->input->post('FC'),
				'LR'               => $this->input->post('LR'),
				'Pronunciation'    => $this->input->post('Pronunciation'),
				'GRA'              => $this->input->post('GRA'),
				'meeting_url_sent' => $meeting_url_sent,
			);
			
			
			$idd = $this->User_model->update_availabilityby_examiner($id,$params);
						
			if($idd){
				
				$spData             = $this->Student_model->get_student_info($student_id);       
				$programe_id        = $spData['programe_id'];
				$test_module_id     = $spData['test_module_id'];
				$student_fname      = $spData['fname'];
				$student_mobile     = $spData['mobile'];
				$student_email      = $spData['email'];
				$unique_test_id     = $idd['unique_test_id'];
				
				if($this->input->post('meeting_id') != "" AND $this->input->post('meeting_url_sent') == 0){
					/* Send notification to student */				
					$this->insert_notification(MOC_MEETING_UPDATE,$student_id,$programe_id,$test_module_id,"","Speaking-booking-$unique_test_id","");
			  
					/* SMS/email to checker start */
					$sep = "%0a%0a";
					$message = "You mock test speaking has been updated.$sep Meeting medium : Zoom $sep Meeting ID : ".$params['meeting_id']." $sep Meeting URL : ".$params['meeting_url'];
					
					define("SMS_MESSAGE", "Dear $student_fname, $message $sepRegards: $sep Team Masterprep");

					$data_sms ="username=".API_USERNAME."&password=".API_PASSWORD."&to=".$student_mobile."&from=".API_FROM."&text=".SMS_MESSAGE;
					$response = $this->curlpostdata(API_URL, $data_sms);
					
					$mail_data = array('fname' => $student_fname , 'speaking_date' => $date , 'meeting_id' => $params['meeting_id'] , 'meeting_url' => $params['meeting_url'] , 'meeting_medium' => "Zoom" );
					$this->sendEmail_forupdates($student_email , "Speaking booking updates" , $mail_data );
				}
				$this->session->set_flashdata('flsh_msg', UPDATE_MSG);					
			}else{
				$this->session->set_flashdata('flsh_msg', UPDATE_FAILED_MSG);					
			}
			redirect("user/view_bookings/$date");
		}
		
	}
	
	
	public function sendEmail_forupdates($email,$subject,$mail_data){         
		$this->load->library('email');
		$this->email->set_mailtype("html");
		$this->email->set_newline("\r\n");
		$this->email->from(FROM_EMAIL, FROM_NAME);
		$this->email->to($email);
		$this->email->bcc(ADMIN_EMAIL_BCC);
		$this->email->subject($subject);
		$body = $this->load->view('emails/student_updates.php',$mail_data,TRUE);
		$this->email->message($body);
		$this->email->send();
	}
    
}
