<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/ 
class Tips_tricks extends MY_Controller{
    
    function __construct()
    {
        parent::__construct();
        if (!$this->_is_logged_in()) {$this->session->set_userdata('lock_url', $_SERVER['REDIRECT_QUERY_STRING']); redirect('/login');}
        $this->load->model('Tips_tricks_model');
        $this->load->model('Tips_tricks_master_model');
    }
    /*
     * Listing of Tips_tricks Academic
     */
    function index()
    {
        $rid = $this->uri->segment(3);
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $this->load->library('pagination');
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('tips_tricks/index?');
        $config['total_rows'] = $this->Tips_tricks_model->get_all_tipstricks_count();
        $this->pagination->initialize($config);
        $data['title'] = 'Tips & Tricks';
        $data['tips_tricks'] = $this->Tips_tricks_model->get_all_tipstricks($params,$rid);
        $data['_view'] = 'tips_tricks/index';
        $this->load->view('layouts/main',$data);
    }

    
    /*
     * Adding a new Tips_tricks
     */
    function add()
    {   
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $data['title'] = 'Add Tips & Tricks';
        $this->load->library('form_validation');
		$this->form_validation->set_rules('pid','Parent subject','required');
        $this->form_validation->set_rules('question','Question','required|trim');
        $this->form_validation->set_rules('answer','Answer','required|trim');
		
		if($this->form_validation->run())     
        {   
            $user = $this->session->userdata('admin_login_data');
            foreach ($user as $d){$by_user=$d->id;}
            $params = array(
                'pid'  => $this->input->post('pid'),
				'question'  => $this->input->post('question'),
                'answer'    => $this->input->post('answer'),
                'display_order' => $this->input->post('display_order'),
                'active'    => $this->input->post('active'),
                'by_user' => $by_user,
            );            
            $id = $this->Tips_tricks_model->add_tipstricks($params);
            if($id){
                $this->session->set_flashdata('flsh_msg', SUCCESS_MSG);
                redirect('tips_tricks/index');
            }else{
                $this->session->set_flashdata('flsh_msg', FAILED_MSG);
                redirect('tips_tricks/add');
            }            
        }
        else
        { 
           $data['all_parent_masters'] = $this->Tips_tricks_master_model->get_all_tips_tricks_master_active();
            $data['_view'] = 'tips_tricks/add';
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a Tips_tricks
     */
    function edit($tips_tricks_id)
    {   
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $data['title'] = 'Edit Tips & Tricks';
        $data['tips_tricks'] = $this->Tips_tricks_model->get_tipstricks($tips_tricks_id);
        if(isset($data['tips_tricks']['tips_tricks_id']))
        {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('pid','Parent subject','required');
            $this->form_validation->set_rules('question','Question','required|trim');
            $this->form_validation->set_rules('answer','Answer','required|trim'); 			
		
			if($this->form_validation->run())     
            {   
                $user = $this->session->userdata('admin_login_data');
                foreach ($user as $d){$by_user=$d->id;}
                $params = array(
                    'pid'  => $this->input->post('pid'),
                    'question'  => $this->input->post('question'),
                    'answer'    => $this->input->post('answer'),
                    'display_order' => $this->input->post('display_order'),
                    'active'    => $this->input->post('active'),
                    'by_user' => $by_user,
                );
                $id = $this->Tips_tricks_model->update_tipstricks($tips_tricks_id,$params);

                if($id){
                    $this->session->set_flashdata('flsh_msg', UPDATE_MSG);
                    redirect('tips_tricks/index');
                }else{
                    $this->session->set_flashdata('flsh_msg', UPDATE_FAILED_MSG);           
                    redirect('tips_tricks/edit/'.$tips_tricks_id);
                }                
            }
            else
            { 
                $data['all_parent_masters'] = $this->Tips_tricks_master_model->get_all_tips_tricks_master_active();
                $data['_view'] = 'tips_tricks/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error(ITEM_NOT_EXIST);
    }   
    /*
     * Deleting tips_tricks
     */
    function remove($tips_tricks_id)
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $tips_tricks = $this->Tips_tricks_model->get_tipstricks($tips_tricks_id);
        if(isset($tips_tricks['tips_tricks_id']))
        {
            $this->Tips_tricks_model->delete_tipstricks($tips_tricks_id);
            $this->session->set_flashdata('flsh_msg', DEL_MSG);
            redirect('tips_tricks/index');
        }
        else
            show_error(ITEM_NOT_EXIST);
    }    
    
}
