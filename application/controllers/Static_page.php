<?php
   /**
    * @package         MasterPrep
    * @subpackage      Test Series
    * @author          Harman
    *
    **/ 
   class Static_page extends MY_Controller{
       function __construct()
       {
           parent::__construct();
           if (!$this->_is_logged_in()) {$this->session->set_userdata('lock_url', $_SERVER['REDIRECT_QUERY_STRING']); redirect('/login');}
           $this->load->model('Static_page_model');
           $this->load->model('Question_type_model');
           $this->load->model('Category_master_model');
           //$this->load->library('m_pdf');
       }
       /*
        * Listing of static_pages
        */
       function index($pid=0)
       {
           //access control start
           $cn = $this->router->fetch_class().''.'.php';
           $mn = $this->router->fetch_method();        
           if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
           //access control ends
           $this->load->library('pagination');
           $params['limit'] = RECORDS_PER_PAGE;
           $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
           $config = $this->config->item('pagination');
           $config['base_url'] = site_url('static_page/index?');
           $config['total_rows'] = $this->Static_page_model->get_all_static_pages_count_parent();
           $this->pagination->initialize($config);
           $data['title'] = 'Static pages- '.IELTS.'-'.ACD;
           $data['static_pages'] = $this->Static_page_model->get_all_static_pages_parent($params,$pid);
           $data['_view'] = 'static_page/index';
           $this->load->view('layouts/main',$data);
       }
   
       function index_gt($pid=0)
       {
           //access control start
           $cn = $this->router->fetch_class().''.'.php';
           $mn = $this->router->fetch_method();        
           if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
           //access control ends
           $this->load->library('pagination');
           $params['limit'] = RECORDS_PER_PAGE; 
           $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
           $config = $this->config->item('pagination');
           $config['base_url'] = site_url('static_page/index_gt?');
           $config['total_rows'] = $this->Static_page_model->get_all_static_pages_count_parent_gt();
           $this->pagination->initialize($config);
           $data['title'] = 'Static pages- '.IELTS.'-'.GT;
           $data['static_pages'] = $this->Static_page_model->get_all_static_pages_parent_gt($params,$pid);
           $data['_view'] = 'static_page/index';
           $this->load->view('layouts/main',$data);
       }
   
       function index_ee($pid=0)
       {
           //access control start
           $cn = $this->router->fetch_class().''.'.php';
           $mn = $this->router->fetch_method();        
           if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
           //access control ends
           $this->load->library('pagination');
           $params['limit'] = RECORDS_PER_PAGE; 
           $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
           $config = $this->config->item('pagination');
           $config['base_url'] = site_url('static_page/index_ee?');
           $config['total_rows'] = $this->Static_page_model->get_all_static_pages_count_parent_ee();
           $this->pagination->initialize($config);
           $data['title'] = 'Static pages- '.IELTS.'-'.ENGLISH_ESSENTIAL;
           $data['static_pages'] = $this->Static_page_model->get_all_static_pages_parent_ee($params,$pid);
           $data['_view'] = 'static_page/index';
           $this->load->view('layouts/main',$data);
       }
       /*
        * Adding a new static_page
        */
       function add()
       {   
           //access control start
           $cn = $this->router->fetch_class().''.'.php';
           $mn = $this->router->fetch_method();        
           if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
           //access control ends
           $data['title'] = 'Add static page';
           $this->load->library('form_validation');
			$this->form_validation->set_rules('category_id','Category','required');
			$this->form_validation->set_rules('template_type','Template type','required');
			$this->form_validation->set_rules('title','Title','required|trim|min_length[3]|max_length[100]');	
   			
   		if($this->form_validation->run())     
           { 
               $user = $this->session->userdata('admin_login_data');
               foreach ($user as $d){$by_user=$d->id;}
               $config['upload_path']      = STATIC_PAGE_IMAGE_PATH;
               $config['allowed_types']    = STATIC_PAGE_ALLOWED_TYPES;
               $config['encrypt_name']     = FALSE;         
               $this->load->library('upload',$config);
               if($this->upload->do_upload("image")){
                   $data_img = array('upload_data' => $this->upload->data());
                   $image= site_url(STATIC_PAGE_IMAGE_PATH).$data_img['upload_data']['file_name'];
               }else{
                   $image=NULL;
               }            
               if($this->upload->do_upload("video")){
                   $data_vid = array('upload_data' => $this->upload->data());
                   $video= site_url(STATIC_PAGE_IMAGE_PATH).$data_vid['upload_data']['file_name'];
               }else{
                   $video=NULL;
               }
   				$packages = implode("," , $this->input->post('package_id'));
                   $params = array(
                       'category_id' => $this->input->post('category_id'),
                       'question_type_id'=> $this->input->post('question_type_id'),
                       'active' => $this->input->post('active'),
                       'pid' => $this->input->post('pid'),             
                       'title' => $this->input->post('title'),
                       'image' => $image,
                       'packages' => $packages,
                       'template_type' => $this->input->post('template_type'),
                       'video' => $video,
                       'contents' => $this->input->post('contents'),
                       'audio' => $this->input->post('audio') ? $this->input->post('audio') : NULL,
                       'by_user' => $by_user
                   );
                   $id = $this->Static_page_model->add_static_page($params);
                   $catId = $params['category_id'];
                   if($id){
                       
                       $this->session->set_flashdata('flsh_msg', SUCCESS_MSG);
                       if($id and ($catId>=44 and $catId<=47))
                       redirect('static_page/index');
                       elseif($id and ($catId>=48 and $catId<=51))
                       redirect('static_page/index_gt');
                       else
                       redirect('static_page/index_ee');
                   }else{
                       $this->session->set_flashdata('flsh_msg', FAILED_MSG);
                       redirect('static_page/add');
                   }            
           }
           else
           {
   			$this->load->model('Category_master_model');
   			$data['all_category_masters'] = $this->Category_master_model->get_all_category_masters_active();
   			$this->load->model('Static_page_model');
   			$data['static_pages2'] = $this->Static_page_model->get_all_static_pages2();
               $data['all_question_types'] = $this->Question_type_model->get_all_question_types_active();
               $data['all_packages'] = $this->Category_master_model->get_all_packages();
               $data['Spage_templates'] = STATIC_PAGE_TEMPLATES;
               $data['_view'] = 'static_page/add';
               $this->load->view('layouts/main',$data);
           }
       }
       /*
        * Editing a static_page
        */
       function edit($static_page_id)
       {   
           //access control start
           $cn = $this->router->fetch_class().''.'.php';
           $mn = $this->router->fetch_method();        
           if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
           //access control ends
           $data['title'] = 'Edit static page';
           $data['static_page'] = $this->Static_page_model->get_static_page($static_page_id);
           $data['static_page_parent'] = $this->Static_page_model->get_static_page($data['static_page']['pid']);        
           if(isset($data['static_page']['static_page_id']))
           {           
               $user = $this->session->userdata('admin_login_data');
               foreach ($user as $d){$by_user=$d->id;}
               $this->load->library('form_validation');
   			$this->form_validation->set_rules('category_id','Category','required');
   			$this->form_validation->set_rules('template_type','Template type','required');
   			$this->form_validation->set_rules('title','Title','required|trim|min_length[3]|max_length[100]');
               //$this->form_validation->set_rules('icon','Icon URL','trim');
   			if($this->form_validation->run())     
               {
   				$packages = implode("," , $this->input->post('package_id'));
                   $params = array(
                       'category_id' => $this->input->post('category_id'),
                       'question_type_id'=> $this->input->post('question_type_id'),
                       'active' => $this->input->post('active'),
                       'pid' => $this->input->post('pid'),             
                       'title' => $this->input->post('title'),
                       'contents' => $this->input->post('contents'),
                       'packages' => $packages,
                       'template_type' => $this->input->post('template_type'),
                       //'icon' => $this->input->post('icon') ? $this->input->post('icon') : NULL,
                       'audio' => $this->input->post('audio') ? $this->input->post('audio') : NULL,
                       'by_user' => $by_user
                   );
                   $config['upload_path']  = STATIC_PAGE_IMAGE_PATH;
                   $config['allowed_types'] = STATIC_PAGE_ALLOWED_TYPES;
                   $config['encrypt_name'] = FALSE;         
                   $this->load->library('upload',$config);
                   if($this->upload->do_upload("image")){
                       $data_img = array('upload_data' => $this->upload->data());
                       $image= site_url(STATIC_PAGE_IMAGE_PATH).$data_img['upload_data']['file_name'];                    
                   }else{
                       $image=NULL;
                   }
                   $params = array(
                       'category_id' => $this->input->post('category_id'),
                       'question_type_id'=> $this->input->post('question_type_id'),
                       'active' => $this->input->post('active'),
                       'pid' => $this->input->post('pid'),             
                       'title' => $this->input->post('title'),
                       'image' => $image, 
                       'packages' => $packages,
                       'template_type' => $this->input->post('template_type'),
                       'contents' => $this->input->post('contents'),
                       'audio' => $this->input->post('audio') ? $this->input->post('audio') : NULL,
                       'by_user' => $by_user
                       );
   
                   if($this->upload->do_upload("video")){
                   $data_vid = array('upload_data' => $this->upload->data());
                   $video= site_url(STATIC_PAGE_IMAGE_PATH).$data_vid['upload_data']['file_name'];
                   
                       if(isset($image)and $image!=NULL){
                           $params = array(
                               'category_id' => $this->input->post('category_id'),
                               'question_type_id'=> $this->input->post('question_type_id'),
                               'active' => $this->input->post('active'),
                               'pid' => $this->input->post('pid'),             
                               'title' => $this->input->post('title'),
                               'image' => $image,
                               'packages' => $packages,
                               'template_type' => $this->input->post('template_type'),
                               'video' => $video,
                               'contents' => $this->input->post('contents'),
                               'audio' => $this->input->post('audio') ? $this->input->post('audio') : NULL,
                               'by_user' => $by_user
                           );
                       }else{
                           $params = array(
                               'category_id' => $this->input->post('category_id'),
                               'question_type_id'=> $this->input->post('question_type_id'),
                               'active' => $this->input->post('active'),
                               'pid' => $this->input->post('pid'),             
                               'title' => $this->input->post('title'),
                               'video' => $video,
                               'packages' => $packages,
                               'template_type' => $this->input->post('template_type'),
                               'contents' => $this->input->post('contents'),
                               'audio' => $this->input->post('audio') ? $this->input->post('audio') : NULL,
                               'by_user' => $by_user
                           );
                       }
                       
                   }else{
                       $video=NULL;
                       if(isset($image)and $image!=NULL){
                           $params = array(
                               'category_id' => $this->input->post('category_id'),
                               'question_type_id'=> $this->input->post('question_type_id'),
                               'active' => $this->input->post('active'),
                               'pid' => $this->input->post('pid'),             
                               'title' => $this->input->post('title'),
                               'image' => $image,
                               'packages' => $packages,
                               'template_type' => $this->input->post('template_type'),
                               'contents' => $this->input->post('contents'),
                               'audio' => $this->input->post('audio') ? $this->input->post('audio') : NULL,
                               'by_user' => $by_user
                           );
                       }else{
                           $params = array(
                               'category_id' => $this->input->post('category_id'),
                               'question_type_id'=> $this->input->post('question_type_id'),
                               'active' => $this->input->post('active'),
                               'pid' => $this->input->post('pid'),             
                               'title' => $this->input->post('title'),
                               'packages' => $packages,
                               'template_type' => $this->input->post('template_type'),
                               'contents' => $this->input->post('contents'),
                               'audio' => $this->input->post('audio') ? $this->input->post('audio') : NULL,
                               'by_user' => $by_user
                           );
                       }
                   }
                   $id = $this->Static_page_model->update_static_page($static_page_id,$params);
                   $catId = $params['category_id'];
                   if($id){
                       $this->session->set_flashdata('flsh_msg', UPDATE_MSG);
                       if($catId>=44 and $catId<=47)
                       redirect('static_page/index');
                       elseif($catId>=48 and $catId<=51)
                       redirect('static_page/index_gt');
                       else
                       redirect('static_page/index_ee');
                   }else{
                       $this->session->set_flashdata('flsh_msg', UPDATE_FAILED_MSG);           
                       redirect('static_page/edit/'.$static_page_id);
                   }               
               }
               else
               {
   				$this->load->model('Category_master_model');
                   $data['all_category_masters'] = $this->Category_master_model->get_all_category_masters_active();
                   $this->load->model('Static_page_model');
                   $data['static_pages2'] = $this->Static_page_model->get_all_static_pages2();
                   $data['all_question_types'] = $this->Question_type_model->get_all_question_types();
                   $data['all_packages'] = $this->Category_master_model->get_all_packages();
                   $data['Spage_templates'] = STATIC_PAGE_TEMPLATES;
                   $data['_view'] = 'static_page/edit';
                   $this->load->view('layouts/main',$data);
               }
           }
           else
               show_error(ITEM_NOT_EXIST);
       }
       /*
        * Deleting static_page
        */
       function remove($static_page_id)
       {
           //access control start
           $cn = $this->router->fetch_class().''.'.php';
           $mn = $this->router->fetch_method();        
           if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
           //access control ends
           $static_page = $this->Static_page_model->get_static_page($static_page_id);
           if(isset($static_page['static_page_id']))
           {
               $this->Static_page_model->delete_static_page($static_page_id);
               $this->session->set_flashdata('flsh_msg', DEL_MSG);
   
               if($static_page['category_id']>=44 and $static_page['category_id']<=47)
                   redirect('static_page/index');
               elseif($static_page['category_id']>=48 and $static_page['category_id']<=51)
                   redirect('static_page/index_gt');
               else
                   redirect('static_page/index_ee');
               
           }
           else
               show_error(ITEM_NOT_EXIST);
       }
   
   
       function print_sp($category_id){
   
           //access control start
           $cn = $this->router->fetch_class().''.'.php';
           $mn = $this->router->fetch_method();        
           if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
           //access control ends
           $this->load->library('pagination');
           $params['limit'] = RECORDS_PER_PAGE;
           $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
           $config = $this->config->item('pagination');
           $config['base_url'] = site_url('static_page/print_sp?');
           $config['total_rows'] = $this->Static_page_model->get_all_static_pages_count_parent();
           $this->pagination->initialize($config);
           $data['title'] = 'Print Static page';
           $data['sp_all'] = $this->Static_page_model->get_sp_to_print($category_id);
           $data['_view'] = 'static_page/print_sp';
           $this->load->view('layouts/main',$data);
       }
       
       function view($pageId){
   		
   		?>
<html>
   <title> data</title>
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
   <style>li {
      margin-left: 13px;
      padding: 3px;
      }li.active {
      background-color: lightgrey;    
      }
   </style>
   <script type="text/javascript">
      $(document).ready(function(){
      $("img").toggleClass("img-responsive");
      });
   </script>
   </head>
   <body style="background-color: #F9E79F;margin: 0 44;">
      <?php
         $curl = curl_init();
         curl_setopt_array($curl, array(
           CURLOPT_URL => "http://192.163.212.17/~masterprepapp/newV1/MP-API-V7/Sp_new/$pageId",
           CURLOPT_RETURNTRANSFER => true,
           CURLOPT_ENCODING => "",
           CURLOPT_MAXREDIRS => 10,
           CURLOPT_TIMEOUT => 30,
           CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
           CURLOPT_CUSTOMREQUEST => "POST",
           CURLOPT_HTTPHEADER => array(
         	"cache-control: no-cache",
         	"postman-token: 88f83b30-8ae1-1da2-0da2-ac4041f16cf2",
         	"token: 5225ee2fc7987924d9f192169391c376"
           ),
         ));
         
         $response = curl_exec($curl);
         $err = curl_error($curl);
         
         curl_close($curl);
         
         if ($err) {
           echo "cURL Error #:" . $err;
         } else {
			 
			// t($response , 1);
           $array_res = json_decode($response , TRUE );		  
           $data = $array_res['error_message']['data'];
           $inner_html = "";
           $count = 0;
                     
           if(isset($data['parent']['template_type']) && $data['parent']['template_type'] == "Tabs"):		  
         	  $inner_html_tbs = '<ul class="nav nav-tabs">';
         	  $inner_html_cnt = '<div class="tab-content">';
         	  foreach($data['child'] as $key => $child){				  
         		  $tabclass = $contentclass = "";				  
         		  if($count == 0){ $tabclass = "active";  $contentclass = "in active show"; }
         		  $inner_html_tbs .= '<li class="'.$tabclass.'"><a data-toggle="tab" href="#tabs-'.$child['static_page_id'].'">'.$child['title'].'</a></li>';
         		  $inner_html_cnt .= '<div id="tabs-'.$child['static_page_id'].'" class="tab-pane fade '.$contentclass.'"><p>'.$child['contents'].'</p></div>';
         		  $count++;
         	  }
         	   $inner_html_tbs .= '</ul>';
         	   $inner_html_cnt .= '</div>';
         	   $inner_html = $inner_html_tbs.$inner_html_cnt;
         	elseif(isset($data['parent']['template_type']) && $data['parent']['template_type'] == "Listing"):
         		$inner_html_list = '<ul class="list-group">';
         	 
         	  foreach($data['child'] as $key => $child){				  
         		  $inner_html_list .= '<li class="list-group-item"><a href="'.$child['static_page_id'].'">'.$child['title'].'</a></li>';			  
         		  
         	  }
         	   $inner_html_list .= '</ul>';			   
         	   $inner_html = $inner_html_list;				
         	elseif(isset($data['parent']['template_type']) && $data['parent']['template_type'] == "Buttons"):
         		$inner_html_btn = '<div class="">';			 
         		  foreach($data['child'] as $key => $child){
         			  $inner_html_btn .= '&nbsp;<a class="btn btn-primary" href="'.$child['static_page_id'].'">'.$child['title'].'</a>&nbsp;';
         		  }
         	   $inner_html_list .= '</div>';			   
         	   $inner_html = $inner_html_btn;
         	   
            elseif(isset($data['parent']['template_type']) && $data['parent']['template_type'] == "Nested Listing"):
//t($data);
				$inner_html = "";
				$inner_html_list = "";
				$inner_html_list = '<ul class="list-group">';
				foreach($data['child'] as $key => $child_first){
         		  $inner_html_list .= '<li class="list-group-item"><a href="'.$child_first['static_page_id'].'">'.$child_first['title'].'</a>';
         		  if(isset($child_first['child']) && !empty($child_first['child'])){
					  $inner_html_list .= '<ul class="list-group">' ;
					  foreach($child_first['child'] as $child_second){
						  $inner_html_list .= '<li class="list-group-item"><a href="'.$child_second['static_page_id'].'">'.$child_second['title'].'</a>';
						  if(isset($child_second['child']) && !empty($child_second['child'])){
							  $inner_html_list .= '<ul class="list-group">' ;
							  foreach($child_second['child'] as $child_third){
								  $inner_html_list .= '<li class="list-group-item"><a href="'.$child_third['static_page_id'].'">'.$child_third['title'].'</a></li>';	
							  }
							  $inner_html_list .= '</ul>';	
						  }
						  $inner_html_list .= '</li>';	
					  }
					  $inner_html_list .= '</ul>';	
				  }
				   $inner_html_list .= '</li>';		  
         		  
				}
				$inner_html_list .= '</ul>';	

				$inner_html = $inner_html_list;
         		
         	else:					   
         	   $inner_html = "";				
            endif;
            
            
           echo "<h2 class='text-center'>".$data['parent']['title']."</h2><br><p class='text-justify'>".str_replace("[child_location]", $inner_html , $data['parent']['contents'] )."</p>";
           
           
          
         }
         
         ?>
   </body>
</html>
<?php
}
}
