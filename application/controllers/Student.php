<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/ 
class Student extends MY_Controller{
    
    function __construct()
    {
        parent::__construct();
        if (!$this->_is_logged_in()) {$this->session->set_userdata('lock_url', $_SERVER['REDIRECT_QUERY_STRING']); redirect('/login');}
        $this->load->model('Student_model');
        $this->load->model('Student_answer_model');
        $this->load->model('Student_results_model');
        $this->load->model('Gender_model');
        $this->load->model('Notification_model');
        $this->load->model('Package_master_model');
    }
    /*
     * Listing of all students Acad
     */
    function index()
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends

        $data['title'] = 'Students'.'- '.ACD;
        $this->load->library('pagination');
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('student/index?');
        $config['total_rows'] = $this->Student_model->get_all_students_count();
        $this->pagination->initialize($config);        
        $data['students'] = $this->Student_model->get_all_students($params);        
        $data['_view'] = 'student/index';
        $this->load->view('layouts/main',$data);
    }

    function goal($id)
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends

        $data['title'] = 'Goal Data';                
        $data['goalData'] = $this->Student_model->get_goal($id);
        //print_r($data['goalData']);die;        
        $data['_view'] = 'student/goal';
        $this->load->view('layouts/main',$data);
    }

    /*
     * Listing of all students Inactive
     */
    function fresh_student()
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends

        $data['title'] = 'Students- Inactive';
        $this->load->library('pagination');
        $params['limit'] = RECORDS_PER_PAGE;
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('student/index?');
        $config['total_rows'] = $this->Student_model->get_all_students_count_fresh();
        $this->pagination->initialize($config);        
        $data['students'] = $this->Student_model->get_all_students_fresh($params);
        $data['_view'] = 'student/index';
        $this->load->view('layouts/main',$data);
    }

    /*
     * Listing of all students GT
     */
    function index_gt()
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends

        $data['title'] = 'Students'.'- '.GT;
        $this->load->library('pagination');
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('student/index_gt?');
        $config['total_rows'] = $this->Student_model->get_all_students_count_gt();
        $this->pagination->initialize($config);        
        $data['students'] = $this->Student_model->get_all_students_gt($params);
        $data['_view'] = 'student/index';
        $this->load->view('layouts/main',$data);
    }

    /*
     * Listing of all enquiry Acad
     */
    function enquiry()
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends

        $data['title'] = 'Enquiry (Academic)';
        $this->load->library('pagination');
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('student/enquiry?');
        $config['total_rows'] = $this->Student_model->get_all_enquiry_count();
        $this->pagination->initialize($config);
        
        $data['enquiry'] = $this->Student_model->get_all_enquiry($params,ACD_ID);
        $data['_view'] = 'student/enquiry';
        $this->load->view('layouts/main',$data);
    }

    /*
     * Listing of all enquiry GT
     */
    function enquiry_gt()
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends

        $data['title'] = 'Enquiry (GT)';
        $this->load->library('pagination');
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('student/enquiry_gt?');
        $config['total_rows'] = $this->Student_model->get_all_enquiry_count_gt();
        $this->pagination->initialize($config);
        
        $data['enquiry'] = $this->Student_model->get_all_enquiry($params,GT_ID);
        $data['_view'] = 'student/enquiry';
        $this->load->view('layouts/main',$data);
    }


     /*
     * Listing of all VISA enquiry Acad
     */
    function visa_enquiry()
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends

        $data['title'] = 'VISA Enquiry (Academic)';
        $this->load->library('pagination');
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('student/visa_enquiry?');
        $config['total_rows'] = $this->Student_model->get_all_visa_enquiry_count();
        $this->pagination->initialize($config);
        
        $data['visa_enquiry'] = $this->Student_model->get_all_visa_enquiry($params,ACD_ID);
        //echo '<pre>';
        //print_r($data['visa_enquiry']);die;
        $data['_view'] = 'student/visa_enquiry';
        $this->load->view('layouts/main',$data);
    }

    /*
     * Listing of all VISA enquiry GT
     */
    function visa_enquiry_gt()
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends

        $data['title'] = 'VISA Enquiry (GT)';
        $this->load->library('pagination');
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('student/visa_enquiry_gt?');
        $config['total_rows'] = $this->Student_model->get_all_visa_enquiry_count_gt();
        $this->pagination->initialize($config);
        
        $data['visa_enquiry'] = $this->Student_model->get_all_visa_enquiry($params,GT_ID);
        $data['_view'] = 'student/visa_enquiry';
        $this->load->view('layouts/main',$data);
    }
    /*
     * Adding a new student
     */
    function add()
    {   
       //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends

        $data['title'] = 'Add student';
        $this->load->library('form_validation');        
            $this->form_validation->set_rules('programe_id','Programme','required');
            $this->form_validation->set_rules('email','Email id','valid_email|max_length[60]|trim|is_unique[students.email]');
            $this->form_validation->set_rules('mobile','Mobile no.','required|max_length[10]|trim');
            $this->form_validation->set_rules('fname','First name','required|trim');
            $this->form_validation->set_rules('gender_name','Gender','required');
		
		if($this->form_validation->run())     
        {   
            $user = $this->session->userdata('admin_login_data');
            foreach ($user as $d){$by_user=$d->id;}
            $params = array(
				'active' => $this->input->post('active'),
				'programe_id' => $this->input->post('programe_id'),
				'gender' => $this->input->post('gender_name'),				
				'email' => $this->input->post('email'),
				'username' => $this->input->post('email'),
				'mobile' => $this->input->post('mobile'),
				'fname' => ucfirst($this->input->post('fname')),
				'lname' => ucfirst($this->input->post('lname')),
				'dob' => $this->input->post('dob'),
				'residential_address' => $this->input->post('residential_address'),
                'by_user' => $by_user,
            );            
            $id = $this->Student_model->add_student($params);
            if($id and $params['programe_id']==ACD_ID){
                $this->session->set_flashdata('flsh_msg', SUCCESS_MSG);
                redirect('student/index');
            }elseif ($id and $params['programe_id']==GT_ID) {
                $this->session->set_flashdata('flsh_msg', SUCCESS_MSG);
                redirect('student/index_gt');
            }
            else{
                $this->session->set_flashdata('flsh_msg', FAILED_MSG);
                redirect('student/add');
            }            
        }
        else
        {
			$this->load->model('Programe_master_model');
			$data['all_programe_masters'] = $this->Programe_master_model->get_all_programe_masters_active();
            $data['all_genders'] = $this->Gender_model->get_all_gender_active();
            
            $data['_view'] = 'student/add';
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a student
     */
    function edit($id)
    {   
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        
        $data['title'] = 'Edit student';
        $data['student'] = $this->Student_model->get_student($id);        
        if(isset($data['student']['id']))
        {
            $this->load->library('form_validation');        
            $this->form_validation->set_rules('programe_id','Programme','required');
            $this->form_validation->set_rules('email','Email id','valid_email|max_length[60]|trim');
            $this->form_validation->set_rules('mobile','Mobile no.','required|max_length[10]|trim');
            $this->form_validation->set_rules('fname','First name','required|trim');
            $this->form_validation->set_rules('gender_name','Gender','required');
		
			if($this->form_validation->run())     
            {
                $user = $this->session->userdata('admin_login_data');
                foreach ($user as $d){$by_user=$d->id;}
                $params = array(
					'active' => $this->input->post('active'),
                    'programe_id' => $this->input->post('programe_id'),
                    'gender' => $this->input->post('gender_name'),              
                    'email' => $this->input->post('email'),
                    'username' => $this->input->post('email'),
                    'mobile'=> $this->input->post('mobile'),
                    'fname' => ucfirst($this->input->post('fname')),
                    'lname' => ucfirst($this->input->post('lname')),
                    'dob'   => $this->input->post('dob'),
                    'residential_address' => $this->input->post('residential_address'),
                    'by_user' => $by_user,
                );

                $idd = $this->Student_model->update_student($id,$params); 
                if($idd and $params['programe_id']==ACD_ID){
                    $this->session->set_flashdata('flsh_msg', UPDATE_MSG);          
                    redirect('student/index');
                }elseif($idd and $params['programe_id']==GT_ID){
                    $this->session->set_flashdata('flsh_msg', UPDATE_MSG);          
                    redirect('student/index_gt');
                }
                else{
                    $this->session->set_flashdata('flsh_msg', UPDATE_FAILED_MSG);
                    redirect('student/edit/'.$id);
                }       
                
            }
            else
            {
				$this->load->model('Programe_master_model');
				$data['all_programe_masters'] = $this->Programe_master_model->get_all_programe_masters();
                $data['all_genders'] = $this->Gender_model->get_all_gender_active();
                $data['_view'] = 'student/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error(ITEM_NOT_EXIST);
    }
    /*
     * Deleting student
     */
    function remove($id)
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        
        $student = $this->Student_model->get_student($id);
        if(isset($student['id']))
        {
            $this->Student_model->delete_student($id);
            $this->session->set_flashdata('flsh_msg', DEL_MSG);
            if($student['programe_id']==ACD_ID)
            redirect('student/index');
            else
            redirect('student/index_gt');
        }
        else
            show_error(ITEM_NOT_EXIST);
    }


    /*
     * Adding a new student
     */
    function reply_to_student_enquiry($enquiry_id)
    {   
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends

        $data['title'] = 'Reply to enquiry';
        $this->load->library('form_validation');        
        $this->form_validation->set_rules('admin_reply','Reply','required|trim');
        
        if($this->form_validation->run())     
        {   
            $user = $this->session->userdata('admin_login_data');
            foreach ($user as $d){$by_user=$d->id;}
            $params = array(                
                'enquiry_id' => $enquiry_id,
                'admin_reply' => $this->input->post('admin_reply'),
                'by_user' => $by_user,
            );
            $id = $this->Student_model->add_reply($params);
            if($id){

                $params2= array('isReplied' => 1);
                $this->db->where('enquiry_id',$enquiry_id);
                $this->db->update('students_enquiry',$params2);
                $std_info = $this->Student_model->get_enquiry($enquiry_id); 
                $student_id = $std_info['student_id'];
                $programe_id = $std_info['programe_id'];
                $test_module_id = $std_info['test_module_id'];
                $email = $std_info['email'];
                $mobile = $std_info['mobile'];
                $ts_cat_assoc_id=$enquiry_id;
                $collection_no='Student Enquiry Reply';
                $paper_checked=NULL;
                $this->insert_notification(ENQUIRY_REPLY_NOTIFY,$student_id,$programe_id,$test_module_id,$ts_cat_assoc_id,$collection_no,$paper_checked);
                $this->session->set_flashdata('flsh_msg', SUCCESS_MSG);
                redirect('student/enquiry');
            }else{
                $this->session->set_flashdata('flsh_msg', FAILED_MSG);
                redirect('student/reply_to_student_enquiry/'.$enquiry_id);
            }            
        }
        else
        {            
            $data['enquiryData'] = $this->Student_model->get_enquiry($enquiry_id);
            $data['preReplies'] = $this->Student_model->get_preReplies($enquiry_id);
            $data['_view'] = 'student/reply_to_student_enquiry';
            $this->load->view('layouts/main',$data);
        }
    }

    /*
     * Adding a new student
     */
    function reply_to_student_visa_enquiry($visa_enquiry_id)
    {   
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends

        $data['title'] = 'Reply to VISA enquiry';
        $this->load->library('form_validation');        
        $this->form_validation->set_rules('admin_reply','Reply','required|trim');
        
        if($this->form_validation->run())     
        {   
            $user = $this->session->userdata('admin_login_data');
            foreach ($user as $d){$by_user=$d->id;}
            $params = array(                
                'visa_enquiry_id' => $visa_enquiry_id,
                'admin_reply' => $this->input->post('admin_reply'),
                'by_user' => $by_user,
            );
            $id = $this->Student_model->add_visa_reply($params);
            if($id){
                $params2= array('isReplied' => 1);
                $this->db->where('visa_enquiry_id',$visa_enquiry_id);
                $this->db->update('students_visa_enquiry',$params2);
                $std_info = $this->Student_model->get_visa_enquiry($visa_enquiry_id); 
                $student_id = $std_info['student_id'];
                $programe_id = $std_info['programe_id'];
                $test_module_id = $std_info['test_module_id'];
                $email = $std_info['email'];
                $mobile = $std_info['mobile'];
                $ts_cat_assoc_id=$visa_enquiry_id;
                $collection_no='Student VISA Enquiry Reply';
                $paper_checked=NULL;
                $this->insert_notification(VISA_ENQUIRY_REPLY_NOTIFY,$student_id,$programe_id,$test_module_id,$ts_cat_assoc_id,$collection_no,$paper_checked);
                $this->session->set_flashdata('flsh_msg', SUCCESS_MSG);
                redirect('student/visa_enquiry');
            }else{
                $this->session->set_flashdata('flsh_msg', FAILED_MSG);
                redirect('student/reply_to_student_visa_enquiry/'.$visa_enquiry_id);
            }            
        }
        else
        {            
            $data['visa_enquiryData'] = $this->Student_model->get_visa_enquiry($visa_enquiry_id);
            $data['preReplies'] = $this->Student_model->get_visa_preReplies($visa_enquiry_id);
            $data['_view'] = 'student/reply_to_student_visa_enquiry';
            $this->load->view('layouts/main',$data);
        }
    }


    public function details($id){

        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        
        $data['title'] = 'Students Details';
        $data['basic'] = $this->Student_model->get_student($id);
        $data['enquiry'] = $this->Student_model->get_std_enquiry($id);               
        $data['student_attempts_r'] = $this->Student_answer_model->get_student_attempts_reading($id);
        $data['student_attempts_l'] = $this->Student_answer_model->get_student_attempts_listening($id);
        $data['student_attempts_w'] = $this->Student_answer_model->get_student_attempts_writing($id);
        $data['student_attempts_s'] = $this->Student_answer_model->get_student_attempts_speaking($id);
        $data['student_attempts_ee'] = $this->Student_answer_model->get_student_attempts_ee($id);
        $data['student_attempts_et'] = $this->Student_answer_model->get_student_attempts_et($id);

        $data['transaction'] = $this->Package_master_model->get_student_pack_subscribed($id);
        
        $data['mock_tests'] = $this->Student_answer_model->get_student_attempts_mt($id);

		//t($data['mock_tests']);
		
        $data['avgScore_l'] = $this->Student_answer_model->get_student_avgScore_l($id);
        $data['avgScore_r'] = $this->Student_answer_model->get_student_avgScore_r($id);
        $data['avgScore_w'] = $this->Student_answer_model->get_student_avgScore_w($id);
        $data['avgScore_s'] = $this->Student_answer_model->get_student_avgScore_s($id);
        $data['avgScore_e'] = $this->Student_answer_model->get_student_avgScore_e($id);

        $data['_view'] = 'student/details';
        $this->load->view('layouts/main',$data);

    }
    
}
