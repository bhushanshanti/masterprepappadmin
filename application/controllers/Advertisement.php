<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Harman
 *
 **/
 
class Advertisement extends MY_Controller{
    
    function __construct()
    {
        parent::__construct();
       if (!$this->_is_logged_in()) {$this->session->set_userdata('lock_url', $_SERVER['REDIRECT_QUERY_STRING']); redirect('/login');}
        $this->load->model('Advertisement_model');        
    }
    /*
     * Listing of App slides
     */
    function index()
    {
        
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $this->load->library('pagination');
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('advertisement/index?');
        $config['total_rows'] = $this->Advertisement_model->get_all_advertisements_count();
        $this->pagination->initialize($config);
        $data['title'] = 'Advertisements';
        $data['advertisements'] = $this->Advertisement_model->get_all_advertisements($params);        
        $data['_view'] = 'advertisement/index';

        $this->load->view('layouts/main',$data);
    }
    /*
     * Adding a new App slides
     */
    function add()
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $this->load->library('form_validation');
        $this->form_validation->set_rules('title','Title','required|trim');
        $data['title'] = 'Add Advertisement';
        if($this->form_validation->run())     
        {   
            $user = $this->session->userdata('admin_login_data');
            foreach ($user as $d){$by_user=$d->id;}
            $params = array(
                'active' => $this->input->post('active'),
                'adv_image'  => $this->input->post('image'),
                'adv_title'  => $this->input->post('title'),
                'adv_content'  => $this->input->post('contents'),
                'by_user' => $by_user,
            );



            $config['upload_path']   = ADVERTISEMENT_IMAGE_PATH;
            $config['allowed_types'] = ADVERTISEMENT_ALLOWED_TYPES;
            $config['encrypt_name']  = FALSE;         
            $this->load->library('upload',$config);

                if($this->upload->do_upload("image")){
                    $data = array('upload_data' => $this->upload->data());
                    $image= $data['upload_data']['file_name'];
                    $params = array(
                        'active' => $this->input->post('active'),
                        'adv_image' => site_url(ADVERTISEMENT_IMAGE_PATH.$image),
                        'adv_title' => $this->input->post('title'),
                        'adv_content' => $this->input->post('contents'),                        
                        'by_user' => $by_user,
                    );
                    $id = $this->Advertisement_model->add_advertisements($params);
                    if($id){
                        $this->session->set_flashdata('flsh_msg', SUCCESS_MSG);
                        redirect('advertisement/index');
                    }else{
                        $this->session->set_flashdata('flsh_msg', FAILED_MSG);
                        redirect('advertisement/add');
                    }                    
                    
                }else{
                    $params = array(
                        'active' => $this->input->post('active'),
                        'adv_title' => $this->input->post('title'),
                        'adv_content' => $this->input->post('contents'),   
                        'by_user' => $by_user,
                    );
                    $this->session->set_flashdata('flsh_msg', FAILED_MSG);
                    redirect('advertisement/add');
                } 
        }
        else
        {            
            $data['_view'] = 'advertisement/add';
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a App slides
     */
    function edit($id)
    { 
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $data['advertisements'] = $this->Advertisement_model->get_advertisements($id);
        $data['title'] = 'Edit Advertisement';
        
        if(isset($data['advertisements']['id']))
        {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('title','Title','required|trim'); 
            if($this->form_validation->run())     
            {   
                $user = $this->session->userdata('admin_login_data');
                foreach ($user as $d){$by_user=$d->id;}
                $params = array(
                    'active' => $this->input->post('active'),                       
                        'adv_title' => $this->input->post('title'),
                        'adv_content' => $this->input->post('contents'),                        
                        'by_user' => $by_user,
                );
                $config['upload_path']   = ADVERTISEMENT_IMAGE_PATH;
                $config['allowed_types'] = ADVERTISEMENT_ALLOWED_TYPES;
                $config['encrypt_name']  = FALSE;         
                $this->load->library('upload',$config);

                if($this->upload->do_upload("image")){
                    $data = array('upload_data' => $this->upload->data());
                    $image= $data['upload_data']['file_name'];
                    $params = array(
                        'active' => $this->input->post('active'),
                        'adv_image' => site_url(ADVERTISEMENT_IMAGE_PATH.$image),
                        'adv_title' => $this->input->post('title'),
                        'adv_content' => $this->input->post('contents'),                        
                        'by_user' => $by_user,
                    );                    
                }else{
                    $params = array(
                        'active' => $this->input->post('active'),                        
                        'adv_title' => $this->input->post('title'),
                        'adv_content' => $this->input->post('contents'),                        
                        'by_user' => $by_user,
                    );
                }
                $idd = $this->Advertisement_model->update_advertisements($id,$params);
                if($idd){
                    $this->session->set_flashdata('flsh_msg', UPDATE_MSG);           
                    redirect('advertisement/index');
                }else{
                    $this->session->set_flashdata('flsh_msg', UPDATE_FAILED_MSG);           
                    redirect('advertisement/edit/'.$id);
                }                
            }
            else
            {
                $data['_view'] = 'advertisement/edit';                

                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error(ITEM_NOT_EXIST);
    } 

    /*
     * Deleting App slides
     */
    function remove($id){
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $advertisements = $this->Advertisement_model->get_advertisements($id);
        if(isset($advertisements['id']))
        {
            $this->Advertisement_model->delete_advertisements($id);
            $del_picture=$advertisements['add_image'];
            unlink($del_picture);
            $this->session->set_flashdata('flsh_msg', DEL_MSG);
            redirect('advertisement/index');
        }
        else
            show_error(ITEM_NOT_EXIST);
    }



}
