<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        
        if($this->_is_logged_in()){
            redirect('/dashboard');
            exit;
        }
        $data = array();
        $data['error'] = '';
        $data['message'] = '';
        $data['base_url'] = base_url();
        $this->load->helper('cookie');
        if (isset($_REQUEST['sbmt']) && $_REQUEST['sbmt'] <> "") {
            extract($_REQUEST);
            $this->load->model('User_model');
            $res = $this->User_model->checkLogin($email,$passwrd,1);
            if ($res) {
                $this->session->set_userdata('admin_login_data', $res);
                $datas = $this->session->userdata('admin_login_data');
                if (isset($rememberme) && $rememberme == "on") {
                    $mp_username = $email;
                    $mp_pwd = md5($passwrd);
                    //setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/");
                    set_cookie('mp_username',$mp_username,COOKIE_EXPIRY);
                    set_cookie('mp_pwd',$passwrd,COOKIE_EXPIRY);
                } else {
                    delete_cookie('mp_username');
                    delete_cookie('mp_pwd');
                }
                //$this->session->set_flashdata('flsh_msg', ADMIN_LOGIN_SUC_MSG);
                //sleep(3);
               
               $lock_url = $this->session->userdata('lock_url');
                if(isset($lock_url) &&  $lock_url != "" ){
                    redirect($lock_url);
                }else{
                    redirect('/dashboard');
                }
               
            }else{
               $this->session->set_flashdata('flsh_msg', ADMIN_LOGIN_ERR_MSG);
               redirect('/');
            }
        }       
       
        $data['_view'] = 'login';
        $this->load->view('layouts/login_layout',$data);
    }

    public function logout() {
        $this->session->set_userdata('admin_login_data', '');
        $this->session->sess_destroy();
        redirect('/');
    }

}

?>
