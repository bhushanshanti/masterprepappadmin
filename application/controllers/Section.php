<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/
class Section extends MY_Controller{

    function __construct()
    {
        parent::__construct();
        if (!$this->_is_logged_in()) {$this->session->set_userdata('lock_url', $_SERVER['REDIRECT_QUERY_STRING']); redirect('/login');}
        $this->load->model('Section_model');       
    }
    /*
     * Listing of sections
     */
    function index()
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $this->load->library('pagination');
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('section/index?');
        $config['total_rows'] = $this->Section_model->get_all_sections_count();
        $this->pagination->initialize($config);
        $data['sections'] = $this->Section_model->get_all_sections($params);        
        $data['_view'] = 'section/index';
        $this->load->view('layouts/main',$data);
    }
    /*
     * Adding a new section
     */
    function add()
    {   
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $this->load->library('form_validation');
        $this->form_validation->set_rules('section_heading', 'Section heading', 'trim|required');
        $this->form_validation->set_rules('section_desc', 'Section description', 'trim');
        $this->form_validation->set_rules('section_para', 'Section paragraph', 'trim');
        if ($this->form_validation->run() == false) {
             header('Content-Type: application/json');
            $response = ['msg'=>'<span class="text-danger">Section heading required!</span>', 'status'=>'false'];
            echo json_encode($response);
        }else{
            $user = $this->session->userdata('admin_login_data');
            foreach ($user as $d){$by_user=$d->id;}
            $datas = array(
                'ts_cat_assoc_id'   => $this->input->post('ts_cat_assoc_id', true),
                'section_heading'   => $this->input->post('section_heading', true),
                'section_desc'      => $this->input->post('section_desc', true),
                'section_para'      => $this->input->post('section_para', true),                
                'section_image'     => $this->input->post('image_id', true) ? $this->input->post('image_id', true) : NULL,
                'active'            => $this->input->post('active', true),
                'by_user' => $by_user,
            );            
            $id = $this->Section_model->add_section($datas);
            if($id){ 
                header('Content-Type: application/json');
                $response = ['msg'=>SECTION_ADD_MSG, 'status'=>'true'];
                echo json_encode($response);
            }else{
                header('Content-Type: application/json');
                $response = ['msg'=>SECTION_ADD_MSG_FAILED, 'status'=>'false'];
                echo json_encode($response);
            }                
            
        }
    }

    function do_upload_section_image(){
        
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $config['upload_path']      = SECTION_IMAGE_PATH;
        $config['allowed_types']    = SECTION_ALLOWED_TYPES;
        $config['encrypt_name']     = FALSE;         
        $this->load->library('upload',$config);
        if($this->upload->do_upload("file")){
            $data = array('upload_data' => $this->upload->data());
            $image= $data['upload_data']['file_name'];
            echo site_url(SECTION_IMAGE_PATH).$image;
        }
     }    

    function view_section(){

        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $ts_cat_assoc_id = $this->input->post('ts_cat_assoc_id', true);
        $data=$this->Section_model->get_section($ts_cat_assoc_id);
        echo json_encode($data);
    }
    /*
     * Editing a section
     */
    function edit($section_id)
    {        
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $data['title'] = 'Edit section';
        $data['section'] = $this->Section_model->get_section2($section_id);
        $data['section']['section_id'];
        if(isset($data['section']['section_id']))
        {
            $this->load->library('form_validation');
			$this->form_validation->set_rules('section_para','Section paragraph','trim');
			$this->form_validation->set_rules('section_desc','Section description','trim');
			$this->form_validation->set_rules('section_heading','Section heading','required');	
			if($this->form_validation->run())     
            {   
                $user = $this->session->userdata('admin_login_data');
                foreach ($user as $d){$by_user=$d->id;}
                $params = array(
					'active' => $this->input->post('active'),
					'section_heading' => $this->input->post('section_heading'),
					'section_desc' => $this->input->post('section_desc'),
					'section_para' => $this->input->post('section_para'),
                    'by_user' => $by_user,
                );
                $config['upload_path']      = SECTION_IMAGE_PATH;
                $config['allowed_types']    = SECTION_ALLOWED_TYPES;
                $config['encrypt_name']     = FALSE;         
                $this->load->library('upload',$config);
                if($this->upload->do_upload("section_image")){
                    $data = array('upload_data' => $this->upload->data());
                    $image= $data['upload_data']['file_name'];
                    $params = array(
                        'active' => $this->input->post('active'),
                        'section_heading' => $this->input->post('section_heading'),
                        'section_desc' => $this->input->post('section_desc'),
                        'section_para' => $this->input->post('section_para'),
                        'section_image' => site_url(SECTION_IMAGE_PATH).$image,
                        'by_user' => $by_user,
                    );
                    
                }else{
                    $params = array(
                        'active' => $this->input->post('active'),
                        'section_heading' => $this->input->post('section_heading'),
                        'section_desc' => $this->input->post('section_desc'),
                        'section_para' => $this->input->post('section_para'),
                        'section_image' => NULL,
                        'by_user' => $by_user,
                    );
                }
                $id = $this->Section_model->update_section($section_id,$params);
                    if($id){
                        $this->session->set_flashdata('flsh_msg', SECTION_EDIT_MSG);
                        redirect('section/edit/'.$section_id);                        
                    }else{
                        $this->session->set_flashdata('flsh_msg', UPDATE_FAILED_MSG);
                        redirect('section/edit/'.$section_id);
                    }                     
                }
            else
            {
				$data['_view'] = 'section/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
           show_error(ITEM_NOT_EXIST);
    }
    /*
     * Deleting section
     */
    function remove($section_id)
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $section = $this->Section_model->get_section2($section_id);
        if(isset($section['section_id']))
        {
            $this->Section_model->delete_section($section_id);
            $this->session->set_flashdata('flsh_msg', SECTION_DEL_MSG);
            redirect('test_seriese/index');
        }
        else
            show_error(ITEM_NOT_EXIST);
    }
    
}
