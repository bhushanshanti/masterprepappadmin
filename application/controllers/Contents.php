<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/ 
class Contents extends MY_Controller{
    
    function __construct()
    {
        parent::__construct();
        if (!$this->_is_logged_in()) {$this->session->set_userdata('lock_url', $_SERVER['REDIRECT_QUERY_STRING']); redirect('/login');}
        $this->load->model('Contents_model');        
    }

    /*
     * Listing of Contents
     */
    function index()
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $this->load->library('pagination');
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('contents/index?');
        $config['total_rows'] = $this->Contents_model->get_all_contents_count();
        $this->pagination->initialize($config);
        $data['title'] = 'Contents';
        $data['contents'] = $this->Contents_model->get_all_contents($params);       
        $data['_view'] = 'contents/index';
        $this->load->view('layouts/main',$data);
    }

   
    /*
     * Adding a new contents
     */
    function add()
    {   
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $data['title'] = 'Add contents';
        $this->load->library('form_validation');
		$this->form_validation->set_rules('title','Short code','required|trim');
		$this->form_validation->set_rules('sub_title','Title','required|trim');
		$this->form_validation->set_rules('tag','Tag','required|trim');
        $this->form_validation->set_rules('description','Description','required|trim');
		if($this->form_validation->run())
        {   
            $user = $this->session->userdata('admin_login_data');
            foreach ($user as $d){$by_user=$d->id;}
            $params = array(
				
				'title' => $this->input->post('title'),
				'sub_title' => $this->input->post('sub_title'),				
				'tag' => $this->input->post('tag'),
                'description' => $this->input->post('description'),
                'active' => $this->input->post('active'),
                'by_user' => $by_user,
            );

                $idd = $this->Contents_model->add_contents($params);
                if($idd){
                    $this->session->set_flashdata('flsh_msg', SUCCESS_MSG);
                    redirect('contents/index');
                }else{
                    $this->session->set_flashdata('flsh_msg', FAILED_MSG);
                    redirect('contents/index');
                }               
                
        }
        else
        {             
            $data['_view'] = 'contents/add';
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a contents
     */
    function edit($id)
    {   
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $data['title'] = 'Edit contents';
        $data['contents'] = $this->Contents_model->get_contents($id);
        
        if(isset($data['contents']['id']))
        {
            $this->load->library('form_validation');
			$this->form_validation->set_rules('title','Short code','required|trim');
            $this->form_validation->set_rules('sub_title','Title','required|trim');
            $this->form_validation->set_rules('tag','Tag','required|trim');
            $this->form_validation->set_rules('description','Description','required|trim');		
			if($this->form_validation->run())     
            {   
                $user = $this->session->userdata('admin_login_data');
                foreach ($user as $d){$by_user=$d->id;}
                $params = array(
					'title' => $this->input->post('title'),
                    'sub_title' => $this->input->post('sub_title'),             
                    'tag' => $this->input->post('tag'),
                    'description' => $this->input->post('description'),
                    'active' => $this->input->post('active'),
                    'by_user' => $by_user,
                );
                $idd = $this->Contents_model->update_contents($id,$params);
                if($idd){
                    $this->session->set_flashdata('flsh_msg', UPDATE_MSG);
                    redirect('contents/index');
                }else{
                    $this->session->set_flashdata('flsh_msg',UPDATE_FAILED_MSG);
                    redirect('contents/edit/'.$id);
                }           
                
            }
            else
            {
                $data['_view'] = 'contents/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error(ITEM_NOT_EXIST);
    }
    /*
     * Deleting live_lecture
     */
    function remove($id)
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $contents = $this->Contents_model->get_contents($id);
        if(isset($contents['id']))
        {
            $this->Contents_model->delete_contents($id);
            $this->session->set_flashdata('flsh_msg', DEL_MSG);
            redirect('contents/index');
        }
        else
            show_error(ITEM_NOT_EXIST);
    }
    
}
