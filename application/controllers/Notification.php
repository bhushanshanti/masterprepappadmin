<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Harman uppal
 *
 **/
 
class Notification extends MY_Controller{
    
    function __construct()
    {
        parent::__construct();
        if (!$this->_is_logged_in()) {$this->session->set_userdata('lock_url', $_SERVER['REDIRECT_QUERY_STRING']); redirect('/login');}
        
        $this->load->model('Notification_model');
        $this->load->model('Student_model');
        $this->load->model('Package_master_model');
        $this->load->model('Student_answer_model');
        $this->load->model('User_model');
        $this->load->model('Manage_speaking_slots_model');
    }
    /*
     * Listing of Notifications
     */
    function index()
    {
		$cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        //if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
			
		$user = $this->session->userdata('admin_login_data');
        foreach ($user as $d){$user_id=$d->id;}
        $params = array("limit" => 10 , "offset" => 0 );
        $data['total_notifications'] = $this->Notification_model->count_all_user_notifications($user_id);          
        $data['notifications'] = $this->Notification_model->get_all_notifications($user_id , $params);          
        $data['_view'] = 'notification/index'; 
        $data['title'] = 'Notifications'; 
        $this->load->view('layouts/main',$data);
	}
	
	
	function view_notification($id)
    {
		$cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        //if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        
        $user = $this->session->userdata('admin_login_data');
        foreach ($user as $d){$user_id=$d->id;}
        
		$data['notification'] = $this->Notification_model->get_notification_byId($id);        
        $notification_userID = $data['notification']['user_id'];
        
        if($notification_userID != $user_id){redirect('/notification');}
        
        $other_info = json_decode($data['notification']['other_info']);
			
		$examiner_id = $other_info->examiner_id;
		$student_id  = $other_info->student_id;
		$avail_id    = $other_info->avail_id;
		if(isset($other_info->notify_type))
			$data['notify_type'] = $other_info->notify_type;
        else
			$data['notify_type'] = "";
			
        /* Get student information starts */       
        
        if($data['notify_type'] != ""):
			$data['basic'] = $this->Student_model->get_student($student_id);
			if(isset($data['basic']['time_zone']))
				$data['zone_info'] = $this->Student_model->get_zone_name($data['basic']['time_zone']);
			else
				$data['zone_info'] = array();
			
			$data['enquiry']            = $this->Student_model->get_std_enquiry($student_id);               
			$data['student_attempts_r'] = $this->Student_answer_model->get_student_attempts_reading($student_id);
			$data['student_attempts_l'] = $this->Student_answer_model->get_student_attempts_listening($student_id);
			$data['student_attempts_w'] = $this->Student_answer_model->get_student_attempts_writing($student_id);
			$data['student_attempts_s'] = $this->Student_answer_model->get_student_attempts_speaking($student_id);
			$data['student_attempts_ee'] = $this->Student_answer_model->get_student_attempts_ee($student_id);
			$data['student_attempts_et'] = $this->Student_answer_model->get_student_attempts_et($student_id);
			$data['transaction']         = $this->Package_master_model->get_student_pack_subscribed($student_id);
			$data['avgScore_l']          = $this->Student_answer_model->get_student_avgScore_l($student_id);
			$data['avgScore_r']          = $this->Student_answer_model->get_student_avgScore_r($student_id);
			$data['avgScore_w']          = $this->Student_answer_model->get_student_avgScore_w($student_id);
			$data['avgScore_s']          = $this->Student_answer_model->get_student_avgScore_s($student_id);
			$data['avgScore_e']          = $this->Student_answer_model->get_student_avgScore_e($student_id);
			$data['mock_tests'] = $this->Student_answer_model->get_student_attempts_mt($student_id);
			/* Get student information ends */
			
			/* Get Examiner Information starts */			
			$data['examiner_info'] = $this->User_model->get_user($examiner_id); 
			if(isset($data['basic']['time_zone']))
				$data['ex_zone_info'] = $this->Student_model->get_zone_name($data['examiner_info']['time_zone']);
			else
				$data['ex_zone_info'] = array();			
			/* Get Examiner Information ends */
			
			$data['slot_information'] = $this->Manage_speaking_slots_model->get_slot_information($avail_id); 
			
			if($examiner_id == $user_id)
				$data['alow_update'] = TRUE;
			else
				$data['alow_update'] = FALSE;
        
        endif;
        
        $data['_view'] = 'notification/view'; 
        $data['title'] = 'Notifications'; 
        
       
        $this->load->view('layouts/main',$data);
	}
	
	
	function load_more_notifications(){
		
		$cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        //if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
			
		$user = $this->session->userdata('admin_login_data');
        foreach ($user as $d){$user_id=$d->id;}
        
        $limit = $_REQUEST['limit'];
        $offset = $_REQUEST['offset'];
        
        $params = array("limit" => $limit , "offset" => $offset );
        
        $notifications = $this->Notification_model->get_all_notifications($user_id , $params);
        $html = "";
        $kounter = $offset + 1;
        foreach($notifications as $notification){
			$url = site_url().'notification/view_notification/'.$notification['id'];
			
			if($notification['status'] == 0)
				$read_class = "list-group-item-info";
			else
				$read_class = "";
				
			$html .= "<li class='list-group-item $read_class'><a href='$url'>$kounter.) ".$notification['notification_message']." </a></li>";
			$kounter++;
		}
		
		echo json_encode(array("status" => TRUE , "limit" => $limit , "offset" => $limit + $offset , "data" => $html ));
        
	}
}
