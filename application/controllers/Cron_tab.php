<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/
   class Cron_tab extends MY_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('Package_master_model'); 
        $this->load->model('Db_model');              
        $this->load->model('Student_results_model');              
        $this->load->model('Student_model');              
    }    
    
    /*
     * std pack deactivation
     */
    function update_expiring_package(){
        $today=date('Y-m-d');
        $cronData = $this->Package_master_model->update_expiring_package($today);        
    }

    /*
     * send SMS to all Expired Package today
     */
    function sendSMS_toExpiredPackage()
    {        
        $expiredData = $this->Package_master_model->getExpiredPackage();
        foreach ($expiredData as $exp) {

            $fname  = $exp['fname'];
            $mobile = $exp['contact'];
            $email  = $exp['email'];
            $package_name  = $exp['package_name'];

            define('SMS_MESSAGE', 'Dear '.$fname.',%0a%0aYour '.$package_name.' package has expired on '.TODAY.'.%0a%0aPlease renew it or buy a new package.%0a%0aRegards: %0aTeam MasterPrep');

            $data_sms ="username=".API_USERNAME."&password=".API_PASSWORD."&to=".$mobile."&from=".API_FROM."&text=".SMS_MESSAGE;
            $response = $this->curlpostdata(API_URL, $data_sms);

            //mail
            $subject = PACK_EXPIRY;
            $mail_data = array(
                'fname'    => $fname,
                'package_name'  => $package_name, 
            );  
            $this->sendEmail_expiredPackage($email,$subject,$mail_data);           
        }
    }

    public function sendEmail_expiredPackage($email,$subject,$mail_data){
         
        $this->load->library('email');
        $this->email->set_mailtype("html");
        $this->email->set_newline("\r\n");
        $this->email->from(FROM_EMAIL, FROM_NAME);
        $this->email->to($email);
        //$this->email->bcc(ADMIN_EMAIL_BCC);
        $this->email->subject($subject);
        $body = $this->load->view('emails/sendEmail_expiredPackage.php' ,$mail_data,TRUE);
        $this->email->message($body);
        $this->email->send();
    } 

    /*
     * std pack deactivation
     */
    function updateMockHistory(){
        $today=date('Y-m-d');
        $cronData = $this->Package_master_model->updateMockHistory($today);      
    }

    /*
     * DB backup
     */
    function get_db_backup_mp(){
       
        $db = $this->Db_model->get_db();
        // Database configuration
        $host = $db['env'];
        $username = $db['db_user'];
        $password = $db['db_pwd'];
        $database_name = $db['db_name'];

        // Get connection object and set the charset
        $conn = mysqli_connect($host, $username, $password, $database_name);
        $conn->set_charset("utf8");

        // Get All Table Names From the Database
        $tables = array();
        $sql = "SHOW TABLES";
        $result = mysqli_query($conn, $sql);

        while ($row = mysqli_fetch_row($result)) {
            $tables[] = $row[0];
        }

        $sqlScript = "";
        foreach ($tables as $table) {
            
            // Prepare SQLscript for creating table structure
            $query = "SHOW CREATE TABLE $table";
            $result = mysqli_query($conn, $query);
            $row = mysqli_fetch_row($result);
            
            $sqlScript .= "\n\n" . $row[1] . ";\n\n";
            
            
            $query = "SELECT * FROM $table";
            $result = mysqli_query($conn, $query);
            
            $columnCount = mysqli_num_fields($result);
            
            // Prepare SQLscript for dumping data for each table
            for ($i = 0; $i < $columnCount; $i ++) {
                while ($row = mysqli_fetch_row($result)) {
                    $sqlScript .= "INSERT INTO $table VALUES(";
                    for ($j = 0; $j < $columnCount; $j ++) {
                        $row[$j] = $row[$j];
                        
                        if (isset($row[$j])) {
                            $sqlScript .= '"' . $row[$j] . '"';
                        } else {
                            $sqlScript .= '""';
                        }
                        if ($j < ($columnCount - 1)) {
                            $sqlScript .= ',';
                        }
                    }
                    $sqlScript .= ");\n";
                }
            }
            
            $sqlScript .= "\n"; 
        }

        if(!empty($sqlScript))
        {
            // Save the SQL script to a backup file
            $date_today = date('Y-m-d');
            $backup_file_name = $database_name . '_backup_' . $date_today . '-'.time().' .sql';
            $fileHandler = fopen($backup_file_name, 'w+');
            $number_of_lines = fwrite($fileHandler, $sqlScript);
            fclose($fileHandler); 

            // Download the SQL backup file to the browser
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename=' . basename($backup_file_name));
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($backup_file_name));
            if (ob_get_contents()) 
            ob_end_clean();
            if (ob_get_length()) 
            ob_end_clean();
            flush();
            readfile($backup_file_name);
            exec('rm ' . $backup_file_name); 
        }
    }
    
    function send_mockresults(){
		
		$results_to_send = $this->Student_results_model->getPendingMocktests();
		$update_data = array();
		
		if(!empty($results_to_send)){
			foreach($results_to_send as $un_id => $result){				
				$modules_result = array();				
				$stud_id   = $result['student_info']['stud_id'];
				$unique_test_id   = $result['student_info']['unique_test_id'];
				$modules_result['name']   = $result['student_info']['fname']." ".$result['student_info']['lname'];
				$student_email = $modules_result['email']  = $result['student_info']['email'];
				$modules_result['mobile'] = $result['student_info']['mobile'];
				$book_speaking_slots_id = $result['student_info']['book_speaking_slots_id'];				
				$modules_result['Speaking']['band_score'] = $result['student_info']['band_score'];
				$update_data[] = array("book_speaking_slots_id" => $book_speaking_slots_id , "result_sent" => 1);
				
				foreach($result as $module_un => $module){
					$module_name_array = explode("-" , $module_un);
					if(isset($module_name_array['2'])):
						$module_name                  = $module_name_array['2'];
						$modules_result["$module_name"] = $module;
					endif;
				}
				
				
				
				$subject = "Mock test result";
				
				$this->sendSMS_toStudentMock($modules_result);
				$this->sendEmail_Mocktest($student_email,$subject,$modules_result);
				
				$spData             = $this->Student_model->get_student_info($stud_id);       
				$programe_id        = $spData['programe_id'];
				$test_module_id     = $spData['test_module_id'];
				$student_fname      = $spData['fname'];
				$student_mobile     = $spData['mobile'];
				$student_email      = $spData['email'];
				
				/* Send notification to student */				
				$this->insert_notification(RESULT_SUC_MOCK,$stud_id,$programe_id,$test_module_id,"","mocktest-result-$unique_test_id","");
				
			}
		
			$this->Student_results_model->updatePendingMocktests($update_data);
		}		
	}
    
    function sendSMS_toStudentMock($data){
       $fname = $data['name'];
       $mobile = $data['mobile'];

	   $Listening = $Reading = $Writing = $Speaking = 0;
	
		if(isset($data['Listening']['band_score']))
			$Listening = $data['Listening']['band_score'];			
		
		if(isset($data['Reading']['band_score']))
			$Reading = $data['Reading']['band_score'];			
		
		if(isset($data['Writing']['band_score']))
			$Writing = $data['Writing']['band_score'];			
		
		if(isset($data['Speaking']['band_score']))
			$Speaking = $data['Speaking']['band_score'];	
		
		
		$total = $this->calc_total_band($Listening , $Reading , $Writing , $Speaking );
		
	   define('SMS_MESSAGE', 'Dear '.$fname.',%0a%0aYour Mocktest Result is declared.%0a%0aListening : '.$Listening.'.%0a%Reading : '.$Reading.'.%0a%Writing : '.$Writing.'.%0a%Speaking : '.$Speaking.'.%0a%Overall score : '.$total.'. %0a%0aRegards: %0aTeam MasterPrep');

		$data_sms ="username=".API_USERNAME."&password=".API_PASSWORD."&to=".$mobile."&from=".API_FROM."&text=".SMS_MESSAGE;
		$response = $this->curlpostdata(API_URL, $data_sms);

		
    }
    
    public function sendEmail_Mocktest($email,$subject,$mail_data){
		$this->load->library('email');
        $this->email->set_mailtype("html");
        $this->email->set_newline("\r\n");
        $this->email->from(FROM_EMAIL, FROM_NAME);
        $this->email->to($email);
        //$this->email->bcc(ADMIN_EMAIL_BCC);
        $this->email->subject($subject);
        $mail_data['total'] = $this->calc_total_band($mail_data['Listening']['band_score'] , $mail_data['Reading']['band_score'] , $mail_data['Writing']['band_score'] , $mail_data['Speaking']['band_score'] );
                                        
        $body = $this->load->view('emails/sendEmail_MockTest_Result.php' ,$mail_data,TRUE);
        $this->email->message($body);
        $this->email->send();
    }

	function send_speaking_reminder(){
		$bookings    = $this->Student_results_model->get_bookings_reminder();
		$update_data = array();
		foreach($bookings as $booking){
			$stud_id         = $booking['stud_id'];
			$book_speaking_slots_id  = $booking['book_speaking_slots_id'];
			$unique_test_id  = $booking['unique_test_id'];
			$meeting_id      = $booking['meeting_id'];
			$meeting_url     = $booking['meeting_url'];
			$mobile          = $booking['mobile'];
			$email           = $booking['email'];
			$fname           = $booking['fname'];
			$lname           = $booking['lname'];			
			$zoneName        = $booking['zoneName'];
			$stu_booking_info= json_decode($booking['stu_booking_info']);
			
			$booking_time = $stu_booking_info->booking_date." ".$stu_booking_info->booking_starttime;
			if(DFT_SPEAKING_EXAMINER_TIMEZONE != $zoneName){
				$start_time_conv = new DateTime($booking_time , new DateTimeZone($zoneName));							
				$start_time_conv->setTimezone(new DateTimeZone(DFT_SPEAKING_EXAMINER_TIMEZONE));
				$booking_dt = $start_time_conv->format('Y-m-d G:i');
			}else{
				$booking_dt = $booking_time;
			}
			
			$date   = strtotime($booking_dt);//Converted to a PHP date (a second count)

			//Calculate difference
			$diff=$date-time();//time returns current time in seconds
			$days=floor($diff/(60*60*24));//seconds/minute*minutes/hour*hours/day)
			$hour=round(($diff-$days*60*60*24)/(60*60));

			//Report			
			//if ($hour){
			if ($hour == 1){
				$return = "Time to send Reminder";				
				$subject = "MockTest Speaking Reminder";
				$reminder_data = array("name" => $fname." ".$lname , "mobile" => $mobile ,"datetime" => $booking_dt,"meeting_id" => $meeting_id,"meeting_url" => $meeting_url);				
				
				$this->sendSMS_toRemindMock($reminder_data);
				$this->sendEmail_RemindMocktest($email,$subject,$reminder_data);
				
				$spData             = $this->Student_model->get_student_info($stud_id);       
				$programe_id        = $spData['programe_id'];
				$test_module_id     = $spData['test_module_id'];
				$student_fname      = $spData['fname'];
				$student_mobile     = $spData['mobile'];
				$student_email      = $spData['email'];
				
				/* Send notification to student */				
				$this->insert_notification(MOCK_SPEAKING_REMINDER,$stud_id,$programe_id,$test_module_id,"","mocktest-reminder-$unique_test_id","");	
				
				$update_data[] = array("book_speaking_slots_id" => $book_speaking_slots_id , "reminder_sent" => 1);	
			}
		}
		
		if(!empty($update_data))
			$this->Student_results_model->updatePendingMocktests($update_data);
			
	}
	
	function sendSMS_toRemindMock($data){
       $fname       = $data['name'];
       $mobile      = $data['mobile'];
       $datetime    = $data['datetime'];
       $meeting_id  = $data['meeting_id'];
       $meeting_url = $data['meeting_url'];		
	   $content     = "";
		
	   if($meeting_id != ""){
		   $content .= "%0a%0a Meeting ID is $meeting_id .";
		}
	   
	   if($meeting_url != ""){
		   $content .= "%0a%0a Meeting URL is $meeting_url .";
		}
	   
	   define('SMS_MESSAGE', 'Dear '.$fname.',%0a%0aYour Mocktest Speaking has been scheduled at '.$datetime.''.$content.' .%0a%0a Be Ready on time. %0a%0aRegards: %0aTeam MasterPrep');

		$data_sms ="username=".API_USERNAME."&password=".API_PASSWORD."&to=".$mobile."&from=".API_FROM."&text=".SMS_MESSAGE;
		$response = $this->curlpostdata(API_URL, $data_sms);
		
		
    }
    
    public function sendEmail_RemindMocktest($email,$subject,$mail_data){
        $this->load->library('email');
        $this->email->set_mailtype("html");
        $this->email->set_newline("\r\n");
        $this->email->from(FROM_EMAIL, FROM_NAME);
        $this->email->to($email);
        //$this->email->bcc(ADMIN_EMAIL_BCC);
        $this->email->subject($subject);
        $body = $this->load->view('emails/sendEmail_MockTest_Reminder.php' ,$mail_data,TRUE);
        $this->email->message($body);
        $this->email->send();
    }

    
}
