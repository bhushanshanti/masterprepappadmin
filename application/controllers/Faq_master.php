<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/ 
class Faq_master extends MY_Controller{
    function __construct()
    {
        parent::__construct();
        if (!$this->_is_logged_in()) {$this->session->set_userdata('lock_url', $_SERVER['REDIRECT_QUERY_STRING']); redirect('/login');}
        $this->load->model('FAQ_master_model');
        $this->load->model('Test_module_model');

    }
    /*
     * Listing of tips_tricks_master
     */
    function index(){
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $this->load->library('pagination');
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('faq_master/index?');
        $config['total_rows'] = $this->FAQ_master_model->get_all_faq_master_count();
        $this->pagination->initialize($config);
        $data['title'] = 'Faq master';
        $data['tips_tricks_master'] = $this->FAQ_master_model->get_all_faq_master($params);        
        $data['_view'] = 'faq_master/index';
        $this->load->view('layouts/main',$data);
    }

    /*
     * Adding a new tips_tricks_master
     */
    function add()
    {   
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $data['title'] = 'Add Faq master';
        $this->load->library('form_validation');
		$this->form_validation->set_rules('parent_subject','Subject','required|trim');
        $this->form_validation->set_rules('test_module_id','Test','required');        	
		if($this->form_validation->run())     
        {   
            $user = $this->session->userdata('admin_login_data');
            foreach ($user as $d){$by_user=$d->id;}
            $params = array(
				'active' => $this->input->post('active'),
                'parent_subject' => $this->input->post('parent_subject'),
				'test_module_id' => $this->input->post('test_module_id'),
                'by_user' => $by_user,
                'icon' => $this->input->post('icon') ? $this->input->post('icon') : NULL,
                'short_desc' => $this->input->post('short_desc'),
            );            
            $id = $this->FAQ_master_model->add_faq_master($params);
            if($id){
                $this->session->set_flashdata('flsh_msg', SUCCESS_MSG);
                redirect('faq_master/index');
            }else{
                $this->session->set_flashdata('flsh_msg', FAILED_MSG);
                redirect('faq_master/add');
            }            
        }
        else
        {            
            $data['all_test_module'] = $this->Test_module_model->get_all_test_module_active();
            $data['_view'] = 'faq_master/add';
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a tips_tricks_master
     */
    function edit($id)
    {         
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $data['title'] = 'Edit Faq master';
        $data['faq_master'] = $this->FAQ_master_model->get_faq_master($id);
        
        if(isset($data['faq_master']['id']))
        {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('test_module_id','Test','required');
            $this->form_validation->set_rules('parent_subject','Subject','required|trim');		
			if($this->form_validation->run())     
            {   
                $user = $this->session->userdata('admin_login_data');
                foreach ($user as $d){$by_user=$d->id;}
                $params = array(
					'active' => $this->input->post('active'),
                    'test_module_id' => $this->input->post('test_module_id'),
					'parent_subject' => $this->input->post('parent_subject'),
                    'by_user' => $by_user,
                    'icon' => $this->input->post('icon') ? $this->input->post('icon') : NULL,
                    'short_desc' => $this->input->post('short_desc'),
                );
                $idd = $this->FAQ_master_model->update_faq_master($id,$params); 
                if($idd){
                    $this->session->set_flashdata('flsh_msg', UPDATE_MSG);
                    redirect('faq_master/index');
                }else{
                    $this->session->set_flashdata('flsh_msg', UPDATE_FAILED_MSG);           
                    redirect('faq_master/edit/'.$id);
                } 
            }
            else
            {
                 $data['all_test_module'] = $this->Test_module_model->get_all_test_module_active();
                $data['_view'] = 'faq_master/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error(ITEM_NOT_EXIST);
    }    
    /*
     * Deleting faq_master
     */
    function remove($id)
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $faq_master = $this->FAQ_master_model->get_faq_master($id);
        if(isset($faq_master['id']))
        {
            $this->FAQ_master_model->delete_faq_master($id);
            $this->session->set_flashdata('flsh_msg', DEL_MSG);
            redirect('faq_master/index');
        }
        else
            show_error(ITEM_NOT_EXIST);
    }
    
}
