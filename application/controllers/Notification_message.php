<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/
   class Notification_message extends MY_Controller{

    function __construct()
    {
        parent::__construct();
        if (!$this->_is_logged_in()) {$this->session->set_userdata('lock_url', $_SERVER['REDIRECT_QUERY_STRING']); redirect('/login');}
        $this->load->model('Notification_subject_model');  
        $this->load->model('Notification_message_model');                 
    }    
    /*
     * Listing of all category
     */
    function index()
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $this->load->library('pagination');
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('notification_message/index?');
        $config['total_rows'] = $this->Notification_message_model->get_all_message_count();
        $this->pagination->initialize($config);
        $data['notification_message'] = $this->Notification_message_model->get_all_message($params);
        $data['title'] = 'Notification Message';
        $data['_view'] = 'notification_message/index';
        $this->load->view('layouts/main',$data);
    }
    /*
     * Adding a new category
     */
    function add()
    {   
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $data['title'] = 'Add Message';
        $this->load->library('form_validation');
		$this->form_validation->set_rules('message','Message','required|trim');
		$this->form_validation->set_rules('subject_id','Subject','required');        
		
		if($this->form_validation->run())     
        {   
            $user = $this->session->userdata('admin_login_data');
            foreach ($user as $d){$by_user=$d->id;}
            $params = array(
				'active' => $this->input->post('active'),
				'subject_id' => $this->input->post('subject_id'),
				'message' => $this->input->post('message'),                
                'by_user' => $by_user,
            );
            $dup = $this->Notification_message_model->dupliacte_message($params['subject_id'],$params['message']);
            if($dup=='DUPLICATE'){
                $this->session->set_flashdata('flsh_msg', DUP_MSG);
                redirect('notification_message/add');
            }else{
                $id = $this->Notification_message_model->add_message($params);
                if($id){
                    $this->session->set_flashdata('flsh_msg', SUCCESS_MSG);
                    redirect('notification_message/index');
                }else{                    
                    $this->session->set_flashdata('flsh_msg', FAILED_MSG);
                    redirect('notification_message/add');
                } 
            }
        }
        else
        {			
			$data['all_subject_list'] = $this->Notification_subject_model->get_all_notification_subject_active();            
            $data['_view'] = 'notification_message/add';
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a category_master
     */
    function edit($messege_id)
    {   
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $data['title'] = 'Edit Message';
        $data['notification_message'] = $this->Notification_message_model->get_message($messege_id);
        if(isset($data['notification_message']['messege_id']))
        {
            $this->load->library('form_validation');
			$this->form_validation->set_rules('message','Message','required|trim');
            $this->form_validation->set_rules('subject_id','Subject','required');           
		
			if($this->form_validation->run())     
            {   
                $user = $this->session->userdata('admin_login_data');
                foreach ($user as $d){$by_user=$d->id;}
                $params = array(
                'active' => $this->input->post('active'),
                'subject_id' => $this->input->post('subject_id'),
                'message' => $this->input->post('message'),                
                'by_user' => $by_user,
            );
                $id = $this->Notification_message_model->update_message($messege_id,$params);
                if($id){
                    $this->session->set_flashdata('flsh_msg', UPDATE_MSG);           
                    redirect('notification_message/index');
                }else{
                    $this->session->set_flashdata('flsh_msg', UPDATE_FAILED_MSG);           
                    redirect('notification_message/edit/'.$message_id);
                }                
            }
            else
            {				
				$data['all_subject_list'] = $this->Notification_subject_model->get_all_notification_subject_active(); 
                $data['_view'] = 'notification_message/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error(ITEM_NOT_EXIST);
    } 

    /*
     * Deleting category_master
     */
    function remove($message_id)
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $mes = $this->Notification_message_model->get_message($message_id);
        if(isset($mes['messege_id']))
        {
            $this->Notification_message_model->delete_message($message_id);
            $this->session->set_flashdata('flsh_msg', DEL_MSG);
            redirect('notification_message/index');
        }
        else
            show_error(ITEM_NOT_EXIST);
    }

    
    
}
