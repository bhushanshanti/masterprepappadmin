<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/
   class City extends MY_Controller{

    function __construct()
    {
        parent::__construct();
        if (!$this->_is_logged_in()) {$this->session->set_userdata('lock_url', $_SERVER['REDIRECT_QUERY_STRING']); redirect('/login');}
        $this->load->model('City_model');
        $this->load->model('State_model');
        $this->load->model('Country_model');        
    }    
    /*
     * Listing of all category
     */
    function index()
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $this->load->library('pagination');
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('city/index?');
        $config['total_rows'] = $this->City_model->get_all_city_count();
        $this->pagination->initialize($config);
        $data['city'] = $this->City_model->get_all_city($params);
        $data['title'] = 'Cities/Districts';
        $data['_view'] = 'city/index';
        $this->load->view('layouts/main',$data);
    }
    /*
     * Adding a new category
     */
    function add()
    {   
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $data['title'] = 'Add City';
        $this->load->library('form_validation');
		$this->form_validation->set_rules('city_name','City Name','required|trim');
        $this->form_validation->set_rules('state_id','State name','required');
		$this->form_validation->set_rules('country_id','Country name','required');        
		
		if($this->form_validation->run())     
        {   
            $user = $this->session->userdata('admin_login_data');
            foreach ($user as $d){$by_user=$d->id;}
            $params = array(
				'active' => $this->input->post('active'),
				'country_id' => $this->input->post('country_id'),
                'state_id' => $this->input->post('state_id'),
				'city_name' => $this->input->post('city_name'),                
                'by_user' => $by_user,
            ); 
            $id = $this->City_model->add_city($params);
            if($id){
                $this->session->set_flashdata('flsh_msg', SUCCESS_MSG);
                redirect('city/index');
            }else{                    
                $this->session->set_flashdata('flsh_msg', FAILED_MSG);
                redirect('city/add');
            }
            
        }
        else
        {			
			$data['all_country_list'] = $this->Country_model->get_all_country_active();            
            $data['_view'] = 'city/add';
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a category_master
     */
    function edit($city_id)
    {   
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $data['title'] = 'Edit city';
        $data['city'] = $this->City_model->get_city($city_id);
        if(isset($data['city']['city_id']))
        {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('city_name','City name','required|trim');
            $this->form_validation->set_rules('state_id','state name','required');
            $this->form_validation->set_rules('country_id','country name','required');           
		
			if($this->form_validation->run())     
            {   
                $user = $this->session->userdata('admin_login_data');
                foreach ($user as $d){$by_user=$d->id;}
                $params = array(
					'active' => $this->input->post('active'),
					'country_id' => $this->input->post('country_id'),
                    'state_id' => $this->input->post('state_id'),
					'city_name' => $this->input->post('city_name'),                    
                    'by_user' => $by_user,
                );
                $id = $this->City_model->update_city($city_id,$params);
                if($id){
                    $this->session->set_flashdata('flsh_msg', UPDATE_MSG);           
                    redirect('city/index');
                }else{
                    $this->session->set_flashdata('flsh_msg', UPDATE_FAILED_MSG);           
                    redirect('city/edit/'.$city);
                }                
            }
            else
            {				
				$data['all_country_list'] = $this->Country_model->get_all_country();
                $data['all_state_list'] = $this->State_model->get_state_list($data['city']['country_id']);
                $data['_view'] = 'city/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error(ITEM_NOT_EXIST);
    } 

    /*
     * Deleting category_master
     */
    function remove($city_id)
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $city = $this->State_model->get_city($city_id);
        if(isset($city['city_id']))
        {
            $this->City_model->delete_city($city_id);
            $this->session->set_flashdata('flsh_msg', DEL_MSG);
            redirect('city/index');
        }
        else
            show_error(ITEM_NOT_EXIST);
    }


    function get_state_list(){

        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $country_id = $this->input->post('country_id');
        if(isset($country_id)){            
            $response =  $this->State_model->get_state_list($country_id);
            echo json_encode($response);
        }else{
            header('Content-Type: application/json');
            $response = ['msg'=>'list not available!', 'status'=>'false'];
            echo json_encode($response);
        }
    }

    function get_city_list(){ 

        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $state_id = $this->input->post('state_id');
        if(isset($state_id)){            
            $response =  $this->City_model->get_city_list($state_id);
            echo json_encode($response);
        }else{
            header('Content-Type: application/json');
            $response = ['msg'=>'list not available!', 'status'=>'false'];
            echo json_encode($response);
        }
    }

    
    
}
