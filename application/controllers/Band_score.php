<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/ 
class Band_score extends MY_Controller{
    
    function __construct()
    {
        parent::__construct();
       if (!$this->_is_logged_in()) {$this->session->set_userdata('lock_url', $_SERVER['REDIRECT_QUERY_STRING']); redirect('/login');}
        $this->load->model('Band_score_model');
        $this->load->model('Proficiency_level_model');     
        $controller_name = 'Band_score';
        $cn = $controller_name.''.'.php';
    }
    /*
     * Listing of all band scores
     */
    function index()
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $this->load->library('pagination');
        $params['limit'] = RECORDS_PER_PAGE;
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('band_score/index?');
        $config['total_rows'] = $this->Band_score_model->get_all_band_scores_count();
        $this->pagination->initialize($config);
        $data['band_scores'] = $this->Band_score_model->get_all_band_scores($params);
        $data['title'] = 'Band Score'.'- '.ACD;
        $data['_view'] = 'band_score/index';
        $this->load->view('layouts/main',$data);
    }

    function index_gt()
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $this->load->library('pagination');
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('band_score/index_gt?');
        $config['total_rows'] = $this->Band_score_model->get_all_band_scores_count_gt();
        $this->pagination->initialize($config);
        $data['band_scores'] = $this->Band_score_model->get_all_band_scores_gt($params);
        $data['title'] = 'Band Score'.'- '.GT;;
        $data['_view'] = 'band_score/index';
        $this->load->view('layouts/main',$data);
    }
    /*
     * Adding a new band score
     */
    function add()
    {   
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $this->load->library('form_validation');
        $this->form_validation->set_rules('category_id','Category','required');
        $this->form_validation->set_rules('band_score','Band Score','required|trim');
        $this->form_validation->set_rules('band_range_lower','Lower Band Range','required|trim');
        $this->form_validation->set_rules('band_range_upper','Upper Band Range','required|trim');
        $this->form_validation->set_rules('pro_id','Proficiency level','required|trim');
        $data['title'] = 'Add band score';
		
		if($this->form_validation->run())     
        {   
            $user = $this->session->userdata('admin_login_data');
            foreach ($user as $d){$by_user=$d->id;}
            $params = array(				
				'category_id' => $this->input->post('category_id'),
				'band_score' => $this->input->post('band_score'),
				'band_range_lower' => $this->input->post('band_range_lower'),
                'band_range_upper' => $this->input->post('band_range_upper'),
                'pro_id' => $this->input->post('pro_id'),
                'active' => $this->input->post('active'),
                'by_user' => $by_user,
            );
            $id = $this->Band_score_model->add_band_score($params);
            $catId = $params['category_id'];
            if($id and ($catId>=44 and $catId<=47)){
                $this->session->set_flashdata('flsh_msg', SUCCESS_MSG);
                redirect('band_score/index');
            }elseif ($id and ($catId>=48 and $catId<=51)) {
                $this->session->set_flashdata('flsh_msg', SUCCESS_MSG);
                redirect('band_score/index_gt');
            }
            else{
                $this->session->set_flashdata('flsh_msg', FAILED_MSG);
                redirect('band_score/add');
            }            
        }
        else
        {
			$this->load->model('Category_master_model');
			$data['all_category_masters'] = $this->Category_master_model->get_all_category_masters_active(); 
            $data['all_pro_level'] = $this->Proficiency_level_model->get_all_proficiency_level_active();            
            $data['_view'] = 'band_score/add';
            $this->load->view('layouts/main',$data);
        }
    }  
    /*
     * Editing a band_score
     */
    function edit($band_score_id)
    {   
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $data['title'] = 'Edit band score';
        $data['band_score'] = $this->Band_score_model->get_band_score($band_score_id);        
        if(isset($data['band_score']['band_score_id']))
        {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('category_id','Category Id','required');
            $this->form_validation->set_rules('band_score','Band Score','required|trim');
            $this->form_validation->set_rules('band_range_lower','Band Range Lower','required|trim');
            $this->form_validation->set_rules('band_range_upper','Band Range Upper','required|trim');
            $this->form_validation->set_rules('pro_id','Proficiency level','required|trim');
			if($this->form_validation->run())     
            {   
                $user = $this->session->userdata('admin_login_data');
                foreach ($user as $d){$by_user=$d->id;}
                $params = array(
    				'category_id' => $this->input->post('category_id'),
                    'band_score' => $this->input->post('band_score'),
                    'band_range_lower' => $this->input->post('band_range_lower'),
                    'band_range_upper' => $this->input->post('band_range_upper'),
                    'pro_id' => $this->input->post('pro_id'),
                    'active' => $this->input->post('active'),
                    'by_user' => $by_user,
                );
                $id = $this->Band_score_model->update_band_score($band_score_id,$params);
                $catId = $params['category_id'];
                if($id and ($catId>=44 and $catId<=47)){
                    $this->session->set_flashdata('flsh_msg', UPDATE_MSG);
                    redirect('band_score/index');
                }elseif ($id and ($catId>=48 and $catId<=51)) {
                    $this->session->set_flashdata('flsh_msg', UPDATE_MSG);
                    redirect('band_score/index_gt');
                }
                else{
                    $this->session->set_flashdata('flsh_msg', UPDATE_FAILED_MSG);
                    redirect('band_score/edit/'.$band_score_id);
                }
            }
            else
            {
				$this->load->model('Category_master_model');
				$data['all_category_masters'] = $this->Category_master_model->get_all_category_masters_active();
                $data['all_pro_level'] = $this->Proficiency_level_model->get_all_proficiency_level_active();
                $data['_view'] = 'band_score/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error(ITEM_NOT_EXIST);
    }
    /*
     * Deleting band_score
     */
    function remove($band_score_id)
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $band_score = $this->Band_score_model->get_band_score($band_score_id);
        if(isset($band_score['band_score_id']))
        {
            $this->Band_score_model->delete_band_score($band_score_id);
            $this->session->set_flashdata('flsh_msg', DEL_MSG);
            redirect('band_score/index');
        }
        else
            show_error(ITEM_NOT_EXIST);
    }
    
}
