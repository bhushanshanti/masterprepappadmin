<?php
/**
 * @test_module         MasterPrep
 * @subtest_module      Test Series
 * @author          Mohammad Haroon
 *
 **/ 
class Test_module extends MY_Controller{
    
    function __construct()
    {
        parent::__construct();
        if (!$this->_is_logged_in()) {$this->session->set_userdata('lock_url', $_SERVER['REDIRECT_QUERY_STRING']); redirect('/login');}
        $this->load->model('Test_module_model'); 
        $this->load->model('Programe_master_model');       
    }
    /*
     * Listing of test_modules
     */
    function index()
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $this->load->library('pagination');
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('test_module/index?');
        $config['total_rows'] = $this->Test_module_model->get_all_test_module_count();
        $this->pagination->initialize($config);
        $data['title'] = 'All Test Type';
        $data['test_modules'] = $this->Test_module_model->get_all_test_module($params);
        $data['_view'] = 'test_module/index';
        $this->load->view('layouts/main',$data);
    }
    /*
     * Adding a new test_module
     */
    function add()
    {   
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $data['title'] = 'Add Test module';
        $this->load->library('form_validation');
		$this->form_validation->set_rules('test_module_name','Test module name','required|trim');
        $this->form_validation->set_rules('programe_id','Programme','required');
		$this->form_validation->set_rules('test_module_desc','Description','trim');		
		
		if($this->form_validation->run())     
        {   
            $user = $this->session->userdata('admin_login_data');
            foreach ($user as $d){$by_user=$d->id;}
            $params = array(
				'active' => $this->input->post('active'),
				'test_module_name' => $this->input->post('test_module_name'),
                'programe_id' => $this->input->post('programe_id'),				
				'test_module_desc' => $this->input->post('test_module_desc'),
                'by_user' => $by_user,
            );            
            $id = $this->Test_module_model->add_test_module($params);
            if($id){
                $this->session->set_flashdata('flsh_msg', SUCCESS_MSG);
                redirect('test_module/index');
            }else{
                $this->session->set_flashdata('flsh_msg', FAILED_MSG);
                redirect('test_module/add');
            }            
        }
        else
        {    
        $data['all_programe_masters'] = $this->Programe_master_model->get_all_programe_masters_active();        
            $data['_view'] = 'test_module/add';
            $this->load->view('layouts/main',$data);
            

            ///print_r($data['all_programe_masters']);
        }
    }  

    /*
     * Editing a test_module
     */
    function edit($test_module_id)
    {   
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $data['title'] = 'Edit test_module';
        $data['test_module'] = $this->Test_module_model->get_test_module($test_module_id);
        if(isset($data['test_module']['test_module_id']))
        {
            $data['title'] = 'Add Test module';
            $this->load->library('form_validation');
            $this->form_validation->set_rules('test_module_name','Test module name','required|trim');
            $this->form_validation->set_rules('programe_id','Programme','required');
            $this->form_validation->set_rules('test_module_desc','Description','trim');			
		
			if($this->form_validation->run())
            {   
                $user = $this->session->userdata('admin_login_data');
                foreach ($user as $d){$by_user=$d->id;}
                $params = array(
					'active' => $this->input->post('active'),
					'test_module_name' => $this->input->post('test_module_name'),	
                    'programe_id' => $this->input->post('programe_id'),				
					'test_module_desc' => $this->input->post('test_module_desc'),
                    'by_user' => $by_user,
                );
                $id = $this->Test_module_model->update_test_module($test_module_id,$params);
                if($id){
                    $this->session->set_flashdata('flsh_msg', UPDATE_MSG);           
                    redirect('test_module/index');
                }else{
                    $this->session->set_flashdata('flsh_msg', UPDATE_FAILED_MSG);           
                    redirect('test_module/edit/'.$test_module_id);
                }
            }
            else
            {
                    $data['all_programe_masters'] = $this->Programe_master_model->get_all_programe_masters_active();
                $data['_view'] = 'test_module/edit';
                $this->load->view('layouts/main',$data);
                
            }
        }
        else
            show_error(ITEM_NOT_EXIST);
    }
    
    /*
     * Deleting test_module
     */
    function remove($test_module_id)
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $test_module = $this->Test_module_model->get_test_module($test_module_id);
        if(isset($test_module['test_module_id']))
        {
            $this->Test_module_model->delete_test_module($test_module_id);
            $this->session->set_flashdata('flsh_msg', DEL_MSG);
            redirect('test_module/index');
        }
        else
            show_error(ITEM_NOT_EXIST);
    }    
    
}
