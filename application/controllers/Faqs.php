<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Harman
 *
 **/ 
class Faqs extends MY_Controller{
    
    function __construct()
    {
        parent::__construct();
        if (!$this->_is_logged_in()) {$this->session->set_userdata('lock_url', $_SERVER['REDIRECT_QUERY_STRING']); redirect('/login');}
        $this->load->model('FAQ_model');
        $this->load->model('Tips_tricks_master_model');
    }
    /*
     * Listing of FAQ Academic
     */
    function index(){
        $rid = $this->uri->segment(3);
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $this->load->library('pagination');
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('faqs/index?');
        $config['total_rows'] = $this->FAQ_model->get_all_faqs_count();
        $this->pagination->initialize($config);
        $data['title'] = 'FAQ';
        $data['tips_tricks'] = $this->FAQ_model->get_all_faqs($params,$rid);
        $data['_view'] = 'faqs/index';
        $this->load->view('layouts/main',$data);
    }

    
    /*
     * Adding a new FAQ
     */
    function add(){   
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $data['title'] = 'Add FAQ';
        $this->load->library('form_validation');
		$this->form_validation->set_rules('pid','Parent subject','required');
        $this->form_validation->set_rules('question','Question','required|trim');
        $this->form_validation->set_rules('answer','Answer','required|trim');
		
		if($this->form_validation->run())     
        {   
            $user = $this->session->userdata('admin_login_data');
            foreach ($user as $d){$by_user=$d->id;}
            $params = array(
                'pid'  => $this->input->post('pid'),
				'question'  => $this->input->post('question'),
                'answer'    => $this->input->post('answer'),
                'display_order' => $this->input->post('display_order'),
                'active'    => $this->input->post('active'),
                'by_user' => $by_user,
            );            
            $id = $this->FAQ_model->add_faqs($params);
            if($id){
                $this->session->set_flashdata('flsh_msg', SUCCESS_MSG);
                redirect('faqs/index');
            }else{
                $this->session->set_flashdata('flsh_msg', FAILED_MSG);
                redirect('faqs/add');
            }            
        }
        else
        { 
           $data['all_parent_masters'] = $this->Tips_tricks_master_model->get_all_tips_tricks_master_active();
            $data['_view'] = 'faqs/add';
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a FAQ
     */
    function edit($faq_id)
    {   
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $data['title'] = 'Edit FAQ';
        $data['tips_tricks'] = $this->FAQ_model->get_faqs($faq_id);
        if(isset($data['tips_tricks']['faq_id']))
        {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('pid','Parent subject','required');
            $this->form_validation->set_rules('question','Question','required|trim');
            $this->form_validation->set_rules('answer','Answer','required|trim'); 			
		
			if($this->form_validation->run())     
            {   
                $user = $this->session->userdata('admin_login_data');
                foreach ($user as $d){$by_user=$d->id;}
                $params = array(
                    'pid'  => $this->input->post('pid'),
                    'question'  => $this->input->post('question'),
                    'answer'    => $this->input->post('answer'),
                    'display_order' => $this->input->post('display_order'),
                    'active'    => $this->input->post('active'),
                    'by_user' => $by_user,
                );
                $id = $this->FAQ_model->update_faqs($faq_id,$params);

                if($id){
                    $this->session->set_flashdata('flsh_msg', UPDATE_MSG);
                    redirect('faqs/index');
                }else{
                    $this->session->set_flashdata('flsh_msg', UPDATE_FAILED_MSG);           
                    redirect('faqs/edit/'.$faq_id);
                }                
            }
            else
            { 
                $data['all_parent_masters'] = $this->Tips_tricks_master_model->get_all_tips_tricks_master_active();
                $data['_view'] = 'faqs/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error(ITEM_NOT_EXIST);
    }   
    /*
     * Deleting FAQ
     */
    function remove($faq_id)
    {
        //access control start
        $cn = $this->router->fetch_class().''.'.php';
        $mn = $this->router->fetch_method();        
        if(!$this->_has_access($cn,$mn)) {redirect('error_cl/index');}
        //access control ends
        $tips_tricks = $this->FAQ_model->get_faqs($faq_id);
        if(isset($tips_tricks['faq_id']))
        {
            $this->FAQ_model->delete_faqs($faq_id);
            $this->session->set_flashdata('flsh_msg', DEL_MSG);
            redirect('faqs/index');
        }
        else
            show_error(ITEM_NOT_EXIST);
    }    
    
}
