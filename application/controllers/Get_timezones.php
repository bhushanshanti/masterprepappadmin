<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/ 
class Get_timezones extends MY_Controller{
    
    function __construct()
    {
        parent::__construct();
        if (!$this->_is_logged_in()) {$this->session->set_userdata('lock_url', $_SERVER['REDIRECT_QUERY_STRING']); redirect('/login');}

        $this->load->model('Get_timezones_model'); 
      
        
    }
    /*
     * Listing of Tips_tricks Academic
     */
    function Get_timezonebycid()
    {
        $country_code = $this->input->post('country_code');
        $timezone_data = $this->Get_timezones_model->get_all_timezonesbycountry_code($country_code);

        $html = '<option data-subtext="" value="">Select Timezone</option>';

        if(!empty($timezone_data)){
            foreach ($timezone_data as $key => $value) {
              $html .= "<option data-subtext='".$value['zoneName']."(".$country_code.")' value='".$value['id']."'>".$value['zoneName']."(".$country_code.")</option>";
            }            
         }else{
              $html .= "";   
        }        

       echo $html;

    }

     
    
}
