<?php

 class Test_module_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get test_module_master by test_module_id
     */
    function get_test_module($test_module_id)
    {
        return $this->db->get_where('test_module',array('test_module_id'=>$test_module_id))->row_array();
    }
    
    /*
     * Get all test_module count
     */
    function get_all_test_module_count()
    {
        $this->db->from('test_module');
        return $this->db->count_all_results();
    }
        
    /*
     * Get all test_module
     */
    function get_all_test_module($params = array())
    {  
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }  
        $this->db->select('*');
        $this->db->from('test_module tm'); 
         $this->db->join('programe_masters pgm', 'tm.programe_id=pgm.programe_id','left');
        $this->db->order_by('tm.test_module_id', 'desc');
        return $this->db->get()->result_array();
    }

    function get_all_test_module_active(){       
        
        $this->db->select('tm.test_module_id,tm.test_module_name','pgm.programe_name');
        $this->db->from('test_module tm');  
        $this->db->join('programe_masters pgm', 'tm.programe_id=pgm.programe_id','left');    
        $this->db->where('tm.active', 1);  
        $this->db->order_by('test_module_id', 'desc');
        return $this->db->get()->result_array();
    }

    function get_course($programe_id){       
        
        $this->db->select('test_module_id,test_module_name');
        $this->db->from('test_module');    
        $this->db->where(array('enqActive'=>1,'active'=>1));  
        $this->db->order_by('test_module_name', 'ASC');
        return $this->db->get()->result_array();
    }
        
    /*
     * function to add new test_module_master
     */
    function add_test_module($params)
    {
        $this->db->insert('test_module',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update test_module_master
     */
    function update_test_module($test_module_id,$params)
    {
        $this->db->where('test_module_id',$test_module_id);
        return $this->db->update('test_module',$params);
    }
    
    /*
     * function to delete test_module_master
     */
    function delete_test_module($test_module_id)
    {
        return $this->db->delete('test_module',array('test_module_id'=>$test_module_id));
    }
}
