<?php

 
class Tips_tricks_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get tipstricks by tips_tricks_id
     */
    function get_tipstricks($tips_tricks_id)
    {
        return $this->db->get_where('tips_and_tricks',array('tips_tricks_id'=>$tips_tricks_id))->row_array();
    }
    
    /*
     * Get all tipstricks count
     */
    function get_all_tipstricks_count()
    {
        $this->db->from('tips_and_tricks');
        return $this->db->count_all_results();
    }

    /*
     * Get all tipstricks
     */
    function get_all_tipstricks($params = array(),$rid)
    {        
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        $this->db->select('
            tt.`tips_tricks_id`,
            tt.`question`, 
            tt.`answer`,
            tt.`display_order`,            
            tt.`active`,
            tm.`test_module_name`,
            ttm.`parent_subject`,
        ');
        $this->db->from('`tips_and_tricks` tt'); 
        $this->db->join('`tips_tricks_master` ttm', 'ttm.`id`= tt.`pid`');
        $this->db->join('`test_module` tm', 'tm.`test_module_id`= ttm.`test_module_id`');
        if($rid!=''){
            $this->db->where(array('pid'=>$rid));
        } else{
            $this->db->where(array());
        }               
               
        $this->db->order_by('ttm.`parent_subject` ASC');
        return $this->db->get('')->result_array();
    }    
        
    /*
     * function to add new tipstricks
     */
    function add_tipstricks($params)
    {
        $this->db->insert('tips_and_tricks',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update tipstricks
     */
    function update_tipstricks($tips_tricks_id,$params)
    {
        $this->db->where('tips_tricks_id',$tips_tricks_id);
        return $this->db->update('tips_and_tricks',$params);
    }
    
    /*
     * function to delete tipstricks
     */
    function delete_tipstricks($tips_tricks_id)
    {
        return $this->db->delete('tips_and_tricks',array('tips_tricks_id'=>$tips_tricks_id));
    }
    
}
