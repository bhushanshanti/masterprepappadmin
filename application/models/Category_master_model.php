<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/
 
class Category_master_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get category_master by category_id
     */
    function get_category_master($category_id)
    {
        return $this->db->get_where('category_masters',array('category_id'=>$category_id))->row_array();
    }
    
    /*
     * Get all category_masters count IELTS
     */
    function get_all_category_masters_count()
    {
        $this->db->from('category_masters');
        $this->db->where(array('test_module_id'=>IELTS_ID)); 
        return $this->db->count_all_results();
    }

    /*
     * Get all category_masters count EE
     */
    function get_all_category_masters_count_ee()
    {
        $this->db->from('category_masters');
         $this->db->where(array('test_module_id'=>EE_ID));         
        return $this->db->count_all_results();
    }

    
        
    /*
     * Get all category_masters IELTS
     */
    function get_all_category_masters($params = array())
    {        
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }  
        $this->db->select('cat.`category_id`,cat.`category_name`,cat.`icon`,cat.`active`, pgm.`programe_name`,tm.`test_module_name`');
        $this->db->from('category_masters cat');
        $this->db->join('`test_module` tm', 'tm.`test_module_id`=cat.`test_module_id`'); 
        $this->db->join('`programe_masters` pgm', 'cat.`programe_id`=pgm.`programe_id`');
        $this->db->where(array('cat.test_module_id'=>IELTS_ID));
        return $this->db->get()->result_array();
    }

     /*
     * Get all category_masters EE
     */
    function get_all_category_masters_ee($params = array())
    {
        
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }  
        $this->db->select('cat.`category_id`,cat.`category_name`,cat.`icon`,cat.`active`, pgm.`programe_name`,tm.`test_module_name`');
        $this->db->from('category_masters cat');
        $this->db->join('`test_module` tm', 'tm.`test_module_id`=cat.`test_module_id`'); 
        $this->db->join('`programe_masters` pgm', 'cat.`programe_id`=pgm.`programe_id`');
        $this->db->where(array('cat.test_module_id'=>EE_ID));
        return $this->db->get()->result_array();
    }

    

    function get_category_name_audio($category_id){

        $this->db->select('category_name');
        $this->db->from('category_masters'); 
        $this->db->where('category_id', $category_id);      
        return $this->db->get()->result_array();
        //print_r($this->db->last_query());exit;
    }

    function get_category_id($category_name,$programe_id){

        $this->db->select('category_id,test_module_id');
        $this->db->from('category_masters'); 
        $this->db->where(array('category_name'=> $category_name,'programe_id'=>$programe_id));      
        return $this->db->get()->row_array();
        //print_r($this->db->last_query());exit;
    }    

    function get_all_category_masters_active($params = array()){
       
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        } 
        $this->db->select('`category_id`,`category_name`,`programe_name`');
        $this->db->from('category_masters');
        $this->db->join('programe_masters', 'programe_masters.programe_id = category_masters.programe_id', 'left');
        $this->db->where('category_masters.active', 1);  
        $this->db->order_by('category_id', 'desc');
        return $this->db->get()->result_array();
    }

    function get_all_category_masters_active_tt($test_module_id){       
        
        $this->db->select('`category_id`,`category_name`,`programe_name`');
        $this->db->from('category_masters');
        $this->db->join('programe_masters', 'programe_masters.programe_id = category_masters.programe_id', 'left');
        $this->db->where('category_masters.test_module_id', $test_module_id);  
        $this->db->order_by('category_id', 'desc');
        return $this->db->get()->result_array();
    }        
    /*
     * function to add new category_master
     */
    function add_category_master($params)
    {
        
        $category_name =  $params['category_name'];
        $programe_id =  $params['programe_id'];
        $this->db->where('category_name', $category_name);
        $this->db->where('programe_id', $programe_id);
        $query = $this->db->get('category_masters');
        $count_row = $query->num_rows();
        if ($count_row > 0) {          
            return FALSE;
         } else {
          
            $this->db->insert('category_masters',$params);
            return $this->db->insert_id();
         }
    }

    function dupliacte_category_master($programe_id,$category_name){

        $this->db->where('category_name', $category_name);
        $this->db->where('programe_id', $programe_id);
        $query = $this->db->get('category_masters');
        $count_row = $query->num_rows();
        if ($count_row > 0) {          
            return 'DUPLICATE';
         } else {          
            
            return 'NOT DUPLICATE';
         }
    }
    
    /*
     * function to update category_master
     */
    function update_category_master($category_id,$params)
    {
        $this->db->where('category_id',$category_id);
        return $this->db->update('category_masters',$params);
    }
    
    /*
     * function to delete category_master
     */
    function delete_category_master($category_id)
    {
        return 0;
        return $this->db->delete('category_masters',array('category_id'=>$category_id));
    }
    
    function get_all_packages($params = array())
    {        
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }  
        $this->db->select('pm.`package_id`,pm.`package_name`,pm.`active`, pgm.`programe_name`');
        $this->db->from('package_masters pm');       
        $this->db->join('`programe_masters` pgm', 'pm.`programe_id`=pgm.`programe_id`');
        $this->db->where(array('pm.active'=> 1));
        return $this->db->get()->result_array();
    }
}
