<?php

 
class FAQ_master_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    /*
     * Get notification_subject by id
     */
    function get_faq_master($id)
    {
        return $this->db->get_where('faq_master',array('id'=>$id))->row_array();
    }
    /*
     * Get all faq_master count
     */
    function get_all_faq_master_count()
    {
        $this->db->from('faq_master');
        return $this->db->count_all_results();
    }  
    /*
     * Get all faq_master
     */
    function get_all_faq_master($params = array())
    {        
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        $this->db->select('
            ttm.`id`,
            ttm.`parent_subject`,
            ttm.`icon`,
            ttm.`short_desc`,
            ttm.`active`,
            tm.`test_module_name`
        ');
        $this->db->from('`faq_master` ttm');
        $this->db->join('`test_module` tm', 'tm.`test_module_id`= ttm.`test_module_id`');
        $this->db->order_by('ttm.`parent_subject`','ASC');
        return $this->db->get()->result_array();
    }

    function get_all_faq_master_active($params = array())
    {        
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        $this->db->where('active', '1');
        $this->db->order_by('parent_subject', 'ASC');
        return $this->db->get('faq_master')->result_array();
    }
    /*
     * function to add new faq_master
     */
    function add_faq_master($params)
    {
        $this->db->insert('faq_master',$params);
        return $this->db->insert_id();
    }    
    /*
     * function to update faq_master
     */
    function update_faq_master($id,$params)
    {
        $this->db->where('id',$id);
        return $this->db->update('faq_master',$params);
    } 
    /*
     * function to delete faq_master
     */
    function delete_faq_master($id)
    {
        return $this->db->delete('faq_master',array('id'=>$id));
    }

    function getFaqParent($test_module_id){

        $this->db->select('`id`,`parent_subject`,`icon`,`short_desc`');
        $this->db->from('faq_master');
        $this->db->where(array('active' => 1,'test_module_id'=> $test_module_id));  
        $this->db->order_by('parent_subject', 'ASC');
        return $this->db->get()->result_array();
    }

    function getFaqChild($pid){

        $this->db->select('`question` , `answer`  ');
        $this->db->from('faq');
        $this->db->where(array('active' => 1,'pid'=>$pid));  
        $this->db->order_by('display_order', 'ASC');
        return $this->db->get()->result_array();
    }
        
}
