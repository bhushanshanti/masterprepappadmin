<?php
 
class Paper_model extends CI_Model
{

function __construct()
{
    parent::__construct();
}

function get_basic_details($test_seriese_id, $ts_cat_assoc_id){

    $this->db->select("
        ts.test_seriese_name, 
        ts.test_seriese_type, 
        cat.category_name,
        pgm.programe_name, 
        ts_cat.ts_cat_assoc_id, 
        ts_cat.paper_duration, 
        ts_cat.max_marks,
        ts_cat.audio_file,
        ts_cat.audio_time,
        ts_cat.totalquest
    ");
    $this->db->from('test_seriese ts');
    $this->db->join('ts_cat_assoc ts_cat', 'ts_cat.test_seriese_id = ts.test_seriese_id');
    $this->db->join('category_masters cat', 'cat.category_id = ts_cat.category_id');
    $this->db->join('programe_masters pgm', 'pgm.programe_id = cat.programe_id');
    $this->db->where(array('ts_cat.ts_cat_assoc_id' => $ts_cat_assoc_id, 'ts_cat.test_seriese_id' => $test_seriese_id));        
    return $this->db->get()->result_array();
    //print_r($this->db->last_query());exit;
}

function get_basic_details2($test_seriese_id, $ts_cat_assoc_id){

    $this->db->select("
            ts.test_seriese_id,
            ts.test_seriese_name, 
            ts.test_seriese_type,
            ts.is_MtEt, 
            cat.category_id,
            cat.category_name, 
            pgm.programe_name, 
            ts_cat.ts_cat_assoc_id, 
            ts_cat.paper_duration, 
            ts_cat.max_marks,
            ts_cat.audio_file,
            ts_cat.audio_time,
            ts_cat.totalquest
    ");
    $this->db->from('test_seriese ts');
    $this->db->join('ts_cat_assoc ts_cat', 'ts_cat.test_seriese_id = ts.test_seriese_id');
    $this->db->join('category_masters cat', 'cat.category_id = ts_cat.category_id');
    $this->db->join('programe_masters pgm', 'pgm.programe_id = cat.programe_id');
    $this->db->where(array('ts_cat.ts_cat_assoc_id' => $ts_cat_assoc_id, 'ts_cat.test_seriese_id' => $test_seriese_id));        
    return $this->db->get()->row_array();
    //print_r($this->db->last_query());exit;
}

function get_audio_details($test_seriese_id, $ts_cat_assoc_id){

    $this->db->select('
        audio_file,
        audio_time,
        totalquest
    ');
    $this->db->from('ts_cat_assoc');       
    $this->db->where(array('ts_cat_assoc_id' => $ts_cat_assoc_id, 'test_seriese_id'=> $test_seriese_id));        
    return $this->db->get()->result_array();
    //print_r($this->db->last_query());exit;
}

function get_section_details2($ts_cat_assoc_id){
        
    $this->db->select("
        sec.section_id, 
        sec.section_heading, 
        sec.section_desc, 
        sec.section_para, 
        sec.section_image
    ");
    $this->db->from('sections sec');        
    $this->db->where(array('sec.ts_cat_assoc_id' => $ts_cat_assoc_id));
    return $this->db->get()->result_array();
    //print_r($this->db->last_query());exit;
}

function get_question_sets_details2($section_id){
        
    $this->db->select('
        qs.question_sets_id,
        qs.question_sets_heading, 
        qs.question_sets_desc, 
        qs.question_sets_para, 
        qs.question_sets_image, 
        qs.section_id
    ');
    $this->db->from('question_sets qs');        
    $this->db->where(array('qs.section_id' => $section_id));
    return $this->db->get()->result_array();
    //print_r($this->db->last_query());exit;
}

function get_section_details($ts_cat_assoc_id){
        
    $this->db->select("
        sec.section_id, 
        sec.section_heading as Section_name, 
        sec.section_desc, 
        sec.section_para, 
        sec.section_image
    ");
    $this->db->from('sections sec');        
    $this->db->where(array('sec.ts_cat_assoc_id' => $ts_cat_assoc_id));
    return $this->db->get()->result_array();
    //print_r($this->db->last_query());exit;
}    

function get_question_sets_details($section_id){
        
    $this->db->select("
        qs.question_sets_id, 
        qs.total_questions as Question_count, 
        qs.`display_no_of_questions`,
        qs.question_sets_heading, 
        qt.type_name as Question_type,
        qs.question_sets_desc as Question_descp, 
        qs.question_sets_para as Question_passage, 
        qs.question_sets_image AS `Question_image`,
        qs.`answer_audio_recrod_duration`,
        qb.`behavior_name`" 
    );
    $this->db->join('question_types qt', 'qs.`question_type_id`= qt.`question_type_id`'); 
    $this->db->join('question_behavior qb', 'qb.`question_behavior_id`= qt.`question_behavior_id`', 'left');
    $this->db->from('`question_sets` qs');        
    $this->db->where(array('qs.section_id' => $section_id));
    return $this->db->get()->result_array();
    //print_r($this->db->last_query());exit;
}    

function get_questions_details($question_set_id){
        
    $this->db->select('
        question_no, 
        question_id, 
        question
    ');
    $this->db->from('questions');        
    $this->db->where(array('question_sets_id' => $question_set_id));
    $this->db->order_by('question_no', 'ASC');
    return $this->db->get()->result_array();        
    //print_r($this->db->last_query());exit;        
} 

function get_questions_details_opt($question_id){

    $this->db->select('option_key,option_value');
    $this->db->from('question_options');        
    $this->db->where(array('question_id' => $question_id));   
    return $this->db->get()->result_array();
}  
   
}
