<?php

 
class Tips_tricks_master_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    /*
     * Get notification_subject by id
     */
    function get_tips_tricks_master($id)
    {
        return $this->db->get_where('tips_tricks_master',array('id'=>$id))->row_array();
    }
    /*
     * Get all tips_tricks_master count
     */
    function get_all_tips_tricks_master_count()
    {
        $this->db->from('tips_tricks_master');
        return $this->db->count_all_results();
    }  
    /*
     * Get all tips_tricks_master
     */
    function get_all_tips_tricks_master($params = array())
    {        
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        $this->db->select('
            ttm.`id`,
            ttm.`parent_subject`,
            ttm.`icon`,
            ttm.`short_desc`,
            ttm.`active`,
            tm.`test_module_name`
        ');
        $this->db->from('`tips_tricks_master` ttm');
        $this->db->join('`test_module` tm', 'tm.`test_module_id`= ttm.`test_module_id`');
        $this->db->order_by('ttm.`parent_subject`','ASC');
        return $this->db->get()->result_array();
    }

    function get_all_tips_tricks_master_active($params = array())
    {        
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        $this->db->where('active', '1');
        $this->db->order_by('parent_subject', 'ASC');
        return $this->db->get('tips_tricks_master')->result_array();
    }
    /*
     * function to add new tips_tricks_master
     */
    function add_tips_tricks_master($params)
    {
        $this->db->insert('tips_tricks_master',$params);
        return $this->db->insert_id();
    }    
    /*
     * function to update tips_tricks_master
     */
    function update_tips_tricks_master($id,$params)
    {
        $this->db->where('id',$id);
        return $this->db->update('tips_tricks_master',$params);
    } 
    /*
     * function to delete tips_tricks_master
     */
    function delete_tips_tricks_master($id)
    {
        return $this->db->delete('tips_tricks_master',array('id'=>$id));
    }

    function getTipsTricksParent($test_module_id){

        $this->db->select('`id`,`parent_subject`,`icon`,`short_desc`');
        $this->db->from('tips_tricks_master');
        $this->db->where(array('active' => 1,'test_module_id'=> $test_module_id));  
        $this->db->order_by('parent_subject', 'ASC');
        return $this->db->get()->result_array();
    }

    function getTipsTricksChild($pid){

        $this->db->select('`question` as tip,
        ');
        $this->db->from('tips_and_tricks');
        $this->db->where(array('active' => 1,'pid'=>$pid));  
        $this->db->order_by('display_order', 'ASC');
        return $this->db->get()->result_array();
    }
        
}
