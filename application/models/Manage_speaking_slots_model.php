<?php
 
class Manage_speaking_slots_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function get_available_teacher_same_timezone($time_zone , $date){
		$speaking_time = DFT_SPEAKING_BOOKINGS;
		$after2days    = date('Y-m-d',strtotime(" + $speaking_time", strtotime($date)));
    	$this->db->select('manage_availability.`id` as avail_id , manage_availability.`user_id`, manage_availability.`start_time` , manage_availability.`end_time`, manage_availability.`date`');
        $this->db->from('manage_availability');
        $this->db->join('`user`', 'user.`id` = manage_availability.`user_id`');
        $this->db->join('`user_role` usr', 'usr.`user_id` = user.`id`');        
        $this->db->where(array('user.active'=>1,'usr.role_id'=>12,'manage_availability.booking_status'=>0,'user.time_zone'=>$time_zone ));
        $this->db->where('manage_availability.date >=' ,  $date);
        $this->db->where('manage_availability.date <=' ,  $after2days);
        $this->db->order_by('manage_availability.`date`', 'asc');
        return $this->db->get()->result_array();
    }

	function get_available_teacher_from_def_timezone($time_zone , $date){
		$speaking_time      = DFT_SPEAKING_BOOKINGS;
		$examiner_time_zone = DFT_SPEAKING_EXAMINER_TIMEZONEID;
		$after2days = date('Y-m-d',strtotime(" + $speaking_time", strtotime($date)));
    	$this->db->select('manage_availability.`id` as avail_id , manage_availability.`id` , manage_availability.`user_id`, manage_availability.`start_time` , manage_availability.`end_time`, manage_availability.`date`');
        $this->db->from('manage_availability');
        $this->db->join('`user`', 'user.`id` = manage_availability.`user_id`');
        $this->db->join('`user_role` usr', 'usr.`user_id` = user.`id`');        
        $this->db->where(array('user.active'=>1,'usr.role_id'=>12,'manage_availability.booking_status'=>0,'user.time_zone'=>$examiner_time_zone ));
        $this->db->where('manage_availability.date >=' ,  $date);
        $this->db->where('manage_availability.date <=' ,  $after2days);
        $this->db->order_by('manage_availability.`date`', 'asc');
        return $this->db->get()->result_array();
    }    
       
    function get_examiner_speaking_booking_count(){		
    	$this->db->select(' manage_availability.`user_id` , SUM(manage_availability.booking_status) as cnt ');
        $this->db->from('manage_availability');
        $this->db->join('`user`', 'user.`id` = manage_availability.`user_id`');
        $this->db->join('`user_role` usr', 'usr.`user_id` = user.`id`');        
        $this->db->where(array('user.active'=>1,'usr.role_id'=>12 ));
        $this->db->group_by('manage_availability.`user_id`'); 
		$this->db->order_by('cnt', 'asc');

        return $this->db->get()->result_array();
	}
    
    function check_availble_slot($avail_id){
    	$this->db->select('manage_availability.`user_id` , user.`email` , user.`fname` ');
        $this->db->from('manage_availability');
         $this->db->join('`user`', 'user.`id` = manage_availability.`user_id`');
        $this->db->where(array( 'manage_availability.booking_status' =>  0 , 'manage_availability.id' =>  $avail_id ));
        return $this->db->get()->row_array();
    }

    function book_speaking_slot($avail_id , $student_id , $examiner_id , $json){
    	/* update for examiner */ 
    	$params = array();
    	$params['booking_status'] = 1;
    	$params['booked_by'] = $student_id;
    	$params['booking_timestamp'] = time();
    	
    	$this->db->where(array('id' => $avail_id , 'booking_status' => 0 ));
       	$this->db->update('manage_availability',$params);

		/* Book mock test */
		$unique_test_id = "stu_".$student_id."_".time();
		$insert_data = array("stud_id" => $student_id , "examiner_id" => $examiner_id ,"availability_id" => $avail_id ,"reminder_sent" => 0 ,"stu_booking_info" => $json ,"unique_test_id" => $unique_test_id ,"active" => 1,"attempt" => 1);

		$this->db->insert('mock_speaking_slots',$insert_data);
        return $unique_test_id;
    }
    
    function check_mock_eligibility($stud_id){
		$eligible = array("status" => "success" , "message" => "Eligible for test" , "other_info" => "");
		
		/* check if speaking slot already booked starts  */
		
		$this->db->select('mss.`book_speaking_slots_id`,mss.`unique_test_id`,ma.`start_time`,ma.`date`,ma.`booking_timestamp`,zones_info.`zoneName`');
		$this->db->from('`mock_speaking_slots` mss');
		$this->db->join('`manage_availability` ma', 'ma.`id` = mss.`availability_id`');
		$this->db->join('students' , 'mss.`stud_id` = students.`id`');
		$this->db->join('zones_info' , 'students.`time_zone` = zones_info.`id`');
		$this->db->where(array('mss.active'=> 1 ,'mss.stud_id'=>$stud_id , 'ma.booking_status' => 1 ));
		$this->db->order_by('mss.book_speaking_slots_id',"desc")->limit(1);		
		$old_bookings = $this->db->get('')->row_array();	
		
		/* Get last booking */		
		
		if(!empty($old_bookings)){
			$date                   = $old_bookings['date'];
			$start_time             = $old_bookings['start_time'];
			$booking_timestamp      = $old_bookings['booking_timestamp'];
			$book_speaking_slots_id = $old_bookings['book_speaking_slots_id'];
			$zoneName               = $old_bookings['zoneName'];
			
			/* Exam time stamp after 15 minutes */
			$exam_timestamp = $date." ".$start_time;
			$waiting_time = DFT_WAIT_TIME;
			$exam_timestampafter_10 = strtotime("+ $waiting_time", strtotime($exam_timestamp));			
			$date = new DateTime("now", new DateTimeZone('Asia/Calcutta') );
			$local_time =  strtotime($date->format('Y-m-d H:i:s'));
			
			if($local_time > $exam_timestampafter_10){
				/* update old booking as inactive*/
				
				$params = array();
				$params['active'] = 0;
				$params['attempt'] = 0;
				$this->db->where(array('book_speaking_slots_id' => $book_speaking_slots_id , 'active' => 1 ));
				if($this->db->update('mock_speaking_slots',$params))				
					$eligible = array("status" => "success" , "message" => "Ready to book new." , "other_info" => "");
				else
					$eligible = array("status" => "fails" , "message" => "You have already booking opened." , "other_info" => "$book_speaking_slots_id");
			}
			else{
				$eligible = array("status" => "fails" , "message" => "You have already booking opened." , "other_info" => "$book_speaking_slots_id");
			}
			
		}
			
		
		/* check if speaking slot already booked ends  */
		
		
		if($eligible['status'] == "success"){
			/* Active package info */
			$this->db->select('sp.`package_id`, pm.`no_of_mocktest`, pm.`no_of_practest` ');
			$this->db->from('`students` s');        
			$this->db->join('`student_package` sp', 's.`id` = sp.`student_id`'); 
			$this->db->join('`package_masters` pm', 'sp.`package_id` = pm.`package_id`');		
			$this->db->where(array('s.id'=>$stud_id , 'sp.active' => 1 ));		
			$active_pack_info = $this->db->get('')->row_array();	
		
			/* Attempt tests info */
			if(!empty($active_pack_info)){
				
				$package_id     = $active_pack_info['package_id'];
				$no_of_mocktest = $active_pack_info['no_of_mocktest'];			
				$no_of_practest = $active_pack_info['no_of_practest'];		
				/* practice test info  */			
				$this->db->select('student_result_id,mockId');
				$this->db->from('`student_results`');			
				$this->db->where(array('student_results.student_id'=>$stud_id ));		
				$prac_test_info = $this->db->get('')->result_array();	
				
				$count_mock = 0;
				$count_prac = 0;
				if(!empty($prac_test_info)){
					foreach($prac_test_info as $key => $test){
						if($test['mockId'] == 2)
							$count_mock++;
						else
							$count_prac++;
					}
					
					if($no_of_mocktest <= $count_mock)
						$eligible = array("status" => "fails" , "message" => "Mock test limit in active package finished.Please try new package" , "other_info" => "");
						
					if($count_prac < $no_of_practest)
						$eligible = array("status" => "fails" , "message" => "You must have $no_of_practest practice test to start mock test" , "other_info" => "");
					
					
				}
				else{
					if($no_of_practest > 0)
						$eligible = array("status" => "fails" , "message" => "You must have $no_of_practest practice test to start mock test" , "other_info" => "");
				}
				/* Mock test info  */
				
			}else{
				$eligible = array("status" => "fails" , "message" => "No active pakage found" , "other_info" => "");
			}
		}else{
			$eligible = [ "success" => "fails", "message" => $eligible['message'],"data" => array() ];
		}
		return $eligible;
	}
		
	function insert_examiner_notification($user_id , $notification_message, $other_info){
		$params = array();
		$params['user_id'] = $user_id;
		$params['notification_message'] = $notification_message;
		$params['other_info'] = $other_info;
		$params['status'] = 0;
		$this->db->insert('admin_notifications',$params);
        return $this->db->insert_id();
	}

	function get_slot_information($avail_id){
		$this->db->select('mock_speaking_slots.`availability_id` , mock_speaking_slots.`unique_test_id` , mock_speaking_slots.`meeting_id` , mock_speaking_slots.`meeting_url` , manage_availability.`date`, manage_availability.`start_time`, manage_availability.`end_time`, manage_availability.`booking_timestamp`');
        $this->db->from('mock_speaking_slots');
         $this->db->join('`manage_availability`', 'manage_availability.`id` = mock_speaking_slots.`availability_id`');
        $this->db->where(array( 'mock_speaking_slots.availability_id' =>  $avail_id ));
        return $this->db->get()->row_array();
	}
}

?>
