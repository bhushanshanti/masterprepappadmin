<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/

class Center_location_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get country by package_id
     */
    function get_center_location($center_id)
    {
        return $this->db->get_where('center_location',array('center_id'=>$center_id))->row_array();
    }
    
    /*
     * Get all country count
     */
    function get_all_center_location_count()
    {
        $this->db->from('center_location');
        return $this->db->count_all_results();
    }
        
    /*
     * Get all country
     */
    function get_all_center_location($params = array())
    {
        $this->db->order_by('center_id', 'desc');
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        $this->db->select('cl.center_id,cl.center_name,cl.contact,cl.email,cl.zip_code,cl.address_line_1,cl.latitude,cl.longitude,cl.active,cnt.name,st.state_name,ct.city_name');
        $this->db->from('center_location cl');
        $this->db->join('country cnt', 'cnt.country_id = cl.country_id');
        $this->db->join('state st', 'st.state_id = cl.state_id');
        $this->db->join('city ct', 'ct.city_id = cl.city_id');
        return $this->db->get('')->result_array();
    }

    /*
     * Get all branches
     */
    function get_all_branch_location($country_id)
    {  
        $this->db->select('cl.center_name,cl.contact,cl.email,cl.zip_code,cl.address_line_1,cl.latitude,cl.longitude,cnt.name,st.state_name,ct.city_name');
        $this->db->from('center_location cl');
        $this->db->join('country cnt', 'cnt.country_id = cl.country_id');
        $this->db->join('state st', 'st.state_id = cl.state_id');
        $this->db->join('city ct', 'ct.city_id = cl.city_id');
        $this->db->where(array('cl.active'=>1 ,'cl.country_id'=>$country_id));
        $this->db->order_by('ct.city_name ASC','st.state_name ASC');
        return $this->db->get('')->result_array();
    }

    /*
     * Get all branches
     */
    function get_all_branch()
    {  
        $this->db->select('cl.center_id,cl.center_name,ct.city_name');
        $this->db->from('center_location cl');
        $this->db->join('city ct', 'ct.city_id = cl.city_id');
        $this->db->where('cl.active',1);
        $this->db->order_by('ct.city_name', 'ASC');
        return $this->db->get('')->result_array();
    }
        
    /*
     * function to add new country
     */
    function add_center_location($params)
    {
        $this->db->insert('center_location',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update country
     */
    function update_center_location($center_id,$params)
    {
        $this->db->where('center_id',$center_id);
        return $this->db->update('center_location',$params);
    }
    
    /*
     * function to delete country
     */
    function delete_center_location($center_id)
    {
        return $this->db->delete('center_location',array('center_id'=>$center_id));
    }

    /*
     * Get all country
     */
    function getAllCountry()
    {  
        $this->db->select('country_id,iso3,name,phonecode,flag,phoneNo_limit');
        $this->db->from('country');
        $this->db->where('active',1);
        $this->db->order_by('name', 'ASC');
        return $this->db->get('')->result_array();
    }

    /*
     * Get all state
     */
    function getAllState($country_id)
    {  
        $this->db->select('state_id,state_name');
        $this->db->from('state');
        $this->db->where(array('country_id'=>$country_id, 'active'=>1,));
        $this->db->order_by('state_name', 'ASC');
        return $this->db->get('')->result_array();
    }

    /*
     * Get all city
     */
    function getAllCity($state_id)
    {  
        $this->db->select('city_id,city_name');
        $this->db->from('city');
        $this->db->where(array('state_id'=>$state_id, 'active'=>1,));
        $this->db->order_by('city_name', 'ASC');
        return $this->db->get('')->result_array();
    }

    function getAllForeignCountry()
    {  
        $this->db->select('country_id,iso3,name,phonecode,flag,phoneNo_limit');
        $this->db->from('country');
        $this->db->where(array('active'=>1,'name!='=>'India'));
        $this->db->order_by('name', 'ASC');
        return $this->db->get('')->result_array();
    }
}
