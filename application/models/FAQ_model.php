<?php

 
class FAQ_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get faqs by faq_id
     */
    function get_faqs($faq_id)
    {
        return $this->db->get_where('faq',array('faq_id'=>$faq_id))->row_array();
    }
    
    /*
     * Get all faqs count
     */
    function get_all_faqs_count()
    {
        $this->db->from('faq');
        return $this->db->count_all_results();
    }

    /*
     * Get all faqs
     */
    function get_all_faqs($params = array(),$rid)
    {        
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        $this->db->select('
            tt.`faq_id`,
            tt.`question`, 
            tt.`answer`,
            tt.`display_order`,            
            tt.`active`,
            tm.`test_module_name`,
            ttm.`parent_subject`,
        ');
        $this->db->from('`faq` tt'); 
        $this->db->join('`tips_tricks_master` ttm', 'ttm.`id`= tt.`pid`');
        $this->db->join('`test_module` tm', 'tm.`test_module_id`= ttm.`test_module_id`');
        if($rid!=''){
            $this->db->where(array('pid'=>$rid));
        } else{
            $this->db->where(array());
        }               
               
        $this->db->order_by('ttm.`parent_subject` ASC');
        return $this->db->get('')->result_array();
    }    
        
    /*
     * function to add new faqs
     */
    function add_faqs($params)
    {
        $this->db->insert('faq',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update faqs
     */
    function update_faqs($faq_id,$params)
    {
        $this->db->where('faq_id',$faq_id);
        return $this->db->update('faq',$params);
    }
    
    /*
     * function to delete faqs
     */
    function delete_faqs($faq_id)
    {
        return $this->db->delete('faq',array('faq_id'=>$faq_id));
    }    
}
