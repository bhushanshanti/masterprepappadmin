<?php

 
class Contents_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get contents by id
     */
    function get_contents($id)
    {
    return $this->db->get_where('contents',array('id'=>$id))->row_array();
    }
    
    /*
     * Get all contents count 
     */
    function get_all_contents_count()
    {
        $this->db->from('contents');        
        return $this->db->count_all_results();
    }    
        
    /*
     * Get all contents 
     */
    function get_all_contents($params = array())
    {        
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        $this->db->select('*');
        $this->db->from('`contents`');        
        $this->db->order_by('id', 'desc');
        return $this->db->get('')->result_array();
    }
        
    /*
     * function to add new contents
     */
    function add_contents($params)
    {
        $this->db->insert('contents',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update contents
     */
    function update_contents($id,$params)
    {
        $this->db->where('id',$id);
        return $this->db->update('contents',$params);
    }
    
    /*
     * function to delete contents
     */
    function delete_contents($id)
    {
        return $this->db->delete('contents',array('id'=>$id));
    }
}
