<?php 
class Performance_model extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    
    function get_performance_report($studid , $select_type){
		$this->db->select_avg('band_score');		
		$this->db->from('student_results');
		$this->db->where(array('student_id'=>$studid,'type'=> $select_type,'paper_checked'=> 1));        
        return $this->db->get('')->row()->band_score;
	}
    
    function get_student_perf_modulewise($studid , $select_type){
		$this->db->select_avg('band_score');		
		$this->db->from('student_results');
		$this->db->where(array('student_id'=>$studid));
		$this->db->like('collection_no', $select_type);
		return $this->db->get('')->row()->band_score;
	}
	
	function get_performance_report_by_tsid($studid , $select_type , $tsid){
		$this->db->select_avg('band_score');		
		$this->db->from('student_results');
		$this->db->where(array('student_id'=>$studid,'type'=> $select_type,'test_seriese_id'=> $tsid,'paper_checked'=> 1));        
        return $this->db->get('')->row()->band_score;
	}
    
    function get_student_given_tests($stud_id,$select_type){
		//$this->db->select_avg('`str`.band_score');
		$this->db->select('`str`.test_seriese_id,`str`.created,`str`.collection_no,`str`.band_score,`ts`.test_seriese_name,`std`.Goal_avg');
        $this->db->from('`student_results` str');
         $this->db->join('`test_seriese` ts', 'str.`test_seriese_id` = ts.`test_seriese_id`');
         $this->db->join('`students` std', 'str.`student_id` = std.`id`');
        $this->db->where(array('str.student_id'=>$stud_id,'str.type'=> $select_type,'str.paper_checked'=> 1));
        $this->db->order_by('str.student_result_id', 'desc');       
        $test_given = $this->db->get('')->result_array();
        $final_tests = array();
       
		foreach((array)$test_given as $test){
			$test_seriese_id = $test['test_seriese_id'];
			$collection_no   = explode("-",$test['collection_no']);
			
			if(!isset($final_tests["test_id_$test_seriese_id"]))
				$final_tests["test_id_$test_seriese_id"] = array();
				
			$final_tests["test_id_$test_seriese_id"]['Title']        = $test['test_seriese_name'];
			$final_tests["test_id_$test_seriese_id"]['Goal_avg']        = $test['Goal_avg'];
			$final_tests["test_id_$test_seriese_id"]['count_attempts']      = 0;
			$final_tests["test_id_$test_seriese_id"]['Last_attempt'] = $test['created'];
			$final_tests["test_id_$test_seriese_id"]['test_seriese_id'] = $test_seriese_id;
			
			if(isset($collection_no[2])){
				$final_tests["test_id_$test_seriese_id"][$collection_no[2]][] = $test['band_score'];
			}
		}
		
		
		
		//t($final_tests , 1);
		return $final_tests;
	}
	
	
    function get_student_given_tests_by_tsid($stud_id,$testId){
		
		$this->db->select('`str`.test_seriese_id,`str`.created,`str`.collection_no,`str`.band_score,`ts`.test_seriese_name,`std`.Goal_avg');
        $this->db->from('`student_results` str');
        $this->db->join('`test_seriese` ts', 'str.`test_seriese_id` = ts.`test_seriese_id`');
        $this->db->join('`students` std', 'str.`student_id` = std.`id`');  
        $this->db->where(array('str.student_id'=>$stud_id,'str.test_seriese_id'=> $testId,'str.paper_checked'=> 1));
        $this->db->order_by('str.student_result_id', 'desc');       
        $test_given = $this->db->get('')->result_array();
        $final_tests = array();
       
		foreach((array)$test_given as $test){
			$test_seriese_id = $test['test_seriese_id'];
			$collection_no   = explode("-",$test['collection_no']);
			
			if(!isset($final_tests["test_id_$test_seriese_id"]))
				$final_tests["test_id_$test_seriese_id"] = array();
				
			$final_tests["test_id_$test_seriese_id"]['Title']          = $test['test_seriese_name'];
			$final_tests["test_id_$test_seriese_id"]['count_attempts'] = 0;
			$final_tests["test_id_$test_seriese_id"]['Goal_avg']       = $test['Goal_avg'];
			$final_tests["test_id_$test_seriese_id"]['Last_attempt']   = $test['created'];
			$final_tests["test_id_$test_seriese_id"]['test_seriese_id']= $test_seriese_id;
			
			if(isset($collection_no[2])){
				$final_tests["test_id_$test_seriese_id"][$collection_no[2]][] = $test['band_score'];
			}
		}		
		return $final_tests;
	}
	
	function get_student_given_tests_by_tsid_detail($stud_id,$testId){
		
		$this->db->select('`str`.test_seriese_id,`str`.student_result_id,`str`.created,`str`.collection_no,`str`.band_score,`str`.marks_secured,`str`.totalQuestion,`ts`.test_seriese_name,`std`.Goal_avg');
        $this->db->from('`student_results` str');
        $this->db->join('`test_seriese` ts', 'str.`test_seriese_id` = ts.`test_seriese_id`');
        $this->db->join('`students` std', 'str.`student_id` = std.`id`');  
        $this->db->where(array('str.student_id'=>$stud_id,'str.test_seriese_id'=> $testId,'str.paper_checked'=> 1));
        $this->db->order_by('str.student_result_id', 'desc');       
        $test_given = $this->db->get('')->result_array();
        $final_tests = array();
       
		foreach((array)$test_given as $test){
			$inner_array = array();
			$test_seriese_id = $test['test_seriese_id'];
			$collection_no   = explode("-",$test['collection_no']);
			
			if(!isset($final_tests["test_id_$test_seriese_id"]))
				$final_tests["test_id_$test_seriese_id"] = array();				
				
			$final_tests["test_id_$test_seriese_id"]['Title']          = $test['test_seriese_name'];
			$final_tests["test_id_$test_seriese_id"]['count_attempts'] = 0;
			$final_tests["test_id_$test_seriese_id"]['Goal_avg']       = $test['Goal_avg'];
			$final_tests["test_id_$test_seriese_id"]['Last_attempt']   = $test['created'];
			$final_tests["test_id_$test_seriese_id"]['test_seriese_id']= $test_seriese_id;
			
			if(isset($collection_no[2])){
				$final_tests["test_id_$test_seriese_id"][$collection_no[2]][] = array("student_result_id" => $test['student_result_id'] ,"band_score" => $test['band_score'] ,"marks_secured" => $test['marks_secured'] ,"totalQuestion" => $test['totalQuestion']);
			}
		}
		
				
		return $final_tests;
	}
	
	
	function get_student_resultbyId($goal_column , $testId){
		//$this->db->select('`str`.test_seriese_id,`str`.student_result_id,`str`.created,`str`.collection_no,`str`.band_score,`str`.marks_secured as correctAns,"`str`.attempted" - "`str`.marks_secured" as incorrect,`str`.attempted,`str`.totalQuestion,`ts`.test_seriese_name,`std`.'.$goal_column.'');
		/* $this->db->select("`str`.`test_seriese_id`, `str`.`student_result_id`, `str`.`created`, `str`.`collection_no`, `str`.`band_score`, `str`.`marks_secured` as `correctAns`, `str`.attempted - `str`.marks_secured as `incorrect`, `str`.`attempted`, `str`.`totalQuestion`, `ts`.`test_seriese_name`, `std`.`Goal_l`");
        $this->db->from('`student_results` str');
        $this->db->join('`test_seriese` ts', 'str.`test_seriese_id` = ts.`test_seriese_id`');
        $this->db->join('`students` std', 'str.`student_id` = std.`id`');  
        $this->db->where(array('str.student_result_id'=> $testId,'str.paper_checked'=> 1));
        $this->db->order_by('str.student_result_id', 'desc');       
        $test_given = $this->db->get('')->result_array();		
		return $test_given; */
		
		$query = $this->db->query("SELECT `str`.`test_seriese_id`, `str`.`student_result_id`, `str`.`created`, `str`.`collection_no`, `str`.`band_score`, `str`.`marks_secured` as `correct`, `str`.attempted - `str`.marks_secured as `incorrect`, `str`.`attempted`, `str`.`totalQuestion`, `ts`.`test_seriese_name`, `std`.`$goal_column` FROM `student_results` `str` JOIN `test_seriese` `ts` ON str.`test_seriese_id` = ts.`test_seriese_id` JOIN `students` `std` ON str.`student_id` = std.`id` WHERE `str`.`student_result_id` = '$testId' AND `str`.`paper_checked` = 1 ORDER BY `str`.`student_result_id` DESC");
		
		$test_given = $query->result_array();
		return $test_given;

	}
    
}
