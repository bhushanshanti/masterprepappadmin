<?php
 
class Advertisement_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get role by id
     */
    function get_advertisements($id)
    {
        return $this->db->get_where('advertisements',array('id'=>$id))->row_array();
    }
    
    /*
     * Get all roles count
     */
    function get_all_advertisements_count()
    {
        $this->db->from('advertisements');
        return $this->db->count_all_results();
    }

    function get_all_advertisements_active()
    {
        $this->db->order_by('id', 'desc');  
        $this->db->where('active', 1);           
        return $this->db->get('advertisements')->result_array();
    }
        
    /*
     * Get all roles
     */
    function get_all_advertisements($params = array())
    {
        $this->db->order_by('id', 'desc');
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        return $this->db->get('advertisements')->result_array();
    }
        
    /*
     * function to add new role
     */
    function add_advertisements($params)
    {
        $this->db->insert('advertisements',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update role
     */
    function update_advertisements($id,$params)
    {
        $this->db->where('id',$id);
        return $this->db->update('advertisements',$params);
    }
    
    /*
     * function to delete role
     */
    function delete_advertisements($id)
    {
        return $this->db->delete('advertisements',array('id'=>$id));
    }

     
}
