<?php

 
class User_activity_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }        
    /*
     * function to add new band_score
     */
    function add_user_activity($params)
    {
        $this->db->insert('user_activity',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update band_score
     */
    function update_band_score($activity_id,$params)
    {
        $this->db->where('activity_id',$activity_id);
        return $this->db->update('user_activity',$params);
    }
    
    /*
     * function to delete band_score
     */
    function delete_band_score($activity_id)
    {
        return $this->db->delete('user_activity',array('activity_id'=>$activity_id));
    }
}
