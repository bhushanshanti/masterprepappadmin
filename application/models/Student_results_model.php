<?php

class Student_results_model extends CI_Model
{
    
    function __construct()
    {
        parent::__construct();
    }    

    function update_result_marks($collection_no,$marks_secured,$band_score,$percentage,$by_user,$test_seriese_id,$ts_cat_assoc_id,$category_name,$pro_id)
    {
        $this->db->query("update `student_results` set `marks_secured` = '".$marks_secured."', `band_score` = '".$band_score."', `percentage` = '".$percentage."', `by_user` = '".$by_user."', `pro_id` = '".$pro_id."',`paper_checked` = 1 where collection_no ='".$collection_no."' ");
        //print_r($this->db->last_query());exit;

        if($category_name==READING or $category_name==LISTENING or $category_name==EX1 or $category_name==EX2 or $category_name==EX3 or $category_name==EX4 or $category_name==EX5){

            //update rank
            $query  =   "SET @currentRank := 0, @lastRating := null, @rowNumber := 1";
            $this->db->query($query);
            $sql = "update 
            `student_results` r
            inner join (
                select
                    `student_result_id`,
                    @currentRank := if(@lastRating = `marks_secured`, @currentRank, @rowNumber) `rank`,
                    @rowNumber := @rowNumber + if(@lastRating = `marks_secured`, 0, 1) `rowNumber`,
                    @lastRating := `marks_secured`
                from `student_results`
                where `test_seriese_id` = ".$test_seriese_id." and `ts_cat_assoc_id` = ".$ts_cat_assoc_id."
                order by `marks_secured` desc
            )var on
                var.`student_result_id` = r.`student_result_id`
            set r.`rank` = var.`rank`";
            return $this->db->query($sql);

        }elseif($category_name==WRITING or $category_name==SPEAKING){

             //update rank
            $query  =   "SET @currentRank := 0, @lastRating := null, @rowNumber := 1";
            $this->db->query($query);
            $sql = "update 
            `student_results` r
            inner join (
                select
                    `student_result_id`,
                    @currentRank := if(@lastRating = `band_score`, @currentRank, @rowNumber) `rank`,
                    @rowNumber := @rowNumber + if(@lastRating = `band_score`, 0, 1) `rowNumber`,
                    @lastRating := `band_score`
                from `student_results`
                where `test_seriese_id` = ".$test_seriese_id." and `ts_cat_assoc_id` = ".$ts_cat_assoc_id."
                order by `band_score` desc
            )var on
                var.`student_result_id` = r.`student_result_id`
            set r.`rank` = var.`rank`";
            return $this->db->query($sql);

        }else{
            
        }
    } 

    function checked_paper_count()
    {        

        $this->db->from('student_results');
        $this->db->where(array('paper_checked'=>1,'programe_id'=>ACD_ID));
        return $this->db->count_all_results();
    }

    function checked_paper_count_gt()
    {        

        $this->db->from('student_results');
        $this->db->where(array('paper_checked'=>1,'programe_id'=>GT_ID));
        return $this->db->count_all_results();
    } 

    function unchecked_paper_count()
    {    

        $this->db->from('student_results');
        $this->db->where(array('paper_checked'=>NULL,'programe_id'=>ACD_ID));
        return $this->db->count_all_results();
    }

    function unchecked_paper_count_gt()
    {    

        $this->db->from('student_results');
        $this->db->where(array('paper_checked'=>NULL,'programe_id'=>GT_ID));
        return $this->db->count_all_results();
    } 

    function assigned_paper_count($by_user)
    {
        $this->db->from('student_results');
        $this->db->where(array('assigned_to'=> $by_user));
        return $this->db->count_all_results();
    } 

    function get_distinct_given_test($id,$test_module_id,$programe_id){

        $this->db->distinct('');
        $this->db->select('test_seriese_id');
        $this->db->from('student_results');
        $this->db->where(array('student_id'=>$id,'test_module_id'=>$test_module_id,'programe_id'=>$programe_id));
        $this->db->not_like('collection_no', 'ET');
        $this->db->not_like('collection_no', 'SPI');
        $this->db->order_by('created', 'DESC');
        return $this->db->get('')->row_array();
        //print_r($this->db->last_query());exit;
    }

    function get_count_distinct_given_tscat($id,$test_seriese_id,$test_module_id){
       
        $this->db->distinct('');
        $this->db->select('ts_cat_assoc_id');
        $this->db->from('student_results');
        $this->db->where(array('student_id' => $id,'test_seriese_id' => $test_seriese_id,'test_module_id' => $test_module_id));
        $this->db->not_like('collection_no', 'ET');
        $this->db->not_like('collection_no', 'MOCK');
        return $this->db->get('')->result_array();
    }

    function get_incompleted_tests($arr,$test_seriese_id){
        
        $this->db->select('
            ts_cat.ts_cat_assoc_id,
            ts_cat.test_seriese_id,
            ts_cat.category_id,
            cat.category_name,
            ts.test_seriese_name,
            ts.test_seriese_type,'
        );
        $this->db->from('ts_cat_assoc ts_cat');
        $this->db->join('category_masters cat', 'cat.category_id = ts_cat.category_id');
        $this->db->join('test_seriese ts', 'ts.test_seriese_id = ts_cat.test_seriese_id');
        $this->db->where(array('ts.test_seriese_id'=>$test_seriese_id));
        $this->db->where_not_in('ts_cat.ts_cat_assoc_id', $arr);
        return $this->db->get('')->result_array();
    }

    function getRank($programe_id,$test_module_id,$id){

        $rank1 = base_url('uploads/gallery/Rank1.jpg');        
        $one = 1;        
        $this->db->select('
            std.profile_pic,
            std.fname,
            FORMAT(AVG(sr.`rank`), 0) as `rankx`,
            IF(std.`id` = '.$id.', "A", "Z") as `pro`, 
            IF( AVG(sr.`rank`) = '.$one.', "'.$rank1.'", "") as `icon`,     
        ');
        $this->db->from('`student_results` sr');
        $this->db->join('`students` std', 'std.`id`= sr.`student_id`');
        $this->db->where(array('sr.`test_module_id`'=>$test_module_id, 'sr.`programe_id`'=>$programe_id));
        $this->db->group_by('sr.`student_id`');
        $this->db->order_by('`pro` ASC,`rankx` ASC');
        return $this->db->get('')->result_array();
    }

    function getGraphData($programe_id,$test_module_id,$id,$category_name){

        $this->db->select('
           `collection_no`,`band_score`,`created`     
        ');
        $this->db->from('`student_results`');
        $this->db->where(array('`test_module_id`'=>$test_module_id, '`programe_id`'=>$programe_id,'student_id'=>$id,'paper_checked'=>1));
        $this->db->like('collection_no', $category_name);
        $this->db->order_by('`created` ASC');
        return $this->db->get('')->result_array();
    }

    function get_f($a){
        
        $a = base_url(PROFILE_PIC_FILE_PATH).$a;
        $this->db->select('profile_pic');
        $this->db->from('students');
        $this->db->where(array('profile_pic'=> $a));
        return $this->db->get('')->row_array();
    }
    
    
    /* function getPendingtests(){
		$this->db->select('mss.`unique_test_id`');
		$this->db->from('`mock_speaking_slots` mss');
		//$this->db->join('`students` std', 'std.`id`= mss.`stud_id`' , 'left');
		//$this->db->join('`student_results` st_r', 'st_r.`un_mock_id` = mss.`unique_test_id`' , 'left');
       // $this->db->where('DATE(mss.`created`) + INTERVAL 72 HOUR < NOW()');
        $this->db->where('mss.`result_sent`' , 0);        
        $this->db->order_by('mss.`created` ASC');
		$test_unique_codes = $this->db->get('')->result_array();
		
		$return_data = array();
		foreach($test_unique_codes as  $unique_test){
			$unique_test_id = $unique_test['unique_test_id'];
			$this->db->select('*');
			$this->db->from('`student_results`');			
			$this->db->where('un_mock_id' , $unique_test_id);
			$inner_array = $this->db->get('')->result_array();
			
			if(!empty($inner_array))
				$return_data[$unique_test_id][] = $inner_array;
			
		}
		
		return $return_data;	
	} */ 
	
	
	function getPendingMocktests(){
				
		$this->db->select('mss.`book_speaking_slots_id`, mss.`stud_id`, mss.`unique_test_id` ,mss.`band_score` ,mss.`examiner_comment` , std.`mobile`, std.`email`, std.`fname`, std.`lname`');
		$this->db->from('`mock_speaking_slots` mss');
		$this->db->join('`students` std', 'std.`id`= mss.`stud_id`' , 'left');
		$this->db->where('mss.`result_sent`' , 0);        
        $this->db->order_by('mss.`created` ASC');
		$test_unique_codes = $this->db->get('')->result_array();
		$student_info = array();
		$final_Result_info = array();
		if(!empty($test_unique_codes)){
			$list_array = array();
			foreach($test_unique_codes as $uncode){
				$list_array[] = $uncode['unique_test_id'];
				$student_info[$uncode['unique_test_id']]['student_info'] = $uncode;
			}
			$list        = implode("','" , $list_array);
			$list        = "'$list'";
			$and_where   = "";
			$and_where   = "AND DATE(created) + INTERVAL 72 HOUR < NOW()";
			$inner_array = $this->db->query("SELECT * FROM student_results WHERE un_mock_id IN ($list) $and_where ");
							
			if($inner_array->num_rows() > 0){
				$mock_tests = $inner_array->result_array();
				
				foreach($mock_tests as $test){
					
					if(!isset($final_Result_info[$test['un_mock_id']]['student_info']))
						$final_Result_info[$test['un_mock_id']] = $student_info[$test['un_mock_id']];
										
					$final_Result_info[$test['un_mock_id']][$test['collection_no']] = $test;
				}
				
			}else{
				return false;
			}
		
		}else{
			return false;
		}
		
		return $final_Result_info;
		
	}
	
	function updatePendingMocktests($data){
		$this->db->update_batch('mock_speaking_slots', $data, 'book_speaking_slots_id'); 

	}
	
	
	function get_bookings_reminder(){
		$this->db->select('mss.`book_speaking_slots_id`, mss.`stud_id`, mss.`unique_test_id` , mss.`stu_booking_info`, mss.`meeting_id`, mss.`meeting_url` , std.`mobile`, std.`email`, std.`fname`, std.`lname` , zi.`zoneName`');
		$this->db->from('`mock_speaking_slots` mss');
		$this->db->join('`students` std', 'std.`id`= mss.`stud_id`');
		$this->db->join('`zones_info` zi', 'zi.`id`= std.`time_zone`');
		$this->db->where('mss.`result_sent`' , 0);        
        $this->db->order_by('mss.`created` ASC');
		return $this->db->get('')->result_array();		
	}
	
	function get_bookings_information($id){
		$this->db->select('mss.`book_speaking_slots_id`, mss.`stud_id`, mss.`unique_test_id` , mss.`stu_booking_info`, mss.`meeting_id`, mss.`meeting_url` , std.`mobile`, std.`email`, std.`fname`, std.`lname` , zi.`zoneName`');
		$this->db->from('`mock_speaking_slots` mss');
		$this->db->join('`students` std', 'std.`id`= mss.`stud_id`');
		$this->db->join('`zones_info` zi', 'zi.`id`= std.`time_zone`');
		$this->db->where('mss.`unique_test_id`' , $id);        
        $this->db->order_by('mss.`created` ASC');
		return $this->db->get('')->result_array();
		
	}
	
	function getMocktestbyId($id){
		$this->db->select('mss.`book_speaking_slots_id`, mss.`stud_id`, mss.`unique_test_id` ,mss.`band_score` ,mss.`examiner_comment` , std.`mobile`, std.`email`, std.`fname`, std.`lname`');
		$this->db->from('`mock_speaking_slots` mss');
		$this->db->join('`students` std', 'std.`id`= mss.`stud_id`' , 'left');
		$this->db->where('mss.`unique_test_id`' , $id);        
        $this->db->order_by('mss.`created` ASC');
		$test_unique_codes = $this->db->get('')->result_array();
		$student_info = array();
		$final_Result_info = array();
		$modules_result = array();
		if(!empty($test_unique_codes)){
			$list_array = array();
			foreach($test_unique_codes as $uncode){
				$list_array = $uncode['unique_test_id'];
				$student_info[$uncode['unique_test_id']]['student_info'] = $uncode;
			}
			
			
			$list        = $list_array;
			
			$inner_array = $this->db->query("SELECT `test_seriese_id`, `ts_cat_assoc_id` , `un_mock_id`, `collection_no`, `rank`, `marks_secured`, `percentage`, `band_score`, `time_taken`, `totalQuestion`, `skipped`, `attempted`, `paper_checked` FROM student_results WHERE un_mock_id = '$list_array' ");
					
			//t($inner_array);
			
			if($inner_array->num_rows() > 0){
				$mock_tests = $inner_array->result_array();
				
				foreach($mock_tests as $test){
					
					if(!isset($final_Result_info[$test['un_mock_id']]['student_info']))
						$final_Result_info[$test['un_mock_id']] = $student_info[$test['un_mock_id']];
										
					$final_Result_info[$test['un_mock_id']][$test['collection_no']] = $test;
				}
				
				if(!empty($final_Result_info)){
					foreach($final_Result_info as $un_id => $result){		
						$stud_id   = $result['student_info']['stud_id'];
						$unique_test_id   = $result['student_info']['unique_test_id'];
						$modules_result['name']   = $result['student_info']['fname']." ".$result['student_info']['lname'];
						$student_email = $modules_result['email']  = $result['student_info']['email'];
						$modules_result['mobile'] = $result['student_info']['mobile'];
						$book_speaking_slots_id = $result['student_info']['book_speaking_slots_id'];				
						$modules_result['Speaking']['band_score'] = $result['student_info']['band_score'];
						$modules_result['Speaking']['examiner_comment'] = $result['student_info']['examiner_comment'];
						//$update_data[] = array("book_speaking_slots_id" => $book_speaking_slots_id , "result_sent" => 1);

						foreach($result as $module_un => $module){
							$module_name_array = explode("-" , $module_un);
							if(isset($module_name_array['2'])):
								$module_name                  = $module_name_array['2'];
								$modules_result["$module_name"] = $module;
							endif;
						}
					}
				}
				
			}else{
				return false;
			}
		
		}else{
			return false;
		}
		
		return $modules_result;
	}
	
}
