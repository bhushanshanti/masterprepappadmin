<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/
 
class Notification_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }    

    function get_all_notification_message($id,$test_module_id,$programe_id){
       
        $this->db->select('
            sn.`notification_id`,
            sn.test_module_id,
            sn.ts_cat_assoc_id,
            sn.collection_no,
            sn.`read_status`, 
            nm.`message`,
            ns.`subject`,
            ns.`icon`,
            date_format(sn.created, "%D %b %y %I:%i %p") as created,
        ');
        $this->db->from('student_notification sn');        
        $this->db->join('notification_message nm', 'sn.messege_id = nm.messege_id');
        $this->db->join('notification_subject ns', 'ns.id = nm.subject_id');
        $this->db->where(array('sn.active'=> 1,'sn.student_id'=> $id,'sn.programe_id'=> $programe_id)); 
        //$this->db->or_where(array('sn.test_module_id'=> EE_ID,'sn.student_id'=> $id ));
        $this->db->order_by('sn.created', 'DESC');
        $this->db->limit(NOTIFICATION_LIMIT);
        return $this->db->get()->result_array();
        //print_r($this->db->last_query());exit;
    }

    function get_notification_message($subject){       
        $this->db->select('nm.`messege_id`, nm.`message`');
        $this->db->from('notification_message nm');
        $this->db->join('notification_subject ns', 'ns.id = nm.subject_id');
        $this->db->where(array('nm.active'=> 1,'ns.subject' => $subject));
        return $this->db->get()->row_array();
    }
    
    function get_user_deviceToken($user_id){
		$this->db->select('dtoken,dtype');
        $this->db->from('students');       
        $this->db->where(array('id'=> $user_id));
        return $this->db->get()->row_array();
	}

    function add_notification_message($student_id,$messege_id,$programe_id,$test_module_id,$ts_cat_assoc_id,$collection_no,$paper_checked){

        $params = array(
            'student_id'    =>  $student_id,
            'programe_id'   =>  $programe_id,
            'test_module_id' => $test_module_id,
            'ts_cat_assoc_id'=> $ts_cat_assoc_id,
            'collection_no' =>  $collection_no,
            'messege_id'    =>  $messege_id,
            'read_status'   =>  0,
            'active'        =>  1,
            'paper_checked' =>  $paper_checked
        );
        $this->db->insert('student_notification',$params);
        return $this->db->insert_id();
    }

    function get_notification_settings($id){
        
        $this->db->select('
            `mobile_notification`,
            `email_notification`,            
        ');
        $this->db->from('students');
        $this->db->where(array('id'=> $id));
        return $this->db->get()->row_array();
    }
    
    function get_all_notifications($id , $params){
		
		if(isset($params) && !empty($params)){
            $this->db->limit($params['limit'], $params['offset']);
        }
		
		$this->db->select('*');
        $this->db->from('admin_notifications');
        $this->db->where(array('user_id'=> $id));
         $this->db->order_by('id', 'desc');
        return $this->db->get()->result_array();
	}
	
	function count_unread_notifications($id){
		$this->db->select('*');
        $this->db->from('admin_notifications');
        $this->db->where(array('user_id'=> $id , 'status'=> 0));        
        return $this->db->count_all_results();
	}
	
	function count_all_user_notifications($id){
		$this->db->select('*');
        $this->db->from('admin_notifications');
        $this->db->where(array('user_id'=> $id));        
        return $this->db->count_all_results();
	}
	
	function get_notification_byId($id){
        
        $this->db->select('*');
        $this->db->from('admin_notifications');       
        $this->db->where(array('id'=> $id));
         $this->db->order_by('id', 'desc');
        $row = $this->db->get()->row_array();
        if(!empty($row)){
			$this->db->where('id',$id);
			$params = array();
			$params['status'] = 1;
			$this->db->update('admin_notifications',$params);
			
			return $row;
		}
    }
    
}
