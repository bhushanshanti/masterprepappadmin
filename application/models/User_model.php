<?php

 
class User_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    public function checkLogin($email,$password,$checkadmin=false){
        $subquery="";
        if($checkadmin){
            $subquery=" and (roles.name='Admin' or roles.name='Tester' or roles.name='Editor' or roles.name='app_developer' or roles.name='Content Writer' or roles.name='Content Writer SP' or roles.name='Examiner' or roles.name='Enquiry Admin' or roles.name='Mock Test Admin')";
        }
       $query = $this->db->query("select user.id,user.fname,user.lname,user.email,user.mobile,user.dob,user.residential_address,user.created,roles.name,roles.id as roleid from user "
             ." LEFT JOIN user_role ON user_role.user_id=user.id"
             ." LEFT JOIN roles ON roles.id=user_role.role_id"
             ." WHERE user.email='".$email."' and user.password='".md5($password)."' and user.active='1' $subquery");
      //echo $this->db->last_query();die;
      return $query->result();
    }    
    /*
     * Get user by id
     */
    function get_user($id)
    {
        return $this->db->get_where('user',array('id'=>$id))->row_array();
    }
    
    
    /*
     * Get all user count
     */
    function get_all_user_count()
    {
        $this->db->from('user');
        return $this->db->count_all_results();
    }
        
    /*
     * Get all user
     */
    function get_all_user($params = array())
    {
        
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        $this->db->select('u.id,u.fname,u.lname,u.email,u.mobile,u.dob,u.active, 
            ur.role_id, r.name,g.gender_name');
        $this->db->from('user u');
        $this->db->join('user_role ur', 'ur.user_id = u.id','left');
        $this->db->join('roles r', 'r.id = ur.role_id','left');
        $this->db->join('gender g', 'g.id = u.gender','left');
        $this->db->order_by('r.name ASC');
        return $this->db->get('')->result_array();
    } 

    /*
     * Get all examiner
     */
    function get_examiner()
    {                
        $this->db->select('u.id,u.fname,u.email,u.mobile');
        $this->db->from('user u');
        $this->db->join('user_role ur', 'ur.user_id = u.id','left');
        $this->db->join('roles r', 'r.id = ur.role_id','left');
        $this->db->where(array('r.name'=>'Examiner','u.active'=>1));
        $this->db->order_by('assigned_count ASC');
        return $this->db->get('')->row_array();
    }

    /*
     * Get enquiry admin
     */
    function get_enquiry_admin()
    {                
        $this->db->select('u.id,u.fname,u.email,u.mobile');
        $this->db->from('user u');
        $this->db->join('user_role ur', 'ur.user_id = u.id','left');
        $this->db->join('roles r', 'r.id = ur.role_id','left');
        $this->db->where(array('r.name'=>'Enquiry Admin','u.active'=>1));
        $this->db->order_by('assigned_count ASC');
        return $this->db->get('')->row_array();
    } 

    /*
     * Get mock admin
     */
    function get_mock_admin()
    {                
        $this->db->select('u.id,u.fname,u.email,u.mobile');
        $this->db->from('user u');
        $this->db->join('user_role ur', 'ur.user_id = u.id','left');
        $this->db->join('roles r', 'r.id = ur.role_id','left');
        $this->db->where(array('r.name'=>'Mock Test Admin','u.active'=>1));
        $this->db->order_by('assigned_count ASC');
        return $this->db->get('')->row_array();
    } 

    function get_user_role($id){

      $this->db->select('role_id');
        $this->db->from('user_role');
        $this->db->where(array('user_id'=>$id));
        return $this->db->get('')->result_array();  

    }   
        
    /*
     * function to add new user
     */
    function add_user($params)
    {
        $this->db->insert('user',$params);
        return $this->db->insert_id();
    }    
    /*
     * function to update user
     */
    function update_user($id,$params)
    {
        $this->db->where('id',$id);
        return $this->db->update('user',$params);
    }

    function update_assigned_count($id){
       
         return $this->db->query('update `user` 
          set `assigned_count` = `assigned_count` + 1 where id = '.$id.' ');

    }

    function change_password($id,$params)
    {
        $this->db->where(array('id'=>$id));
        return $this->db->update('user',$params);
    }

    function check_old_pwd($id,$pwd){

        $this->db->select('password');
        $this->db->from('user');        
        $this->db->where(array('id'=>$id,'password'=>$pwd));
        return $this->db->get('')->result_array();
        //print_r($this->db->last_query());exit;
    }
    
    /*
     * function to delete user
     */
    function delete_user($id)
    {
        $this->db->delete('user_role',array('user_id'=>$id));
        return $this->db->delete('user',array('id'=>$id));
    }


	/*
     * function to availability
      @userid : user to update
      @data : avaibility info
      @by_user : updated by

     */
    function add_availability($userid , $data, $by_user)
    {
        $add_data = array();

       foreach ($data as $key => $dates) {
          foreach ($dates as $d => $singledate) {           
            $newDate = date("Y-m-d", strtotime($key));
            $singledate_arr = explode("-", $singledate);           
            $innerarray = array();
            $innerarray['user_id'] = $userid;
            $innerarray['date'] = $newDate;
            $innerarray['start_time'] = $singledate_arr[0];
            $innerarray['end_time'] = $singledate_arr[1];            
            $innerarray['byuser'] = $by_user;
            $add_data[] = $innerarray;
          }          
        }
      return $response = $this->db->insert_batch('manage_availability', $add_data); 
    }

    function update_availability($userid , $data, $by_user)
    {
        $update_data = array();
		$date = '';
       foreach ($data as $key => $dates) {
          foreach ($dates as $d => $singledate) {           
            $date = $newDate = $key;
            $singledate_arr = explode("-", $singledate);           
            $innerarray = array();            
            $innerarray['user_id'] = $userid;
            $innerarray['date'] = $newDate;
            $innerarray['start_time'] = $singledate_arr[0];
            $innerarray['end_time'] = $singledate_arr[1];           
            $innerarray['byuser'] = $by_user;
            $update_data[] = $innerarray;
          }          
        }

      $this->db->delete('manage_availability',array('date' => $date ,'user_id' => $userid));
      return $response = $this->db->insert_batch('manage_availability', $update_data); 
    }

    function get_availability($id , $params){
       if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        $this->db->select('date');
        $this->db->from('manage_availability');        
        $this->db->where(array('user_id'=>$id));
        $this->db->order_by('date DESC');
        $this->db->group_by("date");
         
        return $this->db->get('')->result_array();
    }

    function get_availability_count($id){
        $this->db->select('id, start_time , end_time');
        $this->db->from('manage_availability');        
        $this->db->where(array('user_id'=>$id ));
        return $this->db->get()->result_array();
    }

    function get_availability_bydate($id , $date){
        $this->db->select('id, start_time , end_time');
        $this->db->from('manage_availability');        
        $this->db->where(array('user_id'=>$id , 'date'=>$date));        
        return $this->db->get('')->result_array();
    }
    
    function get_bookings_bydate($id , $date){
        $this->db->select('ma.id, ma.start_time , ma.end_time ,ma.booked_by,ma.user_id , ma.booking_status ,ms.FC ,ms.LR ,ms.Pronunciation ,ms.GRA ,ms.meeting_url_sent , ma.booked_by , students.fname, students.lname, students.gender, students.Goal_s, students.targateDate , students.country_code , students.time_zone , zin.zoneName , country.name , gender.gender_name , ms.meeting_id , ms.meeting_url , ms.band_score , ms.examiner_comment, ms.audio_file, ms.book_speaking_slots_id');        
        $this->db->from('manage_availability ma');
        $this->db->join('students', 'ma.`booked_by` = students.`id`','left');        
        $this->db->join('country', 'country.`phonecode` = students.`country_code`','left');
        $this->db->join('zones_info zin', 'zin.`countryCode` = country.`iso`','left');
        $this->db->join('gender', 'gender.`id` = students.`gender`','left');
        $this->db->join('mock_speaking_slots ms', 'ma.`id` = ms.`availability_id`','left');
        $this->db->where(array('user_id'=>$id , 'date'=>$date, 'booking_status'=> 1 ));       
        return $this->db->get('')->result_array();
    }
    
    function remove_schedule($date){
		return $this->db->delete('manage_availability',array('date'=>$date));
	}
    
    
    function update_availabilityby_examiner($id , $params){
        $this->db->where('book_speaking_slots_id',$id);
        $this->db->update('mock_speaking_slots',$params);
        
         $this->db->select('unique_test_id');
        $this->db->from('mock_speaking_slots');        
        $this->db->where(array('book_speaking_slots_id'=>$id));        
        return $this->db->get('')->row_array();
        
    }
}
