<?php
/**
 * @package         MasterPrep
 * @subpackage      Test Series
 * @author          Mohammad Haroon
 *
 **/
 
class Real_test_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    } 

    function get_test_dates($realTestDatesId)
    {
        return $this->db->get_where('real_test_dates',array('realTestDatesId'=>$realTestDatesId))->row_array();
    }

     /*
     * Get all test_dates count IELTS
     */
    function get_all_test_dates_count()
    {        
        $this->db->from('real_test_dates rtd');        
        $this->db->join('test_module tm', 'tm.test_module_id = rtd.test_module_id', 'left');
        $this->db->where(array('tm.test_module_name'=>IELTS,'rtd.programe_id'=>ACD_ID));
        return $this->db->count_all_results();        
    }
    /*
     * Get all real_test_dates
     */
    function get_all_test_dates($params = array())
    {        
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        $this->db->select('
            rtd.`realTestDatesId`,
            rtd.`testDate`,
            rtd.`test_url`,
            rtd.`type`,
            rtd.`active`,
            rtd.`created`,
            pgm.programe_name,
            tm.test_module_name,
            cnt.name
        ');
        $this->db->from('`real_test_dates` rtd');
        $this->db->join('test_module tm', 'tm.test_module_id = rtd.test_module_id', 'left');
        $this->db->join('`programe_masters` pgm', 'rtd.`programe_id`=pgm.`programe_id`', 'left');
        $this->db->join('`country` cnt', 'cnt.`country_id`=rtd.`country_id`', 'left');
        $this->db->where(array());
        $this->db->order_by('`testDate` ASC');
        return $this->db->get()->result_array();
    }

    function add_real_test_dates($params)
    {  
        if($params['type']==1){
            
            $this->db->where('programe_id', $params['programe_id']);
            $this->db->where('test_module_id', $params['test_module_id']);
            $this->db->where('country_id', $params['country_id']);
            $this->db->where('type', $params['type']);
            $this->db->where('test_url', $params['test_url']);

        }elseif($params['type']==2){

            $this->db->where('programe_id', $params['programe_id']);
            $this->db->where('test_module_id', $params['test_module_id']);
            $this->db->where('country_id', $params['country_id']);
            $this->db->where('type', $params['type']);
            $this->db->where('testDate', $params['testDate']);

        }else{

        }        
        $query = $this->db->get('real_test_dates');
        $count_row = $query->num_rows();
        if ($count_row > 0) {          
            return 'DUPLICATE';
        }else{          
            
            $this->db->insert('real_test_dates',$params);
            return $this->db->insert_id(); 
        }               
    }

    /*
     * function to update real_test_dates
     */
    function update_test_date($realTestDatesId,$params)
    {          
            
        $this->db->where('realTestDatesId',$realTestDatesId);
        return $this->db->update('real_test_dates',$params);
    } 

    function getRealTestDates($test_module_id,$country_id){

        $pgm_temp = ACD_ID;

            $this->db->select('`test_url`');
            $this->db->from('`real_test_dates` rtd'); 
            $this->db->where(array('rtd.test_module_id'=> $test_module_id,'rtd.country_id'=> $country_id, 'active'=>1 ));
            $urlData = $this->db->get('')->row_array();

            $this->db->select('            
                DATE_FORMAT(STR_TO_DATE(rtd.testDate, "%d-%m-%Y"), "%Y-%m-%d") as testDate,
                IF(rtd.`programe_id` = '.$pgm_temp.', "Academic", "Academic and General training") as label,
                IF(rtd.`programe_id` = '.$pgm_temp.', "Red", "Blue") as color
            ');
            $this->db->from('`real_test_dates` rtd'); 
            $this->db->where(array('rtd.test_module_id'=> $test_module_id,'rtd.country_id'=> $country_id, 'active'=>1 ));
            $dateData = $this->db->get('')->result_array();
           
            $Data['urlData'] = $urlData;
            $Data['dateData'] = $dateData;
            return $Data;        
    }

    /*
     * function to delete test_dates
     */
    function delete_test_dates($realTestDatesId)
    {
        return $this->db->delete('real_test_dates',array('realTestDatesId'=>$realTestDatesId));
    }
}
