<?php

 
class Package_master_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get package_master by package_id
     */
    function get_package_master($package_id)
    {
        return $this->db->get_where('package_masters',array('package_id'=>$package_id))->row_array();
    }
    
    /*
     * Get all package_masters count Academic
     */
    function get_all_package_masters_count()
    {
        $this->db->from('package_masters');
        $this->db->where('programe_id',ACD_ID);
        return $this->db->count_all_results();
    }

    /*
     * Get all package_masters count GT
     */
    function get_all_package_masters_count_gt()
    {
        $this->db->from('package_masters');
        $this->db->where('programe_id',GT_ID);
        return $this->db->count_all_results();
    }
        
    /*
     * Get all package_masters Academic
     */
    function get_all_package_masters($params = array())
    {   

        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        $this->db->select('
            pkg.`package_id`, 
            pkg.`package_name`,
            pkg.`package_color`,
            pkg.`package_desc`,
            pkg.`star_count`,
            pkg.`amount`,
            pkg.`duration`,
            pkg.`test_paper_limit`,
            pkg.`active`,
            pgm.`programe_name`,
            tm.`test_module_name`
        ');
        $this->db->from('`package_masters` pkg');
        $this->db->join('`programe_masters` pgm', 'pkg.`programe_id`= pgm.`programe_id`');
        $this->db->join('`test_module` tm', 'pkg.`test_module_id`=tm.`test_module_id`');
        $this->db->where('pkg.programe_id',ACD_ID);
        $this->db->order_by('pkg.`amount` ASC');
        return $this->db->get('')->result_array();
        //print_r($this->db->last_query());exit;
    }

    /*
     * Get all package_masters Academic
     */
    function get_all_package_masters_gt($params = array())
    {      

        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        $this->db->select('
            pkg.`package_id`, 
            pkg.`package_name`,
            pkg.`package_color`,
            pkg.`package_desc`,
            pkg.`star_count`,
            pkg.`amount`,
            pkg.`duration`,
            pkg.`test_paper_limit`,
            pkg.`active`,
            pgm.`programe_name`,
            tm.`test_module_name`
        ');
        $this->db->from('`package_masters` pkg');
        $this->db->join('`programe_masters` pgm', 'pkg.`programe_id`= pgm.`programe_id`');
        $this->db->join('`test_module` tm', 'pkg.`test_module_id`=tm.`test_module_id`');
        $this->db->where('pkg.programe_id',GT_ID);
        $this->db->order_by('pkg.`amount` ASC');
        return $this->db->get('')->result_array();
        //print_r($this->db->last_query());exit;
    }

    function get_recommended_package($programe_id,$test_module_id,$wt){

        $this->db->select('
            `package_id`,            
            `package_color`, 
            `package_name`,
            `star_count`,
            `package_desc`,
            `amount`,
             CONCAT(`duration`, " Day(s)") as duration,
            `test_paper_limit`
        ');
        $this->db->from('`package_masters`');
        $this->db->where(array('programe_id'=>$programe_id,'test_module_id'=>$test_module_id, 'active'=>1));   
        if($wt==1){
            $this->db->order_by('`amount` DESC');
            $this->db->limit(1);
            
        }elseif($wt==3){
           $this->db->order_by('`amount` ASC');
            $this->db->limit(1);
        }elseif($wt==2){
            $this->db->order_by('`amount` DESC');
            $this->db->limit(1,1);
        }else{
            return [];
        }
        return $this->db->get('')->row_array();
    }

    function get_all_package($programe_id,$test_module_id,$id)
    {
        $this->db->select('`package_id`');
        $this->db->from('`student_package`');
        $this->db->where(array('student_id'=>$id, 'active'=>1));
        $up = $this->db->get('')->row_array();
        $package_id = $up['package_id'];

        if(!$package_id){

            $package_id=0;            
            $this->db->select('
            `package_id`,            
            `package_color`, 
            `package_name`,
            `star_count`,
            `package_desc`,
            `amount`,
             CONCAT(`duration`, " Day(s)") as duration,
            `test_paper_limit`,
             IF(`package_id` = '.$package_id.', 1, 0) as bought,
             IF(`amount` > 0 , 0 , 1 ) as free_package,
            ');
            $this->db->from('`package_masters`');
            $this->db->where(array('programe_id'=>$programe_id,'test_module_id'=>$test_module_id, 'active'=>1));        
            $this->db->order_by('`amount` ASC');
            return $this->db->get('')->result_array();

        }else{

            $this->db->select('
            `package_id`, 
            `package_color`,
            `package_name`,
            `star_count`,
            `package_desc`,
            `amount`,
            CONCAT(`duration`, " Day(s)") as duration,
            `test_paper_limit`,             
             IF(package_id = '.$package_id.', 1, 0) as bought, 
             IF(`amount` > 0 , 0 , 1 ) as free_package,
            ');
            $this->db->from('`package_masters`');
            $this->db->where(array('programe_id'=>$programe_id,'test_module_id'=>$test_module_id, 'active'=>1));        
            $this->db->order_by('`amount` ASC');
            return $this->db->get('')->result_array();
            //print_r($this->db->last_query());exit;
        }        
    }

    function get_all_package_history($id,$programe_id,$test_module_id){

        $this->db->select('
                   
            pkg.package_id,
            pkg.package_color,
            pkg.package_name,
            pkg.star_count,
            pkg.package_desc,
            CONCAT("Rs ", pkg.amount) AS package_cost,
            CONCAT(pkg.duration, " Days") as package_duration,
            pkg.test_paper_limit as package_paper_count,
            spkg.`student_package_id`, 
            spkg.`order_id`,
            spkg.`active` as package_status,
            spkg.`paper_left` as left_paper_count,
            CONCAT("Rs ", FORMAT(spkg.`amount`/100,2)) AS amount_paid,
            date_format(spkg.subscribed_on, "%D %b %y") as `subscribed_on`,
            date_format(spkg.expired_on, "%D %b %y") as `expired_on`,
            date_format(spkg.requested_on, "%D %b %y %I:%i %p") as `requested_on`,
            IF(pkg.amount > 0 , 0 , 1 ) as free_package,
        ');
        $this->db->from('`student_package` spkg');
        $this->db->join('`package_masters` pkg', 'pkg.`package_id`= spkg.`package_id`');
        $this->db->where(array('spkg.programe_id'=>$programe_id,'spkg.test_module_id'=>$test_module_id, 'spkg.student_id'=>$id));
        $this->db->order_by('spkg.`requested_on` DESC');
        return $this->db->get('')->result_array();

    }

    function get_student_pack_subscribed($id){

        $this->db->select('         
            
            pkg.package_id,            
            pkg.package_color,
            pkg.package_name,
            pkg.star_count,
            CONCAT("Rs ", pkg.amount) AS package_cost,
            CONCAT(pkg.duration, " Days") as package_duration,
            pkg.test_paper_limit as package_paper_count,
            spkg.`student_package_id`, 
            spkg.`order_id`,
            spkg.`payment_id`,
            spkg.`method`,
            spkg.`active` as package_status,
            spkg.`paper_left` as left_paper_count,
            CONCAT("Rs ", FORMAT(spkg.`amount`/100,2)) AS amount_paid,
            date_format(spkg.subscribed_on, "%D %b %y") as `subscribed_on`,
            date_format(spkg.expired_on, "%D %b %y") as `expired_on`,
            date_format(spkg.requested_on, "%D %b %y %I:%i %p") as `requested_on`,
        ');
        $this->db->from('`student_package` spkg');
        $this->db->join('`package_masters` pkg', 'pkg.`package_id`= spkg.`package_id`');
        $this->db->where(array('spkg.student_id'=>$id));       
        $this->db->order_by('spkg.`requested_on` DESC');
        return $this->db->get('')->result_array();

    }


    /*
     * Get all package_masters count Academic
     */
    function get_all_transaction_count()
    {
        $this->db->from('student_package');
        $this->db->where('programe_id',ACD_ID);
        return $this->db->count_all_results();
    }

    function get_all_transaction_count_gt()
    {
        $this->db->from('student_package');
        $this->db->where('programe_id',GT_ID);
        return $this->db->count_all_results();
    }

    function get_all_package_history_admin($params = array()){

        if(isset($params) && !empty($params)){
            $this->db->limit($params['limit'], $params['offset']);
        }

        $this->db->select('         
            
            pkg.package_id,
            pkg.package_name,
            pkg.star_count,
            spkg.`student_package_id`, 
            spkg.`email`,
            spkg.`contact`,
            spkg.`order_id`,
            spkg.`payment_id`,
            spkg.`method`,
            spkg.`paper_left`,
            spkg.`active` as package_status,
            spkg.`paper_left` as left_paper_count,
            CONCAT("Rs ", FORMAT(spkg.`amount`/100,2)) AS amount_paid,
            date_format(spkg.subscribed_on, "%D %M %y") as `subscribed_on`,
            date_format(spkg.expired_on, "%D %M %y") as `expired_on`,
            date_format(spkg.requested_on, "%D %M %y") as `requested_on`,
            std.fname,
            std.lname,
        ');
            $this->db->from('`student_package` spkg');
            $this->db->join('`package_masters` pkg', 'pkg.`package_id`= spkg.`package_id`');
            $this->db->join('`students` std', 'std.`id`= spkg.`student_id`');
            $this->db->where(array('spkg.programe_id'=> ACD_ID ));        
            $this->db->order_by('spkg.`requested_on` DESC');
            return $this->db->get('')->result_array();

    }

    function get_all_package_history_admin_gt($params = array()){

        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }

        $this->db->select('         
            
            pkg.package_id,
            pkg.package_name,
            pkg.star_count,
            spkg.`student_package_id`, 
            spkg.`email`,
            spkg.`contact`,
            spkg.`order_id`,
            spkg.`payment_id`,
            spkg.`method`,
            spkg.`paper_left`,
            spkg.`active` as package_status,
            spkg.`paper_left` as left_paper_count,
            CONCAT("Rs ", FORMAT(spkg.`amount`/100,2)) AS amount_paid,
            date_format(spkg.subscribed_on, "%D %M %y") as `subscribed_on`,
            date_format(spkg.expired_on, "%D %M %y") as `expired_on`,
            date_format(spkg.requested_on, "%D %M %y") as `requested_on`,
            std.fname,
            std.lname,
        ');
            $this->db->from('`student_package` spkg');
            $this->db->join('`package_masters` pkg', 'pkg.`package_id`= spkg.`package_id`');
            $this->db->join('`students` std', 'std.`id`= spkg.`student_id`');
            $this->db->where(array('spkg.programe_id'=>GT_ID));        
            $this->db->order_by('spkg.`requested_on` DESC');
            return $this->db->get('')->result_array();    
    }
        
    /*
     * function to add new package_master
     */
    function add_package_master($params)
    {
        $this->db->insert('package_masters',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update package_master
     */
    function update_package_master($package_id,$params)
    {
        $this->db->where('package_id',$package_id);
        return $this->db->update('package_masters',$params);
    }
    
    /*
     * function to delete package_master
     */
    function delete_package_master($package_id)
    {
        return $this->db->delete('package_masters',array('package_id'=>$package_id));
    }

    /*
     * function to deactivate std pack
     */
    function update_expiring_package($today)
    {
        $params = array(
            'active' => 0,
        );
        $this->db->where('expired_on',$today)
        ->or_where('paper_left',0);
        return $this->db->update('student_package',$params);
    }

    function update_expiring_package_byPaperLimit($student_id,$programe_id,$test_module_id){
        $params = array(
            'active' => 0,
        );
        $this->db->where(array('programe_id'=>$programe_id,'test_module_id'=>$test_module_id, 'student_id'=>$student_id,'paper_left'=>0));
        return $this->db->update('student_package',$params);
    }

    /*
    * function to update pack limit
    */
    function update_package_limit($student_id,$programe_id,$test_module_id){

        $this->db->query('UPDATE `student_package` SET `paper_left` = `paper_left`-0.25 WHERE `student_id` = '.$student_id.' AND `programe_id` = '.$programe_id.' AND `test_module_id` = '.$test_module_id.' AND `active` = 1');
    }

    /*
    * function to get pack status
    */
    function get_package_limit_status($student_id,$programe_id,$test_module_id){

        $this->db->select('paper_left');
        $this->db->from('`student_package`');
        $this->db->where(array('programe_id'=>$programe_id,'test_module_id'=>$test_module_id, 'student_id'=>$student_id,'active'=>1));
            return $this->db->get('')->row_array();
    }

    /*
    * function to get Expired Package
    */
    function getExpiredPackage(){

        $this->db->select('             
            pkg.`package_name`,
            pkg.`star_count`,
            spkg.`contact`,
            spkg.`email`,
            std.fname,
        ');
        $this->db->from('`student_package` spkg');
        $this->db->join('`package_masters` pkg', 'pkg.`package_id`= spkg.`package_id`');
        $this->db->join('`students` std', 'std.`id`= spkg.`student_id`');
        $this->db->where(array('spkg.expired_on'=> TODAY,'spkg.active'=>0));
        return $this->db->get('')->result_array();
    }
}
