<?php

class Get_timezones_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    
    /*
     * Get all Timezones by countrycode
     */
    function get_all_timezonesbycountry($phoneCode)
    {
       
        $this->db->select('z.`id`, z.`zoneName`');
        $this->db->from('country c');
        $this->db->join('`zones_info` z', 'z.`countryCode`= c.`iso`');
       
        $this->db->where(array('c.`phonecode`' => $phoneCode ));
        return $this->db->get()->result_array();
    }
    
    /*
     * Get all Timezones by countrycode
     */
    function get_all_timezonesbycountry_code($iso)
    {
       
        $this->db->select('z.`id`, z.`zoneName`');
        $this->db->from('country c');
        $this->db->join('`zones_info` z', 'z.`countryCode`= c.`iso`');
       
        $this->db->where(array('c.`iso`' => $iso ));
        return $this->db->get()->result_array();
    }

    
}
