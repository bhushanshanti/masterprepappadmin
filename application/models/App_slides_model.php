<?php
 
class App_slides_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get role by id
     */
    function get_app_slides($id)
    {
        return $this->db->get_where('app_slides',array('id'=>$id))->row_array();
    }
    
    /*
     * Get all roles count
     */
    function get_all_app_slides_count()
    {
        $this->db->from('app_slides');
        return $this->db->count_all_results();
    }

    function get_all_app_slides_active()
    {
        $this->db->order_by('id', 'desc');  
        $this->db->where('active', 1);           
        return $this->db->get('app_slides')->result_array();
    }
        
    /*
     * Get all roles
     */
    function get_all_app_slides($params = array())
    {
        $this->db->order_by('id', 'desc');
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        return $this->db->get('app_slides')->result_array();
    }
        
    /*
     * function to add new role
     */
    function add_app_slides($params)
    {
        $this->db->insert('app_slides',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update role
     */
    function update_app_slides($id,$params)
    {
        $this->db->where('id',$id);
        return $this->db->update('app_slides',$params);
    }
    
    /*
     * function to delete role
     */
    function delete_app_slides($id)
    {
        return $this->db->delete('app_slides',array('id'=>$id));
    }

     
}
