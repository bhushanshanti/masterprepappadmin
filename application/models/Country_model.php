<?php


class Country_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get country by country_id
     */
    function get_country($id)
    {
        return $this->db->get_where('country',array('country_id'=>$id))->row_array();
    }
    
    /*
     * Get all country count
     */
    function get_all_country_count()
    {
        $this->db->from('country');
        return $this->db->count_all_results();
    }
        
    /*
     * Get all country
     */
    function get_all_country($params = array())
    {
        $this->db->order_by('name', 'ASC');
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        return $this->db->get('country')->result_array();
    }

    /*
     * Get all country active
     */
    function get_all_country_active($params = array())
    {
        $this->db->order_by('name', 'ASC');
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        $this->db->where(array('active'=>1));
        return $this->db->get('country')->result_array();
    }
        
    /*
     * function to add new country
     */
    function add_country($params)
    {
        $this->db->insert('country',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update country
     */
    function update_country($id,$params)
    {
        $this->db->where('country_id',$id);
        return $this->db->update('country',$params);
    }
    
    /*
     * function to delete country
     */
    function delete_country($id)
    {
        return $this->db->delete('country',array('country_id'=>$id));
    }
}
