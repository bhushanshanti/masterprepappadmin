<?php

 
class Instruction_master_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get instruction_master by id
     */
    function get_instruction_master($id)
    {
        return $this->db->get_where('instruction_masters',array('id'=>$id))->row_array();
    }
    
    /*
     * Get all instruction_masters count
     */
    function get_all_instruction_masters_count()
    {
        $this->db->from('instruction_masters');
        return $this->db->count_all_results();
    }

    function check_duplicate_instruction_master($category_id)
    {
        $this->db->from('instruction_masters');
        $this->db->where(array('category_id'=>$category_id));
        return $this->db->count_all_results();
    }
        
    /*
     * Get all instruction_masters
     */
    function get_all_instruction_masters($params = array())
    {
        
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }       

        $this->db->select('ins.`id`,ins.`category_id`,ins.`content`,ins.`active`, cat.`category_name`,pgm.`programe_name`,tm.`test_module_name`');
        $this->db->from('`instruction_masters` ins');
        $this->db->join('`category_masters` cat', 'cat.`category_id`=ins.`category_id`');
        $this->db->join('`programe_masters` pgm', 'cat.`programe_id`=pgm.`programe_id`');
        $this->db->join('`test_module` tm', 'tm.`test_module_id`= cat.`test_module_id`');
        $this->db->order_by('ins.id', 'desc');
        return $this->db->get()->result_array();
    }

    function get_instruction($category_id)
    {
        $this->db->select('cat.`category_name`, ins.`content`');
        $this->db->from('`instruction_masters` ins');
        $this->db->join('`category_masters` cat', 'cat.`category_id`=ins.`category_id`');
        $this->db->where(array('cat.`category_id`'=>$category_id, 'ins.`active`'=>1));
        return $this->db->get()->row_array();
    }
        
    /*
     * function to add new instruction_master
     */
    function add_instruction_master($params)
    {
        $this->db->insert('instruction_masters',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update instruction_master
     */
    function update_instruction_master($id,$params)
    {
        $this->db->where('id',$id);
        return $this->db->update('instruction_masters',$params);
    }
    
    /*
     * function to delete instruction_master
     */
    function delete_instruction_master($id)
    {
        return $this->db->delete('instruction_masters',array('id'=>$id));
    }
}
