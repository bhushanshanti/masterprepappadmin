<?php
 
class Previous_Ielts_tests_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get role by id
     */
    function get_p_tests($id)
    {
		$this->db->select('pit.*, pgm.`programe_name`');
        $this->db->from('`previous_ielts_tests` pit');       ;
        //$this->db->join('`category_masters` cat', 'pit.`category_id`= cat.`category_id`');
        $this->db->join('`programe_masters` pgm', 'pit.`category_id`= pgm.`programe_id`');
        $this->db->where(array('pit.`id`'=> $id));
         return $this->db->get()->result_array();
    }
    
    /*
     * Get all roles count
     */
    function get_all_p_tests_count()
    {
        $this->db->from('previous_ielts_tests');
        return $this->db->count_all_results();
    }

    function get_all_p_tests_active()
    {
        $this->db->order_by('id', 'desc');  
        $this->db->where('active', 1);           
        return $this->db->get('previous_ielts_tests')->result_array();
    }
        
    /*
     * Get all roles
     */
    function get_all_p_tests($params = array())
    {
        $this->db->order_by('id', 'desc');
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        return $this->db->get('previous_ielts_tests')->result_array();
    }
        
    /*
     * function to add new role
     */
    function add_p_tests($params)
    {
        $this->db->insert('previous_ielts_tests',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update role
     */
    function update_p_tests($id,$params)
    {
        $this->db->where('id',$id);
        return $this->db->update('previous_ielts_tests',$params);
    }
    
    /*
     * function to delete role
     */
    function delete_p_tests($id)
    {
        return $this->db->delete('previous_ielts_tests',array('id'=>$id));
    }


	function get_all_previous_test_gt($params = array())
    {        
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        $this->db->select('pit.*, pgm.`programe_name`,c.`name`');
        $this->db->from('`previous_ielts_tests` pit');       ;
        //$this->db->join('`category_masters` cat', 'pit.`category_id`= cat.`category_id`');
        $this->db->join('`programe_masters` pgm', 'pit.`category_id`= pgm.`programe_id`');
        $this->db->join('`country` c', 'pit.`country_id`= c.`country_id`');
        $this->db->where(array('pgm.`programe_id'=>GT_ID));   
        return $this->db->get()->result_array();
    }
    
    function get_all_previous_test_ac($params = array())
    {       
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        $this->db->select('pit.*, pgm.`programe_name`,c.`name`');
        $this->db->from('`previous_ielts_tests` pit');       ;
        //$this->db->join('`category_masters` cat', 'pit.`category_id`= cat.`category_id`');
        $this->db->join('`programe_masters` pgm', 'pit.`category_id`= pgm.`programe_id`');
        $this->db->join('`country` c', 'pit.`country_id`= c.`country_id`');
        $this->db->where(array('pgm.`programe_id'=>ACD_ID));       
      
        return $this->db->get()->result_array();
    }
    
    function get_all_previous_test_ac_count($params = array())
    {       
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        $this->db->select('pit.*');
        $this->db->from('`previous_ielts_tests` pit');       
        $this->db->where(array('pit.`category_id'=>ACD_ID));       
      
        return $this->db->count_all_results();
    }
    
    function get_all_previous_test_gt_count($params = array())
    {        
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        $this->db->select('pit.*, cat.`category_name`');
        $this->db->from('`previous_ielts_tests` pit');        
        $this->db->where(array('pit.`category_id'=>GT_ID));   
        return $this->db->count_all_results();
    }
    
    
    /* Function for REST API */
    function get_previous_tests($programe_id , $test_module_id , $student_id ,$params = array()){
		
		$this->db->select('pre_test_id');
        $this->db->from('pre_tests_read_history');       
        $this->db->where(array('stud_id'=>$student_id));   
        $already_read = $this->db->get()->result_array();
      
        if(!empty($already_read)){
			$previous_tests = $this->db->query("SELECT `pit`.`id`, `pit`.`content`, `pit`.`created`, `pgm`.`programe_name` FROM `previous_ielts_tests` `pit` JOIN `programe_masters` `pgm` ON pit.`category_id`= pgm.`programe_id` WHERE `pgm`.`programe_id` = $programe_id AND `pit`.`module_id` = 1 AND `pit`.`active` = 1 AND NOT EXISTS (SELECT `id` FROM `pre_tests_read_history` WHERE pre_test_id = pit.id AND stud_id = $student_id) ORDER BY `pit`.`created` DESC LIMIT ".$params['offset'].", ".$params['limit']." ");
						
			if($previous_tests->num_rows() > 0){
				return $previous_tests->result_array();
			}else{
				return false;
			}
			
		}
		else{
			$this->db->select('pit.id , pit.content , pit.created , pgm.`programe_name`');
			$this->db->from('`previous_ielts_tests` pit');
			$this->db->join('`programe_masters` pgm', 'pit.`category_id`= pgm.`programe_id`');        
			$this->db->where(array('pgm.`programe_id'=>$programe_id , 'pit.`module_id`'=> $test_module_id ,'pit.`active`'=> 1));
			$this->db->order_by('pit.id', 'DESC');
			if(isset($params) && !empty($params)){
				$this->db->limit($params['limit'], $params['offset']);
			}
			return $this->db->get()->result_array();
		}
       
       
		
	}
	
    function count_previous_tests($programe_id , $test_module_id , $student_id){
		
		$this->db->select('pre_test_id');
        $this->db->from('pre_tests_read_history');       
        $this->db->where(array('stud_id'=>$student_id));   
        $already_read = $this->db->get()->result_array();
      
        if(!empty($already_read)){
			$previous_tests = $this->db->query("SELECT `pit`.`id`, `pit`.`content`, `pit`.`created`, `pgm`.`programe_name` FROM `previous_ielts_tests` `pit` JOIN `programe_masters` `pgm` ON pit.`category_id`= pgm.`programe_id` WHERE `pgm`.`programe_id` = $programe_id AND `pit`.`module_id` = 1 AND `pit`.`active` = 1 AND NOT EXISTS (SELECT `id` FROM `pre_tests_read_history` WHERE pre_test_id = pit.id AND stud_id = $student_id) ");
						
			if($previous_tests->num_rows() > 0){
				return $previous_tests->num_rows();
			}else{
				return false;
			}
			
		}
		else{
			$this->db->select('pit.id , pit.content , pit.created , pgm.`programe_name`');
			$this->db->from('`previous_ielts_tests` pit');
			$this->db->join('`programe_masters` pgm', 'pit.`category_id`= pgm.`programe_id`');        
			$this->db->where(array('pgm.`programe_id'=>$programe_id , 'pit.`module_id`'=> $test_module_id ,'pit.`active`'=> 1));
			
			return $this->db->get()->num_rows();
		}
       
       
		
	}
    
    function check_if_shortlisted($test_id , $student_id){
		$this->db->select('shortlist_id');
        $this->db->from('shortlist_previous_test');       
        $this->db->where(array('pre_test_id'=>$test_id ,'student_id'=>$student_id));   
        return $this->db->get()->row_array();
	}
	
	function add_shortlist($data){
		$this->db->insert('shortlist_previous_test',$data);
        return $this->db->insert_id();
	}
	
	function remove_shortlist($test_id , $student_id){
        return $this->db->delete('shortlist_previous_test', array('pre_test_id'=>$test_id ,'student_id'=>$student_id));
	}
	
	function get_shortlisted_previous_tests($student_id){
		$this->db->select('pre_test_id');
        $this->db->from('shortlist_previous_test');       
        $this->db->where(array('student_id'=>$student_id));   
        $shortlistearray = $this->db->get()->result_array();
        $return_array    = array();
        if(!empty($shortlistearray)){
			foreach($shortlistearray as $key => $value){
				$return_array[] = $value['pre_test_id'];
			}
		}
        
        return $return_array;
        
	}
	
	
	function get_detailed_shortlisted_previous_tests($programe_id , $test_module_id , $student_id){
		$this->db->select('pit.id , pit.content , pgm.`programe_name`');
        $this->db->from('`shortlist_previous_test` spt'); 
        $this->db->join('`previous_ielts_tests` pit' , 'pit.`id`= spt.`pre_test_id`');       
        $this->db->join('`programe_masters` pgm', 'pit.`category_id` = pgm.`programe_id`');     
        $this->db->where(array('pit.`module_id`'=> $test_module_id ,'pit.`active`'=> 1  ,'spt.`student_id`'=> $student_id ));   
		return $this->db->get()->result_array();
	}
	
	function add_readlist($data){
		$this->db->select('pre_test_id');
        $this->db->from('pre_tests_read_history');       
        $this->db->where(array('stud_id'=>$data['stud_id'] ,'pre_test_id'=>$data['pre_test_id'] ));   
        $already_read = $this->db->get()->result_array();
      
        if(!empty($already_read)){
			return true;
		}else{
			$this->db->insert('pre_tests_read_history',$data);
			return $this->db->insert_id();
		}       
	}
	
	function get_user_read_test($programe_id , $test_module_id , $student_id ,$params = array()){
		$this->db->select('pit.id , pit.created , pit.content , pgm.`programe_name`');
        $this->db->from('`pre_tests_read_history` ptrh'); 
        $this->db->join('`previous_ielts_tests` pit' , 'pit.`id`= ptrh.`pre_test_id`');       
        $this->db->join('`programe_masters` pgm', 'pit.`category_id` = pgm.`programe_id`');        
        $this->db->where(array('pit.`active`'=> 1  ,'ptrh.`stud_id`'=> $student_id ));
        $this->db->order_by('ptrh.read_history_id', 'DESC');        
        if(isset($params) && !empty($params)){
            $this->db->limit($params['limit'], $params['offset']);
        }
		return $this->db->get()->result_array();	
	}
	
	
}
