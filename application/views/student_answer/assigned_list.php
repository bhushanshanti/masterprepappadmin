<input class="form-control" type="text" placeholder="Search" aria-label="Search" id="myInput">
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <?php echo $this->session->flashdata('flsh_msg'); ?>           

            <div class="box-body table-responsive">
                <table class="table table-striped table-bordered table-sm">
                    <thead>
                    <tr>
						<th><?php echo SR;?></th>	
						<th>Paper Attemt Id</th>
                        <th>Module</th>    	
                        <th>Test Name<?php echo SEP;?>Type</th>
                        <th>Programme<?php echo SEP;?>Category</th>
                        <th>Duration<?php echo SEP;?>M.M.</th>
                        <th>Attempted on</th>
                        <th><?php echo STATUS;?></th>
                        <th><?php echo ACTION;?></th>                        
                    </tr>
                    </thead>
                    <tbody id="myTable">
                    <?php $sr=0; foreach($student_attempts as $a){ $sr++; ?>
                    <tr>
						<td><?php echo $sr; ?></td>						 
						<td><?php echo $a['collection_no']; ?></td>
                        <td><?php echo $a['test_module_name']; ?></td>
                        <td><?php echo $a['test_seriese_name'].SEP.$a['test_seriese_type']; ?></td>
                        <td><?php echo $a['programe_name'].SEP.$a['category_name']; ?></td>
                        <td><?php echo $a['paper_duration'].PAPER_DURATION_UNIT.SEP.$a['max_marks']; ?></td>	
                        <td>
                            <?php
                                $date=date_create($a['created']);
                                echo $created = date_format($date,"M d, Y | H:i:s");
                            ?>
                        </td>
                        <td>
                            <?php
                                if($a['paper_checked']==1){
                                    echo $chk='<span class="text-success"><i class="fa fa-check"></i></span>';
                                    $btn_text='Get result';
                                    $btn_icon='eye';
                                    $btn_color='info';
                                }else{
                                    echo $chk='<span class="text-warning"><i class="fa fa-close"></i></span>';
                                    $btn_text='Check paper';
                                    $btn_icon='check';
                                    $btn_color='warning';
                                }                                
                            ?>
                                
                        </td>					
						
						<td>
                            <a href="<?php echo site_url('student_answer/get_result/'.$a['collection_no'].'/'.$a['ts_cat_assoc_id'].'/'.$a['student_id']); ?>" class="btn btn-<?php echo $btn_color;?> btn-xs" data-toggle="tooltip" title="<?php echo $btn_text;?>"><span class="fa fa-<?php echo $btn_icon;?>"></span> <?php echo $btn_text;?></a> 
                            <a href="<?php echo site_url('student_answer/remove/'.$a['collection_no'].'/'.$a['ts_cat_assoc_id'].'/'.$a['student_id']); ?>" class="btn btn-danger btn-xs" data-toggle="tooltip" title="Delete Result" onclick="return confirm('Are you sure you want to delete this item?');"><span class="fa fa-trash"></span> </a>                           
                        </td>
                        
                    </tr>
                    <?php } ?>
                </tbody>
                </table>
                <div class="pull-right">
                    <?php echo $this->pagination->create_links(); ?>                    
                </div>                
            </div>
        </div>
    </div>
</div>
