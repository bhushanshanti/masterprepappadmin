<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title"><?php echo $title;?></h3>
            </div>
            <?php echo $this->session->flashdata('flsh_msg'); ?>
			<?php echo form_open('Faq_master/edit/'.$faq_master['id']); ?>
			<div class="box-body">
				<div class="row clearfix">
					
					<div class="col-md-4">
						<label for="parent_subject" class="control-label"><span class="text-danger">*</span>Parent subject</label>
						<div class="form-group has-feedback">
							<input type="text" name="parent_subject" value="<?php echo ($this->input->post('parent_subject') ? $this->input->post('parent_subject') : $faq_master['parent_subject']); ?>" class="form-control" id="parent_subject" />							
							<span class="text-danger"><?php echo form_error('parent_subject');?></span>
						</div>
					</div>

					<div class="col-md-4">
						<label for="test_module_id" class="control-label"><span class="text-danger">*</span>Test module</label>
						<div class="form-group">
							<select name="test_module_id" class="form-control selectpicker" data-show-subtext="true" data-live-search="true">
								<option data-subtext="" value="">Select </option>
								<?php 
								foreach($all_test_module as $t)
								{
									$selected = ($t['test_module_id'] == $faq_master['test_module_id']) ? ' selected="selected"' : "";

									echo '<option data-subtext="'.$t['test_module_name'].'" value="'.$t['test_module_id'].'" '.$selected.'>'.$t['test_module_name'].'</option>';
								} 
								?>
							</select>
							<span class="text-danger"><?php echo form_error('test_module_id');?></span>
						</div>
					</div>

					<div class="col-md-4">
						<label for="icon" class="control-label">Icon URL</label>
						[
						<span class="text-danger"><a href="<?php echo site_url('gallery/add');?>" target="_blank">
							<?php echo GALLERY_URL_LABEL;?></a>
						</span>
						]&nbsp;
						[
						<span class="text-danger"><a href="<?php echo site_url('gallery/index');?>" target="_blank">
							<?php echo GALLERY_URL_LABEL_LIST;?></a>
						</span>
						]
						<div class="form-group has-feedback">
							<input type="url" name="icon" value="<?php echo ($this->input->post('icon') ? $this->input->post('icon') : $faq_master['icon']); ?>" class="form-control" id="icon" placeholder='Copy link from gallery & paste here'/>
							<span class="glyphicon glyphicon-link form-control-feedback"></span>
							<span class="text-danger"><?php echo form_error('icon');?></span>
						</div>
					</div>

					<div class="col-md-12">
						<label for="short_desc" class="control-label">Short Description</label>
						<div class="form-group has-feedback">
							<textarea name="short_desc" class="form-control myckeditor" id="short_desc"><?php echo ($this->input->post('short_desc') ? $this->input->post('short_desc') : $faq_master['short_desc']); ?></textarea>
							<span class="glyphicon glyphicon-text-size form-control-feedback"></span>
						</div>
					</div>					

					<div class="col-md-6">
						<div class="form-group">
							<input type="checkbox" name="active" value="1" <?php echo ($faq_master['active']==1 ? 'checked="checked"' : ''); ?> id='active' />
							<label for="active" class="control-label">Active</label>
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
            	<button type="submit" class="btn btn-success">
					<i class="fa fa-check"></i> Save
				</button>
	        </div>				
			<?php echo form_close(); ?>
		</div>
    </div>
</div>
