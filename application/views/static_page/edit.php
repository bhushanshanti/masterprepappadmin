<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Edit Static Page </h3>
            </div>
            <?php echo $this->session->flashdata('flsh_msg');?>
			<?php echo form_open_multipart('static_page/edit/'.$static_page['static_page_id']); ?>
			<?php //echo 'Parent id : '. $static_page_parent['static_page_id'].' , Title : '.$static_page_parent['title'];?>
			</div>
			<div class="box-body">
				<div class="row clearfix">
					
					<div class="col-md-6">
						<label for="pid" class="control-label">Parent</label>
						<div class="form-group">
							<select name="pid" class="form-control selectpicker" data-show-subtext="true" data-live-search="true">
								<option value="">Select parent</option>
								<?php 
								foreach($static_pages2 as $pid)
								{
									$selected = ($pid['static_page_id'] == $static_page_parent['static_page_id']) ? ' selected="selected"' : "";

									echo '<option value="'.$pid['static_page_id'].'" '.$selected.'>
									'.$pid['title'].'</option>';									
								} 
								?>
							</select>
							<span class="text-danger"><?php echo form_error('pid');?></span>
						</div>
					</div>

					<div class="col-md-6">
						<label for="package_id" class="control-label"><span class="text-danger">*</span>Package</label>
						<div class="form-group">
							<select name="package_id[]" class="form-control selectpicker" data-show-subtext="true" data-live-search="true" multiple>
								<option data-subtext="" value="">Select Package</option>
								<?php
								$packages = explode("," , $static_page['packages']);
								
								
								foreach($all_packages as $p)
								{
									$selected = "";
									if(in_array($p['package_id'], $packages)){
										$selected = "selected";
									}									
									echo '<option data-subtext="'.$p['package_name'].' | '.$p['programe_name'].'" value="'.$p['package_id'].'" '.$selected.'>'.$p['package_name'].' | '.$p['programe_name'].'</option>';
								} 
								?>
							</select>
							<span class="text-danger"><?php echo form_error('package_id');?></span>
						</div>
					</div>

					<div class="col-md-6">
						<label for="category_id" class="control-label"><span class="text-danger">*</span>Category </label>
						<div class="form-group">
							<select name="category_id" class="form-control selectpicker" data-show-subtext="true" data-live-search="true">
								<option value="">select category</option>
								<?php 
								foreach($all_category_masters as $category_master)
								{
									$selected = ($category_master['category_id'] == $static_page['category_id']) ? ' selected="selected"' : "";

									echo '<option value="'.$category_master['category_id'].'" '.$selected.'>'.$category_master['programe_name'].' | '.$category_master['category_name'].'</option>';
								} 
								?>
							</select>
							<span class="text-danger"><?php echo form_error('category_id');?></span>
						</div>
					</div>

					<div class="col-md-6">
						<label for="question_type_id" class="control-label">Question type </label>
						<div class="form-group">
							<select name="question_type_id" class="form-control selectpicker" data-show-subtext="true" data-live-search="true">
								<option value="">Select Question type</option>
								<?php 
								foreach($all_question_types as $qt)
								{
									$selected = ($qt['question_type_id'] == $static_page['question_type_id']) ? ' selected="selected"' : "";

									echo '<option value="'.$qt['question_type_id'].'" '.$selected.'>'.$qt['type_name'].'</option>';
								} 
								?>
							</select>
							<span class="text-danger"><?php echo form_error('question_type_id');?></span>
						</div>
					</div>

					<div class="col-md-6">
						<label for="category_id" class="control-label"><span class="text-danger">*</span>Template type </label>
						<div class="form-group">
							<select name="template_type" class="form-control selectpicker" data-show-subtext="true" data-live-search="true">
								<option value="">Select Template</option>
								<?php 
								foreach($Spage_templates as $template)
								{
									$selected = ($template == $static_page['template_type']) ? ' selected="selected"' : "";

									echo '<option value="'.$template.'" '.$selected.'>'.$template.' | '.$template.'</option>';
								} 
								?>
							</select>
							<span class="text-danger"><?php echo form_error('template_type');?></span>
						</div>
					</div>
					
					<div class="col-md-6">
						<label for="title" class="control-label"><span class="text-danger">*</span>Title</label>
						<div class="form-group">
							<input type="text" name="title" value="<?php echo ($this->input->post('title') ? $this->input->post('title') : $static_page['title']); ?>" class="form-control" id="title" maxlength="100"/>
							<span class="text-danger"><?php echo form_error('title');?></span>
						</div>
					</div>

					<!-- <div class="col-md-4">
						<label for="icon" class="control-label">Icon URL</label>
						[
						<span class="text-danger"><a href="<?php echo site_url('gallery/add');?>" target="_blank">
							<?php echo GALLERY_URL_LABEL;?></a>
						</span>
						]&nbsp;
						[
						<span class="text-danger"><a href="<?php echo site_url('gallery/index');?>" target="_blank">
							<?php echo GALLERY_URL_LABEL_LIST;?></a>
						</span>
						]
						<div class="form-group has-feedback">
							<input type="url" name="icon" value="<?php echo ($this->input->post('icon') ? $this->input->post('icon') : $static_page['icon']); ?>" class="form-control" id="icon"/>
							<span class="glyphicon glyphicon-link form-control-feedback"></span>
							<span class="text-danger"><?php echo form_error('icon');?></span>
						</div>
					</div> -->								

					<div class="col-md-6">
						<label for="audio" class="control-label">Audio URL</label>
						[
						<span class="text-danger"><a href="<?php echo site_url('gallery/add');?>" target="_blank">
							<?php echo GALLERY_URL_LABEL;?></a>
						</span>
						]&nbsp;
						[
						<span class="text-danger"><a href="<?php echo site_url('gallery/index');?>" target="_blank">
							<?php echo GALLERY_URL_LABEL_LIST;?></a>
						</span>
						]
						<div class="form-group has-feedback">
							<input type="url" name="audio" value="<?php echo ($this->input->post('audio') ? $this->input->post('audio') : $static_page['audio']); ?>" class="form-control" id="audio"/>
							<span class="glyphicon glyphicon-link form-control-feedback"></span>
							<span class="text-danger"><?php echo form_error('audio');?></span>
						</div>
					</div>

					<div class="col-md-6">
						<label for="video" class="control-label">Video</label>
						<div class="form-group has-feedback">
							<input type="file" name="video" value="<?php echo ($this->input->post('video') ? $this->input->post('video') : $static_page['video']); ?>" class="form-control" id="video" />
							<span class="glyphicon glyphicon-facetime-video form-control-feedback"></span>
							<span>
								<?php 
								if(isset($static_page['video'])){      
                                    echo '<span>
                                            <a href="'.$static_page['video'].'" target="_blank">'.OPEN_FILE.'</a>
                                        </span>';
                                }else{
                                    echo NO_FILE;
                                }
                                ?>
							</span>
						</div>
						
					</div>

					<div class="col-md-6">
						<label for="image" class="control-label">Image</label>
						<?php echo STATIC_PAGE_ALLOWED_TYPES_LABEL;?>
						<div class="form-group">
							<input type="file" name="image" value="<?php echo ($this->input->post('image') ? $this->input->post('image') : $static_page['image']); ?>" class="form-control" id="image" />
							<span>
								<?php 
								if(isset($static_page['image'])){      
                                    echo '<span>
                                            <a href="'.$static_page['image'].'" target="_blank">'.OPEN_FILE.'</a>
                                        </span>';
                                }else{
                                    echo NO_FILE;
                                }
                                ?>
							</span>
						</div>						
					</div>		

					<div class="col-md-6">
						<div class="form-group">
							<label for="active" class="control-label">Active</label>
							<input type="checkbox" name="active" value="1" <?php echo ($static_page['active']==1 ? 'checked="checked"' : ''); ?> id='active' />	
						</div>
					</div>

					<div class="col-md-12">
						<label for="contents" class="control-label"><span class="text-danger">*</span>Contents <code title="Place this text to show child">[child_location]</code></label> 
						<div class="form-group has-feedback">
							<textarea name="contents" class="form-control myckeditor" id="contents"><?php echo ($this->input->post('contents') ? $this->input->post('contents') : $static_page['contents']); ?></textarea>
							<span class="glyphicon glyphicon-text-size form-control-feedback"></span>
							<span class="text-danger"><?php echo form_error('contents');?></span>
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
            	<button type="submit" class="btn btn-success">
					<i class="fa fa-check"></i> Save
				</button>
	        </div>				
			<?php echo form_close(); ?>
		</div>
    </div>
</div>
