<input class="form-control" type="text" placeholder="Search" aria-label="Search" id="myInput">
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header bg-danger">
                
                <h3 class="box-title text-primary"><?php echo $title;?></h3>
                <div class="box-tools">
                    <a href="<?php echo site_url('static_page/add'); ?>" class="btn btn-success btn-sm">Add</a> 
                </div>
                <?php echo $this->session->flashdata('flsh_msg');?>
            </div>
           
            <div class="box-body table-responsive">
                <table class="table table-striped table-bordered table-sm">
                    <thead>
                    <tr>
                        <th><?php echo SR;?></th>
                        <th><?php echo ACTION;?></th>
                        <th>Program<?php echo SEP;?>Category</th>
                        <th>Title</th>
                        <th>Image</th>
                        <th>Video</th>
                        <th>Contents</th>
                       <th><?php echo STATUS;?></th>
                        
                    </tr>
                    </thead>
                    <tbody id="myTable">
                    <?php $sr=0; foreach($static_pages as $s){ $zero=0;$one=1;$pk='static_page_id'; $table='static_pages';$sr++; ?>
                    <tr>
                        <td><?php echo $sr; ?></td>
                        <td>
                            <?php if($s['test_module_name']==IELTS 
                            and ($s['category_id']==44 or $s['category_id']==45 or $s['category_id']==46 or $s['category_id']==47)){ ?>
                            <a href="<?php echo site_url('static_page/index/'.$s['static_page_id']); ?>" class="btn btn-info btn-xs" data-toggle="tooltip" title="View Child"><span class="fa fa-child"></span> </a>
                        <?php } elseif($s['test_module_name']==IELTS 
                            and ($s['category_id']==48 or $s['category_id']==49 or $s['category_id']==50 or $s['category_id']==51)){ ?>

                            <a href="<?php echo site_url('static_page/index_gt/'.$s['static_page_id']); ?>" class="btn btn-info btn-xs" data-toggle="tooltip" title="View Child"><span class="fa fa-child"></span> </a>

                        <?php }else{ ?>
                            <a href="<?php echo site_url('static_page/index_ee/'.$s['static_page_id']); ?>" class="btn btn-info btn-xs" data-toggle="tooltip" title="View Child"><span class="fa fa-child"></span> </a>                           
                        <?php } ?>
						
						<a href="<?php echo site_url('static_page/view/'.$s['static_page_id']); ?>" target = '_blank' class="btn btn-primary btn-xs" data-toggle="tooltip" title="View page"><span class="fa fa-eye"></span></a>

						
                        <a href="<?php echo site_url('static_page/edit/'.$s['static_page_id']); ?>" class="btn btn-info btn-xs" data-toggle="tooltip" title="Edit"><span class="fa fa-pencil"></span> </a> 
                            <a href="<?php echo site_url('static_page/remove/'.$s['static_page_id']); ?>" class="btn btn-danger btn-xs" data-toggle="tooltip" title="Delete" onclick="return confirm('<?php echo DELETE_CONFIRM?>');"><span class="fa fa-trash"></span> </a>
                        </td>
                       
                        <td><?php echo $s['programe_name'].SEP.$s['category_name']; ?></td>
                        <td><?php echo $s['title']; ?></td>
                       <td>
                            <?php 
                                if(isset($s['image'])){      
                                    echo '<span>
                                            <a href="'.$s['image'].'" target="_blank">'.OPEN_FILE.'</a>
                                        </span>';
                                }else{
                                    echo NO_FILE;
                                }                                
                            ?>   
                        </td>                       
                        <td>
                            <?php 
                                if(isset($s['video'])){      
                                    echo '<span>
                                            <a href="'.$s['video'].'" target="_blank">'.OPEN_FILE.'</a>
                                        </span>';
                                }else{
                                    echo NO_FILE;
                                }                                
                            ?>   
                        </td>

                        <td></td>
                        
                        <td>
                            <?php 
                            if($s['active']==1){
                                echo '<span class="text-success"><a href="javascript:void(0);" id='.$s['static_page_id'].' data-toggle="tooltip" title="Click to De-activate" onclick=activate_deactivete('.$s['static_page_id'].','.$zero.',"'.$table.'","'.$pk.'") >'.ACTIVE.'</a></span>';
                            }else{
                                echo '<span class="text-danger"><a href="javascript:void(0);" id='.$s['static_page_id'].' title="Click to Activate" onclick=activate_deactivete('.$s['static_page_id'].','.$one.',"'.$table.'","'.$pk.'") >'.DEACTIVE.'</a></span>';
                            }
                            ?>                                
                        </td>     
                        
                    </tr>

                    <?php } ?>
                </tbody>
                </table>
                <div class="pull-right">
                    <?php echo $this->pagination->create_links(); ?>                    
                </div>                
            </div>
        </div>
    </div>
</div>
