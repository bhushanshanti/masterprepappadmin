<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Add Program</h3>
            </div>
            <?php echo $this->session->flashdata('flsh_msg'); ?>
            <?php echo form_open('programe_master/add'); ?>
          	<div class="box-body">
          		<div class="row clearfix">
                <div class="col-md-6">
            <label for="programe_name" class="control-label"><span class="text-danger">*</span>Program Name</label>
            <div class="form-group">
              <input type="text" name="programe_name" value="<?php echo $this->input->post('programe_name'); ?>" class="form-control" id="programe_name" maxlength="30"/>
              <span class="text-danger"><?php echo form_error('programe_name');?></span>
              <span class="text-danger"><?php echo $this->session->flashdata('flsh_msg'); ?></span>
            </div>
          </div>
					<div class="col-md-12">
						<div class="form-group">
							<input type="checkbox" name="active" value="1"  id="active" checked="checked"/>
							<label for="active" class="control-label">Active</label>
						</div>
					</div>
					
				</div>
			</div>
          	<div class="box-footer">
            	<button type="submit" class="btn btn-success">
            		<i class="fa fa-check"></i> Save
            	</button>
          	</div>
            <?php echo form_close(); ?>
      	</div>
    </div>
</div>