<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title"> Edit Notification Message</h3>
            </div>
            <?php echo $this->session->flashdata('flsh_msg'); ?>
			<?php echo form_open('notification_message/edit/'.$notification_message['messege_id']); ?>
			<div class="box-body">
				<div class="row clearfix">					
					
					<div class="col-md-6">
						<label for="subject_id" class="control-label"><span class="text-danger">*</span>Subject</label>
						<div class="form-group">
							<select name="subject_id" class="form-control selectpicker" data-show-subtext="true" data-live-search="true">
								<option value="">Select Subject</option>
								<?php 
								foreach($all_subject_list as $p)
								{
									$selected = ($p['id'] == $notification_message['subject_id']) ? ' selected="selected"' : "";

									echo '<option value="'.$p['id'].'" '.$selected.'>'.$p['subject'].'</option>';
								} 
								?>
							</select>
							<span class="text-danger"><?php echo form_error('subject_id');?></span>
						</div>
					</div>

					<div class="col-md-6">
						<label for="message" class="control-label"><span class="text-danger">*</span>Notification Message</label>
						<div class="form-group has-feedback">
							<input type="text" name="message" value="<?php echo ($this->input->post('message') ? $this->input->post('message') : $notification_message['message']); ?>" class="form-control" id="message" />							
							<span class="text-danger"><?php echo form_error('message');?></span>
							
						</div>
					</div>
					
					
					<div class="col-md-12">
						<div class="form-group">
							<input type="checkbox" name="active" value="1" <?php echo ($notification_message['active']==1 ? 'checked="checked"' : ''); ?> id='active' />
							<label for="active" class="control-label">Active</label>
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
            	<button type="submit" class="btn btn-success">
					<i class="fa fa-check"></i> Save
				</button>
	        </div>				
			<?php echo form_close(); ?>
		</div>
    </div>
</div>