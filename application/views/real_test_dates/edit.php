<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Edit <?php echo TS_EE;?></h3>
            </div>
            <?php echo $this->session->flashdata('flsh_msg');?>
			<?php echo form_open('Real_test_dates/edit/'.$Real_test_dates['realTestDatesId']); ?>
			<div class="box-body">
				<div class="row clearfix">	

					<div class="col-md-4">
						<label for="type" class="control-label"><span class="text-danger">*</span>Type</label>
						<div class="form-group">
							<select name="type" class="form-control selectpicker" data-show-subtext="true" data-live-search="true" onchange="checkdateurl_edit(this.value)">
								<?php 
								if($Real_test_dates['type']==1){
								?>
								<option value="">Select</option>
								<option value="1" selected="selected">CD based Test</option>
								<option value="2">Paper based Test</option>
								<?php }elseif($Real_test_dates['type']==2){ ?>
									<option value="">Select</option>
								<option value="1">CD based Test</option>
								<option value="2" selected="selected">Paper based Test</option>
								<?php }else{ ?>
									<option value="" selected="selected">Select</option>
								<option value="1">CD based Test</option>
								<option value="2">Paper based Test</option>
							<?php } ?>
							</select>
							<span class="text-danger"><?php echo form_error('type');?></span>
						</div>
					</div>				
					<?php 
						if($Real_test_dates['testDate']){
					?>
					<div class="col-md-4 td">
						<label for="testDate" class="control-label"><span class="text-danger">*</span>Real test date</label>
						<div class="form-group has-feedback">
							<input type="text" id="testDate" name="testDate" value="<?php echo ($this->input->post('testDate') ? $this->input->post('testDate') : $Real_test_dates['testDate']); ?>" class="form-control has-datepicker" id="testDate" maxlength="10"/>
							<span class="glyphicon glyphicon-calendar form-control-feedback"></span>
							<span class="text-danger"><?php echo form_error('testDate');?></span>
							<span class="text-danger"><?php echo $this->session->flashdata('flsh_msg'); ?></span>
						</div>
					</div>
					<?php } ?>

					<?php 
						if($Real_test_dates['test_url']){
					?>
					<div class="col-md-4 tu">
						<label for="test_url" class="control-label"><span class="text-danger">*</span>Test URL</label>
						<div class="form-group has-feedback">
							<input type="text" id="test_url" name="test_url" value="<?php echo ($this->input->post('test_url') ? $this->input->post('test_url') : $Real_test_dates['test_url']); ?>" class="form-control" id="test_url" />
							<span class="glyphicon glyphicon-link form-control-feedback"></span>
							<span class="text-danger"><?php echo form_error('test_url');?></span>						
						</div>
					</div>	
					<?php } ?>			

					<div class="col-md-4">
						<label for="programe_id" class="control-label"><span class="text-danger">*</span>Program</label>
						<div class="form-group">
							<select name="programe_id" class="form-control selectpicker" data-show-subtext="true" data-live-search="true">
								<option data-subtext="" value="">Select Program</option>
								<?php 
								foreach($all_programe_masters as $p)
								{
									$selected = ($p['programe_id'] == $Real_test_dates['programe_id']) ? ' selected="selected"' : "";

									echo '<option data-subtext="'.$p['programe_name'].'" value="'.$p['programe_id'].'" '.$selected.'>'.$p['programe_name'].'</option>';
								} 
								?>
							</select>
							<span class="text-danger"><?php echo form_error('programe_id');?></span>
						</div>
					</div>			

					<div class="col-md-4">
						<label for="test_module_id" class="control-label"><span class="text-danger">*</span>Test module</label>
						<div class="form-group">
							<select name="test_module_id" class="form-control selectpicker" data-show-subtext="true" data-live-search="true">
								<option data-subtext="" value="">Select </option>
								<?php 
								foreach($all_test_module as $t)
								{
									$selected = ($t['test_module_id'] == $Real_test_dates['test_module_id']) ? ' selected="selected"' : "";

									echo '<option data-subtext="'.$t['test_module_name'].'" value="'.$t['test_module_id'].'" '.$selected.'>'.$t['test_module_name'].'</option>';
								} 
								?>
							</select>
							<span class="text-danger"><?php echo form_error('test_module_id');?></span>
						</div>
					</div>

					<div class="col-md-4">
						<label for="country_id" class="control-label"><span class="text-danger">*</span>Country</label>
						<div class="form-group">
							<select name="country_id" class="form-control selectpicker" data-show-subtext="true" data-live-search="true" onchange="get_state_list(this.value)">
								<option  data-subtext="" value="">Select country</option>
								<?php 
								foreach($all_country_list as $p)
								{	
									
									$selected = ($p['country_id'] == $Real_test_dates['country_id']) ? ' selected="selected"' : "";
									echo '<option data-subtext="'.$p['name'].'" value="'.$p['country_id'].'" '.$selected.'>'.$p['name'].'</option>';
								} 
								?>
							</select>
							<span class="text-danger"><?php echo form_error('country_id');?></span>
						</div>
					</div>					

					<div class="col-md-4">
						<div class="form-group">
							<input type="checkbox" name="active" value="1" <?php echo ($Real_test_dates['active']==1 ? 'checked="checked"' : ''); ?> id='active' />
							<label for="active" class="control-label">Active</label>
						</div>
					</div>

				</div>
			</div>
			<div class="box-footer">
				
            	<button type="submit" class="btn btn-success">
					<i class="fa fa-check"></i> Save
				</button>
	        </div>				
			<?php echo form_close(); ?>
		</div>
    </div>
</div>

<script type="text/javascript">
	
	function checkdateurl_edit(val){

		//alert(val)
		if(val==1){		

			$('.td').hide();
			$('.tu').show();
		}else{

			$('.td').show();
			$('.tu').hide();
		}
	}
</script>
