<script type="text/javascript" src="<?php echo base_url('resources/js/jquery-3.2.1.js');?>"></script>
<input class="form-control" type="text" placeholder="Search" aria-label="Search" id="myInput">
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header bg-danger">
                <h3 class="box-title text-primary"><?php echo $title;?></h3>
                <div class="box-tools">
                    <a href="<?php echo site_url('Real_test_dates/add'); ?>" class="btn btn-success btn-sm">Add</a>
                </div>
                <?php echo $this->session->flashdata('flsh_msg');?>
            </div>
            <div class="box-body table-responsive">
                <table class="table table-striped table-bordered table-sm">
                    <thead>
                        <tr>     
                        <th><?php echo SR;?></th>             
                        <th>Test dates</th>
                        <th>URL</th> 
                        <th>Program</th>
                        <th>Test Module</th>
                        <th>Country</th>
                        <th>Type</th>                        
                        <th><?php echo STATUS;?></th>
                        <th><?php echo ACTION;?></th>
                    </tr>
                    </thead>
                    <tbody id="myTable">
                    <?php $sr=0; foreach($Real_test_dates as $t){ $zero=0;$one=1;$pk='realTestDatesId'; $table='real_test_dates';$sr++; ?>

                    <tr> 
                        <td><?php echo $sr; ?></td>
                        <td><?php echo $t['testDate'];?></td>
                        <td><?php echo $t['test_url'];?></td>
                        <td><?php echo $t['programe_name'];?></td>
                        <td><?php echo $t['test_module_name'];?></td>
                        <td><?php echo $t['name'];?></td>
                        <td>
                            <?php 
                            if($t['type']==1){
                                echo 'CD based Test';
                            }else{
                                echo 'Paper based Test';
                            }
                            ?>                                
                        </td>
                       
                        <td>
                            <?php 
                            if($t['active']==1){
                                echo '<span class="text-success"><a href="javascript:void(0);" id='.$t['realTestDatesId'].' data-toggle="tooltip" title="Click to De-activate" onclick=activate_deactivete('.$t['realTestDatesId'].','.$zero.',"'.$table.'","'.$pk.'") >'.ACTIVE.'</a></span>';
                            }else{
                                echo '<span class="text-danger"><a href="javascript:void(0);" id='.$t['realTestDatesId'].' title="Click to Activate" onclick=activate_deactivete('.$t['realTestDatesId'].','.$one.',"'.$table.'","'.$pk.'") >'.DEACTIVE.'</a></span>';
                            }
                            ?>                                 
                        </td> 
                        <td>
                            <a href="<?php echo site_url('Real_test_dates/edit/'.$t['realTestDatesId']); ?>" class="btn btn-info btn-xs" data-toggle="tooltip" title="Edit dates"><span class="fa fa-pencil"></span></a>   
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
                </table>
                <div class="pull-right">
                    <?php echo $this->pagination->create_links(); ?>
                </div>                
            </div>
        </div>
    </div>
</div>
<!-- list view of all test series ends-->

