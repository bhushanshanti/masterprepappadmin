<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo $title; ?></title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="icon" href="<?php echo site_url('resources/img/favicon.png');?>" type="image/gif">
        <link rel="stylesheet" href="<?php echo site_url('resources/css/bootstrap-select.min.css');?>">
        <link rel="stylesheet" href="<?php echo site_url('resources/css/bootstrap.min.css');?>">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?php echo site_url('resources/css/font-awesome.min.css');?>">
        <link rel="stylesheet" href="<?php echo site_url('resources/css/ionicons.min.css');?>">       
        <!-- Datetimepicker -->
        <link rel="stylesheet" href="<?php echo site_url('resources/css/bootstrap-datetimepicker.min.css');?>">
        <link rel="stylesheet" href="<?php echo site_url('resources/css/AdminLTE.min.css');?>">       
        <link rel="stylesheet" href="<?php echo site_url('resources/css/_all-skins.min.css');?>">
        <link rel="stylesheet" href="<?php echo site_url('resources/css/daterangepicker.css');?>">
    </head>
   
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <header class="main-header">
                <!-- Logo -->
                <a href="<?php echo site_url('dashboard');?>" class="logo" data-toggle="tooltip" data-placement="bottom" title="Go to dashboard">                   
                    <span class="logo-mini"><img src=<?php echo site_url('resources/img/masterprep_logo.svg');?> /></span>
                    <span class="logo-lg"><img src=<?php echo site_url('resources/img/masterprep_logo.svg');?> /></span>                   
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a> 

                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                        <!-- User Account: style can be found in dropdown.less -->
                            <?php    
                                                                
                                $data = $this->session->userdata('admin_login_data');
                                foreach ($data as $d) {
									$user_id=$d->id;
                                    $role_id=$d->roleid;
                                    $role_name=$d->name;
                                   $unread_notifictaions =  $this->Notification_model->count_unread_notifications($user_id);
                                   if($unread_notifictaions > 0)
                                   {
									   $active_class = "active";
									   $active_count = "<span class='counter counter-sm'>$unread_notifictaions</span>";
									}else{
										$active_class = "";
										$active_count = "";
										}      
                            ?>
                            <li class="user user-menu cnm-notifications <?= $active_class ?>">
                                <a href="<?= site_url("notification") ?>" class="" data-placement="bottom" title="Notifications">                                   
                                    <i class="fa fa-bell"></i><?= $active_count ?>                                  
                                </a>
                                
                            </li>
                            
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-placement="bottom" title="Click here">                                   
                                    <i class="fa fa-user"></i>
                                    <span class="hidden-xs"><?php echo $d->name;?></span>
                                </a>
                                <ul class="dropdown-menu">         
                                    <li class="user-header">             
                                       <i class="fa fa-user text-danger"></i>
                                    <p>                                        
                                        <?php 
                                            echo 'Hi '.$d->fname;
                                            $date=date_create($d->created);
                                            $Member_since = date_format($date,"M d, Y");
                                        ?>
                                        <small>Member since <i><?php echo $Member_since;?></i></small>
                                        <small>Role <i><?php echo $d->name;?></i></small>
                                    </p>
                                    </li>
                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-left">
                                            <a href="<?php echo site_url('user/profile');?>" class="btn btn-default btn-flat"><i class="fa fa-user"></i> Profile</a>
                                        </div>
                                        <div class="pull-right">
                                        <a href="<?php echo site_url('Login/logout');?>" class="btn btn-default btn-flat"><i class="fa fa-sign-out"></i> Sign out</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <?php } ?>
                        </ul>
                    </div>
                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
            <!-- <aside class="main-sidebar" style="background-color: #153B6A !important"> -->
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                   
                    <ul class="sidebar-menu">
                       <!--  <li class="header">MAIN NAVIGATION</li> -->
                        <?php if($role_name=='Admin'){ ?>
                        <li>
                            <a href="#">
                                <i class="fa fa-gavel"></i> <span>Admin&Role</span>
                            </a>
                            <ul class="treeview-menu">
                                <li class="active">
                                    <a href="<?php echo site_url('user/add');?>"><i class="fa fa-plus"></i> Add admin</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('user/index');?>"><i class="fa fa-list-ul"></i> Admin List</a>
                                </li>
                                <li >
                                    <a href="<?php echo site_url('role/add');?>"><i class="fa fa-plus"></i> Add Role</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('role/index');?>"><i class="fa fa-list-ul"></i> Role List</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('role/manage_controller');?>"><i class="fa fa-list-ul"></i> Manage controllers</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('role/manage_controller_method');?>"><i class="fa fa-list-ul"></i> Manage methods</a>
                                </li>

                                <li>
                                    <a href="<?php echo site_url('Db/db_backup_mp');?>"><i class="fa fa-list-ul"></i> DB backup</a>
                                </li>                                
                            </ul>
                        </li> 

                        <li>
                            <a href="#">
                                <i class="glyphicon glyphicon-th"></i><span>Prog/Test </span>
                            </a>
                            <ul class="treeview-menu">
                                <li class="active">
                                    <a href="<?php echo site_url('programe_master/add');?>"><i class="fa fa-plus"></i> Add programe</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('programe_master/index');?>"><i class="fa fa-list-ul"></i> Programe List</a>
                                </li>
                                
                                <li class="active">
                                    <a href="<?php echo site_url('test_module/add');?>"><i class="fa fa-plus"></i> Add Test Module</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('test_module/index');?>"><i class="fa fa-list-ul"></i> Test Module List</a>
                                </li>
                            </ul>
                        </li>

                        <li>
                            <a href="#">
                                <i class="glyphicon glyphicon-th"></i> <span>Category</span>
                            </a>
                            <ul class="treeview-menu">
                                <li class="active">
                                    <a href="<?php echo site_url('category_master/add');?>"><i class="fa fa-plus"></i> Add</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('category_master/index');?>"><i class="fa fa-list-ul"></i> List (IELTS)</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('category_master/index_ee');?>"><i class="fa fa-list-ul"></i> List (EE) </a>
                                </li>
                                
                            </ul>
                        </li>                        
                       
                        <li>
                            <a href="#">
                                <i class="glyphicon glyphicon-th"></i> <span>Question Type&Behavior</span>
                            </a>
                            <ul class="treeview-menu">
                                <li class="active">
                                    <a href="<?php echo site_url('question_type/add');?>"><i class="fa fa-plus"></i> Add type</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('question_type/index');?>"><i class="fa fa-list-ul"></i> Type List</a>
                                </li>

                                <li class="active">
                                    <a href="<?php echo site_url('question_behavior/add');?>"><i class="fa fa-plus"></i> Add behavior</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('question_behavior/index');?>"><i class="fa fa-list-ul"></i> Behavior List</a>
                                </li>
                            </ul>
                        </li>
                        
                        

                        <?php } ?>



						<li>
                            <a href="#">
                                <i class="fa fa-file-o"></i> <span>Previous test</span>
                            </a>
                            <ul class="treeview-menu">
                                <li class="active">
                                    <a href="<?php echo site_url('Previous_Ielts_tests/add');?>"><i class="fa fa-plus"></i> Add</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('Previous_Ielts_tests/index');?>"><i class="fa fa-list-ul"></i> IELTS(ACD)-List</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('Previous_Ielts_tests/index_gt');?>"><i class="fa fa-list-ul"></i> IELTS(GT)-List</a>
                                </li>
                               
                            </ul>
                        </li>

                        <li>
                            <a href="#">
                            <i class="fa fa-image"></i> <span>Gallery</span>
                            </a>
                            <ul class="treeview-menu">
                                <li class="active">
                                    <a href="<?php echo site_url('gallery/add');?>"><i class="fa fa-plus"></i> Add</a>
                                </li>
                                <li>
                                <a href="<?php echo site_url('gallery/index');?>"><i class="fa fa-list-ul"></i> List</a>
                                </li>
                            </ul>
                        </li>

						<li>
                            <a href="#">
                            <i class="fa fa-mobile"></i> <span>Advertisement</span>
                            </a>
                            <ul class="treeview-menu">
                                <li class="active">
                                    <a href="<?php echo site_url('advertisement/add');?>"><i class="fa fa-plus"></i> Add</a>
                                </li>
                                <li>
                                <a href="<?php echo site_url('advertisement/index');?>"><i class="fa fa-list-ul"></i> List</a>
                                </li>
                            </ul>
                        </li>
                        </li>

						<li>
                            <a href="#">
                            <i class="fa fa-mobile"></i> <span>App slides</span>
                            </a>
                            <ul class="treeview-menu">
                                <li class="active">
                                    <a href="<?php echo site_url('app_slides/add');?>"><i class="fa fa-plus"></i> Add</a>
                                </li>
                                <li>
                                <a href="<?php echo site_url('app_slides/index');?>"><i class="fa fa-list-ul"></i> List</a>
                                </li>
                            </ul>
                        </li>

                        <li>
                            <a href="#">
                                <i class="fa fa-users"></i> <span>Students </span>
                            </a>
                            <ul class="treeview-menu">
                                <li class="active">
                                    <a href="<?php echo site_url('student/add');?>"><i class="fa fa-plus"></i> Add</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('student/index');?>"><i class="fa fa-list-ul"></i> List (Acad)</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('student/index_gt');?>"><i class="fa fa-list-ul"></i> List (GT)</a>
                                </li>
                            </ul>
                        </li>                       

                        <li>
                            <a href="#">
                                <i class="fa fa-rupee"></i> <span>Transaction</span>
                            </a>
                            <ul class="treeview-menu">
                                <li class="active">
                                    <a href="<?php echo site_url('Package_transaction/transaction_acd');?>"><i class="fa fa-list"></i> ACD</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('Package_transaction/transaction_gt');?>"><i class="fa fa-list-ul"></i> GT</a>
                                </li>
                            </ul>
                        </li>                        

                        <li>
                            <a href="#">
                                <i class="fa fa-book"></i> <span>Test Papers-IELTS</span>
                            </a>
                            <ul class="treeview-menu">
                                
                                <li class="active">
                                    <a href="<?php echo site_url('test_seriese/index');?>"><i class="fa fa-list-ul"></i> <?php echo ACD;?></a>
                                </li>
                                <li class="active">
                                    <a href="<?php echo site_url('test_seriese/index_gt');?>"><i class="fa fa-list-ul"></i> <?php echo GT;?></a>
                                </li>
                                
                                <li>
                                    <a href="<?php echo site_url('test_seriese/index_et');?>"><i class="fa fa-list-ul"></i> <?php echo ET.'-'.'ACD';?></a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('test_seriese/index_et_gt');?>"><i class="fa fa-list-ul"></i> <?php echo ET.'-'.'GT';?></a>
                                </li>

                                <li>
                                    <a href="<?php echo site_url('test_seriese/index_mt');?>"><i class="fa fa-list-ul"></i> <?php echo MOCK.'-'.'ACD';?></a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('test_seriese/index_mt_gt');?>"><i class="fa fa-list-ul"></i> <?php echo MOCK.'-'.'GT';?></a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('test_seriese/index_sp');?>"><i class="fa fa-list-ul"></i> <?php echo 'Static test';?></a>
                                </li>                                
                            </ul>
                        </li>

                        <li>
                            <a href="#">
                                <i class="fa fa-book"></i> <span>Test Papers-English </span>
                            </a>
                            <ul class="treeview-menu">
                                <li>
                                    <a href="<?php echo site_url('test_seriese/english_essential');?>"><i class="fa fa-list-ul"></i> <?php echo ENGLISH_ESSENTIAL;?></a>
                                </li>                                
                            </ul>
                        </li>

                        <li>
                            <a href="#">
                                <i class="fa fa-paperclip"></i> <span>Test Association</span>
                            </a>
                            <ul class="treeview-menu">  
                                
                                <li>
                                    <a href="<?php echo site_url('ts_cat_assoc/add');?>"><i class="fa fa-plus"></i> Add Association</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('ts_cat_assoc/index');?>"><i class="fa fa-list-ul"></i> Association List IELTS-ACD</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('ts_cat_assoc/index_gt');?>"><i class="fa fa-list-ul"></i> Association List IELTS-GT</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('ts_cat_assoc/index_ee');?>"><i class="fa fa-list-ul"></i> Association List EE</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('ts_cat_assoc/index_et');?>"><i class="fa fa-list-ul"></i> Association List ET</a>
                                </li>

                                <li>
                                    <a href="<?php echo site_url('ts_cat_assoc/index_sptest');?>"><i class="fa fa-list-ul"></i> Association List SP test</a>
                                </li>
                            </ul>
                        </li>

                        <li>
                            <a href="#">
                                <i class="fa fa-file-o"></i> <span>Static Pages </span>
                            </a>
                            <ul class="treeview-menu">
                                <li class="active">
                                    <a href="<?php echo site_url('static_page/add');?>"><i class="fa fa-plus"></i> Add</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('static_page/index');?>"><i class="fa fa-list-ul"></i> IELTS(ACD)-List</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('static_page/index_gt');?>"><i class="fa fa-list-ul"></i> IELTS(GT)-List</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('static_page/index_ee');?>"><i class="fa fa-list-ul"></i> EE-List</a>
                                </li>
                            </ul>
                        </li>

                        <li>
                            <a href="#">
                                <i class="fa fa-video-camera"></i> <span>Live Lectures </span>
                            </a>
                            <ul class="treeview-menu">
                                <li class="active">
                                    <a href="<?php echo site_url('live_lecture/add');?>"><i class="fa fa-plus"></i> Add</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('live_lecture/index');?>"><i class="fa fa-list-ul"></i> List ACD</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('live_lecture/index_gt');?>"><i class="fa fa-list-ul"></i> List GT</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('live_lecture/index_ee');?>"><i class="fa fa-list-ul"></i> List EE</a>
                                </li>
                            </ul>
                        </li> 

                        <li>
                            <a href="#">
                                <i class="fa fa-star-half-o"></i> <span>Band Score </span>
                            </a>
                            <ul class="treeview-menu">
                                <li class="active">
                                    <a href="<?php echo site_url('band_score/add');?>"><i class="fa fa-plus"></i> Add</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('band_score/index');?>"><i class="fa fa-list-ul"></i> List (ACD)</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('band_score/index_gt');?>"><i class="fa fa-list-ul"></i> List (GT)</a>
                                </li>
                            </ul>
                        </li>            

                        <li>
                            <a href="#">
                                <i class="fa fa-briefcase"></i> <span>Packages </span>
                            </a>
                            <ul class="treeview-menu">
                                <li class="active">
                                    <a href="<?php echo site_url('package_master/add');?>"><i class="fa fa-plus"></i> Add</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('package_master/index');?>"><i class="fa fa-list-ul"></i> List (Academic)</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('package_master/index_gt');?>"><i class="fa fa-list-ul"></i> List (GT)</a>
                                </li>
                            </ul>
                        </li>

                        <li>
                            <a href="#">
                                <i class="fa fa-bell"></i> <span>Notification</span>
                            </a>
                            <ul class="treeview-menu">                                
                                <li class="active">
                                    <a href="<?php echo site_url('notification_subject/add');?>"><i class="fa fa-plus"></i> Add Notification Subject</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('notification_subject/index');?>"><i class="fa fa-list"></i> List- Notification Subject</a> 
                                </li>
                                <li class="active">
                                    <a href="<?php echo site_url('notification_message/add');?>"><i class="fa fa-plus"></i> Add Notification Message</a>
                                </li>
                                <li class="active">
                                    <a href="<?php echo site_url('notification_message/index');?>"><i class="fa fa-list"></i> List- Notification Message</a>
                                </li>                                
                            </ul>
                        </li>

                        <li>
                            <a href="#">
                                <i class="fa fa-calendar"></i> <span>Test Dates(Goal) </span>
                            </a>
                            <ul class="treeview-menu">    
                                <li>
                                    <a href="<?php echo site_url('Real_test_dates/index');?>"><i class="fa fa-list-ul"></i> List</a>
                                </li>                                
                            </ul>
                        </li>

                        <li>
                            <a href="#">
                                <i class="fa fa-lightbulb-o"></i> <span>Tips & Tricks </span>
                            </a>
                            <ul class="treeview-menu">

                                <li>
                                    <a href="<?php echo site_url('Tips_tricks_master/add');?>"><i class="fa fa-plus"></i> Add Master</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('Tips_tricks_master/index');?>"><i class="fa fa-list-ul"></i> Master List</a>
                                </li>    
                                <li>
                                    <a href="<?php echo site_url('Tips_tricks/index');?>"><i class="fa fa-list-ul"></i> List(QA)</a>
                                </li>                                
                            </ul>
                        </li>
                        </li>

                        <li>
                            <a href="#">
                                <i class="fa fa-lightbulb-o"></i> <span>FAQ </span>
                            </a>
                            <ul class="treeview-menu">

                                <li>
                                    <a href="<?php echo site_url('faq_master/add');?>"><i class="fa fa-plus"></i> Add Master</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('faq_master/index');?>"><i class="fa fa-list-ul"></i> Master List</a>
                                </li>    
                                <li>
                                    <a href="<?php echo site_url('faqs/index');?>"><i class="fa fa-list-ul"></i> List(QA)</a>
                                </li>                                
                            </ul>
                        </li>

                        <li>
                            <a href="#">
                                <i class="fa fa-hand-o-right"></i> <span>Instructions </span>
                            </a>
                            <ul class="treeview-menu">    
                                <li class="active">
                                    <a href="<?php echo site_url('instruction_master/add');?>"><i class="fa fa-plus"></i> Add Instructions</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('instruction_master/index');?>"><i class="fa fa-list-ul"></i> Instructions List</a>
                                </li>                                
                            </ul>
                        </li> 

                        <li>
                            <a href="#">
                                <i class="fa fa-map-marker"></i> <span>Locations</span>
                            </a>
                            <ul class="treeview-menu">                                
                               <li class="active">
                                    <a href="<?php echo site_url('country/add');?>"><i class="fa fa-plus"></i> Add country</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('country/index');?>"><i class="fa fa-list-ul"></i> Country List</a>
                                </li>

                                <li>
                                    <a href="<?php echo site_url('state/add');?>"><i class="fa fa-plus"></i> Add state</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('state/index');?>"><i class="fa fa-list-ul"></i> State List</a>
                                </li>

                                <li>
                                    <a href="<?php echo site_url('city/add');?>"><i class="fa fa-plus"></i> Add city</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('city/index');?>"><i class="fa fa-list-ul"></i> City List</a>
                                </li>

                                <li>
                                    <a href="<?php echo site_url('center_location/add');?>"><i class="fa fa-plus"></i> Add Center location</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('center_location/index');?>"><i class="fa fa-list-ul"></i> Center Location List</a>
                                </li>                          
                            </ul>
                        </li>

                        <li>
                            <a href="#">
                                <i class="fa fa-level-up"></i> <span>Proficiency level </span>
                            </a>
                            <ul class="treeview-menu">    
                                <li class="active">
                                    <a href="<?php echo site_url('proficiency_level/add');?>"><i class="fa fa-plus"></i> Add Pro level</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('proficiency_level/index');?>"><i class="fa fa-list"></i> Pro Level List</a> 
                                </li>                                
                            </ul>
                        </li>

                        <li>
                            <a href="#">
                                <i class="fa fa-paragraph"></i> <span>Contents </span>
                            </a>
                            <ul class="treeview-menu">    
                                <li class="active">
                                    <a href="<?php echo site_url('contents/add');?>"><i class="fa fa-plus"></i> Add </a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('contents/index');?>"><i class="fa fa-list"></i> List</a> 
                                </li>                                
                            </ul>
                        </li>

                        <li>
                            <a href="#">
                                <i class="fa fa-venus"></i> <span>Gender </span>
                            </a>
                            <ul class="treeview-menu">    
                                <li class="active">
                                    <a href="<?php echo site_url('gender/add');?>"><i class="fa fa-plus"></i> Add Gender</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('gender/index');?>"><i class="fa fa-list"></i> Gender List</a> 
                                </li>                                
                            </ul>
                        </li>
                       
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper" style="background-color: white !important">
                <!-- Main content -->
                <section class="content">                    
                    <?php                    
                    if(isset($_view) && $_view)
                        $this->load->view($_view);
                    ?>                    
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <strong>Generated By <a href="https://www.masterprep.in/" target="_blank"><?php echo MASTERPREP;?></a> 3.1.10</strong>
            </footer>

            
        </div>
        <!-- ./wrapper -->

<!-- jQuery 2.2.3 -->   
<script src="<?php echo base_url('resources/js/jquery-3.2.1.js');?>"></script>   
<script src="<?php echo site_url('resources/js/jquery.min.js');?>"></script>    
<script src="<?php echo site_url('resources/js/jquery-2.2.3.min.js');?>"></script>
<script src="<?php echo site_url('resources/js/bootstrap.min.js');?>"></script>
<script src="<?php echo site_url('resources/js/bootstrap-select.min.js');?>"></script>
<script src="<?php echo site_url('resources/js/fastclick.js');?>"></script>
<script src="<?php echo site_url('resources/js/app.min.js');?>"></script>
<script src="<?php echo site_url('resources/js/demo.js');?>"></script>
<script src="<?php echo site_url('resources/js/moment.js');?>"></script>
<script src="<?php echo site_url('resources/js/bootstrap-datetimepicker.min.js');?>"></script>

<script src="<?php echo site_url('resources/js/global.js');?>"></script>
<script src="https://cdn.ckeditor.com/4.15.1/full/ckeditor.js"></script>    
<script src="<?php echo site_url('resources/js/bootstrap3-wysihtml5.all.min.js');?>"></script>
<script src="<?php echo site_url('resources/js/daterangepicker.min.js');?>"></script>
<script type="text/javascript">

//for replace all textarea to ckeditor
CKEDITOR.replaceAll('myckeditor');
$(".textarea").wysihtml5();

CKEDITOR.config.removePlugins = 'newpage,save,exportpdf,preview,print,templates,Form,Checkbox,Radio,TextField,Textarea,Select';
//CKEDITOR.plugins.addExternal( 'bootstrapTabs', '/ckeditorPlugins/bootstrapTabs/', 'plugin.js' );
/* CKEDITOR.replace( 'ckeditor-bootstrap-tabs-demo', {
  extraPlugins: 'bootstrapTabs',
  contentsCss: [ 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css' ],
  on: {
    instanceReady: loadBootstrap,
    mode: loadBootstrap
  }
});

// Add the necessary jQuery and Bootstrap JS source so that tabs are clickable.
// If you're already using Bootstrap JS with your editor instances, then this is not necessary.
function loadBootstrap(event) {
    if (event.name == 'mode' && event.editor.mode == 'source')
        return; // Skip loading jQuery and Bootstrap when switching to source mode.

    var jQueryScriptTag = document.createElement('script');
    var bootstrapScriptTag = document.createElement('script');

    jQueryScriptTag.src = 'https://code.jquery.com/jquery-1.11.3.min.js';
    bootstrapScriptTag.src = 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js';

    var editorHead = event.editor.document.$.head;

    editorHead.appendChild(jQueryScriptTag);
    jQueryScriptTag.onload = function() {
      editorHead.appendChild(bootstrapScriptTag);
    };
}
   */
CKEDITOR.config.specialChars = [
    '!', '&quot;', '#', '$', '%', '&amp;', "'", '(', ')', '*', '+', '-', '.', '/',
    '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ':', ';',
    '&lt;', '=', '&gt;', '?', '@',
    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O',
    'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
    '[', ']', '^', '_', '`',
    'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p',
    'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
    '{', '|', '}', '~',
    '&euro;', '&lsquo;', '&rsquo;', '&ldquo;', '&rdquo;', '&ndash;', '&mdash;', '&iexcl;', '&cent;', '&pound;',
    '&curren;', '&yen;', '&brvbar;', '&sect;', '&uml;', '&copy;', '&ordf;', '&laquo;', '&not;', '&reg;', '&macr;',
    '&deg;', '&sup2;', '&sup3;', '&acute;', '&micro;', '&para;', '&middot;', '&cedil;', '&sup1;', '&ordm;', '&raquo;',
    '&frac14;', '&frac12;', '&frac34;', '&iquest;', '&Agrave;', '&Aacute;', '&Acirc;', '&Atilde;', '&Auml;', '&Aring;',
    '&AElig;', '&Ccedil;', '&Egrave;', '&Eacute;', '&Ecirc;', '&Euml;', '&Igrave;', '&Iacute;', '&Icirc;', '&Iuml;',
    '&ETH;', '&Ntilde;', '&Ograve;', '&Oacute;', '&Ocirc;', '&Otilde;', '&Ouml;', '&times;', '&Oslash;', '&Ugrave;',
    '&Uacute;', '&Ucirc;', '&Uuml;', '&Yacute;', '&THORN;', '&szlig;', '&agrave;', '&aacute;', '&acirc;', '&atilde;',
    '&auml;', '&aring;', '&aelig;', '&ccedil;', '&egrave;', '&eacute;', '&ecirc;', '&euml;', '&igrave;', '&iacute;',
    '&icirc;', '&iuml;', '&eth;', '&ntilde;', '&ograve;', '&oacute;', '&ocirc;', '&otilde;', '&ouml;', '&divide;',
    '&oslash;', '&ugrave;', '&uacute;', '&ucirc;', '&uuml;', '&yacute;', '&thorn;', '&yuml;', '&OElig;', '&oelig;',
    '&#372;', '&#374', '&#373', '&#375;', '&sbquo;', '&#8219;', '&bdquo;', '&hellip;', '&trade;', '&#9658;', '&bull;',
     '&rArr;', '&hArr;', '&diams;', '&asymp;' , '&rarr;', '←', '↓'  , '↑' , '↱' , '↰' , '◯'
];


function get_qb(question_type_id){

    //alert(question_type_id)
    $.ajax({
        url: "<?php echo site_url('Question_type/get_qb');?>",
        async : true,
        type: 'post',
        data: {question_type_id: question_type_id},
        dataType: 'json',                
        success: function(data){      
            
            $('#qb').val(data);  
        }
    });
}
    
function get_state_list(country_id){
        
    var html='';
    $.ajax({
        url: "<?php echo site_url('city/get_state_list');?>",
        async : true,
        type: 'post',
        data: {country_id: country_id},
        dataType: 'json',                
        success: function(data){
        
            html = '';
            html='<option data-subtext="" value="">Select state</option>';
            for(i=0; i<data.length; i++){

                html += '<option data-subtext='+data[i]['state_name']+' value='+data[i]['state_id']+' >'+data[i]['state_name']+'</option>';
            }
            html += '</select>';
            $('#state_id').html(html);
            $('#state_id').selectpicker('refresh');   
        }
    });
}

function get_city_list(state_id){
       
    var html='';
    $.ajax({
        url: "<?php echo site_url('city/get_city_list');?>",
        async : true,
        type: 'post',
        data: {state_id: state_id},
        dataType: 'json',                
        success: function(data){
        
            html = '';
            html='<option data-subtext="" value="">Select city</option>';
            for(i=0; i<data.length; i++){

                html += '<option data-subtext='+data[i]['city_name']+' value='+data[i]['city_id']+' >'+data[i]['city_name']+'</option>';
            }
            html += '</select>';
            $('#city_id').html(html);
            $('#city_id').selectpicker('refresh');   
        }
    });
}

//gallery link copy
function copy_link(id) {

    var copyText = document.getElementById(id);
    copyText.select();
    copyText.setSelectionRange(0, 999)
    document.execCommand("copy");
    alert("Copied the LINK: " + copyText.value); 
}

//ts_cat_assoc add/edit
function get_category_list(test_seriese_id){
            
    var html='';
    $.ajax({
        url: "<?php echo site_url('category_master/get_category_list');?>",
        async : true,
        type: 'post',
        data: {test_seriese_id: test_seriese_id},
        dataType: 'json',                
        success: function(data){

            html = '';
            html='<option data-subtext="" value="">Select category</option>';
            for(i=0; i<data.length; i++){
                html += '<option data-subtext='+data[i]['category_id']+' value='+data[i]['category_id']+' >'+data[i]['programe_name']+' | '+data[i]['category_name']+'</option>';
            }
            html += '</select>';
            $('#category_id').html(html);
            $('#category_id').selectpicker('refresh');    
        }
    });
}    

//ts_cat_assoc edit -on page load  fetch enable/disable audio field
function get_category(category_id){

    if(category_id==''){
        document.getElementById("audio_file").disabled = true;
        document.getElementById("audio_time").disabled = true;
    }else{
        $.ajax({
            url: "<?php echo site_url('category_master/get_category_name');?>",
            async : true,
            type: 'post',
            data: {category_id: category_id},
            dataType: 'json',                
            success: function(response){
                for(i=0; i<response.length; i++){
                    if(response[i].category_name=='Listening'){
                        document.getElementById("audio_file").disabled = false;
                        document.getElementById("audio_time").disabled = false;
                        }else{
                        document.getElementById("audio_file").disabled = true;
                        document.getElementById("audio_time").disabled = true;
                    }                    
                }
            }
        });
    }       
        
}

//delete file for ts_cat_assoc
function delete_file(id){

    $.ajax({
        url: "<?php echo site_url('ts_cat_assoc/delete_file');?>",
        async : true,
        type: 'post',
        data: {id: id},
        dataType: 'json',                
        success: function(response){
            
            if(response==true){
                $('#file_delete_msg').html('<span class="text-success">File deleted successfully.Now you can upload another file</span>');
                $('#onf').html('<span class="text-info">NO FILE</span>');
                $('.del_file_btn').hide();
            }else{
                $('#file_delete_msg').html('<span class="text-danger">File not deleted! try again</span>');
            }
        }
    });

}

//ts_cat_assoc audio field disable/enable   
$( document ).ready(function() {
    var elementExists = document.getElementById("category_name_hidden");
    if(elementExists){
		var cat_name = document.getElementById("category_name_hidden").value
		if(cat_name=='Listening'){
			document.getElementById("audio_file").disabled = false;
			document.getElementById("audio_time").disabled = false;
		}
	}
});

//activate/deactivate all
function activate_deactivete(id,active,table,pk){
    //alert(id)
    var idd = '#'+id;
    $.ajax({
        url: "<?php echo site_url('gallery/activate_deactivete');?>",
        async : true,
        type: 'post',
        data: {id: id,active: active,table: table,pk: pk},
        dataType: 'json',
        success: function(response){
            if(response==1){
                //alert(response)
                  window.location.href=window.location.href
            }else{
                 $(idd).html('');
            }               
        }
    });
}

//print student_answer result
function printx(){
        
    var prtContent = document.getElementById("printid");
    var WinPrint = window.open('', '', 'left=0,top=0,width=800,height=900,toolbar=0,scrollbars=0,status=0');
    WinPrint.document.write(prtContent.innerHTML);
    WinPrint.document.close();
    WinPrint.focus();
    WinPrint.print();
    WinPrint.close();
}

//student_answer
function update_final_marks(collection_no, category_name,test_seriese_id,ts_cat_assoc_id,max_marks,student_id,programe_id){

    //alert(programe_id);
    var total = 0;
    var list = document.getElementsByClassName("myinput");
    var values = [];
    var count = 0;
    for(var i = 0; i < list.length; ++i) {
        values.push(parseFloat(list[i].value));
        count = count + 1;
    }
    total = values.reduce(function(previousValue, currentValue, index, array){
        return previousValue + currentValue;
    });
    $.ajax({
        url: "<?php echo site_url('student_answer/update_final_marks');?>",
        type: 'post',
        data: {count: count, total: total, collection_no: collection_no, category_name: category_name, test_seriese_id: test_seriese_id, ts_cat_assoc_id: ts_cat_assoc_id, max_marks: max_marks, student_id: student_id,programe_id: programe_id},                              
        success: function(response){ 
              //alert(response)           
            if(response.status=='true'){
                $('#msg').html(response.msg);
                window.location.href='<?php echo current_url(); ?>'                        
            }else{
                $('#msg').html(response.msg);
                window.location.href='<?php echo current_url(); ?>'
            }                    
        }
    });
}

//student_answer  
function update_marks(name,value){
   
   //alert(name);alert(value)
    var collection_no =  document.getElementById('collection_no').value;
    if(value=='' ){
        document.getElementById(name).value='';
        document.getElementById(name).focus();
        alert('Please enter marks');
        return false;
    }else{
        $('#msg').html('');
    }

    if(value!='' && isNaN(value)){
        document.getElementById(name).value='';
        document.getElementById(name).focus();
        alert('Please input numeric characters only');
        return false;
    }else{
        $('#msg').html('');
    }

    $.ajax({
        url: "<?php echo site_url('student_answer/update_marks');?>",
        type: 'post',
        data: {id: name, value: value, collection_no: collection_no },                              
        success: function(response){                    
            if(response.status=='true'){
                //$('#msg').html(response.msg);                        
            }else{
                //$('#msg').html(response.msg);
            }                    
        }
    });
}

function update_p_level(id , value , key){
	console.log();
}

function insert_p_level(id , value , key , answer_id){
	console.log(id);
	console.log(value);
	console.log(key);
	var collection_no =  document.getElementById('collection_no').value;
	console.log(collection_no);
	$.ajax({
        url: "<?php echo site_url('student_answer/insert_p_level');?>",
        type: 'post',
        data: {id: id, value: value, collection_no: collection_no, key: key, answer_id: answer_id },                              
        success: function(response){
            if(response.status=='true'){
                //$('#msg').html(response.msg);                        
            }else{
                //$('#msg').html(response.msg);
            }                    
        }
    });
}

function update_remarks(name,value){
   
   //alert(name);alert(value)
   var collection_no =  document.getElementById('collection_no').value;
   //alert(collection_no);   

    $.ajax({
        url: "<?php echo site_url('student_answer/update_remarks');?>",
        type: 'post',
        data: {id: name, value: value, collection_no: collection_no },                              
        success: function(response){                    
            if(response.status=='true'){
                //$('#msg').html(response.msg);                        
            }else{
                //$('#msg').html(response.msg);
            }                    
        }
    });
}


function get_qt(){

    //alert('ok')
    $('.opt_num_fields').show();
}

function display_option_fields(opt_num,j){

    //alert(opt_num); alert(j);
    var opt_html='';

    for (var i = 1; i <= opt_num; i++) {

        //var val= 065
        
        opt_html += '<div class="col-md-6"><label for="option_key" class="control-label">Enter Key</label><div class="form-group"><input type="text" id="option_key'+J+'[]" name="option_key'+J+'[]" class="form-control" placeholder="Enter key e.g. A,B,C.."/></div></div><div class="col-md-6"><label for="option_value" class="control-label">Enter Option Value</label><div class="form-group"><input type="text" id="option_value'+J+'[]" name="option_value'+J+'[]" class="form-control" placeholder="Enter Value"/></div></div>';
    }

    idd = '#opt_fields'+j;
    //alert(idd)
    $(idd).html(opt_html);
    $(idd).show();
    //$('.opt_fields').show();

}


function recheck_paper(collection_no){

    //alert(collection_no);
    $.ajax({
        url: "<?php echo site_url('student_answer/recheck_paper');?>",
        type: 'post',
        data: {collection_no: collection_no },                              
        success: function(response){                    
            if(response.status=='true'){
                alert('updated to recheck') 
                window.location.href='<?php echo current_url(); ?>'                       
            }else{
                alert('Not updated') 
            }                    
        }
    });
}
function get_timezone(country_code){   
    $.ajax({
        url: "<?php echo site_url('Get_timezones/Get_timezonebycid');?>",
        type: 'post',
        data: {country_code: country_code },                              
        success: function(response){            
              $('#time_zone').html(response);            
              $('#time_zone').selectpicker('refresh')
        }
    });
}

function add_update_availibility(parent_date,current_time , action){
    var data_array = $("#schedule_data").val();             
    if(data_array == ''){
        data_array = {};       
    }else{
        data_array = JSON.parse(data_array);                                
    }

    if(typeof data_array[parent_date] !== 'undefined'){
      var innerArray = data_array[parent_date]; 
    }else{
      var innerArray = []; 
    }
 
  if(action == 'add'){
    innerArray.push(current_time);
  }
  else{
    innerArray = $.grep(innerArray, function(value) {
      return value != current_time;
    });   
  }
 
  data_array[parent_date] = innerArray;

  $("#schedule_data").val(JSON.stringify(data_array));

}

function getAvailabilityDates(startDate, endDate) {
 var dates = [],
      currentDate = Date.parse(startDate),
      addDays = function(days) {
        var date = new Date(this.valueOf());
        date.setDate(date.getDate() + days);
        return date;
      };
      var k = 0;
  while (currentDate <= Date.parse(endDate)) {
    if(k == 0)
        dates.push(new Date(currentDate));
        else
            dates.push(currentDate);
    
    currentDate = addDays.call(currentDate, 1);
     k++;
  }
 return dates;
}

</script>
<script>
	
	
$(function() {
  var maximum_slots = <?= EX_MAX_SLOTS ?>;

$('input[name="datefilter"]').daterangepicker({
    opens: 'left',
    autoApply: true,
    //startDate: moment().startOf('hour'),
    minDate :  moment().startOf('hour'),
    maxDate :  moment().startOf('hour').add(74, 'hour'),
   // endDate: moment().startOf('hour').add(74, 'hour'),
  }, function(start, end, label) {
 //   console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
  var dates= getAvailabilityDates(start.format('YYYY,MM,DD') , end.format('YYYY,MM,DD') );
 $.ajax({
        url: "<?php echo site_url('user/check_schedule_dates');?>",
        async : true,
        type: 'post',
        data: {dates: dates},       
        success: function(data){            
             $(".cnm-time-slots").html(data);             
        }
    });

   /* var html = "";

   var counter = 0;

   dates.forEach(function(date) {
    if(counter == 0){
         date = new Date(date).toString();
    }
    else{
        date = new Date(date).toString();       
    }
    date = date.split(' ').slice(0, 4).join(' ');

   html += "<ul data-date='"+date+"' data-schedule='' data-un='"+counter+"' class='cnm-select-avail-ul'><h2>"+date+"</h2>";
    for(var j=8;j<=18;j++){
        var k = j + 1;
        html += "<li data-date='"+date+"' data-time='"+j+":00-"+k+":00' class='cnm-select-avail-li'>"+j+":00 - "+k+":00</li>";
    }
    html += "</ul>"; 

   
    counter++;
});

html += " <div class='cnm-bottom-section'><button class='btn btn-primary cnm-save-shd-btn'>Save schedule</button></div>"; 

    $(".cnm-time-slots").html(html); */
  });


    $(".cnm-add-avail-btn").click(function(){
        $(".cnm-sections-hidden").slideToggle();
    })

	$('body').on('click', '.bootstrap-select', function() {  
        $(".sidebar-mini").addClass("sidebar-collapse");
    });


    $('body').on('click', '.cnm-select-avail-li', function() {    
        var parent_date = $(this).attr('data-date');
        var current_time = $(this).attr('data-time');
        
        if($(this).hasClass('active')){
        $(this).removeClass('active'); 
        add_update_availibility(parent_date,current_time , 'remove');
        }
        else{            
         var counter = 1;
          $(".cnm-select-avail-ul[data-date='"+parent_date+"'] li.active").each(function( index ) {
          counter++;
          });
          if(counter > maximum_slots){
            alert("Only 2 time slot can be choosen at the moment");
          }else{
            $(this).addClass('active');
            add_update_availibility(parent_date,current_time , 'add');
          }
        }        
    })

    $('body').on('click', '.cnm-save-shd-btn', function() {
        var data_array = $("#schedule_data").val();
        $.ajax({
            url: "<?php echo site_url('user/create_schedule');?>",
            async : true,
            type: 'post',
            data: {data_array: data_array , byuser : 'self'},
            dataType: 'json',                
            success: function(data){
               window.location.href = data.redirect;
                                
            }
        });

    })
    
    
    //$('body').on('focusout', '.cnm-display-order', function() {
		
	$(".cnm-display-order").focusout(function() {
		
		console.log(1111);
		
        var ref_id  = $(this).attr('data-sereies');
        var table = $(this).attr('data-table');
        var value = $(this).val();      
        
        if(value != ""){
			$.ajax({
				url: "<?php echo site_url('test_seriese/update_display_order');?>",
				async : true,
				type: 'post',
				data: {ref_id: ref_id , table : table, value : value},				           
				success: function(data){
				   window.location.href='<?php echo current_url(); ?>'                                
				}
			});
		}else{
			alert("Value can not be empty");
			return false;
		}

    })

    $('body').on('click', '.cnm-update-shd-btn', function() {
        var data_array = $("#schedule_data").val();
        $.ajax({
            url: "<?php echo site_url('user/update_schedule');?>",
            async : true,
            type: 'post',
            data: {data_array: data_array , byuser : 'self'},
            dataType: 'json',                
            success: function(data){
               window.location.href = data.redirect;
                                
            }
        });

    })
    
    $('body').on('click', '.cnm-show-pre-noti', function() {
        var limit  = $(this).attr('data-limit');
        var offset = $(this).attr('data-offset');
        var total  = $(this).attr('data-total');
        
        $.ajax({
            url: "<?php echo site_url('notification/load_more_notifications');?>",
            async : true,
            type: 'post',
            data: {limit: limit , offset : offset , total : total },
            dataType: 'json',                
            success: function(data){
              $(".ul-more-notifictaions").append(data.data);
              $(".cnm-show-pre-noti").attr('data-offset' , data.offset);
              
             if(data.offset >= total)
				$(".cnm-show-pre-noti").remove();
				
                                
            }
        });

    })
    
	$("#cnm-previous-test-form").submit(function(event) {

		/* stop form from submitting normally */
		event.preventDefault();
		$(".box-header .alert").remove();
		$(".alert-danger").remove();
		/* get the action attribute from the <form action=""> element */
		var $form = $(this),
		url = $form.attr('action');
		var formdata = {
			active: 1,
			module_id: 1,
			country_id: 78,
			category_id: $("#category_id option:selected").val(),
			contents: CKEDITOR.instances['contents'].getData(),
		};
		
		
		/* Send the data using post with element id name and name2*/
		var posting = $.post(url, formdata);

		/* Alerts the results */
		posting.done(function(data) {
			console.log(data);
			if(data == "success"){	
			$(".box-header").append(`<?= SUCCESS_MSG ?>`);
			CKEDITOR.instances['contents'].setData('');
			$("#category_id")[0].selectedIndex = 0;
			$("#category_id").selectpicker("refresh");
			}else{
				$(".box-header").append(`<?= FAILED_MSG ?>`);
			}
			
		});
		posting.fail(function(data) {			
			$(".box-header").append(data);
		});
	});
    

});

function show_inner(unid){
	$(".cnm-inner-rw").hide();
	$(".mock-"+unid).toggle();
}
</script>
</body>
</html>
