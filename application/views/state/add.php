<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Add State</h3>
            </div>
            <?php echo $this->session->flashdata('flsh_msg'); ?>
            <?php echo form_open('state/add'); ?>
          	<div class="box-body">
          		
          		<div class="row clearfix">

					<div class="col-md-6">
						<label for="country_id" class="control-label"><span class="text-danger">*</span>Country</label>
						<div class="form-group">
							<select name="country_id" class="form-control selectpicker" data-show-subtext="true" data-live-search="true">
								<option value="">Select country</option>
								<?php 
								foreach($all_country_list as $p)
								{	
									echo '<option value="'.$p['country_id'].'" >'.$p['name'].'</option>';
								} 
								?>
							</select>
							<span class="text-danger"><?php echo form_error('country_id');?></span>
						</div>
					</div>

					<div class="col-md-6">
						<label for="category_name" class="control-label"><span class="text-danger">*</span>State Name</label>
						<div class="form-group has-feedback">
							<input type="text" name="state_name" value="<?php echo $this->input->post('state_name'); ?>" class="form-control" id="state_name" />
							<span class="glyphicon glyphicon-flag form-control-feedback"></span>
							<span class="text-danger"><?php echo form_error('state_name');?></span>
						</div>
					</div>					
					
					<div class="col-md-12">
						<div class="form-group">
							<input type="checkbox" name="active" value="1"  id="active" checked="checked"/>
							<label for="active" class="control-label">Active</label>
						</div>
					</div>

				</div>
			</div>
          	<div class="box-footer">
            	<button type="submit" class="btn btn-success">
            		<i class="fa fa-check"></i> Save
            	</button>
          	</div>
            <?php echo form_close(); ?>
      	</div>
    </div>
</div>