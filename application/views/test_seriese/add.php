<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Add <?php echo TS_EE;?></h3>
            </div>
            <?php echo $this->session->flashdata('flsh_msg');?>
            <?php echo form_open('test_seriese/add'); ?>
          	<div class="box-body">
          		<div class="row clearfix">
					<div class="col-md-3">
						<label for="test_seriese_name" class="control-label"><span class="text-danger">*</span><?php echo TS_EE;?> name</label>
						<div class="form-group">
							<input type="text" name="test_seriese_name" value="<?php echo $this->input->post('test_seriese_name'); ?>" class="form-control" id="test_seriese_name" maxlength="30" placeholder='eg. Test 1'/>
							<span class="text-danger"><?php echo form_error('test_seriese_name');?></span>
							<span class="text-danger"><?php echo $this->session->flashdata('flsh_msg'); ?></span>
						</div>
					</div>

					 <div class="col-md-3">
						<label for="test_seriese_type" class="control-label"><span class="text-danger">*</span>Type</label>
						<div class="form-group">
							<select name="test_seriese_type" class="form-control selectpicker" data-show-subtext="true" data-live-search="true">
								<option data-subtext="" value="">Select</option>
								<?php 
								foreach($all_test_type as $p)
								{
									$selected = ($p['test_type_id'] == $this->input->post('test_type_id')) ? ' selected="selected"' : "";

									echo '<option data-subtext="'.$p['test_type_name'].'" value="'.$p['test_type_name'].'" '.$selected.'>'.$p['test_type_name'].'</option>';
								} 
								?>
							</select>
							<span class="text-danger"><?php echo form_error('test_seriese_type');?></span>
						</div>
					</div>

					<div class="col-md-3">
						<label for="programe_id" class="control-label"><span class="text-danger">*</span>Program</label>
						<div class="form-group">
							<select name="programe_id" class="form-control selectpicker" data-show-subtext="true" data-live-search="true">
								<option data-subtext="" value="">Select Program</option>
								<?php 
								foreach($all_programe_masters as $p)
								{
									$selected = ($p['programe_id'] == $this->input->post('programe_id')) ? ' selected="selected"' : "";

									echo '<option data-subtext="'.$p['programe_name'].'" value="'.$p['programe_id'].'" '.$selected.'>'.$p['programe_name'].'</option>';
								} 
								?>
							</select>
							<span class="text-danger"><?php echo form_error('programe_id');?></span>
						</div>
					</div>					

					<div class="col-md-3">
						<label for="test_seriese_color" class="control-label"><?php echo TS_EE;?> group color</label>
						<div class="form-group">							
							<input type="color" name="test_seriese_color" value="#ff0000" class="form-control">
							<span class="text-danger"><?php echo form_error('test_seriese_color');?></span>
						</div>
					</div>					

					<div class="col-md-3">
						<label for="test_module_id" class="control-label"><span class="text-danger">*</span>Test module</label>
						<div class="form-group">
							<select name="test_module_id" class="form-control selectpicker" data-show-subtext="true" data-live-search="true">
								<option data-subtext="" value="">Select</option>
								<?php 
								foreach($all_test_module as $p)
								{
									$selected = ($p['test_module_id'] == $this->input->post('test_module_id')) ? ' selected="selected"' : "";

									echo '<option data-subtext="'.$t['test_module_name'].'" value="'.$p['test_module_id'].'" >'.$p['test_module_name'].'</option>';
								} 
								?>
							</select>
							<span class="text-danger"><?php echo form_error('test_module_id');?></span>
						</div>
					</div>

					<div class="col-md-3">
						<label for="is_MtEt" class="control-label">Sub test</label>
						<div class="form-group">
							<select name="is_MtEt" class="form-control selectpicker" data-show-subtext="true" data-live-search="true">								
								<option value="" selected="selected">Select</option>
								<option value="1">Evaluation Test</option>
								<option value="2">Mock Test</option>
								<option value="3">Static Test</option>
							</select>
							<span class="text-danger"><?php echo form_error('is_MtEt');?></span>
						</div>
					</div>

					<div class="col-md-3">
						<label for="package_id" class="control-label"><span class="text-danger">*</span>Package</label>
						<div class="form-group">
							<select name="package_id[]" class="form-control selectpicker" data-show-subtext="true" data-live-search="true" multiple>
								<option data-subtext="" value="">Select Package</option>
								<?php 
								foreach($all_packages as $p)
								{
									$selected = ($p['package_id'] == $this->input->post('package_id')) ? ' selected="selected"' : "";

									echo '<option data-subtext="'.$p['package_name'].' | '.$p['programe_name'].'" value="'.$p['package_id'].'" '.$selected.'>'.$p['package_name'].' | '.$p['programe_name'].'</option>';
								} 
								?>
							</select>
							<span class="text-danger"><?php echo form_error('package_id');?></span>
						</div>
					</div>

					<div class="col-md-3">
						<div class="form-group">
							<input type="checkbox" name="active" value="1"  id="active" checked="checked"/>
							<label for="active" class="control-label">Active</label>
						</div>
					</div>
					
					<div class="col-md-12">
						<label for="test_seriese_desc" class="control-label"><?php echo TS_EE;?> description</label>
						<div class="form-group has-feedback">
							<textarea name="test_seriese_desc" class="form-control" id="test_seriese_desc"><?php echo $this->input->post('test_seriese_desc'); ?></textarea>
							<span class="glyphicon glyphicon-text-size form-control-feedback"></span>
						</div>
					</div>

					<div class="col-md-12">
						<label for="x" class="control-label text-danger">
							NOTE: There should be only one free active test paper.
						</label>						
					</div>

					

				</div>
			</div>       

   			<div class="box-footer">   				
            	<button type="submit" class="btn btn-success">
            		<i class="fa fa-check"></i> Save
            	</button>            	
          	</div>

            <?php echo form_close(); ?>
      	</div>
    </div>
</div>



  
    

    
  
