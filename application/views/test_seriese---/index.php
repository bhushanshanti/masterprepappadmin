<script type="text/javascript" src="<?php echo base_url('resources/js/jquery-3.2.1.js');?>"></script>
<input class="form-control" type="text" placeholder="Search" aria-label="Search" id="myInput">
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header bg-danger">
                <h3 class="box-title text-primary"><?php echo $title;?></h3>
                <div class="box-tools">
                    <a href="<?php echo site_url('test_seriese/add'); ?>" class="btn btn-success btn-sm">Add</a>
                </div>
                <?php echo $this->session->flashdata('flsh_msg');?>                 
            </div>
            <div class="box-body table-responsive">
				<?php //t($test_seriese , 1); ?>
				
                <table class="table table-striped table-bordered table-sm">
                    <thead>
                        <tr>     
                        <th><?php echo SR;?></th>             
                        <th style="width: 14%;">Test/Type</th> 
                        <th style="width: 12%;">Category</th>
                        <th style="width: 11%;">Sections</th>
                        <th style="width: 8%;" title="Paper duration">Duration</th>
                        <th style="width: 7%;">Audio</th>
                        <th style="width: 5%;" title="Total no of question in paper">Qs.</th> 
                        <th style="width: 10%;" title="Display order">Order</th> 
                        <th style="width: 3%;"><?php echo STATUS;?></th>
                        <th style="width: 33%;"><?php echo ACTION;?></th>
                    </tr>
                    </thead>
                    <tbody id="myTable">
                    <?php $sr=0; foreach($test_seriese as $t){ $sr++; ?>

                    <tr> 
                        <td style="background: <?php echo $t['test_seriese_color'];?>">
                            <?php echo $sr; ?>                             
                        </td>
                        <td id="test_seriese_name">
                            <?php echo $t['test_seriese_name'].SEP.$t['test_seriese_type'];?>
                        </td>
                        <td>
                            <?php 
                                if(isset($t['category_name'])){
                                    echo $t['category_name'];
                                }else{
                            ?>
                            <a href="<?php echo site_url("ts_cat_assoc/add/".$t["test_seriese_id"]); ?>" data-toggle="tooltip" 
                                title="<?php echo NO_ASSOCIATION_INST;?>"><?php echo NO_ASSOCIATION;?>
                                <span class="fa fa-info text-warning"></span>
                            </a>
                        <?php } ?>                                
                                
                        </td>
                        <td style="width: 220px !important;">
                            <?php 
                            if(isset($t['sec_head']))
                            echo '<span class="text-info">'.$t['sec_head'].'</span>'; 
                            else
                                echo '<span class="text-danger" data-toggle="tooltip" 
                                title="'.NO_SECTION_INST.'">'.NO_SECTION.'</span>';
                            ?>                                
                        </td>
                        <td>
                            <?php 
                                if(isset($t['paper_duration']) or $t['paper_duration']!=''){
                                    echo $t['paper_duration'].PAPER_DURATION_UNIT; 
                                }else{
                                    echo NILL;
                                }                                
                            ?>                                
                        </td>
                        <td>
                            <?php 
                                if(isset($t['audio_file'])){      
                                    echo '<span>
                                            <a href="'.$t['audio_file'].'" target="_blank">Open File</a>
                                        </span>';
                                }else{
                                    echo NA;
                                }                                
                            ?>   
                        </td>
                        <td>
                            <?php 
                                if(isset($t['totalquest'])){      
                                    echo $t['totalquest'];
                                }else{
                                    echo NA;
                                }                                
                            ?>   
                        </td>
                       <td><input type="number" data-sereies="<?= $t['test_seriese_id'] ?>" data-table="test_seriese" value="<?= $t['display_order'] ?>" class="cnm-display-order form-control" style="input.cnm-display-order.form-control { width: 40%;}" /></td>
                        <td>
                            <?php 
                            if($t['active']==1){
                                echo '<span class="text-success"><a href="#" data-toggle="tooltip" title="Click to De-activate" onclick="activate_deactivete('.$t['test_seriese_id'].','.$t['active'].');" >'.ACTIVE.'</a></span>';
                            }else{
                                echo '<span class="text-danger"><a href="#" title="Click to Activate" onclick="activate_deactivete('.$t['test_seriese_id'].','.$t['active'].');"  >'.DEACTIVE.'</a></span>';
                            }
                            ?>                                
                        </td> 
                        <td>

                            <a href="<?php echo site_url('test_seriese/edit/'.$t['test_seriese_id']); ?>" class="btn btn-info btn-xs"  data-toggle="tooltip" title="Edit Test"><span class="fa fa-pencil"></span></a>

                            <?php if(isset($t['ts_cat_assoc_id'])){ ?>
                                <a href="<?php echo site_url('test_seriese/remove_ts_assoc/'.$t['ts_cat_assoc_id'].'/'.$t['test_module_id']); ?>" class="btn btn-danger btn-xs" data-toggle="tooltip" title="Delete Test Association" onclick="return confirm('<?php echo DELETE_CONFIRM;?>');">
                                    <span class="fa fa-trash"></span> 
                                </a>
                            <?php }else{ ?>
                                <a href="<?php echo site_url('test_seriese/remove_ts/'.$t['test_seriese_id'].'/'.$t['test_module_id']); ?>" class="btn btn-danger btn-xs" data-toggle="tooltip" title="Delete Test" onclick="return confirm('<?php echo DELETE_CONFIRM;?>');">
                                    <span class="fa fa-trash"></span> 
                                </a>
                            <?php } ?>  

                            <?php if( isset($t['programe_name']) and isset($t['category_name'])){ ?>

                             <a href="<?php echo site_url('paper/view/'.$t['test_seriese_id'].'/'.$t['ts_cat_assoc_id']); ?>" target = '_blank' class="btn btn-primary btn-xs" data-toggle="tooltip" title="View full paper"><span class="fa fa-eye"></span> Preview</a>

                            <button type="button" class="btn btn-success btn-xs <?php echo $t['category_name'];?>" 
                            data-toggle="modal" data-target="#modal-add" 
                            name="<?php echo $t['test_seriese_name'].'- '.$t['programe_name'].' | '.$t['category_name'];?>" id="<?php echo $t['ts_cat_assoc_id'];?>" data-toggle="tooltip" title="Add new section" onClick="add_section(this.id,this.name,this);"><span class="fa fa-plus"></span> <span id="btn_text_add"> Section</span>
                            </button>

                            <button type="button" class="btn btn-primary btn-xs" 
                            data-toggle="modal" data-target="#modal-view" 
                            name="<?php echo $t['test_seriese_name'].'- '.$t['programe_name'].' | '.$t['category_name'];?>" id="<?php echo $t['ts_cat_assoc_id'];?>" data-toggle="tooltip" title="View all section" onClick="load_section(this.id,this.name);"><span class="fa fa-eye"></span><span id="btn_text_view"> Section</span>
                            </button> 
                        <?php } ?>                                                  
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
                </table>
                <div class="pull-right">
                    <?php echo $this->pagination->create_links(); ?>
                </div>                
            </div>
        </div>
    </div>
</div>
<!-- list view of all test series ends-->

<!-- modal box for add section starts-->
        <div class="modal fade" id="modal-add" style="display: none;">
          <div class="modal-dialog" style="width:1000px !important;">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="refresh_page();">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-heading text-info"></h4>
                <h5 class="msg"></h5>
              </div>

              <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-info">            
                           
                            <div class="box-body">
                                <div class="row clearfix">

                                    <form id="submit_img">
                                        <div class="col-md-4">
                                            <label for="section_image" class="control-label">Section Image</label>
                                            <?php echo SECTION_ALLOWED_TYPES_LABEL;?>
                                            <div class="form-group">
                                                <input type="file" name="file" class="form-control"/><br/>
                                                <button class="btn btn-info" id="btn_upload" type="submit">Upload</button> 
                                                <span id="section_image_msg"></span> 
                                            </div>                                            
                                        </div>
                                    </form>                                                    
                                    
                                    <div class="col-md-4">
                                        <label for="section_heading" class="control-label"><span class="text-danger">*</span>Section Heading</label>
                                        <div class="form-group">
                                            <input type="text" name="section_heading" class="form-control" id="section_heading" placeholder="eg. Passage 1 or Section 2" />
                                            <span class="text-danger" id="section_heading_err"></span>
                                        </div>
                                    </div>
                                    
                                     <div class="col-md-1">
                                        <div class="form-group">
                                          
                                          <label for="active" class="control-label">Active</label>
                                          <input type="checkbox" name="active" value="1" id="active" checked="checked" />
                                        </div>
                                    </div>

                                    <div class="col-md-1">               
                                        <div class="form-group">
                                            <input type="hidden" name="ts_cat_assoc_id" 
                                            id="ts_cat_assoc_id" 
                                            value="<?php echo $t['ts_cat_assoc_id'];?>" 
                                            class="form-control" />                 
                                        </div>
                                    </div> 
                                    <!-- for catching category_name while adding section-->
                                    <div class="col-md-1">               
                                        <div class="form-group">
                                            <input type="hidden" name="category_name_field" 
                                            id="category_name_field" class="form-control" />
                                        </div>
                                    </div> 

                                    <div class="col-md-1">               
                                        <div class="form-group">
                                            <input type="hidden" name="image_id" id="image_id" class="form-control" />
                                        </div>
                                    </div>  

                                    <div class="col-md-12">
                                        <label for="section_desc" class="control-label"><span class="text-danger">*</span>Section Description (Keep the text bold)</label>
                                        <div class="form-group">
                                            <textarea name="section_desc" class="form-control myckeditor" id="section_desc"></textarea>
                                            <span class="text-danger" id="section_desc_err"></span>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <label for="section_para" class="control-label"><span class="text-danger">*</span>Section Paragraph (Don't have bold text only heading should be bold)</label>
                                        <div class="form-group">
                                            <textarea name="section_para" class="form-control myckeditor" id="section_para"></textarea>
                                            <span class="text-danger" id="section_para_err"></span>
                                        </div>
                                    </div>                                   

                                    <div class="col-md-6">
                                        <h5 class="msg"></h5>
                                    </div> 

                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
              </div>

            <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal" onclick="refresh_page();">
            Close
            </button>
                <button type="button" class="btn btn-info" id="sendSection" onclick="sendSection();">Save Section</button>
            </div>

            </div>
          </div>
        </div>
<!-- modal box for add section ends-->

<script type="text/javascript"> 

    //on closing section box
    function refresh_page(){        
        window.location.href=window.location.href
    }

    //for add new section form heading/cat/pgm display
    function add_section(clicked_id, test_seriese_name, obj){

       var fullclassname = obj.className;
       var cut ='btn btn-success btn-xs ';
       var category_name = fullclassname.substr(23, 15);  
       document.getElementById("category_name_field").value = category_name; 

        document.getElementById("ts_cat_assoc_id").value = clicked_id;                
        var modal_heading = document.getElementById("btn_text_add").innerText;       
        $(".modal-heading").html(modal_heading + ' to ' + test_seriese_name);    
    }  

    //submit image for section starts
    $(document).ready(function(){
 
        $('#submit_img').submit(function(e){
            e.preventDefault(); 
                $.ajax({
                     url:"<?php echo site_url('section/do_upload_section_image');?>",
                     type:"post",
                     data:new FormData(this),
                     processData:false,
                     contentType:false,
                     cache:false,
                     async:false,
                     before: function(){
                        $('#section_image_msg').html('<div class="spinner-border"></div>');
                     },
                    success: function(data){
                          
                        document.getElementById("image_id").value = data;
                        var img_val = document.getElementById("image_id").value;
                        if(img_val!=''){
                            document.getElementById("submit_img").reset();
                            $('#section_image_msg').html('<?php echo FILE_SUCCESS_MSG;?>');  
                        }else{
                            document.getElementById("submit_img").reset();
                            $('#section_image_msg').html('<?php echo FILE_FAILED_MSG;?>');
                        }
                        
                    }
                });
        });
    });
    //submit image for section ends

    //submit image for question set starts
    $(document).ready(function(){
 
        $('#question_sets_image_submit_img').submit(function(e){
            e.preventDefault(); 
                 $.ajax({
                     url:"<?php echo site_url('question_set/do_upload_qs_image');?>",
                     type:"post",
                     data:new FormData(this),
                     processData:false,
                     contentType:false,
                     cache:false,
                     async:false,
                    success: function(data){
                        //alert(data)
                        document.getElementById("question_sets_image_id").value = data;
                        var img_val = document.getElementById("question_sets_image_id").value;

                        if(img_val=='false'){                            
                            $('#question_sets_image_msg').html('<?php echo FILE_FAILED_MSG;?>');
                            document.getElementById("question_sets_image_submit_img").reset();                            
                        }else{                            
                            $('#question_sets_image_msg').html('<?php echo FILE_SUCCESS_MSG;?>');
                            document.getElementById("question_sets_image_submit_img").reset();
                        }
                        
                    }
                 });
            });
    });
    //submit image for question set ends
    

    //add new section  
    function sendSection(){
    
        var section_heading= document.getElementById("section_heading").value;
        var section_desc = CKEDITOR.instances['section_desc'].getData(); 
        var section_para = CKEDITOR.instances['section_para'].getData();
        var active=document.getElementById("active").value;            
        var ts_cat_assoc_id = document.getElementById("ts_cat_assoc_id").value;
        var category_name_field = document.getElementById("category_name_field").value;
        var image_id = document.getElementById("image_id").value;

        if(category_name_field=='Reading'){

            if(section_heading==""){
                $('#section_heading_err').text('Enter section heading !'); 
                $('#section_heading').focus();
                return false;
            }if(section_desc==""){
                $('#section_desc_err').text('Enter section description !'); 
                $('#section_desc_err').focus();
                return false;
            }if(section_para==""){
                $('#section_para_err').text('Enter section paragrph !'); 
                $('#section_para_err').focus();
                return false;
            }
            
        }
        if(category_name_field=='Writing'){
             if(section_heading==""){
                $('#section_heading_err').text('Enter section heading !'); 
                $('#section_heading').focus();
                return false;
            }
        }
                
            $.ajax({
               url: "<?php echo site_url('section/add');?>",
                async : true,
                type: 'post',
                data: {ts_cat_assoc_id: ts_cat_assoc_id,section_heading: section_heading,section_desc: section_desc,section_para: section_para, image_id: image_id, active: active},
                dataType: 'json',
                success: function(response){

                    if(response.status=='true'){
                        $('.msg').html(response.msg);                     
                        $('#section_heading').val('');
                        CKEDITOR.instances.section_desc.setData("");
                        CKEDITOR.instances.section_para.setData("");                    
                        $('#active').prop('checked', false);
                    }else{
                        $('.msg').html(response.msg); 
                    }
                    
                }
            });

      
    } 
</script>



<!-- modal box for view section-->
<div class="modal fade" id="modal-view" style="display: none;">
    <div class="modal-dialog" style="width:1250px !important;">
        <div class="modal-content">

              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-heading-view text-info"></h4>
                <h3 class="msg-view"></h3>
              </div>

              <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-info">            
                           
                            <div class="box-body">
                                <div class="row clearfix"> 
                                    <span class="text-warning pull-left"><h4 class="no-section-msg"></h4></span>
                                    <div class="col-md-12">               
                                        <div class="form-group">
                                            <input type="hidden" name="ts_cat_assoc_id_view" id="ts_cat_assoc_id_view" 
                                            value="<?php echo $t['ts_cat_assoc_id'];?>" 
                                            class="form-control"  />
                                        </div>
                                    </div>                                   
                                   <table class="table table-striped table-bordered table-sm" id="mydata">
                                    <thead>
                                        <tr>
                                            <th><?php echo SR;?></th> 
                                            <th>Id</th>
                                            <th>Assoc Id</th>
                                            <th>Heading</th>
                                            <th>Q Sets</th>                          
                                            <th><?php echo STATUS;?></th>
                                            <th><?php echo ACTION;?></th>
                                        </tr>
                                    </thead>
                                    <tbody id="show_data"></tbody>  
                                </table>

                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close
                </button>               
              </div>

        </div>
    </div>
</div>
<!-- modal box for view section ends-->


<!-- modal box for add question set starts-->
        <div class="modal fade" id="modal-add-qs" style="display: none;">
          <div class="modal-dialog" style="width: 835px;">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onClick="get_section_name_ts_cat_id();">
                  <span aria-hidden="true">×</span></button>
                    <h4 class="modal-heading-qs text-info" ></h4>
                    
              </div>
              <div class="modal-body">
                <div class="row">
                <div class="col-md-12">
                    <div class="box box-info">                       
                        
                        <div class="box-body">

                            <div class="row clearfix"> 


                                <div class="col-md-12">                           
                                    <div class="form-group">
                                        <input type="hidden" name="section_id" id="section_id" value="'+data[i].section_id+'" class="form-control"/>
                                    </div>
                                </div>  

                                <div class="col-md-12">               
                                    <div class="form-group">
                                        <input type="hidden" name="question_sets_image_id" id="question_sets_image_id" class="form-control" />
                                    </div>
                                </div>                   
                               
                                <div class="col-md-6">
                                    <label for="question_sets_heading" class="control-label"><span class="text-danger">*</span>Question Set Heading</label>
                                    <div class="form-group">
                                        <input type="text" name="question_sets_heading" class="form-control" id="question_sets_heading" placeholder="eg. Question 1-10" />
                                        <span class="text-danger" id="question_sets_heading_err"></span>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label for="question_type_id" class="control-label"><span class="text-danger">*</span>Question Type</label>
                                    <div class="form-group">
                                        <select id="question_type_id" name="question_type_id" class="form-control selectpicker" data-show-subtext="true" data-live-search="true" onchange="get_qb(this.value)">
                                            <option data-subtext="" value="">Select Question type</option>
                                            <?php 
                                            foreach($all_question_types as $question_type)
                                            {
                                                $selected = ($question_type['question_type_id'] == $this->input->post('question_type_id')) ? ' selected="selected"' : "";

                                                echo '<option data-subtext="'.$question_type['behavior_name'].'" value="'.$question_type['question_type_id'].'" '.$selected.'>'.$question_type['type_name'].'</option>';
                                            } 
                                            ?>
                                        </select>
                                        <span class="text-danger" id="question_type_id_err"></span>
                                    </div>
                                </div>

                                <div class="col-md-12">           
                                    <div class="form-group">
                                        <input type="hidden" name="qb" class="form-control" id="qb" />              
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <label for="total_questions" class="control-label"><span class="text-danger">*</span>Total Question in set</label>
                                    <div class="form-group">
                                        <input type="text" id="total_questions" name="total_questions" class="form-control" maxlength="2" placeholder="Enter Only Number eg. 10"  />
                                        <span class="text-danger" id="total_questions_err"></span>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <label for="display_no_of_questions" class="control-label"><span class="text-danger"></span>No Of Question to show</label>
                                    <div class="form-group">
                                        <input type="text" id="display_no_of_questions" name="display_no_of_questions" class="form-control" placeholder="Enter Only Number eg. 10" maxlength="2" />
                                        <span class="text-danger" id="display_no_of_questions_err"></span>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <label for="answer_audio_recrod_duration" class="control-label"><span class="text-danger"></span>Recording time</label>
                                    <div class="form-group">
                                        <input type="text" id="answer_audio_recrod_duration" name="answer_audio_recrod_duration" class="form-control" maxlength="2" placeholder="in Minutes/per question" />
                                        <span class="text-danger" id="answer_audio_recrod_duration_err"></span>
                                    </div>
                                </div>

                                <form id="question_sets_image_submit_img">
                                    <div class="col-md-6">
                                        <label for="question_sets_image" class="control-label">Set Image</label>
                                        <?php echo QUESTTION_SET_ALLOWED_TYPES_LABEL;?>
                                        <div class="form-group">
                                            <input type="file" name="question_sets_image" class="form-control"/>
                                            
                                        </div>                             
                                    </div>

                                    <div class="col-md-4">
                                        <label for="question_sets_image_up" class="control-label">Upload</label>       
                                        <div class="form-group">
                                            <button class="btn btn-info" id="question_sets_image_btn_upload" type="submit">Upload</button> 
                                            <span id="question_sets_image_msg"></span> 
                                        </div>                             
                                    </div>
                                </form>

                                <div class="col-md-2">
                                    <div class="form-group">
                                    <label for="active" class="control-label">Active</label>
                                    <input type="checkbox" name="active" value="1" id="active_qs" checked="checked" />
                                    </div>
                                </div>
                                
                                <div class="col-md-12">
                                    <label for="question_sets_desc" class="control-label">Question Set Description</label>
                                    <div class="form-group">
                                        <textarea name="question_sets_desc" class="form-control" id="question_sets_desc"></textarea>
                                        <span class="text-danger" id="question_sets_desc_err"></span>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <label for="question_sets_para" class="control-label">Question Set Paragraph</label>
                                    <?php echo QS_PARA_NOTE;?>
                                    <div class="form-group">
                                        <textarea name="question_sets_para" class="form-control myckeditor" id="question_sets_para"></textarea>
                                        <span class="text-danger" id="question_sets_para_err"></span>
                                    </div>
                                </div>                                
                                
                            </div>
                        </div>                       
                       
                    </div>
                </div>
                </div>
              </div>

            <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal" 
            onClick="get_section_name_ts_cat_id();">
                Close
            </button>
            <button type="button" class="btn btn-info" id="add_qs" onclick="sendQuestionSets();">Save</button>                
            </div>

            </div>
          </div>
        </div>
<!-- modal box for add question set ends-->



<script type="text/javascript">
    //search
    $(document).ready(function(){
      $("#myInput").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $("#myTable tr").filter(function() {
          $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
      });
    });

    function get_section_name_ts_cat_id(){
        var ts_cat_assoc_id = $("#xxx").text();
        var section_heading = $(".modal-heading-view").text();    
        load_section(ts_cat_assoc_id, section_heading);
    }    

    //for displaying heading/title on question set form
    function add_qs(section_id,section_heading){
        document.getElementById("section_id").value = section_id;      
        $(".modal-heading-qs").html('Add Question set for ' + section_heading);
    }

    
    //add new question set
    function sendQuestionSets(){
        var section_id = document.getElementById("section_id").value;
        var question_sets_heading = document.getElementById("question_sets_heading").value;
        $("#question_type_id :selected").text(); 
        var question_type_id = $("#question_type_id").val();
        var set_behavior = $("#qb").val();         
        var total_questions = document.getElementById("total_questions").value;
        var display_no_of_questions = document.getElementById("display_no_of_questions").value;        
        var question_sets_desc = document.getElementById("question_sets_desc").value;
        var question_sets_para = CKEDITOR.instances['question_sets_para'].getData();
        var active_qs = document.getElementById("active_qs").value;

        var question_sets_image_id = document.getElementById("question_sets_image_id").value;
        var answer_audio_recrod_duration = document.getElementById("answer_audio_recrod_duration").value;

            if(question_sets_heading == ""){
                $('#question_sets_heading_err').text('Enter heading !'); 
                $('#question_sets_heading').focus();
                return false;
            }if(question_type_id==""){
                $('#question_type_id_err').text('Select question type !'); 
                $('#question_type_id').focus();
                return false;
            }if(total_questions==""){
                $('#total_questions_err').text('Enter no. of question !'); 
                $('#total_questions').focus();
                return false;
            }

              if(total_questions!='' && isNaN(total_questions)){
              document.getElementById('total_questions').value='';
              document.getElementById('total_questions').focus();
              $('#total_questions_err').text('Please input numeric characters only');
              return false;
              }else{
                 $('#total_questions_err').text('');
              }

              if(display_no_of_questions!='' && isNaN(display_no_of_questions)){
              document.getElementById('display_no_of_questions').value='';
              document.getElementById('display_no_of_questions').focus();
              $('#display_no_of_questions_err').text('Please input numeric characters only');
              return false;
              }else{
                 $('#display_no_of_questions_err').text('');
              }

              if(answer_audio_recrod_duration!='' && isNaN(answer_audio_recrod_duration)){
              document.getElementById('answer_audio_recrod_duration').value='';
              document.getElementById('answer_audio_recrod_duration').focus();
              $('#answer_audio_recrod_duration_err').text('Please input numeric characters only');
              return false;
              }else{
                 $('#answer_audio_recrod_duration_err').text('');
              }
            
            $.ajax({
               url: "<?php echo site_url('question_set/add');?>",
                async : true,
                type: 'post',
                data: {section_id: section_id, question_sets_heading: question_sets_heading,question_type_id: question_type_id, set_behavior: set_behavior, total_questions: total_questions,display_no_of_questions: display_no_of_questions, question_sets_desc: question_sets_desc, question_sets_para: question_sets_para, question_sets_image_id: question_sets_image_id,answer_audio_recrod_duration:answer_audio_recrod_duration, active: active_qs},
                dataType: 'json',
                success: function(response){

                    if(response.status=='true'){
                        alert(response.msg);                         
                        $('#question_sets_heading').val('');
                        $('#question_type_id').val('');
                        $('#total_questions').val('');
                        $('#display_no_of_questions').val('');
                        $('#answer_audio_recrod_duration').val('');
                        $('#display_no_of_questions').val('');
                        $('#question_sets_desc').val(''); 
                        $('#question_sets_image_msg').hide(); 
                        CKEDITOR.instances.question_sets_para.setData("");
                    }else{                        
                        alert(response.msg);
                    }
                    
                }
            });
        
    }

    // show all question sets
    function view_qs(section_id,section_heading){

        document.getElementById("section_id_view").value = section_id; 
        $(".modal-heading-qs-view").html('Question Set for ' + section_heading);  
        
            $.ajax({
                url: "<?php echo site_url('question_set/view');?>",
                async : true,
                type: 'post',
                data: {section_id: section_id},
                dataType: 'json',
                success: function(data){
                    var html = '';
                    var i;
                    var sr=0;
                    if(data.length<=0){
                        no_qs_msg = 'No Question set added for this Section yet!';

                        $('.no-qs-msg').text(no_qs_msg);
                    }else{
                    for(i=0; i<data.length; i++){
                        sr++;
                        if(data[i].active!=1){
                            data[i].active='<span style="color:red;">De-active</span>';

                        }else{
                            data[i].active='<span style="color:green;">Active</span>';
                        }
                        html += '<tr>'+
                                '<td>'+sr+'</td>'+
                                '<td id="qsid">'+data[i].question_sets_id+'</td>'+
                                '<td>'+data[i].type_name+'</td>'+
                                '<td>'+data[i].question_sets_heading+'</td>'+
                                 '<td>'+data[i].total_questions+'</td>'+
                                '<td>'+data[i].display_no_of_questions+'</td>'+
                                '<td>'+data[i].question_sets_desc+'</td>'+
                                '<td>'+data[i].active+'</td>'+

                                '<td style="text-align:left;">'+
                                    '<a onclick=windowOpen("<?php echo site_url('question_set/edit/'."'+data[i].question_sets_id+'");?>") href="javascript:void(0);" class="btn btn-info btn-sm item_edit"><span class="fa fa-pencil"></span></a>'+' '+
                                    '<a href="<?php echo site_url('question_set/remove/'."'+data[i].question_sets_id+'");?>" class="btn btn-danger btn-sm item_delete" id="id" name="name"><span class="fa fa-trash"></span></a>'+' '+
                                    '<a href="#" class="btn btn-success btn-sm item_add '+data[i].type_name+'" id= "'+data[i].question_sets_id+'" name="'+data[i].total_questions+'" data-toggle="tooltip" title="Add Questions" onclick=load_question_form(this.id,this.name,this)><span class="fa fa-plus"></span> Ques</a>'+' '+
                                    '<a href="#" class="btn btn-primary btn-sm item_add" id= "'+data[i].question_sets_id+'" name="" data-toggle="tooltip" title="View Questions" onclick=view_questions(this.id)><span class="fa fa-eye"></span> Ques</a>'+
                                '</td>'+

                                '</tr>';
                    }
                }
                    $('#show_qs_data').html(html);
                }
            });
    }

    function reset_form(){
        $('#ques_form').html('');
        $('#show_question_data').html('');
    }

    //view section
    function load_section(clicked_id_view, test_seriese_name_view){

        $('.no-section-msg').text('');
        document.getElementById("ts_cat_assoc_id_view").value = clicked_id_view;                
        var modal_heading_view = document.getElementById("btn_text_view").innerText;       
        $(".modal-heading-view").html(modal_heading_view + ' for ' + test_seriese_name_view);    
        
            $.ajax({
                url: "<?php echo site_url('section/view_section');?>",
                async : true,
                type: 'post',
                data: {ts_cat_assoc_id: clicked_id_view},
                dataType: 'json',
                success: function(data){
                    var html = '';
                    var i;
                    var sr=0;
                    if(data.length<=0){
                        no_section_msg = '<?php echo NO_SECTION_INST; ?>';
                        $('.no-section-msg').text(no_section_msg);
                    }else{
                    for(i=0; i<data.length; i++){
                        sr++;
                        if(data[i].active!=1){
                            data[i].active='<span style="color:red;">De-active</span>';
                        }else{
                            data[i].active='<span style="color:green;">Active</span>';
                        }
                        if(data[i].qs_head==null){
                            data[i].qs_head='<span style="color:red;">No Set</span>';
                        }else{
                            //data[i].active='<span style="color:green;">Active</span>';
                        }
                        html += '<tr>'+
                                '<td>'+sr+'</td>'+
                                '<td>'+data[i].section_id+'</td>'+
                                '<td id="xxx">'+data[i].ts_cat_assoc_id+'</td>'+
                                '<td>'+data[i].section_heading+'</td>'+
                                '<td id="qs_head_blink">'+data[i].qs_head+'</td>'+
                                '<td>'+data[i].active+'</td>'+
                                '<td style="text-align:left;">'+
                                    '<a onclick=windowOpen("<?php echo site_url('section/edit/'."'+data[i].section_id+'");?>") href="javascript:void(0);" class="btn btn-info btn-sm item_edit" data-toggle="tooltip" title="Edit"><span class="fa fa-pencil"></span></a>'+' '+
                                    '<a href="<?php echo site_url('section/remove/'."'+data[i].section_id+'");?>" class="btn btn-danger btn-sm item_delete" data-toggle="tooltip" title="Delete"><span class="fa fa-trash"></span></a>'+' '+
                                    '<a href="#" data-toggle="modal" data-target="#modal-add-qs" data-toggle="tooltip" title="Add new question set" class="btn btn-success btn-sm item_add" id="'+data[i].section_id+'" name="'+data[i].section_heading+'" onclick=add_qs(this.id,this.name)><span class="fa fa-plus  "></span> Qst.</a>'+' '+
                                    '<a href="#" data-toggle="modal" data-target="#modal-view-qs" data-toggle="tooltip" title="Viw all question set" class="btn btn-primary btn-sm item_add" id="'+data[i].section_id+'" name="'+data[i].section_heading+'" onclick=view_qs(this.id,this.name)><span class="fa fa-eye"></span> Qst.</a>'+
                                '</td>'+
                                '</tr>';
                    }
                }
                    $('#show_data').html(html);
                }
            });
    }

    function windowOpen(url) { 
        Window = window.open( url, "_blank", "width=1150, height=600");
    } 
    
    //show all question
    function view_questions(question_sets_id){
        $('.q-thead').show();
        $('.add-q-form').hide();
        $.ajax({
                url: "<?php echo site_url('question/get_question');?>",
                async : true,
                type: 'post',
                data: {question_sets_id: question_sets_id},
                dataType: 'json',                
                success: function(data){
                    var html = '';
                    var i;
                    var sr=0;
                    if(data.length<=0){
                        html = '<span class="text-danger pull-left no-q-yet"><h4>No question added for this set yet!</h4></span>';
                    }else{
                        for(i=0; i<data.length; i++){
                            sr++;
                            if(data[i].active!=1){
                                data[i].active='<span style="color:red;">De-active</span>';

                            }else{
                                data[i].active='<span style="color:green;">Active</span>';
                            }
                            html += '<tr>'+
                                    '<td>'+sr+'</td>'+
                                    '<td>'+data[i].question_sets_id+'</td>'+
                                    '<td>'+data[i].question_id+'</td>'+
                                    '<td>'+data[i].question_no+'</td>'+
                                    '<td>'+data[i].question+'</td>'+
                                    '<td style="text-align:justify">'+data[i].correct_answer+'</td>'+
                                    '<td>'+data[i].active+'</td>'+
                                    '<td style="text-align:left;">'+
                                        '<a onclick=windowOpen("<?php echo site_url('question/edit/'."'+data[i].question_id+'");?>") href="javascript:void(0);" class="btn btn-info btn-sm item_edit"><span class="fa fa-pencil"></span> </a>'+' '+
                                        '<a href="<?php echo site_url('question/remove/'."'+data[i].question_id+'");?>" class="btn btn-danger btn-sm item_delete"><span class="fa fa-trash"></span></a>'+
                                    '</td>'+
                                    '</tr>';
                        }
                    }
                    $('#show_question_data').html(html);
                }
            });
    }

    function load_question_form(question_sets_id, total_questions, obj){

            $.ajax({
               url: "<?php echo site_url('question_set/check_set_limit');?>",
                type: 'post',
                data: {question_sets_id: question_sets_id },
                              
                success: function(response){
                    //alert(response)
                   if(response >= total_questions){
                        //alert("Limit levelled you can't add more question");
                        //return false;
                        load_question_form2(question_sets_id, total_questions, obj);
                   } else{
                        load_question_form2(question_sets_id, total_questions, obj);
                   }                   
                }
            });
    }

    function load_question_form2(question_sets_id, total_questions, obj){
       
       var fullclassname = obj.className;
       var cut ='btn btn-success btn-sm item_add ';
       var type_name = fullclassname.substr(32, 50);
       //alert(type_name)
       $('.no-q-yet').hide();

        form='<div class="row add-q-form"><div class="col-md-12"><div class="box box-info"><div class="box-body"><div class="row clearfix"><form id="addQ"><h4 class="box-title qh text-info">Enter questions and answers:</h4>';
        var i;
        for (i = 1; i <= total_questions; i++) {

            if( type_name == 'Multiple choice' || type_name == 'Matching' || type_name == 'Classification'  || type_name == 'Sentence completion with a box' || type_name =='Pick from a list'){

                var dynamicformContents ='<div class="col-md-12"><label for="answers" class="control-label"><span class="text-danger">*</span>Correct Answer</label><div class="form-group"><select id="correct_answer'+i+'" name="correct_answer'+i+'" class="form-control"><option value="">Select Correct Answer</option><option value="A">A</option><option value="B">B</option><option value="C">C</option><option value="D">D</option><option value="E">E</option><option value="F">F</option><option value="G">G</option><option value="H">H</option></select><span class="text-danger" id="correct_answer_err'+i+'"></span></div></div>';

            }else if(type_name == 'Global multiple choice'){

               var dynamicformContents ='<div class="col-md-12"><label for="answers" class="control-label"><span class="text-danger">*</span>Correct Answer (if muliple please type A~~B or C~~D or D~~B or whatever is correct answer )</label><div class="form-group"><textarea name="correct_answer'+i+'" id="correct_answer'+i+'" class="form-control myckeditor"></textarea><span class="text-danger" id="correct_answer_err'+i+'"></span></div></div>';

            }else if(type_name == 'True/False/Not Given'){

                var dynamicformContents ='<div class="col-md-12"><label for="answers" class="control-label"><span class="text-danger">*</span>Correct Answer</label><div class="form-group"><select id="correct_answer'+i+'" name="correct_answer'+i+'" class="form-control"><option value="">Select Correct Answer</option><option value="A">True</option><option value="B">False</option><option value="C">Not Given</option></select><span class="text-danger" id="correct_answer_err'+i+'"></span></div></div>';


            }else if(type_name == 'Yes/No/Not Given'){

                var dynamicformContents ='<div class="col-md-12"><label for="answers" class="control-label"><span class="text-danger">*</span>Correct Answer</label><div class="form-group"><select id="correct_answer'+i+'" name="correct_answer'+i+'" class="form-control"><option value="">Select Correct Answer</option><option value="A">Yes</option><option value="B">No</option><option value="C">Not Given</option></select><span class="text-danger" id="correct_answer_err'+i+'"></span></div></div>';

            }

            else{

                 var dynamicformContents ='<div class="col-md-12"><label for="answers" class="control-label"><span class="text-danger">*</span>Correct Answer</label><div class="form-group"><textarea name="correct_answer'+i+'" id="correct_answer'+i+'" class="form-control myckeditor"></textarea><span class="text-danger" id="correct_answer_err'+i+'"></span></div></div>';

            }

            form =  form.concat('<div class="col-md-12"><label for="question_no" class="control-label">Enter Question No.</label><div class="form-group"><input type="text" name="question_no'+i+'" id="question_no'+i+'" class="form-control" maxlength="2" placeholder="eg. '+i+'"><span class="text-danger" id="question_no_err'+i+'"></span></div></div><div class="col-md-12"><label for="questions" class="control-label">Enter Question - '+i+' (Dont use bold text here)</label><div class="form-group"><textarea name="question'+i+'" id="question'+i+'" class="form-control myckeditor"></textarea><span class="text-danger" id="question_err'+i+'"></span></div></div><div class="col-md-12"><label for="opt" class="control-label">Options-<span class="text-info"> Options shuld be seperated with sign <span class="text-danger"> ~~</span: <span class="text-danger"> For Ex.</span> <span class="text-success">Ambala~~New Delhi~~Mohali~~Patna</span> <span class="text-danger">OR </span> True~~False~~Not Given </span><span class="text-danger">OR </span> Yes~~No~~Not Given</span></span></label><div class="form-group"><textarea name="opt'+i+'" id="opt'+i+'" class="form-control myckeditor"></textarea><span class="text-danger" id="opt_err'+i+'"></span></div></div>'+dynamicformContents+'<div class="col-md-12"><label for="ans_explaination" class="control-label">Answer Explanation</label><div class="form-group"><textarea name="correct_answer_explaination'+i+'" id="correct_answer_explaination'+i+'" class="form-control myckeditor"></textarea><span class="text-danger" id="correct_answer_exp_err'+i+'"></span></div><hr style="border-top: 2px dotted orange;width:100%"></div>');
        }


        form =
        form.concat('<input type="hidden" id="question_sets_id" name="question_sets_id" value="'+question_sets_id+'"><input type="hidden" id="qtn" name="qtn" value="'+type_name+'"><div class="modal-footer"><button type="button" class="btn btn-primary" onClick="save_question('+total_questions+');">Save Questions</button></div></form><span id="zzz"></span></div></div></div></div></div>');

        $('#ques_form').html(form);
        $( "#question_no1" ).focus();
        
        for (i = 1; i <= total_questions; i++) {
            var q='question'+i;
            CKEDITOR.replace(q);
        }
    }

    //save questions starts
    function save_question(total_questions){ 
       
        var type_name = document.getElementById("qtn").value;
        var i;
        for (i = 1; i <= total_questions; i++) { 

             var qn  = 'question_no' + i;
             var q   = 'question' + i;
             var ca  = 'correct_answer' + i;            
             var cae = 'correct_answer_explaination' + i;
             var optvar = 'opt' + i;
             
            var question_nos = document.getElementById(qn).value;
            var questions = CKEDITOR.instances[q].getData();

            if(type_name == 'True/False/Not Given' || type_name == 'Yes/No/Not Given' || type_name == 'Multiple Optional' || type_name == 'Multiple choice'){

                var e = document.getElementById(ca);            
                var correct_answers = e.options[e.selectedIndex].value;
            }else{
                var correct_answers = document.getElementById(ca).value;
            }
            var correct_answer_explainations = document.getElementById(cae).value;
            var question_sets_id = document.getElementById("question_sets_id").value;
            var opt =document.getElementById(optvar).value;

           
            if(question_nos==''){
                $('#question_no_err' + i).text('Enter question no.!'); 
                $('#question_no_err' + i).focus();
                return false;
            }
            if(question_nos.length>2){
                $('#question_no_err' + i).text('Enter maximum 2 digit no.!'); 
                $('#question_no_err' + i).focus();
                return false;
            }  

            if(correct_answers==""){
                $('#correct_answer_err' + i).text('Enter correct answer !'); 
                $('#correct_answer_err' + i).focus();
                return false;
            }   
            /*var mxlen = 1000;
            if(correct_answers.length> mxlen)
            { 
                alert("Please enter correct answer not more than" +mxlen+ " characters");
                $('#correct_answer_err' + i).focus();
                return false;
            }*/
        
            $.ajax({
               url: "<?php echo site_url('question/add');?>",
                type: 'post',
                data: {question_nos: question_nos, total_questions: total_questions,question_sets_id: question_sets_id,questions: questions,correct_answers: correct_answers,correct_answer_explainations: correct_answer_explainations, opt: opt  },
                              
                success: function(response){

                    if(response.status=='true'){                        
                        //alert(response.msg);
                        $('#zzz').html(response.msg);
                        //var msg="Question saved successfully";
                        document.getElementById("addQ").reset();
                    }else{
                         $('#zzz').html(response.msg);
                    }
                    
                }
            });
        }

    }
    //save questions ends
</script>

<!-- modal box for view question_sets and add question-->
<div class="modal fade" id="modal-view-qs" style="display: none;">
    <div class="modal-dialog" style="width:1000px !important;">
        <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="reset_form();">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-heading-qs-view text-info"></h4>
                <h3 class="msg-add-q"></h3>
              </div>
              <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-info">            
                           
                            <div class="box-body">
                                <div class="row clearfix"> 
                                    <span><h4 class="no-qs-msg text-danger"></h4></span>
                                    <div class="col-md-12">                                       
                                        <div class="form-group">
                                            <input type="hidden" name="section_id_view" 
                                            id="section_id_view" 
                                            value="" 
                                            class="form-control"  />
                                        </div>
                                    </div> 
                                   <table class="table table-striped table-bordered table-sm" id="mydata">
                                    <thead>
                                        <tr>
                                            <th><?php echo SR;?></th> 
                                            <th>Id</th>
                                            <th>Q-Type</th>
                                            <th>Heading</th>
                                            <th>Total Ques</th>
                                            <th>Display</th>
                                            <th style="width: 190px !important;">
                                            Description</th>
                                            <th><?php echo STATUS;?></th>
                                            <th><?php echo ACTION;?></th>
                                        </tr>
                                    </thead>
                                    <tbody id="show_qs_data"></tbody>
                                    </table>                                      

                                    <table class="table table-striped table-bordered table-sm" id="mydata2">
                                    <thead style="display: none;" class="q-thead">
                                    <tr>
                                        <th><?php echo SR;?></th> 
                                        <th>Set-Id.</th>
                                        <th>Q-Id</th>
                                        <th>Q. No.</th>
                                        <th style="width:290px;">Question</th>
                                        <th>Answer</th>               
                                        <th><?php echo STATUS;?></th>
                                        <th><?php echo ACTION;?></th>
                                    </tr>
                                    </thead>
                                    <tbody id="show_question_data"></tbody>
                                    </table> 
                                
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="box box-info">
                                                <div class="box-body">
                                                    <div class="row clearfix">   
                                                        <div id="ques_form">
                                                            


                                                        </div>
                                                    </div>
                                                </div>                            
                                            </div>
                                        </div>
                                    </div> 


                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal" onclick="reset_form();">Close
                </button>   
              </div>

        </div>
    </div>
</div>
<!-- modal box for view question_sets and add questions ends-->
