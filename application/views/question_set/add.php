<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Add Question Set</h3>
            </div>
            <?php echo $this->session->flashdata('flsh_msg'); ?>
            <?php echo form_open('question_set/add'); ?>
          	<div class="box-body">
          		<div class="row clearfix">
					
					
					<div class="col-md-6">
						<label for="question_sets_heading" class="control-label"><span class="text-danger">*</span>Question Set Heading</label>
						<div class="form-group">
							<input type="text" name="question_sets_heading" value="<?php echo $this->input->post('question_sets_heading'); ?>" class="form-control" id="question_sets_heading" />
							<span class="text-danger"><?php echo form_error('question_sets_heading');?></span>
						</div>
					</div>
					<div class="col-md-6">
						<label for="question_type_id" class="control-label"><span class="text-danger">*</span>Question Type</label>
						<div class="form-group">
							<select name="question_type_id" class="form-control">
								<option value="">Select Question type</option>
								<?php 
								foreach($all_question_types as $question_type)
								{
									$selected = ($question_type['question_type_id'] == $this->input->post('question_type_id')) ? ' selected="selected"' : "";

									echo '<option value="'.$question_type['question_type_id'].'" '.$selected.'>'.$question_type['type_name'].'</option>';
								} 
								?>
							</select>
							<span class="text-danger"><?php echo form_error('question_type_id');?></span>
						</div>
					</div>
					<div class="col-md-6">
						<label for="total_questions" class="control-label"><span class="text-danger">*</span>Total No Of Question in set</label>
						<div class="form-group">
							<input type="number" name="total_questions" value="<?php echo $this->input->post('total_questions'); ?>" class="form-control" id="total_questions" min="1" max="10" />
							<span class="text-danger"><?php echo form_error('total_questions');?></span>
						</div>
					</div>
					<div class="col-md-6">
						<label for="display_no_of_questions" class="control-label"><span class="text-danger">*</span>No Of Question to Display</label>
						<div class="form-group">
							<input type="number" name="display_no_of_questions" value="<?php echo $this->input->post('display_no_of_questions'); ?>" class="form-control" id="display_no_of_questions" min="1" max="10" />
							<span class="text-danger"><?php echo form_error('display_no_of_questions');?></span>
						</div>
					</div>
					
					<div class="col-md-12">
						<label for="question_sets_desc" class="control-label"><span class="text-danger">*</span>Question Sets Description</label>
						<div class="form-group">
							<textarea name="question_sets_desc" class="form-control" id="question_sets_desc"><?php echo $this->input->post('question_sets_desc'); ?></textarea>
							<span class="text-danger"><?php echo form_error('question_sets_desc');?></span>
						</div>
					</div>

					<div class="col-md-12">
						<label for="question_sets_para" class="control-label">Question Sets Paragraph</label>
						<div class="form-group">
							<textarea name="question_sets_para" class="form-control" id="question_sets_para" class="myckeditor"><?php echo ($this->input->post('question_sets_para') ? $this->input->post('question_sets_para') : $question_set['question_sets_para']); ?></textarea>
							<span class="text-danger"><?php echo form_error('question_sets_para');?></span>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<input type="checkbox" name="active" value="1"  id="active" checked="checked"/>
							<label for="active" class="control-label">Active</label>
						</div>
					</div>
				</div>
			</div>
          	<div class="box-footer">
            	<button type="submit" class="btn btn-success">
            		<i class="fa fa-check"></i> Save
            	</button>
          	</div>
            <?php echo form_close(); ?>
      	</div>
    </div>
</div>