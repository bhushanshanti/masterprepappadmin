
<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Add Previous test</h3> 
            </div>
            <?php echo $this->session->flashdata('flsh_msg');?>
             <button class="btn btn-success" onclick="$('.cnm-pre-ilts-iframe').slideToggle();">Add from website</button>
            <div class="cnm-pre-ilts-iframe" style="display:none;"><br><form action=""><div class="col-md-6"><div class="form-group"><input class="form-control" type="text" value="<?= $this->input->get('url') ?>" name="url" required></div></div><input type="submit" value="Go" class="btn btn-info btn-sm" /> </form> </div>
           
            <?php echo form_open_multipart('Previous_Ielts_tests/add_ajax', 'id="cnm-previous-test-form"'); ?>            
          	<div class="box-body">
          		<div class="row clearfix">					
					<?php
					//t($all_countries);					
					?>     	
          			

					<div class="col-md-12">
						<label for="category_id" class="control-label"><span class="text-danger">*</span>Program </label>
						<div class="form-group">
							<select name="category_id" id="category_id" class="form-control selectpicker" data-show-subtext="true" data-live-search="true">
								<option value="">Select category</option>
								<?php 
								foreach($all_programe_masters as $category_master)
								{
									$selected = ($category_master['programe_id'] == $this->input->post('category_id')) ? ' selected="selected"' : "";

									echo '<option value="'.$category_master['programe_id'].'" '.$selected.'>'.$category_master['programe_name'].'</option>';
								} 
								?>
							</select>
							<span class="text-danger"><?php echo form_error('category_id');?></span>
						</div>
					</div>					

					

					<div class="col-md-12">
						<label for="contents" class="control-label">Contents</label>
						<div class="form-group has-feedback">
							<textarea name="contents" class="form-control myckeditor" id="contents"><?php echo $this->input->post('contents'); ?></textarea>
							<span class="glyphicon glyphicon-text-size form-control-feedback"></span>
							<span class="text-danger"><?php echo form_error('contents');?></span>
						</div>
					</div>

					
				</div>
			</div>
          	<div class="box-footer">				
            	<button type="submit" class="btn btn-success">
            		<i class="fa fa-check"></i> Save
            	</button>
          	</div>
            <?php echo form_close(); ?>
            <?php
            if($this->input->get('url') && $this->input->get('url') != ""){				
            ?>
            <iframe src="<?= $this->input->get('url') ?>" width="800px" height="500px" ></iframe>
           <?php } ?>
      	</div>
    </div>
</div>


  
  

