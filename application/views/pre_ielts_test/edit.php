<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Edit Previous test</h3>
            </div>
            <?php echo $this->session->flashdata('flsh_msg');?>
            <?php echo form_open_multipart('Previous_Ielts_tests/edit/'.$all_tests[0]['id']); ?>    
            
             
          	<div class="box-body">
          		<div class="row clearfix">
					<div class="col-md-6" style="display:none;">
						<label for="country_id" class="control-label"><span class="text-danger">*</span>Country </label>
						<div class="form-group">
							<select name="country_id" class="form-control selectpicker" data-show-subtext="true" data-live-search="true">
								<option value="">Select Country</option>
								<?php 
								foreach($all_countries as $country){
									$selected = ($country['country_id'] == $all_tests[0]['country_id']) ? ' selected="selected"' : "";
									echo '<option value="'.$country['country_id'].'" '.$selected.'>'.$country['name'].'</option>';
								} 
								?>
							</select>
							<span class="text-danger"><?php echo form_error('category_id');?></span>
						</div>
					</div>
					<div class="col-md-12">
						<label for="category_id" class="control-label"><span class="text-danger">*</span>Program </label>
						<div class="form-group">
							<select name="category_id" class="form-control selectpicker" data-show-subtext="true" data-live-search="true">
								<option value="">Select category</option>
								<?php 
								foreach($all_programe_masters as $category_master)
								{
									$selected = ($category_master['programe_id'] == $all_tests[0]['category_id']) ? ' selected="selected"' : "";

									echo '<option value="'.$category_master['programe_id'].'" '.$selected.'>'.$category_master['programe_name'].'</option>';
								} 
								?>
							</select>
							<span class="text-danger"><?php echo form_error('category_id');?></span>
						</div>
					</div>					

					<div class="col-md-4">
						<div class="form-group">
							<label for="active" class="control-label">Active</label>
							 <input type="checkbox" name="active" value="1" <?php echo ($all_tests[0]['active']==1 ? 'checked="checked"' : ''); ?> id='active' />					
						</div>
					</div>

					<div class="col-md-12">
						<label for="contents" class="control-label">Contents</label>
						<div class="form-group has-feedback">
							<textarea name="contents" class="form-control myckeditor" id="contents"><?= $all_tests[0]['content']; ?></textarea>
							<span class="glyphicon glyphicon-text-size form-control-feedback"></span>
							<span class="text-danger"><?php echo form_error('contents');?></span>
						</div>
					</div>

					
				</div>
			</div>
          	<div class="box-footer">
            	<button type="submit" class="btn btn-success">
            		<i class="fa fa-check"></i> Save
            	</button>
          	</div>
            <?php echo form_close(); ?>
      	</div>
    </div>
</div>


  
  

