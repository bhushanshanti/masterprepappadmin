<input class="form-control" type="text" placeholder="Search" aria-label="Search" id="myInput">
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header bg-danger">
                
                <h3 class="box-title text-primary"><?php echo $title;?></h3>
                <div class="box-tools">
                    <a href="<?php echo site_url('Previous_Ielts_tests/add'); ?>" class="btn btn-success btn-sm">Add</a> 
                </div>
                <?php echo $this->session->flashdata('flsh_msg');?>
            </div>
           
            <div class="box-body table-responsive">
                <table class="table table-striped table-bordered table-sm">
                    <thead>
                    <tr>
                        <th><?php echo SR;?></th>                        
                        <th>Program</th>
                       
                        <th>Content</th>                                      
                       <th><?php echo STATUS;?></th>
                       <th><?php echo ACTION;?></th>      
                        
                    </tr>
                    </thead>
                    <tbody id="myTable">
						<?php
						$count = 0;
						foreach($all_tests as $info => $value){
							$zero=0;$one=1;$pk='id'; $table='previous_ielts_tests';
							$count++;
							$id = $value['id'];
							$name = $value['name'];
							$module_id = $value['module_id'];
							$category_id = $value['category_id'];
							$content = $value['content'];
							$active = $value['active'];
							$category_name = $value['category_name'];
							$programe_name = $value['programe_name'];
							$out = strlen($content) > 50 ? substr($content,0,100)."..." : $content;

							if($active==1){
                                $active_cont = '<span class="text-success"><a href="javascript:void(0);" id='.$id.' data-toggle="tooltip" title="Click to De-activate" onclick=activate_deactivete('.$id.','.$zero.',"'.$table.'","'.$pk.'") >'.ACTIVE.'</a></span>';
                            }else{
                                $active_cont = '<span class="text-danger"><a href="javascript:void(0);" id='.$id.' title="Click to Activate" onclick=activate_deactivete('.$id.','.$one.',"'.$table.'","'.$pk.'") >'.DEACTIVE.'</a></span>';
                            }
                           
                           
                          
							echo "<tr><td>$count</td><td>$programe_name</td><td>$out</td><td>$active_cont</td><td>";
							
							?>
							 <a href="<?php echo site_url('Previous_Ielts_tests/edit/'.$id); ?>" class="btn btn-info btn-xs" data-toggle="tooltip" title="Edit"><span class="fa fa-pencil"></span> </a> 
                            <a href="<?php echo site_url('Previous_Ielts_tests/remove/'.$id); ?>" class="btn btn-danger btn-xs" data-toggle="tooltip" title="Delete" onclick="return confirm('Are you sure you want to delete this item?');"><span class="fa fa-trash"></span> </a>
							<?php
							
							echo "</td></tr>";
							}
						
						?>                
					</tbody>
                </table>
                <div class="pull-right">
                    <?php echo $this->pagination->create_links(); ?>                    
                </div>                
            </div>
        </div>
    </div>
</div>
