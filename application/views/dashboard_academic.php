<span class="badge badge-danger text-uppercase">Dashboard -Academic: </span><br/><br/>
<button type="button" name="GT" class="btn btn-primary btn-lg" onclick="show_gt_dashboard();">General Training Dashboard</button>
<button type="button" name="EE" class="btn btn-info btn-lg" onclick="show_ee_dashboard();">English essentials Dashboard</button><br/><br/>


<div class="row bg-danger">
<br/>
  <div class="col-md-3 acd" data-toggle="tooltip">
    <a href="<?php echo site_url('Student/enquiry');?>" target="<?php echo TARGET_S;?>" class="text-danger">
        <div class="info-box">  
        <span class="info-box-icon <?php echo BG;?>">
          <span class="fa fa-commenting"></span>
        </span>
        <div class="info-box-content">
          <span class="info-box-text text-muted">Enquiry</span>
          <span class="info-box-number text-muted"><?php echo $ec;?></span>
        </div>
      </div>
      </a>
  </div>

  <div class="col-md-3 acd" data-toggle="tooltip">
    <a href="<?php echo site_url('Student/visa_enquiry');?>" target="<?php echo TARGET_S;?>" class="text-danger">
        <div class="info-box">  
        <span class="info-box-icon <?php echo BG;?>">
          <span class="fa fa-commenting"></span>
        </span>
        <div class="info-box-content">
          <span class="info-box-text text-muted">VISA Enquiry</span>
          <span class="info-box-number text-muted"><?php echo $vec;?></span>
        </div>
      </div>
      </a>
  </div>  

  <div class="col-md-3">
    <a href="<?php echo site_url('student/index');?>" target="<?php echo TARGET_S;?>" class="text-danger">
        <div class="info-box">
        <span class="info-box-icon <?php echo BG;?>"><i class="fa fa-check text-success"></i></span>
        <div class="info-box-content">
          <span class="info-box-text text-muted">Paper checked</span>
          <span class="info-box-number text-muted"><?php echo $tpc;?></span>
        </div>
      </div>
      </a>
  </div>

  <div class="col-md-3">
    <a href="<?php echo site_url('student/index');?>" target="<?php echo TARGET_S;?>" class="text-danger">
        <div class="info-box">
        <span class="info-box-icon <?php echo BG;?>"><i class="fa fa-close text-danger"></i></span>
        <div class="info-box-content">
          <span class="info-box-text text-muted">Paper un-checked</span>
          <span class="info-box-number text-muted"><?php echo $tpuc;?></span>
        </div>
      </div>
      </a>
  </div>
  
  <div class="col-md-3 ET">
    <a href="<?php echo site_url('test_seriese/index_et');?>" target="<?php echo TARGET_S;?>" class="text-danger">
        <div class="info-box">
        <span class="info-box-icon <?php echo BG;?>">
          <i class="fa fa-book text-warning"></i></span>
        <div class="info-box-content">
          <span class="info-box-text text-muted">Evaluation Tests</span>
          <span class="info-box-number text-muted"><?php echo $ic;?></span>
        </div>
      </div>
      </a>
  </div>

  <div class="col-md-3 acd">
    <a href="<?php echo site_url('test_seriese/index');?>" target="<?php echo TARGET_S;?>" class="text-danger">
        <div class="info-box">
        <span class="info-box-icon <?php echo BG;?>">
          <i class="fa fa-book text-warning"></i></span>
        <div class="info-box-content">
          <span class="info-box-text text-muted"><?php echo IELTS;?> Tests</span>
          <span class="info-box-number text-muted"><?php echo $tc;?></span>
        </div>
      </div>
      </a>
  </div>

  <div class="col-md-3 acd">
    <a href="<?php echo site_url('test_seriese/index_mt');?>" target="<?php echo TARGET_S;?>" class="text-danger">
        <div class="info-box">
        <span class="info-box-icon <?php echo BG;?>">
          <i class="fa fa-book text-warning"></i></span>
        <div class="info-box-content">
          <span class="info-box-text text-muted"><?php echo MOCK;?> Tests</span>
          <span class="info-box-number text-muted"><?php echo $mc;?></span>
        </div>
      </div>
      </a>
  </div>  

  <div class="col-md-3 acd">
    <a href="<?php echo site_url('static_page/index');?>" target="<?php echo TARGET_S;?>" class="text-danger">
        <div class="info-box">
        <span class="info-box-icon <?php echo BG;?>">
          <i class="fa fa-lightbulb-o text-danger"></i></span>
        <div class="info-box-content">
          <span class="info-box-text text-muted">Static Pages</span>
        </div>
      </div>
      </a>
  </div>

  <div class="col-md-3 acd">
    <a href="<?php echo site_url('student/index');?>" target="<?php echo TARGET_S;?>" class="text-danger">
        <div class="info-box">
        <span class="info-box-icon <?php echo BG;?>">
          <i class="fa fa-users text-success"></i></span>
        <div class="info-box-content">
          <span class="info-box-text text-muted">Students</span>
          <span class="info-box-number text-muted"><?php echo $sc;?></span>
        </div>
      </div>
      </a>
  </div>
  

  <div class="col-md-3 acd">
    <a href="<?php echo site_url('band_score/index');?>" target="<?php echo TARGET_S;?>" class="text-danger">
        <div class="info-box">
        <span class="info-box-icon <?php echo BG;?>">
          <i class="fa fa-question text-success"></i></span>
        <div class="info-box-content">
          <span class="info-box-text text-muted">Band Score</span>
        </div>
      </div>
      </a>
  </div>  

  <div class="col-md-3 acd">
    <a href="<?php echo site_url('package_master/index');?>" target="<?php echo TARGET_S;?>" class="text-danger">
        <div class="info-box">
        <span class="info-box-icon <?php echo BG;?>">
          <i class="fa fa-briefcase text-primary"></i></span>
        <div class="info-box-content">
          <span class="info-box-text text-muted">Packages</span>
          <span class="info-box-number text-muted"><?php echo $pkgc;?></span>
        </div>
      </div>
      </a>
  </div> 

  <div class="col-md-3 acd">
    <a href="<?php echo site_url('static_page/print_sp/46');?>" target="<?php echo TARGET_S;?>" class="text-danger">
        <div class="info-box">
        <span class="info-box-icon <?php echo BG;?>">
          <i class="fa fa-print text-danger"></i></span>
        <div class="info-box-content">
          <span class="info-box-text text-muted">Listening content</span>
        </div>
      </div>
      </a>
  </div>


  <div class="col-md-3 acd">
    <a href="<?php echo site_url('static_page/print_sp/44');?>" target="<?php echo TARGET_S;?>" class="text-danger">
        <div class="info-box">
        <span class="info-box-icon <?php echo BG;?>">
          <i class="fa fa-print text-danger"></i></span>
        <div class="info-box-content">
          <span class="info-box-text text-muted">Reading content</span>
        </div>
      </div>
      </a>
  </div>

  <div class="col-md-3 acd">
    <a href="<?php echo site_url('static_page/print_sp/47');?>" target="<?php echo TARGET_S;?>" class="text-danger">
        <div class="info-box">
        <span class="info-box-icon <?php echo BG;?>">
          <i class="fa fa-print text-danger"></i></span>
        <div class="info-box-content">
          <span class="info-box-text text-muted">Speaking content</span>
        </div>
      </div>
      </a>
  </div>

  <div class="col-md-3 acd">
    <a href="<?php echo site_url('static_page/print_sp/45');?>" target="<?php echo TARGET_S;?>" class="text-danger">
        <div class="info-box">
        <span class="info-box-icon <?php echo BG;?>">
          <i class="fa fa-print text-danger"></i></span>
        <div class="info-box-content">
          <span class="info-box-text text-muted">Writing content</span>
        </div>
      </div>
      </a>
  </div>

</div>





  
 