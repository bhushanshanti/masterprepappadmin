<div class="table-responsive panel">
   <div class="row">
	  <div class="col-md-12">
		 <div class="box">
			<div class="box-body table-responsive">
				<table class="table table-bordered table-sm">
				  <thead class="bg-warning">
					 <tr>
						<th><?php echo SR;?></th>
						<th>Paper Unique Id</th>
						<th>Module</th>
						<th>Test Name<?php echo SEP;?>Type</th>
						<th>Programme<?php echo SEP;?>Category</th>
						<th>Duration<?php echo SEP;?>M.M.</th>
						<th>Attempted on</th>
						<th>Score</th>
						<th><?php echo "view";?></th>
						<th>ACTION</th>
						
					 </tr>
				  </thead>
				  <tbody id="myTable">
				<?php
				$srM = 1;
				$array_colors = array("bg-primary" , "bg-success" , "bg-info" , "bg-danger");
				$colors_counter = 0;
				//t($mock_tests);
				if(!empty($mock_tests)){
					foreach($mock_tests as $un_id => $result){
						$totaltime = 0;
						$created   = "";
						$modules_result = array();				
						$stud_id   = $result['student_info']['stud_id'];
						$unique_test_id   = $result['student_info']['unique_test_id'];
						$modules_result['name']   = $result['student_info']['fname']['lname'];
						$student_email = $modules_result['email']  = $result['student_info']['email'];
						$modules_result['mobile'] = $result['student_info']['mobile'];
						$book_speaking_slots_id = $result['student_info']['book_speaking_slots_id'];				
						$modules_result['Speaking']['band_score'] = $result['student_info']['band_score'];
						
						$module_name = "Speaking";
						
						$inner_table = "";
						$sr = 1;
						
						//t($modules_result);
						foreach($result as $module_un => $module){
							$module_name_array = explode("-" , $module_un);
							if(isset($module_name_array['2'])):
								$module_name                  = $module_name_array['2'];								
							endif;
						$modules_result["$module_name"] = $module;
						
						if(!isset($module['created']) || $module['created'] == ""){
							$stu_booking_info = json_decode($module['stu_booking_info'] , TRUE);
							$module['created'] = $stu_booking_info['booking_date']." ".$stu_booking_info['booking_starttime'];
						}
						
									
							if(($module['paper_checked']==1 )|| ($module_name == "Speaking")){
								$chk='<span class="text-success"><i class="fa fa-check"></i></span>';
								$btn_text='Get result';
								$btn_icon='eye';
								$btn_color='info';
							}else{
								$chk='<span class="text-warning"><i class="fa fa-close"></i></span>';
								$btn_text='Check paper';
								$btn_icon='check';
								$btn_color='warning';
							} 
							
							$un_mock_id = $unique_test_id;
																		   
							$inner_table .= "<tr id='mock-$un_mock_id' class='collapse mock-$un_mock_id cnm-inner-rw $array_colors[$colors_counter]' ><td>$srM.$sr</td><td>$unique_test_id</td><td>IELTS</td><td>$module_name</td><td>Academic | MOCK</td><td>".$module['time_taken']."</td><td>".$module['created']."</td><td>".$module['band_score']."</td><td>$chk</td>";
							
							$get_result_url = site_url('student_answer/get_result/'.$module['collection_no'].'/'.$module['ts_cat_assoc_id'].'/'.$module['student_id']);
							
							$remove_result_url = site_url('student_answer/remove/'.$module['collection_no'].'/'.$module['ts_cat_assoc_id'].'/'.$module['student_id']);
							
							
							if($module_name != "Speaking"):
								$inner_table .= "<td><a href='$get_result_url' class='btn btn-$btn_color btn-xs' data-toggle='tooltip' title='$btn_text'><span class='fa fa-$btn_icon'></span> $btn_text</a><a href='$remove_result_url ?>' class='btn btn-danger btn-xs' data-toggle='tooltip' title='Delete Result' onclick='return confirm(`Are you sure you want to delete this item?`);'><span class='fa fa-trash'></span> </a></td>";
							else:
								$inner_table .= "<td>&nbsp;</td>";
							endif;
							
											
							$inner_table .= "</tr>";
							
							$totaltime = $totaltime + $module['time_taken'];
							if($module['created'] != "")
								$created = $module['created'];
													
							$sr++;
						}
						
						
						if(isset($modules_result['Listening']['band_score']))
							$Listening = $modules_result['Listening']['band_score'];			
						
						if(isset($modules_result['Reading']['band_score']))
							$Reading = $modules_result['Reading']['band_score'];			
						
						if(isset($modules_result['Writing']['band_score']))
							$Writing = $modules_result['Writing']['band_score'];			
						
						if(isset($modules_result['Speaking']['band_score']))
							$Speaking = $modules_result['Speaking']['band_score'];	
		
						
						
						
						$overall_score = $this->Student_answer_model->calc_total_band($Listening , $Reading , $Writing , $Speaking );
						
						
						$inner_table .= "";						
						echo '<tr style="border-top: 6px solid #333;" class="'.$array_colors[$colors_counter].'"><td>'.$srM.'</td><td>'.$un_mock_id.'</td></td><td>IELTS</td><td>Mock test</td><td>Academic | MOCK</td><td>--</td><td>'.$created.'</td><td>'.$overall_score.'</td><td><span class="text-success"><i class="fa fa-check"></i></span></td><td><a href="javascript:;" class="btn btn-info btn-xs" data-toggle="collapse" data-target=".mock-'.$un_mock_id.'" title="" data-original-title="Detail view"><span class="fa fa-eye"></span> Detail view</a> </td></tr>'.$inner_table;
						
										
						$colors_counter++;					
						if($colors_counter > 3)
							$colors_counter = 0;
							
						$srM++;
					}
				}
				
				?>				           
					</tbody>
				  </table> 
			</div>
		 </div>
	  </div>
   </div>
</div>
