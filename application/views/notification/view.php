<div class="row">
   <div class="col-md-12">
      <div class="box">
         <div class="box-header bg-danger">
            <h3 class="box-title text-primary"><?php echo $title;?></h3>
            <?php echo $this->session->flashdata('flsh_msg');?>
         </div>
         <?php //t($slot_information) ?>
         <div class="box-body table-responsive">
            <h2><?= $notification['notification_message'] ?></h2>
            
            <?php if($notify_type != "" ): ?>
            <ul class="nav nav-tabs">
               <li class="active"><a data-toggle="tab" href="#home">Student Information</a></li>
               <li><a data-toggle="tab" href="#menu1">Teacher Information</a></li>
               <li><a data-toggle="tab" href="#menu2">Booking Information</a></li>
            </ul>
            <div class="tab-content">
               <div id="home" class="tab-pane fade in active">
                  <h3>Student Information</h3>
                  <div class="row panel panel-info">
                     <div class="panel-body">
                        <div class="row">
                           <div class="col-lg-12 col-md-12">
                              <div class="row">
                                 <div class="col-lg-12 col-md-12">
                                    <ul class="nav nav-tabs">
                                       <li class="active"><a data-toggle="tab" href="#Summery" class="bg-primary"><i class="fa fa-user"></i> Profile</a></li>
                                       <li><a data-toggle="tab" href="#History" class="bg-success"><i class="fa fa-history"></i> History</a></li>
                                       <li><a data-toggle="tab" href="#Enquiry" class="bg-danger"><i class="fa fa-comment"></i> Enquiry</a></li>
                                       <li><a data-toggle="tab" href="#Listening" class="bg-warning"><i class="fa fa-headphones"></i> Listening</a></li>
                                       <li><a data-toggle="tab" href="#Reading" class="bg-info"><i class="fa fa-book"></i> Reading</a></li>
                                       <li><a data-toggle="tab" href="#Writing" class="bg-primary"><i class="fa fa-pencil"></i> Writing</a></li>
                                       <li><a data-toggle="tab" href="#Speaking" class="bg-success"><i class="fa fa-comments"></i> Speaking</a></li>
                                       <li><a data-toggle="tab" href="#EE" class="bg-danger"><i class="fa fa-etsy"></i> Grammar</a></li>
                                       <li><a data-toggle="tab" href="#ET" class="bg-warning"><i class="fa fa-star"></i> Evaluation</a></li>
                                       <li><a data-toggle="tab" href="#MT" class="bg-info"><i class="fa fa-object-group"></i> Mock</a></li>
                                       <li><a data-toggle="tab" href="#PACK" class="bg-primary"><i class="fa fa-briefcase"></i> Package</a></li>
                                    </ul>
                                    <div class="tab-content">
                                       <div id="Summery" class="tab-pane fade in active">
                                          <div class="table-responsive panel">
                                             <table class="table">
                                                <tbody>
                                                   <tr>
                                                      <td class="text-success"><i class="fa fa-image"></i> Profile Picture</td>
                                                      <td>
                                                         <?php if($basic['profile_pic']){ ?>
                                                         <img src='<?php echo $basic['profile_pic'];?>' class="img-responsive img-thumbnail" style="width: 80px;height: 60px">
                                                         </span>
                                                         <?php }else{ ?>
                                                         <img src='<?php echo 'default_profile_pic.png';?>' class="img-responsive img-thumbnail" style="width: 80px;height: 60px">
                                                         <?php } ?>                                        
                                                      </td>
                                                   </tr>
                                                   <tr>
                                                      <td class="text-success"><i class="fa fa-user"></i> Full Name</td>
                                                      <td><?php echo $basic['fname'].' '.$basic['lname']; ?></td>
                                                   </tr>
                                                   <tr>
                                                      <td class="text-success"><i class="fa fa-user"></i> User Type</td>
                                                      <td>
                                                         <?php echo $basic['programe_id']==11? ACD:GT; ?>
                                                      </td>
                                                   </tr>
                                                   <tr>
                                                      <td class="text-success"><i class="fa fa-phone"></i> Contact</td>
                                                      <td><?php echo $basic['country_code'].'-'.$basic['mobile'];?>  <b><?php echo $zone_info['zoneName'] ?> (<?php echo $zone_info['countryCode'] ?>)</b></td>
                                                   </tr>
                                                   <tr>
                                                      <td class="text-success"><i class="fa fa-envelope"></i> Email</td>
                                                      <td>
                                                         <a href="mailto:<?php echo 
                                                            $basic['email'];?>"><?php echo 
                                                            $basic['email'];?></a>
                                                      </td>
                                                   </tr>
                                                   <tr>
                                                      <td class="text-success"><i class="fa fa-venus"></i> Gender</td>
                                                      <td><?php echo $basic['gender']==1?'Male':'Female';?></td>
                                                   </tr>
                                                   <tr>
                                                      <td class="text-success"><i class="fa fa-calendar"></i> DOB</td>
                                                      <td><?php 
                                                         $date=date_create($basic['dob']);
                                                         $dob = date_format($date,"M d, Y");
                                                         echo $dob;
                                                         ?>           
                                                      </td>
                                                   </tr>
                                                   <tr>
                                                      <td class="text-success"><i class="fa fa-home"></i> Address</td>
                                                      <td><?php echo $basic['residential_address'];?></td>
                                                   </tr>
                                                   <tr style="background-color: #abcdef">
                                                      <td class="text-warning"><i class="fa fa-star"></i> Goal Settings</td>
                                                      <td>
                                                         <?php echo 'Listening '.$basic['Goal_l'].SEP.'Reading '.$basic['Goal_r'].SEP.'Writing '.$basic['Goal_w'].SEP.'Speaking '.$basic['Goal_s'].SEP.'Target Date '.$basic['targateDate'];?>
                                                      </td>
                                                   </tr>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </div>
                                       </div>
                                       <!-- history start -->
                                       <div id="History" class="tab-pane fade">
                                          <div class="row">
                                             <br/>  
                                             <p class="text-success" style="padding-left: 15px;"><b>Over All Band score/Percentile</b></p>
                                             <div class="col-lg-3 col-6">
                                                <div class="small-box bg-warning">
                                                   <div class="inner">
                                                      <h3><?php echo $avgScore_l['bs'] ? $avgScore_l['bs']:'0.0';?></h3>
                                                      <p>Listening</p>
                                                   </div>
                                                   <div class="icon">
                                                      <i class="fa fa-headphones"></i>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="col-lg-3 col-6">
                                                <div class="small-box bg-info">
                                                   <div class="inner">
                                                      <h3><?php echo $avgScore_r['bs'] ? $avgScore_r['bs']:0.0 ;?></h3>
                                                      <p>Reading</p>
                                                   </div>
                                                   <div class="icon">
                                                      <i class="fa fa-book"></i>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="col-lg-3 col-6">
                                                <div class="small-box bg-primary">
                                                   <div class="inner">
                                                      <h3><?php echo $avgScore_w['bs'] ? $avgScore_w['bs']:0.0 ;?></h3>
                                                      <p>Writing</p>
                                                   </div>
                                                   <div class="icon">
                                                      <i class="fa fa-pencil"></i>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="col-lg-3 col-6">
                                                <div class="small-box bg-success">
                                                   <div class="inner">
                                                      <h3><?php echo $avgScore_s['bs'] ? $avgScore_s['bs']:0.0 ;?></h3>
                                                      <p>Speaking</p>
                                                   </div>
                                                   <div class="icon">
                                                      <i class="fa fa-comments"></i>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="col-lg-3 col-6">
                                                <div class="small-box bg-danger">
                                                   <div class="inner">
                                                      <h3><?php echo $avgScore_e['bs'] ? $avgScore_e['bs']:0.0 ;?> %</h3>
                                                      <p>Grammar</p>
                                                   </div>
                                                   <div class="icon">
                                                      <i class="fa fa-etsy"></i>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <!-- history closed -->
                                       <!-- Enquiry start -->
                                       <div id="Enquiry" class="tab-pane fade">
                                          <div class="table-responsive panel">
                                             <div class="row">
                                                <div class="col-md-12">
                                                   <div class="box">
                                                      <div class="box-body table-responsive">
                                                         <table class="table table-striped table-bordered table-sm">
                                                            <thead class="bg-danger">
                                                               <tr>
                                                                  <th><?php echo SR;?></th>
                                                                  <th>Name</th>
                                                                  <th>Email Id</th>
                                                                  <th>Contact no.</th>
                                                                  <th>Branch</th>
                                                                  <th>Course</th>
                                                                  <th>Message</th>
                                                                  <th>Created</th>
                                                                  <th><?php echo ACTION;?></th>
                                                               </tr>
                                                            </thead>
                                                            <tbody id="myTable">
                                                               <?php $sr=0;foreach($enquiry as $s){$zero=0;$one=1;$pk='enquiry_id'; $table='students_enquiry';$sr++; ?>
                                                               <tr>
                                                                  <td><?php echo $sr; ?></td>
                                                                  <td><?php echo $s['name']; ?></td>
                                                                  <td><a href="mailto:<?php echo $s['email'];?>"><?php echo $s['email']; ?></a></td>
                                                                  <td><?php echo $s['mobile']; ?></td>
                                                                  <td><?php echo $s['center_name']; ?></td>
                                                                  <td><?php echo $s['test_module_name']; ?></td>
                                                                  <td><?php echo $s['message']; ?></td>
                                                                  <td>
                                                                     <?php 
                                                                        $date=date_create($s['created']);
                                                                        echo $created = date_format($date,"M d, Y");
                                                                        ?>                                
                                                                  </td>
                                                                  <td> 
                                                                     <a href="<?php echo site_url('student/reply_to_student_enquiry/'.$s['enquiry_id']); ?>" class="btn btn-info btn-xs" data-toggle="tooltip" title="Reply">Reply</a>
                                                                  </td>
                                                               </tr>
                                                               <?php } ?>
                                                            </tbody>
                                                         </table>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <!-- Enquiry closed -->
                                       <!-- Reading start -->
                                       <div id="Reading" class="tab-pane fade">
                                          <div class="table-responsive panel">
                                             <div class="row">
                                                <div class="col-md-12">
                                                   <div class="box">
                                                      <div class="box-body table-responsive">
                                                         <table class="table table-striped table-bordered table-sm">
                                                            <thead class="bg-info">
                                                               <tr>
                                                                  <th><?php echo SR;?></th>
                                                                  <th>Paper Attemt Id</th>
                                                                  <th>Module</th>
                                                                  <th>Test Name<?php echo SEP;?>Type</th>
                                                                  <th>Programme<?php echo SEP;?>Category</th>
                                                                  <th>Duration<?php echo SEP;?>M.M.</th>
                                                                  <th>Attempted on</th>
                                                                  <th>Score</th>
                                                                  <th><?php echo STATUS;?></th>
                                                                  <th><?php echo ACTION;?></th>
                                                               </tr>
                                                            </thead>
                                                            <tbody id="myTable">
                                                               <?php $sr=0; foreach($student_attempts_r as $a){ $sr++; ?>
                                                               <tr>
                                                                  <td><?php echo $sr; ?></td>
                                                                  <td><?php echo $a['collection_no']; ?></td>
                                                                  <td><?php echo $a['test_module_name']; ?></td>
                                                                  <td>
                                                                     <?php 
                                                                        echo $a['test_seriese_name'].SEP.$a['test_seriese_type']; 
                                                                        ?>                                
                                                                  </td>
                                                                  <td>
                                                                     <?php echo $a['programe_name'].SEP.$a['category_name']; ?>
                                                                  </td>
                                                                  <td>
                                                                     <?php 
                                                                        echo $a['paper_duration'].PAPER_DURATION_UNIT.SEP.$a['max_marks']; 
                                                                        ?>                                
                                                                  </td>
                                                                  <td>
                                                                     <?php
                                                                        $date=date_create($a['created']);
                                                                        echo $created = date_format($date,"M d, Y | H:i:s");
                                                                        ?>
                                                                  </td>
                                                                  <td>
                                                                     <?php 
                                                                        if($a['test_module_name']==IELTS)
                                                                            echo $a['band_score']; 
                                                                        else
                                                                            echo $a['percentage'].'%';
                                                                        ?>                            
                                                                  </td>
                                                                  <td>
                                                                     <?php
                                                                        if($a['paper_checked']==1){
                                                                            echo $chk='<span class="text-success"><i class="fa fa-check"></i></span>';
                                                                            $btn_text='Get result';
                                                                            $btn_icon='eye';
                                                                            $btn_color='info';
                                                                        }else{
                                                                            echo $chk='<span class="text-warning"><i class="fa fa-close"></i></span>';
                                                                            $btn_text='Check paper';
                                                                            $btn_icon='check';
                                                                            $btn_color='warning';
                                                                        }                                
                                                                        ?>                                
                                                                  </td>
                                                                  <td>
                                                                     <a href="<?php echo site_url('student_answer/get_result/'.$a['collection_no'].'/'.$a['ts_cat_assoc_id'].'/'.$a['student_id']); ?>" class="btn btn-<?php echo $btn_color;?> btn-xs" data-toggle="tooltip" title="<?php echo $btn_text;?>"><span class="fa fa-<?php echo $btn_icon;?>"></span> <?php echo $btn_text;?></a> 
                                                                     <a href="<?php echo site_url('student_answer/remove/'.$a['collection_no'].'/'.$a['ts_cat_assoc_id'].'/'.$a['student_id']); ?>" class="btn btn-danger btn-xs" data-toggle="tooltip" title="Delete Result" onclick="return confirm('Are you sure you want to delete this item?');"><span class="fa fa-trash"></span> </a>
                                                                  </td>
                                                               </tr>
                                                               <?php } ?>
                                                            </tbody>
                                                         </table>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <!-- Reading closed -->
                                       <!-- Listening start -->
                                       <div id="Listening" class="tab-pane fade">
                                          <div class="table-responsive panel">
                                             <div class="row">
                                                <div class="col-md-12">
                                                   <div class="box">
                                                      <div class="box-body table-responsive">
                                                         <table class="table table-striped table-bordered table-sm">
                                                            <thead class="bg-warning">
                                                               <tr>
                                                                  <th><?php echo SR;?></th>
                                                                  <th>Paper Attemt Id</th>
                                                                  <th>Module</th>
                                                                  <th>Test Name<?php echo SEP;?>Type</th>
                                                                  <th>Programme<?php echo SEP;?>Category</th>
                                                                  <th>Duration<?php echo SEP;?>M.M.</th>
                                                                  <th>Attempted on</th>
                                                                  <th>Score</th>
                                                                  <th><?php echo STATUS;?></th>
                                                                  <th><?php echo ACTION;?></th>
                                                               </tr>
                                                            </thead>
                                                            <tbody id="myTable">
                                                               <?php $sr=0; foreach($student_attempts_l as $a){ $sr++; ?>
                                                               <tr>
                                                                  <td><?php echo $sr; ?></td>
                                                                  <td><?php echo $a['collection_no']; ?></td>
                                                                  <td><?php echo $a['test_module_name']; ?></td>
                                                                  <td>
                                                                     <?php 
                                                                        echo $a['test_seriese_name'].SEP.$a['test_seriese_type']; 
                                                                        ?>                                
                                                                  </td>
                                                                  <td>
                                                                     <?php echo $a['programe_name'].SEP.$a['category_name']; ?>
                                                                  </td>
                                                                  <td>
                                                                     <?php 
                                                                        echo $a['paper_duration'].PAPER_DURATION_UNIT.SEP.$a['max_marks']; 
                                                                        ?>                                
                                                                  </td>
                                                                  <td>
                                                                     <?php
                                                                        $date=date_create($a['created']);
                                                                        echo $created = date_format($date,"M d, Y | H:i:s");
                                                                        ?>
                                                                  </td>
                                                                  <td>
                                                                     <?php 
                                                                        if($a['test_module_name']==IELTS)
                                                                            echo $a['band_score']; 
                                                                        else
                                                                            echo $a['percentage'].'%';
                                                                        ?>                            
                                                                  </td>
                                                                  <td>
                                                                     <?php
                                                                        if($a['paper_checked']==1){
                                                                            echo $chk='<span class="text-success"><i class="fa fa-check"></i></span>';
                                                                            $btn_text='Get result';
                                                                            $btn_icon='eye';
                                                                            $btn_color='info';
                                                                        }else{
                                                                            echo $chk='<span class="text-warning"><i class="fa fa-close"></i></span>';
                                                                            $btn_text='Check paper';
                                                                            $btn_icon='check';
                                                                            $btn_color='warning';
                                                                        }                                
                                                                        ?>                                
                                                                  </td>
                                                                  <td>
                                                                     <a href="<?php echo site_url('student_answer/get_result/'.$a['collection_no'].'/'.$a['ts_cat_assoc_id'].'/'.$a['student_id']); ?>" class="btn btn-<?php echo $btn_color;?> btn-xs" data-toggle="tooltip" title="<?php echo $btn_text;?>"><span class="fa fa-<?php echo $btn_icon;?>"></span> <?php echo $btn_text;?></a> 
                                                                     <a href="<?php echo site_url('student_answer/remove/'.$a['collection_no'].'/'.$a['ts_cat_assoc_id'].'/'.$a['student_id']); ?>" class="btn btn-danger btn-xs" data-toggle="tooltip" title="Delete Result" onclick="return confirm('Are you sure you want to delete this item?');"><span class="fa fa-trash"></span> </a>
                                                                  </td>
                                                               </tr>
                                                               <?php } ?>
                                                            </tbody>
                                                         </table>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <!-- Listening closed -->
                                       <!-- Writing start -->
                                       <div id="Writing" class="tab-pane fade">
                                          <div class="table-responsive panel">
                                             <div class="row">
                                                <div class="col-md-12">
                                                   <div class="box">
                                                      <div class="box-body table-responsive">
                                                         <table class="table table-striped table-bordered table-sm">
                                                            <thead class="bg-primary">
                                                               <tr>
                                                                  <th><?php echo SR;?></th>
                                                                  <th>Paper Attemt Id</th>
                                                                  <th>Module</th>
                                                                  <th>Test Name<?php echo SEP;?>Type</th>
                                                                  <th>Programme<?php echo SEP;?>Category</th>
                                                                  <th>Duration<?php echo SEP;?>M.M.</th>
                                                                  <th>Attempted on</th>
                                                                  <th>Score</th>
                                                                  <th><?php echo STATUS;?></th>
                                                                  <th><?php echo ACTION;?></th>
                                                               </tr>
                                                            </thead>
                                                            <tbody id="myTable">
                                                               <?php $sr=0; foreach($student_attempts_w as $a){ $sr++; ?>
                                                               <tr>
                                                                  <td><?php echo $sr; ?></td>
                                                                  <td><?php echo $a['collection_no']; ?></td>
                                                                  <td><?php echo $a['test_module_name']; ?></td>
                                                                  <td>
                                                                     <?php 
                                                                        echo $a['test_seriese_name'].SEP.$a['test_seriese_type']; 
                                                                        ?>                                
                                                                  </td>
                                                                  <td>
                                                                     <?php echo $a['programe_name'].SEP.$a['category_name']; ?>
                                                                  </td>
                                                                  <td>
                                                                     <?php 
                                                                        echo $a['paper_duration'].PAPER_DURATION_UNIT.SEP.$a['max_marks']; 
                                                                        ?>                                
                                                                  </td>
                                                                  <td>
                                                                     <?php
                                                                        $date=date_create($a['created']);
                                                                        echo $created = date_format($date,"M d, Y | H:i:s");
                                                                        ?>
                                                                  </td>
                                                                  <td>
                                                                     <?php 
                                                                        if($a['test_module_name']==IELTS)
                                                                            echo $a['band_score']; 
                                                                        else
                                                                            echo $a['percentage'].'%';
                                                                        ?>                            
                                                                  </td>
                                                                  <td>
                                                                     <?php
                                                                        if($a['paper_checked']==1){
                                                                            echo $chk='<span class="text-success"><i class="fa fa-check"></i></span>';
                                                                            $btn_text='Get result';
                                                                            $btn_icon='eye';
                                                                            $btn_color='info';
                                                                        }else{
                                                                            echo $chk='<span class="text-warning"><i class="fa fa-close"></i></span>';
                                                                            $btn_text='Check paper';
                                                                            $btn_icon='check';
                                                                            $btn_color='warning';
                                                                        }                                
                                                                        ?>                                
                                                                  </td>
                                                                  <td>
                                                                     <a href="<?php echo site_url('student_answer/get_result/'.$a['collection_no'].'/'.$a['ts_cat_assoc_id'].'/'.$a['student_id']); ?>" class="btn btn-<?php echo $btn_color;?> btn-xs" data-toggle="tooltip" title="<?php echo $btn_text;?>"><span class="fa fa-<?php echo $btn_icon;?>"></span> <?php echo $btn_text;?></a> 
                                                                     <a href="<?php echo site_url('student_answer/remove/'.$a['collection_no'].'/'.$a['ts_cat_assoc_id'].'/'.$a['student_id']); ?>" class="btn btn-danger btn-xs" data-toggle="tooltip" title="Delete Result" onclick="return confirm('Are you sure you want to delete this item?');"><span class="fa fa-trash"></span> </a>
                                                                  </td>
                                                               </tr>
                                                               <?php } ?>
                                                            </tbody>
                                                         </table>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <!-- Writing closed -->
                                       <!-- Speaking start -->
                                       <div id="Speaking" class="tab-pane fade">
                                          <div class="table-responsive panel">
                                             <div class="row">
                                                <div class="col-md-12">
                                                   <div class="box">
                                                      <div class="box-body table-responsive">
                                                         <table class="table table-striped table-bordered table-sm">
                                                            <thead class="bg-success">
                                                               <tr>
                                                                  <th><?php echo SR;?></th>
                                                                  <th>Paper Attemt Id</th>
                                                                  <th>Module</th>
                                                                  <th>Test Name<?php echo SEP;?>Type</th>
                                                                  <th>Programme<?php echo SEP;?>Category</th>
                                                                  <th>Duration<?php echo SEP;?>M.M.</th>
                                                                  <th>Attempted on</th>
                                                                  <th>Score</th>
                                                                  <th><?php echo STATUS;?></th>
                                                                  <th><?php echo ACTION;?></th>
                                                               </tr>
                                                            </thead>
                                                            <tbody id="myTable">
                                                               <?php $sr=0; foreach($student_attempts_s as $a){ $sr++; ?>
                                                               <tr>
                                                                  <td><?php echo $sr; ?></td>
                                                                  <td><?php echo $a['collection_no']; ?></td>
                                                                  <td><?php echo $a['test_module_name']; ?></td>
                                                                  <td>
                                                                     <?php 
                                                                        echo $a['test_seriese_name'].SEP.$a['test_seriese_type']; 
                                                                        ?>                                
                                                                  </td>
                                                                  <td>
                                                                     <?php echo $a['programe_name'].SEP.$a['category_name']; ?>
                                                                  </td>
                                                                  <td>
                                                                     <?php 
                                                                        echo $a['paper_duration'].PAPER_DURATION_UNIT.SEP.$a['max_marks']; 
                                                                        ?>                                
                                                                  </td>
                                                                  <td>
                                                                     <?php
                                                                        $date=date_create($a['created']);
                                                                        echo $created = date_format($date,"M d, Y | H:i:s");
                                                                        ?>
                                                                  </td>
                                                                  <td>
                                                                     <?php 
                                                                        if($a['test_module_name']==IELTS)
                                                                            echo $a['band_score']; 
                                                                        else
                                                                            echo $a['percentage'].'%';
                                                                        ?>                            
                                                                  </td>
                                                                  <td>
                                                                     <?php
                                                                        if($a['paper_checked']==1){
                                                                            echo $chk='<span class="text-success"><i class="fa fa-check"></i></span>';
                                                                            $btn_text='Get result';
                                                                            $btn_icon='eye';
                                                                            $btn_color='info';
                                                                        }else{
                                                                            echo $chk='<span class="text-warning"><i class="fa fa-close"></i></span>';
                                                                            $btn_text='Check paper';
                                                                            $btn_icon='check';
                                                                            $btn_color='warning';
                                                                        }                                
                                                                        ?>                                
                                                                  </td>
                                                                  <td>
                                                                     <a href="<?php echo site_url('student_answer/get_result/'.$a['collection_no'].'/'.$a['ts_cat_assoc_id'].'/'.$a['student_id']); ?>" class="btn btn-<?php echo $btn_color;?> btn-xs" data-toggle="tooltip" title="<?php echo $btn_text;?>"><span class="fa fa-<?php echo $btn_icon;?>"></span> <?php echo $btn_text;?></a> 
                                                                     <a href="<?php echo site_url('student_answer/remove/'.$a['collection_no'].'/'.$a['ts_cat_assoc_id'].'/'.$a['student_id']); ?>" class="btn btn-danger btn-xs" data-toggle="tooltip" title="Delete Result" onclick="return confirm('Are you sure you want to delete this item?');"><span class="fa fa-trash"></span> </a>
                                                                  </td>
                                                               </tr>
                                                               <?php } ?>
                                                            </tbody>
                                                         </table>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <!-- Speaking closed -->
                                       <!-- EE start -->
                                       <div id="EE" class="tab-pane fade">
                                          <div class="table-responsive panel">
                                             <div class="row">
                                                <div class="col-md-12">
                                                   <div class="box">
                                                      <div class="box-body table-responsive">
                                                         <table class="table table-striped table-bordered table-sm">
                                                            <thead class="bg-danger">
                                                               <tr>
                                                                  <th><?php echo SR;?></th>
                                                                  <th>Paper Attemt Id</th>
                                                                  <th>Module</th>
                                                                  <th>Test Name<?php echo SEP;?>Type</th>
                                                                  <th>Programme<?php echo SEP;?>Category</th>
                                                                  <th>Duration<?php echo SEP;?>M.M.</th>
                                                                  <th>Attempted on</th>
                                                                  <th>Score</th>
                                                                  <th><?php echo STATUS;?></th>
                                                                  <th><?php echo ACTION;?></th>
                                                               </tr>
                                                            </thead>
                                                            <tbody id="myTable">
                                                               <?php $sr=0; foreach($student_attempts_ee as $a){ $sr++; ?>
                                                               <tr>
                                                                  <td><?php echo $sr; ?></td>
                                                                  <td><?php echo $a['collection_no']; ?></td>
                                                                  <td><?php echo $a['test_module_name']; ?></td>
                                                                  <td>
                                                                     <?php 
                                                                        echo $a['test_seriese_name'].SEP.$a['test_seriese_type']; 
                                                                        ?>                                
                                                                  </td>
                                                                  <td>
                                                                     <?php echo $a['programe_name'].SEP.$a['category_name']; ?>
                                                                  </td>
                                                                  <td>
                                                                     <?php 
                                                                        echo $a['paper_duration'].PAPER_DURATION_UNIT.SEP.$a['max_marks']; 
                                                                        ?>                                
                                                                  </td>
                                                                  <td>
                                                                     <?php
                                                                        $date=date_create($a['created']);
                                                                        echo $created = date_format($date,"M d, Y | H:i:s");
                                                                        ?>
                                                                  </td>
                                                                  <td>
                                                                     <?php 
                                                                        if($a['test_module_name']==IELTS)
                                                                            echo $a['band_score']; 
                                                                        else
                                                                            echo $a['percentage'].'%';
                                                                        ?>                            
                                                                  </td>
                                                                  <td>
                                                                     <?php
                                                                        if($a['paper_checked']==1){
                                                                            echo $chk='<span class="text-success"><i class="fa fa-check"></i></span>';
                                                                            $btn_text='Get result';
                                                                            $btn_icon='eye';
                                                                            $btn_color='info';
                                                                        }else{
                                                                            echo $chk='<span class="text-warning"><i class="fa fa-close"></i></span>';
                                                                            $btn_text='Check paper';
                                                                            $btn_icon='check';
                                                                            $btn_color='warning';
                                                                        }                                
                                                                        ?>                                
                                                                  </td>
                                                                  <td>
                                                                     <a href="<?php echo site_url('student_answer/get_result/'.$a['collection_no'].'/'.$a['ts_cat_assoc_id'].'/'.$a['student_id']); ?>" class="btn btn-<?php echo $btn_color;?> btn-xs" data-toggle="tooltip" title="<?php echo $btn_text;?>"><span class="fa fa-<?php echo $btn_icon;?>"></span> <?php echo $btn_text;?></a> 
                                                                     <a href="<?php echo site_url('student_answer/remove/'.$a['collection_no'].'/'.$a['ts_cat_assoc_id'].'/'.$a['student_id']); ?>" class="btn btn-danger btn-xs" data-toggle="tooltip" title="Delete Result" onclick="return confirm('Are you sure you want to delete this item?');"><span class="fa fa-trash"></span> </a>
                                                                  </td>
                                                               </tr>
                                                               <?php } ?>
                                                            </tbody>
                                                         </table>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <!-- EE closed -->
                                       <!-- ET start -->
                                       <div id="ET" class="tab-pane fade">
                                          <div class="table-responsive panel">
                                             <div class="row">
                                                <div class="col-md-12">
                                                   <div class="box">
                                                      <div class="box-body table-responsive">
                                                         <table class="table table-striped table-bordered table-sm">
                                                            <thead class="bg-warning">
                                                               <tr>
                                                                  <th><?php echo SR;?></th>
                                                                  <th>Paper Attemt Id</th>
                                                                  <th>Module</th>
                                                                  <th>Test Name<?php echo SEP;?>Type</th>
                                                                  <th>Programme<?php echo SEP;?>Category</th>
                                                                  <th>Duration<?php echo SEP;?>M.M.</th>
                                                                  <th>Attempted on</th>
                                                                  <th>Score</th>
                                                                  <th><?php echo STATUS;?></th>
                                                                  <th><?php echo ACTION;?></th>
                                                               </tr>
                                                            </thead>
                                                            <tbody id="myTable">
                                                               <?php $sr=0; foreach($student_attempts_et as $a){ $sr++; ?>
                                                               <tr>
                                                                  <td><?php echo $sr; ?></td>
                                                                  <td><?php echo $a['collection_no']; ?></td>
                                                                  <td><?php echo $a['test_module_name']; ?></td>
                                                                  <td>
                                                                     <?php 
                                                                        echo $a['test_seriese_name'].SEP.$a['test_seriese_type']; 
                                                                        ?>                                
                                                                  </td>
                                                                  <td>
                                                                     <?php echo $a['programe_name'].SEP.$a['category_name']; ?>
                                                                  </td>
                                                                  <td>
                                                                     <?php 
                                                                        echo $a['paper_duration'].PAPER_DURATION_UNIT.SEP.$a['max_marks']; 
                                                                        ?>                                
                                                                  </td>
                                                                  <td>
                                                                     <?php
                                                                        $date=date_create($a['created']);
                                                                        echo $created = date_format($date,"M d, Y | H:i:s");
                                                                        ?>
                                                                  </td>
                                                                  <td>
                                                                     <?php 
                                                                        if($a['test_module_name']==IELTS)
                                                                            echo $a['band_score']; 
                                                                        else
                                                                            echo $a['percentage'].'%';
                                                                        ?>                            
                                                                  </td>
                                                                  <td>
                                                                     <?php
                                                                        if($a['paper_checked']==1){
                                                                            echo $chk='<span class="text-success"><i class="fa fa-check"></i></span>';
                                                                            $btn_text='Get result';
                                                                            $btn_icon='eye';
                                                                            $btn_color='info';
                                                                        }else{
                                                                            echo $chk='<span class="text-warning"><i class="fa fa-close"></i></span>';
                                                                            $btn_text='Check paper';
                                                                            $btn_icon='check';
                                                                            $btn_color='warning';
                                                                        }                                
                                                                        ?>                                
                                                                  </td>
                                                                  <td>
                                                                     <a href="<?php echo site_url('student_answer/get_result/'.$a['collection_no'].'/'.$a['ts_cat_assoc_id'].'/'.$a['student_id']); ?>" class="btn btn-<?php echo $btn_color;?> btn-xs" data-toggle="tooltip" title="<?php echo $btn_text;?>"><span class="fa fa-<?php echo $btn_icon;?>"></span> <?php echo $btn_text;?></a> 
                                                                     <a href="<?php echo site_url('student_answer/remove/'.$a['collection_no'].'/'.$a['ts_cat_assoc_id'].'/'.$a['student_id']); ?>" class="btn btn-danger btn-xs" data-toggle="tooltip" title="Delete Result" onclick="return confirm('Are you sure you want to delete this item?');"><span class="fa fa-trash"></span> </a>
                                                                  </td>
                                                               </tr>
                                                               <?php } ?>
                                                            </tbody>
                                                         </table>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <!-- ET closed -->
                                       <!-- Mock start -->
                                       <div id="MT" class="tab-pane fade">
                                          <?php include("stu_mockdetails.php"); ?>
                                       </div>
                                       <!-- Mock closed -->
                                       <!-- Mock start -->
                                       <div id="PACK" class="tab-pane fade">
                                          <div class="table-responsive panel">
                                             <div class="row">
                                                <div class="col-md-12">
                                                   <div class="box">
                                                      <div class="box-body table-responsive">
                                                         <table class="table table-striped table-bordered table-sm">
                                                            <thead class="bg-primary">
                                                               <tr>
                                                                  <th>Pack</th>
                                                                  <th>Cost</th>
                                                                  <th>Duration</th>
                                                                  <th>No. of Tests</th>
                                                                  <th>Payment Id</th>
                                                                  <th>Order Id</th>
                                                                  <th>Amount Paid</th>
                                                                  <th>Method</th>
                                                                  <th>Left</th>
                                                                  <th>Pack Status</th>
                                                                  <th>Subscribed</th>
                                                                  <th>Expiry</th>
                                                                  <th>Requested</th>
                                                               </tr>
                                                            </thead>
                                                            <tbody id="myTable">
                                                               <?php foreach($transaction as $s){ ?>
                                                               <tr>
                                                                  <td style="background-color: <?php echo $s['package_color']; ?>;"><?php echo $s['package_name']; ?></td>
                                                                  <td style="background-color: <?php echo $s['package_color']; ?>;"><?php echo $s['package_cost']; ?></td>
                                                                  <td style="background-color: <?php echo $s['package_color']; ?>;"><?php echo $s['package_duration']; ?></td>
                                                                  <td style="background-color: <?php echo $s['package_color']; ?>;"><?php echo $s['package_paper_count']; ?></td>
                                                                  <td><?php echo $s['payment_id']; ?></td>
                                                                  <td><?php echo $s['order_id']; ?></td>
                                                                  <td><?php echo $s['amount_paid']; ?></td>
                                                                  <td><?php echo $s['method']; ?></td>
                                                                  <td><?php echo $s['left_paper_count']; ?></td>
                                                                  <td>
                                                                     <?php 
                                                                        if($s['package_status']==1){
                                                                            echo '<span class="text-danger">'.ACTIVE.'</span>';
                                                                        }else{
                                                                            echo '<span class="text-danger">'.DEACTIVE.'</span>';
                                                                        }
                                                                        ?>
                                                                  </td>
                                                                  <td><?php echo $s['subscribed_on']; ?></td>
                                                                  <td><?php echo $s['expired_on']; ?></td>
                                                                  <td><?php echo $s['requested_on']; ?></td>
                                                               </tr>
                                                               <?php } ?>
                                                            </tbody>
                                                         </table>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <!-- Mock closed -->
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- /.table-responsive -->                
                     </div>
                  </div>
               </div>
               <div id="menu1" class="tab-pane fade">
                  <h3>Teacher Information</h3>
                  <?php //t($examiner_info); ?> 
                  <table class="table">
                     <tbody>
                        <tr>
                           <td class="text-success"><i class="fa fa-user"></i> Full Name</td>
                           <td><?php echo $examiner_info['fname'].' '.$basic['lname']; ?></td>
                        </tr>
                        <tr>
                           <td class="text-success"><i class="fa fa-phone"></i> Contact</td>
                           <td><?php echo $examiner_info['mobile'];?>  <b><?php echo $ex_zone_info['zoneName'] ?> (<?php echo $ex_zone_info['countryCode'] ?>)</b></td>
                        </tr>
                        <tr>
                           <td class="text-success"><i class="fa fa-envelope"></i> Email</td>
                           <td>
                              <a href="mailto:<?php echo 
                                 $examiner_info['email'];?>"><?php echo 
                                 $examiner_info['email'];?></a>
                           </td>
                        </tr>
                        <tr>
                           <td class="text-success"><i class="fa fa-venus"></i> Gender</td>
                           <td><?php echo $examiner_info['gender']==1?'Male':'Female';?></td>
                        </tr>
                        <tr>
                           <td class="text-success"><i class="fa fa-calendar"></i> DOB</td>
                           <td><?php 
                              $date=date_create($examiner_info['dob']);
                              $dob = date_format($date,"M d, Y");
                              echo $dob;
                              ?>           
                           </td>
                        </tr>
                        <tr>
                           <td class="text-success"><i class="fa fa-home"></i> Address</td>
                           <td><?php echo $examiner_info['residential_address'];?></td>
                        </tr>
                        </tr>
                     </tbody>
                  </table>
               </div>
               <div id="menu2" class="tab-pane fade">
                  <h3>Booking Information</h3>
                  <p><b>NOTE: This timing is based on Examiner time zone - <?php echo $ex_zone_info['zoneName'] ?> (<?php echo $ex_zone_info['countryCode'] ?>) </b></p>
                  <table class="table">
                     <tbody>
                        <tr>
                           <td class="text-success"><i class="fa fa-calendar"></i> Speaking date</td>
                           <td><?= $slot_information['date']; ?></td>
                        </tr>
                        <tr>
                           <td class="text-success"><i class="fa fa-calendar"></i> Start time - End time</td>
                           <td><?= $slot_information['start_time']; ?> - <?= $slot_information['start_time']; ?> </td>
                        </tr>                       
                        <tr>
                           <td class="text-success"><i class="fa fa-commenting-o"></i> Meeting ID</td>
                           <td><?php echo $slot_information['meeting_id'];?></td>
                        </tr>
                        <tr>
                           <td class="text-success"><i class="fa fa-link"></i> Meeting URL</td>
                           <td><?php echo $slot_information['meeting_url'];?></td>
                        </tr>
                        <?php if($alow_update): ?>
                        <tr>
                           <td class="text-success"><i class="fa fa-pencil-square-o"></i> Update details</td>
                           <td><a class="btn btn-info btn-sm" href="<?= site_url(); ?>/user/view_bookings/<?= $slot_information['date']; ?>">Update</a></td>
                        </tr>
                        <?php endif; ?>
                        </tr>
                     </tbody>
                  </table>
               </div>
            </div>
            <?php 
            endif;
               ?>
         </div>
      </div>
   </div>
</div>
