<div class="row">
    <div class="col-md-12">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Add Gallery</h3>
            </div>
            <?php echo $this->session->flashdata('flsh_msg');?>
            <?php if(isset($error)) {echo $error;}?>
            <?php echo form_open_multipart('gallery/add'); ?>
            <div class="box-body">
              <div class="row clearfix">
          
          <div class="col-md-6">

            <label for="image" class="control-label"><span class="text-danger">*</span>Image<?php echo SEP;?>Icon<?php echo SEP;?>Video<?php echo SEP;?>File<?php echo SEP;?>Audio</label><?php echo GALLERY_ALLOWED_TYPES_LABEL;?>
            <div class="form-group">
              <input type="file" name="image" value="<?php echo $this->input->post('image'); ?>" class="form-control" id="image" required/>              
              <span class="text-danger"><?php echo form_error('image');?></span>             
            </div>
          </div>

          <div class="col-md-6">
            <label for="title" class="control-label"><span class="text-danger">*</span>Title</label>
            <div class="form-group">
              <input type="text" name="title" value="<?php echo $this->input->post('title'); ?>" class="form-control" id="title" maxlength="50"/>
              <span class="text-danger"><?php echo form_error('title');?></span>
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <input type="checkbox" name="active" value="1"  id="active" checked="checked"/>
              <label for="active" class="control-label">Active</label>
            </div>
          </div>
          
        </div>
      </div>
            <div class="box-footer">
              <button type="submit" class="btn btn-success">
                <i class="fa fa-check"></i> Save
              </button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>