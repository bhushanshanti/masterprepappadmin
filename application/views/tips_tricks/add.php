<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title"><?php echo $title;?></h3>
            </div>
            <?php echo $this->session->flashdata('flsh_msg'); ?>
            <?php echo form_open('tips_tricks/add'); ?>
          	<div class="box-body">
          		<div class="row clearfix">
					
					<div class="col-md-6">
						<label for="pid" class="control-label"><span class="text-danger">*</span>Parent Tips & Tricks</label>
						<div class="form-group">
							<select name="pid" class="form-control selectpicker" data-show-subtext="true" data-live-search="true">
								<option value="">Select</option>
								<?php 
								foreach($all_parent_masters as $p)
								{
									$selected = ($p['id'] == $this->input->post('pid')) ? ' selected="selected"' : "";

									echo '<option value="'.$p['id'].'" >'.$p['parent_subject'].'</option>';
								} 
								?>
							</select>
							<span class="text-danger"><?php echo form_error('pid');?></span>
						</div>
					</div>					

					<div class="col-md-6">
						<label for="display_order" class="control-label">Order</label>
						<div class="form-group has-feedback">
							<input type="text" name="display_order" value="<?php echo $this->input->post('display_order'); ?>" class="form-control" id="display_order" />
							<span class="text-danger"><?php echo form_error('display_order');?></span>
						</div>
					</div>	

					<div class="col-md-12">
						<label for="question" class="control-label"> Tip</label>
						<div class="form-group has-feedback">
							<textarea name="question" class="form-control" id="question"><?php echo $this->input->post('question'); ?></textarea>
							<span class="glyphicon glyphicon-text-size form-control-feedback"></span>
						</div>
					</div>

					<div class="col-md-12" style="display:none;">
						<label for="answer" class="control-label"> Answer</label>
						<div class="form-group has-feedback">
							<textarea name="answer" class="form-control" id="answer">1111</textarea>
							<span class="glyphicon glyphicon-text-size form-control-feedback"></span>
						</div>
					</div>								

					<div class="col-md-4">
						<div class="form-group">
							<label for="active" class="control-label">Active</label>
							<input type="checkbox" name="active" value="1"  id="active" checked="checked"/>
							
						</div>
					</div>	
					
				</div>
			</div>
          	<div class="box-footer">
            	<button type="submit" class="btn btn-success">
            		<i class="fa fa-check"></i> Save
            	</button>
          	</div>
            <?php echo form_close(); ?>
      	</div>
    </div>
</div>
