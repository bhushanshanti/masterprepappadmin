<section class="content-header">
      <h1 class="text-primary">
        Manage schedule
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>        
        <li class="#">Profile</li>
        <li class="active">Manage schedule</li>
      </ol>
    </section>
    <?php echo $this->session->flashdata('flsh_msg');?>
    <span class="msg"></span>
     
     <span class="msg"></span>
         <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-md-12">           
              <div class="pull-right"><a href="<?= site_url('/user/add_schedule') ?>" class="btn btn-info cnm-add-avail-btn">Add new availabitity</a></div>
          </div>

           <div class="box-body table-responsive">
                <table class="table table-striped table-bordered table-sm">
                    <thead>
                    <tr>
                        <th><?php echo SR;?></th>
                        <th>Date</th>   
                        <th><?php echo ACTION;?></th>
                    </tr>
                    </thead>
                    <tbody id="myTable">
                      <?php

                      /* $new_array = array();

                      foreach ($availability_info as $key => $value) {
                        $date = $value['date'];
                        $new_array[$date][] = $value;
                      } */
                      $sr=0;
                      foreach ($availability_info as $key => $value) {
                        $zero=0;$one=1;$pk='id'; $table='manage_availability'; $sr++;                    
                        $id = $value['date'];
                       

                      echo "<tr><td>$sr</td><td>".$value['date']."</td><td>";
                      ?>
                      <a href="<?php echo site_url('user/edit_schedule/'.$id); ?>" class="btn btn-info btn-xs" data-toggle="tooltip" title="Edit"><span class="fa fa-pencil"></span> </a> 
                      <a href="<?php echo site_url('user/remove_schedule/'.$id); ?>" class="btn btn-danger btn-xs" data-toggle="tooltip" title="Delete" onclick="return confirm('Are you sure you want to delete this item?');"><span class="fa fa-trash"></span> </a>
                      <a href="<?php echo site_url('user/view_bookings/'.$id); ?>" class="btn btn-success btn-xs" data-toggle="tooltip" title="view"><span class="fa fa-eye"></span> </a>
                      <?php

                      echo "</td></tr>";
                      }
                      ?>
                    </tbody>
                    </table> 
          
        </div>
      </section>
