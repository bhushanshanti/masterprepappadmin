<section class="content-header">
      <h1 class="text-primary">
        View bookings
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>        
        <li class="#">Profile</li>
        <li class="active">View bookings</li>
      </ol>
    </section>
    <?php echo $this->session->flashdata('flsh_msg');?>
    <span class="msg"></span>
     
     <span class="msg"></span>
         <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-md-12">   
              <?php
              $start_dates = array();
              $end_dates = array();
              $json_hidden = array();
              $date = trim($date);
              $json_hidden[$date] = array();

              foreach ($avail_schedule as $key => $value) {
					$start_dates[] = $value['start_time'];           	
					$end_dates[] = $value['end_time'];
					$user_info[] = array("fname" => $value['fname'] , "lname" => $value['lname'] , "Goal_s" => $value['Goal_s'] , "zoneName" => $value['zoneName'] ,"name" => $value['name']  ,"gender_name" => $value['gender_name']  ,"targateDate" => $value['targateDate'] ,"meeting_id" => $value['meeting_id'] ,"meeting_url" => $value['meeting_url'] ,"band_score" => $value['band_score']  ,"FC" => $value['FC']  ,"LR" => $value['LR']  ,"Pronunciation" => $value['Pronunciation']  ,"GRA" => $value['GRA']  ,"meeting_url_sent" => $value['meeting_url_sent'] ,"examiner_comment" => $value['examiner_comment'] ,"audio_file" => $value['audio_file'] ,"book_speaking_slots_id" => $value['book_speaking_slots_id'],"booked_by" => $value['booked_by'],"user_id" => $value['user_id']);
					$json_hidden[$date][] = $value['start_time']."-".$value['end_time'];
              }
			
              ?>
               <div class="cnm-sections">
                 
                  <div class="cnm-time-slots">
                    <div class='cnm-dates-empty'>
                  	<ul data-date="<?= $date ?>" data-schedule="" data-un="0" class="cnm-select-avail-ul">
                  		<h2>Date : <?= $date ?></h2>



<?php
$starttime = EX_START_TIME;  // your start time
$endtime = EX_END_TIME;  // End time
$duration = EX_SLOT_DIFF;  // split by 30 mins

$array_of_time = array ();
$start_time    = strtotime ($starttime); //change to strtotime
$end_time      = strtotime ($endtime); //change to strtotime

$add_mins  = $duration * 60;

$counter = 0;
$k = 1;

while ($start_time <= $end_time) // loop between time
{
   $array_of_time["counter_".$counter]['start_time'] = date ("H:i", $start_time);
   $start_time += $add_mins; // to check endtie=me
   $array_of_time["counter_".$counter]['end_time'] = date ("H:i", $start_time);
   $k++;
   $counter++;
}

$Kounter = 0;

foreach ($array_of_time as $key => $value) {
	
	if(isset($value['start_time']) && isset($value['end_time'])){
		$active = "";
		$view_btn = "";
		if(in_array($value['start_time'], $start_dates)){
			$index_val = array_search($value['start_time'] , $start_dates);			
			if($end_dates[$index_val] == $value['end_time']){
				
				
				
				$fname        = $user_info[$index_val]['fname'];
				$lname        = $user_info[$index_val]['lname'];
				$Goal_s = $targateDate = $zoneName = $country_name = $gender_name = "---";
				
				if(isset($user_info[$index_val]['Goal_s']) && $user_info[$index_val]['Goal_s'] != "")
					$Goal_s       = $user_info[$index_val]['Goal_s'];
					
				if(isset($user_info[$index_val]['zoneName']) && $user_info[$index_val]['zoneName'] != "")
					$zoneName       = $user_info[$index_val]['zoneName'];
					
				if(isset($user_info[$index_val]['name']) && $user_info[$index_val]['name'] != "")
					$country_name       = $user_info[$index_val]['name'];
					
				if(isset($user_info[$index_val]['gender_name']) && $user_info[$index_val]['gender_name'] != "")
					$gender_name       = $user_info[$index_val]['gender_name'];
					
				if(isset($user_info[$index_val]['targateDate']) && $user_info[$index_val]['targateDate'] != "")
					$targateDate       = $user_info[$index_val]['targateDate'];
					
								
				$active = "active";
				$view_btn = "<div class='btns-sections' style='float: right;'><button type='button' class='btn btn-success btn-xs' data-toggle='modal' data-target='#update-availibility-$Kounter'><span class='fa fa-pencil'></span></button>&nbsp;&nbsp;<button type='button' class='btn btn-success btn-xs' data-toggle='modal' data-target='#myModal-$Kounter'><span class='fa fa-eye'></span></button></div>";
				?>
				<div id="myModal-<?= $Kounter ?>" class="modal fade" role="dialog">
					<div class="modal-dialog">   
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title">Booking information</h4>
							</div>
							<div class="modal-body">
								
								<div class="col-md-6">
									<label for="lname" class="control-label">Student name</label>
									<div class="form-group">
										<p><?= $fname." ".$lname ?></p>
									</div>
								</div>	
								
								<div class="col-md-6">
									<label for="lname" class="control-label">Gender</label>
									<div class="form-group">
										<p><?= $gender_name ?></p>
									</div>
								</div>	
																
								<div class="col-md-6">
									<label for="lname" class="control-label">Country name</label>
									<div class="form-group">
										<p><?= $country_name."(".$zoneName.")" ?></p>
									</div>
								</div>
								
								<div class="col-md-6">
									<label for="lname" class="control-label">Goal set(Speaking)</label>
									<div class="form-group">
										<p><?= $Goal_s."(".$targateDate.")" ?></p>
									</div>
								</div>
								
							</div>
							<div class="modal-footer" style="margin-top: 179px;">
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							</div>
						</div>
					</div>
				</div>
				
				<div id="update-availibility-<?= $Kounter ?>" class="modal fade" role="dialog">
					<div class="modal-dialog">   
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title">Update booking information</h4>
							</div>
							<div class="modal-body">
								<?php echo form_open_multipart('user/update_booking_schedule/'.$user_info[$index_val]['book_speaking_slots_id'].'/'.$date); ?>
								<div class="col-md-6">
									<label for="meeting_id<?= $Kounter ?>" class="control-label">Meeting id</label>
									<div class="form-group">
										<input type="hidden" name="booked_by" value="<?= $user_info[$index_val]['booked_by']; ?>" class="form-control" />
										<input type="hidden" name="user_id" value="<?= $user_info[$index_val]['user_id']; ?>" class="form-control" />
										<input type="text" name="meeting_id" value="<?php echo ($this->input->post('meeting_id') ? $this->input->post('meeting_id') : $user_info[$index_val]['meeting_id']); ?>" class="form-control" id="meeting_id<?= $Kounter ?>" />
									</div>
								</div>	
								
								<div class="col-md-6">
									<label for="meeting_url<?= $Kounter ?>" class="control-label">Meeting url</label>
									<div class="form-group">
										<input type="text" name="meeting_url" value="<?php echo ($this->input->post('meeting_url') ? $this->input->post('meeting_url') : $user_info[$index_val]['meeting_url']); ?>" class="form-control" id="meeting_url<?= $Kounter ?>" />
									</div>
								</div>	
																
								<div class="col-md-6">
									<label for="band_score" class="control-label">Band score</label>
									<div class="form-group">
										<input type="text" name="band_score" value="<?php echo ($this->input->post('band_score') ? $this->input->post('band_score') : $user_info[$index_val]['band_score']); ?>" class="form-control" id="band_score"/>
									</div>
								</div>
								
																
								<div class="col-md-6">
									<label for="band_score" class="control-label">FC</label>
									<div class="form-group">
										<input type="text" name="FC" value="<?php echo ($this->input->post('FC') ? $this->input->post('FC') : $user_info[$index_val]['FC']); ?>" class="form-control" id="FC"/>
									</div>
								</div>
								
																
								<div class="col-md-6">
									<label for="band_score" class="control-label">LR</label>
									<div class="form-group">
										<input type="text" name="LR" value="<?php echo ($this->input->post('LR') ? $this->input->post('LR') : $user_info[$index_val]['LR']); ?>" class="form-control" id="LR"/>
									</div>
								</div>
								
																
								<div class="col-md-6">
									<label for="band_score" class="control-label">Pronunciation</label>
									<div class="form-group">
										<input type="text" name="Pronunciation" value="<?php echo ($this->input->post('Pronunciation') ? $this->input->post('Pronunciation') : $user_info[$index_val]['Pronunciation']); ?>" class="form-control" id="Pronunciation"/>
									</div>
								</div>
								
																
								<div class="col-md-6">
									<label for="band_score" class="control-label">GRA</label>
									<div class="form-group">
										<input type="text" name="GRA" value="<?php echo ($this->input->post('GRA') ? $this->input->post('GRA') : $user_info[$index_val]['GRA']); ?>" class="form-control" id="GRA"/>
									</div>
								</div>
								
								<div class="col-md-6">
									<label for="audio_file<?= $Kounter ?>" class="control-label">Upload audio file</label>
									<div class="form-group">
										<input type="file" name="audio_file" value="" class="form-control" id="audio_file<?= $Kounter ?>" maxlength="30"/>
										<span><?php echo $user_info[$index_val]['audio_file']; ?></span>
									</div>
								</div>
								
								<div class="col-md-12">
									<label for="examiner_comment<?= $Kounter ?>" class="control-label">Additional comments</label>
									<div class="form-group">
										<textarea class="form-control" rows="10" cols="100" name="examiner_comment"><?= $user_info[$index_val]['examiner_comment']; ?></textarea>
									</div>
								</div>
								<input type="hidden" name="meeting_url_sent" value="<?= $this->input->post('meeting_url_sent') ?>" />
								<div class="col-md-12">
									<input type="submit" class="btn btn-success" name="update_schedule" value="update">			
								</div>
								
								
								<?php echo form_close(); ?>
							</div>
							<div class="modal-footer" style="margin-top: 600px;">
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							</div>
						</div>
					</div>
				</div>
				<?php
				$Kounter++;
			}
		}
		echo '<li data-date="'.$date.'" start="'.$value['start_time'].'" end="'.$value['end_time'].'" data-time="'.$value['start_time'].'-'.$value['end_time'].'" class="cnm-select-avail-li-view '.$active.'">'.$value['start_time'].'-'.$value['end_time'].$view_btn.'</li>';
	}
}
?>
                  	</ul></div>
                  	
               </div> 
              
            
          </div>
        </div>
      </section>
      

    

