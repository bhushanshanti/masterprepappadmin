<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Add Admin</h3>
            </div>
            <?php echo $this->session->flashdata('flsh_msg');?>
            <?php echo form_open('user/add'); ?>
          	<div class="box-body">
          		<div class="row clearfix">					
					
					<div class="col-md-6">
						<label for="fname" class="control-label">First name</label>
						<div class="form-group">
							<input type="text" name="fname" value="<?php echo $this->input->post('fname'); ?>" class="form-control" id="fname" maxlength="30"/>
						</div>
					</div>
					<div class="col-md-6">
						<label for="lname" class="control-label">Last name</label>
						<div class="form-group">
							<input type="text" name="lname" value="<?php echo $this->input->post('lname'); ?>" class="form-control" id="lname" maxlength="30"/>
						</div>
					</div>
					<div class="col-md-6">
						<label for="gender_name" class="control-label"><span class="text-danger">*</span>Gender</label>
						<div class="form-group">
							<select name="gender_name" class="form-control selectpicker" data-show-subtext="true" data-live-search="true">
								<option data-subtext="" value="">Select gender</option>
								<?php 
								foreach($all_genders as $p)
								{
									$selected = ($p['id'] == $this->input->post('gender_name')) ? ' selected="selected"' : "";

									echo '<option data-subtext="'.$p['gender_name'].'" value="'.$p['id'].'" '.$selected.'>'.$p['gender_name'].'</option>';
								} 
								?>
							</select>
							<span class="text-danger"><?php echo form_error('gender_name');?></span>
						</div>
					</div>
					<div class="col-md-6">
						<label for="dob" class="control-label">Date of birth</label>
						<div class="form-group has-feedback">
							<input type="text" name="dob" value="<?php echo $this->input->post('dob'); ?>" class="has-datepicker form-control" id="dob" maxlength="10"/>
							<span class="glyphicon form-control-feedback"><i class="fa fa-birthday-cake"></i></span>
						</div>
					</div>

					<div class="col-md-6">
						<label for="email" class="control-label"><span class="text-danger">*</span>Email Id/Username</label><span class="text-danger"> (Max. 60 characters)</span>
						<div class="form-group has-feedback">
							<input type="text" name="email" value="<?php echo $this->input->post('email'); ?>" class="form-control" id="email" maxlength="60"/>
							<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
							<span class="text-danger"><?php echo form_error('email');?></span>
							
						</div>
					</div>
					<div class="col-md-6">
						<label for="mobile" class="control-label">Mobile no.</label>
						<div class="form-group has-feedback">
							<input type="text" name="mobile" value="<?php echo $this->input->post('mobile'); ?>" class="form-control" id="mobile" maxlength="10"/>
							<span class="text-danger"><?php echo form_error('mobile');?></span>
							<span class="glyphicon glyphicon-phone form-control-feedback"></span>
						</div>
					</div>

					<div class="col-md-6">
						<label for="role_id" class="control-label"><span class="text-danger">*</span>Role</label>
						<div class="form-group">
							<select name="role_id" class="form-control selectpicker" data-show-subtext="true" data-live-search="true">
								<option data-subtext="" value="">Select role</option>
								<?php 
								foreach($all_roles as $r)
								{
									$selected = ($r['id'] == $this->input->post('role_id')) ? ' selected="selected"' : "";

									echo '<option data-subtext="'.$r['name'].'" value="'.$r['id'].'" '.$selected.'>'.$r['name'].'</option>';
								} 
								?>
							</select>
							<span class="text-danger"><?php echo form_error('role_id');?></span>
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group">
							<input type="checkbox" name="active" value="1"  id="active" checked="checked"/>
							<label for="active" class="control-label">Active</label>
						</div>
					</div>
					
					<div class="col-md-12">
						<label for="residential_address" class="control-label">Residential Address</label>
						<div class="form-group has-feedback">							
							<textarea name="residential_address" class="form-control" id="residential_address"><?php echo $this->input->post('residential_address'); ?></textarea>
							<span class="glyphicon glyphicon-home form-control-feedback"></span>
						</div>
					</div>					
					<div class="col-md-6">
						<label for="residential_address" class="control-label">Country</label>
						<div class="form-group has-feedback">							
						<select name="country_iso" class="form-control selectpicker" onchange="get_timezone(this.value)" data-show-subtext="true" data-live-search="true">
								<option data-subtext="" value="">Select Country</option>
								<?php 
								foreach($all_countries as $r)
								{
									$selected = ($r['iso'] == $this->input->post('country_iso')) ? ' selected="selected"' : "";

									echo '<option data-subtext="'.$r['name'].'('.$r['iso3'].')" value="'.$r['iso'].'" '.$selected.'>'.$r['name'].'</option>';
								} 
								?>
						</select>							
						</div>
					</div>

					<div class="col-md-6">
						<label for="residential_address" class="control-label">Time Zone</label>
						<div class="form-group has-feedback">							
						<select name="time_zone" id="time_zone" class="form-control selectpicker" data-show-subtext="true" data-live-search="true">
								<option data-subtext="" value="">Select Timezone</option>
						</select>							
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">							
							<label for="active" class="control-label">
								<span class="text-danger">Note: Auto email for Credential details and Role would be sent to this user. </span></label>
						</div>
					</div>

				</div>
			</div>
          	<div class="box-footer">
            	<button type="submit" class="btn btn-success">
            		<i class="fa fa-check"></i> Save
            	</button>
          	</div>
            <?php echo form_close(); ?>
      	</div>
    </div>
</div>
