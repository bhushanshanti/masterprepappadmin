<section class="content-header">
      <h1 class="text-primary">
        Profile
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>        
        <li class="active">Profile</li>
      </ol>
    </section>
    <?php echo $this->session->flashdata('flsh_msg');?>
    <span class="msg"></span>
     
     <span class="msg"></span>
         <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-3">
          <div class="box box-primary">
            <div class="box-body box-profile">
              <?php                                        
                  $data = $this->session->userdata('admin_login_data');
                  foreach ($data as $d) {                                   
             ?>
              <img class="profile-user-img img-responsive img-circle" src="<?php echo site_url('resources/img/user2-160x160.jpg');?>" alt="User profile picture">
              <h3 class="profile-username text-center"><?php echo $d->fname.' '.$d->lname;?></h3>
              <p class="text-muted text-center"><?php echo $d->name;?></p>
              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b><i class="fa fa-envelope"></i></b> <a href="mailto:<?php echo $d->email;?>" class="pull-right"><?php echo $d->email;?></a>
                </li>
                <li class="list-group-item">
                  <b><i class="fa fa-phone"></i></b> <a class="pull-right"><?php echo $d->mobile;?></a>
                </li>
                 <li class="list-group-item">
                  <?php
                    $date=date_create($d->dob);
                    $dob = date_format($date,"M d, Y");
                  ?>
                  <b><i class="fa fa-birthday-cake"></i></b> <a class="pull-right"><?php echo $dob;?></a>
                </li>
                <li class="list-group-item">
                  <b><i class="fa fa-home"></i></b> <?php echo $d->residential_address;?>
                </li> 
                <li class="list-group-item">
                  <?php
                    $date=date_create($d->created);
                    $created = date_format($date,"M d, Y");
                  ?>
                  <b><i class="fa fa-calendar"></i></b> <a class="pull-right"><?php echo $created;?></a>
                </li>                
              </ul>
              <a href="<?php echo site_url('user/edit/'.$d->id);?>" class="btn btn-primary btn-block"><b><i class="fa fa-edit"></i> Update profile</b></a>
            </div>
            <?php } ?>
          </div>

          <!-- About Me Box -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">About Me</h3>
            </div>
            <div class="box-body">
              <strong><i class="fa fa-book margin-r-5"></i> Education</strong>
              <p class="text-muted">
                ---
              </p>
              <hr>              
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        
        <?php
        $user = $this->session->userdata('admin_login_data');
        foreach ($user as $d){$by_user=$d->id; $roleid = $d->roleid; }   
       
       
        ?>
        <div class="col-md-9">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#cp" data-toggle="tab">Change password</a></li>
              <li ><a href="#activity" data-toggle="tab">Activity</a></li>
              <?php if($roleid == 12){  ?>
				  <li ><a href="<?= site_url('/user/manage_schedule') ?>">Manage schedule</a></li> 
			<?php	  } ?>
               
            </ul>

            <div class="tab-content">  

              <div class="active tab-pane" id="cp"> 
                <!-- <form action="user/change_password" method="post" class='form-horizontal' onsubmit = "return validate_cp();"> -->
                  <?php echo form_open_multipart('user/change_password', array('class'=>'form-horizontal')); ?>
                
                  <div class="form-group">
                    <label for="op" class="col-sm-2 control-label">Old password</label>
                    <div class="col-sm-10">
                      <input type="password" class="form-control" name="op" id="op" placeholder="Enter old passowrd" maxlength="30" autocomplete="off" onblur="check_old_pwd(this.value)">
                      <span class="text-danger" id="op"><?php echo form_error('op');?></span>
                      <span class="text-danger" id="op_err"></span>
                    </div>
                    
                  </div>

                  <div class="form-group">
                    <label for="np" class="col-sm-2 control-label">New password</label>
                    <div class="col-sm-10">
                      <input type="password" class="form-control" name="np" id="np" placeholder="Enter new passowrd" maxlength="30" autocomplete="off">
                      <span class="text-danger" id="np"><?php echo form_error('np');?></span>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="rnp" class="col-sm-2 control-label">Re-confirm new password</label>
                    <div class="col-sm-10">
                      <input type="password" class="form-control" name="rnp" id="rnp" placeholder="Re-enter new password" maxlength="30" autocomplete="off">
                      <span class="text-danger" id="rnp"><?php echo form_error('rnp');?></span>
                    </div>
                  </div> 

                  <div class="form-group">
                    <label for="spp" class="col-sm-2 control-label">Show Password</label>
                    <div class="col-sm-10">
                      <input id="spp" type="checkbox" onclick="showPwd()">
                    </div>
                  </div> 

                  
                  
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <button type="submit" class="btn btn-danger">Submit</button>
                    </div>
                  </div>
                  <?php echo form_close(); ?>               
              </div>
<script type="text/javascript">

  function showPwd() {
  var x = document.getElementById("np");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }

  var y = document.getElementById("rnp");
  if (y.type === "password") {
    y.type = "text";
  } else {
    y.type = "password";
  }
}

  function check_old_pwd(old_pwd){
    
          $.ajax({
               url: "<?php echo site_url('user/check_old_pwd');?>",
                async : true,
                type: 'post',
                data: {old_pwd: old_pwd},
                dataType: 'json',
                success: function(response){

                    if(response.status=='true'){
                        $('.msg').html(response.msg);
                        $('#op_err').text('');

                    }else{
                        $('.msg').html(response.msg);
                        $('#op').text('');
                        $('#op_err').text('Please enter correct old password !'); 
                        $('#op').focus();
                        return false;
                    }
                    
                }
            });

  }
    
function validate_cp(){ 

  var op = document.getElementById("op").value;
  var np = document.getElementById("np").value;
  var rnp = document.getElementById("rnp").value;

  if(op==''){
    $('#op_err').text('Please enter old password !'); 
    $('#op').focus();
    return false;
  }else{
    $('#op_err').text(''); 
  }

  if(np==''){
    $('#np_err').text('Please enter new password !'); 
    $('#np').focus();
    return false;
  }else{
    $('#np_err').text(''); 
  }

  if(rnp==''){
    $('#rnp_err').text('Please re-enter new password !'); 
    $('#rnp').focus();
    return false;
  }else{
    $('#rnp_err').text('');
  }

  if(np!='' && rnp!=''){

    if(np!=rnp){
       $('#rnp_err').text('Password mismatched! new password and confirm password do not match'); 
       $('#rnp').focus();
       return false;
    }else{
      $('#rnp_err').text('');
      return true;
    }

  }



}
</script>


              <div class="tab-pane" id="activity">
                No contents here !
              </div> 



            </div>
          </div>
        </div>
      </div>

    </section>
  </div>
 

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-user bg-yellow"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>

                <p>New phone +1(800)555-1234</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>

                <p>nora@example.com</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-file-code-o bg-green"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>

                <p>Execution time 5 seconds</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="label label-danger pull-right">70%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Update Resume
                <span class="label label-success pull-right">95%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-success" style="width: 95%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Laravel Integration
                <span class="label label-warning pull-right">50%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Back End Framework
                <span class="label label-primary pull-right">68%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Allow mail redirect
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Other sets of options are available
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Expose author name in posts
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Allow the user to show his name in blog posts
            </p>
          </div>
          <!-- /.form-group -->

          <h3 class="control-sidebar-heading">Chat Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Show me as online
              <input type="checkbox" class="pull-right" checked>
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Turn off notifications
              <input type="checkbox" class="pull-right">
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Delete chat history
              <a href="javascript:void(0)" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
            </label>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>


  
