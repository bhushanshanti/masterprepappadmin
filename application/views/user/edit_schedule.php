<section class="content-header">
      <h1 class="text-primary">
        Edit schedule
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>        
        <li class="#">Profile</li>
        <li class="active">Edit schedule</li>
      </ol>
    </section>
    <?php echo $this->session->flashdata('flsh_msg');?>
    <span class="msg"></span>
     
     <span class="msg"></span>
         <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-md-12">   
              <?php
              $start_dates = array();
              $end_dates = array();
              $json_hidden = array();
              $date = trim($date);
              $json_hidden[$date] = array();

              foreach ($avail_schedule as $key => $value) {
              	$start_dates[] = $value['start_time'];           	
              	$end_dates[] = $value['end_time'];
              	$json_hidden[$date][] = $value['start_time']."-".$value['end_time'];
              }

              ?>
               <div class="cnm-sections">
                 <input type="hidden" name="schedule_data" id="schedule_data" value='<?= json_encode($json_hidden , true) ?>'>
                  <div class="cnm-time-slots">
                    <div class='cnm-dates-empty'>
                  	<ul data-date="<?= $date ?>" data-schedule="" data-un="0" class="cnm-select-avail-ul">
                  		<h2>Date : <?= $date ?></h2>



<?php
$starttime = EX_START_TIME;  // your start time
$endtime = EX_END_TIME;  // End time
$duration = EX_SLOT_DIFF;  // split by 30 mins

$array_of_time = array ();
$start_time    = strtotime ($starttime); //change to strtotime
$end_time      = strtotime ($endtime); //change to strtotime

$add_mins  = $duration * 60;

$counter = 0;
$k = 1;

while ($start_time <= $end_time) // loop between time
{
   $array_of_time["counter_".$counter]['start_time'] = date ("H:i", $start_time);
   $start_time += $add_mins; // to check endtie=me
   $array_of_time["counter_".$counter]['end_time'] = date ("H:i", $start_time);
   $k++;
   $counter++;
}

foreach ($array_of_time as $key => $value) {
	
	if(isset($value['start_time']) && isset($value['end_time'])){
		$active = "";
		if(in_array($value['start_time'], $start_dates)){
			$index_val = array_search($value['start_time'] , $start_dates);

			if($end_dates[$index_val] == $value['end_time']){
				$active = "active";
			}
		}



		echo '<li data-date="'.$date.'" start="'.$value['start_time'].'" end="'.$value['end_time'].'" data-time="'.$value['start_time'].'-'.$value['end_time'].'" class="cnm-select-avail-li '.$active.'">'.$value['start_time'].'-'.$value['end_time'].'</li>';
	}
}
?>
                  	</ul></div>
                  	<div class="cnm-bottom-section"><button class="btn btn-primary cnm-update-shd-btn">update schedule</button></div></div>

               </div> 
              
            
          </div>
        </div>
      </section>
