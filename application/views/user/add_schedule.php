<section class="content-header">
      <h1 class="text-primary">
        Add schedule
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>        
        <li class="#">Profile</li>
        <li class="active">Add schedule</li>
      </ol>
    </section>
    <?php echo $this->session->flashdata('flsh_msg');?>
    <span class="msg"></span>

     <span class="msg"></span>
         <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-md-12">   
              
               <div class="cnm-sections-hidden">
                 <div class="form-group">
                    <label for="datefilter" class="col-sm-2 control-label">Choose Date</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="datefilter" id="datefilter" placeholder="Enter date range" > 
                       <input type="hidden" name="schedule_data" id="schedule_data" value="">            
                    </div>
                  </div> 

                  <div class="cnm-time-slots"></div>

               </div> 
              
            
          </div>
        </div>
      </section>