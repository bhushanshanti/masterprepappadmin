<span class="badge badge-danger text-uppercase">Common Sections: </span><br/><br/>

<!-- <div class="row">

   <div class="col-md-3" data-toggle="tooltip">
    <a href="<?php echo site_url('programe_master/index');?>" target="<?php echo TARGET_S;?>" class="text-danger">
        <div class="info-box bg-danger">  
        <span class="info-box-icon">
          <span class="glyphicon glyphicon-th text-info"></span>
        </span>
        <div class="info-box-content">
          <span class="info-box-text text-muted">Programe</span>
          <span class="info-box-number text-muted"><?php echo $pc;?></span>
        </div>
      </div>
      </a>
  </div>  

  <div class="col-md-3">
    <a href="<?php echo site_url('category_master/index');?>" target="<?php echo TARGET_S;?>" class="text-danger">
        <div class="info-box">
        <span class="info-box-icon <?php echo BG;?>">
          <span class="glyphicon glyphicon-th text-info"></span></span>
        <div class="info-box-content">
          <span class="info-box-text text-muted">Category (IELTS)</span>
          <span class="info-box-number text-muted"><?php echo $cc;?></span>
        </div>
      </div>
      </a>
  </div>

  <div class="col-md-3">
    <a href="<?php echo site_url('category_master/index_ee');?>" target="<?php echo TARGET_S;?>" class="text-danger">
        <div class="info-box">
        <span class="info-box-icon <?php echo BG;?>">
          <span class="glyphicon glyphicon-th text-info"></span></span>
        <div class="info-box-content">
          <span class="info-box-text text-muted">Category (EE)</span>
          <span class="info-box-number text-muted"><?php echo $cc_ee;?></span>
        </div>
      </div>
      </a>
  </div>

  <div class="col-md-3">
    <a href="<?php echo site_url('category_master/index_et');?>" target="<?php echo TARGET_S;?>" class="text-danger">
        <div class="info-box">
        <span class="info-box-icon <?php echo BG;?>">
          <span class="glyphicon glyphicon-th text-info"></span></span>
        <div class="info-box-content">
          <span class="info-box-text text-muted">Category (Evaluation Test)</span>
          <span class="info-box-number text-muted"><?php echo $cc_et;?></span>
        </div>
      </div>
      </a>
  </div>

  <div class="col-md-3">
    <a href="<?php echo site_url('Test_module/index');?>" target="<?php echo TARGET_S;?>" class="text-danger">
        <div class="info-box">
        <span class="info-box-icon <?php echo BG;?>">
          <span class="glyphicon glyphicon-th text-info"></span></span>
        <div class="info-box-content">
          <span class="info-box-text text-muted">Test Modules</span>
          <span class="info-box-number text-muted"><?php echo $tm;?></span>
        </div>
      </div>
      </a>
  </div>

</div> -->

<div class="row">
          <div class="col-lg-3 col-6">            
            <div class="small-box bg-info">
              <div class="inner">
                <h3><?php echo $clc;?></h3>
                <p>Branches</p>
              </div>
              <div class="icon">
                <i class="fa fa-map-marker"></i>
              </div>
              <a href="<?php echo site_url('center_location/index');?>" target="<?php echo TARGET_S;?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
          
          <div class="col-lg-3 col-6">
            <div class="small-box bg-danger">
              <div class="inner">
                <h3><?php echo $fs;?></h3>
                <p>Inactive Students</p>
              </div>
              <div class="icon">
                <i class="fa fa-user"></i>
              </div>
              <a href="<?php echo site_url('student/fresh_student');?>" target="<?php echo TARGET_S;?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>

          <div class="col-lg-3 col-6">
            <div class="small-box bg-primary">
              <div class="inner">
                <h3><?php echo $ac;?></h3>
                <p>Assigned Tests</p>
              </div>
              <div class="icon">
                <i class="fa fa-book"></i>
              </div>
              <a href="<?php echo site_url('student_answer/assigned_list');?>" target="<?php echo TARGET_S;?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
         
        </div>


<span class="badge badge-danger text-uppercase">Filter: </span><br/><br/>

<button type="button" name="GT" class="btn btn-primary btn-lg" onclick="show_gt_dashboard();">General Training Dashboard</button>
<button type="button" name="ACD" class="btn btn-warning btn-lg" onclick="show_acd_dashboard();">Academic Dashboard</button>
<button type="button" name="EE" class="btn btn-info btn-lg" onclick="show_ee_dashboard();">English essentials Dashboard</button>