<input class="form-control" type="text" placeholder="Search" aria-label="Search" id="myInput">
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header bg-danger">
                <h3 class="box-title text-primary"><?php echo $title;?></h3>
            	<!-- <div class="box-tools">
                    <a href="<?php echo site_url('student/add'); ?>" class="btn btn-success btn-sm">Add</a> 
                </div> -->
                <?php echo $this->session->flashdata('flsh_msg');?>
            </div>
            <div class="box-body table-responsive">
                <table class="table table-striped table-bordered table-sm">
                    <thead>
                    <tr>
						
                        <th>Listening</th>				
						<th>Reading</th>
						<th>Writing</th>
						<th>Speaking</th>						
						<th>Avg.</th>						
						<th>Target Date</th>
                    </tr>
                    </thead>
                    <tbody id="myTable">
                    
                    <tr>						
                        <td><?php echo $goalData['Goal_l']; ?></td>
						<td><?php echo $goalData['Goal_r']; ?></td>
						<td><?php echo $goalData['Goal_w']; ?></td>	
						<td><?php echo $goalData['Goal_s']; ?></td>
                        <td><?php echo $goalData['Goal_avg']; ?></td>
                        <td><?php echo $goalData['targateDate']; ?></td>
                    </tr>
                    
                </tbody>
                </table>
                <div class="pull-right">
                    <?php //echo $this->pagination->create_links(); ?>                    
                </div>                
            </div>
        </div>
    </div>
</div>