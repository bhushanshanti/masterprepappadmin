<input class="form-control" type="text" placeholder="Search" aria-label="Search" id="myInput">
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header bg-danger">
                <h3 class="box-title text-primary"><?php echo $title;?></h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('student/add'); ?>" class="btn btn-success btn-sm">Add</a> 
                </div>
                <?php echo $this->session->flashdata('flsh_msg');?>
            </div>
            <div class="box-body table-responsive">
                <table class="table table-striped table-bordered table-sm">
                    <thead>
                    <tr>
						<th><?php echo SR;?></th>
                        <th>Pic</th>				
						<!-- <th>Token</th> -->
						<th>Name</th>
						<th>Email Id</th>						
						<th>Contact no.</th>						
						<th><?php echo STATUS;?></th>
                        <th><?php echo ACTION;?></th>
                    </tr>
                    </thead>
                    <tbody id="myTable">
                    <?php $sr=0;foreach($students as $s){$zero=0;$one=1;$pk='id'; $table='students';$sr++; ?>
                    <tr>
						<td><?php echo $sr; ?></td>
                        <td>
                            <img src="<?php echo $s['profile_pic'];?>" style="width:50px;height:40px;">
                                
                        </td>
						<!-- <td><?php echo $s['token']; ?></td> -->
						<td><?php echo $s['fname'].' '.$s['lname']; ?></td>
						<td><a href="mailto:<?php echo $s['email'];?>"><?php echo $s['email']; ?></a></td>						
						<td><?php echo $s['mobile']; ?></td>
						
						<td>
                            <?php 
                            if($s['active']==1){
                                echo '<span class="text-success"><a href="javascript:void(0);" id='.$s['id'].' data-toggle="tooltip" title="Click to De-activate" onclick=activate_deactivete('.$s['id'].','.$zero.',"'.$table.'","'.$pk.'") >'.ACTIVE.'</a></span>';
                            }else{
                                echo '<span class="text-danger"><a href="javascript:void(0);" id='.$s['id'].' title="Click to Activate" onclick=activate_deactivete('.$s['id'].','.$one.',"'.$table.'","'.$pk.'") >'.DEACTIVE.'</a></span>';
                            }
                            ?>                                
                        </td>  
						<td>
                            <?php if($s['cnt']>0){ ?>
                            <a href="<?php echo site_url('student_answer/index/'.$s['id']); ?>" class="btn btn-success btn-xs" data-toggle="tooltip" title="View all Attempted Tests"> Attempts &nbsp;
                                <span class="badge"> 
                                   <?php                                    
                                        echo $s['cnt'];                                    
                                    ?>                                  
                                </span>
                            </a>
                        <?php } ?>

                            <a href="<?php echo site_url('student/details/'.$s['id']); ?>" class="btn btn-info btn-xs" data-toggle="tooltip" title="Details"><span class="fa fa-eye"></span> </a>
                            <a href="<?php echo site_url('student/edit/'.$s['id']); ?>" class="btn btn-info btn-xs" data-toggle="tooltip" title="Edit"><span class="fa fa-pencil"></span> </a>
                            
                            <a href="<?php echo site_url('student/goal/'.$s['id']); ?>" class="btn btn-warning btn-xs" data-toggle="tooltip" title="Goal Settings"><span class="fa fa-star"></span> </a>
                            
                            <a href="<?php echo site_url('student/remove/'.$s['id']); ?>" class="btn btn-danger btn-xs" data-toggle="tooltip" title="Delete" onclick="return confirm('<?php echo DELETE_CONFIRM;?>');"><span class="fa fa-trash"></span> </a>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
                </table>
                <div class="pull-right">
                    <?php echo $this->pagination->create_links(); ?>                    
                </div>                
            </div>
        </div>
    </div>
</div>