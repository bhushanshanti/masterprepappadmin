<style type="text/css">
  .bgdiv{
    background-color: #C5E2EC;
  }
  .bgdiv_rep{
    background-color: #FACF65;
  }
</style>
<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border bg-danger">
              	<h3 class="box-title text-primary"><?php echo $title;?></h3>
            </div>
            <?php echo $this->session->flashdata('flsh_msg');?>
            <?php echo form_open('student/reply_to_student_visa_enquiry/'.$visa_enquiryData['visa_enquiry_id']); ?>
          	<div class="box-body">
          		<div class="row clearfix">

              <div class="col-md-3 bgdiv">
                <label for="admin_reply" class="control-label">Name</label>
                <div class="form-group has-feedback">
                  <?php echo $visa_enquiryData['name']; ?>                  
                </div>
              </div>

              <div class="col-md-3 bgdiv">
                <label for="admin_reply" class="control-label">Email</label>
                <div class="form-group has-feedback">
                     
                  <a href="mailto:<?php echo $visa_enquiryData["email"];?>"><?php echo $visa_enquiryData["email"]; ?></a>
                              
                </div>
              </div>

              <div class="col-md-3 bgdiv">
                <label for="admin_reply" class="control-label">Mobile</label>
                <div class="form-group has-feedback">
                  <?php echo $visa_enquiryData['mobile']; ?>                  
                </div>
              </div>

              <div class="col-md-3 bgdiv">
                <label for="admin_reply" class="control-label">Country interested</label>
                <div class="form-group has-feedback">
                  <?php echo $visa_enquiryData['country_intrested']; ?>                  
                </div>
              </div>

              <div class="col-md-3 bgdiv">
                <label for="admin_reply" class="control-label">VISA Type</label>
                <div class="form-group has-feedback">
                  <?php echo $visa_enquiryData['VISA_type']; ?>                  
                </div>
              </div>              

              <div class="col-md-9 bgdiv">
                <label for="admin_reply" class="control-label">Sent on</label>
                <div class="form-group has-feedback">
                  <?php                  
                  $date=date_create($visa_enquiryData['created']);
                  echo date_format($date,"M d, Y H:i"); 
                  ?>                  
                </div>
              </div>

              <div class="col-md-12 bgdiv_rep">
                <label for="admin_reply" class="control-label">Admin previously replied</label>
                <?php $c=0; foreach ($preReplies as $pr) { $c++;?>
                  
                <div class="form-group has-feedback">
                  <?php 
                  echo $c.'-'.$pr['admin_reply']; 
                  echo '<br/>';
                  $date=date_create($pr['created']);
                  echo date_format($date,"M d, Y H:i");

                  ?>                  
                </div>
              <?php } ?>
              </div>              

    					<div class="col-md-12">
    						<label for="admin_reply" class="control-label">Reply</label>
    						<div class="form-group has-feedback">
    							<textarea name="admin_reply" class="form-control" id="admin_reply"><?php echo $this->input->post('admin_reply'); ?></textarea>
    							<span class="fa fa-commenting form-control-feedback"></span>
    						</div>
    					</div>			

				  </div>
			 </div>
          	<div class="box-footer">
            	<button type="submit" class="btn btn-success">
            		<i class="fa fa-check"></i> Send
            	</button>
          	</div>
            <?php echo form_close(); ?>
      	</div>
    </div>
</div>