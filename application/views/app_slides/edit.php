<div class="row">
    <div class="col-md-12">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Edit Slide</h3>
            </div>
            <?php echo $this->session->flashdata('flsh_msg');?>
            <?php if(isset($error)) {echo $error;}?>
           
            <?php echo form_open_multipart('App_slides/edit/'.$app_slides['id']); ?>
             <div class="box-body">
              <div class="row clearfix">
          
          <div class="col-md-6">

            <label for="image" class="control-label"><span class="text-danger">*</span>Image</label><?php echo SLIDES_ALLOWED_TYPES_LABEL;?>
            

             <div class="form-group">
              <input type="file" name="image" value="<?php echo ($this->input->post('image') ? $this->input->post('image') : $app_slides['slide_image']); ?>" class="form-control" id="image" />
              <span>
                <?php 
                if(isset($app_slides['slide_image'])){      
                                    echo '<span>
                                            <a href="'.site_url(APPSLIDE_IMAGE_PATH.$app_slides['slide_image']).'" target="_blank">'.$app_slides['slide_image'].'</a>
                                        </span>';
                                }else{
                                    echo NO_FILE;
                                }
                                ?>
            </span> 
              <span class="text-danger"><?php echo form_error('image');?></span>
            </div>   


          </div>

          <div class="col-md-6">
            <label for="title" class="control-label"><span class="text-danger">*</span>Title</label>
            <div class="form-group">              
               <input type="text" name="title" value="<?php echo ($this->input->post('title') ? $this->input->post('title') : $app_slides['slide_title']); ?>" class="form-control" id="title" required="required" maxlength="50"/>
              <span class="text-danger"><?php echo form_error('slide_title');?></span>
            </div>
          </div>
          <div class="col-md-12">
            <label for="contents" class="control-label">Contents</label>
            <div class="form-group has-feedback">
              <textarea name="contents" class="form-control myckeditor" id="contents"><?php echo ($this->input->post('title') ? $this->input->post('title') : $app_slides['slide_content']); ?></textarea>
              <span class="glyphicon glyphicon-text-size form-control-feedback"></span>
              <span class="text-danger"><?php echo form_error('contents');?></span>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <input type="checkbox" name="active" value="1" <?php echo ($app_slides['active']==1 ? 'checked="checked"' : ''); ?> id='active' />
              <label for="active" class="control-label">Active</label>
            </div>
          </div>
          
        </div>
      </div>
            <div class="box-footer">
              <button type="submit" class="btn btn-success">
                <i class="fa fa-check"></i> Save
              </button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>