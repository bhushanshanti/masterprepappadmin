<input class="form-control" type="text" placeholder="Search" aria-label="Search" id="myInput">
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header bg-danger">
                <h3 class="box-title text-primary"><?php echo $title;?></h3>
                <div class="box-tools">
                    <a href="<?php echo site_url('App_slides/add'); ?>" class="btn btn-success btn-sm">Add</a> 
                </div>
                <?php echo $this->session->flashdata('flsh_msg');?>
            </div>
            <div class="box-body table-responsive">
                <table class="table table-striped table-bordered table-sm">
                    <thead>
                    <tr>
                        <th><?php echo SR;?></th>
                        <th>Title</th>                                              
                        <th>File: Image<?php echo SEP;?>Icon<?php echo SEP;?>Video<?php echo SEP;?>Audio</th>                        
                        <th>Link</th>
                        <th><?php echo STATUS;?></th>
                        <th><?php echo ACTION;?></th>
                    </tr>
                    </thead>
                    <tbody id="myTable">
                        <?php $sr=0;foreach($app_slides as $r){ $zero=0;$one=1;$pk='id'; $table='app_slides'; $sr++;
                            $link=$r['slide_image'];
                        ?>
                    <tr>
                        <td><?php echo $sr; ?></td> 
                        <td><?php echo $r['slide_title']; ?></td>                              
                        <td>
                            <?php 

                                $ext = pathinfo($r['slide_image'], PATHINFO_EXTENSION);
                                if( isset( $r['slide_image']) and ( $ext=='JPG' or $ext=='jpg' or $ext=='jpeg' or $ext=='gif' or $ext=='png' ) ){
                            ?>
                            <img src= '<?php echo $r["slide_image"]; ?>' style="width: 50px; height:40px"/>

                            <?php }elseif( isset($r['slide_image']) and ($ext=='mp4' or $ext=='mp3') ){ ?> 

                                <?php echo '<a href="'.$link.'" target="_blank">'.OPEN_FILE.'</a>';?>
                            <?php }else{ ?>

                                <?php echo NO_FILE; ?>
                            
                            <?php } ?>
                        </td>
                        
                        <td>
                            <input type="text" class="form-control" value="<?php echo $link; ?>" id="<?php echo $sr;?>" style="width:73px;">
                            <button class="btn btn-info btn-xs" onclick="copy_link('<?php echo $sr;?>')"><span class="fa fa-copy"></span> Copy link</button>                            
                        </td>
                        
                        <td>
                            <?php 
                            if($r['active']==1){
                                //echo $r['active'];
                                echo '<span class="text-success"><a href="javascript:void(0);" id='.$r['id'].' data-toggle="tooltip" title="Click to De-activate" onclick=activate_deactivete('.$r['id'].','.$zero.',"'.$table.'","'.$pk.'") >'.ACTIVE.'</a></span>';
                            }else{
                                //echo $r['active'];
                                //$r['active']=0;
                                echo '<span class="text-danger"><a href="javascript:void(0);" id='.$r['id'].' title="Click to Activate" onclick=activate_deactivete('.$r['id'].','.$one.',"'.$table.'","'.$pk.'") >'.DEACTIVE.'</a></span>';
                            }
                            ?>                                
                        </td>                                
                        
                        <td>
                            <a href="<?php echo site_url('App_slides/edit/'.$r['id']); ?>" class="btn btn-info btn-xs" data-toggle="tooltip" title="Edit"><span class="fa fa-pencil"></span> </a> 
                            <a href="<?php echo site_url('App_slides/remove/'.$r['id']); ?>" class="btn btn-danger btn-xs" data-toggle="tooltip" title="Delete" onclick="return confirm('Are you sure you want to delete this item?');"><span class="fa fa-trash"></span> </a>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
                </table>
                <div class="pull-right">
                    <?php echo $this->pagination->create_links(); ?>                    
                </div>                
            </div>
        </div>
    </div>
</div>